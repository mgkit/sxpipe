=pod
    PROGRAMME      unitex2sxpipe - big2small
    DESCRIPTION    réduit drastiquement le nombre de règles de la CFG
    AUTEUR         Antoine Désir
=cut
use strict ;

my $fileIn;
my $fileOut;

while(1) {
	$_ = shift;
	if (/^$/) {last;}
	elsif (/^-in$/) { $fileIn = shift ; }
	elsif (/^-out$/) { $fileOut = shift ; }
}

open(BNF,"<$fileIn") or die ("err bnf");

my %rules ;	# $rules{tag}{motif}[0] = 	1 si on garde la règle
					#	0 ou 2 si on la jette
		# $rules{tag}{motif}[1] = priorité de la règles (-1 pour non prioritaires)
while(<BNF>){
	chomp;
	s/ +/ /g ; # supprime doubles espaces
	if (/^<(.+)> = (.+?) ;/){
		$rules{$1}{$2}[0] = 1 ;
	}
	if (/^<(.+)> = +;/){ # epsilon
		$rules{$1}{""}[0] = 1 ;
	}
	if (/^<(.+)> = (.+?) ; (.+)/){
		$rules{$1}{$2}[1] = $3 ;
	}
}
print "big2small : bnf lu\n";
close BNF;

my $cpt = 1;
foreach (1..20){ # ce maximum de 20 passes devrait suffire
	if ($cpt == 0) { last ; } # si aucune règle n'a été modifiée lors de la dernière passe, la boucle s'arrête
	$cpt = 0;
	foreach my $tag (sort keys %rules){
		if ($tag eq "T"){ next ; }
		if ($tag =~ /_opt$/) { next ; }
		foreach my $motif (sort keys %{$rules{$tag}}){
#			if ($rules{$tag}{$motif}[0] == 0) { next ; }
			my @pattern = split (" ",$motif);
			if ($#pattern == 0) { next ; }
			mot: foreach my $i (0..$#pattern){
				# pour chaque symbole de la règle, on vérifie s'il est optionnel
				# si oui, les règles sont modifiées en fonction
				# et des règles additionnelles sont créées
				my $opt = "_opt";
				if ($pattern[$i] =~ /_opt>/) { next; $opt = "";}
				my $item ;
				my $term = 0 ;
				if ($pattern[$i] =~ /<(.+)>/) {
					$item = $1 ;
				}
				if ($pattern[$i] =~ /"(.+)"/) {
					$item = $1 ;
					$term = 1;
				}
				my $prefixe;
				my $suffixe;
				my $esp0 = " " ;
				my $esp1 = " " ;
				my $esp2 = " " ;
				if ($i == 0) { $prefixe = "" ; $esp0 = ""; $esp1 = ""; }
				else { $prefixe = join(" ",@pattern[0..$i-1]);}
				if ($i == $#pattern) { $suffixe = "" ; $esp0 = ""; $esp2 = ""; }
				else { $suffixe = join(" ",@pattern[$i+1..$#pattern]) ; }
				my $motifSans = "$prefixe$esp0$suffixe" ;
				if ($rules{$tag}{$motifSans}[0] == 1){
					my $us = "";
					if ($term) { $us = "_" ;}
					if ($opt) { # création des règles additionnelles
						$rules{$us.$item."_opt"}{""}[0] = 1;
						$rules{$us.$item."_opt"}{$pattern[$i]}[0] = 1;
					}
#					$rules{$tag}{$motif}[0] = 0;
					delete($rules{$tag}{$motif});
					$rules{$tag}{$prefixe.$esp1."<".$us.$item.$opt.">".$esp2.$suffixe}[0] = 1;
					$rules{$tag}{$motifSans}[0] = 2;
					$cpt++ ;
					last mot;
				}
			}
		}
	}
	print "$_ ( $cpt )\n";
}

print "\nBig2Small OK\n";

# création d'un nouveau fichier, à partir des règles de valeur 1 stockées dans %rules
open(OUT,">$fileOut.tmp") or die ("err small");
foreach my $tag (keys %rules){
	foreach my $motif (keys %{$rules{$tag}}){
		if ($rules{$tag}{$motif}[0] == 1) {
			print OUT "<$tag> = $motif ;";
			if ($rules{$tag}{$motif}[1] < 0 || $rules{$tag}{$motif}[1] > 0){ # règles à priorité
				print OUT " $rules{$tag}{$motif}[1]";
			}
			print OUT "\n";
		}
	}
}
close OUT;

system("sort $fileOut.tmp > $fileOut");
