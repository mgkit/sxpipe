=pod
    PROGRAMME      unitex2sxpipe - moins
    DESCRIPTION    traite le cas particulier des "attributs n�gatifs" (ex. <N-Hum>, par opposition � <N+Hum>)
    AUTEUR         Antoine D�sir
=cut

use strict;

my $bnfIn;
my $bnfOut;
my $lexIn;
my $lexOut;
my $plusLem;
my $lim = "===";

while(1) {
	$_ = shift;
	if (/^$/) {last;}
	elsif (/^-bnf-in$/ || /^-bi$/) { $bnfIn = shift ; }
	elsif (/^-bnf-out$/ || /^-bo$/) { $bnfOut = shift ; }
	elsif (/^-lex-in$/ || /^-li$/) { $lexIn = shift ; }
	elsif (/^-lex-out$/ || /^-lo$/) { $lexOut = shift ; }
	elsif (/^-plus-lem$/ || /^-pl$/) { $plusLem = shift ; }
	elsif (/^-lim$/) { $lim = shift ; }
}

my %poss = ( 	"A" 	=> 1,
		"N"	=> 1,
		"V" 	=> 1,
		"ADV" 	=> 1,
		"DET" 	=> 1,
		"CONJC" => 1,
		"CONJS" => 1,
		"PREP" 	=> 1,
		"INTJ" 	=> 1,
		"PRO" 	=> 1,
		"XI" 	=> 1 );
my %remplace;
my %traitsParPOS;
my %POSTraitNeg;

open(PLUS,"<$plusLem") or die ("err plus");
while(<PLUS>){
	chomp;
	/$lim(.+?)([+-].+?)(:.+)?$lim/;
	my $pos = $1; # "N"
	my $traitsStr = $2; # "+Hum-Ville+Topo"
	my $flexion = $3; # ":m"
	if ($poss{$pos}){
		my %traits;
		trai:while ($traitsStr =~/([+-])([A-Za-z0-9]+)/g){
			$traits{$2} = $1; # $traits{"Hum"} = "+"
			$traitsParPOS{$pos}{$flexion}{$2} = 1; # "Hum"
			if ($1 eq "-") { $POSTraitNeg{$pos}{$flexion} = 1; }
		}
		my $newTraitsStr = "";
		foreach my $tr (sort keys %traits){
			$newTraitsStr = $newTraitsStr.$traits{$tr}.$tr ;
		}
		$remplace{$pos.$traitsStr.$flexion} = $pos.$newTraitsStr.$flexion ;
	}
}

open(IN,"<$bnfIn") or die ("err in");
open(OUT,">$bnfOut") or die ("err out");

my %fromAxiom ;
# modification du .bnf, chaque terminal dont le POS a des "trait(s) n�gatifs" est remplac� par un non-terminal
# ex : ===N-duree=== devient <N-duree>
print OUT "<T> = %word ;\n";
in: while (<IN>){
	chomp;
	foreach my $r (keys %remplace){
		my $qmR = quotemeta($r);
		if (/^<T> = \"$lim$qmR/) { next in; }
		$r =~ /^(.+?)[+-]/;
		my $pos = $1;
		my $flexion;
		if ($r =~ /(:.+)$/) { $flexion = $1; }
		if (exists($POSTraitNeg{$pos}{$flexion}) && s/\"$lim$qmR$lim\"/<$remplace{$r}>/){
			$fromAxiom{$remplace{$r}} = 1;
		}
	}
	foreach my $pos (keys %POSTraitNeg){
		foreach my $flexion (keys %{$POSTraitNeg{$pos}}){
			if (/^<T> = \"$lim$pos$flexion$lim/) { next in; }
			if ( s/\"$lim$pos$flexion$lim\"/<$pos$flexion>/ ){
				$fromAxiom{$pos.$flexion} = 1;
			}
		}
	}
	print OUT "$_\n";
}

my %fA2L;
foreach my $specif (keys %fromAxiom){
	$specif =~ /(.+?)([+-].+?)?(:.+)?$/;
	my $pos = $1; # "N"
	my $traitsStr = $2; # "+Hum-Ville+Topo"
	my $flexion = $3; # ":m"
	my %traits;
	while ($traitsStr =~/([+-])([A-Za-z0-9]+)/g){
		$traits{$2} = $1; # $traits{"Hum"} = "+"
	}
	# ajout des r�gles <POS+trait2:fl> = "===POS-trait1+trait2-etc.:fl==="
	foreach my $chaine (traits(\%traits,keys %{$traitsParPOS{$pos}{$flexion}})){
		print OUT "<$specif> = \"$lim$pos$chaine$flexion$lim\" ;\n";
		print OUT "<T> = \"$lim$pos$chaine$flexion$lim\" ;\n";
		$fA2L{$pos.$chaine.$flexion} = 1;
	}
}
close OUT ;

# stocke tous les mots sp�cifi�s et les compounds
my %fL2L;
# my %compound;
open(IN2,"<$lexIn") or die ("err in2");
while (<IN2>){
	chomp;
	/$lim(.+?)([+-].+?)?(:.+)?$lim/;
	my $pos = $1;
	my $flexion = $3;
	foreach my $spec (keys %fA2L){
		my $qmSpec = quotemeta($spec);
		if (/$qmSpec/){
			/"(.+)"/;
			$fL2L{$1}{$pos.$flexion} = 1;
			# ex : $fL2L{"Zimbabw�en"}{"N:m"} = 1
		}
	}
}
close IN2;
print "lex lu\n";

open(OUT,">$lexOut") or die ("err out");

# suppression des doublons dans le lexique
# remplacement de ===N:m=== par ===N-Hum-Top-Ville:m===
open(IN2,"<$lexIn") or die ("err in2");
inn:while (<IN2>){
	chomp;
	my $term;
	if (/$lim(.+)$lim/){ $term = $1 }
	else {
		print OUT "$_\n";
		next ;
	}
	my $pos;
	my $flexion;
	if ($term =~ /^(.+?)[+-]?.*?(:.+)?$/){
		$pos = $1;
		$flexion = $2;
	}
	if (!exists $POSTraitNeg{$pos}{$flexion}){
		print OUT "$_\n";
		next ;
	}
	/^"(.+?)"\t/;
	if (exists $fA2L{$term}) {
		print OUT "$_\n";
		next;
	}
	if (exists $fL2L{$1}{$term}) { next ; }
	my $chaine = $pos."-".join("-",sort keys %{$traitsParPOS{$pos}{$flexion}}).$flexion;
	if (!exists $fA2L{$chaine}) {next ;}
	my $qmT = quotemeta($term);
	s/$lim$qmT$lim/$lim$chaine$lim/;
	print OUT "$_\n"; 
}
close OUT ;

print "moins\tok\n";

sub traits{
	my ($a1,@tout) = @_;
	my %h1 = %{$a1};
	my @return = ("");
	foreach my $t (sort @tout){
		foreach my $i (0..$#return){
			if ($return[$i] =~ /[+-]$t$/) {next ;}
			if (exists $h1{$t}) {
				$return[$i] = $return[$i].$h1{$t}.$t;
			}
			else {
				push (@return,$return[$i]."-".$t);
				$return[$i] = $return[$i]."+".$t;
			}
		}
	}
	return @return;
}
