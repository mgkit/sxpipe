=pod
    PROGRAMME      unitex2sxpipe - lex
    DESCRIPTION    extrait du Lefff les formes fl�chies n�cessaires au lexique
    AUTEUR         Antoine D�sir
=cut

use strict;
binmode STDOUT, ":utf8";

my $fileIn;
my $fileOut;
my $lim1 = "===";
my $quote = "'";

my $repLefff = "/usr/local/share/lefff";

while(1) {
	$_ = shift;
	if (/^$/) {last;}
	elsif (/^-in$/) { $fileIn = shift ; }
	elsif (/^-out$/) { $fileOut = shift ; }
	elsif (/^-lim$/) { $lim1 = shift ; }
	elsif (/^-quote$/) { $quote = shift ; }
	elsif (/^-lefffdir$/) { $repLefff = shift ; }
}

my $lim2 = $lim1;

open(IN,"<$fileIn") or die ("err in");
open(OUT,">$fileOut") or die ("err out");

# hash %poss : tags Unitex (cl�s) => tags sxpipe (valeurs)
my %poss = ( "A" => 	[ "adj" ],
		"N" => 	[ "nc", "np", ],
		"V" => 	[ "v" ],
		"V+z1" => [ "v" ],
		"ADV" => [ "adv", "advneg", "advp", "advm"],
		"DET" => [ "det" ],
		"CONJC" => [ "coo" ],
		"CONJS" => [ "csu" ],
		"PREP" => [ "prep" ],
		"INTJ" => [ "pres" ],
		"PRO" => [ "pro", "prel", "cln", "cld", "clr", "cll", "cla", "clg", ],
		"XI" => [ "clneg" ] );

# cr�ation d'un hash reprenant le contenu du Lefff
open(LEF,"cat ${repLefff}/*.lex | LC_ALL=C cut -f 1,3,5,7 |") or die ("err lefff");
	# la commande cut -f permet d'extraire les colonnes qui nous int�ressent
	# ici, colonne	1 : forme fl�chie
	#		3 : POS
	#		5 : lemme suivi de "_____#"
	#		7 : flexion
my %lefff;
my %remplaceFlex = ( "m" => "msp",
			"f" => "fsp",
			"s" => "mfs",
			"p" => "mfp",
			"" => "mfsp",
			"e" => "mfsp" );
my $hn;
while(<LEF>){
	chomp;
	my @ln = split("\t",$_);
	$ln[2] =~ s/___.+//;
	if ($remplaceFlex{$ln[3]}) { $ln[3] = $remplaceFlex{$ln[3]}; }
	if ( $ln[3] =~ /^(.+)_/ && $remplaceFlex{$1}) { $ln[3] = $remplaceFlex{$1}; }
	if (exists $lefff{$ln[2]}{$ln[1]}{$ln[3]}){ # hack pour -il et -t-il (et autres !)	
		$hn=1;
		while (exists $lefff{$ln[2]."_$hn"}{$ln[1]}{$ln[3]}){
			$hn++;
		}
		$ln[2] = $ln[2]."_$hn";
	}
	$lefff{$ln[2]}{$ln[1]}{$ln[3]} = $ln[0];
	# exemple : $lefff{"accepter"}{"v"}{"T3s"} = accept�t
}
close LEF;
print "lefff ok\n";

# pour V+z1
open(LEF,"grep \"W\" ${repLefff}/v*.lex | LC_ALL=C cut -f 1,2 |") or die ("err V+z1");
	# la commande cut -f permet d'extraire les colonnes qui nous int�ressent
	# colonne 2 : indice de fr�quence
my %freq;
while(<LEF>){
	chomp;
	my @ln = split("\t",$_);
	$ln[0] =~ s/.*://;
	if ($ln[0] =~ /_uw/) {next;}
	next if (!defined $ln[1]);
	$freq{$ln[0]} = $ln[1];
	# exemple : $freq{"prendre"} = 300
}
close LEF;
print "freq\tOK\n";

# parcourt toutes les entr�es du Lefff correspondant � la grammaire
# et les place dans le fichier de sortie sous la syntaxe des lexiques sxpipe
ligne: while(<IN>){
	chomp;
	/$lim1(.+)$lim2/;
        my $term = $1;
	if ( $term =~ /(.+?)\.([A-Z]+)/) { # lemme avec POS
		my $lemme = $1;
		my $pos = $2;
		my @flex;
		while ($term =~ /:([A-Za-z0-9]+)/g){
			push(@flex,$1);
		}
		foreach my $fl (keys(%{$lefff{$lemme}{$poss{$pos}[0]}})){
			if ($flex[0]){ # terminaux avec flexion (avoir.V:W)
			    foreach my $f (@flex){
				if ($fl =~ /$f/) {
					my $sortie = "\"$lefff{$lemme}{$poss{$pos}[0]}{$fl}\"\t\t$quote$lim1$term$lim2$quote\t\[\] ;\n";
					$sortie = underscore($sortie);
					print OUT $sortie;
				}
			    }
			}
			else { # sans flexion (avoir.V)
				my $sortie = "\"$lefff{$lemme}{$poss{$pos}[0]}{$fl}\"\t\t$quote$lim1$term$lim2$quote\t\[\] ;\n";
				$sortie = underscore($sortie);
				print OUT $sortie;
			}
		}
	}
	elsif ( $term =~ /^(A|ADV|CONJC|CONJS|DET|INTJ|N|PREP|PRO|V|XI)(\+|\-|:|$)/ ) { # pas de lemme, mais POS
		$term =~ /[A-Za-z0-9+-]+/;
		my $pos = $&;
		my $posOld = quotemeta($pos);
		$pos =~ s/-.+?([+:]|$)/$1/g ;
		if (!exists $poss{$pos}) {print "Le POS \"$pos\" n'est pas reconnu pour l'instant.\n";}
		my @flex;
		while ($term =~ /:([A-Za-z0-9]+)/g){
			push(@flex,$1);
		}
		$term =~ s/$posOld/$pos/ ;
		lemme: foreach my $lemme (keys(%lefff)){
			if ($pos =~ /\+z1/ && defined($freq{$lemme}) && $freq{$lemme} < 100){ next lemme; } # V+z1
			foreach my $i ( 0 .. $#{ $poss{$pos} } ) {
				foreach my $fl (keys(%{$lefff{$lemme}{$poss{$pos}[$i]}})){
					if ($flex[0]){ # avec flexion (V:3s)
					    flex: foreach my $f (@flex){
						my @flexSplit = split(//,$f);
						foreach my $fS (@flexSplit) {
						    	if ($fl !~ /$fS/) { next flex; }
						}
						my $sortie = "\"$lefff{$lemme}{$poss{$pos}[$i]}{$fl}\"\t\t$quote$lim1$term$lim2$quote\t\[\] ;\n";
						$sortie = underscore($sortie);
						print OUT $sortie;
					    }
					}
					else { # sans flexion (par ex. N)
						if ($lefff{$lemme}{$poss{$pos}[$i]}{$fl} =~ /~/) { next; } # petit hack
						my $sortie = "\"$lefff{$lemme}{$poss{$pos}[$i]}{$fl}\"\t\t$quote$lim1$term$lim2$quote\t\[\] ;\n";
						$sortie = underscore($sortie);
						print OUT $sortie;
					}
				}
			}
		}
	}
	else { # il reste les lemmes sans POS
		my $lemme;
		my @flex;
		if ($term =~ /^(.+?):(.+)$/){
			$lemme = $1;
			while ($term =~ /:([A-Za-z0-9]+)/g){
				push(@flex,$1);
			}
		}
		else { $lemme = $term; }
		if (!exists $lefff{$lemme}) { 
			print "!!! \"$lemme\" non trouv� dans le Lefff\n";
			next ;
		}
		my $compt = 1;
		while (exists $lefff{$lemme}){
			foreach my $pos (keys(%{$lefff{$lemme}})){
				foreach my $fl (keys(%{$lefff{$lemme}{$pos}})){
					my $sortie;
					if ($flex[0]){ # avec flexion (===avoir:3s===)
						flex2: foreach my $f (@flex){
							my @flexSplit = split(//,$f);
							foreach my $fS (@flexSplit) {
							    	if ($fl !~ /$fS/) { next flex2; }
							}
							$sortie = "\"$lefff{$lemme}{$pos}{$fl}\"\t\t$quote$lim1$term$lim2$quote\t\[\] ;\n";
						}
					}
					else { # sans flexion (===avoir===)
						$sortie = "\"$lefff{$lemme}{$pos}{$fl}\"\t\t$quote$lim1$term$lim2$quote\t\[\] ;\n";
					}
					$sortie = underscore($sortie);
					print OUT $sortie;
				}
			}
			$lemme =~ s/_[0-9]+$//;
			$lemme = "${lemme}_$compt";
			$compt++;
		}
	}	
}
close IN;
close OUT;
print "lex1 OK\n";

sub underscore {
	my ($sortie) = @_;
	$sortie =~ /^(.+? .+?)+?\t/ ;
	my $temp = $& ;
	my $temp2 = quotemeta($&) ;
	$temp =~ s/ /_/g;
	$sortie =~ s/$temp2/$temp/ ;
	return $sortie ;
}
