=pod
    PROGRAMME      unitex2sxpipe - recode_all
    DESCRIPTION    recodage des fichiers .grf en UTF-16-LittleEndian
    AUTEUR         Antoine Désir
=cut

use strict;

my $repGrf = "/home/antoine/sxpipe/dag2dag/ilimp/tmp";

while(1) {
	$_ = shift;
	if (/^$/) {last;}
	elsif (/^-tempdir$/) { $repGrf = shift ; }
}

foreach (<${repGrf}/*.grf>){
	system ("iconv -f UTF-8 -t UTF-16LE $_");
}
