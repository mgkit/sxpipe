=pod
    PROGRAMME      unitex2sxpipe - bnf
    DESCRIPTION    cr�e le fichier .bnf (CFG)
    AUTEUR         Antoine D�sir
=cut
use strict;

my $graph;
my $bnf;
my $lem;
my $conf;
my $lim1 = "===";
my $quote = '"';
my $repgraphs = "/home/antoine/alpage_tools/sxpipe/dag2dag/ilimp/tmp";
my $unitexbindir = "~/Unitex/App";
my $compiler = 0;

while(1) {
	$_ = shift;
	if (/^$/) {last;}
	elsif (/^-graph$/) { $graph = shift ; }
	elsif (/^-grfdir$/) { $repgraphs = shift ; }
	elsif (/^-ubd$/ || /--?unitex_bindir/) { $unitexbindir = shift ; }
	elsif (/^-bnf$/) { $bnf = shift ; }
	elsif (/^-lem$/) { $lem = shift ; }
	elsif (/^-lim$/) { $lim1 = shift ; }
	elsif (/^-quote$/) { $quote = shift ; }
	elsif (/^-config$/) { $conf = shift ; }
	elsif (/^\+c$/){ $compiler = 1 ; }
}

my $lim2 = $lim1;
if (!$bnf) { $bnf = "$graph.bnf"; }
if (!$lem) { $lem = "$graph.lem"; }

# compilation du graphe (si ce n'est pas d�j� fait / si option +c)
if (($compiler) || !open(TEMP,"$repgraphs/$graph.fst2")) { compile() ; }
close TEMP;



open(CONF,"<$conf") or die ("err conf");
my %sorties;
my %sortiesBalises;
my %priorites;
my %entitesNommees;
my %remplace;
my @aAjouter;
my $num ;
my $sousNum ;
my $prendreEnCompteSorties = 0 ;
while (<CONF>){ # r�cup�ration des infos du fichier de config
	chomp;
	if ($_ eq "") {next;}
	if (/^____(.+?) /) { $num = $1 ; next ; }
	if ($num == 1)	{
		/(.+?)\t(.+?)\t(.+)/;
		$sorties{$1}{$2} = $3;
		# ex : $sorties{"[IMP]"}{"il"} = "ilimp!"
	}
	if ($num == 2)	{
		/(.+)\t(.+)/;
		$priorites{$1} = $2;
	}
	if ($num == 3)	{
		if (/^----(.+?) /){
			$sousNum = $1 ;
			next ;
		}
		if ($sousNum == 2) {
			push(@aAjouter,$_);
		}
		if (/(.+)\t(.+)/){
			$remplace{$1} = $2;
			# ex : $remplace{jusque �} = jusqu'�
		}
		if ($sousNum == 3) {
			if (/oui/i) { $prendreEnCompteSorties = 1; }
		}
	}
	if ($num == 5)	{
		if (/(.+)\t(.+)/){
			$sortiesBalises{$1} = $2;
			# ex : $sortiesBalise{"<DateHoraire>"} = "DATE"
		}
	}
	if ($num == 6)	{
		if (/(.+)\t(.+)/){
			$entitesNommees{$1} = $2;
			# ex : $entitesNommees{"<Evenement>"} = "Evenement"
		}
	}
}
$num = undef;


# les fichiers .fst2 sont encod�s en UTF-16LE => recodage
open(IN,"cat $repgraphs/$graph.fst2 | iconv -f UTF-16 -t L1 |") or die ("err fst");
open(OUT,">$bnf") or die("Could not open $bnf for writing: $!");

# traitement de tous les terminaux comme
# <T> = "pas" ;
# <T> = "[[avoir.V:3s]]" ;    etc.
open(LEM,">$lem") or die("err LEM");

my %elisions = ( "l" => 1,
		"s" => 1,
		"d" => 1,
		"m" => 1,
		"n" => 1,
		"t" => 1,
		"jusqu" => 1,
		"qu" => 1 );
while(<IN>){
 	s/\r//g;
        chomp;
        if (/^%<E>/) { next; }
	if (/^%<MOT>/) { # hack pour <MOT>
		# print "MOT reper� ;)\n";
		# print OUT "<MOT> = <T> ;\n" ;
		next ;
	}
        infsup();
	foreach my $aRemplacer (keys %remplace){ # remplacements sp�cifi�s dans le fichier de config
	  my $remplacerPar = $remplace{$aRemplacer};
	  $aRemplacer = quotemeta($aRemplacer);
	  s/$aRemplacer/$remplacerPar/g ;
	}
	if (/^%($lim1.+?$lim2)/) {
		my $lem = $1;
		print LEM "$lem\n";
		# next;
	}
        if (/^[%@](.+)/) {
                my $term = $1;
		if ($term =~ /^(.+?)\//) { $term = $1 ; }
		if (defined $elisions{$term}) { $term = $term."'";} # hack pour �lisions
		if ($term eq '"') { $term = '\\"' ; }  # hack pour """
		if ($term eq "'") { next; } # $term = "\\'" ; }  # hack pour "'"
		if ($term eq "\\") { $term = "\\\\" ; }  # hack pour "\"
                print OUT "<T> = $quote$term$quote ;\n";
        }
}
close LEM;
close IN;

# transcodage du *.fst2 en *autolst.txt contenant tous les motifs possibles de la grammaire
my $separateurInOut = " oouutt ";
my $separ = "::;::";
print STDERR "$unitexbindir/Fst2List -p s -t s -s '$separ' -s0 '$separateurInOut' $repgraphs/$graph.fst2\n";
system ("$unitexbindir/Fst2List -p s -t s -s '$separ' -s0 '$separateurInOut' $repgraphs/$graph.fst2") == 0 
  or die ("Could not execute $unitexbindir/Fst2List -p s -t s -s '$separ' -s0 '$separateurInOut' $repgraphs/$graph.fst2");

# toujours de l'UTF-16...
system ("iconv -c -f UTF-16 -t L1 $repgraphs/".$graph."autolst.txt > a && mv a $repgraphs/".$graph."autolst.txt");# == 0 or die ("Could not open recode pipe for $repgraphs/".$graph."autolst.txt: $!");

$num=undef;
# ajouts des <T> = "jusqu'�" (termes remplac�s)
foreach (values %remplace){
	if (/^\{/) {next;}	# print "<T> = \"".$_."\" ;\n";
	print OUT "<T> = \"".$_."\" ;\n";
}

# traitements de tous les motifs (<PATTERN>)
# et sous-motifs (par ex. <etreGN>)
open(IN,"< $repgraphs/".$graph."autolst.txt") or die("Could not open $repgraphs/".$graph."autolst.txt for reading: $!");
my $boucles = 0;
my $pri = 0;
my $tag;
my %balisesEnPlus;
my $ltot = `wc -l $repgraphs/${graph}autolst.txt`;
chomp($ltot);
$ltot =~ s/^ +//;
$ltot =~ s/ .*//;
my $l;
while (<IN>) {
  chomp;
  $l++;
  print STDERR "  $l/$ltot\r";
  s/\r//g;
  s/::_::;//g;
  if (/([0-9]+) path stopped by cycle/) {
    if ($1 > 0) {
      /the automate (.+?),/;
      print "Warning : ".$1." contient encore (au moins) une boucle.\n";
      $boucles++;
    }
    next;
  }
  if (/^\[/) {
    print OUT "\*$_\n" ;
    if (/^\[1 /) {
      $tag = "PATTERN";
    } else {
      /automata (.*?)]/;
      $tag = $1 ;
    } 
    next;
  }
  if (/^ the autom/) {
    print OUT "*msg Unitex : $_\n";
    next;
  }
  my $prio;
  my @inOut = split($separateurInOut);
  $_ = $inOut[0];
  my $sortieUnitex = $inOut[1];
  my $qmSepar = quotemeta($separ);
  if ($prendreEnCompteSorties) { # ins�re les sorties dans le motif
    my @splitIn = split ($qmSepar,$_);
    my @splitOut = split ($qmSepar,$sortieUnitex);
    $_ = "";
    my $end;
    if ($#splitIn > $#splitOut) {
      $end = $#splitIn ;
    } else {
      $end = $#splitOut ;
    }
    foreach my $i (0..$end) {
      my $o;
      if ($splitOut[$i] ne "") {
	if ($splitOut[$i] =~ s/prio ?= ?([0-9]+)//ig) {
	  $prio = " $2";
	}
	$splitOut[$i] =~ s/ /_/g;
	$splitOut[$i] =~ s/</\\</g;
	$splitOut[$i] =~ s/>/\\>/g;
	$o = "{$splitOut[$i]!} ";
	$balisesEnPlus{$splitOut[$i]} = 1 ;
      }
      $_ = "$_$o$splitIn[$i]$separ";
    }
  }
  s/($separ)+/ /g;		# supprime les doubles espaces
  s/ +/ /g;			# supprime les doubles espaces
  if ($sortieUnitex =~ /\[[A-Z]+?\]/) {
    $sortieUnitex = $& ;
  }
  foreach my $aTagger (keys %{$sorties{$sortieUnitex}}) {
    s/ $aTagger / $sorties{$sortieUnitex}{$aTagger} / ;
  }
  s/ '/'/g;	       # supprime l'espace �ventuel devant apostrophes
  s/\\/\\\\/g;	       # hack pour "\"
  s/\\\\([<>])/\\\1/g; # hack pour "\": on repare les \< et \>
  s/"/\\"/g;	       # hack pour """
  infsup();
  foreach my $aRemplacer (keys %remplace) { # remplacements sp�cifi�s dans le fichier de config
    my $remplacerPar = $remplace{$aRemplacer};
    $aRemplacer = quotemeta($aRemplacer);
    s/$aRemplacer/$remplacerPar/g ;
  }
  tr/{}/<>/; # dans le fichier Unitex, les noms des sous-graphes sont d�limit�s par des accolades
  # $_ = $_." ";
  s/([^ ^<^>]+|\\[<>])( |$)/$quote$1$quote /g; # quotes
  s/$quote([^ ]+\!)$quote/$1/g;	# d�quote les ilimp! et autres
  s/^ +//; s/ +$// ;	      # supprime les blancs en d�but et en fin
  if (exists $sortiesBalises{$_}) {
    #		print "$sortiesBalises{$_} $_ \n";
    print OUT "<$sortiesBalises{$_}\:\!> = ;\n";
    print OUT "<\:\!$sortiesBalises{$_}> = ;\n";
    $_ = "<$sortiesBalises{$_}\:\!> $_ <\:\!$sortiesBalises{$_}>";
  }
  if (exists $entitesNommees{$_}) {
    #		print "$entitesNommees{$_} $_ \n";
    print OUT "<$entitesNommees{$_}!> = $_ ;\n";
    $_ = "<$entitesNommees{$_}!>";
  }
  my $regle = "<$tag> = $_";
  if (exists $priorites{$regle}) { # sp�cification des r�gles � priorit�
    $prio = " $priorites{$regle}";
    $pri++ ;
  } 
  print OUT "$regle ;$prio\n";
}
$tag = undef;
close(IN);
if ($boucles > 1) { print "!!! $boucles graphes contiennent encore des boucles.\n"; }
if ($boucles == 1) { print "!!! Un graphe contient encore des boucles.\n"; }
print "$pri r�gle(s) non prioritaire(s) rencontr�e(s).\n";

#r�gles � ajouter pour les sorties
foreach my $b (keys %balisesEnPlus){
	$b =~ s/"/\\"/g;	# hack pour """
	print OUT "<$b\!> = ;\n";
}

#r�gles � ajouter du fichier de config
foreach my $r (@aAjouter){
	print OUT "$r\n";
}

#enfin, on ajoute les fioritures
print OUT "<T> = __any__ ;\n";
print OUT "<XPATTERN> = __dummy__ ;\n";

close(OUT);
print "bnf\tOK\n";

sub compile { #compilation du graphe (� partir du .grf)
	print STDERR "Compiling graph $graph.grf: $unitexbindir/Grf2Fst2 $repgraphs/$graph.grf\n";
	print STDERR  "cd $repgraphs && $unitexbindir/Grf2Fst2 $graph.grf";
	system("cd $repgraphs && ../$unitexbindir/Grf2Fst2 $graph.grf") == 0 or die ("err compile");
	print "Graphs compilation OK\n";	
}

sub infsup { # change <blabla> en "===blabla===" en modifiant blabla �ventuellement
	while(/(?<!\\)<(.*?[^\\])>/g){
		my $term = $1;
		if ($term eq "T") { next; }
		if ($term eq "MOT") { s/<MOT>/<T>/g; next ; } # <MOT> devient <T>
		# ici, on peut bidouiller $term pour le rendre conforme
		my $term2 = $lim1.$term.$lim2;
		$term = quotemeta($term);
		s/<$term>/$term2/;
		s/$lim1<(.*?)$lim2>/$lim1$1$lim2/g;
	}
}
