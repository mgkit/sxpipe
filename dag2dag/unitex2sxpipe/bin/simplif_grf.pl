=pod
    PROGRAMME      unitex2sxpipe - simplif_grf
    DESCRIPTION    "simplifie" les graphes, en cr�ant des sous-graphes et en rempla�ant les boucles
    AUTEUR         Antoine D�sir
=cut

use strict;
use Encode;

my $repHome = "/home/antoine";
my $repDag2dag = "alpage_tools/sxpipe/dag2dag";
my $repGrf = $repHome."/".$repDag2dag."/ilimp/grf";
my $repTemp = $repHome."/".$repDag2dag."/ilimp/tmp";
my $repBlank = $repHome."/".$repDag2dag."/unitex2sxpipe/grf";

while(1) {
	$_ = shift;
	if (/^$/) {last;}
	elsif (/^-tempdir$/) { $repTemp = shift ; }
	elsif (/^-blank$/) { $repBlank = shift ; }
	elsif (/^-grfdir$/) { $repGrf = shift ; }
}

my %sousGraphes;
my $numSG = 1;

graphe:foreach my $graph (<${repGrf}/*.grf>) {
  $graph =~ /\/([^\/]+)\.grf/;
  my $nomGraph = $1;

  open(IN,"<:encoding(UTF16-LE)", "$graph") or die ("err in");
  open(OUT,">:encoding(UTF16-LE)", "$repTemp/${nomGraph}.grf") or die ("err out");

  my $enfin = 0;
  my $nbLn;
  my %boucles;
  my %boxes;
  my %boxesLiees;
  my $numLn = 0;
  while (<IN>) {
    chomp;
    s/\r$//;
    if ($enfin == 0 && $_ eq "#") {
      $enfin = 1;
      print OUT "$_\r\n";
      next;		
    }
    if ($enfin == 0) {
      print OUT "$_\r\n";
      next;
    }
    if ($enfin == 1) {
      $nbLn = $_;
      $enfin = 2;
      print OUT "$_\r\n";
      next;
    }
    my $out;
    /\"(.+)\"(.+)/;
    my $box = $1;
    my $fin = $2;
    my $epsilon = 0;
    # remplacement des boxes � plusieurs items par des sous-graphes
    if ($box =~ /\+/ && $nbLn > 3) {
      if ($box =~ /<E>/) {	# epsilon sera rajout� par apr�s
	$epsilon = 1 ;
	$box =~ s/\+<E>//;
	$box =~ s/<E>\+//;
      }
      while ($box =~ /<([^>]+?)\+([^<]+?)>/g) {
				# pour �viter de confondre les "+" des masques s�mantiques et ceux qui s�parent les items
	my $qM = quotemeta($&);
	my $x = $&;
	$x =~ s/\+/====plus====/g;
	$box =~ s/$qM/$x/;
      }
      if ($box =~ /\+/) {
	my @itemsBox = sort(split("\\+",$box));
	foreach (@itemsBox) {
	  s/====plus====/+/g;
	}
	$box = join('+',@itemsBox);
	if (exists $sousGraphes{$box}) {
	  $box = ":$sousGraphes{$box}";
	} else {
	  my $nomSG = "${nomGraph}_sg${numSG}_$itemsBox[0]_$itemsBox[-1]";
	  $nomSG =~ s/[ \']/-/g; # pour �viter tout probl�me de nom de fichier,
	  $nomSG =~ s/[:\.<>\\�]//g; # tous les caract�res autres que A-Z, a-z, 0-9 et _
	  $nomSG =~ s/[^A-Za-z0-9_-]/e/g; # sont supprim�s ou remplac�s
	  $sousGraphes{$box} = $nomSG;
	  open(SG,">:encoding(UTF16-LE)", "$repTemp/$nomSG.grf") or die("err SG");
	  # pour cr�er le nouveau .grf, un fichier "vierge" (blank2.grf) est utilis�
	  open(BLANK,"<:encoding(UTF16-LE)", "${repBlank}/blank2.grf") or die("err blank2");
	  while (<BLANK>) {
	    print SG $_;
	  }
	  close BLANK;
	  print SG "\"$box\" 145 200 1 1 \r\n\r\n";
	  close SG;
	  $box = ":$nomSG";
	  $numSG++;
	}
      }
      $box =~ s/====plus====/+/g;
      $out = '"';
      if ($epsilon) {
	$out .= "<E>+";
      }
      $out = $out."$box\"$fin\r\n";
    } else {
      $out = "$_\r\n";
    }
    # remplacement des boucles par un sous-graphe r�cursif
    my @outs = split(" ",$fin);
    foreach my $i (1..$outs[2]) {
      if ($outs[2+$i] == $numLn) {
	if (exists $boucles{$box}) {
	  # !!!!!!!! � compl�ter
	}
	$boucles{$box} = $box."_plus";
	$boucles{$box} =~ s/[:<>]/_/g;
	open(BOUCLE,">:encoding(UTF16-LE)", "$repTemp/${boucles{$box}}.grf") or die("err bcl");
	open(BLANK,"<:encoding(UTF16-LE)", "${repBlank}/boucle_blank.grf") or die("err boucle_blank");
	while (<BLANK>) {
	  print BOUCLE $_;
	}
	close BLANK;
	print BOUCLE "\"$box\" 145 200 2 1 3 \r\n";
	$box = ":$boucles{$box}";
	print BOUCLE "\"$box\" 30 100 1 1 \r\n\r\n";
	close BOUCLE;
	$outs[2]--;
	my $end = " ".join(" ",@outs[0..1+$i])." ".join(" ",@outs[3+$i..$#outs])." ";
	$out = "\"$box\"$end\r\n";
      }
      $boxesLiees{$outs[2+$i]} = 1;
    }
    if ($outs[2] > 0) {
      $boxes{$numLn} = $box;
    }

    print OUT $out;
    $numLn++;			# num�ro de ligne / box
  }
  foreach my $numBox (1..$nbLn) { # rep�re les boxes "isol�es", qui risquent de poser probl�me plus tard
    if (exists $boxes{$numBox} && !exists $boxesLiees{$numBox}) {
      print STDERR "Attention : ${nomGraph}.grf -> la box n�$numBox \"$boxes{$numBox}\" n'est pas li�e � l'�tat initial.\n";
    }
  }
  close IN;
  close OUT;
}
