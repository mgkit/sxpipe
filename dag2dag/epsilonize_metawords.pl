#!/usr/bin/env perl
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$| = 1;

while (<>) {
    s/({[^}]+}) *( ?[\"\[\]«»] ?)/($1 $2 | $1 _EPSILON)/g;
    print $_;
}
