#!/usr/bin/env perl
use DBI;
use Encode;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

my $oldstyle = 0;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-d$/) {$dir=shift;}
    elsif (/^-o$/) {$oldstyle = 1;}
    elsif (/^-l$/) {$lang=shift;}
}

#my $dbh = DBI->connect("dbi:SQLite:$dir/ne.dat", "", "", {RaiseError => 1, AutoCommit => 1});
#my $sth = $dbh->prepare('select value from data where key=?;');
my $vars_dbh = DBI->connect("dbi:SQLite:$dir/ne_vars.$lang.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $vars_sth = $vars_dbh->prepare('select key from data where variant=?;');
my $refs_dbh = DBI->connect("dbi:SQLite:$dir/ne_refs.$lang.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $refs_sth = $refs_dbh->prepare('select weight,name,def,link from data where key=? and type=?;');
my $refs_person_sth = $refs_dbh->prepare('select weight,name,def,link from data where key=? and (type="_PERSON" or type="_PERSON_m" or type="_PERSON_f");');
my $refs_person_m_sth = $refs_dbh->prepare('select weight,name,def,link from data where key=? and (type="_PERSON" or type="_PERSON_m");');
my $refs_person_f_sth = $refs_dbh->prepare('select weight,name,def,link from data where key=? and (type="_PERSON" or type="_PERSON_f");');
my $lefff_dbh = DBI->connect("dbi:SQLite:$dir/lefff.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $lefff_sth = $lefff_dbh->prepare('select value from data where key=?;');
my $first_names_dbh = DBI->connect("dbi:SQLite:$dir/first_names.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $first_names_sth = $first_names_dbh->prepare('select value from data where key=?;');
my $lefff_locations_dbh = DBI->connect("dbi:SQLite:$dir/lefff_locations.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $lefff_locations_sth = $lefff_locations_dbh->prepare('select value from data where key=?;');


my %is_in_lefff;
my %is_in_lefff_locations;

my (%cache, %cache_is_filled);
my %entity;
my $contains_first_token = 0;
$country_context{FR} = 1;
$city_context{Paris} = 1;

my $escapednonstdchar = qr/(?<=\\)[\(\|\)]/;
my $stdchar = qr/(?:[^\(\|\)]|$escapednonstdchar)/;

while (<>) {
  chomp;

#  print STDERR "="x40 ."\n";

  if (is_new_doc($_)) {
    for (keys %entity) {
#      $entity{$_} /= 2;
      $entity{$_} = 0;
    }
    %country_context = ();
    $country_context{FR} = 1;
    %city_context = ();
    $city_context{Paris} = 1;
    %temporal_context = ();
  }

#  while (s/([\(\|]\s*)\((\s*{([^}]*)}\s*\S+\s*(?:\|\s*{\3}\s*\S+\s*)+)\)(\s*[\|\)])/\1\2\4/g) {}
  while (s/([\(\|]\s*)\((\s*{([^}]*)}\s*$stdchar+\s*(?:\|\s*{\3}\s*$stdchar+\s*)+?)\)(\s*[\|\)])/\1\2\4/g) {}

  %best_choice_count = ();
  %best_choice = ();
  %best_choice_type = ();
  %seen_tokens = ();
  %ttc2tokens_content = ();
  %ttc2tokens = ();
  %seen = ();

  while (/{([^}]*)}\s*(_PERSON(?:_[mf])?|_LOCATION|_ORGANIZATION|_PRODUCT|_WORK|_COMPANY)(\d*)(?: *(\[\|(.*?)\|\]))?/g) {
    next if (defined($seen{"$1 ### $2"}) && $seen{"$1 ### $2"} == $3);
    $seen{"$1 ### $2"} = $3;
    $tokens = $1;
    $semlex = $4;
    $subdag = $5;
    $tokens_content = $tokens;
    $type = $2;
    $type_ext = $3;
    $tokens_content =~ s/<F.*?>//g;
    $tokens_content =~ s/<\/F>//g;
    if ($subdag ne "" && $tokens_content =~ /\b($subdag)\b/i) {
      $tokens_content = $1;
    }
    $tokens_content =~ s/ *_-_ */-/g;
    # print STDERR "$tokens_content\n";
    ### ROSA SAYS ?
    $is_safe = ($type_ext ne "");
    if ($is_safe) {$re = quotemeta($tokens); s/{$re}(\s*$type)\d+/{$tokens}$1/;}
    ### END
    $tokens_and_tokens_content = $tokens ." ### ". $tokens_content;
    $ttc2tokens_content{$tokens_and_tokens_content} = $tokens_content;
    $ttc2tokens{$tokens_and_tokens_content} = $tokens;
    $re = quotemeta ("{$tokens}")."\\s*".$type."\\s*".quotemeta($semlex);
    s/$re/{$tokens} $type/g;
    if ($tokens =~ /\"E\d+F1\"/) {$contains_first_token = 1} else {$contains_first_token = 0}
    my $tmp = "$tokens_content ### $type";
    if (!defined($cache_is_filled{$tmp})) {
      fill_cache ($tokens_content,$type,$tmp);
      if ($tokens_content =~ /'/) {
	my $tokens_content_2 = $tokens_content;
	if ($tokens_content_2 =~ s/' /'/g) {
	  fill_cache ($tokens_content_2,$type,$tmp);
	}
      }
      if ($tokens_content =~ /^M\. (.*)$/) {
	fill_cache ($1,$type,$tmp);
      }
      ### ROSA SAYS ? ON (lieutenant-)général
      if ($type =~ /^_PERSON/ && $tokens_content =~ /^(?:(?:Mr|Mll?e|Mme|Me|Pr|Dr)s?\.?|(?:lieutenant-)?général) (.*)$/) {
	fill_cache ($1,$type,$tmp);
      }
      ### END
      ### ROSA SAYS ? WHY NOT PUT THIS IN THE BNF? (AND CONTROL WHAT FOLLOWS?)
      elsif ($tokens_content =~ /^(?:ville|village|agglomération|cité|localité|district|commune|région|département) (?:du|de l[a']|de|d')(.*)$/i) {
	fill_cache ($1,$type,$tmp);
      } elsif ($tokens_content =~ /^(?:.*?'|[a-zé'].*?-)(.*)$/) {
	fill_cache ($1,$type,$tmp);
      }
    }
    if (!defined($cache_is_filled{$tmp})) { # && ($tmp =~ /^[A-Z­°²\´'ÂÇÈÊÉÎÏÖÜŸ -]+###/ || $tmp =~ /^[a-z­°²\´'àáâäçèéêëíîïñóôöùûüÿ -]+###/)
      $tmp2 = $tokens_content;
      $tmp2 =~ s/(^| )(.)([^ ]*)/$1.uc($2).lc($3)/ge;

      fill_cache ($tmp2,$type,$tmp);
      if ($tmp2 =~ /^M\. (.*)$/) {
	fill_cache ($1,$type,$tmp);
      }
      if ($type =~ /^_PERSON/ && $tmp2 =~ /^(?:(?:Mr|Mll?e|Mme|Me|Pr|Dr)s?\.?|(?:lieutenant-)?général) (.*)$/) {
	fill_cache ($1,$type,$tmp);
      } elsif ($tmp2 =~ /^(?:ville|village|agglomération|cité|localité|district|commune|région|département) (?:du|de l[a']|de|d')(.*)$/i) {
	fill_cache ($1,$type,$tmp);
      } elsif ($tmp2 =~ /^(?:.*?'|ex-)(.*)$/i) {
	fill_cache ($1,$type,$tmp);
      }
    }
    if (!defined($cache_is_filled{$tmp})) {
      $type =~ /^_(.*)$/;
      $tmpinfo = "$tokens_content\t\\(UNKNOWN $1\\)";
      $tmpinfo =~ s/\t/___/g;
      $tmpinfo =~ s/ /_/g;
      $cache{$tmp}{$tmpinfo}{weight} = 0.5;
      $cache{$tmp}{$tmpinfo}{id} = 0;
    }
#    $changed_best_choice = 0;
    if (!defined ($best_choice{$tokens_and_tokens_content})) {$best_choice{$tokens_and_tokens_content} = ""}
    for (keys %{$cache{$tmp}}) {
      my $local_weight = $cache{$tmp}{$_}{weight};
      if ($type eq "_LOCATION" && /Geoname_\d+_([A-Z]{2,})_/ && defined($country_context{$1})) {$local_weight += 0.001}
#      print STDERR "############ \$entity{$_} = $entity{$_} + $local_weight\n";
      if ($best_choice_count{$tokens_and_tokens_content} <= $entity{$_} + $local_weight) {
#	  print STDERR "is_dubious($tokens_content, $type, $_, $local_weight, $is_safe) = ".is_dubious($tokens_content, $type, $_, $local_weight, $is_safe)."\n";
	if ($entity{$_} == 0 && is_dubious($tokens_content, $type, $_, $local_weight, $is_safe)) {
	  next;
	}
	$best_choice{$tokens_and_tokens_content} = $_;
	$best_choice_count{$tokens_and_tokens_content} = $entity{$_} + $local_weight;
	$best_choice_type{$tokens_and_tokens_content} = $type;
	$best_choice_id{$tokens_and_tokens_content} = $cache{$tmp}{$_}{id};
#	$changed_best_choice = 1;
      }
    }
#    if ($changed_best_choice == 1) {
#      print STDERR "$best_choice_count{$tokens_and_tokens_content}\n";
#      print STDERR "\$entity{$best_choice{$tokens_and_tokens_content}} = $entity{$best_choice{$tokens_and_tokens_content}}\t$best_choice_count{$tokens_and_tokens_content}\n";
#    }
  }

  for $tokens_and_tokens_content (keys %best_choice) {
    $tokens_content = $ttc2tokens_content{$tokens_and_tokens_content};
    $tokens = $ttc2tokens{$tokens_and_tokens_content};
#    print STDERR "\$best_choice{$tokens_and_tokens_content} = $best_choice{$tokens_and_tokens_content}\n";
    if ($best_choice{$tokens_and_tokens_content} ne "") {
      my $type = $best_choice_type{$tokens_and_tokens_content};
      $suff = "";
      if ($best_choice{$tokens_and_tokens_content} =~ /\(UNKNOWN_/) {
	$suff = "__0"; # ça veut dire qu'on n'aime pas trop cette interprétation
      }
      $re = quotemeta($tokens);
      if ($oldstyle) {
	s/\((?:\s*{$re}\s*[\w\d_]+\s*\|\s*)*{$re}(\s*$type)\d*(?:__0)?\s*(?:\|\s*{$re}\s*[\w\d_]+\s*)*\)/{$tokens}$1$suff \[|$best_choice{$tokens_and_tokens_content}|] /g;#print "HERE1:\n$_\n";
	s/{$re}(\s*$type)\d*(?:__0)?($|[^ _]| [^\[]| \[[^\|])/{$tokens}$1$suff \[|$best_choice{$tokens_and_tokens_content}|] $2/g;#print "HERE2:\n$_\n$best_choice{$tokens_and_tokens_content}\n";
      } else {
	$best_choice{$tokens_and_tokens_content} =~ /^(.*?)___+/;
	my $canonical_name = $1;
	my $local_id = $best_choice_id{$tokens_and_tokens_content};
	s/\((?:\s*{$re}\s*[\w\d_]+\s*\|\s*)*{$re}(\s*$type)\d*(?:__0)?\s*(?:\|\s*{$re}\s*[\w\d_]+\s*)*\)/{$tokens}$1$suff \[|$local_id:$canonical_name|] /g;#print "HERE1:\n$_\n";
	s/{$re}(\s*$type)\d*(?:__0)?($|[^ _]| [^\[]| \[[^\|])/{$tokens}$1$suff \[|$local_id:$canonical_name|] $2/g;#print "HERE2:\n$_\n$best_choice{$tokens_and_tokens_content}\n";
	s/  +/ /g;
      }
    } else {
      my $tmp = $tokens_content;
      $tmp =~ s/ /_/g;
      $tmp =~ s/([+?()\[\]])/\\\1/g;
      $tmp .= "__0"; # ça veut dire qu'on n'aime pas trop cette interprétation
      $re = quotemeta($tokens);
      s/\(\s*{$re}(\s*)_[\w\d_]+\s*(?:\|\s*{$re}\s*[\w\d_]+\s*)*\)/{$tokens}$1$tmp /g;#print "HERE3\n";
      s/{$re}(\s*)_[\w\d_]+([^\w\d_:]|$)/{$tokens}$1$tmp $2/g;#print "HERE4\n";
    }
  }
  # on enlève les chemins passant par des __0 s'il y en a d'autres
  while (s/(<F id[^>]*>[^<]*)(?<=[^\\])([(|)])/\1\\\2/g){}
  s/(^|[^\\])([(|)])/\1 \2 /g;
  s/(^|[^\\])([(|)])/\1 \2 /g;
  s/ *\[ +\| +/ \[\|/g;
  s/ +\| +\] */\|\] /g;
  s/  +/ /g;
  $linear_path_elt = qr/(?:\[\||\|\]|[^(|)]|\\[(|)])/o;
  ### on vire d'abord ce qui est __0 et n'a pas de [|...|], pour garder un truc avec [|...|] s'il y en a mais qu'il n'y a que des __0
  while (s/ \| $linear_path_elt*?__0(?: [^\[(|)]$linear_path_elt*?)? \| / \| /g) {s/  +/ /g;}
  while (s/(^| \( )$linear_path_elt*?__0(?: [^\[(|)]$linear_path_elt*?)? \| /\1 /g) {s/  +/ /g;s/ \( ($linear_path_elt+?)(?: \)|$)/ \1 /g;}
  while (s/ \| $linear_path_elt*?__0(?: [^\[(|)]$linear_path_elt*?)?( \) |$)/\1 /g) {s/  +/ /g;s/ \( ($linear_path_elt+?)(?: \)|$)/ \1 /g;}
  ### on vire ce qui n'a pas de [|...|] s'il reste autre chose
  s/_EPSILON/_#EPSILON/g;
  while (s/ \| (?:(?:[^(|)]|\\[(|)])*_[A-Z](?:[^(|)]|\\[(|)])*) \| / \| /g) {s/  +/ /g;}
  while (s/(^| \( )(?:(?:[^(|)]|\\[(|)])*_[A-Z](?:[^(|)]|\\[(|)])*) \| /\1 /g) {s/  +/ /g;s/(?:^| \( )($linear_path_elt+?)(?: \)|$)/ \1 /g;}
  while (s/ \| (?:(?:[^(|)]|\\[(|)])*_[A-Z](?:[^(|)]|\\[(|)])*)( \) |$)/\1 /g) {s/  +/ /g;s/(?:^| \( )($linear_path_elt+?)(?: \)|$)/ \1 /g;}
  s/_#EPSILON/_EPSILON/g;
  ### on vire les __0 s'il reste autre chose
  while (s/ \| $linear_path_elt*?__0$linear_path_elt*? \| / \| /g) {s/  +/ /g;}
  while (s/(^| \( )$linear_path_elt*?__0$linear_path_elt*? \| /\1 /g) {s/  +/ /g;s/(?:^| \( )($linear_path_elt+?)(?: \)|$)/ \1 /g;}
  while (s/ \| $linear_path_elt*?__0$linear_path_elt*?( \) |$)/\1 /g) {s/  +/ /g;s/(?:^| \( )($linear_path_elt+?)(?: \)|$)/ \1 /g;}
  s/  +/ /g;
  s/__0 / /g;
  %seen = ();
  if ($oldstyle) {
    while (/{([^}]*)}\s*(_ADDRESS|_ADRESSE)(\d*)(?: *(\[\|(.*?)\|\]))?/g) {
      next if (defined($seen{"$1 ### $2"}));
      $seen{"$1 ### $2"} = 1;
      $tokens = $1;
      $type = $2;
      $tokens_content = $tokens;
      $tokens_content =~ s/<F.*?>//g;
      $tokens_content =~ s/<\/F>//g;
      $google_query = $tokens_content.", ";
      for (sort {$city_context{$b} <=> $city_context{$a}} keys %city_context) {
	$google_query .= "$_";
	last;
      }
      $google_query =~ s/ /+/g;
      $href = "http://maps.google.com/maps?q=$google_query&hl=fr";
      $re = quotemeta($tokens);
      s/{$re}\s*$type\d*(?: *(\[\|(.*?)\|\]))?/{$tokens} _LOCATION \[\|(address)____<a_href="$href">Google_maps<\/a>\|\]/g;
    }
  }
  while (/{([^}]*)}\s*(_PERSON(?:_[mf])?|_LOCATION|_ORGANIZATION|_PRODUCT|_WORK|_COMPANY)(\d*)(?: *(\[\|(.*?)\|\]))?/g) {
    next if (defined($seen{"$1 ### $2"}));
    $seen{"$1 ### $2"} = 1;
    $tokens = $1;
    $semlex = $4;
    $subdag = $5;
    $tokens_content = $tokens;
    $type = $2;
    $type_ext = $3;
    $tokens_content =~ s/<F.*?>//g;
    $tokens_content =~ s/<\/F>//g;
    $tokens_and_tokens_content = $tokens ." ### ". $tokens_content;
    $entity{$best_choice{$tokens_and_tokens_content}}+=$cache{$tokens_content." ### ".$type}{$best_choice{$tokens_and_tokens_content}}{weight};
#    print STDERR ">>>> \$entity{$best_choice{$tokens_and_tokens_content}} = $entity{$best_choice{$tokens_and_tokens_content}}\n";
    if ($type eq "_LOCATION") {
      if ($best_choice{$tokens_and_tokens_content} =~ /Geoname_\d+_([A-Z]{2,})_A\.PCLI/) {
	$country_context{$1}++;
	#      print STDERR "Spatial context: $1\n";
      } elsif ($best_choice{$tokens_and_tokens_content} =~ /Geoname_\d+_([A-Z]{2,})_A\.PCL/) {
	$city_context{$1}++;
      }
    }
    if ($best_choice{$tokens_and_tokens_content} =~ /\(UNKNOWN_(.*?)\)/) {
      fill_cache_subproc ($tokens_content, $best_choice{$tokens_and_tokens_content}, $type, "$tokens_content ### $type");
      $tokens_content =~ /(^| )([^ ]+)$/;
      my $ending = $2;
      unless (defined($new_entities_endings{$ending}) && defined($new_entities_endings{$ending}{$best_choice_type{$tokens_and_tokens_content}})) {
	$new_entities{"$tokens_content ### $type"} = $best_choice{$tokens_and_tokens_content};
	$new_entities_endings{$ending}{$type} = "$tokens_content ### $type";
      }
    }
  }
  print "$_\n";
}
$refs_sth->finish;
$refs_person_sth->finish;
$refs_person_m_sth->finish;
$refs_person_f_sth->finish;
$vars_sth->finish;
$lefff_sth->finish;
$first_names_sth->finish;
$lefff_locations_sth->finish;
undef $refs_sth;
undef $refs_person_sth;
undef $refs_person_m_sth;
undef $refs_person_f_sth;
undef $vars_sth;
undef $lefff_sth;
undef $first_names_sth;
undef $lefff_locations_sth;
$lefff_locations_dbh->disconnect;
$first_names_dbh->disconnect;
$lefff_dbh->disconnect;
$refs_dbh->disconnect;
$vars_dbh->disconnect;


sub fill_cache {
  my $tokens_content = shift;
  my $type = shift;
  my $key = shift;
  return if (defined($cache_is_filled{$key}));
  $vars_sth->execute($tokens_content);
  while (my $id = $vars_sth->fetchrow) {
    if ($type eq "_PERSON") {
      $refs_person_sth->execute(sprintf("%.0f", $id));
      while (my @t = $refs_person_sth->fetchrow_array) {
	$tmpinfo = Encode::decode("utf8",$t[0]."\t".$t[1]."\t".$t[2]."\t".$t[3]);
	fill_cache_subproc ($tokens_content, $tmpinfo, $type, $key, $id);
      }
      $refs_person_sth->finish;
    } elsif ($type eq "_PERSON_m") {
      $refs_person_m_sth->execute(sprintf("%.0f", $id));
      while (my @t = $refs_person_m_sth->fetchrow_array) {
	$tmpinfo = Encode::decode("utf8",$t[0]."\t".$t[1]."\t".$t[2]."\t".$t[3]);
	fill_cache_subproc ($tokens_content, $tmpinfo, $type, $key, $id);
      }
      $refs_person_m_sth->finish;
    } elsif ($type eq "_PERSON_f") {
      $refs_person_f_sth->execute(sprintf("%.0f", $id));
      while (my @t = $refs_person_f_sth->fetchrow_array) {
	$tmpinfo = Encode::decode("utf8",$t[0]."\t".$t[1]."\t".$t[2]."\t".$t[3]);
	fill_cache_subproc ($tokens_content, $tmpinfo, $type, $key, $id);
      }
      $refs_person_f_sth->finish;
    } else {
      $refs_sth->execute(sprintf("%.0f", $id),$type);
      while (my @t = $refs_sth->fetchrow_array) {
	$tmpinfo = Encode::decode("utf8",$t[0]."\t".$t[1]."\t".$t[2]."\t".$t[3]);
	fill_cache_subproc ($tokens_content, $tmpinfo, $type, $key, $id);
      }
      $refs_sth->finish;
    }
  }
  $vars_sth->finish;
  $tokens_content =~ /(^| )([^ ]+)$/;
  my $ending = $2;
  if (defined($new_entities_endings{$ending}) && defined($new_entities_endings{$ending}{$type})) {
    fill_cache_subproc ($tokens_content, $new_entities{$new_entities_endings{$ending}{$type}}, $type, $key);
  }
}

sub fill_cache_subproc {
  my $tokens_content = shift;
  my $tmpinfo = shift;
  my $type = shift;
  my $key = shift;
  my $id = shift;
  $tmpinfo =~ s/^(\d+)\t//;
  $weight = $1 || 1;
  $tmpinfo =~ s/([(|)])/\\\1/g;
  if ($weight > 0) {$weight = (log (1+$weight))/10 - log(1.9)}
  if ($tmpinfo =~ /\(UNKNOWN_/) {$weight +=0.5}
  if ($tmpinfo =~ /Geoname \d+ \w* P\./) {$weight += 0.05}
    elsif ($tmpinfo =~ /Geoname \d+ \w* A\./) {$weight += 0.04}
  $tmpinfo =~ s/\t/___/g;
  $tmpinfo =~ s/ /_/g;
  $weight = 1 + $weight unless /^<unknown>$/;
  if (($tokens_content !~ / / && $type =~ /^_PERSON/)
      || ($tokens_content =~ / / && $type eq "_LOCATION")) {
    $weight -=0.1;
  }
  if ($type eq "_ORGANIZATION") {$weight -= 0.05}
  $cache{$key}{$tmpinfo}{weight} = $weight;
  $cache_is_filled{$key} = 1;
  if ($key =~ /_PERSON$/) {
    $cache{$key."_f"}{$tmpinfo}{weight} = $weight-0.001;
    $cache{$key."_m"}{$tmpinfo}{weight} = $weight-0.001;
  }
  $cache{$key}{$tmpinfo}{id} = sprintf("%.0f", $id);
#  print STDERR "\$cache{$key}{$tmpinfo}{id} = ".sprintf("%.0f", $id).";\n";
}


sub is_dubious {
  my $tokens_content = shift;
  my $type = shift;
  my $def = shift;
  my $weight = shift;
  my $is_safe = shift;
  if ($is_safe) {return 0}
  if ($type =~ /^_PERSON/) {
    if ($tokens_content =~ /^([^ ]+) /) {
      fill_is_in_lefff ($1);
      if (defined($is_in_lefff_locations{$1}) && (!defined($is_in_lefff{$1}) || $is_in_lefff{$1} < 4)) {return 1}
      if (defined($is_in_lefff_locations{$1}) && $tokens_content =~ /^[^ ]+ [A-ZÂÇÈÊÉÎÏÖÜŸ][^ ]+ [A-ZÂÇÈÊÉÎÏÖÜŸ][^ ]+/) {return 1}
    }
    if ($tokens_content =~ /^M\. /) {return 1}
    my $mod_tokens_content = $tokens_content;
    $mod_tokens_content =~ s/^(La|Les?|Du|De [lL]a|Des) //;
    if ($mod_tokens_content =~ / / && $mod_tokens_content !~ /^[a-z]+ [^ ]+$/) {return 0}
    if ($weight > 0.9061 && $tokens_content !~ /^[a-z]+ [^ ]+$/) {return 0}
    fill_is_in_lefff ($tokens_content);
    if ($tokens_content =~ /^[a-z]+ ([^ ]+)$/) {fill_is_in_lefff ($1);}
    if (defined($is_in_lefff{$tokens_content}) >= 2) {return 1}
    elsif ($tokens_content =~ /^[a-z]+ ([^ ]+)$/ && defined($is_in_lefff{$1}) && $is_in_lefff{$1} >= 2) {return 1}
    $tokens_content_re = quotemeta ($tokens_content);
    if ($def =~ /^$tokens_content_re\_/) {return 1}
    $def =~ /^(.)(.*)$/;
    if (remove_diacritics ($1) ne $1) {
      $def =~ s/^(.)/remove_diacritics($1)/e;
      if ($def =~ /^$tokens_content_re\_/) {return 1}
    }
    $tokens_content =~ /^(.)(.*)$/;
    if (remove_diacritics ($1) ne $1) {
      $tokens_content =~ s/^(.)/remove_diacritics($1)/e;
      $tokens_content_re = quotemeta ($tokens_content);
      if ($def =~ /^$tokens_content_re\_/) {return 1}
    }
    if ($mod_tokens_content !~ / /) {return 1} # violent, on va tenter
  } elsif ($type =~ /_LOCATION/ || $type =~ /_ORGANIZATION/) {
    if ($tokens_content =~ / /) {return 0}
    if ($weight >= 1.6 && $type =~ /_LOCATION/) {return 0}
    if ($tokens_content =~ /^(?:Chirac)$/) {return 1}
#    if ($tokens_content =~ /^(?:[DLdl'])?(?:Paris)$/) {return 0}
    fill_is_in_lefff ($tokens_content);
    if ($is_in_lefff{$tokens_content} == 2) {return 1}
    if ($is_in_lefff{$tokens_content} >= 4) {return 1}
    if ($tokens_content =~ s/^[LDld]'//) {
      fill_is_in_lefff ($tokens_content);
      if ($is_in_lefff{$tokens_content} == 2) {return 1}
      if ($is_in_lefff{$tokens_content} >= 4) {return 1}
    }
  }
  return 0;
}

sub fill_is_in_lefff {
  my $tokens_content = shift;
  if (!defined($is_in_lefff{$tokens_content})) {
    $lefff_locations_sth->execute($tokens_content);
    if ($lefff_locations_sth->fetchrow) {
      $lefff_locations_sth->finish;
      $is_in_lefff_locations{$tokens_content} = 1;
    }
    $lefff_locations_sth->finish;
    $lefff_sth->execute($tokens_content);
    if ($lefff_info = $lefff_sth->fetchrow) {
      $is_in_lefff{$tokens_content} += 1;
    }
    $lefff_sth->finish;
    $lefff_sth->execute(lc($tokens_content));
    if ($lefff_sth->fetchrow) {
      $is_in_lefff{$tokens_content} += 2;
    }
    $lefff_sth->finish;
    $first_names_sth->execute(Encode::encode("utf8",$tokens_content));
    if ($first_names_sth->fetchrow) {
      $is_in_lefff{$tokens_content} += 4;
    }
    $first_names_sth->finish;
  }
}

sub is_new_doc {
  my $line = shift;
  if ($line =~ /<news_item/ || $line =~ /<item/){
    #print STDERR "Changement de contexte: $_\n"; 
    return 1;
  }
  return 0;
}

sub remove_diacritics {
  my $s = shift;
  $s =~ s/[áàâäąãăå]/a/g;
  $s =~ s/[ćčç]/c/g;
  $s =~ s/[ď]/d/g;
  $s =~ s/[éèêëęě]/e/g;
  $s =~ s/[ğ]/g/g;
  $s =~ s/[ìíîĩĭıï]/i/g;
  $s =~ s/[ĺľł]/l/g;
  $s =~ s/[ńñň]/n/g;
  $s =~ s/[òóôõöø]/o/g;
  $s =~ s/[ŕř]/r/g;
  $s =~ s/[śšş]/s/g;
  $s =~ s/[ťţ]/t/g;
  $s =~ s/[ùúûũüǔ]/u/g;
  $s =~ s/[ỳýŷÿ]/y/g;
  $s =~ s/[źẑżž]/z/g;
  $s =~ s/[ÁÀÂÄĄÃĂÅ]/A/g;
  $s =~ s/[ĆČÇ]/C/g;
  $s =~ s/[Ď]/D/g;
  $s =~ s/[ÉÈÊËĘĚ]/E/g;
  $s =~ s/[Ğ]/G/g;
  $s =~ s/[ÌÍÎĨĬİÏ]/I/g;
  $s =~ s/[ĹĽŁ]/L/g;
  $s =~ s/[ŃÑŇ]/N/g;
  $s =~ s/[ÒÓÔÕÖØ]/O/g;
  $s =~ s/[ŔŘ]/R/g;
  $s =~ s/[ŚŠŞ]/S/g;
  $s =~ s/[ŤŢ]/T/g;
  $s =~ s/[ÙÚÛŨÜǓ]/U/g;
  $s =~ s/[ỲÝŶŸ]/Y/g;
  $s =~ s/[ŹẐŻŽ]/Z/g;
  return $s;
}
