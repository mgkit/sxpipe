#!/usr/bin/env perl
use DBI;
use Encode;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

my $oldstyle = 0;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-d$/) {$dir=shift;}
    # oldstyle: print toutes les infos aleda de l'entité dans les [| |] - sans doute plus opérationnel - à virer
    elsif (/^-o$/) {$oldstyle = 1;}
    elsif (/^-l$/) {$lang=shift;}
}

#my $dbh = DBI->connect("dbi:SQLite:$dir/ne.dat", "", "", {RaiseError => 1, AutoCommit => 1});
#my $sth = $dbh->prepare('select value from data where key=?;');
my $vars_dbh = DBI->connect("dbi:SQLite:$dir/ne_vars.$lang.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $vars_sth = $vars_dbh->prepare('select key from data where variant=?;');
my $refs_dbh = DBI->connect("dbi:SQLite:$dir/ne_refs.$lang.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $refs_sth = $refs_dbh->prepare('select weight,name,def,link from data where key=? and type=?;');
my $refs_person_sth = $refs_dbh->prepare('select weight,name,def,link,type from data where key=? and (type="_PERSON" or type="_PERSON_m" or type="_PERSON_f");');
my $refs_person_m_sth = $refs_dbh->prepare('select weight,name,def,link,type from data where key=? and (type="_PERSON" or type="_PERSON_m");');
my $refs_person_f_sth = $refs_dbh->prepare('select weight,name,def,link,type from data where key=? and (type="_PERSON" or type="_PERSON_f");');
my $lefff_dbh;
if ($lang eq "en") {
  $lefff_dbh = DBI->connect("dbi:SQLite:$dir/enlex.dat", "", "", {RaiseError => 1, AutoCommit => 1});
} elsif ($lang eq "de") {
  $lefff_dbh = DBI->connect("dbi:SQLite:$dir/delex.dat", "", "", {RaiseError => 1, AutoCommit => 1});
} elsif ($lang eq "es") {
  $lefff_dbh = DBI->connect("dbi:SQLite:$dir/leffe.dat", "", "", {RaiseError => 1, AutoCommit => 1});
} else {
  $lefff_dbh = DBI->connect("dbi:SQLite:$dir/lefff.dat", "", "", {RaiseError => 1, AutoCommit => 1});
}
my $lefff_sth = $lefff_dbh->prepare('select value from data where key=?;');
my $first_names_dbh = DBI->connect("dbi:SQLite:$dir/first_names.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $first_names_sth = $first_names_dbh->prepare('select value from data where key=?;');
my $lefff_locations_dbh = DBI->connect("dbi:SQLite:$dir/lefff_locations.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $lefff_locations_sth = $lefff_locations_dbh->prepare('select value from data where key=?;');


my %is_in_lefff;
my %is_in_lefff_locations;

# %cache: pour une séquence de tokens, lors de l'accès à aleda, différents look ups sont faits ; ils sont mis en cache pour ne pas avoir à les refaire
# hash à 3 niveaux: $cache{$tokens ### $type}{$tmpinfo = infos aleda pour désambiguiser l'entité}{weight donné par le normalizer + id aleda}
# %cache_is_filled: pour savoir + vite si {$tokens ### $type} a déjà été vu => $cache_is_filled{$tokens ### $type} = 1 ou 0
# la sub fill_cache remplit %cache à partir d'aleda ; fill_cache_subproc : calcul du poids donné par normalizer
my (%cache, %cache_is_filled);

# %entity: même clé primaire que %cache ; stocke le poids dynamique (grossit à chaque fois qu'on rencontre l'entité) => $entity{$tokens ### $type} = poids
my %entity;

# $contains_first_token dit si la mention est le début de la phrase
my $contains_first_token = 0;
# setting du géo-contexte
$country_context{FR} = 1;
$city_context{Paris} = 1;

#ceci...
# pattern : car non std précédé d'un \
my $D2PDescapednonstdchar = qr/(?<=\\)[\(\|\)]/;
# car std = tout sauf (, ), | non précédé de \
my $D2PDstdchar = qr/(?:[^\(\|\)]|$escapednonstdchar)/;
#... est remplacé par une copie de dag2pdag, dont le premier bout est ceci...
my $D2PDcomment = qr/\{(?:\\}|[^}])*?\}/;
my $D2PDform = qr/(?:\\[()|\\]|[^ \t\n{}()|])+/;
my $D2PDsemlex = qr/(?:\[\|(?:\|[^\]]|[^\|])*?\|\])/;
my $D2PDcform = qr/$D2PDcomment\s*$D2PDform(?:\s*$D2PDsemlex)?/;
my $D2PDcform_star = qr/(?:$D2PDcform\s*)*/;
my $D2PDparento = qr/(?<!\\)\(/;
my $D2PDparentf = qr/(?<!\\)\)/;
my $D2PDquestionmark = qr/(?<!\\)\?/;
my $D2PDpipe = qr/(?<![\\\[])\|(?!\])/;
my $D2PDparents = qr/$D2PDparento\s*(?:$D2PDpipe\s*|$D2PDcform\s*)*$D2PDparentf/;
my $D2PDcform_or_parents_star = qr/(?:$D2PDcform\s*|$D2PDparents\s*)*/;

my $D2PDcopy;
### ...fin du premier bout de dag2pdag

while (<>) {
  chomp;

#  print STDERR "="x40 ."\n";
  # si on est au début d'un doc, on remet à 0 tous les poids
  if (is_new_doc($_)) {
    # pour chaque entité
    for (keys %entity) {
      # poids divisé par 2 (plus utilisé)
      if (0) {
	$entity{$_} /= 2;
	# poids à 0
      } else {
	$entity{$_} = 0;
	# %new_entities_endings : pendant de %entities ; stocke le dernier token de l'entité et pointe sur l'entité dans %new_entities => regroupe en paquet les entités finissant par le même token
	# %new_entities_endings{Dupont}{PERSON} = "Jean Dupont ### PERSON" (soit la clé de %new_entities)
	%new_entities_endings = ();
	%new_entities = ();
      }
    }
    %country_context = ();
    $country_context{FR} = 1;
    %city_context = ();
    $city_context{Paris} = 1;
    %temporal_context = ();
  }

  $output = "";

  my $save = $_;

  eval { # timeout proposé par EVDLC
    local $SIG{ALRM} = sub { die "timeout\n" };
    alarm 100;
  for (split /(?<=_SENT_BOUND)/, $_) {

    # supprime les variantes d'entités (_PERSON/x -> _PERSON, par exemple)
    s/({[^}]*}\s*(?:_PERSON(?:_[mf])?|_LOCATION|_ORGANIZATION|_PRODUCT|_WORK|_COMPANY)\d*)\/[^ \(\)\?\|\[\]]*/\1/g;
    
    # tente de supprimer la récursion des dags ; par ex. ((a(b|c)d)|e) => (abd | acd | e)
    # ceci...
    # while (s/([\(\|]\s*)\((\s*{([^}]*)}\s*$stdchar+\s*(?:\|\s*{\3}\s*$stdchar+\s*)+?)\)(\s*[\|\)])/\1\2\4/g) {}
    # ... est remplacé par le corps de dag2pdag, que voici:
    while (s/({(?:\\}|[^}])*)\|/\1_#_PIPE/g) {}
    s/\{((?:\\}|[^}])*)(<F [^<>]*>(?:\\}|[^}])*?<\/F>)\s*\}\s*($D2PDform(?:\s*$D2PDsemlex)?)\s*\{\s*\2\s*\}\s*($D2PDform(?:\s*$D2PDsemlex)?)\s*$D2PDquestionmark/({$1$2} $3 | {$1} $3 {$2} $4)/g;
    s/\{\s*((?:\\}|[^}])*?)\s*\}\s*($D2PDform(?:\s*$D2PDsemlex)?)\s*$D2PDquestionmark\s*\{\1((?:\\}|[^}])*)\s*\}\s*($D2PDform(?:\s*$D2PDsemlex)?)/({$1} $2 {$3} $4 | {$1$3} $4)/g;
    s/($D2PDcform)$D2PDquestionmark/(|$1)/g;
    $D2PDcopy = $_;
    while ($D2PDcopy =~ s/$D2PDparento\s*(?:$D2PDpipe\s*|$D2PDcform\s*)*\s*$D2PDparentf//) {}
    $_ = "( $_ )" if ($D2PDcopy =~ /$D2PDpipe/ && $D2PDcopy !~ /$D2PDparento/);
    while (s/($D2PDparento|$D2PDpipe)(\s*$D2PDcform_star)$D2PDparento\s*($D2PDcform_star)$D2PDpipe\s*((?:$D2PDpipe\s*|$D2PDcform\s*)*)$D2PDparentf\s*($D2PDcform_or_parents_star)($D2PDparentf|$D2PDpipe)/\1\2 \3 \5 | \2 ( \4 ) \5\6/) {
      s/$D2PDparento\s*($D2PDcform_star)$D2PDparentf/ \1 /g;
      s/  +/ /g;
      if (s/($D2PDparento\s*(?:$D2PDcform_star\s*$D2PDpipe\s*){16,}(?:$D2PDcform_star\s*))(?:$D2PDpipe\s*(?:$D2PDcform_or_parents_star|$D2PDpipe)\s*)*($D2PDparentf)/$1 $2/) {
	s/  +/ /g;
	/^.*?id="(E\d+)F/;
	print STDERR "### WARNING (np_normalizer): sentence $1 had very complex embedded recursions, some paths had to be removed\n";
      }

    }
    while (s/($D2PDparento|$D2PDpipe)\s*($D2PDcform_star(?:\s*$D2PDpipe\s*$D2PDcform_star)*)\s*$D2PDpipe\s*\2\s*($D2PDparentf|$D2PDpipe)/$1 $2 $3/g) {
      s/$D2PDparento\s*($D2PDcform_star)$D2PDparentf/ \1 /g;
      s/  +/ /g;
    }
    s/$D2PDparento\s*($D2PDcform_star)$D2PDparentf/ \1 /g;
    s/  +/ /g;
    s/_#_PIPE/\|/g;
    # fin du corps de dag2pdag

    # suppression des fausses alternatives --- i.e. (a | b | a) => (a | b)
    $new = "";
    while (s/^(.*? )\((.*?[^\\])\)//) {
      $new .= $1;
      %hash = ();
      for (split (/ \| /, $2)) {
	$hash{$_}=1;
      }
      $new .= "(".(join " | ", sort keys %hash).")";
    }
    $new .= $_;
    $_ = $new;

    # on a clé = $tokens_content ### $type
    # $tokens_content = contenu des commentaires-tokens sans accolades ni xml => $tc
    # $tokens = contenu des commentaires-tokens sans accolades => $t
    # $tokens_and_tokens_content = concaténation de $tokens et $tokens_content => $ttc
    
    # %best_choice, %best_choice_count et %best_choice_type : pour chaque mention = $ttc on cherche la meilleure clé, on stocke la clé dans %best_choice, le poids dans %best_choice_count et le type dans %best_choice_type
    %best_choice_count = ();
    %best_choice = ();
    %best_choice_type = ();
    %best_choice_id = ();
    # pas utilisé
    %seen_tokens = ();
    # transparents
    %ttc2tokens_content = ();
    %ttc2tokens = ();
    # %seen stocke les clés déjà vues, sous forme de {$tokens ### $type}, donc avec les mêmes tokens+ids (et non clé normale {$tokens_content ### $type}) (dans les ambiguïités)
    %seen = ();
    
    $first_token_does_not_count = 0;
    if (/<F id=\"E\d+F1\">(?:[\"-])<\/F>/) {
      $first_token_does_not_count = 1;
    }
    
    while (/{([^}]*)}\s*(_PERSON(?:_[mf])?|_LOCATION|_ORGANIZATION|_PRODUCT|_WORK|_COMPANY)(\d*)(?: *(\[\|(.*?)\|\]))?/g) {
      # si on a déjà vu ces {$tokens ### $type}, on passe
      next if (defined($seen{"$1 ### $2"}) && $seen{"$1 ### $2"} == $3);
      # sinon, on les stocke
      $seen{"$1 ### $2"} = $3;
      # (<F>token</F>)+
      $left_context = $`;
      $tokens = $1;
      #################################################################
      # dans [| |] - à oublier (pas encore là)
      $semlex = $4;
      $subdag = $5;
      #################################################################
      $tokens_content = $tokens;
      $type = $2;
      $type_ext = $3;
      $tokens_content =~ s/<F.*?>//g;
      $tokens_content =~ s/<\/F>//g;
      $left_context_without_comments = $left_context;
      $left_context_without_comments =~ s/{.*?} //g;
      #################################################################
      # à oublier
      if ($subdag ne "" && $tokens_content =~ /\b($subdag)\b/i) {
	$tokens_content = $1;
      }
      #################################################################
      # _-_ = tiret interne donc devient - (sans blanc autour) pour refabriquer les tokens
      $tokens_content =~ s/ *_-_ */-/g;
      $tokens_content =~ s/ , /, /g;
      # print STDERR "$tokens_content\n";
      # les entités aux types Person2 etc sont considérés comme + safe
      $is_safe = ($type_ext ne "");
      # dans ce cas, on enlève le "2" du type
      if ($is_safe) {
	$re = quotemeta($tokens); s/{$re}(\s*$type)\d+/{$tokens}$1/;
      }
      # fabrique du $tokens_and_tokens_content
      $tokens_and_tokens_content = $tokens ." ### ". $tokens_content;
      # remplissages
      $ttc2tokens_content{$tokens_and_tokens_content} = $tokens_content;
      $ttc2tokens{$tokens_and_tokens_content} = $tokens;
      # normalisation de la phrase : on vire les semlex ([| |] mais y en plus) ; garde un seul blanc entre accolade et $type
      $re = quotemeta ("{$tokens}")."\\s*".$type."([^ ]*)\\s*".quotemeta($semlex)."\\s*";
      s/$re/{$tokens} $type\1 /g;
      # variable dont on ne se sert pas/plus/pas encore : mention est le début de la phrase 
      if ($tokens =~ /\"E\d+F1\"/) {
	$contains_first_token = 1;
      } elsif ($left_context =~ /(_SENT_BOUND|_META_TEXTUAL_\S+|\.\\\)|\.|!|\?)\s*(?:\(\s*)?$/) {
	$contains_first_token = 1;
      } elsif ($left_context_without_comments =~ /^[^\"]*(?:\"[^\"]+?\"[^\"]+?)*\"\s*(?:\(\s*)?$/) {
	$contains_first_token = 1;
      } elsif ($tokens =~ /\"E\d+F2\"/ && $first_token_does_not_count == 1) {
	$contains_first_token = 1;
      } else {
	$contains_first_token = 0
      }
      # clé dans $tmp avec {$tokens_content ### $type}
      my $tmp = "$tokens_content ### $type";
      # Look up dans aleda avec cette clé
      # sauf si déjà look upé
      if (!defined($cache_is_filled{$tmp})) {
	# look up 1 : avec tokens, type, clé tels quels
	# fill_cache prend de aleda les variantes == $tokens_content ayant comme type $type (modulo _m et _f) et associe leurs entités à $tmp (la clé) dans %cache
	fill_cache ($tokens_content,$type,$tmp);
	# si $tc contient ', on recommence le look up sans les espaces avant et après les '
	if ($tokens_content =~ /'/) {
	  my $tokens_content_2 = $tokens_content;
	  if ($tokens_content_2 =~ s/' /'/g) {
	    fill_cache ($tokens_content_2,$type,$tmp);
	  }
	  if ($lang eq "en") {
	    $tokens_content_2 = $tokens_content;
	    if ($tokens_content_2 =~ s/'s$//g) {
	      fill_cache ($tokens_content_2,$type,$tmp);
	    }
	  }
	}
	if ($tokens_content =~ /\+/) {
	  my $tokens_content_2 = $tokens_content;
	  if ($tokens_content_2 =~ s/\\?\+/Plus/g) {
	    fill_cache ($tokens_content_2,$type,$tmp);
	  }
	}
	##################################################################################################################
	# ICI : ces actions ne devraient pas avoir à être faites à ce niveau, le module np ne devrait pas inclure ces éléments dans la variante de façon non ambigüe (et jamais pour 
	# des choses comme lieutenant-général)
	# si $tc commence par M. on refait le look up sur $tc sans M. initial
	if ($tokens_content =~ /^M\. (.*)$/) {
	  fill_cache ($1,$type,$tmp);
	}
	# idem avec ces trucs
	if ($type =~ /^_PERSON/ && $tokens_content =~ /^(?:(?:Mr|Mll?e|Mme|Me|Pr|Dr)s?\.?|(?:lieutenant-)?général) (.*)$/) {
	  fill_cache ($1,$type,$tmp);
	}
	# idem
	elsif ($tokens_content =~ /^(?:ville|village|agglomération|cité|localité|district|commune|région|département) (?:du |de la| de l'|de |d')(.*)$/i) {
	  fill_cache ($1,$type,$tmp);
	  ##################################################################################################################
	  # en cas de mauvaise tokenization sur - et ' : on fait un look up en enlevant tout avant le premier - ou la première ' (e.g. anti-Bush)
	} elsif ($tokens_content =~ /^(?:.*?'|[a-zé'].*?-)(.*)$/) {
	  fill_cache ($1,$type,$tmp);
	}
      }
      # si on n'a toujours rien réussi à filler avec cette clé

      if (!defined($cache_is_filled{$tmp})) { # && ($tmp =~ /^[A-Z­°²\´'ÂÇÈÊÉÎÏÖÜŸ -]+###/ || $tmp =~ /^[a-z­°²\´'àáâäçèéêëíîïñóôöùûüÿ -]+###/)
	$tmp2 = $tokens_content;
	# on essaie en mettant une maj au 1er mot du $tokens_content, on minusculise le reste de ce premier mot => attention si tout était en MAJ, ça devient Maj
	$tmp2 =~ s/(^| )(.)([^ ]*)/$1.uc($2).lc($3)/ge;
	fill_cache ($tmp2,$type,$tmp);
	# on refait comme au-dessus avec cette clé capitalisée
	if ($tmp2 =~ /^M\. (.*)$/) {
	  fill_cache ($1,$type,$tmp);
	}
	if ($lang eq "en") {
	  $tmp3 = $tmp2;
	  if ($tmp3 =~ s/'s$//g) {
	    fill_cache ($tmp3,$type,$tmp);
	  }
	}
	if ($type =~ /^_PERSON/ && $tmp2 =~ /^(?:(?:Mr|Mll?e|Mme|Me|Pr|Dr)s?\.?|(?:lieutenant-)?général) (.*)$/) {
	  fill_cache ($1,$type,$tmp);
	} elsif ($tmp2 =~ /^(?:ville|village|agglomération|cité|localité|district|commune|région|département) (?:du|de l[a']|de|d')(.*)$/i) {
	  fill_cache ($1,$type,$tmp);
	  # } elsif ($tmp2 =~ /^(?:.*?'|ex-)(.*)$/i) {
	} elsif ($tokens_content =~ /^(?:.*?'|[a-zé'].*?-)(.*)$/) {
	  fill_cache ($1,$type,$tmp);
	}
      }
      # si toujours rien de fillé, on déclare UNK - on fabrique des infos spéciales : $cache{$tokens_content ### $type}{$tokens_content Unk $type}
      if (!defined($cache_is_filled{$tmp})) {
	$type =~ /^_(.*)$/;
	$tmpinfo = "$tokens_content\t\\(UNKNOWN $1\\)";
	$tmpinfo =~ s/\d*(?:_[mf])?\\\)$/\\\)/;
	$tmpinfo =~ s/\t/___/g;
	$tmpinfo =~ s/ /_/g;
	# remplissage hard codé des poids (0.4 = faible ; id = 0 = UNK)
	$cache{$tmp}{$tmpinfo}{weight} = 0.4;
	$cache{$tmp}{$tmpinfo}{id} = 0;
	if ($type =~ /_PERSON/ && $tmp =~ /^(.).*? (.*)$/) {
	  $cache{$2}{$tmpinfo}{weight} = 0.4;
	  $cache{$2}{$tmpinfo}{id} = 0;	
	  $cache{"$1. $2"}{$tmpinfo}{weight} = 0.4;
	  $cache{"$1. $2"}{$tmpinfo}{id} = 0;	
	}
      }
      # à ce stade, %best_choice etc ne sont pas remplis
      # la clé est toujours $tmp
      # on est sur une mention, on regarde la meilleure entité de %cache{$tmp} pour cette mention
      $changed_best_choice = 0;
      # on ne recommence pas si on l'a déjà vue dans un autre chemin
      if (!defined ($best_choice{$tokens_and_tokens_content})) {
	$best_choice{$tokens_and_tokens_content} = "";
      }
      # boucle sur les entités de %cache pour cette mention = une transition
      for $e (keys %{$cache{$tmp}}) {
	# on récupère le poids aleda
	my $local_weight = $cache{$tmp}{$e}{weight};
	# on favorise légèrement les noms de lieux qui sont dans le/les pays de %country_context
	if ($type eq "_LOCATION" && /Geoname_\d+_([A-Z]{2,})_/ && defined($country_context{$1})) {
	  $local_weight += 0.001;
	}
	# print STDERR "############ \$entity{$e} = $entity{$e} + $local_weight\n";
	# si le meilleur choix à ce stade a un poids inférieur au poids dynamique de l'entité en cours + le poids aleda (local weight),
	if ($best_choice_count{$tokens_and_tokens_content} <= $entity{$e} + $local_weight + 0.5*$is_safe) {
	  $is_dubious = is_dubious($tokens_content, $type, $e, $local_weight, $is_safe);
	  # print STDERR "is_dubious($tokens_content, $type, $e, $local_weight, $is_safe) = $is_dubious\n";
	  # on vérifie avec is_dubious si l'entité courante a une mauvaise tête : nom de personne avec seulement nom de famille etc...
	  # si elle a une mauvaise tête et qu'on ne l'a jamais vue, cette entité n'est pas la meilleure
	  if ($entity{$e} == 0 && $is_dubious == 1) {
	    next;
	  } elsif ($is_dubious == 2) {
	    next;
	  }
	  # sinon l'entité courante est la meilleure jusqu'à maintenant
	  # on met à jour pour cette clé les meilleurs trucs (aleda_infos, poids, type, id aleda)
	  # la clé des %bc n'a pas le type : il donne la meilleure entité tous types confondus parmi ceux présents dans le dag
	  $best_choice{$tokens_and_tokens_content} = $e;
	  $best_choice_count{$tokens_and_tokens_content} = $entity{$e} + $local_weight + 0.5*$is_safe;
	  $best_choice_type{$tokens_and_tokens_content} = $type;
	  $best_choice_id{$tokens_and_tokens_content} = $cache{$tmp}{$e}{id};
	  $changed_best_choice = 1;
	}
      }
      # on a dans %best_choice etc la meilleure entité pour cette transition
      # if ($changed_best_choice == 1) {
      #   print STDERR "\t$tokens_and_tokens_content\t\$entity{$best_choice{$tokens_and_tokens_content}} = $entity{$best_choice{$tokens_and_tokens_content}}\t$best_choice_count{$tokens_and_tokens_content}\t$best_choice_type{$tokens_and_tokens_content}\n";
      # }
    }
    # fin du while sur chaque transition de la phrase possiblement EN

    # pour chaque clé traitée
    # tout ce bloc supprime les ambiguïtés de transitions avec mêmes bornes
    for $tokens_and_tokens_content (keys %best_choice) {
      $tokens_content = $ttc2tokens_content{$tokens_and_tokens_content};
      $tokens = $ttc2tokens{$tokens_and_tokens_content};
      #     print STDERR "\$best_choice{$tokens_and_tokens_content} = $best_choice{$tokens_and_tokens_content}\n";
      # si un best_choice est défini
      if ($best_choice{$tokens_and_tokens_content} ne "") {
	# on récupère le type du best_choice
	my $type = $best_choice_type{$tokens_and_tokens_content};
	$suff = "";
	# si l'entité est UNK, on ajoute __0 au chemin pour le défavoriser ensuite
	if ($best_choice{$tokens_and_tokens_content} =~ /\(UNKNOWN_/ && (defined($entity{$best_choice{$tokens_and_tokens_content}}) || $entity{$best_choice{$tokens_and_tokens_content}} == 0)) {
	  $suff = "__0"; # ça veut dire qu'on n'aime pas trop cette interprétation
	}
	$re = quotemeta($tokens);
	# on cherche des ambiguïtés de transitions
	# on les remplace par {tokens} TYPE [|...|] (e.g. {orange} LOC | {orange} COM => {orange} LOC en fonction de ce qu'il y a dans %bc
	if ($oldstyle) {
	  s/\((?:\s*{$re}\s*[\w\d_]+\s*\|\s*)*{$re}(\s*$type)\d*(?:__0)?\s*(?:\|\s*{$re}\s*[\w\d_]+\s*)*\)/{$tokens}$1$suff \[|$best_choice{$tokens_and_tokens_content}|] /g; #print "HERE1:\n$_\n";
	  s/{$re}(\s*$type)\d*(?:__0)?([^ _]| [^\[]| \[[^\|]| *$)/{$tokens}$1$suff \[|$best_choice{$tokens_and_tokens_content}|] $2/g; #print "HERE2:\n$_\n$best_choice{$tokens_and_tokens_content}\n";
	} else {
	  $best_choice{$tokens_and_tokens_content} =~ /^(.*?)___+/;
	  my $canonical_name = $1;
	  my $local_id = $best_choice_id{$tokens_and_tokens_content};
	  s/\((?:\s*{$re}\s*[\w\d_]+\s*\|\s*)*{$re}(\s*$type)\d*(?:__0)?\s*(?:\|\s*{$re}\s*[\w\d_]+\s*)*\)/{$tokens}$1$suff \[|$local_id:$canonical_name|] /g; #print "HERE1:\n$_\n";
	  s/{$re}(\s*$type)\d*(?:__0)?([^ _]| [^\[]| \[[^\|]| *$)/{$tokens}$1$suff \[|$local_id:$canonical_name|] $2/g;	#print "HERE2:\n$_\n$best_choice{$tokens_and_tokens_content}\n";
	  s/  +/ /g;
	}
      } else {
	# un bc peut ne pas être défini (cf. if ($entity{$_} == 0 && is_dubious($tokens_content, $type, $_, $local_weight, $is_safe)))
	# dans ce cas on lui colle aussi un __0 (comme les bc qui sont UNK)
	my $tmp = $tokens_content;
	$tmp =~ s/ /_/g;
	if ($tmp !~ / /) {
	  $tmp =~ /^(.)(.*)$/;
	  if (lc($1) ne $1 && lc($2) eq $2) {
	    $tmp = lc($1).$2;
	  }
	}
	$tmp =~ s/([+?()\[\]])/\\\1/g;
	$tmp .= "__0"; # ça veut dire qu'on n'aime pas trop cette interprétation
	$re = quotemeta($tokens);
	s/\(\s*{$re}(\s*)_[\w\d_]+\s*(?:\|\s*{$re}\s*[\w\d_]+\s*)*\)/{$tokens}$1$tmp /g; #print "HERE3\n";
	s/{$re}(\s*)_[\w\d_]+([^\w\d_:]|$)/{$tokens}$1$tmp $2/g; #print "HERE4\n";
      }
    }
    # à ce stade on ne peut plus avoir deux transitions de mêmes bornes (on peut avoir (a | (b c)) mais pas (a|b)
    # on peut avoir (PERS [|...|] | Jacques + PERS [|...|]), ou ((LOC (PERS [|...|] | Jacques + PERS [|...|])) | PERS __0 [|... unk|])
    # on peut donc maintenant choisir les bons chemins en fonction des transitions
    # on enlève les chemins passant par des __0 s'il y en a d'autres
    ####################################################################################################################################################
    # nettoyage
    while (s/(<F id[^>]*>[^<]*)(?<=[^\\])([(|)])/\1\\\2/g){}
    while (s/(\[\|[^\|]+)(?<=[^\\])([(|)])(?!\])/\1\\\2/g){}
    s/(^|[^\\])([(|)])/\1 \2 /g;
    s/(^|[^\\])([(|)])/\1 \2 /g;
    s/ *\[ +\| +/ \[\|/g;
    s/ +\| +\] */\|\] /g;
    s/  +/ /g;
    $linear_path_elt = qr/(?:\[\||\|\]|[^(|)]|\\[(|)])/o;
    # on vire les chemins avec __0 et sans [|...|] s'il reste des chemins, même avec (tous) des __0 mais qui ont des [|...|]
    # sinon on ne vire pas
    while (s/ \| $linear_path_elt*?__0(?: [^\[(|)]$linear_path_elt*?)? \| / \| /g) {s/  +/ /g;}
    while (s/(^| \( )$linear_path_elt*?__0(?: [^\[(|)]$linear_path_elt*?)? \| /\1 /g) {s/  +/ /g;s/ \( ($linear_path_elt+?)(?: \)|$)/ \1 /g;}
    while (s/ \| $linear_path_elt*?__0(?: [^\[(|)]$linear_path_elt*?)?( \) |$)/\1 /g) {s/  +/ /g;s/ \( ($linear_path_elt+?)(?: \)|$)/ \1 /g;}
    ### on vire ce qui n'a pas de [|...|] s'il reste autre chose
    s/_EPSILON/_#EPSILON/g;
    # on ne veut pas traiter les _NUMBER !!!
    s/_NUMBER/__NUMBER/g;
    while (s/ \| (?:$linear_path_elt*? )?\{(?:[^(|)]|\\[(|)])*?} *_[A-Z](?:[^(|)]|\\[(|)])*(?: [^\[(|)]$linear_path_elt*)? \| / \| /g) {s/  +/ /g;}
    while (s/(^| \( )(?:$linear_path_elt*? )?\{(?:[^(|)]|\\[(|)])*?} *_[A-Z](?:[^(|)]|\\[(|)])*(?: [^\[(|)]$linear_path_elt*)? \| /\1 /g) {s/  +/ /g;s/(?:^| \( )($linear_path_elt+?)(?: \)|$)/ \1 /g;}
    while (s/ \| (?:$linear_path_elt*? )?\{(?:[^(|)]|\\[(|)])*?} *_[A-Z](?:[^(|)]|\\[(|)])*(?: [^\[(|)]$linear_path_elt*)?( \) |$)/\1 /g) {s/  +/ /g;s/(?:^| \( )($linear_path_elt+?)(?: \)|$)/ \1 /g;}
    s/__NUMBER/_NUMBER/g;
    s/_#EPSILON/_EPSILON/g;
    ### on vire les __0 s'il reste autre chose
    while (s/ \| $linear_path_elt*?__0$linear_path_elt*? \| / \| /g) {s/  +/ /g;}
    while (s/(^| \( )$linear_path_elt*?__0$linear_path_elt*? \| /\1 /g) {s/  +/ /g;s/(?:^| \( )($linear_path_elt+?)(?: \)|$)/ \1 /g;}
    while (s/ \| $linear_path_elt*?__0$linear_path_elt*?( \) |$)/\1 /g) {s/  +/ /g;s/(?:^| \( )($linear_path_elt+?)(?: \)|$)/ \1 /g;}
    s/  +/ /g;
    s/__0 / /g;
    %seen = ();
    if ($oldstyle) {
      while (/{([^}]*)}\s*(_ADDRESS|_ADRESSE)(\d*)(?: *(\[\|(.*?)\|\]))?/g) {
	next if (defined($seen{"$1 ### $2"}));
	$seen{"$1 ### $2"} = 1;
	$tokens = $1;
	$type = $2;
	$tokens_content = $tokens;
	$tokens_content =~ s/<F.*?>//g;
	$tokens_content =~ s/<\/F>//g;
	$tokens_content =~ s/ *_-_ */-/g;
	$google_query = $tokens_content.", ";
	for (sort {$city_context{$b} <=> $city_context{$a}} keys %city_context) {
	  $google_query .= "$_";
	  last;
	}
	$google_query =~ s/ /+/g;
	$href = "http://maps.google.com/maps?q=$google_query&hl=fr";
	$re = quotemeta($tokens);
	s/{$re}\s*$type\d*(?: *(\[\|(.*?)\|\]))?/{$tokens} _LOCATION \[\|(address)____<a_href="$href">Google_maps<\/a>\|\]/g;
      }
    }
    while (/{([^}]*)}\s*(_PERSON(?:_[mf])?|_LOCATION|_ORGANIZATION|_PRODUCT|_WORK|_COMPANY)(\d*)(?: *(\[\|(.*?)\|\]))?/g) {
      next if (defined($seen{"$1 ### $2"}));
      $seen{"$1 ### $2"} = 1;
      $tokens = $1;
      $semlex = $4;
      $subdag = $5;
      $tokens_content = $tokens;
      $type = $2;
      $type_ext = $3;
      $tokens_content =~ s/<F.*?>//g;
      $tokens_content =~ s/<\/F>//g;
      $tokens_content =~ s/ *_-_ */-/g;
      $tokens_and_tokens_content = $tokens ." ### ". $tokens_content;
      $entity{$best_choice{$tokens_and_tokens_content}}+=$cache{$tokens_content." ### ".$type}{$best_choice{$tokens_and_tokens_content}}{weight};
      # print STDERR ">>>> \$entity{$best_choice{$tokens_and_tokens_content}} = $entity{$best_choice{$tokens_and_tokens_content}}\n";
      if ($type eq "_LOCATION") {
	if ($best_choice{$tokens_and_tokens_content} =~ /Geoname_\d+_([A-Z]{2,})_A\.PCLI/) {
	  $country_context{$1}++;
	  #      print STDERR "Spatial context: $1\n";
	} elsif ($best_choice{$tokens_and_tokens_content} =~ /Geoname_\d+_([A-Z]{2,})_A\.PCL/) {
	  $city_context{$1}++;
	}
      }
      if ($best_choice{$tokens_and_tokens_content} =~ /\(UNKNOWN_(.*?)\)/) {
	fill_cache_subproc ($tokens_content, $best_choice{$tokens_and_tokens_content}, $type, "$tokens_content ### $type");
	$tokens_content =~ /(^| )([^ ]+)$/;
	my $ending = $2;
	$best_choice_type{$tokens_and_tokens_content} =~ s/^_PERSON\d*(?:_[mf])?$/_PERSON/;
	unless (defined($new_entities_endings{$ending}) && defined($new_entities_endings{$ending}{$best_choice_type{$tokens_and_tokens_content}})) {
	  $type =~ s/^_PERSON\d*(?:_[mf])?$/_PERSON/;
	  $new_entities{"$tokens_content ### $type"} = $best_choice{$tokens_and_tokens_content};
	  $new_entities_endings{$ending}{$type} = "$tokens_content ### $type";
	}
      }
    }
    $output =~ s/^ +//;
    $output .= " " unless ($output eq "");
    $output .= $_;
  }
  print "$output\n";
    alarm 0;
  };
  if ($@) {
    $@ eq "timeout\n" or die;
    $save =~ /^.*?id="(E\d+)F/;
    print STDERR "### WARNING (np_normalizer): reached timeout on sentence $1 ";
    $save =~ /^.*id="E\d+F(\d+)/;
    print STDERR "(length $1): discard changes\n";
    print "$save\n";
  }
}
$refs_sth->finish;
$refs_person_sth->finish;
$refs_person_m_sth->finish;
$refs_person_f_sth->finish;
$vars_sth->finish;
$lefff_sth->finish;
$first_names_sth->finish;
$lefff_locations_sth->finish;
undef $refs_sth;
undef $refs_person_sth;
undef $refs_person_m_sth;
undef $refs_person_f_sth;
undef $vars_sth;
undef $lefff_sth;
undef $first_names_sth;
undef $lefff_locations_sth;
$lefff_locations_dbh->disconnect;
$first_names_dbh->disconnect;
$lefff_dbh->disconnect;
$refs_dbh->disconnect;
$vars_dbh->disconnect;


sub fill_cache {
  my $tokens_content = shift;
  my $type = shift;
  my $key = shift;
  return if (defined($cache_is_filled{$key}));
#  print STDERR "fill_cache ($tokens_content, $type, $key)\n";
  $vars_sth->execute($tokens_content);
  while (my $id = $vars_sth->fetchrow) {
#    print STDERR "> $id\n";
    if ($type eq "_PERSON") {
      $refs_person_sth->execute(sprintf("%.0f", $id));
      while (my @t = $refs_person_sth->fetchrow_array) {
	$tmpinfo = Encode::decode("utf8",$t[0]."\t".$t[1]."\t".$t[2]."\t".$t[3]);
	fill_cache_subproc ($tokens_content, $tmpinfo, $type, $key, $id, $t[4]);
      }
      $refs_person_sth->finish;
    } elsif ($type eq "_PERSON_m") {
      $refs_person_m_sth->execute(sprintf("%.0f", $id));
      while (my @t = $refs_person_m_sth->fetchrow_array) {
	$tmpinfo = Encode::decode("utf8",$t[0]."\t".$t[1]."\t".$t[2]."\t".$t[3]);
	fill_cache_subproc ($tokens_content, $tmpinfo, $type, $key, $id, $t[4]);
      }
      $refs_person_m_sth->finish;
    } elsif ($type eq "_PERSON_f") {
      $refs_person_f_sth->execute(sprintf("%.0f", $id));
      while (my @t = $refs_person_f_sth->fetchrow_array) {
	$tmpinfo = Encode::decode("utf8",$t[0]."\t".$t[1]."\t".$t[2]."\t".$t[3]);
	fill_cache_subproc ($tokens_content, $tmpinfo, $type, $key, $id, $t[4]);
#	print STDERR "! fill_cache_subproc ($tokens_content, $tmpinfo, $type, $key, ".sprintf("%.0f", $id).");\n";
      }
      $refs_person_f_sth->finish;
    } else {
      $refs_sth->execute(sprintf("%.0f", $id),$type);
      while (my @t = $refs_sth->fetchrow_array) {
	$tmpinfo = Encode::decode("utf8",$t[0]."\t".$t[1]."\t".$t[2]."\t".$t[3]);
	fill_cache_subproc ($tokens_content, $tmpinfo, $type, $key, $id);
      }
      $refs_sth->finish;
    }
  }
  $vars_sth->finish;
  $tokens_content =~ /(^| )([^ ]+)$/;
  my $ending = $2;
  $type =~ /^(.*?)\d*(?:_[mf])?$/;
  if (defined($new_entities_endings{$ending}) && defined($new_entities_endings{$ending}{$1})) {
    fill_cache_subproc ($tokens_content, $new_entities{$new_entities_endings{$ending}{$1}}, $type, $key);
  }
}

sub fill_cache_subproc {
  my $tokens_content = shift;
  my $tmpinfo = shift;
  my $type = shift;
  my $key = shift;
  my $id = shift;
  my $realtype = shift || "NON-PERSON";
#  print STDERR "fill_cache_subproc ($tokens_content, $tmpinfo, $type, $key, $id)\n";
  return if ($id =~ /^1/ && $type eq "_LOCATION");
  if ($tmpinfo =~ s/^(\d+)\t//) {
    $weight = $1 || 1;
  } elsif ($tmpinfo =~ s/^NULL\t// || $tmpinfo =~ /\(UNKNOWN_/) {
    $weight = 1;
  } else {
    print STDERR "### WARNING (np_normalizer): error with Aleda data about entity $key ($tmpinfo)\n";
    return;
  }
  $tmpinfo =~ s/(?<!\\)([(|)])/\\\1/g;
  if ($weight > 0) {$weight = (log (1+$weight))/10 - log(1.9)}
  if ($tmpinfo =~ /\(UNKNOWN_/) {$weight +=0.2}
  if ($tmpinfo =~ /Geoname \d+ \w* P\./) {$weight += 0.05}
  elsif ($tmpinfo =~ /Geoname \d+ \w* A\./) {$weight += 0.04}
  $tmpinfo =~ s/\t/___/g;
  $tmpinfo =~ s/ /_/g;
  $weight = 1 + $weight unless /^<unknown>$/;
  if (($tokens_content !~ / / && $type =~ /^_PERSON/)
      || ($tokens_content =~ / / && $type eq "_LOCATION")) {
    $weight -=0.1;
  }
  if ($type eq "_ORGANIZATION") {$weight -= 0.05}
  $cache{$key}{$tmpinfo}{weight} = $weight;
  $cache{$key}{$tmpinfo}{id} = sprintf("%.0f", $id);
  if ($realtype eq "_PERSON") {
    $cache{$key."_f"}{$tmpinfo}{weight} = $weight-0.001;
    $cache{$key."_f"}{$tmpinfo}{id} = sprintf("%.0f", $id);
    $cache{$key."_m"}{$tmpinfo}{weight} = $weight-0.001;
    $cache{$key."_m"}{$tmpinfo}{id} = sprintf("%.0f", $id);
  }
  $cache_is_filled{$key} = 1;
}


sub is_dubious {
  my $tokens_content = shift;
  my $type = shift;
  my $def = shift;
  my $weight = shift;
  my $is_safe = shift;
  if ($contains_first_token) {
    if ($lang eq "fr" && $tokens_content =~ /^(Mon|Point|Face|Mesures|Première|Elles?|Lui|Ils?|Ceci|Cela|Ce|Aux?|Du|Des?|On|La|Les?|Pour|Quant|Avant|Aujourd.*|Cette)$/) {return 2}
    if ($lang eq "de" && $tokens_content =~ /^(Die|De[nmr]|Das)$/) {return 2}
  }
  if ($tokens_content eq "EUR") {return 2}
  if ($is_safe) {return 0}
  if ($type =~ /^_PERSON/) {
    if ($tokens_content =~ /^([^ ]+) /) {
      fill_is_in_lefff ($1);
      if (defined($is_in_lefff_locations{$1}) && (!defined($is_in_lefff{$1}) || $is_in_lefff{$1} < 4)) {return 1}
      if (defined($is_in_lefff_locations{$1}) && $tokens_content =~ /^[^ ]+ [A-ZÂÇÈÊÉÎÏÖÜŸ][^ ]+ [A-ZÂÇÈÊÉÎÏÖÜŸ][^ ]+/) {return 1}
    }
    if ($tokens_content =~ /^M\. /) {return 1}
    if ($tokens_content =~ /^(d'|du |des? )/) {return 1}
    my $mod_tokens_content = $tokens_content;
    $mod_tokens_content =~ s/^(La|Les?|Du|De [lL]a|Des) //;
    if ($mod_tokens_content =~ / / && $mod_tokens_content !~ /^[a-z]+ [^ ]+$/) {return 0}
    if ($weight > 0.9061 && $tokens_content !~ /^[a-z]+ [^ ]+$/) {return 0}
    fill_is_in_lefff ($tokens_content);
    if ($tokens_content =~ /^[a-z]+ ([^ ]+)$/) {fill_is_in_lefff ($1);}
    if (defined($is_in_lefff{$tokens_content}) >= 2) {return 1}
    elsif ($tokens_content =~ /^[a-z]+ ([^ ]+)$/ && defined($is_in_lefff{$1}) && $is_in_lefff{$1} >= 2) {return 1}
    if (1) { # no idea why we did this at some point; problematic with acronyms (cf. DSK)
      $tokens_content_re = quotemeta ($tokens_content);
      if ($def =~ /^$tokens_content_re\_/) {return 1}
      $def =~ /^(.)(.*)$/;
      if (remove_diacritics ($1) ne $1) {
	$def =~ s/^(.)/remove_diacritics($1)/e;
	if ($def =~ /^$tokens_content_re\_/) {return 1}
      }
      $tokens_content =~ /^(.)(.*)$/;
      if (remove_diacritics ($1) ne $1) {
	$tokens_content =~ s/^(.)/remove_diacritics($1)/e;
	$tokens_content_re = quotemeta ($tokens_content);
	if ($def =~ /^$tokens_content_re\_/) {return 1}
      }
    }
    if ($mod_tokens_content !~ / / && $mod_tokens_content !~ /^[A-ZÂÇÈÊÉÎÏÖÜŸ]{3,}$/) {return 1} # violent, on va tenter
  } elsif ($type =~ /_ORGANIZATION/ || $type =~ /_COMPANY/) {
    for (split / +/, $tokens_content) {
      fill_is_in_lefff (lc($_));
      fill_is_in_lefff ($_) if ($lang eq "de");
    }
    if ($contains_first_token) {
      if ($tokens_content !~ / /) {
	return 1 if ($is_in_lefff{lc($tokens_content)} >= 2);
      } elsif ($tokens_content =~ /^([^ ]+) +([^ ]+)$/) {
	return 1 if ($is_in_lefff{lc($1)} >= 2 && $is_in_lefff{lc($2)} >= 2);
      }
    }
    fill_is_in_lefff ($tokens_content);
    return 1 if ($is_in_lefff{$tokens_content} >= 2 && $lang eq "de");
    if ($tokens_content =~ / /) {return 0}
    if ($is_in_lefff{$tokens_content} >= 4) {return 1}
    if ($tokens_content =~ s/^[LDld]'//) {
      fill_is_in_lefff ($tokens_content);
      if ($is_in_lefff{$tokens_content} >= 4) {return 1}
    }
  } elsif ($type =~ /_LOCATION/) {
    if (1) { ### TEST
      for (split / +/, $tokens_content) {
	fill_is_in_lefff (lc($_));
	fill_is_in_lefff ($_) if ($lang eq "de");
      }
    }
    if ($contains_first_token) {
      if ($tokens_content !~ / /) {
	return 1 if ($is_in_lefff{lc($tokens_content)} >= 2);
      } elsif ($tokens_content =~ /^([^ ]+) +([^ ]+)$/) {
	return 1 if ($is_in_lefff{lc($1)} >= 2 && $is_in_lefff{lc($2)} >= 2);
      }
    }
    fill_is_in_lefff ($tokens_content);
    return 1 if ($is_in_lefff{$tokens_content} >= 2 && $lang eq "de");
    if ($tokens_content =~ / /) {return 0}
    if ($weight >= 1) {return 0} # was 1.6
    if ($tokens_content =~ /^(?:Chirac)$/) {return 1}
    if ($is_in_lefff{$tokens_content} >= 2) {return 1}
    if ($tokens_content =~ s/^[LDld]'//) {
      fill_is_in_lefff ($tokens_content);
      if ($is_in_lefff{$tokens_content} >= 2) {return 1}
    }
  }
  return 0;
}

sub fill_is_in_lefff {
  my $tokens_content = shift;
  if (!defined($is_in_lefff{$tokens_content})) {
    $lefff_locations_sth->execute($tokens_content);
    if ($lefff_locations_sth->fetchrow) {
      $lefff_locations_sth->finish;
      $is_in_lefff_locations{$tokens_content} = 1;
    }
    if ($lang eq "de") {
      $lefff_sth->finish;
      $lefff_sth->execute($tokens_content);
      if ($lefff_sth->fetchrow) {
	$is_in_lefff{$tokens_content} += 2;
      }
    } else {
      $lefff_locations_sth->finish;
      $lefff_sth->execute($tokens_content);
      if ($lefff_info = $lefff_sth->fetchrow) {
	$is_in_lefff{$tokens_content} += 1;
      }
      $lefff_sth->finish;
      $lefff_sth->execute(lc($tokens_content));
      if ($lefff_sth->fetchrow) {
	$is_in_lefff{$tokens_content} += 2;
      }
    }
    $lefff_sth->finish;
    $first_names_sth->execute(Encode::encode("utf8",$tokens_content));
    if ($first_names_sth->fetchrow) {
      $is_in_lefff{$tokens_content} += 4;
    }
    #print STDERR "$tokens_content\t$is_in_lefff{$tokens_content}\n";
    $first_names_sth->finish;
  }
}

sub is_new_doc {
  my $line = shift;
  if ($line =~ /<news_item/ || $line =~ /<item/){
    #print STDERR "Changement de contexte: $_\n"; 
    return 1;
  }
  return 0;
}

sub remove_diacritics {
  my $s = shift;
  $s =~ s/[áàâäąãăå]/a/g;
  $s =~ s/[ćčç]/c/g;
  $s =~ s/[ď]/d/g;
  $s =~ s/[éèêëęě]/e/g;
  $s =~ s/[ğ]/g/g;
  $s =~ s/[ìíîĩĭıï]/i/g;
  $s =~ s/[ĺľł]/l/g;
  $s =~ s/[ńñň]/n/g;
  $s =~ s/[òóôõöø]/o/g;
  $s =~ s/[ŕř]/r/g;
  $s =~ s/[śšş]/s/g;
  $s =~ s/[ťţ]/t/g;
  $s =~ s/[ùúûũüǔ]/u/g;
  $s =~ s/[ỳýŷÿ]/y/g;
  $s =~ s/[źẑżž]/z/g;
  $s =~ s/[ÁÀÂÄĄÃĂÅ]/A/g;
  $s =~ s/[ĆČÇ]/C/g;
  $s =~ s/[Ď]/D/g;
  $s =~ s/[ÉÈÊËĘĚ]/E/g;
  $s =~ s/[Ğ]/G/g;
  $s =~ s/[ÌÍÎĨĬİÏ]/I/g;
  $s =~ s/[ĹĽŁ]/L/g;
  $s =~ s/[ŃÑŇ]/N/g;
  $s =~ s/[ÒÓÔÕÖØ]/O/g;
  $s =~ s/[ŔŘ]/R/g;
  $s =~ s/[ŚŠŞ]/S/g;
  $s =~ s/[ŤŢ]/T/g;
  $s =~ s/[ÙÚÛŨÜǓ]/U/g;
  $s =~ s/[ỲÝŶŸ]/Y/g;
  $s =~ s/[ŹẐŻŽ]/Z/g;
  return $s;
}
