#!/usr/bin/env perl
# input = la bnf

while (<>) {
  next unless /^<T> = ['"]?(\S+?)['"]? ;/;
  $t = $1;
  next if $t =~ /^\%/;
  for $i (1..100) {
    print "\"$t$i\"\t\t$t\t[] ;\n";
  }
  print "\"$t\"\t\t$t\t[] ;\n";
} 
