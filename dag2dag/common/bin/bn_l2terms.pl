#!/usr/bin/env perl

$state = 0;

while (<>) {
    chomp;
    next if ($state == 0 && $_!~/CODE\s+TERMINAL/);
    $state = 1;
    if (/^\s*-([0-9]+)\s+\"?([^\t]+?)\"?\t/) {
	$tstring[$1] = $2;
	$tstring[$1] =~ s/\\"/"/g;
	$tstring[$1] =~ s/'/\\'/g;
    }
    last if (/C O N T E X T    T A B L E/);
}

for (1..$#tstring) {
  if ($tstring[$_] !~ /^'.*'/) {
    $tstring[$_] = "'".$tstring[$_]."'";
  }
  print "\t$tstring[$_];\n";
}
