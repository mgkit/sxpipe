/* ********************************************************
   *                                                      *
   *                                                      *
   * Copyright (c) 2007 by Institut National de Recherche *
   *                    en Informatique et en Automatique *
   *                                                      *
   *                                                      *
   ******************************************************** */

/* ********************************************************
   *                                                      *
   *          Produit de l'equipe ALPAGE                  *
   *                                                      *
   ******************************************************** */

static char	ME [] = "patterns_semact";

#define sxgetchar
#include "sxunix.h"
#undef sxgetchar
#include "varstr.h"
extern VARSTR              cur_input_vstr;

#include "earley.h"

#include "udag_scanner.h"


#include PRIOS_H

#if has_nbest
#include "nbest.h"
#endif /* has_nbest */

char WHAT_PATTERNS_SEMACT[] = "@(#)SYNTAX - $Id$";

static SXINT optimal_prio;

#if keep_only_mpatterns || longest_match || max_pattern_length
# include "XxY.h"
# include "XxYxZ.h"
static SXBA         to_be_erased_Pij;
#endif /* keep_only_mpatterns || longest_match || max_pattern_length */
#if longest_match
static SXBA         mpattern_is_xmpattern;
#endif /* longest_match */

#if keep_only_mpatterns || longest_match || max_pattern_length
static XxYxZ_header mpatterns ;
static SXINT          mpatterns_foreach[] = {1, 1, 0, 1, 0, 0} ;
static SXINT          mpatterns_nb ;
#if keep_only_mpatterns
static XxYxZ_header t_plus_set ;
static SXINT          t_plus_set_foreach[] = {1, 0, 0, 0, 0, 0} ;
#endif /* keep_only_mpatterns */
#endif /* keep_only_mpatterns || longest_match || max_pattern_length */

static SXVOID	gripe (void)
{
    fputs ("\nThe function \"patterns_scan_act\" is out of date with respect to its specification.\n", sxstderr);
    sxexit(1);
}

static SXBOOLEAN is_first_word =  SXTRUE;

SXSHORT
sxgetchar (void)
{
  SXSHORT cur_char = ((sxsrcmngr.infile != NULL)
		      ? getc (sxsrcmngr.infile)
		      : (sxsrcmngr.instring [++sxsrcmngr.strindex] == SXNUL
			 ? (--sxsrcmngr.strindex, EOF)
			 : sxsrcmngr.instring [sxsrcmngr.strindex]));

  if (cur_char != EOF && cur_input_vstr)
    cur_input_vstr = varstr_catchar (cur_input_vstr, (char) cur_char);

  return cur_char;
}

SXBOOLEAN
patterns_scan_act (SXINT code, SXINT act_no)
{

  switch (code) {
  case SXOPEN:
  case SXCLOSE:
  case SXINIT:
  case SXFINAL:
    break;

  case SXACTION:
    switch (act_no) {
    default:
      gripe ();
      /*NOTREACHED*/
      
    case 1:
      is_first_word = SXFALSE;

    }
    return SXTRUE;

  case SXPREDICATE:
    switch (act_no) {
    default:
      gripe ();
      /*NOTREACHED*/

    case 1:
      return is_first_word;
      
    }

  default:
    gripe ();
  }
  return SXFALSE;
}


#if has_prios
static SXINT
RAZ_optimal_prio (Aij)
     SXINT Aij;
{
  optimal_prio = INT_MIN;
  return 1;
}

static SXINT
compute_optimal_prio (Pij)
     SXINT Pij;
{
  SXINT prio = prod2prio [spf.outputG.lhs [Pij].init_prod];
  if (prio > optimal_prio)
    optimal_prio = prio;
  return 1;
}

static SXINT
erase_non_optimal_prods (SXINT Aij, SXBOOLEAN status)
{
  SXINT Pij;
  SXINT hook = spf.outputG.lhs [spf.outputG.maxxprod+Aij].prolon;

  sxuse (status);

  while ((Pij = spf.outputG.rhs [hook].lispro) != 0) {
    if (Pij > 0) { /* prudence */
      if (prod2prio [spf.outputG.lhs [Pij].init_prod] < optimal_prio) {
	spf.outputG.rhs [hook].lispro = -Pij;
	spf.outputG.is_proper = SXFALSE;
      }
    }
    hook++;
  }
  return 1;
}
#endif /* has_prios */

#if keep_only_mpatterns || longest_match || max_pattern_length
# if keep_only_mpatterns
static SXINT
store_t_plus (Pij)
     SXINT Pij;
{
  SXINT t_plus;

  if (strcmp (spf.inputG.ntstring [Aij2A (spf.walk.cur_Aij)], "T+") == 0) { /* la lhs est T* */
    XxYxZ_set (&t_plus_set, Aij2i (spf.walk.cur_Aij), Aij2j (spf.walk.cur_Aij), Pij, &t_plus); /* on stocke le t_plus */
  }
  return 1;
}
#endif /* keep_only_mpatterns */

static SXINT
store_mpattern (Pij)
     SXINT Pij;
{
  SXINT mpattern;
  char *ntstring;

  ntstring = spf.inputG.ntstring [Aij2A (spf.walk.cur_Aij)];

  if (strcmp (ntstring, "MPATTERN") == 0 /* la lhs est MPATTERN */
      || strcmp (ntstring, "PATTERN") == 0 /* ou un PATTERN (non inclus dans MPATTERN) si ESP=1 */
      || strcmp (ntstring, "XMPATTERN") == 0 /* la lhs est XMPATTERN */
      || strcmp (ntstring, "XPATTERN") == 0 /* ou un XPATTERN (non inclus dans MPATTERN) si ESP=1 */
      ) {
    XxYxZ_set (&mpatterns, Aij2i (spf.walk.cur_Aij), Aij2j (spf.walk.cur_Aij), Pij, &mpattern); /* on stocke le mpattern */
  }
  return 1;
}
#endif /* keep_only_mpatterns || longest_match || max_pattern_length */

#if maximize_patterns_number
static max_patterns_nb;
static SXINT *Xpq2max_patterns_nb, *Pij2max_patterns_nb;

static SXINT
compute_max_patterns_nb (Pij)
     SXINT Pij;
{
  SXINT Xpq;
  SXINT item = spf.outputG.lhs [Pij].prolon;
  SXINT local_max_patterns_nb = 0;

  while ((Xpq = spf.outputG.rhs [item++].lispro) != 0) {
    if (Xpq > 0)
      local_max_patterns_nb += Xpq2max_patterns_nb [Xpq];
  }  

  if (local_max_patterns_nb > max_patterns_nb)
    max_patterns_nb = local_max_patterns_nb;

  Pij2max_patterns_nb [Pij] = local_max_patterns_nb;
  
  return 1;
}

static SXINT
maximize_patterns_nb (SXINT Aij, SXBOOLEAN bool)
{
  SXINT Pij;
  SXINT hook = spf.outputG.lhs [spf.outputG.maxxprod+Aij].prolon;

  sxuse (bool);

  while ((Pij = spf.outputG.rhs [hook].lispro) != 0) {
    if (Pij > 0
	&& Pij2max_patterns_nb [Pij] < max_patterns_nb) {
      spf.outputG.rhs [hook].lispro = -Pij;
      spf.outputG.is_proper = SXFALSE;
    }
    hook++;      
  }  

  if (strcmp (spf.inputG.ntstring [Aij2A (Aij)], "PATTERN") == 0
      || strcmp (spf.inputG.ntstring [Aij2A (Aij)], "XPATTERN") == 0)
    max_patterns_nb++;
  Xpq2max_patterns_nb [Aij] = max_patterns_nb;

  max_patterns_nb = 0;
  
  return 1;
}
#endif /* maximize_patterns_number  */

static SXINT
patterns_sem_pass (void)
{
  SXBOOLEAN filtered_forest_is_not_empty = (spf.outputG.start_symbol != 0);
  SXINT p, q, t_plus, i, j, Pij;
  SXINT *i2longest_match_j, *j2longest_match_i;


  is_first_word = SXTRUE; /* on prépare le scanact &1 du lecl qui regardera (peut-être) les mots (un à un) de la phrase suivante */

#if EBUG
  spf_print_signature (stderr,"Initial forest: ");
#endif /* EBUG */

  if (filtered_forest_is_not_empty) {
#if has_prios
    spf_topological_bottom_up_walk (spf.outputG.start_symbol, compute_optimal_prio, RAZ_optimal_prio, erase_non_optimal_prods);
    if (!spf.outputG.is_proper)
      filtered_forest_is_not_empty = spf_make_proper (spf.outputG.start_symbol);
#if EBUG
    spf_print_signature (stderr,"Forest after prio filtering: ");
#endif /* EBUG */
#else /* has_prios */
#if has_nbest
    nbest_allocate (1 /* 1-best */, PCFG);
    nbest_perform (spf.outputG.start_symbol, 1, 0, PCFG, SXTRUE);
    nbest_free ();
    if (!spf.outputG.is_proper)
      filtered_forest_is_not_empty = spf_make_proper (spf.outputG.start_symbol);
#endif /* has_nbest */
#endif /* has_prios */
  }

#if keep_only_mpatterns || longest_match || max_pattern_length
  if (filtered_forest_is_not_empty) {
    XxYxZ_alloc (&mpatterns, "mpatterns", spf.outputG.maxxnt + 1, 1, mpatterns_foreach, NULL, NULL);
    spf_topological_bottom_up_walk (spf.outputG.start_symbol, store_mpattern, NULL, NULL);

    mpatterns_nb = XxY_top (mpatterns);
#if longest_match
    {
      char *mpattern_ntstring;
      
      mpattern_is_xmpattern = sxba_calloc (mpatterns_nb + 1);
      for (p = 1; p <= mpatterns_nb; p++) {
	mpattern_ntstring = spf.inputG.ntstring [Aij2A (spf.outputG.lhs [XxYxZ_Z (mpatterns, p)].lhs)];
	if (strcmp (mpattern_ntstring, "XMPATTERN") == 0
	    || strcmp (mpattern_ntstring, "XPATTERN") == 0
	    ) {
	  SXBA_1_bit (mpattern_is_xmpattern, p);
	}
      }
    }
#endif /* longest_match */
#if keep_only_mpatterns
    XxYxZ_alloc (&t_plus_set, "t_plus_set", spf.outputG.maxxnt + 1, 1, t_plus_set_foreach, NULL, NULL);
    spf_topological_bottom_up_walk (spf.outputG.start_symbol, store_t_plus, NULL, NULL);
#endif /* keep_only_mpatterns */
  }

  if (filtered_forest_is_not_empty) {
    to_be_erased_Pij = sxba_calloc (spf.outputG.maxxprod + 1);
#ifdef max_pattern_length
#if max_pattern_length > 0
    for (p = 1; p <= mpatterns_nb; p++) {
      i = XxYxZ_X (mpatterns, p);
      j = XxYxZ_Y (mpatterns, p);
      if (j - i >= max_pattern_length) {
	Pij = XxYxZ_Z (mpatterns, p);
	SXBA_1_bit (to_be_erased_Pij, Pij);
      }
    }
#endif /* max_pattern_length > 0 */
#endif /* max_pattern_length */
  }

  if (filtered_forest_is_not_empty) {
#if longest_match
    i2longest_match_j = (SXINT *) sxcalloc (n + 2, sizeof (SXINT));
    j2longest_match_i = (SXINT *) sxcalloc (n + 2, sizeof (SXINT));

    for (p = 1; p <= mpatterns_nb; p++) {
      if (!SXBA_bit_is_set (to_be_erased_Pij, XxYxZ_Z (mpatterns, p))) {
	if (!SXBA_bit_is_set (mpattern_is_xmpattern, p)) {
	  i = XxYxZ_X (mpatterns, p);
	  j = XxYxZ_Y (mpatterns, p);
	  if (i2longest_match_j [i] < j) 
	    i2longest_match_j [i] = j;
	  if (j2longest_match_i [j] == 0 || j2longest_match_i [j] > i) 
	    j2longest_match_i [j] = i;
	} else { // we need to know the longest x(m)pattern, in case there are no (m)patterns
	  i = XxYxZ_X (mpatterns, p);
	  j = XxYxZ_Y (mpatterns, p);
	  if (i2longest_match_j [i] == 0 || (i2longest_match_j [i] < 0 && -i2longest_match_j [i] < j)) 
	    i2longest_match_j [i] = -j;
	  if (j2longest_match_i [j] == 0 || (j2longest_match_i [j] < 0 && -j2longest_match_i [j] > i)) 
	    j2longest_match_i [j] = -i;
	}
      }
    }
    for (i = 1; i <= n + 1; i++) {
      if (i2longest_match_j [i] < 0)
	i2longest_match_j [i] = -i2longest_match_j [i];
    }
    for (j = 1; j <= n + 1; j++) {
      if (j2longest_match_i [j] < 0)
	j2longest_match_i [j] = -j2longest_match_i [j];
    }
#endif /* longest_match */

    for (p = 1; p <= mpatterns_nb; p++) {
      if (!SXBA_bit_is_set (to_be_erased_Pij, XxYxZ_Z (mpatterns, p))) {
	i = XxYxZ_X (mpatterns, p);
	j = XxYxZ_Y (mpatterns, p);
#if longest_match
	XxYxZ_Xforeach (mpatterns, i, q) {
	  if (XxYxZ_Y (mpatterns, q) < i2longest_match_j [i]) {
	    Pij = XxYxZ_Z (mpatterns, q);
	    SXBA_1_bit (to_be_erased_Pij, Pij);
	  }
	}
	XxYxZ_Yforeach (mpatterns, j, q) {
	  if (XxYxZ_X (mpatterns, q) > j2longest_match_i [j]) {
	    Pij = XxYxZ_Z (mpatterns, q);
	    SXBA_1_bit (to_be_erased_Pij, Pij);
	  }
	}
#if keep_only_mpatterns
      }
    }
    for (p = 1; p <= mpatterns_nb; p++) {
      if (!SXBA_bit_is_set (to_be_erased_Pij, XxYxZ_Z (mpatterns, p))) {
	i = XxYxZ_X (mpatterns, p);
	j = XxYxZ_Y (mpatterns, p);
#endif /* keep_only_mpatterns */
#endif /* longest_match */
#if keep_only_mpatterns
	XxYxZ_Xforeach (t_plus_set, i, t_plus) {
	  if (j <= XxYxZ_Y (t_plus_set, t_plus)) {
	    Pij = XxYxZ_Z (t_plus_set, t_plus);
	    SXBA_1_bit (to_be_erased_Pij, Pij);
	  }
	}
#endif /* keep_only_mpatterns */
      }
    }

#if longest_match
    sxfree (i2longest_match_j);
    sxfree (j2longest_match_i);
#endif /* longest_match */
#if keep_only_mpatterns
    XxYxZ_free (&t_plus_set);
#endif /* keep_only_mpatterns */
    XxYxZ_free (&mpatterns);

    filtered_forest_is_not_empty = 
      sxba_is_empty (to_be_erased_Pij) || spf_erase (to_be_erased_Pij); /* soit on fait rien (y'a
									   rien à faire) et la forêt
									   n'est pas vide, soit on
									   filtre et spf_erase rend
									   SXTRUE ssi la forêt
									   filtrée n'est pas vide */

    sxfree (to_be_erased_Pij), to_be_erased_Pij = NULL;
#if longest_match
    sxfree (mpattern_is_xmpattern), mpattern_is_xmpattern = NULL;
#endif /* longest_match */
  }
#endif /* keep_only_mpatterns || longest_match || max_pattern_length */

#if maximize_patterns_number
  if (filtered_forest_is_not_empty) {
    Xpq2max_patterns_nb = (SXINT *) sxcalloc (spf.outputG.maxxnt + 1, sizeof (SXINT));
    Pij2max_patterns_nb = (SXINT *) sxcalloc (spf.outputG.maxprod + 1, sizeof (SXINT));

    spf_topological_bottom_up_walk (spf.outputG.start_symbol, compute_max_patterns_nb, NULL, maximize_patterns_nb);  
    if (!spf.outputG.is_proper)
      filtered_forest_is_not_empty = spf_make_proper (spf.outputG.start_symbol);

    sxfree (Xpq2max_patterns_nb), Xpq2max_patterns_nb = NULL;
    sxfree (Pij2max_patterns_nb), Pij2max_patterns_nb = NULL;
  }
#endif /* maximize_patterns_number */
  
  if (filtered_forest_is_not_empty) {
    //spf_yield2dfa (/*SXFALSE = DAG ;*/ SXTRUE /*= UDAG */);
    spf_yield2dfa (SXFALSE /*= DAG ; SXTRUE = UDAG */);
  } else {
    char *c1 = NULL, *c2 = NULL;

    c2 = SXGET_TOKEN (1).comment;
    if (c2) {
      c1 = strchr (c2, (int)'E');
      c2 = strchr (c1, (int)'F');
      *c2 = '\0';
    }
    printf ("%% The following sentence %s could not be processed correctly. It is left unchanged\n", c1 ? c1 : "(no id known)");
    fprintf (sxstderr, "### WARNING: Sentence abandoned and output unchanged (sentence id: %s)\n", c1 ? c1 : "(no id known)");
    {
      char* cur_input_str, *char_ptr, prev_char;
      varstr_complete (cur_input_vstr);
      cur_input_str = varstr_tostr (cur_input_vstr);
      char_ptr = strchr (cur_input_str, '\n');
      if (char_ptr != NULL) {
	prev_char = *char_ptr;
	*char_ptr = '\0';
      }
      printf ("%s\n", cur_input_str);
      if (char_ptr != NULL)
	*char_ptr = prev_char;
    }
    if (c2) {
      *c2 = 'F';
    }
  }

  if (is_print_time)
    sxtime (SXACTION, "\tForest filtering");

  return 1;
}


/* retourne le ME */
static char*
patterns_ME (void)
{
  return ME;
}

static void
free_all (void)
{
  varstr_free (cur_input_vstr), cur_input_vstr = NULL;
}

/* Point d'entree initial, on positionne qq valeurs qui vont conditionner toute la suite */
void
patterns_semact (void)
{
  for_semact.sem_init = NULL;
  for_semact.sem_final = NULL;
  for_semact.sem_close = free_all;
  for_semact.semact = NULL;
  for_semact.parsact = NULL;
  for_semact.prdct = NULL;
  for_semact.constraint = NULL;
  for_semact.sem_pass = patterns_sem_pass;
  for_semact.scanact = NULL;
  for_semact.rcvr = NULL; /* C'est earley qui choisit la "meilleure" correction */
  for_semact.rcvr_range_walk = NULL; /* On utilise une strategie standard (ici MIXED_FORWARD_RANGE_WALK_KIND, voir +loin) */
  for_semact.process_args = NULL;
  for_semact.string_args = NULL; 
  for_semact.ME = patterns_ME;

  /* Si earley fait de la rcvr, prendre cette strategie d'essai des points de reprise */  
  rcvr_spec.range_walk_kind = MIXED_FORWARD_RANGE_WALK_KIND; 

  /* On change les valeurs par defaut de l'analyseur earley car on est en patterns */
  is_parse_forest = SXTRUE;

  /* valeurs par defaut des options pour patterns */
  rcvr_spec.perform_repair = SXTRUE; /* On fait de la correction d'erreur ... */
  rcvr_spec.repair_kind = 1; /* ... mais on ne genere qu'une chaine de la longueur min */
  rcvr_spec.perform_repair_parse = SXTRUE; /* On analyse cette chaine ... */
  rcvr_spec.repair_parse_kind = 1; /* ... mais on ne genere qu'une seule analyse */

  cur_input_vstr = varstr_alloc (128);
}

