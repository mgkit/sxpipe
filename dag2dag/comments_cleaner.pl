#!/usr/bin/env perl
# $Id$
use utf8;
binmode STDERR, ":utf8";

$| = 1;

$option = shift;
$latin1 = 1 if $option eq "-l1";

if ($latin1) {
  binmode STDIN, ":encoding(iso-8859-1)";
  binmode STDOUT, ":encoding(iso-8859-1)";
} else {
  binmode STDIN, ":utf8";
  binmode STDOUT, ":utf8";
}

while (<>) {
  chomp;
  s/^\s*//;
  s/\s*$//;

  if (/^_PAR_BOUND(?:\s*\[\|0\|\]\s*)?$/) {
    print "$_\n";
    next;
  }

  if (/_XML$/) {
    print "$_\n";
    next;
  }

  $linear_path_elt = qr/(?:{[^{}]+}|\[\||\|\]|[^(|){}]|\\[(|){}])/o;  
  $path2 = qr/$linear_path_elt*(?:\(($linear_path_elt|\|)*\)$linear_path_elt*)?/o;
  $path = qr/$linear_path_elt*(?:\(($path2|\|)*\)$linear_path_elt*)?/o;
  # +, ? AND * SIGNS AS TOKENS MUST BE ESCAPED
#  while(s/([^\\])(\+|\?|\*)/$1\\$2/){} # no!!!
  s/ \(.*?\)\?//g;
  while (s/({[^{}]*?} *(?:[^ (|){}]|\\[\(\)])+(?: \[\|.*?\|\])?) \($path\)\?/(\1 | \1 \2)/g) {}
  # (, ), | SIGNS AS TOKENS MUST BE ESCAPED -> moved at the end of the script (the temporary escaping below requires doing it there)
#  while(s/(<F.+?>)([(|)\+\?\*])(<\/F>)/$1\\$2$3/){} # bs: why not, but why only those chars and not +?*, and why only when they constitute 1-char tokens?
  # (, ), | AS FORMS MUST BE ESCAPED
  while(s/\} *([(|)\+\?\*])/} \\$1/){} # bs: should never happen (if they are forms, they are already escaped)

  #temporary escaping metacharacters inside comments (restored at the end of the script)
  while (s/({[^}]*)\(/\1_\#_LRB/g) {}
  while (s/({[^}]*)\)/\1_\#_RRB/g) {}
  while (s/({[^}]*)\|/\1_\#_PIPE/g) {}
  while (s/({[^}]*)\*/\1_\#_STAR/g) {}
  while (s/({[^}]*)\?/\1_\#_QM/g) {}
  while (s/({[^}]*)\+/\1_\#_PLUS/g) {}

  $r = "";
  if (s/^(\(+)\s*//) {$r .= "$1"}
  for (split(/(?=\{)/, $_)) {
    unless (/^{(.*[^\\])}(.*)$/) {
      print STDERR "WARNING (comments_cleaner.pl): Format error: $_\n";
      $r .= "$_";
      next;
    }
    $comment = $1;
    $ff_and_meta_chars = $2;
    %printed = ();
    $is_first_token = 1;
    $r .= "{";
    for (split(/(?<=<\/F>)\s*/, $comment)) {
      unless (defined($printed{$_})) {
	$r .= " " unless $is_first_token;
	$r .= "$_";
	$printed{$_} = 1;
	$is_first_token = 0;
      }
    }
    $r .= "}";
    $r .= $ff_and_meta_chars;
  }
  while ($r =~ s/(^|[^\\])([\(\|]) *($linear_path_elt*?) *\| \3 ([\)\|])/\1\2\3\4/g) {}
  while ($r =~ s/(^|[^\\])\(($linear_path_elt*[^\\])\)/\1\2/g) {}
  $r =~ s/_\#_LRB/(/g;
  $r =~ s/_\#_RRB/)/g;
  $r =~ s/_\#_PIPE/|/g;
  $r =~ s/_\#_STAR/*/g;
  $r =~ s/_\#_QM/?/g;
  $r =~ s/_\#_PLUS/+/g;
  # (, ), | SIGNS AS TOKENS MUST BE ESCAPED
  while($r =~ s/(<F.+?>)([(|)\+\?\*])(<\/F>)/$1\\$2$3/){} # bs: why not, but why only those chars and not +?*, and why only when they constitute 1-char tokens?

  $r .= "\n";
  print $r;
}
