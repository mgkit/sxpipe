#+Title: Citations - Notes et documentation

* Périmètre du problème de la citation dans l'application AFP
* Description du système de détection
* Evaluation
** Nature des données évaluées
D'un point de vue fonctionnel, on cherche à évaluer la capacité du système à détecter le maximum d'instances de ce qui est entendu par /citation/ dans le cadre de cette tâche.
Les données de base sont l'ensemble des segments entre guillemets (détection triviale en dehors des cas de segmentation phrastiques intra-verbatims). Il s'agit de les /catégoriser/ correctement : Q, SQ ou NQ. Ensuite, pour tout Q, il s'agit de rattacher chaque verbatim à l'instance correcte de Q, ainsi qu'au prédicat et à l'éventuel auteur correspondant. On cherche donc un recouvrement correct de la citation. Pour les SQ, on cherche également à considérer ensemble les verbatims correspondant à un même énoncé. Enfin, pour chaque Q, on pourra - ou non - évaluer la capacité du système à choisir la configuration de citation correcte, prévue dans la grammaire correspondante (IQ, DDIQ, DIQ, DQ, PRQ).
Il est nécessaire de déplier les différents niveaux opérationnels du sytème et de les évaluer de façon distincte :
- Catégoristion en Q, SQ, NQ de chaque segment entre guillemets => précision / accuracy
- Indexation / regroupement de chaque segment entre guillemets => accuracy
- Configuration de Q choisie => accuracy
* Description du format de sortie (SxPipe + Quotes)
