<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jan 12, 2010</xd:p>
            <xd:p><xd:b>Author:</xd:b> stern</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    
    
    <xsl:output indent="yes" method="xml" encoding="utf-8"/>

        
    <!-- TEMPLATES -->
    <xsl:template match="/child::*">
        <raw_corpus>
            <xsl:apply-templates select="./news_item"/>    
        </raw_corpus>
    </xsl:template>

    <xsl:template match="news_item">
            <news_item>
                <xsl:attribute name="rank">
                    <xsl:value-of select="./@rank"/>
                </xsl:attribute>
                <xsl:attribute name="eval_type">
                    <xsl:value-of select="./@eval_type"/>    
                </xsl:attribute>                
                <xsl:attribute name="ref">
                    <xsl:value-of select="./@ref"/>
                </xsl:attribute>
                <xsl:apply-templates select="./para"/>
            </news_item>
    </xsl:template>
   
   
    
    <xsl:template match="para">
        <xsl:choose>
            <xsl:when test="./@signature">
                <para>
                    <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>
                    <xsl:attribute name="signature"><xsl:value-of select="./@signature"/></xsl:attribute>                    
                </para>
            </xsl:when>
            <xsl:otherwise>                
                <para>
                    <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>      
                    <xsl:apply-templates/>
                </para> 
            </xsl:otherwise>
        </xsl:choose>       
    </xsl:template>

</xsl:stylesheet>
