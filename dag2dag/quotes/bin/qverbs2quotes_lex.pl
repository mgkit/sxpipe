#!/usr/bin/env perl

use warnings;
use strict;
use Encode;

my $qverbs_file = shift or die "Usage: $0 <qverbs> <v.lex>\n";
my $vlex = shift or die "Usage: $0 <qverbs> <v.lex>\n";

open VLEX, '<:encoding(iso-8859-1)', $vlex or die "Can't open $vlex: $!\n";
my @vlexlines = <VLEX>;
close(VLEX);

open QV, '<:encoding(iso-8859-1)', $qverbs_file or die "Can't open $vlex: $!\n";
while (<QV>) {
  chomp;
  my $pron = "";
  my($type, $lemma, $class, $feature) = split("\t", $_);
  if (defined($feature)) {
    $pron = "_pron";
  }
  my @lines = grep(/\t${lemma}___/, @vlexlines);
  if ($pron) {
    my $tag = "\t\tqverb".$pron."_".$class."_p3\t[] ;";
    my @results = grep(/ThirdSing/, @lines);
    foreach my $res (@results) {
      if ($res =~ /\@pron|Obj:[^,>]*ser�fl/) {
	my @t = split("\t", $res);
	my $toprint = $t[0].$tag."\n";
	print encode("iso-8859-1", $toprint);
      }
    }
    $tag = "\t\tqverb".$pron."_".$class."_pp\t[] ;";
    @results = grep(/\@K/, @lines);
    foreach my $res (@results) {
      if ($res !~ /K(m|f)*p/ and $res =~ /\@pron|Obj:[^,>]*ser�fl/) {
	my @t = split("\t", $res);
	my $toprint = $t[0].$tag."\n";
	print encode("iso-8859-1", $toprint);
      }
    }
    if (($class eq "Ia") or ($class eq "Ib") or ($class eq "Ic")) {
      $tag = "\t\tqverb".$pron."_".$class."_ger\t[] ;";
      @results = grep(/\@G/, @lines);
      foreach my $res (@results) {
	if ($res =~ /\@pron|Obj:[^,>]*ser�fl/) {
	  my @t = split("\t", $res);
	  my $toprint = $t[0].$tag."\n";
	  print encode("iso-8859-1", $toprint);
	}
      }
    }
  } else {
    my $tag = "\t\tqverb_".$class."_p3\t[] ;";
    my @results = grep(/ThirdSing/, @lines);
    foreach my $res (@results) {
      if ($res !~ /\@pron/) {
	my @t = split("\t", $res);
	my $toprint = $t[0].$tag."\n";
	print encode("iso-8859-1", $toprint);
      }
    }
    $tag = "\t\tqverb_".$class."_pp\t[] ;";
    @results = grep(/\@K/, @lines);
    foreach my $res (@results) {
      if ($res !~ /K(m|f)*p/ and $res !~ /Kf(s|p)/ and $res !~ /\@pron/) {
	my @t = split("\t", $res);
	my $toprint = $t[0].$tag."\n";
	print encode("iso-8859-1", $toprint);
      }
    }
    if (($class eq "Ia") or ($class eq "Ib") or ($class eq "Ic")) {
      $tag = "\t\tqverb_".$class."_ger\t[] ;";
      @results = grep(/\@G/, @lines);
      foreach my $res (@results) {
	if ($res !~ /\@pron/) {
	  my @t = split("\t", $res);
	  my $toprint = $t[0].$tag."\n";
	  print encode("iso-8859-1", $toprint);
	}
      }
    }
  }
}
close(QV);


__END__
K
  Kfp
  Kfs
  Km
  Kmp
  Kms
