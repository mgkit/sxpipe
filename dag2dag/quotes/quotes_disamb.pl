#!/usr/bin/env perl

use warnings;
$| = 1;

#SCRIPT DE DESAMBIGUISATION DE LA SORTIE DU MODULE QUOTES DE SXPIPE
# en partant du principe que le module quotes applique les règles dans l'ordre, 
#et que s'il a pu appliquer et SQ et d'autres combinaisons, c'est la deuxième solution qui est la bonne

use strict;
my $line = "";
my $i = 0;

my $parento = qr/(?<!\\)\(/;
my $parentf = qr/(?<!\\)\)/;
my $pipe = qr/(?<!\\)\|/;
my $no_parent_or_pipe_1 = qr/(?<!\\)[\(\)\|]/;
my $no_parent_or_pipe = qr/(?:$no_parent_or_pipe_1|\\[\(\)\|])/;

while(<>)
{
	if($_ =~ /\} _XML$/){print $_;}
	else
	{
		$line = $_;
		#enlever (...) sans | dedans
		$line =~ s/\\\(/#BACKSLASH##LBR#/g;
		while ($line =~ s/({[^}]+)\(/$1#LBR#/g) {}
		$line =~ s/\\\)/#BACKSLASH##RBR#/g;
		while ($line =~ s/({[^}]+)\)/$1#RBR#/g) {}
		$line =~ s/\\\|/#BACKSLASH##PIPE#/g;
		while ($line =~ s/({[^}]+)\|/$1#PIPE#/g) {}
		#enlever les | des comments [|...|]
		# print STDERR "$line\n\n";
		while($line =~ /\[\|.*?\|\]/)
		{
			my $comments = $&;
			$line = $`."#COM#".$';
			$comments =~ s/\[\|/LCOM/g;
			$comments =~ s/\|\]/RCOM/g;			
			$line =~ s/#COM#/$comments/;
		}
		#si (...|...) => verifier que c'est une amb de quotes, sinon remplacer (, ), et |
		while($line =~ /(^|\()[^\(\)\|]+(?:\|([^\(\)\|]+))+(\)|$)/)
		{
			my $amb = $&;
			my $amb_d = $2;		
			$line = $`."#AMB#".$';
			if($amb =~ /_MARKUP__:?(SQ|Q|NO_PRED|IQ|DIQ|DDIQ|DQ|PRQ|GUILLQ|INTRANSQ|HQ|TQ|INC_QV|DI_QV|DDI_QV|GER_QV|DQV|QPREP|GUILL_QV|INTRANS_QV|AUT|AUT_PRO)/)
			{
				#print STDERR "Quotes in Amb:\n$amb";<STDIN>;
				$line =~ s/#AMB#/$amb_d/;
				#print STDERR "=>\n$line";<STDIN>;
			}
			else
			{
				#print STDERR "No quotes in Amb: $amb";<STDIN>;
				$amb =~ s/\(/#LBR#/;
				$amb =~ s/\)/#RBR#/;
				$amb =~ s/\|/#PIPE#/g;
				$line =~ s/#AMB#/$amb/; 
				#print STDERR "=>\n$amb";<STDIN>;
			}
		}
		# s'il reste un pipe : la disjonction couvre toute la phrase, prendre la partie droite
		$line =~ s/#BACKSLASH#/\\/g;
		$line =~ s/#LBR#/(/g;
		$line =~ s/#RBR#/)/g;
		$line =~ s/#PIPE#/|/g;
		$line =~ s/\[#/[|/g;
		$line =~ s/#\]/|]/g;			
		$line =~ s/LCOM/[|/g;
		$line =~ s/RCOM/\|\]/g;	
		# $line =~ s/\[\|[^0-9].*?\|\]//g;
		print $line;
	}
}	

__END__
	


