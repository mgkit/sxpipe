#+Title: Évolution du module de détection des citations

* Traitement des guillemets /quotes\_tokenizer.pl/
- Remplace les guillemets impairs par /\_QUOTE\_NO\_SPACE\_RIGHT/ et
  les guillemets pairs par /\_QUOTE\_NO\_SPACE\_LEFT/.
- Si le nombre total de guillemets est impair, il s'agit d'une erreur,
  on laisse les symboles " et la phrase ne sera pas traitée par quotes.
- On décide à ce niveau que les verbatims entre parenthèses ne seront
  pas des citations. On ajoute /\_\_notaquote/ derrière le guillemet
  ouvrant.


* Filtre /quotes\_filter.pl/
- Applique des filtres qui décident, selon le contexte linguistique,
  que certains verbes ne sont pas des prédicats de citation et que
  certaines personnes ne sont pas des auteurs de citation.
- Certains verbes peuvent avoir la même forme que des noms communs (ex :
  communiqué, juge, annonce). Le filtre indique que si une de ces
  formes est précédée par un article, une préposition, ou encore se
  trouve dans une proposition relative (introduite par "qui"), alors
  ce n'est pas un verbe. On ajoute /\_\_notaqverb/ derrière leur
  forme.
- De même, certaines entités Person ne seront jamais auteur d'une
  citation si elles sont précédées d'un article ou d'une
  préposition ou encore si elles sont entre parenthèses. On ajoute
  /\_\_notaqauthor/ derrière leur forme.


* Liste de non verbes /notaqverb/
- Mots qui ne sont pas des verbes dans le contexte défini par
  /quotes\_filter/.


* Désambigüisation /quotes\_disamb.pl/
- Ce script ne sert à désambigüiser qu'en dernier recours.
- /--> les désambigüisations sont effectuées au maximum dans la bnf,
  grâce aux poids donnés aux règles/


* Makefile
- Utilisation du lexique /v\_new.lex/ du Lefff car il contient davantage
  de verbes pronominaux que /v.lex/.
- On exclut les verbes au conditionnel.
- Extraction des adverbes depuis /adv.lex/ du Lefff.
- Suivant les filtres implémentés dans /quotes\_filter/,
  certains verbes ne peuvent pas être prédicats de citation selon leur
  contexte linguistique. Ceux-ci sont listés dans le fichier
  /notaqverb/ et ajoutés au lexique sous leur forme, par exemple,
  "annonce\_\_notaqverb".


* Liste de verbes /qverbs/
- Au début, tous les verbes pouvaient être prédicats dans leur forme
  pronominale. (?)
- Depuis la version 3873 avec le script /qverbs2quotes\_lex.pl/, seuls
  certains verbes peuvent être prédicats dans leur forme
  pronominale. L'information est notée par /@pron/ à côté des verbes
  concernés dans le fichier /qverbs/. À partir de la version 4208, les
  qverbs de type /@pron/ peuvent matcher les entrees verbales du lefff
  avec /Obj:serefl/, et pas seulement les entrees pronominales (/@pron/).
- Ajouts de verbes :
  - classe II : s'enflammer
  - classe IIIb : qualifier, s'insurger, professer, démentir,
    expliquer, s'enorgueillir, s'énerver, s'amuser, s'écrier, se
    prononcer, se justifier, riposter, se défendre, esquiver,
    s'esclaffer, balayer, se désoler, se déclarer, contre-attaquer


* Lexique /quotes.lex.header/
- Création de /quotes.lex.header/
  - pronoms
  - prépositions
  - déterminants
  - verbes pour locutions verbales
  - formes pour les Person non auteurs selon leur contexte défini dans
    /quotes\_filter/ ("\_PERSON\_\_notaqauthor"...)


* Grammaire /quotes.bnf/
- Nouvelle configuration /PRQ/
 - Prédicat prépositionnel : "pour", "selon", "d'après"
 - Le prédicat et l'auteur peuvent être placé devant ou derrière la
   citation.
  - <QPREP>Selon</QPREP> <AUT>Jacques Chirac</AUT>, <PRQ>"il faut
    manger des pommes"</PRQ>.
  - <PRQ>"Il faut manger des pommes"</PRQ>, <QPREP>selon</QPREP>
    <AUT>Jacques Chirac</AUT>.

- Nouvelle configuration /GUILLQ/
 - Concerne les structures qui contiennent un verbe de citation mais
   sans réelle configuration syntaxique.
  - <AUT>Nicolas Sarkozy</AUT> <GUILL\_QV>évoque</GUILL\_QV>
    <GUILLQ>"les difficultés économiques"</GUILLQ>.

- Nouvelle configuration /INTRANSQ/
 - Concerne les structures qui contiennent un verbe de citation
   intransitif (classe II)  en position initiale suivi de ":"
  - <AUT>François Hollande</AUT>
    <INTRANS\_QV>intervient</INTRANS\_QV> : <INTRANSQ>"Si je suis élu
    président, tout ira mieux"</INTRANSQ>.

- Nouvelle configuration /NO\_PRED/
 - Configuration sans prédicat.
 - Pour les dépêches particulières non rédigées (/Réactions/).
 - tiret + Auteur [+ description de l'auteur] + : + "..."
  - - <AUT>Christian Jacob</AUT>, président des députés UMP :
    <NO\_PRED>"Le second débat socialiste a oscillé entre populisme,
    incohérence et mensonges"</NO\_PRED>.

- Distinction entre auteur Person et auteur Organization (np -> person
  / organization). On a ensuite décidé de ne garder que des auteurs
  Person, les auteurs Organization générant beaucoup de bruit.

- Suppression de la distinction entre auteur clitique et auteur
  pronom. Ne reste en tag auteur que <AUT> pour une personne et
  <AUT\_PRO> pour les pronoms/clitiques.

- Possibilité d'avoir plusieurs <HQ> et/ou <TQ> pour toutes les
  configurations. On attribue tous les verbatims de la phrase à un
  auteur donné.

- Ajout des verbes "être" et "avoir" marqueurs de propositions
  infinitives dans la même configuration que DIQ.

- Ajout de règles spéciales pour les verbes au gérondif. Intégrés aux
  configurations existantes : /DI\_GER\_QV/, /DDI\_GER\_QV/,
  /D\_GER\_QV/. Sous des balises spécifiques (par exemple, <GER\_QV>
  au lieu de <DI\_QV> pour les verbes aux autres temps).

- Ajout des locutions verbales (faire savoir, tenir à souligner...)

- Possibilité d'avoir un adverbe et/ou un pronom entre l'auxiliaire et
  le verbe (ex : "...", a lui aussi déclaré Jacques Chirac), à l'intérieur d'une locution
  verbale (ex : "...", fait-il également savoir) ou entre un gérondif
  et que/infinitif/colon (ex : Eva Joly estimant, elle aussi, que
  "..."). -> /<PRO\_ADV?>/

- Autre règle /IQ/ pour les cas où l'incise précède la citation :
 - <IQ> = virgule <INC\_QV> <TNQV*> <IQ:!> <Q> <:IQ!>

- Ajout des patterns :
 - <SQ:!> <NQ> <:SQ!>
 - <Q:!> <COMPLEX\_DIQ> <:Q!>
 - <Q:!> <COMPLEX\_DDIQ> <:Q!>
 - <Q:!> <COMPLEX\_GUILLQ> <:Q!>
 - <Q:!> <COMPLEX\_INTRANSQ> <:Q!>
 - <Q:!> <COMPLEX\_DQ> <:Q!>

- Pour DIQ, DDIQ et DQ, distinction des règles avec auteur et des
  règles sans auteur.
 - Utile pour les nouvelles règles /CQ/ :
  - AUT ... "HQ" ... <DIQ> DI\_QV ... "DIQ" </DIQ>
  - AUT ... "HQ" ... <DDIQ> DDI\_QV ... "DDIQ" </DDIQ>
  - -> Ici, les configurations <DIQ> et <DDIQ> ne doivent pas avoir
    d'auteur.

