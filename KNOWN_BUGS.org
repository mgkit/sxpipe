#+SEQ_TODO: TODO ACTIVE DEAD DONE

#+TITLE: || RAPPORTS DE BUGS SXPIPE || SXPIPE KNOWN BUGS ||


* (Example of a bug to copy/paste) dag2pdag
** Reporter
| Rosa Stern | INRIA | stern.rosa@gmail.com | 20/02/11 |
|            |       |                      |          |
** Description
La disjonciton recouvre l'ensemble de la phrase et non seulement la zone ambigüe (semble-t-il lorsque la phrase n'excède pas une certaine longueur ?)
** Exemple
stern@[~/Dev/ALPC/nomos/linking]~$ echo "M. Obama a donné son approbation en dépit du scandale causé récemment" | lingpipe --config /Users/stern/Dev/ALPC/sxpipe/sxpipe-afp.conf --last recode-final | perl -pe 's/\{.+?\}//g'
 M.  _PERSON_m2  a  donné  son  approbation ( en  dépit  du | ( en_dépit_de__prep |  en  dépit  de__prep)  le__det)  scandale  causé  récemment
stern@[~/Dev/ALPC/nomos/linking]~$ echo "M. Obama a donné son approbation en dépit du scandale causé récemment" | lingpipe --config /Users/stern/Dev/ALPC/sxpipe/sxpipe-afp.conf --last recode-final | dag2pdag | perl -pe 's/\{.+?\}//g'
 ( M.  _PERSON_m2  a  donné  son  approbation  en  dépit  du  scandale  causé  récemment | M.  _PERSON_m2  a  donné  son  approbation  en_dépit_de__prep  le__det  scandale  causé  récemment |  M.  _PERSON_m2  a  donné  son  approbation  en  dépit  de__prep  le__det  scandale  causé  récemment
** Observations
Ca marche avec la suite de la phrase néanmoins :
stern@[~/Dev/ALPC/nomos/linking]~$ echo "M. Obama a donné son approbation en dépit du scandale causé récemment par la diffusion d'une vidéo montrant un membre de la famille royale du président des Emirats en train de torturer un homme." | lingpipe --config /Users/stern/Dev/ALPC/sxpipe/sxpipe-afp.conf --last recode-final | perl -pe 's/\{.+?\}//g'
 M.  _PERSON_m2  a  donné  son  approbation ( en  dépit  du | ( en_dépit_de__prep |  en  dépit  de__prep)  le__det)  scandale  causé  récemment  par  la  diffusion  d'  une  vidéo  montrant  un  membre ( de_la |  de  la)  famille  royale ( du |  de__prep  le__det)  président ( des |  de__prep  les__det) (( Emirats |  émirats) |  Emirats |  émirats) ( en_train_de |  en  train  de)  torturer  un  homme  .
stern@[~/Dev/ALPC/nomos/linking]~$ echo "M. Obama a donné son approbation en dépit du scandale causé récemment par la diffusion d'une vidéo montrant un membre de la famille royale du président des Emirats en train de torturer un homme." | lingpipe --config /Users/stern/Dev/ALPC/sxpipe/sxpipe-afp.conf --last recode-final | dag2pdag | perl -pe 's/\{.+?\}//g'
 M.  _PERSON_m2  a  donné  son  approbation ( en  dépit  du |  en_dépit_de__prep  le__det |  en  dépit  de__prep  le__det)  scandale  causé  récemment  par  la  diffusion  d'  une  vidéo  montrant  un  membre ( de_la |  de  la)  famille  royale ( du |  de__prep  le__det)  président ( des |  de__prep  les__det) (  Emirats |  émirats |  Emirats |  émirats) ( en_train_de |  en  train  de)  torturer  un  homme  .
** Debug
   

* sxpipe - segmentation sur guillemets (+ segmentation en phrases)
** Reporter
| Rosa Stern | INRIA | stern.rosa@gmail.com | 11/03/11 |
** Description
Des formes sur un token où le guillemet n'est pas décollé à gauche, et le guillemet + le point à droite.
** Exemple
*** Input (les guillemets de but et de fin du texte entier font bien partie de l'input)
"Quand il a le blues, il file au pays du rock'n roll. Que voulez-vous, il est comme ça, Nicolas Sarkozy, il adore Elvis. Mais il aime aussi le cinéma. John Ford et Marilyn. Il l'avait clamé bien haut, jadis devant le Congrès. C'était en novembre 2007. Depuis, il est vrai, l'eau a coulé sous les ponts du Potomac. Obama a remplacé Bush et les relations bilatérales sont un tantinet plus compliquées. Qu'importe. La visite du couple présidentiel français outre-Atlantique, “c'est que du bonheur”. D'abord, parce que les Obama sont super sympas. Ils invitent aujourd'hui leurs “amis” dans les appartements privés de la Maison-Blanche. On ne connaît pas le menu, ni la couleur de la vaisselle, mais l'initiative est “sans précédent”, précise-t-on. Du coup, il y aura de quoi immortaliser la scène, façon “Point de vue images du monde”. Oh! il y a bien deux ou trois différends entre les deux dirigeants: le nucléaire iranien, la question afghane, le désarmement, la définition du capitalisme... Mais pas de quoi s'énerver. Barack est un complice."
*** Output
formes '"' + 'Point' sur '"Point' et 'monde' + '"' + '.' sur token E13F19 'monde".' (ce dag est très bizarre pour moi, 'Point' est répété sans token etc...)
Extrait :
({<F id="E13F1">Du</F>} dû | ({<F id="E13F1">Du</F>} dû | {<F id="E13F1">Du</F>} du) | {<F id="E13F1">Du</F>} de__prep {<F id="E13F1">Du</F>} le__det) {<F id="E13F2">coup</F>} coup {<F id="E13F3">,</F>} , {<F id="E13F4">il</F>} il {<F id="E13F5">y</F>} y {<F id="E13F6">aura</F>} aura {<F id="E13F7">de</F>} de {<F id="E13F8">quoi</F>} quoi {<F id="E13F9">immortaliser</F>} immortaliser {<F id="E13F10">la</F>} la {<F id="E13F11">scène</F>} scène {<F id="E13F12">,</F>} , {<F id="E13F13">façon</F>} façon ({<F id="E13F14">"Point</F>} " | {<F id="E13F14">"Point</F>} _EPSILON)Point {<F id="E13F15">de</F>} de {<F id="E13F16">vue</F>} vue {<F id="E13F17">images</F>} images ({<F id="E13F18">du</F>} du | {<F id="E13F18">du</F>} de__prep {<F id="E13F18">du</F>} le__det) {<F id="E13F19">monde".</F>} monde (({<F id="E13F19">monde".</F>} " | {<F id="E13F19">monde".</F>} _EPSILON) | (({<F id="E13F19">monde".</F>} " | {<F id="E13F19">monde".</F>} _EPSILON) | {<F id="E13F19">monde".</F>} .)) ({<F id="E13F20">Oh</F>} Oh | {<F id="E13F20">Oh</F>} oh) {<F id="E13F21">!</F>} ! ({<F id="E13F22">il</F> <F id="E13F23">y</F> <F id="E13F24">a</F>} il_y_a | {<F id="E13F22">il</F>} il {<F id="E13F23">y</F>} y {<F id="E13F24">a</F>} a) {<F id="E13F25">bien</F>} bien {<F id="E13F26">deux</F>} _NUMBER {<F id="E13F27">ou</F>} ou {<F id="E13F28">trois</F>} _NUMBER {<F id="E13F29">différends</F>} différends {<F id="E13F30">entre</F>} entre {<F id="E13F31">les</F>} les {<F id="E13F32">deux</F>} _NUMBER {<F id="E13F33">dirigeants</F>} dirigeants {<F id="E13F34">:</F>} : {<F id="E13F35">le</F>} le {<F id="E13F36">nucléaire</F>} nucléaire {<F id="E13F37">iranien</F>} iranien {<F id="E13F38">,</F>} , ({<F id="E13F39">la</F> <F id="E13F40">question</F>} la_question | {<F id="E13F39">la</F>} la {<F id="E13F40">question</F>} question) {<F id="E13F41">afghane</F>} afghane {<F id="E13F42">,</F>} , {<F id="E13F43">le</F>} le {<F id="E13F44">désarmement</F>} désarmement {<F id="E13F45">,</F>} , {<F id="E13F46">la</F>} la {<F id="E13F47">définition</F>} définition ({<F id="E13F48">du</F>} du | {<F id="E13F48">du</F>} de__prep {<F id="E13F48">du</F>} le__det) {<F id="E13F49">capitalisme</F>} capitalisme {<F id="E13F50">...</F>} ...

##DAG BEGIN
1	{<F id="E13F1">Du</F>} de__prep 	2
1	{<F id="E13F1">Du</F>} du 	3
1	{<F id="E13F1">Du</F>} dû 	3
2	{<F id="E13F1">Du</F>} le__det 	3
3	{<F id="E13F2">coup</F>} coup 	4
4	{<F id="E13F3">,</F>} , 	5
5	{<F id="E13F4">il</F>} il 	6
6	{<F id="E13F5">y</F>} y 	7
7	{<F id="E13F6">aura</F>} aura 	8
8	{<F id="E13F7">de</F>} de 	9
9	{<F id="E13F8">quoi</F>} quoi 	10
10	{<F id="E13F9">immortaliser</F>} immortaliser 	11
11	{<F id="E13F10">la</F>} la 	12
12	{<F id="E13F11">scène</F>} scène 	13
13	{<F id="E13F12">,</F>} , 	14
14	{<F id="E13F13">façon</F>} façon 	15
15	{<F id="E13F14">"Point</F>} _EPSILON 	16
15	{<F id="E13F14">"Point</F>} " 	16
16	 Point 	17
17	{<F id="E13F15">de</F>} de 	18
18	{<F id="E13F16">vue</F>} vue 	19
19	{<F id="E13F17">images</F>} images 	20
20	{<F id="E13F18">du</F>} de__prep 	21
20	{<F id="E13F18">du</F>} du 	22
21	{<F id="E13F18">du</F>} le__det 	22
22	{<F id="E13F19">monde".</F>} monde 	23
23	{<F id="E13F19">monde".</F>} . 	24
23	{<F id="E13F19">monde".</F>} _EPSILON 	24
23	{<F id="E13F19">monde".</F>} " 	24
24	{<F id="E13F20">Oh</F>} oh 	25
24	{<F id="E13F20">Oh</F>} Oh 	25
25	{<F id="E13F21">!</F>} ! 	26
26	{<F id="E13F22">il</F>} il 	27
26	{<F id="E13F22">il</F> <F id="E13F23">y</F> <F id="E13F24">a</F>} il_y_a 	29
27	{<F id="E13F23">y</F>} y 	28
28	{<F id="E13F24">a</F>} a 	29
29	{<F id="E13F25">bien</F>} bien 	30
30	{<F id="E13F26">deux</F>} _NUMBER 	31
31	{<F id="E13F27">ou</F>} ou 	32
32	{<F id="E13F28">trois</F>} _NUMBER 	33
33	{<F id="E13F29">différends</F>} différends 	34
34	{<F id="E13F30">entre</F>} entre 	35
35	{<F id="E13F31">les</F>} les 	36
36	{<F id="E13F32">deux</F>} _NUMBER 	37
37	{<F id="E13F33">dirigeants</F>} dirigeants 	38
38	{<F id="E13F34">:</F>} : 	39
39	{<F id="E13F35">le</F>} le 	40
40	{<F id="E13F36">nucléaire</F>} nucléaire 	41
41	{<F id="E13F37">iranien</F>} iranien 	42
42	{<F id="E13F38">,</F>} , 	43
43	{<F id="E13F39">la</F>} la 	44
43	{<F id="E13F39">la</F> <F id="E13F40">question</F>} la_question 	45
44	{<F id="E13F40">question</F>} question 	45
45	{<F id="E13F41">afghane</F>} afghane 	46
46	{<F id="E13F42">,</F>} , 	47
47	{<F id="E13F43">le</F>} le 	48
48	{<F id="E13F44">désarmement</F>} désarmement 	49
49	{<F id="E13F45">,</F>} , 	50
50	{<F id="E13F46">la</F>} la 	51
51	{<F id="E13F47">définition</F>} définition 	52
52	{<F id="E13F48">du</F>} de__prep 	53
52	{<F id="E13F48">du</F>} du 	54
53	{<F id="E13F48">du</F>} le__det 	54
54	{<F id="E13F49">capitalisme</F>} capitalisme 	55
55	{<F id="E13F50">...</F>} ... 	56
##DAG END

** Autres exemples si besoin
(mémo Rosa : 
outputs/20101/16.output.xml:125021: 'brouillard".'
outputs/20101/10.output.xml:40200: 'temps".'
outputs/20101/12.output.xml:4299: '"France'
)
** Observations
Apparemment ça arriverait dans les cas où une grande partie de texte est entièrement entre
guillemets (la segmentation en phrases produit du coup des guillemets "orphelins" de leur jumeau).
Si on regarde tout l'output du texte entier, il y a d'autres cas ('bonheur".'), apparemment toujours
pour des histoires de guillemets dépareillés etc...

* sxpipe - shortest_path - reste ambiguïtés
** Reporter
| Rosa Stern | INRIA | stern.rosa@gmail.com | 20/02/11 |
** Description
** Exemple
171898:( {<F id="E110737F1">Sans</F>} Sans | {<F id="E110737F1">Sans</F>} sans ) {<F id="E110737F2">donner</F>} donner {<F id="E110737F3">de</F>} de ( {<F id="E110737F4">"</F>} " | {<F id="E110737F4">"</F>} _EPSILON ) {<F id="E110737F5">blanc-seing</F>} blanc-seing ( {<F id="E110737F6">"</F>} " | {<F id="E110737F6">"</F>} _EPSILON ) {<F id="E110737F7">à</F>} à {<F id="E110737F8">la</F>} la {<F id="E110737F9">direction</F>} direction {<F id="E110737F10">,</F>} , {<F id="E110737F11">Marie-Pierre</F>} Marie-Pierre {<F id="E110737F12">Vieu</F>} _LOCATION 2000000006453510:Vieu {<F id="E110737F13">,</F>} , {<F id="E110737F14">membre</F>} membre {<F id="E110737F15">de</F>} de {<F id="E110737F16">l'</F>} l' {<F id="E110737F17">exécutif</F>} exécutif {<F id="E110737F18">qui</F>} qui {<F id="E110737F19">s'</F>} s' {<F id="E110737F20">était</F>} était {<F id="E110737F21">associée</F>} associée {<F id="E110737F22">aux</F>} à__prep {<F id="E110737F22">aux</F>} les__det {<F id="E110737F23">communistes</F>} communistes {<F id="E110737F24">unitaires</F>} unitaires {<F id="E110737F25">au</F>} à__prep {<F id="E110737F25">au</F>} le__det {<F id="E110737F26">34e</F>} _NUMBER {<F id="E110737F27">congrès</F>} congrès {<F id="E110737F28">en</F>} en {<F id="E110737F29">décembre</F> <F id="E110737F30">2008</F>} _DATE_year {<F id="E110737F31">,</F>} , {<F id="E110737F32">ne</F>} ne {<F id="E110737F33">voit</F>} voit ( {<F id="E110737F34">"</F>} " | {<F id="E110737F34">"</F>} _EPSILON ) ( {<F id="E110737F35">pas</F> <F id="E110737F36">d'</F>} pas_d' | {<F id="E110737F35">pas</F>} pas {<F id="E110737F36">d'</F>} d' ) {<F id="E110737F37">autres</F>} autres {<F id="E110737F38">solutions</F>} solutions {<F id="E110737F39">que</F>} que {<F id="E110737F40">d'aider</F> <F id="E110737F41">\(</F> <F id="E110737F42">s</F> <F id="E110737F43">\)</F>} d' {<F id="E110737F40">d'aider</F> <F id="E110737F41">\(</F> <F id="E110737F42">s</F> <F id="E110737F43">\)</F>} _PERSON_m 0:d'aider_ ( _s_ ) {<F id="E110737F44">parti</F>} parti ( {<F id="E110737F45">"</F>} " | {<F id="E110737F45">"</F>} _EPSILON ) {<F id="E110737F46">car</F>} car {<F id="E110737F47">avec</F>} avec {<F id="E110737F48">le</F>} le {<F id="E110737F49">FG</F>} FG {<F id="E110737F50">,</F>} , ( {<F id="E110737F51">"</F>} " | {<F id="E110737F51">"</F>} _EPSILON ) {<F id="E110737F52">les</F>} les {<F id="E110737F53">communistes</F>} communistes {<F id="E110737F54">ont</F>} ont {<F id="E110737F55">bougé</F>} bougé ( {<F id="E110737F56">"</F>} " | {<F id="E110737F56">"</F>} _EPSILON ) {<F id="E110737F57">et</F>} et ( {<F id="E110737F58">"</F>} " | {<F id="E110737F58">"</F>} _EPSILON ) {<F id="E110737F59">ça</F>} ça {<F id="E110737F60">m'</F>} m' {<F id="E110737F61">encourage</F>} encourage {<F id="E110737F62">à</F>} à {<F id="E110737F63">continuer</F>} continuer ( {<F id="E110737F64">"</F>} " | {<F id="E110737F64">"</F>} _EPSILON ) {<F id="E110737F65">,</F>} , {<F id="E110737F66">dit</F>} dit {<F id="E110737F67">-elle</F>} -elle {<F id="E110737F68">.</F>} .

