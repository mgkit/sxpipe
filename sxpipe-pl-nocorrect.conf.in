# sxpipe reloaded

# Language
lang    = pl

# Path to different
sxpipe  = @datadir@/sxpipe
sxspell = @datadir@/sxspell

# Verbose flag
verbose

# Component list
components = \
	tag_meta \
	xces2txt \
	recode-u8l2 \
	email \
        url \
	date \
	tel \
	heure \
	adresse \
	smiley \
	mtponct \
	numprefix \
	number \
	formattage \
	remove_inner_1 \
	segment \
	include_tabs \
	rebuild \
	glue \
	normalize \
	text2dag \
	epsilonize \
	recode-l2u8


# raw text to tokens
[recode-u8l2]
cmd = iconv
options = -f UTF-8 -t L2
desc = passe de l'UTF-8 au Latin 2

[xces2txt]
cmd = $sxpipe/txt2tok/xces2txt.pl
options = $*
desc = extrait les tokens depuis le format XCES

[tag_meta]
cmd = $sxpipe/txt2tok/tag_meta.pl
desc = protection des meta-caract�res

[email]
cmd = $sxpipe/txt2tok/gl_email.pl
options = $*
desc = reconnaissance des email

[url]
cmd = $sxpipe/txt2tok/gl_url.pl
options = $* -l=$lang
desc = reconnaissance des url
depend = email

[date]
cmd = $sxpipe/txt2tok/gl_date.pl
options = $* -l=$lang
desc = reconnaissance des dates

[tel]
cmd = $sxpipe/txt2tok/gl_tel.pl
options = $*
desc = reconnaissance des n� de t�l�phone

[heure]
cmd = $sxpipe/txt2tok/gl_heure.pl
options = $* -l=$lang
desc = reconnaissance des heures

[adresse]
cmd = $sxpipe/txt2tok/gl_adresses.pl
options = $* -l=$lang
desc = reconnaissance des adresses

[numprefix]
cmd = $sxpipe/txt2tok/gl_numtruc.pl
options = $*
desc = reconnaissance des prefixes numeriques (genre 4-place)

[number]
cmd = $sxpipe/txt2tok/gl_number.pl
options = $* -l=$lang
desc = reconnaissance des nombres

[smiley]
cmd = $sxpipe/txt2tok/gl_smiley.pl
desc = reconnaissance des smileys

[mtponct]
cmd = $sxpipe/txt2tok/gl_mtponct.pl
options = $* -l=$lang
desc = reconnaissance des ponctuations multitokens

[qword]
cmd = $sxpipe/txt2tok/gl_qword.pl
desc = reconnaissance des mots cit�s

[formattage]
cmd = $sxpipe/txt2tok/gl_format.pl
desc = reconnaissance des mots formatt�s (pseudo-gras type _mot_ ou *mot*)

[segment]
cmd = $sxpipe/txt2tok/segmenteur.pl
options = -af=@pollexdir@/pctabr -p=r $* -l=$lang
desc = segmentation

# tokens to compound components
[include_tabs]
cmd = $sxpipe/tok2cc/include_tabs_in_comments.pl
desc = includes tabulations separating words inside comments for later use

[tag_unknown]
cmd = sxspellerpol
options = -cl -pl -cq -ch -pp -ps -c -nsc -d -lw 1
desc = marquage des tokens inconnus

[sigles]
cmd = $sxpipe/tok2cc/gl_sigles.pl
desc = reconnaissance des sigles

[np]
cmd = $sxpipe/tok2cc/gl_np.pl
desc = reconnaissance des noms propres

[etr]
cmd = $sxpipe/tok2cc/gl_etr.pl
options = -no_gl -l=$lang
desc = reconnaissance des expressions langue �trang�re

[untag_unknown]
cmd = $sxpipe/tok2cc/untag_unknown.pl
desc = d�marquage des tokens inconnus

[remove_inner_1]
cmd = $sxpipe/tok2cc/remove_inner_ne.pl
desc = desencapsulation des entit�es nomm�es

[remove_inner_2]
cmd = $sxpipe/tok2cc/remove_inner_ne.pl
desc = desencapsulation des entit�es nomm�es

[rebuild]
cmd = $sxpipe/tok2cc/rebuild_easy_tags.pl

[glue]
cmd = $sxpipe/tok2cc/glue_easy_sentences.pl

[correct]
cmd = sxspellerpol
options = -cl -pl -cq -ch -pp -ps -c -lw 5 -t -sc -mw 5
desc = correction effective

[normalize]
cmd = $sxpipe/tok2cc/normalize_blanks.pl
desc = normalisation des espaces et de pourcentages

# compound components to DAG
[text2dag]
cmd = $sxpipe/cc2dag/wraptext2dag.pl
options = -cl -pl -cq -ch -pp -ps -lw 5 -sc -mw 5 -mcn 2 -l $lang
desc = analyse des phrases

[epsilonize]
cmd = $sxpipe/epsilonize_metawords.pl
desc = met en alternative avec _EPSILON les m�tamots (quotes, crochets...)

[unfold]
cmd = $sxpipe/cc2dag/wrapunfolddag.pl
desc = d�pliage des DAGs

[recode-l2u8]
cmd = iconv
options = -f L2 -t UTF-8
desc = passe du Latin 2 � l'UTF-8
