#!/usr/bin/env perl
# $Id$

my $line = 0;
my $step = 10;

while (<>) {
    print STDERR "$.\r" unless $. % $step;
    print $_;
}
print STDERR "\n";
