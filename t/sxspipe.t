#!/usr/bin/perl -w
use Test::More;
use File::Basename;
use strict;

############################################
# Purpose    : test sxpipe output
# Comments   : check if there is no missing word


# compute plan
my @sentences = <DATA>;
plan tests => scalar @sentences;

sub mysort {
    my ($ea,$fa) = ($a =~ /E(\d+(?:\.\d+)?)F(\d+)/);
    my ($eb,$fb) = ($b =~ /E(\d+(?:\.\d+)?)F(\d+)/);
    return ($ea <=> $eb || $fa <=> $fb);
}

foreach my $s (@sentences) {
    chomp $s;
    my @edges    = split /\n/, `echo \"$s\" | ./sxpipe | dag2udag`;

    ## store dag words
    my %dagwords;

    foreach my $edge (@edges) {
	next if ($edge =~ m{^##DAG\s+(BEGIN|END)});

	while ($edge =~ m{<F\s+id="(.+?)"\s*>\s*(.*?)\s*</F>}og) {
	    my ($id,$w) = ($1,$2);
	    $dagwords{$1} ||= $2;
	}
    }

    my @expectedwords = split /\s+/, $s;

    my @dagwords;
    foreach (sort mysort keys %dagwords) {
	push @dagwords, $dagwords{$_};
    }
    is(join("",@dagwords), join("",@expectedwords), $s);

    ## todo : ajouter TODO pour tests qui �chouent pour passer 'make check'
}


__DATA__
Il mange une pomme.
Il s'agit de raconter une histoire.
et bonne journ�e bon courage pour ce mercredi 6 octobre dans une petite minute le journal de l'�conomie deuxi�me �dition
et bonne journ�e bon courage pour ce mercredi 6 octobre dans une petite minute le journal de l'�conomie deuxi�me �dition mais tout de suite les grands titres de l'actualit� Maude Bayeu bonjour
Les titres du journal de l'�conomie avec tout d'abord les p�cheurs.
