#!/usr/bin/env perl
# -*- perl -*-
eval 'exec /usr/bin/env perl -S $0 ${1+"$@"}'
    if 0;

use strict;
use Event::XML::Sync;

#use Getopt::Long;
#use Term::ReadKey;

#package main;

# La variable Globale $directory est d�finie dans 
# le programme principal et porte sur tous les modules appel�s

use FindBin qw($Bin);

my $result = "";

# Table des tokens : les mots font r�f�rences aux tokenid pour retrouver les formes, tags initiaux, etc.
my %TableToken=();
my $source="";
my $target="";

my $cmd = "/usr/local/bin/lexed -p DicoTAG";
my $converter = "$Bin/Multext2DyALog";

use Getopt::Mixed "nextOption";
Getopt::Mixed::init("c=s cmd>c",
		    "m=s"
		   );
while (my ($option, $value) = nextOption()) {
  $cmd = $value, next if ($option eq 'c');
  $converter = $value, next if ($option eq 'm');
}
Getopt::Mixed::cleanup();

$cmd .= " -f '' '|' '\n' 'unkown key'";

require "$converter";

converter_init();

print "%% Wrap DyALog cmd=$cmd\n";

my @Transitions=();

Event::XML::Sync->new(
		      callbacks => {
				    done => \&done
				   },
		      cmd => $cmd,
		      twig =>
		      [
		       twig_handlers =>
		       {
			sentence => \&sentence_process,
			w => \&w_process,
			fsm => \&fsm_process
		       },
		       pretty_print => 'indented',
		       output_encoding => 'ISO-8859-1',
		       output_filter => 'latin1',
		       input_filter => 'latin1'
		      ],
		      parser =>  [ProtocolEncoding => 'ISO-8859-1']
#		      , debug => 1
		     );

sub done {
#  print "Done\n";
  my $sync = shift;
  $sync->{twig}->purge;
}

sub sentence_process {
  my ($t,$s) =@_;
  $t->{sync}->delayed_run(Delayed::Sentence->new(text=>$s->trimmed_text()));
}

sub Delayed::Sentence::new {
  my $class = shift;
  return bless({@_},$class);
}

sub Delayed::Sentence::run {
  my $self=shift;
  print "%% Sentence: $self->{text}\n";
}

sub fsm_process {
  my ($t,$fsm) = @_;
  $t->{sync}->delayed_run(Delayed::Fsm->new(elt=>$fsm,twig=>$t));
}

sub Delayed::Fsm::new {
  my $class = shift;
  return bless({@_},$class);
}

sub Delayed::Fsm::run {
  my $self=shift;
  my $fsm = $self->{elt};
  my $twig = $self->{twig};
  my $target = $fsm->last_child('transition')->att('target');
  #  print "FSM Sync $fsm\n";
  print "'N'($target).\n";
  $twig->purge_up_to($fsm);
}

sub w_process {
  my ($t,$w) = @_;
  my $sync = $t->{sync};
  my $idrefs = $w->att('idref');
  my $transition = $w->parent('transition');
  my $from = $transition->att('source');
  my $to = $transition->att('target');
  my @content = ();
  foreach my $idref (split(/\s+/,$idrefs)) {
    my ($token) = $w->parent('fsm')->descendants("token[\@id=\"$idref\"]");
    my $tokencontent = $token->trimmed_text(); 
    push(@content,$tokencontent) if ($tokencontent);  
  }
  my $content = join(' ',@content);
  my $tag = $w->att('tag');
  # Decode Multext information into DyALog feature format
  my @results=&decode($tag);
  foreach $result (@results){
    my %lexiconInfo=(from=>$from,to=>$to);
    my @tmp=split(/\t/, $result);
    my $cat = (defined $tmp[0]) ? $tmp[0] : '_';
    $lexiconInfo{'lex'}=dyalog_quote($content) if ($content);
    if ($cat ne "_"){
      $lexiconInfo{'cat'}=dyalog_quote($cat);
      $lexiconInfo{'top'}="$cat\{$tmp[1]\}" if (defined $tmp[1]);
    }
    # we send tp lexed and synchoronize
    $sync->process(bless({'w'=>$w,%lexiconInfo},'LexiconInfo'),
		   "${content}_$cat\n");
  }
}

sub LexiconInfo::sync {
  my $self = shift;
  my $sync = shift;
  my $input = shift;
  my $w = $self->{'w'};
  my $from = $self->{'from'};
  my $to = $self->{'to'};
  delete $self->{'from'};
  delete $self->{'to'};
  $sync->debug("Sync Callback input=$input cat=$self->{cat}");
  delete $self->{'w'};
  if ($input =~ /^unkown\s+key/) {
    # Add Multex info if the word is not in the grammar lexicon
    # $self->{'lemma'} = dyalog_quote($w->att('lemma'));
    emit_transition($from,$to,$self);
  } else {
    # Else add info from the grammar lexicon
    # info | info... | info
    # where info is
    #    TreeName \t Lemma \t traits \t Equation \t CoAnchors
    foreach my $tb (split(/\|/, $input)){
      my @t=split(/\t/, $tb);
      my %entry=();
      $entry{'lemma'}=dyalog_quote($t[1]) if ($t[1]);
      if ($t[2] eq '_'){
	$entry{'cat'}=$self->{cat};
      } else {
	my $top = $t[2];
	my ($cat) = ($top =~ /^(\w+)/);
	@entry{'top','cat'}= ($top,$cat);
      }
      @entry{'lex','anchor'}=
	($self->{lex},
	 "tag_anchor{name=>@t[0],coanchors=>@t[4],equations=>@t[3]}");
      emit_transition($from,$to,\%entry);
    }
  }
}

sub emit_transition {
    my $from = shift;
    my $to = shift;
    my $entry = shift;
    $entry = join(",\n\t",
		  map( "$_=>$entry->{$_}",
		       sort keys %$entry));
    my $Transition= <<EOF;
'C'($from, 
    lemma{$entry},
    $to).
EOF
    unless (grep($Transition eq $_,@Transitions)) {
      print $Transition;
      push(@Transitions, $Transition);
    }
}


sub dyalog_quote {
  my $s = shift;		# assume $s not empty
  $s =~ s/\'/\'\'/;
  return "'$s'";
}

__END__
