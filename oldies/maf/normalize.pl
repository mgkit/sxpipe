#!/usr/bin/env perl
# -*- perl -*-
eval 'exec /usr/bin/env perl -S $0 ${1+"$@"}'
    if 0;

use strict;
use FindBin qw($Bin $Script);
use XML::Twig;

my $opt_fsm=0;
my $opt_fs=0;
my $opt_span=0;
use Getopt::Mixed "nextOption";
Getopt::Mixed::init("fs span fsm");
while (my ($option, $value) = nextOption()) {
  $opt_fs=1, next if ($option eq 'fs');
  $opt_span=1, next if ($option eq 'span');
  $opt_fsm=1, next if ($option eq 'fsm');
}
Getopt::Mixed::cleanup();


use Multext;

Multext::converter_init();

my $counter=0;

my $t = XML::Twig->new(
                       twig_handlers =>
                       {
			'token' => \&token_process,
			'w' => \&w_process,
			'state' => sub { $_->delete unless ($opt_fsm); },
			'fsm' => \&fsm_process
                       },
                       pretty_print => 'indented',
                       output_encoding => 'ISO-8859-1',
                       output_filter => 'latin1'
                      );

$t->parse(\*STDIN);
$t->flush();

sub w_process {
  my ($t,$elt) = @_;
  my $tag = $elt->att('tag');
  my $entry = $elt->att('lemma');
  $elt->del_att('lemma','tag');
  if (!$opt_fs) {
    $tag = Multext::fs_tag($tag);
    $elt->set_att('tag' => $tag);
  } else {
    my $fs = Multext::fs_expand($tag);
    $fs = parse XML::Twig::Elt($fs,
			       pretty_print => 'indented',
			       output_encoding => 'ISO-8859-1',
			       output_filter => 'latin1'
			      );
    $fs->paste($elt);
  }
  $entry =~ s/\s/_/og;
  $elt->set_att('entry' => "lexicon:$entry") if ($entry);
  $elt->set_gi('wordForm');
}

sub token_process {
  my ($t,$elt) = @_;
  my $v = $elt->att('form');
  my $text =$elt->trimmed_text;
  my ($from,$to) = ($counter,$counter+1);
  ++$counter if ($text);
  $elt->del_att('form','tag');
  $elt->set_att('value' => $v );
  if ($opt_span){
    $elt->set_att('from' => $from);
    $elt->set_att('to' => $to);
    $elt->cut_children();
  }
#  $elt->set_content('#EMPTY');
}

sub fsm_process {
  return if ($opt_fsm);
  my ($t,$elt) = @_;
  my $in_fsm=0;
  my $trans = $elt->first_child('transition');
  my $fsm;
  my $last;
  while ($trans) {
    my $source = $trans->att('source');
    my $next = $trans->next_sibling('transition');
    my $tmp;
    if (!$in_fsm && ! ($tmp=$trans->next_sibling("transition[\@source=\"$source\"]"))){
      $trans->erase;
    } elsif (!$in_fsm) {
      $tmp = $tmp->prev_sibling('transition');
      $last = $tmp->att('target');
      $in_fsm=1;
      $trans->wrap_in('fsm');
      $fsm=$trans->parent;
    } elsif ($trans->att('source') eq $last) {
      $in_fsm=0;
      next;
    } else {
      $trans->move('last_child' => $fsm);
    }
    $trans = $next;
  }
  $elt->erase;
}

exit 0;

__END__
