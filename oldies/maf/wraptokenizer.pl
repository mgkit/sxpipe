#!/usr/bin/env perl

use XML::Twig;
use Getopt::Mixed "nextOption";
use strict;
use warnings;

my $bin  = "tokenizer --nocomposition";
my ($data, $tagset, $verb);
my %tokens;

Getopt::Mixed::init("b=s d=s t=s v bin>b data>d tagset>t");
while (my ($option, $value) = nextOption()) {
    $bin    = $value, next if ($option eq 'b');
    $data   = $value, next if ($option eq 'd');
    $tagset = $value, next if ($option eq 't');
    $verb   = 1, next if ($option eq 'v');
}
Getopt::Mixed::cleanup();

my $cmd = $bin;
$cmd .= " -p $data" if $data;

if ($tagset) {
    $tagset = "Lingua::TagSet::$tagset";
    eval "require $tagset" or die "Unknown tagset $tagset";
}

my $twig = new XML::Twig (
    TwigRoots => {
	'token'             => \&token,
	'document/fsm'      => \&fsm,
	'document/alt'      => \&alt,
	'document/wordForm' => \&word_form,
    },
    TwigPrintOutsideRoots => 1,
    pretty_print          => 'indented',
    output_encoding       => 'ISO-8859-1',
);

open (OUT, "$cmd |") or die "can't run $cmd";
$twig->parse(\*OUT);
close(OUT);

sub token {
    my ($twig, $token) = @_;
    return if $token->att('value') =~ /\/det\//;
    $token->set_att('value', 'du') if $token->att('value') =~ /\/prep\//;
    $tokens{$token->att('id')} = $token;
    $token->print();
    $twig->purge();
}
    
sub fsm {
    my ($twig, $fsm) = @_;

    convert($_) foreach $fsm->descendants('wordForm');

    my @items = map { $_->first_child()->cut() } $fsm->children('transition');
    my ($entry, $tokens);
    if ($items[0]->name() eq 'wordForm') {
	$entry = $items[0]->att('entry');
	$tokens = $items[0]->att('tokens');
    } else {
	$entry = $items[0]->first_child()->att('entry');
	$tokens = $items[0]->first_child()->att('tokens');
    }
    
    my $results = XML::Twig::Elt->new(
	'results',
	{ entry => $entry, tokens => $tokens }
    );

    my $result = XML::Twig::Elt->new(
	'result',
	{ source => 'tokenizer' }
    );
    $result->paste('last_child', $results);

    my $alt = XML::Twig::Elt->new('alt');
    $alt->paste('last_child', $result);

    $items[0]->paste('last_child', $alt);

    my $seq = XML::Twig::Elt->new('seq');
    $seq->paste('last_child', $alt);

    $_->paste('last_child', $seq) foreach @items[1 .. $#items];

    $results->print();
    $twig->purge();
}
    
sub alt {
    my ($twig, $alt) = @_;

    convert($_) foreach $alt->children('wordForm');

    my $entry  = $alt->first_child()->att('entry');
    my $tokens = $alt->first_child()->att('tokens');
    
    my $results = XML::Twig::Elt->new(
	'results',
	{ entry => $entry, tokens => $tokens }
    );

    my $result = XML::Twig::Elt->new(
	'result',
	{ source => 'tokenizer' }
    );
    $result->paste('last_child', $results);

    $alt->cut();
    $alt->paste($result);

    $results->print();
    $twig->purge();
}

sub word_form {
    my ($twig, $word_form) = @_;
    convert($word_form);

    my $entry  = $word_form->att('entry');
    my $tokens = $word_form->att('tokens');
    
    my $results = XML::Twig::Elt->new(
	'results',
	{ entry => $entry, tokens => $tokens }
    );

    my $result = XML::Twig::Elt->new(
	'result',
	{ source => 'tokenizer' }
    );
    $result->paste('last_child', $results);

    $word_form->cut();
    $word_form->paste($result);

    $results->print();
    $twig->purge();
}

sub convert {
    my ($word_form) = @_;

    my $entry = $word_form->att('entry');
    my $info = $word_form->att('tag');

    # compute lemma and tag
    my ($lemma, $tag);
    if ($info eq '-unknown-') {
	$lemma = $entry eq '_NUMBER' ? '_NUMBER' : $entry;
	$tag   = '_UNKNOWN';
    } else {
	($lemma, $tag) = split(/\t/, $word_form->att('tag'));
    }
    $tag = $tagset->tag2string($tag) if $tagset;
    $word_form->set_att('tag', $tag);
    $word_form->set_att('lemma', $lemma);

    # special cases
    switch: {
	if ($entry eq '_NUMBER') {
	    my $token = $word_form->att('tokens');
	    $entry = $tokens{$token}->text();
	    $word_form->set_att('entry', $entry);
	    last switch;
	}

	if ($entry eq "de__prep") {
	    $word_form->set_att('entry', 'de');
	    last switch;
	}

	if ($entry eq "de__prep le__det/") {
	    $word_form->set_att('entry', 'du');
	    $word_form->set_att('lemma', 'du');
	    my $token = (split(/ /, $word_form->att('tokens')))[0];
	    $word_form->set_att('tokens', $token);
	    last switch;
	}

	if ($entry eq "de__prep les__det/") {
	    $word_form->set_att('entry', 'des');
	    $word_form->set_att('lemma', 'des');
	    my $token = (split(/ /, $word_form->att('tokens')))[0];
	    $word_form->set_att('tokens', $token);
	    last switch;
	}

	if ($entry eq "le__det") {
	    $word_form->set_att('entry', 'le');
	    $word_form->set_att('lemma', 'le');
	    my $token = 't' . (substr($word_form->att('tokens'), 1) - 1);
	    $word_form->set_att('tokens', $token);
	    last switch;
	}

	if ($entry eq "les__det") {
	    $word_form->set_att('entry', 'les');
	    $word_form->set_att('lemma', 'les');
	    my $token = 't' . (substr($word_form->att('tokens'), 1) - 1);
	    $word_form->set_att('tokens', $token);
	    last switch;
	}

	if ($entry =~ /� le__det/) {
	    $entry =~ s/� le__/au/;
	    $word_form->set_att('entry', $entry);
	    my @tokens = split(' ', $word_form->att('tokens'));
	    splice @tokens, 1, 1;
	    $word_form->set_att('tokens', join(' ', @tokens));
	}

    }

    $word_form->del_att('weight');
}
