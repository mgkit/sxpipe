#!/usr/bin/env perl

use XML::Twig;
use Getopt::Mixed "nextOption";
use Lingua::Features;
use List::MoreUtils qw/any/;
use Carp;
use strict;
use warnings;

my $verb;

Getopt::Mixed::init("v");
while (my ($option, $value) = nextOption()) {
    $verb = $value, next if ($option eq 'v');
}
Getopt::Mixed::cleanup();

my $twig = new XML::Twig (
    twig_handlers   => { fsm => \&fsm },
    pretty_print    => 'indented',
    output_encoding => 'ISO-8859-1',
);

$twig->parse(\*STDIN);
$twig->flush();

sub fsm {
    my ($twig, $fsm) = @_;

    # extract tokens list
    my @tokens =  $fsm->children('token');
    $_->cut() foreach @tokens;

    # extract wordforms list
    my @wordforms;
    my $init = $fsm->att('init');
    my $final = $fsm->att('final');
    my $current = $init;

    while ($current != $final) {
	my $transition;
	my @transitions = $fsm->children('transition[@source="' . $current . '"]');

	my @results;
	my @lexed = grep { $_->first_child()->att('source') eq 'lexed' } @transitions;
	if (@lexed == 1) {
	    # only one choice from lexicon
	    @results = @lexed;
	} else {
	    # select something compatible with tagger choice, if available
	    my @tagger = grep { $_->first_child()->att('source') eq 'tagger' } @transitions;
	    if (@tagger) {
		my $tag = $tagger[0]->first_child()->att('tag');

		# search exact match first
		@results =
		    grep {
			$_->first_child()->att('tag') eq $tag
		    } @lexed;

		# search compatible match second
		unless (@results) {
		    my $struct = Lingua::Features::Structure->from_string($tag);
		    @results =
		    grep {
			$struct->is_compatible(
			    Lingua::Features::Structure->from_string(
				$_->first_child()->att('tag')
			    )
			)
		    } @lexed;
		}
	    
		# if none found, use tagger choice
		unless (@results) {
		    @results = @tagger;
		}
	    } else {
		@results = @lexed;
	    }
	}

	# try to merge multiple results on lemma
	if (@results > 1) {
	    for my $i (0 .. $#results - 1) {
		my $wordform1 = $results[$i]->first_child();
		my $wordform2 = $results[$i + 1]->first_child();
		if ($wordform1->att('entry') eq $wordform2->att('entry')) {
		    my $tag1 = $wordform1->att('tag');
		    my $tag2 = $wordform2->att('tag');
		    my $tag;
		    if ($tag1 eq $tag2) {
			$tag = $tag1;
		    } else {
			my $structure1 = Lingua::Features::Structure->from_string(
			    $tag1
			);
			my $structure2 = Lingua::Features::Structure->from_string(
			    $tag2
			);
			my $structure;
			if ($structure1->has_same_type($structure2)) {
			    $structure = Lingua::Features::Structure->intersection(
				$structure1, $structure2
			    );
			} else {
			    $structure = Lingua::Features::Structure->union(
				$structure1, $structure2
			    );
			}
			$tag = $structure->to_string();
			print STDERR "merging $tag1 and $tag2 to $tag\n" if $verb;
		    }
		    $results[$i] = undef;
		    $wordform2->set_att('tag', $tag);
		}
	    }
	    @results = grep { $_ } @results;
	}

	$transition = $results[0];
	print "warning: $current" unless $transition;
	my $wordform = $transition->first_child();
	$wordform->cut();
	push(@wordforms, $wordform);

	$current = $transition->att('target');
    }

    # replace
    my $root = $twig->root();
    $fsm->cut();
    $_->paste('last_child', $root) foreach @tokens;
    $_->paste('last_child', $root) foreach @wordforms;
    my $division = XML::Twig::Elt->new('division');
    $division->paste('last_child', $root);

    # purge
    $twig->flush($division);
}
