#!/usr/bin/env perl

use Event::XML::Sync;
use Getopt::Mixed "nextOption";
use Lingua::TagSet::TreeTagger;
use strict;
use warnings;

my $bin = "flemm.pl";
my ($verb) = 0;

Getopt::Mixed::init("b=s v bin>b");
while (my ($option, $value) = nextOption()) {
    $bin    = $value, next if ($option eq 'b');
    $verb   = 1 if ($option eq 'v');
}
Getopt::Mixed::cleanup();

my $cmd = $bin;

Event::XML::Sync->new(
    cmd => $cmd,
    twig => [
        twig_handlers   => { alt => \&alt },
	pretty_print    => 'indented',
	output_encoding => 'ISO-8859-1',
	output_filter   => 'latin1'
    ],
    parser => [
	ProtocolEncoding => 'ISO-8859-1'
    ],
    debug => $verb
);

sub alt {
    my ($twig, $alt) = @_;
    # create new wordform
    my $tagger_wordform = $alt->first_child('[@source="tagger"]');
    my $entry    = $tagger_wordform->att('entry');
    my $tokens   = $tagger_wordform->att('tokens');
    my $tag      = $tagger_wordform->att('tag');
    # convert tag
    $tag = Lingua::TagSet::TreeTagger->string2tag($tag);
    my $wordForm = XML::Twig::Elt->new(
	'wordForm',
	{ entry => $entry, tokens => $tokens, source => 'lemmatizer' },
	'#EMPTY'
    );
    $wordForm->paste('last_child', $alt);
    # proceed further
    $twig->{sync}->process($wordForm, "$entry\t$tag\t<unknown>\n");
}

sub XML::Twig::Elt::sync {
    my ($wordform, $sync, $input) = @_;
    chomp($input);
    my (undef, $tag, $lemma) = split(/\t+/, $input);
    $tag = Lingua::TagSet::TreeTagger->tag2string($tag);
    $wordform->set_att('tag', $tag);
    $wordform->set_att('lemma', $lemma);
    $sync->flush($wordform);
}

exit 0;
