#!/usr/bin/perl
# -*- perl -*-
eval 'exec /usr/bin/perl -S $0 ${1+"$@"}'
    if 0;

#package Multext2DyALog;

use Memoize;
use DB_File;
tie my %cache => 'DB_File', "multext2fstag.cache", O_RDWE|O_CREAT, 0666;

memoize(fs_tag), SCALAR_CACHE => [HASH => \%cache];

my %myfeat=(
	    cat => [qw{adj adv advneg x cl cla cld cln clng det n p ponct pp prep s v vm cc cs interj}],
	    aux => [qw{avoir etre no}],
	    case => [qw{acc dat gen nom}],
	    mode => [qw{ind subj imp cond inf ppart}],
	    tense => [qw{pres imp fut past}],
	    num => [qw{pl sing}],
	    gen => [qw{masc fem}],
	    pers => [qw{1 2 3}],
	    restr => [qw{moinshum plushum}],
	    wh => [qw{+ - rel}],
	    bool => [qw{+ -}],
	    adj_type => [qw/qual ord indef poss/],
	    degree => [qw{pos comp}],
	    pron_type => [qw{pers dem indef poss int rel refl}],
	    det_type => [qw/art dem indef poss int/],
	    n_type => [qw{common proper dist}]	 
	   );

my %myfeattype=(
		case => case,
		cat => cat,
		mode => mode,
		tense => tense,
		num => num,
		gen => gender,
		pers => pers,
		restr => restr,
		wh => wh,
		inv => bool,
		poss => bool,
		numposs => num,
		degree => degree,
		det_type => det_type,
		adj_type => adj_type,
		n_type => n_type,
		def => bool
	       );

my %map = ( gen => { 'm' => 'masc',
		     'f' => 'fem'
		   },
	    num => { 's' => 'sing',
		     'p' => 'pl'
		   },
	    sem => { 'c' => 'country',
		     'h' => 'inhabitant',
		     's' => 'compagny'
		   },
	    pers => { 1 => 1, 2 => 2, 3=>3 },
	    tense => { 'p' => 'pres',
		       'i' => 'imp',
		       'f' => 'fut',
		       's' => 'past'
		     },
	    mode => { 'i' => 'ind',
		      's' => 'subjs',
		      'm' => 'imp',
		      'c' => 'cond',
		      'n' => 'inf',
		      'p' => 'part'
		    }
	  );


my $converter = {
		 'N' => { 'cat' => 'n',
			  'c' => { 'feat' => {'n_type' => ['common']} },
			  'p' => { 'feat' => {'n_type' => ['proper']} },
			  'd' => { 'feat' => {'n_type' => ['dist']} },
			  'decode' => { f=> [qw/gen num sem/],
					r=> [2,3,5] }},
		 'V' => { cat => 'v',
			  decode => { f=> [qw/mode tense pers num gen/],
				      r => [2 .. 6] }},
		 'A' => { cat => 'adj',
			  decode => { f=> [qw/degree gen num/],
				      r=>[2 .. 4] }},
		 'P' => { cat => 'n',
			  decode => { f => [qw/pers gen num cas numpos/],
				      r => [2 .. 6],
				      v => [qw/pers gen num case num/] }},
		 'D' => { cat => 'det',
			  decode => { f=>[qw/pers gen num numpos def/],
				      r=>[2 .. 7],
				      v=>[qw/pers gen num num bool/] } },
		 'R' => { cat => 'adv',
			  skip => { 'n' => { 'cat' => 'advneg' },
				    'd' => { 'cat' => 'advneg' }}},
		 'S' => { 'p' => {cat => 'prep' }},
		 'C' => { 'c' => {cat => 'cc'},
			  's' => {cat => 'cs'} },
		 'M' => { 'c' => {cat => ['det','adj']} },
		 'I' => { cat => 'interj' },
		 'X' => { 'u' => {cat => 'n'},
			  'e' => {cat => 'n'},
			  'f' => {cat => 'n'}},
		 'F' => { 'a' => {cat => 'ponct'},
			  'z' => {cat => 'ponct'},
			  'b' => {cat => 'ponct'},
			  'i' => {cat => 'ponct'},
			  'e' => {cat => 'ponct'},
			}
		};

## Normalize converters
sub normalize_converter {
  my $converter = shift;
  foreach my $key (keys %$converter) {
##    print "Normalizing $key\n";
    if ($key eq "cat") {
      $converter->{cat} = [$converter->{cat}] 
	unless (ref($converter->{cat}) eq 'ARRAY');
      next;
    }
    next if ($key eq "feat");
    if ($key eq "skip") {
      normalize_converter($converter->{'skip'});
      next;
    }
    if ($key eq "decode") {
      $converter->{decode}{v} = $converter->{decode}{f} 
	unless (exists($converter->{decode}{v}));
      next;
    }
    normalize_converter($converter->{$key});
  }
}

sub converter_init {
  normalize_converter($converter);
}

sub use_converter {
  my ($converter,$index,$caracteres,$feat) = @_;
#  print "Test $char index=$index\n";
  if (exists($converter->{'cat'})){
#    print "\tSetting cat=$converter->{'cat'}\n";
    $feat->{'cat'} = $converter->{'cat'};
  } 
  if (defined($converter->{decode})) {
    my $decoder = $converter->{decode};
#    print "Decoder f=@{$decoder->{f}} v=@{$decoder->{v}} r=@{$decoder->{r}}\n";
    for(my $i=0; $i < @{$decoder->{f}}; ++$i){
      $feat->{$decoder->{f}[$i]} =
	generic_decoder($map{$decoder->{v}[$i]},$caracteres->[$decoder->{r}[$i]]);
    }
  }
  if (exists($converter->{feat})){
    foreach my $key (keys %{$converter->{feat}}) {
      $feat->{$key} = $converter->{feat}{$key};
    }
  }
  if (exists($converter->{skip})) {
    use_converter($converter->{skip},$index+1,$caracteres,$feat);
    return;
  }
  my $char = $caracteres->[$index];
  if (exists($converter->{$char})) {
    use_converter($converter->{$char},$index+1,$caracteres,$feat);
  }
}

sub generic_decoder {
  my $map = shift;
  my $char = shift;
  if ($char eq '-') {
    return [];
  } else {
    my @multext = split(/\|/,$char);
    return [ map($map->{$_},@multext) ];
  }
}

sub gds {
  my $caracteres = shift;
  my $indices = shift;
  my $fields = shift;
  my @values = ();
  while (@$indices) {
    my $index = shift(@$indices);
    my $field = shift(@$fields);
    push(@values,generic_decoder($map{$field},$caracteres->[$index]));
  }
  return @values;
}

sub decode {
  my $code = $_[0];
#    print STDERR "*** decode '$code' @caracteres\n";
  my $feat={};
  if ($code eq '-Unknown-'){
    $feat->{"cat"}=["uw"];
  } else {
    my @caracteres = split(/\s+/,$code);
    use_converter($converter,0,\@caracteres,$feat);
  }
  my $Output="";
  my $precAttr=0;
  my @cats=();
  while ((my $attr, my $vals)=each(%$feat)){
#    print "*** attr=$attr val=@{$vals}\n";
    while ((my $myattr, my $myvals)=each(%myfeat)){
      if ($myattr eq $attr){
	if ($attr eq 'cat'){
	  @cats=@{$vals};
		} else {
		  my @attributs=();
		  foreach $val (@{$vals}){
		    foreach $myval (@{$myvals}){
		      if ($val eq $myval){
			push (@attributs, $val);
			    }
		    }
		  }
		  if ($#attributs >= 0){
		    if ($precAttr==1){
		      $Output.= ",";
		    }
		    $Output.="$attr=>";
		    if ($#attributs > 0){
			    $Output.= "$myfeattype{$attr}\[".join(", ", @attributs)."\]";
			  } else {
			    $Output.= $attributs[0];
			  }
		    $precAttr=1;
		  }
		}
      }
    }
    }
  my @result=();
  foreach $cat (@cats){
    while ((my $myattr, my $myvals)=each(%myfeat)){
      if ($myattr eq "cat"){
	foreach $myval (@{$myvals}){
	  if($myval eq $cat){
	    push(@result, "$cat\t$Output");
		  }
	}
      }
    }
  }
  if ($#result < 0){
	# mot inconnu
    @result=("_");
    # @result=("$cat\t$cat");
  }
  @result;
}    

sub fs_tag {
  my $code = shift;
##  print  "*** decode '$code\n";
  my $feat={};
  if ($code eq '-Unknown-'){
    $feat->{"cat"}=["uw"];
  } else {
    my @caracteres = split(/\s+/,$code);
    use_converter($converter,0,\@caracteres,$feat);
  }
  my @tags=();
  my @cats=();
  while ((my $attr, my $vals)=each(%$feat)){
##    print "*** attr=$attr val='@{$vals}'\n";
    push(@tags,$attr."@".join('|',@$vals)) if (@$vals);
  }
  return join(" ",sort @tags);
}    
