#!/usr/bin/perl -T

use CGI qw/:standard *pre *table :html3 -private_tempfiles -oldstyle_urls/;
use CGI::Carp qw(fatalsToBrowser);
#use strict;

$CGI::POST_MAX=1024 * 500;  # max 500K posts

use POSIX qw(strftime);
use IPC::Open2;
use IO::Socket;

$ENV{'PATH'} = '/bin:/usr/local/bin';
$ENV{'DOTFONTPATH'} = '/usr/lib/openoffice/share/fonts/truetype';

my $query = new CGI;
my $pattern = qr/(\w+)\s*(?:{(.*?)})?\((\d+),(\d+)\)/o;

my %accents = ( "e'" => ["e'",'�'],

             'a`' => ['a`','�'],
             'e`' => ['e`', '�'],
             'u`' => ['u`','�'],

             'a^' => ['a\^','�'],
             'e^' => ['e\^','�'],
             'i^' => ['i\^','�'],
             'o^' => ['o\^','�'],
             'u^' => ['u\^','�'],

             'e"' => ['e"','�'],
             'i"' => ['i"','�'],
             'u"' => ['u"','�'],
             'y"' => ['y"','�'],

             'c,' => ['c,(?=[aou])','�']

             );

my $accentpat = join('|',map( $accents{$_}->[0], keys(%accents)));
my $accentpat = qr/($accentpat)/o;

my %graphics = ( 'gif' => { type => 'image/gif',
                            dot => '-Gbgcolor=linen -Gcolor=palegreen -Gstyle=filled',
			    #                           dot => '-Gbgcolor=grey',
                            attachement => 'lingpipe.gif' },
                 'png' => { type => 'image/png',
                            dot => '-Gbgcolor=linen',
                            attachement => 'lingpipe.png' },
                 'ps' => {  type => 'application/postscript',
                            dot => '-Gsize="8,8" -Gmargin=".5,4"',
                            attachement => 'lingpipe.ps'
			 },
                 'dot' => { type=> 'text/plain',
                            attachement => 'lingpipe.dot'
			  },
                 'xml' => { type=> 'text/xml',
                            attachement => 'lingpipe.xml'
			  },
                 'svgz' => { type=> 'image/svg+xml',
			     attachement => 'lingpipe.svgz'
			   }
	       );

######################################################################
# Configuration 

delete @ENV{'IFS', 'CDPATH', 'ENV', 'BASH_ENV'};

my $server = { 'host' => 'localhost',
	       'port' => '9002',
	   };

my %components = ();
my %labels = ();

if (!%components) {
    my $handle = send_server($server,<<MSG);
help
quit
MSG

    while (<$handle> !~ /^Pipe components:/) {
	next;
    }
    while (<$handle> =~ /^\s*(\w+)\s+--\s*(.*)/) {
	$components{$1} = { 'server' => $server, 'label' => "$2" };
	$labels{$1} = "$2";
    }
    close($handle);
}

my %options = ();

######################################################################
# Reading params

my $path_info = $query->path_info;

my $file = $query->upload('filename');

if (!$file && $query->cgi_error) {
    print $query->header(-status=>$query->cgi_error);
    exit 0;
}

if ($file) {
    my $type = $query->uploadInfo($file)->{'Content-Type'};
    unless ($type eq 'text/plain') {
	die "PLAIN FILES ONLY!";
    }
}

my ($component) = $query->param(-name=>'component');

my @history=();

my ($sentence) = &clean_sentence($query->param(-name=>'sentence'));

my ($format) = $query->param(-name=>'format');

my $turned_on = $query->param('save_history');

my $opt_fsm = $query->param('opt_fsm');
my $opt_fs = $query->param('opt_fs');
my $opt_span = $query->param('opt_span');

if (defined($file) && "$file") {
    @history=();
    while (<$file>){
	next if /^\d*\s*$/;
	$_ =  &clean_sentence($_);
	push(@history,$_) unless (/^$/);
    }
} elsif ("$turned_on" eq 'on') {
    @history = $query->cookie('lingpipe');
} else {
    @history = $query->param('hidden');
}

if ($sentence && ! grep("$_" eq $sentence,@history)) {
    @history=() if ($history[0] eq '<none>');
    @history=($sentence,@history);
#    $query->param(-name=>'history',-values=>[$sentence]);
}

if (!@history) {
    @history=('<none>');
}

$query->param(-name=>'history',-values=>[@history]);
$query->param('hidden',@history);

#my @options = $query->param('options');

######################################################################
# Emitting form

if (0 && $format eq 'xml') {
  my $type = 'xml';
  print $query->header(-type=> $graphics{$type}{type},
		       -attachement=> $graphics{$type}{attachement},
		       -expires=>'+1h');
  goto RESULT;
}

my $cookie;

if (defined $turned_on && ("$turned_on" eq 'on')) {
    $cookie = $query->cookie(-name=>'lingpipe',
			     -value=>\@history,
			     -expires=>'+1h',
			     );
    print $query->header(-cookie=>$cookie);
} else {
    print $query->header;
}

my $JSCRIPT=<<END;
// Put selected history line into sentence part (erasing old sentence)
    function paste_into_sentence(element) {
	var sentence = element.options[element.selectedIndex].text;
	document.form1.sentence.value = sentence;
	document.form1.sentence.focus();
	return true;
    }
END

my $STYLE=<<END;
<!--
    BODY {background-color: linen;}
-->
END

print $query->start_html( -title => 'Lingpipe on line',
			  -script=> $JSCRIPT,
			  -style => { -code => $STYLE }
			  );
print h1('Using Lingpipe on line');
print $query->start_multipart_form(-name=>'form1');
print p('Select a terminal component ',
	br,
	$query->popup_menu(-name=>'component',
			   -values=> [ sort {$labels{$a} cmp $labels{$b}} keys %labels ],
			   -labels => \%labels,
			   -default=>'normalize'
			   ),
	);

print p('Select options (for normalize) ',
	$query->checkbox(-name=>'opt_fsm',
			 -value=>'off',
			 -label=>'FSM expand'
			 ),
	$query->checkbox(-name=>'opt_fs',
			 -value=>'off',
			 -label=>'FS expand'
			 ),
	$query->checkbox(-name=>'opt_span',
			 -value=>'off',
			 -label=>'Span'
			)
	);

print p('Enter a sentence or select one in the history popup menu',
	br,
	$query->textarea(-name=>'sentence', 
			 -rows=>3, -columns=>70)
       );

print p('History popup menu',
	br,
	$query->popup_menu(-name=>'history',
			   -onChange=>'paste_into_sentence(this)'
			  ),
	$query->hidden(-name=>'hidden',-default=>[@history]),
	$query->checkbox(-name=>'save_history',
			 -value=>'on',
			 -label=>'Save/Restore history (using a cookie)')
	);

print p('Load your own file as history popup menu (a sentence per line)',
	br,
	$query->filefield(-name=>'filename',
			  -size=>50
			  ));

print  $query->hidden(-name=>'format',-value=>'xml');

print p($query->reset,$query->submit('Action','Submit'));

print $query->endform;
print "<HR>\n";

######################################################################
# Emitting result

 RESULT:

goto NOTHING unless ($component && $sentence);

#print "path_info=$path_info";

$sentence =~ s/$accentpat/$accents{$1}->[1]/og;

open(LOG,">> /tmp/cgi_lingpipe.log") || 
    die "Can't open log file";

print LOG strftime("date= %b %e %Y %H:%M:%S\n", localtime);
print LOG "host=",remote_host(),"\n";
print LOG "component=",$component,"\n";
print LOG "sentence=",$sentence,"\n";
print LOG "=\n";

close(LOG);

my @norm_options = ();
push(@norm_options,'--fs') if $opt_fs;
push(@norm_options,'--span') if $opt_span;
push(@norm_options,'--fsm') if $opt_fsm;
my $norm_options = join(' ',@norm_options);

my $handle = send_server($components{$component}{'server'},<<MSG);
config --normalize_options=$norm_options
terminal $component
sentence $sentence
quit
MSG

use XML::LibXSLT;
use XML::LibXML;

my $parser = XML::LibXML->new();
my $xslt = XML::LibXSLT->new();

my $source = $parser->parse_fh($handle);
my $style_doc = $parser->parse_file('lingpipe.xsl');

my $stylesheet = $xslt->parse_stylesheet($style_doc);

my $results = $stylesheet->transform($source);

print $stylesheet->output_string($results);


#print $query->pre("FOREST FORMAT $format $formats{$format}");
#print "Send $sentence to $component\n";

#while (my $line = <$handle>) {
#  print $line;
#  if ($line =~ /^<\?xml/) {
#    print <<HEADER;
#<?xml-stylesheet type="text/xsl" href="lingpipe.xsl"?>
#HEADER
#  }
#}


close $handle || die "cannot close: $!";

##exit;

NOTHING:
    
print hr,
    address(a({href=>'mailto:Eric.Clergerie@inria.fr'},'Eric de la Clergerie')),
    a({href=>'http://pauillac.inria.fr/~clerger/'},'Home Page');

print $query->end_html;

######################################################################
# sub routines

sub clean_sentence {
    my ($sentence)=@_;
    $sentence =~ s/\s+/ /og;
    $sentence =~ s/^\s+//og;
    $sentence =~ s/\s+$//og;
    return $sentence;
}

sub send_server {
    my $server = shift;
    my $message = shift;

    my $host = $server->{'host'};
    my $port = $server->{'port'};

    my ($kidpid, $handle, $line);

    # create a tcp connection to the specified host and port
    $handle = IO::Socket::INET->new(Proto     => "tcp",
				    PeerAddr  => $host,
				    PeerPort  => $port
				)
	or die "can't connect to the lingpipe server [$host:$port]";

    $handle->autoflush(1);

    # split the program into two processes, identical twins
    die "can't fork: $!" unless defined($kidpid = fork());

    # Child part
    if (!$kidpid) { # Child
	print $handle $message;
	exit 0;
    }
    
    return $handle;
}

__END__

=head1 CGI Perl Script to use Lingpipe on line

This script has been designed to test Lingpipe on line on a set of
various grammars.

