#!/usr/bin/env perl

use utf8;

#binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
#binmode STDERR, ":utf8";

use CGI qw /-compile -nosticky  :standard *pre *table :html3 -private_tempfiles/;
use CGI::Carp qw(fatalsToBrowser);
use Encode;
use strict;
use HTML::Entities;

# $CGI::POST_MAX=1024 * 500;  # max 500K posts

use POSIX qw(strftime);
use IPC::Open2;
use IPC::Run qw(start finish run pump);
use IO::Socket;


$SIG{CHLD} = 'IGNORE';


#$ENV{'PATH'} = '/bin:/usr/local/bin';
#$ENV{'DOTFONTPATH'} = '/usr/lib/openoffice/share/fonts/truetype';

local our $query = new CGI;
$query->autoEscape(0);
$query->charset("");

our $pid = $$;

our $pattern = qr/(\w+)\s*(?:{(.*?)})?\((\d+),(\d+)\)/o;

our $dotcmd = '/usr/bin/dot';
if (-r '/usr/local/bin/dot') {$dotcmd = '/usr/local/bin/dot'}

our %graphics = ( 'gif' => { type => 'image/gif',
			     dot => [qw{-Gbgcolor=linen -Gcolor=palegreen -Gstyle=filled}],
			     attachment => 'sxpipe.gif' },
		  'png' => { type => 'image/png',
                            dot => [qw{-Gbgcolor=linen}],
                            attachment => 'sxpipe.png' },
		  'ps' => {  type => 'application/postscript',
                            dot => [qw{-Gsize="8,8" -Gmargin=".5,4"}],
                            attachment => 'sxpipe.ps'
			 },
		  'dot' => { type=> 'text/plain',
                            attachment => 'sxpipe.dot',
			    dot => []
			  },
                 'svg' => { type=> 'image/svg',
			     attachment => 'sxpipe.svg',
			     dot => []
			   }
	       );

######################################################################
# Configuration 

delete @ENV{'IFS', 'CDPATH', 'ENV', 'BASH_ENV'};

our $server = { 'host' => 'listrac',
	       'port' => '7974',
	   };


our %options = ();

######################################################################
# Reading params

our $path_info = $query->path_info;

our $file = $query->upload('filename');

if (!$file && $query->cgi_error) {
    print $query->header(-status=>$query->cgi_error,
			 -charset=>'utf-8'
			);
    exit 0;
}

if ($file) {
    my $type = $query->uploadInfo($file)->{'Content-Type'};
    unless ($type eq 'text/plain') {
	die "PLAIN FILES ONLY!";
    }
}

our ($view) = $query->param(-name=>'view');

our @history=();

our ($sentence) = &clean_sentence($query->param(-name=>'sentence'));

our ($lang) = $query->param(-name=>'lang') || "French";
$lang = &lname2lcode($lang);


our $turned_on = $query->param('save_history');

if (defined($file) && "$file") {
    @history=();
    while (<$file>){
	next if /^\d*\s*$/;
	$_ =  &clean_sentence($_);
	push(@history,$_) unless (/^$/);
    }
} elsif ("$turned_on" eq 'on') {
    @history = $query->cookie('sxpipe');
} else {
    @history = $query->param('hidden');
}

if (defined $sentence && $sentence && ! grep("$_" eq $sentence,@history)) {
    @history=() if ($history[0] eq '<none>');
    @history=($sentence,@history);
#    $query->param(-name=>'history',-values=>[$sentence]);
}

if (!@history) {
    @history=('<none>');
}

$query->param(-name=>'history',-values=>[@history]);
$query->param('hidden',@history);

#my @options = $query->param('options');

######################################################################
# Emitting form

## $view = 'xml' if (defined $format && $graphics{$format}{type} eq 'text/xml');

our $cookie;

if (defined $turned_on && ("$turned_on" eq 'on')) {
   $cookie = $query->cookie(-name=>'sxpipe',
			     -value=>\@history,
			    -charset=>'utf-8',
			     -expires=>'+1h'			     );
   print $query->header(-cookie=>$cookie,
			 -charset=>'utf-8'
		       );
} else {
    print $query->header(			
			 -charset=>'utf-8'
			);
}

my $JSCRIPT=<<END;
// Put selected history line into sentence part (erasing old sentence)
    function paste_into_sentence(element) {
	var sentence = element.options[element.selectedIndex].text;
	document.form1.sentence.value = sentence;
	document.form1.sentence.focus();
	return true;
    }
END

my $STYLE=<<END;
<!--
    BODY {background-color: linen;}
-->
END

print $query->start_html( -encoding => 'utf-8',
			  -title => 'SxPipe online',
			  -script=> $JSCRIPT,
			  -style => { -code => $STYLE }
			  );
print h1('SxPipe demo page');

print $query->start_multipart_form(-name=>'form1');

print p('SxPipe is a pre-parsing processing chain that handles segmentation and tokenization, spelling error correction, and named entities recognition. It is designed to transform in a robust way raw corpus to DAG of lexical entries.<br>The SxPipe version for French, which is briefly described in a paper available <a href="http://atoll.inria.fr/biblio?key=sagot05:LT">here</a>, has already reached a satisfying level of quality, and is used by several different parsing and information extraction architectures. SxPipe versions for Polish, Slovak and Slovene are currently being developed, and very preliminary versions can already be tested on this page.');

print p('<i><b>Attention: this is experimental software, currently under development (this demo uses always the latest version, and NOT the latest stable version). This is especially true for the Polish, Slovak and Slovene versions.<br>Results may change from one day to the other, and do contain bugs. <br>If you detect reccurrent errors, please inform the maintainer by email. Thanks a lot.</b></i>');

print p('Enter a sentence in',
	$query->popup_menu(-name=>'lang',-values=>["French","Polish","Slovak","Slovene"],-default=>"French"),
	'or select one in the history popup menu',
	br,
	$query->textarea(-name=>'sentence', 
			 -rows=>3, -columns=>70)
       );

#$query->charset("");
print p('History popup menu',
	br,
	$query->popup_menu(-name=>'history',
			   -onChange=>'paste_into_sentence(this)'
			  ),
	$query->hidden(-name=>'hidden',-default=>[@history]),
	$query->checkbox(-name=>'save_history',
			 -value=>'on',
			 -label=>'Save/Restore history (using a cookie)')
	);
#$query->charset("utf-8");

print p('Load your own file as history popup menu (one sentence per line)',
	br,
	$query->filefield(-name=>'filename',
			  -size=>50
			  ));

#$query->charset("");
print  $query->hidden(-name=>'view',-value=>'gif');
#$query->charset("utf-8");

print p($query->reset,$query->submit('Action','Submit'));

print $query->endform;
print "<HR>\n";

######################################################################
# Emitting result

binmode STDOUT, ":crlf";

if ($sentence ne "") {
  print p("Input sentence: ",$sentence);

  print p("View: $view");

  my $handle = send_server($server,"format dag\nl $lang\nsentence $sentence\nquit\n");
  my $answer="".<$handle>;
  while ($answer =~ /^\#\# /) {
    $answer="".<$handle>;
  }
  open(LOG,">> /tmp/cgi_sxpipe.log") || 
    die "Can't open log file";
  print LOG strftime("date= %b %e %Y %H:%M:%S\n", localtime);
  print LOG "host=",remote_host(),"\n";
  print LOG "sentence=",$sentence,"\n";
  print LOG "lang=",$lang,"\n";
  print LOG "answer=",$answer,"\n";
  close(LOG);
  close $handle || die "cannot close: $!";

  if (defined $graphics{$view}) { # dot format
    $handle = send_server($server,"format dot\nl $lang\nlastsentence\nquit\n");
    open(GIF,"| dot -Tgif  -Gbgcolor=linen -Gcolor=palegreen -Gstyle=filled > /home/pomerol/atoll/exportbuild/var/www/perl/sxpipe/gif/$pid.$view") ||
      die "Can't save $view file";
    while (<$handle>) {
      print GIF $_;
    }
    close GIF;
    #print `$dotcmd -Tgif -Gbgcolor=linen -Gcolor=palegreen -Gstyle=filled`;
  }

  print $query->div({align=>"CENTER"},
		    $query->img({src=>"gif/$pid.$view",
				 align=>"CENTER",
				 alt => "please try reload",
				 'usemap' => '#map'
				}));

  print $answer;
}

print hr;
print "Maintainer: ";
print a({href=>'http://atoll.inria.fr/~sagot/'},'Benoit Sagot');
print $query->end_html;

######################################################################
# sub routines

sub clean_sentence {
    my ($sentence)=@_;
    $sentence ||= "";
    $sentence =~ s/\s+/ /og;
    $sentence =~ s/^\s+//og;
    $sentence =~ s/\s+$//og;
    return $sentence;
}

sub send_server {
    my $server = shift;
    my $message = shift;

    my $host = $server->{'host'};
    my $port = $server->{'port'};

    my ($kidpid, $handle, $line);

    # create a tcp connection to the specified host and port
    $handle = IO::Socket::INET->new(Proto     => "tcp",
				    PeerAddr  => $host,
				    PeerPort  => $port,
				)
	or die "can't connect to the sxpipe server [$host:$port]";

#    binmode $handle => ":encoding(utf-8)";
#    binmode $handle => ":encoding(crlf)";
    $handle->autoflush(1);

    # split the program into two processes, identical twins
    die "can't fork: $!" unless defined($kidpid = fork());

    # Child part
    if (!$kidpid) { # Child
      # print $handle $message;
      #      $r->cleanup_for_exec(); # untie the socket
      close(STDIN);
      close(STDOUT);
      close(STDERR);
      print $handle $message;
      CORE::exit 0;
    }
    
    return $handle;
}

sub lcode2lname {
    my $l = shift;
    if ($l eq "fr") {
	return "French";
    } elsif ($l eq "pl") {
	return "Polish";
    } elsif ($l eq "sk") {
	return "Slovak";
    } elsif ($l eq "si") {
	return "Slovene";
    } else {
	die "unknown language code: $l";
    }
}

sub lname2lcode {
    my $l = shift;
    if ($l eq "French") {
	return "fr";
    } elsif ($l eq "Polish") {
	return "pl";
    } elsif ($l eq "Slovak") {
	return "sk";
    } elsif ($l eq "Slovene") {
	return "si";
    } else {
	die "unknown language name: $l";
    }
}

sub add_view_in_url {
    my $url=shift;
    my $view=shift;
    $url =~ s%view=\w+%view=$view%;
    return $url;
}
