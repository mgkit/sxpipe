#!/usr/bin/env perl

# Grammaire des nombres
# Le "un" eventuel commencant un nombre est exclus de ce nombre


# Grammaire non LALR (1) :

# Conflicts | Shift-Reduce  | Reduce-Reduce
# -----------------------------------------
#           | User | System | User | System
#           -------------------------------
#    336    | 0    |  336   | 0    |  0 

# priorites shift sur les Shift-Reduce

#              -2    -1  0   +1
# 	                "un mille"                         => "un mille"
# 		   "cent un mille"                         => "_NUMBER"
# 		   "cent un milles"                        => "_NUMBER milles"
# 			"un mille d' ici"                  => "un mille d' ici"
# 			"un mille de"                      => "un mille de"
# 			"un mille cinq cents milles marin" => "un _NUMBER milles marin"
# 	    "quarante et un mille cinq cents milles marin" => "_NUMBER milles marin"
#		        "un pays neuf"			   => "un pays neuf"
#	      "ce char a neuf roues"			   => "ce char a neuf roues"
#		        "un neuf"			   => "un neuf"
#                       "une ;   un"			   => "une ; un"
#                       "une un"			   => "une un"
#   "ils  ont  tous  les sept une barbe"		   => "ils ont tous les _NUMBER une barbe"
#             "les mille une nuits"		           => "les _NUMBER nuits"
#          "les mille et une nuits"		           => "les _NUMBER et une nuits"



# &1 : TRUE <=> ssi le terminal suivant est : un[e], onze, mille, milli[on|ard]s,
#      milli[on|ard]i�me[s], uni�me[s], onzi�me[s]

# &2 : TRUE <=> les terminaux en position -1, -2, ... verifient les contraintes gauches de &2
#
#	      contraintes gauches de &2 :
#	        - le terminal en position -1 est : eof, WORD, de, un, une ou _NUM
#               - le terminal en position -1 est : "deux", "trois", "quatre", "cinq", "six", "sept", "huit", "neuf", "dix", "onze", "douze", "treize", "quatorze", "quinze", "seize", "dix", "dix", "dix", "vingt", "vingts", "trente", "quarante", "cinquante", "soixante", "soixante", "cents"
#	        - le terminal en position -1 est : et, et le terminal
#	      en position -2 n'est pas vingt[s] trente quarante cinquante soixante quatre-vingt[s]

# &3 : TRUE <=> le terminal en position +1 verifie les contraintes droites de &3
# 	          le terminal en position -1 est de la forme WORD... ou t&2 et verifie les contraintes gauches de &2
# 
# 	      contraintes droites de &3 :
# 	        - le terminal en position +1 est : eof, WORD, et, de

# &4 : TRUE

$| = 1;

$lang="fr";

while (<>) {
  chomp;
# 1 : On sort le texte du terminal
# <word>			= et ; 1
# <word>			= de ; 1
# <word>			= %WORD ; 1
# <word>			= un &2 ; 1
# <word>			= une &2 ; 1
# <word>			= neuf &3 ; 1
# <word>			= cent &3 ; 1
# <word>			= cents &3 ; 1
# <word>			= mille &3 ; 1
# <word>			= _NUM &2 ; 1

# 5 : On sort _NUM
#<number>		= %INTEGER ; 5
  s/{([^\}]+)} [0-9]+/{$1} _NUMBER/goi;


# <2..9>			= deux ;
# <2..9>			= trois ;
# <2..9>			= quatre ;
# <2..9>			= cinq ;
# <2..9>			= six ;
# <2..9>			= sept ;
# <2..9>			= huit ;
# <2..9>			= neuf ;
# <2..9>			= _NUM ;
  $NT2_9 = qr/(?:deux|trois|quatre|cinq|six|sept|huit|neuf|_NUM)/oi;

# 4 : concatenation des commentaires sur le token le + a gauche
# <12..19>		= douze ;
# <12..19>		= treize ;
# <12..19>		= quatorze ;
# <12..19>		= quinze ;
# <12..19>		= seize ;
# <12..19>		= dix sept ; 4
# <12..19>		= dix huit ; 4
# <12..19>		= dix neuf ; 4
# <12..19>		= dix-sept ;
# <12..19>		= dix-huit ;
# <12..19>		= dix-neuf ;
  s/{([^\}]+)} dix {([^\}]+)} (sept|huit|neuf)( |$)/{$1 $2} dix-$3$4/goi
    || s/dix (sept|huit|neuf)( |$)/dix-$1$2/goi;
  $NT12_19 = qr/(?:douze|treize|quatorze|quinze|seize|dix-sept|dix-huit|dix-neuf)/oi;


# Pour les douze cents ...
# <2..19-10>	        = <2..9> ;
# <2..19-10>	        = onze ;
# <2..19-10>	        = <12..19> ;
  $NT2_19_sauf_10 = qr/(?:$NT2_9|$NT12_19|onze)/oi;

# <vingt[s]>		= vingt ;
# <vingt[s]>		= vingts ;
# <20_30_40_50>		= <vingt[s]> ;
# <20_30_40_50>		= trente ;
# <20_30_40_50>		= quarante ;
# <20_30_40_50>		= cinquante ;
  $NT20_30_40_50 = qr/(?:vingts?|trente|quarante|cinquante)/oi ;

# <60_80>			= soixante ;
# <60_80>			= <quatre-vingt[s]> ;
# On accepte qq fautes ...
# <quatre-vingt[s]>	= quatre-vingt ;
# <quatre-vingt[s]>	= quatre-vingts ;
# <quatre-vingt[s]>	= quatre <vingt[s]> ; 4
  s/{([^\}]+)} quatre {([^\}]+)} (vingts?)( |$)/{$1 $2} quatre-$3$4/goi || s/quatre vingt(s | |$)/quatre-vingt$1/goi;
  $NT60_80 = qr/(?:soixante|quatre-vingts?)/oi ;

# <20_30_40_50_60_80>	= <20_30_40_50> ;
# <20_30_40_50_60_80>	= <60_80> ;
  $NT20_30_40_50_60_80	= qr/(?:$NT20_30_40_50|$NT60_80)/oi ;

# <2..99>			= <2..19-10> ;
# <2..99>			= dix ;
# <2..99>			= <20_30_40_50_60_80> ;
# # <2..99>		= <quatre-vingt[s]> ; reconnu par <20_30_40_50_60_80>
# <2..99>			= <60_80> dix ; 4
# <2..99>			= <20_30_40_50> et &1 un ; 4
# <2..99>			= soixante et &1 un ; 4
# <2..99>			= soixante et &1 onze ; 4
# <2..99>			= <quatre-vingt[s]> un ; 4
# <2..99>			= <quatre-vingt[s]> onze ; 4
# <2..99>			= <20_30_40_50_60_80> <2..9> ; 4
# <2..99>			= <60_80> <12..19> ; 4
  s/{([^\}]+)} ($NT60_80) {([^\}]+)} (dix|$NT12_19)( |$)/{$1 $3} $2 $4$5/goi;
  s/{([^\}]+)} (soixante) {([^\}]+)} (et) {([^\}]+)} (un|onze)( |$)/{$1 $3 $5} $2 $4 $6$7/goi;
  s/{([^\}]+)} (quatre-vingts?) {([^\}]+)} (un|onze)( |$)/{$1 $3} $2 $4$5/goi;
  s/{([^\}]+)} ($NT20_30_40_50_60_80) {([^\}]+)} ($NT2_9)( |$)/{$1 $3} $2 $4$5/goi;
  $NT2_99 = qr/(?:(?:soixante et|quatre-vingts?) (?:un|onze)|$NT60_80 (?:dix|$NT12_19)|$NT20_30_40_50 et un|$NT20_30_40_50_60_80(?: $NT2_9)?|$NT2_19_sauf_10|dix)/oi;

# <1..99>			= un ;
# <1..99>			= <2..99> ;
  $NT1_99 = qr/(?:un|$NT2_99)/oi;

# On accepte qq fautes ...
# <cent>			= cent ;
# <cent>			= cents ;

# <million>		= million ;
# <million>		= millions ;

# <milliard>		= milliard ;
# <milliard>		= milliards ;

# Un peu abusif de mettre les douze cents ds les <2..999> !!
# <2..999>		= <2..99> ;
# <2..999>		= <cent> ;
# <2..999>		= <cent> <1..99> ; 4
# <2..999>		= <2..19-10> <cent> ; 4
# <2..999>		= <2..19-10> <cent> <1..99> ; 4
  s/{([^\}]+)} (cents?) {([^\}]+)} ($NT1_99)( |$)/{$1 $3} $2 $4$5/goi;
  s/{([^\}]+)} ($NT2_19_sauf_10) {([^\}]+)} (cents?)( |$)/{$1 $3} $2 $4$5/goi;
  s/{([^\}]+)} ($NT2_19_sauf_10) {([^\}]+)} (cents?) {([^\}]+)} ($NT1_99)( |$)/{$1 $3 $5} $2 $4 $6$7/goi;
  $NT2_999 = qr/(?:(?:$NT2_19_sauf_10 )?cents?(?: $NT1_99)?|$NT2_99)/oi;

# Les <p..q+une> ne contiennent que les nombres entre p et q qui peuvent se terminer par une
# <2..99+une>		= <20_30_40_50> et &1 une ; 4
# <2..99+une>		= soixante et &1 une ; 4
# <2..99+une>		= <quatre-vingt[s]> une ; 4
  s/{([^\}]+)} ($NT20_30_40_50|soixante) {([^\}]+)} (et) {([^\}]+)} (une)( |$)/{$1 $3 $5} $2 $4 $6$7/goi;
  s/{([^\}]+)} (quatre-vingts?) {([^\}]+)} (une)( |$)/{$1 $3} $2 $4$5/goi;
  $NT2_99une = qr/(?:(?:$NT20_30_40_50 et|soixante et|quatre-vingts?) une)/oi;

# <1..99+une>		= une ;
# <1..99+une>		= <2..99+une> ;
  $NT1_99une = qr/(?:une|$NT2_99une)/oi;

# <2..999+une>		= <2..99+une> ;
# <2..999+une>		= <cent> <1..99+une> ; 4
# <2..999+une>		= <2..19-10> <cent> <1..99+une> ; 4
  s/{([^\}]+)} (cents?) {([^\}]+)} ($NT1_99une)( |$)/{$1 $3} $2 $4$5/goi;
  s/{([^\}]+)} ($NT2_19_sauf_10) {([^\}]+)} (cents?) {([^\}]+)} ($NT1_99une)( |$)/{$1 $3 $5} $2 $4 $6$7/goi;
  $NT2_999une = qr/(?:$NT2_99une|(?:$NT2_19_sauf_10 )?cents? $NT1_99)/oi;

# <1..999>		= un ;
# <1..999>		= <2..999> ;
  $NT1_999 = qr/(?:un|$NT2_999)/oi;


# <1..999+une>		= une ;
# <1..999+une>		= <2..999+une> ;
  $NT1_999une = qr/(?:$NT2_999une|une)/oi;

# <2..10^6-1>		= <2..999> ;
# <2..10^6-1>		= mille ;
# <2..10^6-1>		= mille <1..999> ; 4
# <2..10^6-1>		= <2..999> mille ; 4
# <2..10^6-1>		= <2..999> mille <1..999> ; 4
  s/{([^\}]+)} (mille) {([^\}]+)} ($NT1_999)( |$)/{$1 $3} $2 $4$5/goi;
  s/{([^\}]+)} ($NT2_19_sauf_10) {([^\}]+)} (mille)( |$)/{$1 $3} $2 $4$5/goi;
  s/{([^\}]+)} ($NT2_19_sauf_10) {([^\}]+)} (mille) {([^\}]+)} ($NT1_999)( |$)/{$1 $3 $5} $2 $4 $6$7/goi;
  $NT2_10e6 = qr/(?:(?:$NT2_999 )?mille(?: $NT1_999)?|$NT2_999)/oi;

# <1..10^6-1>		= un ;
# <1..10^6-1>		= <2..10^6-1> ;
  $NT1_10e6 = qr/(?:$NT2_10e6|un)/oi;

# <2..10^6-1+une>		= <2..999+une> ;
# <2..10^6-1+une>		= mille <1..999+une> ; 4
# <2..10^6-1+une>		= <2..999> mille <1..999+une> ; 4
  s/{([^\}]+)} (mille) {([^\}]+)} ($NT1_999une)( |$)/{$1 $3} $2 $4$5/goi;
  s/{([^\}]+)} ($NT2_19_sauf_10) {([^\}]+)} (mille) {([^\}]+)} ($NT1_999une)( |$)/{$1 $3 $5} $2 $4 $6$7/goi;
  $NT2_10e6une = qr/(?:(?:$NT2_999 )?mille $NT1_999une|$NT2_999une)/oi;

# <1..10^6-1+une>		= une ;
# <1..10^6-1+une>		= <2..10^6-1+une> ;
  $NT1_10e6une = qr/(?:$NT2_10e6une|une)/oi;

# <2..10^9-1>		= <2..10^6-1> ;
# <2..10^9-1>		= un <million> <1..10^6-1> ; 4
# <2..10^9-1>		= <2..10^6-1> <million> <1..10^6-1> ; 4
# <2..10^9-1+une>		= <2..10^6-1+une> ;
# <2..10^9-1+une>		= un <million> <1..10^6-1+une> ; 4
# <2..10^9-1+une>		= <2..10^6-1> <million> <1..10^6-1+une> ; 4
  s/{([^\}]+)} ($NT1_10e6) {([^\}]+)} (millions?)(?: {([^\}]+)} ($NT1_10e6|$NT1_10e6une))?( |$)/{$1 $3 $5} $2 $4 $6$7/goi;
  $NT2_10e9 = qr/(?:$NT1_10e6 millions?(?: $NT1_10e6| $NT2_10e6)?|$NT1_10e6)/oi;
  $NT2_10e9une = qr/(?:$NT1_10e6 millions? $NT1_10e6une|$NT2_10e6une)/oi;

# <1..10^9-1>		= un ;
# <1..10^9-1>		= <2..10^9-1> ;
# <1..10^9-1>		= _NUM <million> ;
# <1..10^9-1>		= <2..10^6-1> <million> ;
  s/{([^\}]+)} (_NUM(?:BER)?|$NT2_10e6) {([^\}]+)} (millions?)( |$)/{$1 $3} $2 $4$5/goi;
  $NT1_10e9 = qr/(?:(?:$NT2_10e6|_NUM(?:BER)?) millions?|$NT2_10e9|un)/oi;

# <1..10^9-1+une>		= une ;
# <1..10^9-1+une>		= <2..10^9-1+une> ;
  $NT1_10e9une = qr/(?:$NT2_10e6une|une)/oi;

# <2..10^15-1>		= <2..10^9-1> ;
# <2..10^15-1>		= un <milliard> <1..10^9-1> ; 4
# <2..10^15-1>		= _NUM <milliard> <1..10^9-1> ; 4
# <2..10^15-1>		= <2..10^6-1> <milliard> <1..10^9-1>; 4
# <2..10^15-1+une>	= <2..10^9-1+une> ;
# <2..10^15-1+une>	= un <milliard> <1..10^9-1+une> ; 4
# <2..10^15-1+une>	= _NUM <milliard> <1..10^9-1+une> ; 4
# <2..10^15-1+une>	= <2..10^6-1> <milliard> <1..10^9-1+une>; 4
  s/{([^\}]+)} (un|_NUM(?:BER)?|$NT2_10e6) {([^\}]+)} (milliards?)(?: {([^\}]+)} ($NT1_10e9|$NT1_10e9une))?( |$)/{$1 $3 $5} $2 $4 $6$7/goi;
  $NT2_10e15 = qr/(?:(?:un|_NUM(?:BER)?|$NT2_10e6) milliards?(?: $NT1_10e9)?|$NT2_10e9)/oi;
  $NT2_10e15une = qr/(?:(?:un|_NUM(?:BER)?|$NT2_10e6) milliards? $NT1_10e9une|$NT2_10e9une)/oi;


# <10^6+>			= <milli[on|ard]>;
# <10^6+>			= un <milli[on|ard]> ;
# <10^6+>			= _NUM <milli[on|ard]> ;
# <10^6+>			= <2..10^6-1> <milli[on|ard]> ; 4
# <10^6+>			= <10^6+> de &1 <milli[on|ard]> ; 4
  $NT10e6plus = qr/(?:(?:_NUM(?:BER)? |$NT1_10e6 )?milli(?:on|ard)s?(?: de milli(?:on|ard)s?)*)/oi;


# # 2 : On sort _NUMBER (remarque: z�ro/z�ros est un nc)
# <number>		= <2..10^15-1> ; 2
# <number>		= <2..10^15-1+une> ; 2
# <number>		= <10^6+> ; 2
# #ad oc (ouaf ouaf)
# #pris par ieme.bnf
# #<number>		= <10^6+> de &1 mille ; 2

  s/(^| )(?:$NT2_10e15une|$NT2_10e15|$NT10e6plus)( |$)/$1\_NUMBER$2/goi;
  s/(^| )(?:(?:soixante et|quatre-vingts?) (?:un|onze))( |$)/$1\_NUMBER$2/goi;

# corrections a posteriori

  s/({([^\}]+)} un { *<[^>]+>mille<[^>]+> *}) _NUMBER/$1 mille/goi; # dans "un mille", "mille" n'est pas un nombre
  s/({ *<[^>]+>neuf<[^>]+> *}) _NUMBER/$1 neuf/goi; # "un XXX neuf__adj" semble plus fr�quent que "neuf" comme _NUMBER
  s/({ *<[^>]+>(l')?un<[^>]+> *}) _NUMBER/$1 un/goi; # "un__det" semble plus fr�quent que "un" comme
						# _NUMBER (p� faudrait-il un truc malin pour les
						# intervalles, genre "de un � trois machins"...)
  s/({ *<[^>]+>l'<[^>]+> *}\s*{ *<[^>]+>un<[^>]+> *}) _NUMBER/$1 l'un/goi;

  s/  +/ /g;

  print "$_\n";
}
