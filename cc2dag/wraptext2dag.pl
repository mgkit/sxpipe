#!/usr/bin/env perl
# $Id$

$| = 1;

my $command = 'text2dag';
my $options = '';
my $lang = "fr";

while (1) {
  $_=shift;
  if (/^$/) {last;}
  elsif (/^-l$/) {$lang = (shift) || die "Language id must follow -l option"}
  elsif (/^-d$/) {$command = (shift)."/$command" || die "Directory must follow -d option"}
  else {$options .= " $_"}
}

while (<>) {
  $command =~ s/$/$lang/ unless ($lang eq "fr");
  open(TEXT2DAG, "| $command$options") or die "Can't run $command: $!";
  print TEXT2DAG $_ unless ($_ eq "");
  close(TEXT2DAG);
}
