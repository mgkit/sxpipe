#!/usr/bin/env perl
# $Id$

$| = 1;

my $command = 'unfold_dag';
my $options;

while (1) {
  $_=shift;
  if (/^$/) {last;}
  elsif (/^-d$/) {$command = (shift)."/$command" || die "Directory must follow -d option"}
  else {$options .= " $_"}
}

while (<>) {
    open(UNFOLD, "| $command$options") or die "Can't run $command: $!";
    print UNFOLD $_ unless ($_ eq "");
    close(UNFOLD);
}
