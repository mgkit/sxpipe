#ifdef sntfilter_h
#define SNTFILTER_SEM_PASS_ARG chunker_sem_pass_arg
#define SNTS_NAME "chunks"

#define SNT_PREFIX "Easy_"
#define SNT_PREFIX_LENGTH 5

#define    GN  0
#define    GA  1
#define    GR  2
#define    GP  3
#define    NV  4
#define    PV  5
#define    ZZ  6

#define    MAX_SNT_ID  6
#endif /* sntfilter_h */
#ifdef sntfilter_c_init
static char    *sntid2string [7] = {"GN","GA","GR","GP","NV","PV","ZZ"};

static double sntid2weight_n [7] = {/* GN */ 1.4,/* GA */ 1.1,/* GR */ 1.2, /* GP */ 1.3, /* NV */ 1.5, /* PV */ 1.0,/* 00 */ 1.6};
static double sntid2weight_1Maj [7] = {/* GN 1.4 */ 1.3,/* GA */ 1.5,/* GR */ 1.1, /* GP */ 1.2, /* NV 1.3 */ 1.4, /* PV */ 1.0,/* 00 */ 1.6};
static double sntid2weight_1Min [7] = {/* GN */ 1.5,/* GA */ 1.3,/* GR */ 1.1, /* GP */ 1.4, /* NV */ 1.2, /* PV */ 1.0,/* 00 */ 1.6};

static SXINT string2sntid(nts)
     char* nts;
{
  if (strncmp (nts+5, "GN", 2) == 0)
    return 0;
  if (strncmp (nts+5, "GA", 2) == 0)
    return 1;
  if (strncmp (nts+5, "GR", 2) == 0)
    return 2;
  if (strncmp (nts+5, "GP", 2) == 0)
    return 3;
  if (strncmp (nts+5, "NV", 2) == 0)
    return 4;
  if (strncmp (nts+5, "PV", 2) == 0)
    return 5;
  if (strncmp (nts+5, "ZZ", 2) == 0)
    return 6;
  if (strncmp (nts+5, "00", 2) == 0)
    return 6;
  return -1;
}

char* ntstring2chunksname(ntstring)
     char* ntstring;
{
  if (ntstring == NULL || strlen(ntstring) < 7)
    return NULL;
  if (strncmp (ntstring, "Easy_GN", 7) == 0)
    return "GN";
  if (strncmp (ntstring, "Easy_GA", 7) == 0)
    return "GA";
  if (strncmp (ntstring, "Easy_GR", 7) == 0)
    return "GR";
  if (strncmp (ntstring, "Easy_GP", 7) == 0)
    return "GP";
  if (strncmp (ntstring, "Easy_NV", 7) == 0)
    return "NV";
  if (strncmp (ntstring, "Easy_PV", 7) == 0)
    return "PV";
  return NULL;
}

SXINT chunksname2id(name)
     char* name;
{
  if (name == NULL || strlen(name) < 2)
    return -1;
  if (strncmp (name, "GN", 2) == 0)
    return 0;
  if (strncmp (name, "GA", 2) == 0)
    return 1;
  if (strncmp (name, "GR", 2) == 0)
    return 2;
  if (strncmp (name, "GP", 2) == 0)
    return 3;
  if (strncmp (name, "NV", 2) == 0)
    return 4;
  if (strncmp (name, "PV", 2) == 0)
    return 5;
  return -1;
}
#endif /* sntfilter_c_init */
#ifdef sntfilter_c_filter
#define MAX_FILTER 57

static void
filter_snts ()
{
  struct snt c;
  SXINT i,lb,ub,id;
  char *cfirst,*clast,*ntstring;
  
  /* on supprime les groupes vides */
  for (i=1;i<=maxsnt;i++) {
    c=snts[i];
    lb=c.lb; ub=c.ub; id=c.id; cfirst=c.cfirst; clast=c.clast;
    if (lb > ub || lb == 0 || ub == 0)
      delete(lb,ub,id,-1);
  }
  for (i=1;i<=maxsnt;i++) {
    c=snts[i];
    lb=c.lb; ub=c.ub; id=c.id; cfirst=c.cfirst; clast=c.clast;
#if EBUG
    printf("Studying SNT %i: ",i);
    print_snt(c);
#endif /* EBUG */
#ifndef polish
    if (lb==1
	&& (strcmp_ci(cfirst,"a") || strcmp_ci(cfirst,"�") || strcmp_ci(cfirst,"au"))
	&& id == GP) {
      delete(1,1,NV,1);
      delete(1,1,GR,1);
    }
    if ((strcmp_ci(clast,"la") || strcmp_ci(clast,"une") || strcmp_ci(clast,"un"))
	&& ub==lb+1
	&& (snt_start(ub,GN) || snt_start(lb,GP))) {
      delete(lb,ub,GP,2);
      delete(lb,ub,GN,2);
    }
    if ((strcmp_ci(clast,"plus") || strcmp_ci(clast,"moins"))
	&& ub==lb+1
	&& snt_start_min(lb,ub+1,GN)) {
      delete(lb,ub,GN,3);
      delete(lb,ub,NV,3);
    }
    if ((strcmp_ci(clast,"plus") || strcmp_ci(clast,"moins"))
	&& ub==lb+2
	&& snt_start_min(lb,ub+1,GP)) {
      delete(lb,ub,GP,4);
    }
    if (strcmp_ci(clast,"son")
	&& ub==lb+1
	&& snt_start_min(ub,ub+1,GN)) {
      delete(lb,ub,GP,5);
      delete(lb,ub,GN,5);
    }
    if ((strcmp_ci(clast,"par") || strcmp_ci(clast,"pour")|| strcmp_ci(clast,"contre")|| strcmp_ci(clast,"sur")|| strcmp_ci(clast,"sous"))
	&& ub>=lb+1 
	&& snt_start_min(ub,ub+1,GP)) {
      delete(lb,ub,GN,6);
      delete(lb,ub,GP,6);
    }
    if ((strcmp_ci(clast,"a") || strcmp_ci(clast,"car") || strcmp_ci(clast,"mais"))
	&& ub==lb+1
	&& snt_exists(lb,lb,GA)) {
      delete(lb,ub,GN,7);
    }
    if ((strcmp_ci(clast,"la") || strcmp_ci(clast,"une") || strcmp_ci(clast,"un"))
	&& ub==lb+2) {
      delete(lb,ub,GP,8);
      delete(lb,ub,GN,8);
    }
    if ((strcmp_ci(cfirst,"sur") || strcmp_ci(cfirst,"sous") || strcmp_ci(cfirst,"pour"))
	&& id == GP) {
      delete(lb,lb,GA,9);
      delete(lb,lb,GN,9);
      delete(lb,lb+1,GN,9);
      delete(lb,lb+2,GN,9);
      delete(lb-1,lb,GN,9);
      delete(lb-1,lb+1,GN,9);
      delete(lb-1,lb+2,GN,9);
    }
    if ((strcmp_ci(clast,"est") || strcmp_ci(clast,"c'est") || strcmp_ci(clast,"�t�") || strcmp_ci(clast,"a") || strcmp_ci(clast,"elle") || strcmp_ci(clast,"elles"))
	&& id == NV) {
      delete(ub,ub,GA,10);
      delete(ub,ub,GN,10);
      delete(ub,ub+1,GN,10);
      delete(ub,ub+2,GN,10);
      delete(ub,ub+3,GN,10);
    }
    if ((strcmp_ci(clast,"politique") || strcmp_ci(clast,"politiques") || strcmp_ci(clast,"pass�") || strcmp_ci(clast,"pass�es") || strcmp_ci(clast,"unies"))
	&& ub==lb
	&& id == GA) {
      delete(ub,ub,NV,11);
    }
    if ((strcmp_ci(cfirst,"mais") || strcmp_ci(cfirst,"si"))
	&& lb==ub) {
      if (snt_exists(lb,ub,ZZ)) {
	delete(lb,ub,GR,12);
      }
      delete(lb,ub,GN,12);
    }
    if ((strcmp_ci(cfirst,"oui") || strcmp_ci(cfirst,"non") || strcmp_ci(cfirst,"m�me")
	 || strcmp_ci(cfirst,"car")
	 || strcmp_ci(cfirst,"son")|| strcmp_ci(cfirst,"mon"))
	&& lb==ub) {
      delete(lb,ub,GN,13);
    }
    if ((strcmp_ci(clast,"%") || strcmp_ci(clast,"ans") || strcmp_ci(clast,"fran�ais"))
	&& lb<ub) {
      delete(ub,ub,GN,14);
    }
    if (strcmp_ci(cfirst,"plus")
	&& id != NV) {
      if (id != GA)
	delete(lb,ub,GA,15);
      delete(lb,lb,NV,15);
    }
    if (strcmp_ci(cfirst,"plus")
	&& lb<ub) {
      delete(lb,lb,GN,16);
    }
    if ((strcmp_ci(clast,"plus") || strcmp_ci(clast,"mais"))
	&& lb<ub) {
      if (id != NV) {delete(ub-1,ub-1,NV,17);}
      delete(ub-1,ub-1,GN,17);
    }
    if (strcmp_ci(cfirst,"par") || strcmp_ci(cfirst,"pas")) {
      delete(lb,ub,GN,18);
    }
    if ((strcmp_ci(cfirst,"en"))
	&& id==PV) {
      delete(lb,ub,NV,19);
    }
    if ((strcmp_ci(cfirst,"trop"))
	&& id==GR) {
      delete(lb,ub,GA,20);
    }
    if (id == PV) {
      delete(lb+1,ub,NV,21);
      delete(lb+1,ub,GR,21);
    }
    if (lb<ub && id == GA) { 
      delete(lb,lb,GR,22);
    }
    if (lb==1 && ub==1 && id == GP) {
      delete(1,1,GN,23);
    }
    if (strcmp_ci(clast,"avenir")
	&& ( (id==PV && ub > lb + 1)
	     || (id==NV && ub > lb)
	     )  ) {
      delete(lb,ub,NV,24);
      delete(lb,ub,PV,24);
    }
    /*    if (lb==1 && ub==1 && id == GN) {
      delete(1,1,NV,24);
      } A remettre quand on saura limiter l'application de cette r�gle au cas o� le GN n'est PAS un _[Uu]w */
    /*    if (id == GN && ub>lb+2
	&& snt_exists(lb,ub-2,GN)
	&& snt_exists(ub-1,ub-1,GA)
	&& snt_exists(ub,ub,GN)) {
      delete(lb,ub,GN,25);
      } non: "ces 2|horribles|quelques derni�res ann�es */
    if (id == GN && ub>lb
	&& snt_exists(lb,ub-1,GA)
	&& snt_exists(ub,ub,GA)
	&& (snt_end_max(lb-2,lb-1,GN) || snt_end_max(lb-2,lb-1,GP))) {
      delete(lb,ub,GN,26);
    }
    if (id == NV && ub>lb
	&& snt_exists(lb,ub-1,NV)
	&& snt_start(ub,GN)) {
      delete(lb,ub,NV,27);
    }
    if (id == GN && ub==lb+2 
	&& snt_exists(lb,lb,GR)
	&& snt_exists(lb+1,lb+1,GA)
	&& snt_exists(lb+2,lb+2,GN)) {
      delete(lb,ub,GN,28);
    }
    if (id == GN && ub>lb 
	&& snt_start(lb,GA)
	&& snt_exists(ub,ub,NV)) {
      delete(lb,ub,GN,30);
    }
    if (id == NV && ub>lb 
	&& snt_exists(lb,lb,GR)
	&& snt_exists(lb+1,ub,NV)) {
      delete(lb,ub,NV,31);
    } /* "il faut ((cependant) (le faire))" */
    if (id == NV 
	&& !snt_end(lb-1,GR)
	&& snt_exists(lb-1,ub,NV)
	&& snt_end_any(lb-2)) {
      delete(lb,ub,NV,32);
    }
    if ((strcmp_ci(cfirst,"que") || strcmp_ci(cfirst,"qu'"))
	&& ub==lb
	&& snt_start(ub+1,NV)) {
      delete(lb,ub,GR,33);
    }
    if (id == GN && ub==lb+1 
	&& (lb==1 || !snt_exists(lb-1,lb-1,ZZ))
	&& snt_exists(lb,lb,GA)
	&& snt_exists(ub,ub,GN)) {
      delete(lb,ub,GN,36);
    } /* "formidable journ�e!" */
    if (id == GN && ub>lb+1 
	&& snt_exists(lb,lb,GR)
	&& snt_exists(lb+1,ub,GN)) {
      delete(lb,ub,GN,37);
    } /* "environ 100 personnes" */
    if (id == GN 
	&& snt_exists(lb-1,lb-1,GA)
	&& snt_exists(lb-1,ub,GN)
	&& cfirst[0] != sxtolower(cfirst[0])
	) {
      delete(lb-1,ub,GN,38);
    } /* "le premier ministre ((d�sign�) (Abou_Mazen))" */
    if (id == GN && ub == lb
	&& snt_exists(lb,ub,GR)
	&& (strcmp_ci(cfirst,"comment") || strcmp_ci(cfirst,"pourquoi") || strcmp_ci(cfirst,"rien"))
	) {
      delete(lb,ub,GN,39);
    } /* "comment_GR"/"pourquoi_GR" vs. "le comment et le pourquoi" */
    if (id == GN && ub > lb + 1
	&& (snt_exists(lb,ub-1,GN) || snt_exists(lb,ub-1,GP))
	&& snt_exists(ub,ub,GA)
	) {
      delete(lb,ub,GN,40);
      delete(ub,ub,GN,40);
    }
    if (id == GR && lb==1 && ub==1
	&& (strcmp_ci(cfirst,"comment") || strcmp_ci(cfirst,"pourquoi") || strcmp_ci(cfirst,"quand") || strcmp_ci(cfirst,"o�"))
	) {
      delete(lb,ub,ZZ,41);
    }
    if (id == ZZ
	&& (strcmp_ci(cfirst,"comment") || strcmp_ci(cfirst,"pourquoi") || strcmp_ci(cfirst,"quand") || strcmp_ci(cfirst,"o�"))
	) {
      delete(lb,ub,00,42);
    }
    if (id == GA && ub == lb
	&& snt_exists(lb,ub,NV)
	&& snt_start(ub+1,GP)
	&& strcmp_ci(clast + 1,"par")) {
      delete(lb,ub,NV,43);
    }
    if ((strcmp_ci(cfirst,"avec") || strcmp_ci(cfirst,"pour") || strcmp_ci(cfirst,"contre"))
          && lb == ub
          && id == GR
          && snt_start(lb,GP)) {
      delete(lb,ub,GR,44);
    }
    if (id == GP
	&& strcmp_ci(cfirst,"il")
	&& snt_start(lb,NV)
	) {
      delete(lb,ub,GP,45);
    }
    /*    if (id == ZZ && ub == lb && lb > 1
        && (strcmp_ci(cfirst,"que") || strcmp_ci(cfirst,"qu'"))
        && (strcmp_ci(cfirst-1,"plus") || strcmp_ci(cfirst-1,"moins") || strcmp_ci(cfirst-1,"de_plus") || strcmp_ci(cfirst-1,"de_moins"))
        ) {
      delete(lb,ub,GR,45);
      }*/
    if (id == GN
        && cfirst[0] >= '0' && cfirst[0] <= '9' 
        && clast[0] >= '0' && clast[0] <= '9' 
	&& snt_start(ub+1,GP)
        ) {
      delete(lb,ub,GA,46);
    }
    if (id == GN && lb == 1 && ub > lb
	&& strcmp_ci(cfirst,"des")
        ) {
      delete(lb,ub,GP,47);
    }
    if (id == GN && lb > ub
	&& snt_exists(lb,ub-1,GN)
	&& snt_exists(ub,ub,GA)
        ) {
      delete(lb,ub-1,GN,48);
      delete(ub,ub,GA,48);
    }
    if (id == ZZ && ub == lb
	&& strcmp_ci(cfirst,"puis")
        ) {
      delete(lb,ub,NV,49);
    }
    if (id == GR && ub == lb
	&& (strcmp_ci(cfirst,"surtout") || strcmp_ci(cfirst,"aussi"))
        ) {
      delete(lb,ub,GN,50);
      delete(lb,ub,ZZ,50);
      delete(lb-1,ub,ZZ,50);
    }
    if ((strcmp_ci(clast,"plus") || strcmp_ci(clast,"moins"))
	&& ub==lb
	&& snt_exists(lb,ub,GR)) {
      delete(lb,ub,GN,51);
    }
    if (id == GN
	&& ub>lb+1
	&& snt_exists(lb,lb,NV)
	&& snt_exists(lb+1,ub,GN)) {
      delete(lb,ub,GN,52);
    }
    if (strchr(clast,'0') ||
	 strchr(clast,'1') ||    
	 strchr(clast,'2') ||  
	 strchr(clast,'3') ||
	 strchr(clast,'4') ||
	 strchr(clast,'5') ||
	 strchr(clast,'6') ||
	 strchr(clast,'7') ||
	 strchr(clast,'8') ||
	 strchr(clast,'9')) {
	  if (snt_exists(lb,ub+1,GN) || snt_exists(lb,ub+2,GN)) {
	    delete(lb,ub,GN,53);	     	
	    delete(lb,ub,GA,53);
	  }
	  if (snt_exists(lb,ub+1,GP) || snt_exists(lb,ub+2,GP)) {
	    delete(lb,ub,GP,53);	     	
	  }
    }
    if (id == GP && ub > lb + 1
	&& snt_exists(lb,ub-1,GP)
	&& snt_exists(ub,ub,GA)
	) {
      delete(lb,ub,GP,54);
      delete(ub,ub,GN,54);
      delete(ub,ub,GA,54);
    }
    if (id == GP && ub>lb 
	&& snt_exists(lb,ub-1,GP)
	&& snt_exists(ub,ub,GN)) {
      delete(lb,ub-1,GP,55);
      delete(ub,ub,GN,55);
    }
    if ((strcmp_ci(clast,"que") || strcmp_ci(clast,"qu'"))
	&& ub==lb
	&& snt_end(lb-1,GN)) {
      delete(lb,ub,GR,56);
    }
    if (id == ZZ && (strcmp_ci(clast,"que") || strcmp_ci(clast,"qu'"))
	&& ub==lb
	&& lb > 1
	&& (snt_end(lb-1,NV) || snt_end(lb-1,PV))) {
      delete(lb,ub,GN,57);
      delete(lb,ub,GR,57);
    }
#endif /* !polish */
#ifdef polish
    if (strcmp_ci(cfirst,"to")
	&& snt_start(ub+1,NV)
	&& id == GN) {
      delete(lb,ub,GA,1);
      delete(lb,ub,GR,1);
      delete(lb,ub,NV,1);
    }
#endif /* polish */
  }
#if EBUG
  for (i=1;i<=maxsnt;i++) {
    c=snts[i];
    if(snt_exists(c.lb,c.ub,c.id))
      printf("Possible SNT:  %s[%i..%i]<%i..%i>\n",sntid2string[c.id],Aij2i(c.Aij),Aij2j(c.Aij),c.lb,c.ub);
  }
#endif /* EBUG */
}
#endif /* sntfilter_c_filter */
#ifdef sntfilter_c_semact
SXINT
print_chunks (Pij)
     SXINT Pij;
{
  return print_snts (Pij);
}

void
chunker_semact ()
{
  for_semact.sem_init = sntfilter_sem_init;
  for_semact.sem_final = sntfilter_sem_final;
  for_semact.sem_pass = sntfilter_sem_pass;
  for_semact.process_args = sntfilter_args_decode;
  for_semact.string_args = sntfilter_args_usage;
  for_semact.ME = output_ME;
  

  for_semact.rcvr = NULL;
  
  rcvr_spec.range_walk_kind = MIXED_FORWARD_RANGE_WALK_KIND;

}
#endif /* sntfilter_c_semact */
