/*   ******************************************************************************
     This include tdef file for the language "sxlfg-fr" has been generated
     on Thu Oct 30 09:09:06 2008
     by the SYNTAX(*) make_tdef processor
     ******************************************************************************
     (*) SYNTAX is a trademark of INRIA.
     ****************************************************************************** */


#undef TDEF_TIME
#define TDEF_TIME	1225354146

#define coo_code 6
#define sbound_code 1
#define ponctw_code 3
#define poncts_code 2
#define np_code 141
#define nc_code 52
#define cln_code 721
#define clr_code 143
#define cla_code 39
#define clg_code 38
#define cld_code 41
#define cll_code 43
#define epsilon_code 1413
