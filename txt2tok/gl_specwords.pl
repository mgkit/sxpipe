#!/usr/bin/env perl
# $Id: gl_specwords.pl 1151 2007-04-05 12:35:19Z sagot $
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$| = 1;

$corpus = "";

$lang = "unknown";

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;}
    elsif (/^-l$/) {$lang = shift}
    elsif (/^[^-]/) {if ($specwordsfile ne "") {$specwordsfile.=" "} $specwordsfile.=$_}
}

# SPECWORDSFILE:
# motif: string specword (case sensitive)
# ~motif: standard re specword
# ~~motif: case insensitive re specword
# ~~~motif: forced re specword (recognized even if not surrounded by spaces)
# ?motif: unsafe specword (not recognized in almost-only-capitalized sentences)


#if ($corpus eq "afp") {
#  @sw = ("ADDITIF", "ATTENTION", "ACTUALISATION", "CHAPEAU", "CHÂPEAU", "PAPIER GENERAL", "PAPIER GÉNÉRAL", "PHOTO", "VIDEO", "VIDÉO", "CHRONOLOGIE", "REPORTAGE", "PAPIER D'ANGLE", "=YEN=", "ENCADRÉ", "ENCADRE", "LEVER DE RIDEAU", "DOSSIER", "LEAD GENERAL", "LEAD GÉNÉRAL", "ANALYSE", "[a-zA-Z-]+(?:/[a-zA-Z-]+)+", "_NE_[^ {}]+");
#}

if ($specwordsfile ne "") {
  for (split(/ /,$specwordsfile)) {
    open(SW,"<$_") ||  die "ERROR: \"$_\" could not be open";
    while (<SW>) {
      chomp;
      s/#.*//;
      s/\s+$//;
      next if (/^\s*$/);
      if (s/^\?//) {
	push (@unsafe_sw,quotemeta($_));
      } elsif (s/^\~//) {
	if (s/^\~//) {
	  if (s/^\~//) {
	    push(@sw_forced,$_);
	  } else {
	    push(@sw_ci,$_);
	  }
	} else {
	  push(@sw,$_);
	}
      } else {
	push(@sw,quotemeta($_));
      }
    }
    close (SW);
  }
}

$rctxt  = qr/(?:^\s+|[^\}\s]\s+|[^0-9a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ\s}])/o;
$lctxt  = qr/(?:\s+[^\{\s]|\s+{[^}]+} _SPECWORD|[^0-9a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ\s}]|\s+$)/o;
$re_data = join('|',sort {length($b) <=> length($a)} @sw);
$re = qr/($rctxt)($re_data)($lctxt)/o;
$unsafe_re_data = join('|',sort {length($b) <=> length($a)} @unsafe_sw);
$unsafe_re = qr/($rctxt)($unsafe_re_data)($lctxt)/o;
$re_ci_data = join('|',sort {length($b) <=> length($a)} @sw_ci);
$re_ci = qr/($rctxt)($re_ci_data)($lctxt)/io;
$re_data_forced = join('|',sort {length($b) <=> length($a)} @sw_forced);
$re_forced = qr/( ?)($re_data_forced)( ?)/o;

#open LOG, ">gl_specwords_log.$lang" || die $!;
#binmode LOG, ":utf8";

while (<>) {
    # formattage
    chomp;
#    print LOG "$_\n";

    if (/ (_XML|_MS_ANNOTATION) *$/) {
	print "$_\n";
	next;
    }

    if ($re_data ne "" || $re_ci_data ne "" || $unsafe_re_data ne "" || $re_data_forced ne "") {
      s/^/  /;
      s/$/  /;
      if ($re_data_forced ne "") {
	if (s/$re_forced/ \{$2\} _SPECWORD /g) {
	  s/$re_forced/ \{$2\} _SPECWORD /g;
	}
      }
      if ($re_data ne "") {
	if (s/$re/$1\{$2\} _SPECWORD$3/g) {
	  s/$re/$1\{$2\} _SPECWORD$3/g;
	}
      }
      if ($re_ci_data ne "") {
	if (s/$re_ci/$1\{$2\} _SPECWORD$3/g) {
	  s/$re_ci/$1\{$2\} _SPECWORD$3/g;
	}
      }
      if ($unsafe_re_data) {
	$non_maj = $_;
	$non_maj =~ s/[^a-z]//g;
	$maj = $_;
	$maj =~ s/[^A-Z]//g;
	$is_mostly_maj = (length($maj) > 4*length($non_maj));
	if (!$is_mostly_maj) {
	  if (s/$unsafe_re/$1\{$2\} _SPECWORD$3/g) {
	    s/$unsafe_re/$1\{$2\} _SPECWORD$3/g;
	  }
	}
      }

      while (s/{([^{}]*){([^{}]*)} _SPECWORD([^{}]*)} _SPECWORD/{\1\2\3} _SPECWORD/) {}

      if ($no_sw) {
	s/(_SPECWORD)([^A-Z\s\}])/$1 _REGLUE_$2/g;
	s/(^|\s)([^\s\{]+){([^{}]*)}(_SPECWORD)(?=[^A-Z\s\}])/$1\{$2$3\} $2 _UNSPLIT_$4/g;
      } else {
	s/(_SPECWORD)([\-\/])/$1 $2/g;
	s/(_SPECWORD)([^A-Z\s\-\/])/$1 $2/g;
	s/(^|\s)([^\s\{]+){([^{}]*)}(_SPECWORD)(?=[\-\/])/$1\{$2$3\} $2 $4/g;
	s/(^|\s)([^\s\{]+){([^{}]*)}(_SPECWORD)(?=[^A-Z\s\}\-\/])/$1$2 \{$3\}$4/g;
      }
      s/^ +//;
      s/ +$//;
    }

    print "$_\n";
}
