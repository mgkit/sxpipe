#!/usr/bin/env perl
# gl_sigles.pl, nouvel (2012)
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

# language independant
# quelques exemples : FR3103, A330, etc.

$| = 1;

while (1) {
	$_=shift;
	if (/^$/) {last;}
	# PARAMETRES ??? elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;}
}

while (<>) {

	# formattage
	chomp;
	if (/ (_XML|_MS_ANNOTATION) *$/) {
		print "$_\n";
		next;
	}
	s/^\s*//o;
	s/\s$//o;

	# variables
	$an = qr/[0-9a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÃÆÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ]/o;
	$num = qr/[0-9]/o;
	$maj = qr/[A-Z]/o;
	$sig = qr/[0-9A-Z]/o;

	# reconnaissance
	s/(^| )($num+$maj+$sig*|$maj+$num+$sig*)( |$)/$1_SIGL$3/go; # Sigles maj+num ou num+maj
	#s/(^| )($maj{2,})( |$)/$1_SIGL$3/go; # Sigles au moins deux maj
	s/(^| )($maj$an*-?$num+)( |$)/$1_SIGL$3/go; # Sigles num préfixés (avec maj premier car préfixe)
	s/(\{<F id=".*?">($maj$an*-?)$num+<\/F>\}) +\2 +\1 +_NUMBER/$1 _SIGL/go; # Sigles num préfixés reconnus _NUMBER

	# sortie
	print "$_\n";
}

