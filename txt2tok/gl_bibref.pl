#!/usr/bin/env perl
# $Id$

$| = 1;

$a         = qr/[a-z����������������]/o;
$pre_nom   = qr/(?:de |d\'|du |de [lL]\'|de [lL]a |l\'|d[eo]s |v[ao]n[ \-]|ben[ \-]|ibn[ \-]|al[ \-]|of[ \-]|del |della |dell\'|Ma?c|Des?)/o;
$comp_nomp = qr/(?:[\-\']$a+| [A-Z������������]$a*)/o;
$nomp      = qr/(?:$pre_nom?[A-Z������������]$a+(?:[ \-\']$pre_nom?[A-Z������������]$a)*)/o;
$annee     = qr/(?:(?:1[89]|20)?\d{2})/o;

while (<>) {
    # formattage
    chomp;
    s/^\s*/ /o;
    s/\s*$//o;

    #reconnaissance
    $ref=s/(?<=[^\{])($nomp *, +$nomp +(?:et|\&) +al(?:\.|ii)? * +$annee[a-z]?)(?=[^\}0-9a-z\-])/\{$1\}_BIBREF /go;
    $ref=s/(?<=[^\{])($nomp +(?:et|\&) +al(?:\.|ii)? * +$annee[a-z]?)(?=[^\}0-9a-z\-])/\{$1\}_BIBREF /go;
    $ref=s/(?<=[^\{])($nomp *, +$nomp +(?:et|\&) +$nomp * +$annee[a-z]?)(?=[^\}0-9a-z\-])/\{$1\}_BIBREF /go;
    $ref=s/(?<=[^\{])($nomp +(?:et|\&) +$nomp * +$annee[a-z]?)(?=[^\}0-9a-z\-])/\{$1\}_BIBREF /go;
    $ref=s/(?<=\[)($nomp * +$annee[a-z]?)(?=\])/\{$1\}_BIBREF /go;
    $ref=s/(?<=\()($nomp * +$annee[a-z]?)(?=\))/\{$1\}_BIBREF /go;

    # sortie
    s/^ *//o;
    s/ *$//o;
    print "$_\n";
}
