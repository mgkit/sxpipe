#!/usr/bin/env perl
# $Id$

use strict;
use Getopt::Long;
use Lingua::Matcher;

$| = 1;

my $digit       = '[0-9Iligo�a^]';
my $any_year    = '[1Ili][89g][0-9Iligo�a^]{2}';
my $any_author  = '[A-Z]\w+';
my $any_authors = "$any_author(?:, $any_author)*(?: (?:et|and|und) $any_author)?";
my $any_item    = "(?:(?:p[li]|fig)\\. )?$digit+(?:-$digit+)?";
my $any_items   = "$any_item(?:, $any_item)*(?: et $any_item)?";

my ($data, $no_sw);
GetOptions(
    'data=s' => \$data,
    'no_sw' => \$no_sw
);

my $build = sub {
    my ($string) = @_;

    # allow OCR corrections
    $string =~ s/1/[1Ili]/g;
    $string =~ s/0/[0o�]/g;
    $string =~ s/9/[9g]/g;
    $string =~ s/4/[4a]/g;
    $string =~ s/2/[2^]/g;
    $string =~ s/Fl\./F[lI]./g;
    $string =~ s/Pl\./P[lI]./g;

    my ($type, $title, $authors, $year) = split(/\t/, $string);

    if ($type eq 'journal') {
        $year    = $any_year;
        $authors = $any_authors;
    }

    # build final pattern
    my $pattern = "(?:$authors, )?$title(?:, )? (?:$digit+(?:,$digit+)? )?: $any_items(?: \\($year\\)|, $year)";

    return '\b' . $pattern . '\b';
};

my $correct = sub {
    my ($string) = @_;

    my $reference;
    if ($string =~ /^(?:([^,]+), *)?(.+?)(?:, *)? *(?:($digit+(?:,$digit+)?) *)?: *(.+?)(?: *\(($digit+)\)|, *($digit+))$/o) {
        # split string into components
        my ($authors, $title, $volume, $items, $year1, $year2) = ($1, $2, $3, $4, $5);

        # fix numeric transliterations
        foreach my $field ($volume, $items, $year1, $year2) {
            $field =~ tr/Ilio�ga^/11100942/ if $field;
        }

        # fix other common transliterations
        $title =~ s/FI\./Fl./g;
        $title =~ s/PI\./PI./g;
        $items =~ s/p1\./pl./g;
        $items =~ s/f19\./fig./g;

        my @items = split(/(?:, *| *et *)/, $items);
        $items = 
        (@items == 0) ? ''                                     :
        (@items == 1) ? $items[0]                              :
        (@items == 2) ? join(" et ", @items)                   :
        join(", ", @items[0 .. ($#items-1)]) .
        " et $items[-1]";

        # reassemble components
        $reference .= "$authors, " if $authors;
        $reference .= $title;
        $reference .= " $volume" if $volume;
        $reference .= ": $items";
        $reference .= " ($year1)" if $year1;
        $reference .= ", $year2" if $year2;
    } else {
        $reference = $string;
    }

    return $reference;
};

my $matcher = Lingua::Matcher->new(
    'BIBLIOGRAPHIC_REF',
    $data ? $data : \*DATA,
    1,
    $build,
    ($no_sw ? undef : $correct)
);

$matcher->run();
