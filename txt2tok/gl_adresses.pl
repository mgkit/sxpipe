#!/usr/bin/env perl
# $Id$

use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$| = 1;

$lang="fr";

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;}
    elsif (/-robust/)  {$robust=1;}
    elsif (/^-l(?:ang)?=(.*)$/) {$lang=$1;} elsif (/^-l(?:ang)?$/) {$lang=shift;}
}


if ($lang eq "sk") {
    $voie       = qr/(?:rue|r\.|avenue|boulevard|bv?d\.?|cours|impasse|route|rte|ruelle|voie|place|pl\.?)/io;
} elsif ($lang eq "pl") {
    $voie       = qr/(?:ulic[^\s]+|ul\.|al\.|alej[^\s]+?|pl\.|plac)/io;
} elsif ($lang eq "en") {
    $voie       = qr/(?:str|street|road|rd|st|avenue|ave|place|square|gate|estate|unit)/io;
} else {
#    $voie       = qr/(?:rue|r\.|av\.?|avenue|boulevard|bv?d\.?|cours|impasse|route|rte|ruelle|voie|place|pl\.?|quai)/io;
     $voie       = qr/(?:rue|av\.?|avenue|boulevard|bv?d\.?|cours|impasse|route|rte|ruelle|voie|place|pl\.?|quai)/io;
}

sub zh_length_filter {
    $spectok = shift;
    @list = @_;
    @valids = grep { !/^$/ } @list;
    if($#valids > 1){# a matché au moins deux éléments
        return "{". join(" ",@valids) ."} $spectok";
    }
    else {
        return join("", @valids);
    }
}

if($lang =~ /^zh/){
    $province = qr/(?:省)/o;
    $district = qr/(?:县|縣)/o;
    $canton = qr/(?:乡|鄉)/o;
    $departement = qr/(?:部)/o;
    $ville = qr/(?:市)/o;
    $village = qr/(?:村)/o;
    $quartier = qr/(?:区|區)/o;
    $street = qr/(?:[街路]|大道)/o;
    $section = qr/(?:段)/o;
    $alley = qr/(?:巷)/o;
    $lane = qr/(?:弄)/o;
    $num = qr/(?:[号號])/o;
    $floor = qr/(?:楼|樓)/o;
    $zhdigit = qr/(?:[一二三四五六七八九])/o;
    $digit = qr/(?:0-9０-９])/o;
    $order = qr/(?:[十百○〇零])/;
    $number= qr/(?:$digit+|$zhdigit(?:$order$zhdigit?)*)/o;
    $anydigit = qr/(?:$zhdigit|$digit)/o;
}
else {
    $a          = qr/[a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÃÀÁÂÄÅÆÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ]/o;
    $maj        = qr/[A-ZÃÀÁÂÄÅÆÇÈÉÊËÌÍÎÏÒÓÔÖØÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ]/o;
    $preaddr    = qr/b[a\x{00E2}]t(\.|iment)?\s*([0-9]|$a)+/io;

    $robust_ocr_num = qr/[1-9iIzZf><\'\"\^jJ\N{U+00CF}\N{U+00EF}\N{U+00CE}\N{U+00EE}\N{U+00FB}]*[0-9][0-9iIzZf><\'\"jJ\N{U+00CF}\N{U+00EF}\N{U+00CE}\N{U+00EE}\N{U+00FB}]*/; # at least 1 real digit
    $robust_ocr_numlike = qr/[0-9iIzZf><\'\"\^jJ\N{U+00CF}\N{U+00EF}\N{U+00CE}\N{U+00EE}\N{U+00FB}]+/; 

    unless (defined $robust) {
      $numero     = qr/[1-9]\d*(?:\-[1-9]\d*)*/o;
      $suppl      = qr/(?:bis|ter|quater|\s*[]\s*[0-9]+)/io;
      $CP         = qr/(?:(?:[A-Z]{1,2}[\s\-])?\d[0-9\s\-]{2,}\d)/o;  
      $frpostcode = qr/([0-9]{5})/;
    } else {  # robust mode for OCR output
      $numero   = qr/$robust_ocr_num(?:\-$robust_ocr_num)*/o;
      $suppl      = qr/(bis|ter|quater)\s*$robust_ocr_numlike/;
      $CP         = qr/(?:(?:[A-Z]{1,2}[\s\-])?$robust_ocr_numlike[0-9\s\-]{2,}\d)/o;  
      $frpostcode = qr/($robust_ocr_num{5})/;
      $arrdt	  = qr/($robust_ocr_num|([XVIxvilîiÎÏï]+))(e|[^\w])/;
    }
    $pre_nom    = qr/(?:de\s|d\'_?|du\s|de\sl\'_?|de\sla\s|l\'_?|le\s|la\s|d[eo]s\s|v[ao]n[\s\-]|ben[\s\-]|ibn[\s\-]|al[\s\-]|of[\s\-]|del\s|della\s|dell\'_?)/io;
    $comp_nomp  = qr/(?:\.?[\-\']_?$a+|\s$maj$a*)/o;
    $nomp       = qr/$maj$a*(?:$comp_nomp)*/o;
    $nom        = qr/$voie\s+$pre_nom?$nomp/o;
    $bp         = qr/(?:B\.?\s*P\.?|[Bb]o[i\x{00EE}]te\s+[pP]ostale)\s*[1-9][0-9]*/o;
    $ville_pays = qr/(?:(?:[lL][eEaA][sS]?\s)?$maj(?:(?:$a|[\.\-\'])*$a))/o;
    $cedex      = qr/(?:[\s,]\s*cedex(?:\s*\d+)?(?:\s+SP)?|[\s,]\s*(?:cedex)?\s*\d+(?:er?|\x{00E8}?me|nd)?(?:\s+SP)?)/io;
    $street     = qr/(?:$numero(?:\s*$suppl|[a-dA-D])?\s*,?\s*$nom|$nom\s*(?:,\s*)?$numero(?:\s*$suppl|[a-z])?)/o;
    $leftctxt = qr/((?:^|[;:,]|[^\d]\.|_META_TEXTUAL_[^ ]+) *| +)/o;
}

while (<>) {
    # formattage
    chomp;
    if (/ (_XML|_MS_ANNOTATION) *$/) {
	print "$_\n";
	next;
    }

    s/^\s*/ /o;
    s/\s*$/ /o;
    # reconnaissance
#    s/($CP)/\{$1\}_ADRESSE /go;  


    if($lang =~ /^zh/){
        s/(..?$province)?(..?$district)?(..?$canton)?(..?$departement)?(?:(..?$ville)?|(..?$village))?(..?$quartier)?(..?$street)($number$section)?($number$alley)?($number$lane)?($number$num)?($number$floor)?/zh_length_filter("_ADRESSE", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)/ego;
    } elsif ($lang eq 'en') {
 	unless (s/([0-9]*,? ($nomp|$maj+)\s+(unit|estate|gate|$voie),?\s+($nomp|$street)?\s*$voie)(\s|$)/{$1}_ADRESSE /go) {
		s/(([0-9]|$maj)*($street|($maj($a|$maj)+))+\s+$voie\s+$ville_pays?)(\s|$)/{$1}_ADRESSE /go;
	}
    }else{
      	unless (defined $robust) { s/$leftctxt((?:$preaddr\s*,\s*)?$street(?:\s*[,\/-]?\s*(?:(?:$nomp)*)?\s*(?:[,\/-]\s*)?$CP?\s*,?\s*$ville_pays$cedex?(?:\s*,\s*$ville_pays|\s*\(\s*$ville_pays\s*\))?)?)/$1\{$2\}_ADRESSE /go; }
        else { s/$leftctxt((?:$preaddr\s*,\s*)?$street(?:\s*[,\/-]?\s*(?:(?:$nomp)*)?\s*(?:[,\/-]\s*)?$CP?\s*,?\s*($ville_pays|$cedex|$arrdt)?(?:\s*,\s*$ville_pays|\s*\(\s*($ville_pays|$arrdt)\s*\))?)?)/$1\{$2\}_ADRESSE /go; }
        s/$leftctxt($bp(?:\s*[,\/-]?\s*($CP|$frpostcode)?\s*,?\s*$ville_pays$cedex?(?:\s*,\s*$ville_pays|\s*\(\s*$ville_pays\s*\))?)?)/$1\{$2\}_ADRESSE/go;
        s/$leftctxt(${frpostcode}\s+${ville_pays}( $ville_pays)?$cedex?)/$1\{$2\}_ADRESSE/go;
	s/$leftctxt(${frpostcode}\s+[A-Z][A-Z-]+$cedex)/$1\{$2\}_ADRESSE5/go;
 	s/[^\{]($bp)/\{$1\}_ADRESSE/g;
    }
        # sortie
        
    s/(\{.+?)\}_ADRESSE( ?[-,] )\{(.+?\}_ADRESSE)/$1$2$3/go;
    if ($no_sw) {
	s/(_ADRESSE)([^\s\}])/$1 _REGLUE_$2/g;
    	s/(^|\s)([^\s\{]+){([^{}]*)}_ADRESSE/$1\{$2$3\} $2 _UNSPLIT__ADRESSE/g;
    } else {
    	s/(_ADRESSE)([^\s])/$1 $2/g;
    	s/(^|\s)([^\s\{]+){([^{}]*)}_ADRESSE/$1$2 \{$3\}_ADRESSE/g;
    }
   
   # post-correction : {58 av. J.-C}_ADRESSE
   s/\{(.*?av\..*?J\.?\-C.*)\}_ADRESSE/$1/g;
   #post-correction : 44-48, pl. VI
   s/\{([0-9][0-9\.\,-]* [Pp]l\. [XV][XVI, -e]*)\} ?_ADRESSE/$1 /g;
 
   s/^ //;
   s/ $//;
   print "$_\n";
}


