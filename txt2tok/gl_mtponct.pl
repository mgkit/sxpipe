#!/usr/bin/env perl
# $Id$

# grammaire locale des ponctuations multi-tokens, des "M ." et des "," en fin de phrase, des mots de type "incomp(let)" et des apostrophes qui sont des back-quotes
# ad hoc pour la campagne easy et sa segmentation légendaire par sa pitoyable nullité
# mais peut toujours servir pour certifier une certaine robustesse
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$| = 1;

$lang="fr";

$corpus = "";
$use_epsilons = 1;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;}
    elsif (/^-l(?:ang)?=(.*)$/) {$lang=$1;} elsif (/^-l(?:ang)?$/) {$lang=shift;}
    elsif (/^-md$/) {$corpus="mondediplo";}
    elsif (/^-ot$/) {$oral_transcript = 1;}
    elsif (/^-no_eps$/) {$use_epsilons = 0;}
    elsif (/^-xml$/) {$xml = 1;}
}

my $rctxt = qr/(?:[ ;:\?\!\(\)\[\]\/\-\"]|[,\.][^0-9])/;

while (<>) {
    # formattage
    chomp;
    if (/ (_XML|_MS_ANNOTATION) *$/) {
	print "$_\n";
	next;
    }

    my $inputline = $_;
    my $line = "";
    if ($xml) {
      while ($inputline =~ s/(<[^>]*){([^}>]+)} _[^ {}<>]+ /$1$2/g) {}
      while ($inputline =~ s/^(.*?)(<[^>]+>(?:\s*<[^>]+>)*)//) {
	my $sequence = $1;
	my $markup = $2;
	$line .= process_sequence($sequence) unless $sequence =~ /^\s*$/;
	$line .= "$markup";
      }
      $line .= process_sequence($inputline) unless $_ =~ /^\s*$/;
    } else {
      $line = process_sequence($_);
    }
    $_ = $line;

    print "$_\n";
}

sub process_sequence {
  $_ = shift;
  s/^\s*/  /o;
  s/\s*$/ /o;
  s/  +/ /g;
  # variables
  $l    = qr/[a-zàáâäåæãßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźżñA-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ]/o;
  $maj  = qr/[A-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ]/o;
  $min  = qr/[a-zàáâäåãæßçèéêëìíîïòóôöøœùúûüýÿąćčďęěĺľłńňŕřśšťůźż]/;
  $m=qr/$l+/;
  $m_noinitmaj=qr/$min$l*/;
  # reconnaissance
  #    s/([\!\?][\!\? ]+[\!\?])/ {$1} <<<$1>>> /go;
  s/([^\s\{\}]+)(\s+\`) / {$1$2} <<<$1\'>>> /go;
  s/(?<!})(''+)/{$1} "/go;
  s/(?<!})(_UNDERSCORE(?:\s*_UNDERSCORE)+)/{\1} _UNDERSCORE/g;
  s/(?<!})(-(?:\s*-)+)/{\1} -/g;

  if ($oral_transcript) {
    s/^/  /;
    s/$/  /;
    s/(?<= )((?:\*|\^\^)?)\(((?:$l|[-'])*)\)((?:$l|[-'])+)([\.,]? ?)/{$1($2)$3} $2$3 $4/g;
    s/(?<= )((?:\*|\^\^)?)((?:$l|[-'])+)\(((?:$l|[-'])*)\)((?:$l|[-'])*)([\.,]? ?)/{$1$2($3)$4} $2$3$4 $5/g;
    s/(?<!}) \^\^([^ \.,]+) ?/ {^^$1} _SPECWORD_$1 /g;
    s/^ +/ /;
    s/ +$/ /;
  }

  # ATTENTION : blanc non convertis en \s
  s/(?<!}) (\S*oe) (u\S*)/ {\1 \2} \1\2 /goi; # œuvre -> [tokeniseur pas sympa qui ne sait pas que œ est un \w] -> oe uvre

  if ($oral_transcript && $lang eq "fr") {
    # répétition de mots-outils (dès 2 fois)
    s/(?<!}) ((?:l[ae]|les)(?: (?:l[ea]|les|euh))+) (l[ae]|les) / {$1 $2} $2 /goi; # comme suivants
    for $mot (qw(vos moi il ne sa les la le qui à dans un ce y on des et sont du j'ai)) { # un? ; une une devrait donner (une une|une)
      # supprimé "en", cf. "En en mangeant deux, il part" (merci Eric)
      s/(?<!}) ($mot(?: (?:$mot|euh))* $mot) / {$1} $mot /gi; # pas d'option "o"
    }
    if ($use_epsilons) {
      s/(?<!}) ($m)((?: \1| euh)+) +(\1)(?= )/ {$1$2} _EPSILON $3/goi;
      s/(?<!}) (de(?: (?:de|euh))*) (d[e']) / {$1} _EPSILON $2 /goi;   # comme précédemment, mais cas spécial pour de/d'
      s/(?<!}) (le(?: (?:le|euh))*) (l[e']) / {$1} _EPSILON $2 /goi;   # comme précédemment, mais cas spécial pour le/l'
      s/(?<!}) (la(?: (?:la|euh))*) (l[a']) / {$1} _EPSILON $2 /goi;   # comme précédemment, mais cas spécial pour le/l'
    } else {
      s/(?<!}) ($m)( (?:\1 |euh )+\1)(?= )/ {$1$2} $1/goi;
      s/(?<!}) (de(?: (?:de|euh))*) (d[e']) / {$1 $2} $2 /goi;   # comme précédemment, mais cas spécial pour de/d'
      s/(?<!}) (le(?: (?:le|euh))*) (l[e']) / {$1 $2} $2 /goi;   # comme précédemment, mais cas spécial pour le/l'
      s/(?<!}) (la(?: (?:la|euh))*) (l[a']) / {$1 $2} $2 /goi;   # comme précédemment, mais cas spécial pour le/l'
    }
    # répétition de suites de 2 mots "non répétables"
    for $mot ("c' est","il faut","on a","qu' un", "il est") { # douteux pour "qu'un": Il n'y en a qu'un qu'un exemple pourrait éclairer
      if ($use_epsilons) {
	s/(?<!}) ($mot(?: (?:$mot|euh))*) $mot / {$1} _EPSILON $mot /gi; # pas d'option "o"
      } else {
	$mot1 = $mot;
	$mot1 =~ s/ .*//;
	s/(?<!}) ($mot(?: (?:$mot|euh))*) $mot / {$1 $mot1} $mot /gi; # pas d'option "o"
      }
    }
    # répétition de suites d'au moins 3 mots
    if ($use_epsilons) {
      s/(?<!}) (($m)(?: $m){2,})( \1)* \1 / {$1$3} _EPSILON $1 /goi; # il faudrait ne pas faire ça si le mot est, par exemple, oui, non, ...
    } else {
      s/(?<!}) (($m)(?: $m){2,})( \1)* \1 / {$1$3 $2} $1 /goi; # il faudrait ne pas faire ça si le mot est, par exemple, oui, non, ...
    }
    # mot_1 ... mot_n pas de mot_1 ... mot_n de 
    s/(?<!}) ($m(?: $m)+)( pas (?:des?|du)) \1 (des?|du) / {$1$2 $1} $1 $3 /goi; # je veux pas de je veux de (la vodka)
    # mot_1 ... mot_n de mot_1 ... mot_n pas de 
    s/(?<!}) ($m(?: $m)+)( (?:des?|du)) \1 pas (des?|du) / {$1$2 $1} $1 pas $3 /goi;
    # hésitations
    s/ ((?:euh|hein)(?: (?:euh|hein|,))*) / {$1} _EPSILON /goi;
  }

  #    s/^(\*|_UNDERSCORE|_)(\s+[^\s\{\}]+)/{$1} _EPSILON $2/go; # semble périmé depuis qu'on reconnait les _META_TEXTUAL_*
  while (s/\{([^}]+)\}\s*_EPSILON(\s*)\{([^}]+)\}\s*_EPSILON/\{$1$2$3\} _EPSILON/g) {
  }

  # mots de la forme incomp(let)
  if (/} *{/) {
    print STDERR "Warning: found pattern /} {/ in input line $_. Pattern deleted\n";
  }

  if ($oral_transcript && $lang eq "fr") { # ATTENTION : blanc non convertis en \s
    s/(?<=[ \'\{\}])([^ \{\}]+) +\( +([elns]) +\)/\{$1 ( $2 )\} $1$2/go;
    s/(?<=[ \'\{\}])([^ \{\}]+) +\( +(ah|ale|and|ants|aut|bibi|ble|bution|de|dra|dustrie|ectivement|ens|ère|éressent|es|ette|eur|glophone|iaisons|il|incipale|inte|ion|ir|iscuté|jets|jours|lait|le|les|lle|llion|monie|moyenne|nage|nations|ne|née|nnonçait|ord|our|ous|ouver|pinion|que|ques|qu\'il|rait|re|registre|reil|remment|res|riés|rin|roupes|rs|se|sonniers|spectif|sser|staté|ste|tancourt|te|téressent|tissement|tre|trices|trouverons|us|vance|ve|vez|vient|xistent) +\)/\{$1 ( $2 )\} $1$2/go;
    s/(?<=[ \'\{\}])([^ \{\}]+)( *\( *)(s|es?|nt)( *\))/\{$1$2$3$4\} $1$3/go; # pour "mot(s)", mais pas top
    s/(?<=[ {])([^ {}]+)} *{\1/$1/g; # {euh re} re ( ste ) -> {euh re} {re ( ste )} reste -> {euh re ( ste )} reste
  }

  if ($corpus eq "mondediplo") {
    # premiers mots de la phrase entièrement en majuscule, suivi d'un mot à intiale majuscule
    my $postSB      = qr/(?:\{[^\}]*\}\s*)?(?:$maj\'?$min|M\.)/o;
    my $xmaj        = qr/[A-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ\-\'\"\(\)\[\]\%]/o;
    my $xxmaj       = qr/[A-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ\-\'\"\(\)\[\]\%\!\?\:]/o;
    my $xmajORspace = qr/[A-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ\-\'\"\(\)\[\]\% ]/o;
    if ($no_sw) {
      s/^\s*($xxmaj{3,})(\s+$postSB)/ $1 _SENT_BOUND$2/ ||
	s/^\s*([A-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ\-\'\"]$xxmaj*\s+(?:$xmaj$xmajORspace*\s)?)($xmaj{2,})(\s+$postSB)/ $1 $2 _SENT_BOUND$3/;
    } else {
      s/^\s*($xxmaj{3,})(\s+$postSB)/ {$1} $1 {$1} ;$2/ ||
	s/^\s*([A-ZÀÁÂÄÅÆÃÇÈÉÊËÌÍÎÏÒÓÔÖØŒÙÚÛÜÝŸĄĆČĎĘĚĹĽŁŃŇŔŘŚŠŤŮŹŻÑ\-\'\"]$xxmaj*\s+(?:$xmaj$xmajORspace*\s)?)($xmaj{2,})(\s+$postSB)/ $1 {$2} $2 {$2} ;$3/;
    }
  }


  # flèches
  if ($no_sw) {
    s/(?<!})\s+([\-\­_=]+(?:&gt;)+|(?:&gt;)(?:&gt;)+)\s+/ {$1} -&gt; /g;
    s/(?<!})\s+((?:&lt;)(?:&lt;)+|<+[\-\­_=]+)\s+/ {$1} &lt;- /g;
    s/(?<!})\s+((?:&lt;)+[\-\­_=]+(?:&gt;)+)\s+/ {$1} &lt;-&gt; /g;
  } else {
    s/^/!/;
    s/(?<=[^\}\s])\s*([\-\­_=]+(?:&gt;)+|(?:&gt;)(?:&gt;)+)\s*/ {$1} -&gt; /g;
    s/(?<=[^\}\s])\s*((?:&lt;)(?:&lt;)+|<+[\-\­_=]+)\s*/ {$1} &lt;- /g;
    s/(?<=[^\}\s])\s*((?:&lt;)+[\-\­_=]+(?:&gt;)+)\s*/ {$1} &lt;-&gt; /g;
    s/^!//;
  }

  while (s/<<<([^\s<>]*) +([^<>]*)>>>/<<<$1$2>>>/go) {
  }
  s/<<<//go;
  s/>>>//go;
  s/^ +//o;
  s/ $//o;
  return $_;
}
