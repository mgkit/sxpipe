#!/usr/bin/env perl

use strict;

$| = 1;

my $not_first_line=0;
my $last_line_was_xml=0;
my $ks=0;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-ks$/ || /^-keep_segmentation$/i) {$ks=1;}
#    elsif (/^-l(?:ang)?=(.*)$/) {$lang=$1;}
}

my @chunk_types_stack; my $t;

my $ns=-1;
my ($orth,$line);
my ($disamb,@disambs);
while (<>) {
  chomp;
  if (/^<orth>(.*)<\/orth>/) {
    $_=$1;
    $orth=$_;
    #	s/ /_/g; #useless by definition of tok/orth
    if ($last_line_was_xml) {
      print "\n";
    }
    if ($ns == 0) {
      $line.="\t";
    } elsif ($ns == 1) {
      $line.=" ";
    }
    $line.=$_;
    $last_line_was_xml=0;
  } elsif (/<chunk type=\"s\">/) {
    &print_line();
    $ns=-1;
    if ($ks) {
      $line = "{$_} _XML";
      $last_line_was_xml=1;
    } else {
      $last_line_was_xml=0;
    }
    push(@chunk_types_stack,"s");
  } elsif (/^\s*<\/orth>/) {
  } elsif (/^\s*<lex>/) {
  } elsif (/^\s*<lex disamb=\"1\"><base>(.*)<\/base><ctag>(.*)<\/ctag><\/lex>/) {
    if ($disamb ne "") {$disamb.="  "}
    $disamb.="$orth $1 $2";
  } elsif (/^\s*<tok>/) {
    $disamb="";
  } elsif (/^\s*<\/tok>/) {
    if ($disamb eq "") {
      print STDERR "Problem: token \"$orth\" didn't get disamb information\n";
    }
    push(@disambs,$disamb);
    $ns=0;
  } elsif (/^\s*<ns\/>/) {
    $ns=1;
  } elsif (/^\s*<[^\/]/) {
    &print_line();
    $line = "{$_} _XML";
    $last_line_was_xml=1;
    if (/^\s*<[^\/].*[^\/]>\s*$/) {
      push(@chunk_types_stack,"o");
    }
  } elsif (/^\s*<\//) {
    $t=pop(@chunk_types_stack);
    if ($t eq "o"
	|| ($t eq "s" && $ks)) {
      &print_line();
      $line = "{$_} _XML";
      $last_line_was_xml=1;
    }
  } else {
    print $line; $line = "";
    if ($not_first_line) {
      print "\n";
    }
    $line=$_;
    $last_line_was_xml=0;
    $not_first_line=1;
  }
}
&print_line();
print "\n";

sub print_line {
  $not_first_line=1;
  if ($line ne "") {
    if ($#disambs >= 0) {
      print "{".join("\t",@disambs)."} _MS_ANNOTATION\n";
      @disambs=();
    }
    print $line."\n";
    $line = "";
  }
}
