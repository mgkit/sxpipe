#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use locale;

#------------------------------------------------------------------------------------------------------
$|=1; 


while(<>){
	chomp;
	#Si espace entre apostrophe et caractère élisé
	#$_=~ s/(c|d|j|l|m|n|qu|s|t|C|D|J|L|M|N|Qu|S|T) +[’']/$1'/g;
	$_=~ s/(^| )([^ ]*)(c|d|j|l|m|n|qu|s|t|C|D|J|L|M|N|Qu|S|T) +([’'])($| )/$1\{$2$3_$4__$2$3'\} _GlueApos $5/g;
	$_=~ s/(^| )([^ ]*)(c|d|j|l|m|n|qu|s|t|C|D|J|L|M|N|Qu|S|T) +([’'])([^ ]+)($| )/$1\{$2$3__$2$3'\} _AddApos \{$4$5__$5\} _SuppApos $6/g;
	
	
	#Si absence apostrophe
	$_=~ s/(^| )([^ ]*qu|l) +([aàeéèêiou])/$1\{$2__$2'\} _AddApos $3/gi;
	$_=~ s/(^| )(j|n) +ai($| )/$1\{$2__$2'\} _AddApos ai$3/gi;
	$_=~ s/(^| )(n|t) +es($| )/$1\{$2__$2'\} _AddApos es$3/gi;
	$_=~ s/(^| )(c|m|n|qu|s|t) +est($| )/$1\{$2__$2'\} _AddApos est$3/gi;
	$_=~ s/(^| )(l|m|n|qu|t) +([aà]s?)($| )/$1\{$2__$2'\} _AddApos $3$4/gi;
	$_=~ s/(^| )(d|l|qu) +un/$1\{$2__$2'\} _AddApos un/gi;

	print "$_\n";
}
