#!/usr/bin/env perl
# $Id$
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$| = 1;

#$lang="fr";

$hyphen = qr/(?:-| - )/;
$date = qr/((du )?\d+ (janv\.|févr?\.|avr\.|juill\.|sept\.|oct\.|nov\.|déc\.|janvier|février|mars|avril|mai|juin|juillet|août|septembre|octobre|novembre|décembre) \d+)/i;
$art_id = qr/(?:art\. (1er|\d+e|(?:[LR]\.? ?)?\d+(?:(\.|$hyphen)\d+)?)(?:${hyphen}[IVX]+)?(?:(?:, |$hyphen)\d+o)?(?:,? al(?:inéa|\.) \d+|,? al(?:inéa|\.) \d+ (?:à|et) \d+)?|art\. \d+ (?:à|et) \d+)/i;


while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;} # change KG { die "Option -no_sw not supported by gl_jurid.pl";}
    elsif (/^-l(?:ang)?=(.*)$/) {$lang=$1;} elsif (/^-l(?:ang)?$/) {$lang=shift;}
}

while (<>) {
  # formattage
  chomp;
  if (/ (_XML|_MS_ANNOTATION) *$/) {  
    print "$_\n";
    next;
  }

  if ($lang eq "fr") {

    s/-LRB-/<_<(>_>/g;
    s/-RRB-/<_<)>_>/g;
    s/-LSB-/<_<[>_>/g;
    s/-RSB-/<_<]>_>/g;
    s/-LCB-/<_<((>_>/g;
    s/-RCB-/<_<))>_>/g;
    
    #s/((?:(?:l')?article \d+ de )?(?:l')(?:arr.|arrêté|accord) $date(?:, $art_id)?)/{\1} _JURIDID/gi;
    s/((?:l')?(?:ord.|ordonn?ance) (?:n[o°] )?\d+-\d+ $date(?:,? $art_id)?)/{\1} _JURIDID/gi;
    s/((?:l!)?accord(?: (?:national(?: interprofessionnel)?|d'application|franco-[^ ]+|européen))? (?:n[o°] )?\d+(?:-\d+-\d+)?(?: $date)?(?: (?:ann\.|annexe) [A-Z0-9]+)?(?:,? $art_id)?)/{\1} _JURIDID/gi;
    s/(c\. civ\.(?:,? $art_id)?)/{\1} _JURIDID/gi;
    
    # KG. based on FR OPOCE texts
    s/([Dd]écisions? [0-9]+\/[0-9]+(\/CEE?)?)/{\1}_JURIDID/g;
    s/(([Dd]écisions?|[Aa]rrêts?) du $date)/{\1}_JURIDID/g;
    s/(acte d'adhésion)/{\1}_JURIDID/g;
    s/(([Dd]irectives?|[Aa]rrêts?) ([0-9-\/]+)(CEE?)?( du Conseil)?(,? du $date)?)/{\1}_JURIDID/g;

    s/(([Dd]irectives?|[Rr]\N{U+00E8}glements?) du Conseil,? (du $date|n[oº]s?\.? [0-9]+\/?[0-9]*))/{\1}_JURIDID/g;
    s/(([lL][\N{U+0027}\N{U+2019}\N{U+02BC}])?([aA]rticle|art\.) ([0-9]+(?:_UNDERSCORE)?[a-zA-Z]{0,2}|[IVX]+)([-\.][0-9]+)*\,?( (\N{U+00A7}|paragraphes?) [0-9]+((,| et| ou| \N{U+00E0}) [0-9]+){0,3})?(,?( premier| second| [^ ]+i\N{U+00E8}me) alin\N{U+00E9}a)?(,? CEE?)?(,? sous [a-z]\))?)/{\1}_JURIDID/g;
    s/(([lL]es )?[Aa]rticles (([0-9]+[a-z]{0,2}|[IVX]+)( CEE?)?, )?([0-9]+|[IVX]+)( CEE?)? (et|ou|\N{U+00E0}) ([0-9]+|[IVX]+)( CEE?)?) /{\1}_JURIDID /g;
    s/(([Cc]irc.|[Cc]irculaires?|[Rr]\N{U+00E8}glements?)( \(CEE?\))? ([Nn][o\N{U+00BA}\N{U+00B0}]\.? )?([A-Z0-9\/-])+(,? [Nn][o\N{U+00BA}\N{U+00B0}]\.? [0-9]+)?(,? du $date)?)/{\1}_JURIDID/g;
    s/(([Dd]écr\.|[Dd]écret-lois?|[Dd]écrets?)( législatifs?| roya[lu]x?| présidentiels?| ministériels?| d[\N{U+0027}\N{U+2019}\N{U+02BC}]application){0,2}( codifiés?)?(( [Nn][o\N{U+00BA}\N{U+00B0}]s?\.?)? ([0-9\/\.-\/])+)?(,? (du $date|de [12][0-9]{3}))?)/{\1}_JURIDID/g;
    s/ ([Ll]oi( [Nn][o\N{U+00BA}\N{U+00B0}]\.?)? [0-9\/\-IVXLCM]+(,? (du $date|de [12][0-9]{3}))?)\.?/ {\1}_JURIDID/g;
    s/ ([Ll]oi (du $date|de [12][0-9]{3}))\.?/ {\1}_JURIDID/g;
    s/(([Aa]nnexe ([0-9]+|[XIVMCL]+), )?[Pp]oints? [0-9]+[a-z]{0,2}([\.\-][0-9]*)*(( et| ou| \N{U+00E0}|,) [0-9]+([\.\-][0-9]+)*)*( [A-Z])?(,? ([Dd]isp\.|paragraphes?|\N{U+00A7}|p\.) [0-9]+(( et| ou| \N{U+00E0}|,) [0-9]+([\.\-][0-9]+)*)*)?(,?( premier| second| [^ ]+i\N{U+00E8}me) (tiret|alin\N{U+00E9}a))?( supra|(,? sous [a-z][ivxc]*\)?))?)(?![A-Za-z])/{\1}_JURIDID/g;
    s/(([Aa]nnexe ([0-9]+|[XIVMCL]+), )?[Pp]oints? (([A-Zixv][IXVixv]*\)?))( [\.\-0-9ivxl]+\)?)*(( et| ou| \N{U+00E0}|,)( [A-Zixv])?( ?([IXVixv]+|[a-z])\))([\.\-0-9]+)*)*( [A-Z])?(,? ([Dd]isp\.|paragraphes?|\N{U+00A7}|p\.) [0-9]+(( et| \N{U+00E0}|,) [0-9]+([\.\-][0-9]+)*)*)?(,?( premier| second| [^ ]+i\N{U+00E8}me) (tiret|alin\N{U+00E9}a))?(,? supra|(,? sous [a-z][ivxc]*\)?))?)(?![A-Za-z])/{\1}_JURIDID/g;
    
    s/([Aa]nnexe ([0-9]+|[XIVMCL]+) d[eu] ){(.+?}_JURIDID)/{\1\3/g;
    
    s/\}_JURIDID (d[ue]|de la) \{([^\}]+)\}_JURIDID/ $1 $2\}_JURIDID/;
    s/\}_JURIDID (du (règlement|traité) (de procédure|de base|CE)( du Tribunal)?)/ $1\}_JURIDID/;
    s/\}_JURIDID (de la (Commission|[Dd]écision|communication|[Cc]harte)( des griefs| des Nations [uU]nies|,? du $date)?)/ $1\}_JURIDID/;

    
    if ($no_sw) { s/\{(.+?)\}_JURIDID([a-z]+(\.|,| |$))/$1$2/g;}

    s/<_<\(>_>/-LRB-/g;
    s/<_<\)>_>/-RRB-/g;
    s/<_<\[>_>/-LSB-/g;
    s/<_<\]>_>/-RSB-/g;    
    s/<_<\(\(>_>/-LCB-/g;
    s/<_<\)\)>_>/-RCB-/g;
  }
   
  s/^\s+/ /o;
  s/\s+$/ /o;

  print $_."\n";
}

