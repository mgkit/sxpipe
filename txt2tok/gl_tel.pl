#!/usr/bin/env perl
# $Id$
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$| = 1;

$lang="fr";

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;}
    elsif (/^-l(?:ang)?=(.*)$/) {$lang=$1;} elsif (/^-l(?:ang)?$/) {$lang=shift;}
}

$telN = qr/\d{2,}[\s\-\.]+\d{2,}[\s\-\.]+\d{2,}(?:[\s\-\.]+\d{2,})*/o;
$tel2 = qr/\d{3,}[\s\-]+\d{3,}/o;
$pref = qr/(?:\+\s*\d+\s*)?(?:\(\s*\d+(?:\-\d)?\s*\)\s*|\d\s*)?/o;

while (<>) {
    # formattage
    chomp;
    if (/ (_XML|_MS_ANNOTATION) *$/) {
	print "$_\n";
	next;
    }

    s/^\s*/ /o;
    s/\s$/ /o;

    # reconnaissance
    s/(?<=[^0-9{\/])((?:$pref)?$telN)($|[^\/0-9}-])/{$1} _TEL$2/go;
    s/(?<=[^0-9{\/])($pref$tel2)($|[^\/0-9}-])/{$1} _TEL$2/go;
    s/([tT][ée]l\s*[\.:]\s*[\.:]\s*)($tel2)($|[^\/0-9}-])/$1\{$2} _TEL$2/go;
    s/([Ff]ax\s*[\.:]\s*[\.:]?\s*)($tel2)($|[^\/0-9}-])/$1\{$2} _TEL$2/go;

    # against over-generation
    s/{([^}]+)} _TEL(\s*(?:tys|mln|mld|bln|millier|mill?ion|mill?iard|bill?ion|eur|doll?ar|franc))/$1$2/gio;

    # sortie
    if ($no_sw) {
	s/(_TEL)([^\s{}])/$1 _REGLUE_$2/g;
	s/(^|\s)([^\s\{]+){([^{}]*)} _TEL/$1\{$2$3\} $2 _UNSPLIT__TEL/g;
    } else {
	s/(_TEL)([^\s])/$1 $2/g;
	s/(^|\s)([^\s\{]+){([^{}]*)} _TEL/$1$2 \{$3\} _TEL/g;
    }
    print "$_\n";
}
