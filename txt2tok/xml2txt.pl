#!/usr/bin/env perl
# $Id: easy2txt.pl 2216 2009-03-12 09:52:29Z sagot $

# Transforme un corpus XML dont TOUTES les balises forment une ligne à elles toutes seules, quel
# qu'en soit l'encodage (UTF-8 ou non, qui est alors supposé comme étant latin-1), en une alternance
# de lignes de type {<balise XML>} _XML et de phrases encodées en latin-1

$| = 1;

binmode STDOUT, ":encoding(iso-8859-1)";
binmode STDERR, ":encoding(iso-8859-1)";

my $firstline = 1;

while (<>) {
  s/[­·]/-/g;
  s/œ/oe/g;
  s/€/EUR/g;
  chomp;
  if (s/^<(?:\?|\/?DOCUMENT|\!DOCTYPE)//) {
    if (/encoding\s*=\s*\"?([^\"\?]+)\"?.*$/) {
      $encoding = $1;
      if ($encoding =~ /^utf-?8/i) {
	binmode STDIN, ":utf8";
      } else {
	binmode STDIN, ":encoding(iso-8859-1)";
      }
    }
  } elsif ($_!~/^\s*\{/) {
    s/^(\s*<\/?.*>\s*)$/{\1} _XML/g;
  }
  print $_."\n";
}

