#!/usr/bin/env perl
# $Id$
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";


# ATTENTION: à exécuter AVANT gl_number.pl

$| = 1;

$lang="fr";

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_sw$/ || /^-no_split-words$/i) {$no_sw=1;}
    elsif (/^-l(?:ang)?=(.*)$/) {$lang=$1;} elsif (/^-l(?:ang)?$/) {$lang=shift;}
}

$numFR1   = qr/(?:\d{1,3}(?:(?:\s|_UNDERSCORE)+\d{3})*(?:(?:\s|_UNDERSCORE)*,(?:\s|_UNDERSCORE)*\d+)?)/o;
$numFR2   = qr/(?:\d{1,3}(?:\.\d{3})*(?:(?:\s|_UNDERSCORE)*,(?:\s|_UNDERSCORE)*\d+)?)/o;
$numNOSEP = qr/(?:\d+(?:(?:\s|_UNDERSCORE)*[,\.](?:\s|_UNDERSCORE)*\d+)?)/o;
$numUS    = qr/(?:\d{1,3}(?:\s*,\s*\d{3})*(?:\s*\.\s*(?:[12]\s*\/\s*[234]|\d+))?)/o;
$num      = qr/(?:$numFR1|$numNOSEP|$numFR2|$numUS)/o;
$multinum = qr/$num(?:\s*[\-\/\.\­]\s*$num)?/o;

$pre_num_currency = qr/(?:\$|£|€|¥|Fr\.?(?: ?Fr\.)?)/o;
$post_num_currency = qr/(?:\$|£|€|¥|[Ff]r(?:\.? Fr)?|francs?|centimes?|euros?)/o;
$post_num_subcurrency = qr/(?:cents?|centimes?|c)/o;
$post_num_currency_with_dot = qr/(?:[Ff]r\.? Fr\.|[Ff]r\.|cent\.)/o;
$post_num_subcurrency_with_dot = qr/(?:c\.|cent\.)/o;

$rctxt = qr/(?=[ \.,;:\?\!\(\)\[\]\/\-\"\«\»])/;
$rctxt_after_dot = qr/(?=[\.,;:\?\!\(\)\[\]\/\-\"\«\»].| [a-zàáâäåãßçèéêëìíîïòóôöøùúûüýÿąćčďęěĺľłńňŕřśšťůźżñ;, ])/;

while (<>) {
  chomp;
  if (/ (_XML|_MS_ANNOTATION) *$/) {
    print "$_\n";
    next;
  }
  
  s/  +/ /g;
  s/^/  /;
  s/$/  /;

  if ($no_sw) {
    s/(?<=[ \t])($pre_num_currency$multinum)$rctxt/\{$1}_CURRENCY/go;
    s/(?<=[ \t])($pre_num_currency\s+$multinum)$rctxt/\{$1}_CURRENCY/go;
    s/(?<=[^\}])([^ \t]*?[\.,;:\?\!\(\)\[\]\/\-\"])($pre_num_currency$multinum)$rctxt/{$1$2} $1 \{$1$2}_CURRENCY/go;
    s/(?<=[^\}])([^ \t]*?[\.,;:\?\!\(\)\[\]\/\-\"])($pre_num_currency)\s+($multinum)$rctxt/{$1$2} $1 \{$1$2 $3}_CURRENCY/go;

    s/(?<=[ \t])($num$post_num_subcurrency_with_dot)$rctxt_after_dot/\{$1}_SUBCURRENCY/go;
    s/(?<=[ \t])($num\s+$post_num_subcurrency_with_dot)$rctxt_after_dot/\{$1}_SUBCURRENCY/go;
    s/(?<=[ \t])($num$post_num_subcurrency)$rctxt/\{$1}_SUBCURRENCY/go;
    s/(?<=[ \t])($num\s+$post_num_subcurrency)$rctxt/\{$1}_SUBCURRENCY/go;

    s/(?<=[ \t])($multinum$post_num_currency)$rctxt/\{$1}_CURRENCY/go;
    s/(?<=[ \t])($multinum\s+$post_num_currency)$rctxt/\{$1}_CURRENCY/go;
    s/(?<=[^\}])([^ \t]*?[\.,;:\?\!\(\)\[\]\/\-\"])($multinum$post_num_currency_with_dot)$rctxt_after_dot/{$1$2} $1 \{$1$2}_CURRENCY/go;
    s/(?<=[^\}])([^ \t]*?[\.,;:\?\!\(\)\[\]\/\-\"])($multinum)\s+($post_num_currency_with_dot)$rctxt_after_dot/{$1$2} $1 \{$1$2 $3}_CURRENCY/go;
    s/(?<=[^\}])([^ \t]*?[\.,;:\?\!\(\)\[\]\/\-\"])($multinum$post_num_currency)$rctxt/{$1$2} $1 \{$1$2}_CURRENCY/go;
    s/(?<=[^\}])([^ \t]*?[\.,;:\?\!\(\)\[\]\/\-\"])($multinum)\s+($post_num_currency)$rctxt/{$1$2} $1 \{$1$2 $3}_CURRENCY/go;

    s/}_CURRENCY(\s*){([^{}]+)}_SUBCURRENCY/$1$2}_CURRENCY/g;
    s/{([^{}]+)}_SUBCURRENCY/$1/g;
  } else {
    s/(?<=[ \t\.,;:\?\!\(\)\[\]\/\-\"])($pre_num_currency\s*$multinum)$rctxt/\{$1}_CURRENCY /go;
    s/(?<=[ \t\.,;:\?\!\(\)\[\]\/\-\"])($num\s*(?:$post_num_currency_with_dot|$post_num_currency)\s*$num\s*$post_num_subcurrency_with_dot)$rctxt_after_dot/\{$1}_CURRENCY /go;
    s/(?<=[ \t\.,;:\?\!\(\)\[\]\/\-\"])($num\s*(?:$post_num_currency_with_dot|$post_num_currency)\s*$num\s*$post_num_subcurrency)$rctxt/\{$1}_CURRENCY /go;
    s/(?<=[ \t\.,;:\?\!\(\)\[\]\/\-\"])($num\s*(?:$post_num_currency_with_dot|$post_num_currency)\s*$num)$rctxt/\{$1}_CURRENCY /go;
    s/(?<=[ \t\.,;:\?\!\(\)\[\]\/\-\"])($multinum\s*$post_num_currency_with_dot)$rctxt_after_dot/\{$1}_CURRENCY /go;
    s/(?<=[ \t\.,;:\?\!\(\)\[\]\/\-\"])($multinum\s*$post_num_currency)$rctxt/\{$1}_CURRENCY /go;
    s/{([^{}]*?) *{([^{}]+)}_CURRENCY ([^{}]*)}_CURRENCY/{$1$2$3}_CURRENCY/g;
  }

  if ($lang eq "fr") {
    s/(?<=[ \t\.,;:\?\!\(\)\[\]\/\-\"])de (\d+) (à {[^{}]+}_CURRENCY)/de {$1}_CURRENCY $2/g;
  }

  $ne = qr/_CURRENCY/o;
  if ($no_sw) {
    s/($ne)([^\s\}])/$1 _REGLUE_$2/g;
    #    print STDERR "1: $_\n\n";
    s/(^|\s)([^\s\{]+){([^{}]*)}($ne)/$1\{$2$3\} $2 _UNSPLIT_$4/g;
    if (s/{_REGLUE_.*?}//) {
      print STDERR "Warning: gl_currency encoutered an unexpected situation (input data is approx: $_)\n";
    }
    #    print STDERR "2: $_\n\n";
  } else {
    s/_REGLUE_/_PROTECTREGLUE_/g;
    s/($ne)([^\s_])/$1 $2/g;
    s/(^|\s)([^\s\{]+){([^{}]*)}($ne)/$1$2 \{$3\}$4/g;
    s/(?<=[^} ]) _REGLUE_//g;
    s/_PROTECTREGLUE_/_REGLUE_/g;
  }
  
  s/^(.*_META_TEXTUAL_[^ ]+)/\1 _UNSPLIT__SENT_BOUND/g;
  s/({[^}]*(?:{[^}]*(?:{[^}]*(?:{[^}]*}[^}]*)*}[^}]*)*}[^}]*)*})\s*{[^}]*}\s*($ne) _REGLUE_/$1$2 _UNSPLIT_/g;
  s/({[^}]*(?:{[^}]*(?:{[^}]*(?:{[^}]*}[^}]*)*}[^}]*)*}[^}]*)*})\s*{[^}]*}\s*($ne)/$1$2/g;
  
  s/^ +//;
  s/ +$//;
  print "$_\n";
}
