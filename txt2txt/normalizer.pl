#!/usr/bin/env perl

use utf8;

$lang = "fr";
$normalize = 1;

while (1) {
  $_=shift;
  if (/^$/) {last;}
  elsif (/^-l$/) {$lang=shift;}
  elsif (/^-no_n$/) {$normalize = 0;}
  elsif (/^-s$/) {$normalize = 0;}
#  else {die "unknown option : $_"}
}

if ($normalize == 0) {
  binmode STDIN, ":encoding(iso-8859-1)";
  binmode STDERR, ":encoding(iso-8859-1)";
  binmode STDOUT, ":encoding(iso-8859-1)";
} else {
  binmode STDIN, ":utf8";
  binmode STDERR, ":utf8";
  binmode STDOUT, ":utf8";
}

my $wordsep = qr/^[,\.;:\/ \t]$/;
%normalization_word_ending = ();
%normalization_word_beginning = ();
%normalization = ();
%error = ();

if ($lang =~ /^(ar|ckb|fa)$/) {
  $wordsep = qr/[ ٫،.؛؟\t-]/o;
}

if ($lang =~ /^(fa|ckb)$/) {
  $normalization_word_ending{"ئ"} = "ی";
  $normalization_word_ending{"ۀ"} = "هی";
  $normalization_word_ending{"أ"} = "ا";
  $normalization{"ك"} = "ک";	# Arabic Kaf -> Persian Keheh
  $normalization{"ى"} = "ی";	# Arabic Alif Maksura -> Persian Yeh
  $normalization{"ي"} = "ی";	# Arabic Yeh -> Persian Yeh
  unless ($lang eq "ckb") {
    $normalization{"ء"} = ""; # pas en sorani (existe en finale uniquement)
  }
  #$normalization{"ؤ"} = "و";
  #$normalization{"إ"} = "ا";
  $normalization{"ة"} = "ه";
  $normalization{"ـ"} = "";
  $normalization{"ٱ"} = "ا";
  $normalization{"ۇ"} = "و";
  $normalization{"ە"} = "ه";
  $error{"ۀ"} = "ه";
}

if ($lang eq "ar") {
  $normalization{"ک"} = "ك";	# Persian Keheh -> Arabic Kaf
  $normalization{"ی"} = "ى";	# Persian Yeh -> Arabic Alif Maksura
  $normalization{"ﷰ"} = "صلے";
  $normalization{"ﷱ"} = "قلے";
  $normalization{"ﷲ"} = "الله";
  $normalization{"ﷳ"} = "اكبر";
  $normalization{"ﷴ"} = "محمد";
  $normalization{"ﷵ"} = "صلعم";
  $normalization{"ﷶ"} = "رسول";
  $normalization{"ﷷ"} = "عليه";
  $normalization{"ﷸ"} = "وسلم";
  $normalization{"ﷹ"} = "صلى";
  $normalization{"ﷺ"} = "صلى الله عليه وسلم";
  $normalization{"ﷻ"} = "جل جلاله";
  $normalization{"﷼"} = "ريال";
  $normalization{"﷽"} = "بسم الله الرحمن الرحيم";
}

if ($lang =~ /^(ar|fa|ckb)$/) {
  $normalization{"َ"} = ""; # fatḥah (short a)
  $normalization{"ِ"} = ""; # kasrah (short i)
  $normalization{"ُ"} = ""; # ḍammah (short u)
  $normalization{"ٰ"} = ""; # dagger ʾalif (normally non-written long a)
  $normalization{"ْ"} = ""; # sukūn
  $normalization{"ٌ"} = ""; # tanwin -an
  $normalization{"ٍ"} = ""; # tanwin -in
  $normalization{"ً"} = ""; # # tanwin -un
  $normalization{"ّ"} = ""; # shadda/tashdid
  $normalization{"\x{FE84}"} = "\x{0623}";
  $normalization{"\x{FE83}"} = "\x{0623}";
  $normalization{"\x{FE91}"} = "\x{0628}";
  $normalization{"\x{FE92}"} = "\x{0628}";
  $normalization{"\x{FE90}"} = "\x{0628}";
  $normalization{"\x{FE8F}"} = "\x{0628}";
  $normalization{"\x{FE97}"} = "\x{062A}";
  $normalization{"\x{FE98}"} = "\x{062A}";
  $normalization{"\x{FE96}"} = "\x{062A}";
  $normalization{"\x{FE95}"} = "\x{062A}";
  $normalization{"\x{FE9B}"} = "\x{062B}";
  $normalization{"\x{FE9C}"} = "\x{062B}";
  $normalization{"\x{FE9A}"} = "\x{062B}";
  $normalization{"\x{FE99}"} = "\x{062B}";
  $normalization{"\x{FE9F}"} = "\x{062C}";
  $normalization{"\x{FEA0}"} = "\x{062C}";
  $normalization{"\x{FE9E}"} = "\x{062C}";
  $normalization{"\x{FE9D}"} = "\x{062C}";
  $normalization{"\x{FEA3}"} = "\x{062D}";
  $normalization{"\x{FEA4}"} = "\x{062D}";
  $normalization{"\x{FEA2}"} = "\x{062D}";
  $normalization{"\x{FEA1}"} = "\x{062D}";
  $normalization{"\x{FEA7}"} = "\x{062E}";
  $normalization{"\x{FEA8}"} = "\x{062E}";
  $normalization{"\x{FEA6}"} = "\x{062E}";
  $normalization{"\x{FEA5}"} = "\x{062E}";
  $normalization{"\x{FEAA}"} = "\x{062F}";
  $normalization{"\x{FEA9}"} = "\x{062F}";
  $normalization{"\x{FEAC}"} = "\x{0630}";
  $normalization{"\x{FEAB}"} = "\x{0630}";
  $normalization{"\x{FEAE}"} = "\x{0631}";
  $normalization{"\x{FEAD}"} = "\x{0631}";
  $normalization{"\x{FEB0}"} = "\x{0632}";
  $normalization{"\x{FEAF}"} = "\x{0632}";
  $normalization{"\x{FEB3}"} = "\x{0633}";
  $normalization{"\x{FEB4}"} = "\x{0633}";
  $normalization{"\x{FEB2}"} = "\x{0633}";
  $normalization{"\x{FEB1}"} = "\x{0633}";
  $normalization{"\x{FEB7}"} = "\x{0634}";
  $normalization{"\x{FEB8}"} = "\x{0634}";
  $normalization{"\x{FEB6}"} = "\x{0634}";
  $normalization{"\x{FEB5}"} = "\x{0634}";
  $normalization{"\x{FEBB}"} = "\x{0635}";
  $normalization{"\x{FEBC}"} = "\x{0635}";
  $normalization{"\x{FEBA}"} = "\x{0635}";
  $normalization{"\x{FEB9}"} = "\x{0635}";
  $normalization{"\x{FEBF}"} = "\x{0636}";
  $normalization{"\x{FEC0}"} = "\x{0636}";
  $normalization{"\x{FEBE}"} = "\x{0636}";
  $normalization{"\x{FEBD}"} = "\x{0636}";
  $normalization{"\x{FEC3}"} = "\x{0637}";
  $normalization{"\x{FEC4}"} = "\x{0637}";
  $normalization{"\x{FEC2}"} = "\x{0637}";
  $normalization{"\x{FEC1}"} = "\x{0637}";
  $normalization{"\x{FEC7}"} = "\x{0638}";
  $normalization{"\x{FEC8}"} = "\x{0638}";
  $normalization{"\x{FEC6}"} = "\x{0638}";
  $normalization{"\x{FEC5}"} = "\x{0638}";
  $normalization{"\x{FECB}"} = "\x{0639}";
  $normalization{"\x{FECC}"} = "\x{0639}";
  $normalization{"\x{FECA}"} = "\x{0639}";
  $normalization{"\x{FEC9}"} = "\x{0639}";
  $normalization{"\x{FECF}"} = "\x{063A}";
  $normalization{"\x{FED0}"} = "\x{063A}";
  $normalization{"\x{FECE}"} = "\x{063A}";
  $normalization{"\x{FECD}"} = "\x{063A}";
  $normalization{"\x{FED3}"} = "\x{0641}";
  $normalization{"\x{FED4}"} = "\x{0641}";
  $normalization{"\x{FED2}"} = "\x{0641}";
  $normalization{"\x{FED1}"} = "\x{0641}";
  $normalization{"\x{FED7}"} = "\x{0642}";
  $normalization{"\x{FED8}"} = "\x{0642}";
  $normalization{"\x{FED6}"} = "\x{0642}";
  $normalization{"\x{FED5}"} = "\x{0642}";
  $normalization{"\x{FEDB}"} = "\x{0643}";
  $normalization{"\x{FEDC}"} = "\x{0643}";
  $normalization{"\x{FEDA}"} = "\x{0643}";
  $normalization{"\x{FED9}"} = "\x{0643}";
  $normalization{"\x{FEDF}"} = "\x{0644}";
  $normalization{"\x{FEE0}"} = "\x{0644}";
  $normalization{"\x{FEDE}"} = "\x{0644}";
  $normalization{"\x{FEDD}"} = "\x{0644}";
  $normalization{"\x{FEE3}"} = "\x{0645}";
  $normalization{"\x{FEE4}"} = "\x{0645}";
  $normalization{"\x{FEE2}"} = "\x{0645}";
  $normalization{"\x{FEE1}"} = "\x{0645}";
  $normalization{"\x{FEE7}"} = "\x{0646}";
  $normalization{"\x{FEE8}"} = "\x{0646}";
  $normalization{"\x{FEE6}"} = "\x{0646}";
  $normalization{"\x{FEE5}"} = "\x{0646}";
  $normalization{"\x{FEEB}"} = "\x{0647}";
  $normalization{"\x{FEEC}"} = "\x{0647}";
  $normalization{"\x{FEEA}"} = "\x{0647}";
  $normalization{"\x{FEE9}"} = "\x{0647}";
  $normalization{"\x{FEEE}"} = "\x{0648}";
  $normalization{"\x{FEED}"} = "\x{0648}";
  $normalization{"\x{FEF3}"} = "\x{064A}";
  $normalization{"\x{FEF4}"} = "\x{064A}";
  $normalization{"\x{FEF2}"} = "\x{064A}";
  $normalization{"\x{FEF1}"} = "\x{064A}";
  $normalization{"\x{FE82}"} = "\x{0622}";
  $normalization{"\x{FE81}"} = "\x{0622}";
  $normalization{"\x{FE94}"} = "\x{0629}";
  $normalization{"\x{FE93}"} = "\x{0629}";
  $normalization{"\x{FEF0}"} = "\x{0649}";
  $normalization{"\x{FEEF}"} = "\x{0649}";
}

if ($lang eq "ckb") {
  $normalization_word_beginning{"ا"} = "ئا";
  $normalization_word_beginning{"ۆ"} = "ئۆ";
  $normalization_word_beginning{"ه"} = "ئه";
  $normalization_word_beginning{"ێ"} = "ئێ";
}


if ($lang eq "fa") {
  $normalization{"ھ"} = "ه";	# Urdu/Sorani cašmī heh -> Arabic/Persian Heh
  $normalization{"ء"} = "";      # En sorani, existe en finale uniquement (non vérifié pour l'instant par ce script)
}


if ($lang eq "ja") {
  $normalization{"かﾞ"} = "が";
  $normalization{"ｶﾞ"} = "ガ";
  $normalization{"さﾞ"} = "ざ";
  $normalization{"ｻﾞ"} = "ザ";
  $normalization{"たﾞ"} = "だ";
  $normalization{"ﾀﾞ"} = "ダ";
  $normalization{"はﾞ"} = "ば";
  $normalization{"ﾊﾞ"} = "バ";
  $normalization{"きﾞ"} = "ぎ";
  $normalization{"ｷﾞ"} = "ギ";
  $normalization{"しﾞ"} = "じ";
  $normalization{"ｼﾞ"} = "ジ";
  $normalization{"ちﾞ"} = "ぢ";
  $normalization{"ﾁﾞ"} = "ヂ";
  $normalization{"ひﾞ"} = "び";
  $normalization{"ﾋﾞ"} = "ビ";
  $normalization{"くﾞ"} = "ぐ";
  $normalization{"ｸﾞ"} = "グ";
  $normalization{"すﾞ"} = "ず";
  $normalization{"ｽﾞ"} = "ズ";
  $normalization{"つﾞ"} = "づ";
  $normalization{"ﾂﾞ"} = "ヅ";
  $normalization{"ふﾞ"} = "ぶ";
  $normalization{"ﾌﾞ"} = "ブ";
  $normalization{"けﾞ"} = "げ";
  $normalization{"ｹﾞ"} = "ゲ";
  $normalization{"せﾞ"} = "ぜ";
  $normalization{"ｾﾞ"} = "ゼ";
  $normalization{"てﾞ"} = "で";
  $normalization{"ﾃﾞ"} = "デ";
  $normalization{"へﾞ"} = "べ";
  $normalization{"ﾍﾞ"} = "ベ";
  $normalization{"こﾞ"} = "ご";
  $normalization{"ｺﾞ"} = "ゴ";
  $normalization{"そﾞ"} = "ぞ";
  $normalization{"ｿﾞ"} = "ゾ";
  $normalization{"とﾞ"} = "ど";
  $normalization{"ﾄﾞ"} = "ド";
  $normalization{"ほﾞ"} = "ぼ";
  $normalization{"ﾎﾞ"} = "ボ";
  $normalization{"はﾟ"} = "ぱ";
  $normalization{"ﾊﾟ"} = "パ";
  $normalization{"ひﾟ"} = "ぴ";
  $normalization{"ﾋﾟ"} = "ピ";
  $normalization{"ふﾟ"} = "ぷ";
  $normalization{"ﾌﾟ"} = "プ";
  $normalization{"へﾟ"} = "ぺ";
  $normalization{"ﾍﾟ"} = "ペ";
  $normalization{"ほﾟ"} = "ぽ";
  $normalization{"ﾎﾟ"} = "ポ";
  $normalization{" "} = "　";
  $normalization{"｡"} = "。";
  $normalization{"｢"} = "「";
  $normalization{"｣"} = "」";
  $normalization{"､"} = "、";
  $normalization{"･"} = "・";
  $normalization{"ｦ"} = "ヲ";
  $normalization{"ｧ"} = "ァ";
  $normalization{"ｨ"} = "ィ";
  $normalization{"ｩ"} = "ゥ";
  $normalization{"ｪ"} = "ェ";
  $normalization{"ｫ"} = "ォ";
  $normalization{"ｬ"} = "ャ";
  $normalization{"ｭ"} = "ュ";
  $normalization{"ｮ"} = "ョ";
  $normalization{"ｯ"} = "ッ";
  $normalization{"ｰ"} = "ー";
  $normalization{"ｱ"} = "ア";
  $normalization{"ｲ"} = "イ";
  $normalization{"ｳ"} = "ウ";
  $normalization{"ｴ"} = "エ";
  $normalization{"ｵ"} = "オ";
  $normalization{"ｶ"} = "カ";
  $normalization{"ｷ"} = "キ";
  $normalization{"ｸ"} = "ク";
  $normalization{"ｹ"} = "ケ";
  $normalization{"ｺ"} = "コ";
  $normalization{"ｻ"} = "サ";
  $normalization{"ｼ"} = "シ";
  $normalization{"ｽ"} = "ス";
  $normalization{"ｾ"} = "セ";
  $normalization{"ｿ"} = "ソ";
  $normalization{"ﾀ"} = "タ";
  $normalization{"ﾁ"} = "チ";
  $normalization{"ﾂ"} = "ツ";
  $normalization{"ﾃ"} = "テ";
  $normalization{"ﾄ"} = "ト";
  $normalization{"ﾅ"} = "ナ";
  $normalization{"ﾆ"} = "ニ";
  $normalization{"ﾇ"} = "ヌ";
  $normalization{"ﾈ"} = "ネ";
  $normalization{"ﾉ"} = "ノ";
  $normalization{"ﾊ"} = "ハ";
  $normalization{"ﾋ"} = "ヒ";
  $normalization{"ﾌ"} = "フ";
  $normalization{"ﾍ"} = "ヘ";
  $normalization{"ﾎ"} = "ホ";
  $normalization{"ﾏ"} = "マ";
  $normalization{"ﾐ"} = "ミ";
  $normalization{"ﾑ"} = "ム";
  $normalization{"ﾒ"} = "メ";
  $normalization{"ﾓ"} = "モ";
  $normalization{"ﾔ"} = "ヤ";
  $normalization{"ﾕ"} = "ユ";
  $normalization{"ﾖ"} = "ヨ";
  $normalization{"ﾗ"} = "ラ";
  $normalization{"ﾘ"} = "リ";
  $normalization{"ﾙ"} = "ル";
  $normalization{"ﾚ"} = "レ";
  $normalization{"ﾛ"} = "ロ";
  $normalization{"ﾜ"} = "ワ";
  $normalization{"ﾝ"} = "ン";
}


while (<>) {
  chomp;
  if (/ (_XML|_MS_ANNOTATION) *$/) {
    print "$_\n";
    next;
  } elsif ($lang =~ /^(fa|ckb|ar|ja)$/ && $normalize) {
    s/\r//;
    $word = "";
    while (s/^(.)(.*?(?:$wordsep)?|$)/\2/) {
      $c = $1;
      $rest = $2;
      if ($lang =~ /^(fa|ckb|ar|ja)$/ && ($rest eq "" || $rest =~ /^$wordsep/) && defined($normalization_word_ending{$c})) {
	$word .= $normalization_word_ending{$c};
      } elsif ($lang =~ /^(fa|ckb|ar|ja)$/ && $word eq "" && defined($normalization_word_beginning{$c})) {
	$word .= $normalization_word_beginning{$c};
      } elsif (defined($normalization{$c})) {
	$word .= $normalization{$c};
      } elsif ($lang =~ /^(fa|ckb|ar|ja)$/ && defined($error{$c})) {
	print STDERR "### WARNING: erroneous character '$c' found in word $word$rest - replaced by '$error{$c}'\n";
	$word .= $error{$c};
      } elsif ($c =~ /^$wordsep$/) {
	print "$word$c";
	$word = "";
      } else {
	$word .= "$c";
      }
    }
    print "$word\n";
  } else {
    print "$_\n";
  }
}
