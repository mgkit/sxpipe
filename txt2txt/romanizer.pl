#!/usr/bin/env perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$type = "";
$lang = "fa";
$col = -1;
$translitterate = -1;
$ilex = 0;
$type = "u";

while (1) {
  $_=shift;
  if (/^$/) {last;}
  elsif (/^-1$/) {$col=1;}
  elsif (/^-ilex$/) {$col=1; $ilex = 1;}
  elsif (/^-(2|lex)$/) {$col=2;}
  elsif (/^-(la|al|ua|au)$/) {$type=$1;}
  elsif (/^-([au])$/) {$type=$1;}
  elsif (/^-no_tr$/) {$translitterate = 0;}
  elsif (/^-tr$/) {$translitterate = 1;}
  elsif (/^-l$/) {$lang=shift;}
#  else {die "unknown option : $_"}
}

if ($translitterate == -1) {
  if ($lang eq "ru" || $lang eq "ar" || $lang eq "fa") {
    $translitterate = 0;
  } else {
    $translitterate = 1;
  }
}

# Translitteration changes w.r.t. previous versions (+added sorani letters)
# ḣ -> ħ / ł
# v -> w
# ż -> đ
# New translitteration changes proposed:
# e -> ˝
# h -> e
# ġ -> h (moreover, this removes the pb that no corresponding latin-2 character was available, which forced us to use the ugly '¤')
#،
%arabic_to_roman = (
		    "–" => { unicode => "–", latin2 => "–"},
		    "-" => { unicode => "-", latin2 => "-"},
		    "." => { unicode => ".", latin2 => "."},
		    "،" => { unicode => ",", latin2 => ","},
		    "؛" => { unicode => ";", latin2 => ";"},
		    "؟" => { unicode => "?", latin2 => "?"},
		    "ء" => { unicode => "°", latin2 => "°"}, # à supprimer tout court
		    "آ" => { unicode => "ā", latin2 => "â"},  ### diff l2 u8
		    "أ" => { unicode => "á", latin2 => "á"}, # mauvais, à remplacer par ا
		    "ؤ" => { unicode => "ú", latin2 => "ú"}, # mauvais, à remplacer par و
		    "إ" => { unicode => "E", latin2 => "E"}, # mauvais, à remplacer par ا
		    "ئ" => { unicode => "´", latin2 => "´"}, # mauvais en fin de mot, à supprimer
		    "ا" => { unicode => "a", latin2 => "a"},
		    "ب" => { unicode => "b", latin2 => "b"},
		    "ة" => { unicode => "T", latin2 => "T"}, # mauvais, à remplacer par ه 
		    "ت" => { unicode => "t", latin2 => "t"},
		    "ث" => { unicode => "ç", latin2 => "ç"},
		    "ج" => { unicode => "j", latin2 => "j"},
		    "ح" => { unicode => "ħ", latin2 => "ł"},  ### diff l2 u8
		    "خ" => { unicode => "x", latin2 => "x"},
		    "د" => { unicode => "d", latin2 => "d"},
		    "ذ" => { unicode => "đ", latin2 => "đ"},
		    "ر" => { unicode => "r", latin2 => "r"},
		    "ڕ" => { unicode => "ř", latin2 => "ř"}, # sorani
		    "ز" => { unicode => "z", latin2 => "z"},
		    "س" => { unicode => "s", latin2 => "s"},
		    "ش" => { unicode => "š", latin2 => "š"},
		    "ص" => { unicode => "ş", latin2 => "ş"},
		    "ض" => { unicode => "ź", latin2 => "ź"},
		    "ط" => { unicode => "ţ", latin2 => "ţ"},
		    "ظ" => { unicode => "ẓ", latin2 => "ż"},  ### diff l2 u8
		    "ع" => { unicode => "'", latin2 => "'"},
		    "غ" => { unicode => "q", latin2 => "q"},
		    "ـ" => { unicode => "=", latin2 => "="}, # à supprimer
		    "ف" => { unicode => "f", latin2 => "f"},
		    "ڤ" => { unicode => "v", latin2 => "v"}, # sorani
		    "ق" => { unicode => "ŕ", latin2 => "ŕ"},
		    "ك" => { unicode => "K", latin2 => "K"}, # à remplacer par k
		    "ل" => { unicode => "l", latin2 => "l"},
		    "ڵ" => { unicode => "ľ", latin2 => "ľ"}, # sorani
		    "م" => { unicode => "m", latin2 => "m"},
		    "ن" => { unicode => "n", latin2 => "n"},
		    "ه" => { unicode => "e", latin2 => "e"},
		    "و" => { unicode => "w", latin2 => "w"},
		    "ۆ" => { unicode => "o", latin2 => "o"},
		    "ى" => { unicode => "Ý", latin2 => "Ý"}, #y
		    "ي" => { unicode => "Y", latin2 => "Y"}, #y
		    "ً" => { unicode => "˝", latin2 => "˝"},
		    "ٔ" => { unicode => "`", latin2 => "`"}, # à remplacer par un ی en fin de mot
		    "٫" => { unicode => "⎖", latin2 => "˛"},
		    "ٱ" => { unicode => "Á", latin2 => "Á"}, # à remplacer par ا
		    "پ" => { unicode => "p", latin2 => "p"},
		    "چ" => { unicode => "č", latin2 => "č"},
		    "ژ" => { unicode => "ž", latin2 => "ž"},
		    "ک" => { unicode => "k", latin2 => "k"},
		    "گ" => { unicode => "g", latin2 => "g"},
		    "ھ" => { unicode => "h", latin2 => "h"}, # remplacer par ه
		    "ۀ" => { unicode => "X", latin2 => "X"}, # remplacer par هی en fin de mot (ailleurs = erreur)
		    "ۇ" => { unicode => "~", latin2 => "~"}, # remplacer par و
		    "ی" => { unicode => "y", latin2 => "y"},
		    "ێ" => { unicode => "ě", latin2 => "ě"},
		    "۰" => { unicode => "0", latin2 => "0"},
		    "۱" => { unicode => "1", latin2 => "1"},
		    "۲" => { unicode => "2", latin2 => "2"},
		    "۳" => { unicode => "3", latin2 => "3"},
		    "۴" => { unicode => "4", latin2 => "4"},
		    "۵" => { unicode => "5", latin2 => "5"},
		    "۶" => { unicode => "6", latin2 => "6"},
		    "۷" => { unicode => "7", latin2 => "7"},
		    "۸" => { unicode => "8", latin2 => "8"},
		    "۹" => { unicode => "9", latin2 => "9"},
		    "‌" => { unicode => "_", latin2 => "_"}
		   );

%roman_unicode_to_arabic = (
			    "–" => "–",
			    "-" => "-",
			    "." => ".",
			    "," => "،",
			    ";" => "؛",
			    "?" => "؟",
			    "°" => "ء",
			    "ā" => "آ",
			    "á" => "أ",
			    "ú" => "ؤ",
			    "E" => "إ",
			    "´" => "ئ",
			    "a" => "ا",
			    "b" => "ب",
			    "T" => "ة",
			    "t" => "ت",
			    "ç" => "ث",
			    "j" => "ج",
			    "ħ" => "ح",
			    "x" => "خ",
			    "d" => "د",
			    "đ" => "ذ",
			    "r" => "ر",
			    "ř" => "ڕ",
			    "z" => "ز",
			    "s" => "س",
			    "š" => "ش",
			    "ş" => "ص",
			    "ź" => "ض",
			    "ţ" => "ط",
			    "ẓ" => "ظ",
			    "'" => "ع",
			    "q" => "غ",
			    "=" => "ـ",
			    "f" => "ف",
			    "v" => "ڤ",
			    "ŕ" => "ق",
			    "K" => "ك",
			    "l" => "ل",
			    "ľ" => "ڵ",
			    "m" => "م",
			    "n" => "ن",
			    "e" => "ه",
			    "w" => "و",
			    "Ý" => "ى",
			    "Y" => "ي",
			    "˝" => "ً",
			    "`" => "ٔ",
			    "⎖" => "٫",
			    "Á" => "ٱ",
			    "p" => "پ",
			    "č" => "چ",
			    "ž" => "ژ",
			    "k" => "ک",
			    "g" => "گ",
			    "h" => "ھ",
			    "X" => "ۀ",
			    "ě" => "ێ",
			    "o" => "ۆ",
			    "~" => "ۇ",
			    "y" => "ی",
			    "0" => "۰",
			    "1" => "۱",
			    "2" => "۲",
			    "3" => "۳",
			    "4" => "۴",
			    "5" => "۵",
			    "6" => "۶",
			    "7" => "۷",
			    "8" => "۸",
			    "9" => "۹",
			    "_" => "‌",
			   );

%roman_latin2_to_arabic = (
			   "–" => "–",
			   "-" => "-",
			   "." => ".",
			   "," => "،",
			   ";" => "؛",
			   "?" => "؟",
			   "°" => "ء",
			   "â" => "آ",
			   "á" => "أ",
			   "ú" => "ؤ",
			   "E" => "إ",
			   "´" => "ئ",
			   "a" => "ا",
			   "b" => "ب",
			   "T" => "ة",
			   "t" => "ت",
			   "ç" => "ث",
			   "j" => "ج",
			   "ł" => "ح",
			   "x" => "خ",
			   "d" => "د",
			   "đ" => "ذ",
			   "r" => "ر",
			   "ř" => "ڕ",
			   "z" => "ز",
			   "s" => "س",
			   "š" => "ش",
			   "ş" => "ص",
			   "ź" => "ض",
			   "ţ" => "ط",
			   "ż" => "ظ",
			   "'" => "ع",
			   "q" => "غ",
			   "=" => "ـ",
			   "f" => "ف",
			   "v" => "ڤ",
			   "ŕ" => "ق",
			   "K" => "ك",
			   "l" => "ل",
			   "ľ" => "ڵ",
			   "m" => "م",
			   "n" => "ن",
			   "e" => "ه",
			   "w" => "و",
			   "Ý" => "ى",
			   "Y" => "ي",
			   "˝" => "ً",
			   "`" => "ٔ",
			   "˛" => "٫",
			   "Á" => "ٱ",
			   "p" => "پ",
			   "č" => "چ",
			   "ž" => "ژ",
			   "k" => "ک",
			   "g" => "گ",
			   "h" => "ھ",
			   "X" => "ۀ",
			   "ě" => "ێ",
			   "o" => "ۆ",
			   "~" => "ۇ",
			   "y" => "ی",
			   "0" => "۰",
			   "1" => "۱",
			   "2" => "۲",
			   "3" => "۳",
			   "4" => "۴",
			   "5" => "۵",
			   "6" => "۶",
			   "7" => "۷",
			   "8" => "۸",
			   "9" => "۹",
			   "_" => "‌",
			  );


%cyrillic_to_roman = (
		      "А" => "A",
		      "а" => "a",
		      "Б" => "B",
		      "б" => "b",
		      "В" => "V",
		      "в" => "v",
		      "Г" => "G",
		      "г" => "g",
		      "Ґ" => "Ġ",
		      "ґ" => "ġ",
		      "Д" => "D",
		      "д" => "d",
		      "Ѓ" => "Ǵ",
		      "ѓ" => "ǵ",
		      "Ђ" => "Ď",
		      "ђ" => "ď",
		      "Е" => "E",
		      "е" => "e",
		      "Ё" => "Ë",
		      "ё" => "ë",
		      "Є" => "Ě",
		      "є" => "ě",
		      "Ж" => "Ž",
		      "ж" => "ž",
		      "З" => "Z",
		      "з" => "z",
		      "Ѕ" => "DZ",
		      "ѕ" => "dz",
		      "И" => "I",
		      "и" => "i",
		      "I" => "Ì",
		      "і" => "ì",
		      "Ї" => "Ï",
		      "ї" => "ï",
		      "Й" => "J",
		      "й" => "j",
		      "Ј" => "Ĵ",
		      "ј" => "ĵ",
		      "К" => "K",
		      "к" => "k",
		      "Л" => "L",
		      "л" => "l",
		      "Љ" => "Ľ",
		      "љ" => "ľ",
		      "М" => "M",
		      "м" => "m",
		      "Н" => "N",
		      "н" => "n",
		      "Њ" => "Ň",
		      "њ" => "ň",
		      "О" => "O",
		      "о" => "o",
		      "П" => "P",
		      "п" => "p",
		      "Р" => "R",
		      "р" => "r",
		      "С" => "S",
		      "с" => "s",
		      "Т" => "T",
		      "т" => "t",
		      "Ќ" => "Ḱ",
		      "ќ" => "ḱ",
		      "Ћ" => "Ć",
		      "ћ" => "ć",
		      "У" => "U",
		      "у" => "u",
		      "Ў" => "W",
		      "ў" => "w",
		      "Ф" => "F",
		      "ф" => "f",
		      "Х" => "X",
		      "х" => "x",
		      "Ц" => "C",
		      "ц" => "c",
		      "Ч" => "Č",
		      "ч" => "č",
		      "Џ" => "DŽ",
		      "џ" => "dž",
		      "Ш" => "Š",
		      "ш" => "š",
		      "Щ" => "ŠČ",
		      "щ" => "šč",
		      "Ъ" => "Ŭ",
		      "ъ" => "ŭ",
		      "Ы" => "Y",
		      "ы" => "y",
		      "Ь" => "Ĭ",
		      "ь" => "ĭ",
		      "Ѣ" => "Ä",
		      "ѣ" => "ä",
		      "Э" => "È",
		      "э" => "è",
		      "Ю" => "Ǔ",
		      "ю" => "ǔ",
		      "Я" => "Ǎ",
		      "я" => "ǎ",
		      "’" => "'",
		      "Ѡ" => "Ô",
		      "ѡ" => "ô",
		      "Ѧ" => "Ę",
		      "ѧ" => "ę",
		      "Ѩ" => "JĘ",
		      "ѩ" => "ję",
		      "Ѫ" => "Ǫ",
		      "ѫ" => "ǫ",
		      "Ѭ" => "JǪ",
		      "ѭ" => "jǫ",
		      "Ѯ" => "Kˢ",
		      "ѯ" => "kˢ",
		      "Ѱ" => "Pˢ",
		      "ѱ" => "pˢ",
		      "Ѳ" => "Tʰ",
		      "ѳ" => "tʰ",
		      "Ѵ" => "Ü",
		      "ѵ" => "ü",
		     );

for (keys %cyrillic_to_roman) {
  if (defined($roman_to_cyrillic{$cyrillic_to_roman{$_}})) {
    print STDERR "### WARNING: '$cyrillic_to_roman{$_}' translitterates at least $roman_to_cyrillic{$cyrillic_to_roman{$_}} and $_\n";
  } else {
    $roman_to_cyrillic{$cyrillic_to_roman{$_}} = $_;      
  }
  print STDERR "### WARNING: '$_' (cyrillic) has more than one unicode characters\n" if length > 1;
  if (length($cyrillic_to_roman{$_}) > 1) {
    push @{$cmpnd_translitts{"cyrillic"}}, $cyrillic_to_roman{$_};
  }
}
push @{$cmpnd_translitts{"cyrillic"}}, ".";
$cmpnd_translitts_re{"cyrillic"} = join ("|", sort {length($b) <=> length($a)} @{$cmpnd_translitts{"cyrillic"}});

%cyrillic_to_enroman = (
		      "А" => "A",
		      "а" => "a",
		      "Б" => "B",
		      "б" => "b",
		      "В" => "V",
		      "в" => "v",
		      "Г" => "G",
		      "г" => "g",
		      "Д" => "D",
		      "д" => "d",
		      "Ђ" => "DJ",
		      "ђ" => "dj",
		      "Е" => "E",
		      "е" => "e",
		      "Ё" => "Ë",
		      "ё" => "ë",
		      "Є" => "JE",
		      "є" => "je",
		      "Ж" => "ZH",
		      "ж" => "zh",
		      "З" => "Z",
		      "з" => "z",
		      "Ѕ" => "DZ",
		      "ѕ" => "dz",
		      "И" => "I",
		      "и" => "i",
		      "Й" => "J",
		      "й" => "j",
		      "К" => "K",
		      "к" => "k",
		      "Л" => "L",
		      "л" => "l",
		      "Љ" => "LJ",
		      "љ" => "lj",
		      "М" => "M",
		      "м" => "m",
		      "Н" => "N",
		      "н" => "n",
		      "Њ" => "NJ",
		      "њ" => "nj",
		      "О" => "O",
		      "о" => "o",
		      "П" => "P",
		      "п" => "p",
		      "Р" => "R",
		      "р" => "r",
		      "С" => "S",
		      "с" => "s",
		      "Т" => "T",
		      "т" => "t",
		      "У" => "U",
		      "у" => "u",
		      "Ф" => "F",
		      "ф" => "f",
		      "Х" => "H",
		      "х" => "h",
		      "Ц" => "TS",
		      "ц" => "ts",
		      "Ч" => "CH",
		      "ч" => "ch",
		      "Џ" => "DZH",
		      "џ" => "dzh",
		      "Ш" => "SH",
		      "ш" => "sh",
		      "Щ" => "SHCH",
		      "щ" => "shch",
		      "Ъ" => "Ŭ",
		      "ъ" => "ŭ",
		      "Ы" => "Y",
		      "ы" => "y",
		      "Ь" => "Ĭ",
		      "ь" => "ĭ",
		      "Э" => "È",
		      "э" => "è",
		      "Ю" => "JU",
		      "ю" => "ju",
		      "Я" => "JA",
		      "я" => "ja",
		      "’" => "'",
		     );

for (keys %cyrillic_to_enroman) {
  if (defined($enroman_to_cyrillic{$cyrillic_to_enroman{$_}})) {
    print STDERR "### WARNING: '$cyrillic_to_enroman{$_}' translitterates at least $enroman_to_cyrillic{$cyrillic_to_enroman{$_}} and $_\n";
  } else {
    $enroman_to_cyrillic{$cyrillic_to_enroman{$_}} = $_;      
  }
  print STDERR "### WARNING: '$_' (encyrillic) has more than one unicode characters\n" if length > 1;
  if (length($cyrillic_to_enroman{$_}) > 1) {
    push @{$cmpnd_translitts{"encyrillic"}}, $cyrillic_to_enroman{$_};
  }
}
push @{$cmpnd_translitts{"encyrillic"}}, ".";
$cmpnd_translitts_re{"encyrillic"} = join ("|", sort {length($b) <=> length($a)} @{$cmpnd_translitts{"encyrillic"}});


## devanagari unicode section starts
$indic_to_roman{hi}{"ँ"} = "ṁ";# chandrabindu
$indic_to_roman{hi}{"ं"} = "ṃ"; # anuswar
$indic_to_roman{hi}{"ः"} = "ḥ"; # visarg

$indic_to_roman{hi}{"अ"} = "a";
$indic_to_roman{hi}{"आ"} = "ā";
$indic_to_roman{hi}{"इ"} = "i";
$indic_to_roman{hi}{"ई"} = "ī";
$indic_to_roman{hi}{"उ"} = "u";
$indic_to_roman{hi}{"ऊ"} = "ū";
$indic_to_roman{hi}{"ऋ"} = "ṛ";
$indic_to_roman{hi}{"ऌ"} = "ḽ"; # &#2316; letter vocalic l
$indic_to_roman{hi}{"ऍ"} = "ê";
$indic_to_roman{hi}{"ऎ"} = "e";
$indic_to_roman{hi}{"ए"} = "ē";
$indic_to_roman{hi}{"ऐ"} = "aʲ";
$indic_to_roman{hi}{"ऑ"} = "ô";
$indic_to_roman{hi}{"ऒ"} = "o";
$indic_to_roman{hi}{"ओ"} = "ō";
$indic_to_roman{hi}{"औ"} = "aʷ";

$indic_to_roman{hi}{"क"} = "kᵃ";
$indic_to_roman{hi}{"ख"} = "kʰᵃ";
$indic_to_roman{hi}{"ग"} = "gᵃ";
$indic_to_roman{hi}{"घ"} = "gʰᵃ";
$indic_to_roman{hi}{"ङ"} = "ṅᵃ";
$indic_to_roman{hi}{"च"} = "cᵃ";
$indic_to_roman{hi}{"छ"} = "cʰᵃ";
$indic_to_roman{hi}{"ज"} = "jᵃ";
$indic_to_roman{hi}{"ज़"} = "zᵃ"; # &#2395; letter za
$indic_to_roman{hi}{"झ"} = "jʰᵃ";
$indic_to_roman{hi}{"ञ"} = "ñᵃ";
$indic_to_roman{hi}{"ट"} = "ṭᵃ";
$indic_to_roman{hi}{"ठ"} = "ṭʰᵃ";
$indic_to_roman{hi}{"ड"} = "ḍᵃ";
$indic_to_roman{hi}{"ढ"} = "ḍʰᵃ";
$indic_to_roman{hi}{"ढ़"} = "ḓʰᵃ"; # &#2397; letter rha
$indic_to_roman{hi}{"ण"} = "ṇᵃ";
$indic_to_roman{hi}{"त"} = "tᵃ";
$indic_to_roman{hi}{"थ"} = "tʰᵃ";
$indic_to_roman{hi}{"द"} = "dᵃ";
$indic_to_roman{hi}{"ध"} = "dʰᵃ";
$indic_to_roman{hi}{"न"} = "nᵃ";
$indic_to_roman{hi}{"ऩ"} = "ṉᵃ";
$indic_to_roman{hi}{"प"} = "pᵃ";
$indic_to_roman{hi}{"फ"} = "pʰᵃ";
$indic_to_roman{hi}{"फ़"} = "fᵃ"; # &#2398; letter fa
$indic_to_roman{hi}{"ब"} = "bᵃ";
$indic_to_roman{hi}{"भ"} = "bʰᵃ";
$indic_to_roman{hi}{"म"} = "mᵃ";
$indic_to_roman{hi}{"य"} = "yᵃ";
$indic_to_roman{hi}{"य़"} = "ẏᵃ";
$indic_to_roman{hi}{"र"} = "rᵃ";
$indic_to_roman{hi}{"ऱ"} = "ṛᵃ";
$indic_to_roman{hi}{"ल"} = "lᵃ";
$indic_to_roman{hi}{"ळ"} = "ḷᵃ";
$indic_to_roman{hi}{"ऴ"} = "ẕᵃ";
$indic_to_roman{hi}{"व"} = "vᵃ";
$indic_to_roman{hi}{"श"} = "śᵃ";
$indic_to_roman{hi}{"ष"} = "ṣᵃ";
$indic_to_roman{hi}{"स"} = "sᵃ";
$indic_to_roman{hi}{"ह"} = "hᵃ";
$indic_to_roman{hi}{"क़"} = "qᵃ"; # &#2392 letter qa
$indic_to_roman{hi}{"ख़"} = "ḵᴴᵃ"; # &#2393 letter khha
$indic_to_roman{hi}{"ग़"} = "gᴴᵃ"; # &#2394 letter ghha
$indic_to_roman{hi}{"ड़"} = "ḓᵃ"; # &#2396; letter dddha

$indic_to_roman{hi}{"‍"} = ""; # ISCII INVisible -> Unicode ZWJ

$indic_to_roman{hi}{"ा"} = "ã";
$indic_to_roman{hi}{"ि"} = "ì";
$indic_to_roman{hi}{"ॢ"} = "Ḽ"; # &#2402; vowel sign vocalic l
$indic_to_roman{hi}{"ी"} = "ĩ";
$indic_to_roman{hi}{"ॣ"} = "Ḽ²"; # &#2403; vowel sign vocalic ll
$indic_to_roman{hi}{"ु"} = "ù";
$indic_to_roman{hi}{"ू"} = "ũ";
$indic_to_roman{hi}{"ृ"} = "Ṛ";
$indic_to_roman{hi}{"ॄ"} = "Ṛ²"; # &#2372; vowel sign vocalic rr
$indic_to_roman{hi}{"ॆ"} = "è";
$indic_to_roman{hi}{"े"} = "ẽ";
$indic_to_roman{hi}{"ै"} = "àʲ";
$indic_to_roman{hi}{"ॅ"} = "ễ";
$indic_to_roman{hi}{"ॊ"} = "ò";
$indic_to_roman{hi}{"ो"} = "õ";
$indic_to_roman{hi}{"ौ"} = "àʷ";
$indic_to_roman{hi}{"ॉ"} = "ỗ";
$indic_to_roman{hi}{"्"} = "`"; # halant

$indic_to_roman{hi}{"ॐ"} = "oᵐ"; # om

$indic_to_roman{hi}{"ॠ"} = "ṛ²"; # &#2400; letter vocalic rr
$indic_to_roman{hi}{"ॡ"} = "ḽ²"; # &#2401; letter vocalic ll
$indic_to_roman{hi}{"।"} = "."; # full stop / viram
$indic_to_roman{hi}{"॥"} = "|"; # &#2405; double danda (ARD)

$indic_to_roman{hi}{"़"} = "•"; # nukta
$indic_to_roman{hi}{"ऽ"} = "’"; # &#2365; sign avagrṛaha

$indic_to_roman{hi}{"॒"} = "–"; # &#2386; stress sign anudatta (uses EXT)
$indic_to_roman{hi}{"॰"} = "°"; # &#2416; abbreviation sign (uses EXT)
$indic_to_roman{hi}{"०"} = "0";
$indic_to_roman{hi}{"१"} = "1";
$indic_to_roman{hi}{"२"} = "2";
$indic_to_roman{hi}{"३"} = "3";
$indic_to_roman{hi}{"४"} = "4";
$indic_to_roman{hi}{"५"} = "5";
$indic_to_roman{hi}{"६"} = "6";
$indic_to_roman{hi}{"७"} = "7";
$indic_to_roman{hi}{"८"} = "8";
$indic_to_roman{hi}{"९"} = "9";


$indic_to_roman{ta}{"க"} = "kᵃ";
$indic_to_roman{ta}{"ங"} = "ṅᵃ";
$indic_to_roman{ta}{"ச"} = "cᵃ";
$indic_to_roman{ta}{"ஞ"} = "ñᵃ";
$indic_to_roman{ta}{"ட"} = "ṭᵃ";
$indic_to_roman{ta}{"ண"} = "ṇᵃ";
$indic_to_roman{ta}{"த"} = "tᵃ";
$indic_to_roman{ta}{"ந"} = "nᵃ";
$indic_to_roman{ta}{"ப"} = "pᵃ";
$indic_to_roman{ta}{"ம"} = "mᵃ";
$indic_to_roman{ta}{"ய"} = "yᵃ";
$indic_to_roman{ta}{"ர"} = "rᵃ";
$indic_to_roman{ta}{"ல"} = "lᵃ";
$indic_to_roman{ta}{"வ"} = "vᵃ";
$indic_to_roman{ta}{"ழ"} = "ḻᵃ";
$indic_to_roman{ta}{"ள"} = "ḷᵃ";
$indic_to_roman{ta}{"ற"} = "ṟᵃ";
$indic_to_roman{ta}{"ன"} = "ṉᵃ";
$indic_to_roman{ta}{"ஶ"} = "śᵃ";
$indic_to_roman{ta}{"ஜ"} = "jᵃ";
$indic_to_roman{ta}{"ஷ"} = "ṣᵃ";
$indic_to_roman{ta}{"ஸ"} = "sᵃ";
$indic_to_roman{ta}{"ஹ"} = "hᵃ";

$indic_to_roman{ta}{"அ"} = "a";
$indic_to_roman{ta}{"ஆ"} = "ā";
$indic_to_roman{ta}{"இ"} = "i";
$indic_to_roman{ta}{"ஈ"} = "ī";
$indic_to_roman{ta}{"உ"} = "u";
$indic_to_roman{ta}{"ஊ"} = "ū";
$indic_to_roman{ta}{"எ"} = "e";
$indic_to_roman{ta}{"ஏ"} = "ē";
$indic_to_roman{ta}{"ஐ"} = "aʲ";
$indic_to_roman{ta}{"ஒ"} = "o";
$indic_to_roman{ta}{"ஓ"} = "ō";
$indic_to_roman{ta}{"ஔ"} = "aʷ";
$indic_to_roman{ta}{"ெ"} = "à";
$indic_to_roman{ta}{"ா"} = "ã";
$indic_to_roman{ta}{"ி"} = "ì";
$indic_to_roman{ta}{"ீ"} = "ĩ";
$indic_to_roman{ta}{"ு"} = "ù";
$indic_to_roman{ta}{"ூ"} = "ũ";
$indic_to_roman{ta}{"ெ"} = "è";
$indic_to_roman{ta}{"ே"} = "ẽ";
$indic_to_roman{ta}{"ை"} = "àʲ";
$indic_to_roman{ta}{"ொ"} = "ò";
$indic_to_roman{ta}{"ோ"} = "õ";
$indic_to_roman{ta}{"ௌ"} = "àʷ";
$indic_to_roman{ta}{"்"} = "`";
$indic_to_roman{ta}{"஫"} = "_";
$indic_to_roman{ta}{"஠"} = "=";
$indic_to_roman{ta}{"஥"} = "≡";
$indic_to_roman{ta}{"஖"} = "≃";
$indic_to_roman{ta}{"஖"} = "≃";
$indic_to_roman{ta}{"௟"} = "≂";
$indic_to_roman{ta}{"஼"} = "≈";
$indic_to_roman{ta}{"஛"} = "≠";
$indic_to_roman{ta}{"௃"} = "≄";
$indic_to_roman{ta}{"஁"} = "≢";
$indic_to_roman{ta}{"஗"} = "≔";
#$indic_to_roman{ta}{"\x0BA6"} = "≕";

$indic_to_roman{si}{"ක"} = "kᵃ";
$indic_to_roman{si}{"ග"} = "gᵃ";
$indic_to_roman{si}{"ට"} = "ṭᵃ";
$indic_to_roman{si}{"ඩ"} = "ḍᵃ";
$indic_to_roman{si}{"ත"} = "tᵃ";
$indic_to_roman{si}{"ද"} = "dᵃ";
$indic_to_roman{si}{"ප"} = "pᵃ";
$indic_to_roman{si}{"බ"} = "bᵃ";
$indic_to_roman{si}{"ස"} = "sᵃ";
$indic_to_roman{si}{"හ"} = "hᵃ";
$indic_to_roman{si}{"ච"} = "cᵃ";
$indic_to_roman{si}{"ජ"} = "jᵃ";
$indic_to_roman{si}{"ම"} = "mᵃ";
$indic_to_roman{si}{"න"} = "nᵃ";
$indic_to_roman{si}{"ල"} = "lᵃ";
$indic_to_roman{si}{"ර"} = "rᵃ";
$indic_to_roman{si}{"ව"} = "vᵃ";
$indic_to_roman{si}{"ය"} = "yᵃ";
$indic_to_roman{si}{"ණ"} = "ṇᵃ";
$indic_to_roman{si}{"ළ"} = "ḷᵃ";

$indic_to_roman{si}{"ඟ"} = "ṅᵍᵃ";
$indic_to_roman{si}{"ඬ"} = "ṅᴰᵃ";
$indic_to_roman{si}{"ඳ"} = "ṅᵈᵃ";
$indic_to_roman{si}{"ඹ"} = "ṁᵇᵃ";
$indic_to_roman{si}{"ඛ"} = "kʰᵃ";
$indic_to_roman{si}{"ඝ"} = "gʰᵃ";
$indic_to_roman{si}{"ඨ"} = "ṭʰᵃ";
$indic_to_roman{si}{"ඪ"} = "ḍʰᵃ";
$indic_to_roman{si}{"ථ"} = "tʰᵃ";
$indic_to_roman{si}{"ධ"} = "dʰᵃ";
$indic_to_roman{si}{"ඵ"} = "pʰᵃ";
$indic_to_roman{si}{"භ"} = "bʰᵃ";
$indic_to_roman{si}{"ශ"} = "śᵃ";
$indic_to_roman{si}{"ෂ"} = "ṣᵃ";
$indic_to_roman{si}{"ඡ"} = "cʰᵃ";
$indic_to_roman{si}{"ඣ"} = "jʰᵃ";
$indic_to_roman{si}{"ඤ"} = "ñᵃ";
$indic_to_roman{si}{"ඥ"} = "gⁿᵃ";
$indic_to_roman{si}{"ඞ"} = "ŋᵃ";
$indic_to_roman{si}{"ෆ"} = "fᵃ";
$indic_to_roman{si}{"ඦ"} = "ṅʲᵃ";

$indic_to_roman{si}{"්"} = "`";
$indic_to_roman{si}{"අ"} = "a";
$indic_to_roman{si}{"ඓ"} = "aʲ";
$indic_to_roman{si}{"ෛ"} = "àʲ";
$indic_to_roman{si}{"ආ"} = "ā";
$indic_to_roman{si}{"ා"} = "ã";
$indic_to_roman{si}{"එ"} = "e";
$indic_to_roman{si}{"ෙ"} = "è";
$indic_to_roman{si}{"ඒ"} = "ē";
$indic_to_roman{si}{"ේ"} = "ẽ";
$indic_to_roman{si}{"ඉ"} = "i";
$indic_to_roman{si}{"ි"} = "ì";
$indic_to_roman{si}{"ඊ"} = "ī";
$indic_to_roman{si}{"ී"} = "ĩ";
$indic_to_roman{si}{"ඔ"} = "o";
$indic_to_roman{si}{"ො"} = "ò";
$indic_to_roman{si}{"ඕ"} = "ō";
$indic_to_roman{si}{"ෝ"} = "õ";
$indic_to_roman{si}{"ඖ"} = "aʷ";
$indic_to_roman{si}{"ෞ"} = "àʷ";
$indic_to_roman{si}{"උ"} = "u";
$indic_to_roman{si}{"ු"} = "ù";
$indic_to_roman{si}{"ඌ"} = "ū";
$indic_to_roman{si}{"ූ"} = "ũ";
$indic_to_roman{si}{"ඇ"} = "aᵉ";
$indic_to_roman{si}{"ැ"} = "àᵉ";
$indic_to_roman{si}{"ඈ"} = "āᵉ";
$indic_to_roman{si}{"ෑ"} = "ãᵉ";

$indic_to_roman{si}{"ං"} = "ṃ";
$indic_to_roman{si}{"ඃ"} = "ḥ"; # visarg
$indic_to_roman{si}{"ෘ"} = "Ṛ";
$indic_to_roman{si}{"ෲ"} = "Ṛ²";
$indic_to_roman{si}{"ඍ"} = "ṛ";
$indic_to_roman{si}{"ඎ"} = "ṛ²";
$indic_to_roman{si}{"ඏ"} = "ḽ";
$indic_to_roman{si}{"ඐ"} = "ḽ²";
$indic_to_roman{si}{"ෟ"} = "Ḽ";
$indic_to_roman{si}{"ෳ"} = "Ḽ²";

for $lang (keys %indic_to_roman) {
  for (keys %{$indic_to_roman{$lang}}) {
    if (defined($roman_to_indic{$lang}{$indic_to_roman{$lang}{$_}})) {
      print STDERR "### WARNING: '$indic_to_roman{$lang}{$_}' translitterates at least $roman_to_indic{$lang}{$indic_to_roman{$lang}{$_}} and $_\n";
    } else {
      $roman_to_indic{$lang}{$indic_to_roman{$lang}{$_}} = $_;      
    }
    print STDERR "### WARNING: '$_' (lang $lang) has more than one unicode characters\n" if length > 1;
    if (length($indic_to_roman{$lang}{$_}) > 1) {
      push @{$cmpnd_translitts{$lang}}, $indic_to_roman{$lang}{$_};
    }
  }
  push @{$cmpnd_translitts{$lang}}, ".";
  $cmpnd_translitts_re{$lang} = join ("|", sort {length($b) <=> length($a)} @{$cmpnd_translitts{$lang}});
}

if ($lang =~ /^(fa|ckb)$/ && $translitterate) {
  $do_convert = 1;
  if ($type eq "au" || $type eq "u") {
    while (<>) {
      chomp;
      s/ / /g;
      $output = "";
      $tab = 0;
      while (s/^(.)//) {
	$char = $1;
	if ($char eq "\t") {
	  $no_switch = 0;
	  $tab++;
	}
	if ($col == $tab) {
	  $output .= $char.$_;
	  last;
	}
	if ($tab == 0 && $ilex == 1
	    && (($char eq "_" && /^__/)
		|| ($char eq "‌" && /^‌‌/)
	       )) {
	  $no_switch = 1;
	}
	if (defined($arabic_to_roman{$char})) {
	  if ($do_convert == 0) {
	    $do_convert = 1;
	    $output .= " " unless $no_switch;
	  }
	  $output .= $arabic_to_roman{$char}{unicode};
	} else {
	  if ($do_convert == 1 && $char !~ /\s/) {
	    $do_convert = 0;
	    $output .= " " unless $no_switch;
	  }
	  $output .= $char;
	}
      }
      $output =~ s/^/ /;
      while ($output =~ s/^((?:[^ ]* [^ ]* )*)([^ ]*[^  ]) ([^\S ]*)/\1\2\3  /) {}
      while ($output =~ s/^((?:[^ ]* [^ ]* )*)([^ ]* [^ ]*?)([^\S ]+) /\1\2  \3/) {}
      $output =~ s/^ +//;
      $output =~ s/ +$//;
      print "$output\n";
    }
  } elsif ($type eq "ua" || $type eq "la" || $type eq "a") {
    while (<>) {
      chomp;
      s/â/ā/g;
      s/ł/ħ/g;
      s/ż/ẓ/g;
#      s/} (_[A-Z][^ ]*)/}  \1 /g;
      $output = "";
      $tab = 0;
      while (s/^(.)//) {
	$char = $1;
	$tab++ if $char eq "\t";
	$do_convert = 1 - $do_convert if ($char eq " ");
	$do_convert -= 2 if ($char eq "<");
	$do_convert += 2 if ($char eq ">");
	$do_convert += 4 if ($do_convert <= -3 && $char =~ /\s/);
	$do_convert -= 4 if ($do_convert >= 0 && $char =~ /[ }]/ && /^_[A-Z]/);
	if ($col == $tab) {
	  $output .= $char.$_; last;
	}
	if (defined($roman_unicode_to_arabic{$char}) && $do_convert > 0) {
	  $output .= $roman_unicode_to_arabic{$char};
	} else {
	  $output .= $char;
	}
      }
      $output =~ s/ +//g;
      print "$output\n";
    }
  } elsif ($type eq "al") {
    while (<>) {
      chomp;
      s/ / /g;
      $output = "";
      $tab = 0;
      while (s/^(.)//) {
	$char = $1;
	$tab++ if $char eq "\t";
	if ($col == $tab) {
	  $output .= $char.$_; last;
	}
	if (defined($arabic_to_roman{$char})) {
	  if ($do_convert == 0) {
	    $do_convert = 1;
	    $output .= " ";
	  }
	  $output .= $arabic_to_roman{$char}{latin2};
	} else {
	  if ($do_convert == 1 && $char !~ /\s/) {
	    $do_convert = 0;
	    $output .= " ";
	  }
	  $output .= $char;
	}
      }
      $output =~ s/^/ /;
      while ($output =~ s/^((?:[^ ]* [^ ]* )*)([^ ]*[^  ]) ([^\S ]*)/\1\2\3  /) {}
      while ($output =~ s/^((?:[^ ]* [^ ]* )*)([^ ]* [^ ]*?)([^\S ]+) /\1\2  \3/) {}
      $output =~ s/^ +//;
      $output =~ s/ +$//;
      print "$output\n";
    }
  }
} elsif ($lang =~ /^(hi|si|ta)$/ && $translitterate) {
  $do_convert = 1;
  while (<>) {
    chomp;
    $output = "";
    $tab = 0;
    if ($type ne "a") {
      s/ / /g;
      $do_convert = 1;
    }
    while (s/^($cmpnd_translitts_re{$lang})//) {
      $char = $1;
      $tab++ if $char eq "\t";
      if ($type eq "a") {
	if ($char eq " ") {
	  if ($do_convert % 2 == 0) {
	    $do_convert += 1;
	  } else {
	    $do_convert -= 1;
	  }
	}
      }
      $do_convert -= 2 if ($char eq "<");
      $do_convert += 2 if ($char eq ">");
      $do_convert += 4 if ($do_convert <= -3 && $char =~ /\s/);
      $do_convert -= 4 if ($do_convert >= 0 && $char =~ /[ }]/ && /^_[A-Z]/);
      if ($col == $tab) {
	$output .= $char.$_; last;
      }
      if ($type eq "a") {
	if (defined($roman_to_indic{$lang}{$char})) {
	  $output .= $roman_to_indic{$lang}{$char};
	} else {
	  $output .= $char;
	}
      } else {
	if (defined($indic_to_roman{$lang}{$char})) {
	  if ($do_convert == 0) {
	    $do_convert = 1;
	    $output .= " ";
	  }
	  $output .= $indic_to_roman{$lang}{$char};
	} else {
	  if ($do_convert == 1 && $char !~ /[\s,;:?\.\/+=\%\(\)\[\]\!§\&\@\#\"\'\°_-]/) {
	    $do_convert = 0;
	    $output .= " ";
	  }
	  $output .= $char;
	}
      }
    }
    $output =~ s/ +//g;
#    $output =~ s/_`([kṅcñṭṇtnpmyrlvḻḷṟṉśjṣsh])/\1`/g if $lang eq "ta";
    $output =~ s/``+/`/g if $lang eq "ta";
    if ($lang eq "si") {
      $output =~ s/ᵃ([`àãèẽìĩòõùũṚḼ])/\1/g;
      $output =~ s/`//g;
    } elsif ($lang eq "ta") {
      $output =~ s/ᵃ([àãìĩùũèẽòõ`])/\1/g;
      $output =~ s/`//g;
    } elsif ($lang eq "hi") {
      $output =~ s/ᵃ([`ãìḼĩùũṚèẽàễòõỗ])/\1/g;
      $output =~ s/`//g;
    }
    if ($lang eq "ta" && $output =~ s/~\s*$//) {
      print $output;
    } else {
      print "$output\n";
    }
  }
} elsif ($lang =~ /^(cyr|ru|uk)$/ && $translitterate) {
  $do_convert = 1;
  while (<>) {
    chomp;
    $output = "";
    $tab = 0;
    if ($type ne "a") {
      s/ / /g;
      $do_convert = 1;
    }
    while (s/^($cmpnd_translitts_re{cyrillic})//) {
      $char = $1;
      $tab++ if $char eq "\t";
      if ($type eq "a") {
	if ($char eq " ") {
	  if ($do_convert % 2 == 0) {
	    $do_convert += 1;
	  } else {
	    $do_convert -= 1;
	  }
	}
      }
      $do_convert -= 2 if ($char eq "<");
      $do_convert += 2 if ($char eq ">");
      $do_convert += 4 if ($do_convert <= -3 && $char =~ /\s/);
      $do_convert -= 4 if ($do_convert >= 0 && $char =~ /[ }]/ && /^_[A-Z]/);
      if ($col == $tab) {
	$output .= $char.$_; last;
      }
      if ($type eq "a") {
	if (defined($roman_to_cyrillic{$char})) {
	  $output .= $roman_to_cyrillic{$char};
	} else {
	  $output .= $char;
	}
      } else {
	if (defined($cyrillic_to_roman{$char})) {
	  if ($do_convert == 0) {
	    $do_convert = 1;
	    $output .= " ";
	  }
	  $output .= $cyrillic_to_roman{$char};
	} else {
	  if ($do_convert == 1 && $char !~ /[\s,;:?\.\/+=\%\(\)\[\]\!§\&\@\#\"\'\°_-]/) {
	    $do_convert = 0;
	    $output .= " ";
	  }
	  $output .= $char;
	}
      }
    }
    $output =~ s/ +//g;
    print "$output\n";
  }
} elsif ($lang =~ /^(cyr-en)$/ && $translitterate) {
  $do_convert = 1;
  while (<>) {
    chomp;
    $output = "";
    $tab = 0;
    if ($type ne "a") {
      s/ / /g;
      $do_convert = 1;
    }
    while (s/^($cmpnd_translitts_re{encyrillic})//) {
      $char = $1;
      $tab++ if $char eq "\t";
      if ($type eq "a") {
	if ($char eq " ") {
	  if ($do_convert % 2 == 0) {
	    $do_convert += 1;
	  } else {
	    $do_convert -= 1;
	  }
	}
      }
      $do_convert -= 2 if ($char eq "<");
      $do_convert += 2 if ($char eq ">");
      $do_convert += 4 if ($do_convert <= -3 && $char =~ /\s/);
      $do_convert -= 4 if ($do_convert >= 0 && $char =~ /[ }]/ && /^_[A-Z]/);
      if ($col == $tab) {
	$output .= $char.$_; last;
      }
      if ($type eq "a") {
	if (defined($enroman_to_cyrillic{$char})) {
	  $output .= $enroman_to_cyrillic{$char};
	} else {
	  $output .= $char;
	}
      } else {
	if (defined($cyrillic_to_enroman{$char})) {
	  if ($do_convert == 0) {
	    $do_convert = 1;
	    $output .= " ";
	  }
	  $output .= $cyrillic_to_enroman{$char};
	} else {
	  if ($do_convert == 1 && $char !~ /[\s,;:?\.\/+=\%\(\)\[\]\!§\&\@\#\"\'\°_-]/) {
	    $do_convert = 0;
	    $output .= " ";
	  }
	  $output .= $char;
	}
      }
    }
    $output =~ s/ +//g;
    print "$output\n";
  }
} else {
  while (<>) {
    print $_;
  }
}


