#!/usr/bin/env perl

$alexinadir = shift;
$uw_lang_list_h = shift;

$lang_mask = 1;

for (<$alexinadir/*>) {
  chomp;
  s/^.*\///;
  $lang = $_;
  %seen = ();
  unless (open ENC, "<$alexinadir/$lang/encoding") {
    print STDERR "  Ignoring language '$lang' (no encoding found)\n";
    next;
  }
  $encoding = <ENC>;
  chomp($encoding);
  if ($encoding eq "UTF-8") {
    print STDERR "  Ignoring language '$lang' (UTF-8 lexicon)\n";
    next;
  }
  $lang_id2lang_name .= "\"$lang\",\n";
  $lang_id2lang_mask .= "$lang_mask,\n";
  $lang_num++;
  print STDERR "  Adding words from language '$lang'\n";
  for $file (<$alexinadir/$lang/*.lex>) {
    open (FILE, "<$file") || die "Could not open file $file\n";
    while (<FILE>) {
      chomp;
      next if (/^[\{\}]\t/);
      s/[^\"]\#.*$//;
      s/^\#.*$//;
      s/^\"([^\t]+)\"\t/$1\t/;
      s/\\//g;
      s/^ +//;
      s/ +/ /g;
      next if (/^\s*$/);
      s/^([^\t]*)\t(.*?)\t.*/$1/;
      next if (/^\s*$/);
      s/([^\.]\.)\.(\t|$)/$1$2/g;
      s/ /_/g;
      s/\"/\\\"/g;
      s/^(.*[0-9'\.\&\|\!\?\(\);\:,\*\[\]\%\#\$$\=\+\>\<\/�����\�\�����\&\@\+�\~\�\�\�\`\�\�\\].*)$/\"\1\"/;
      $hash{$_} += $lang_mask unless defined($seen{$_});
      $seen{$_} = 1;
    }
    close (FILE);
  }
  $lang_mask *= 2;
}

print STDERR "  Printing output\n";
open (HFILE, "> $uw_lang_list_h") || die "Could not open incl/uw_lang_list.h for writing";
print HFILE "#define MAX_LANG_ID $lang_num\n";
print HFILE "static char* lang_id2lang_name[".($lang_num+1)."] = {\"\",\n";
print HFILE $lang_id2lang_name;
print HFILE "};\n\n";
print HFILE "static SXINT lang_id2lang_mask[".($lang_num+1)."] = {0,\n";
print HFILE $lang_id2lang_mask;
print HFILE "};\n";
close (HFILE);

for (sort keys %hash) {
  print "$_\t$hash{$_}\n";
}
