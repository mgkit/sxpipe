#include "sxversion.h"
#include "sxunix.h"
#include "udag_scanner.h"

char WHAT_SXTAGGER_MAIN[] = "@(#)SxPipe - $Id: sxtagger_main.c 1491 2008-07-23 16:13:15Z sagot $" WHAT_DEBUG;

static char	ME [] = "sxtagger_main";

SXBOOLEAN        tmp_file_for_stdin = SXFALSE;
SXUINT          maximum_input_size; /* Pour udag_scanner */

/*---------------*/
/*    options    */
/*---------------*/



static SXBOOLEAN  is_help;
SXBOOLEAN is_train;
SXINT paths_number;
static char	Usage [] = "\
Usage:\t%s [options] [files]\n\
options=\t-v, -verbose, -nv, -noverbose,\n\
\t\t-n,\n\
\t\t-t, -train,\n\
\t\t-, -stdin,\n\
\t\t-dag,\n\
\t\t-udag,\n\
\t\t-sdag,\n\
\t\t-string,\n\
\t\t--help,\n\
\
";

#define OPTION(opt)	(1 << (opt - 1))
#define noOPTION(opt)	(~OPTION (opt))

#define OPT_NB ((sizeof(option_tbl)/sizeof(*option_tbl))-1)

#define UNKNOWN_ARG 	  0
#define VERBOSE 	  1
#define PATHS_NUMBER      2
#define TRAIN             3
#define STDIN	  	  4
#define DAG_INPUT         5
#define UDAG_INPUT        6
#define SDAG_INPUT        7
#define STRING_INPUT      8
#define HELP	          9
#define SOURCE_FILE	  10


static char	*option_tbl [] = {
    "",
    "v", "verbose", "nv", "noverbose",
    "n",
    "t", "train",
    "stdin",
    "dag",
    "udag",
    "sdag",
    "string",
    "-help",
};

static SXINT	option_kind [] = {
    UNKNOWN_ARG,
    VERBOSE, VERBOSE, -VERBOSE, -VERBOSE,
    PATHS_NUMBER,
    TRAIN, TRAIN,
    DAG_INPUT,
    UDAG_INPUT,
    SDAG_INPUT,
    STRING_INPUT,
    STDIN,
    HELP,
};



static SXINT
option_get_kind (char *arg)
{
  char	**opt;

  if (*arg++ != '-')
    return SOURCE_FILE;

  if (*arg == SXNUL)
    return STDIN;

  for (opt = &(option_tbl [OPT_NB]); opt > option_tbl; opt--) {
    if (strcmp (*opt, arg) == 0 /* egalite */ )
      break;
  }

  return option_kind [opt - option_tbl];
}



static char*
option_get_text (SXINT kind)
{
  SXINT	i;

  for (i = OPT_NB; i > 0; i--) {
    if (option_kind [i] == kind)
      break;
  }

  return option_tbl [i];
}

extern void finalize (void);

static void
tagger_run (char *pathname)
{
  FILE	*infile = NULL;

  if (pathname == NULL) {
    int	c; /* pas SXINT */

    if (sxverbosep) {
      fputs ("\"stdin\":\n", sxtty);
    }

    sxsrc_mngr (SXINIT, stdin, "");

    source_file_name = "<stdin>";
  }
  else {
    if ((infile = sxfopen (pathname, "r")) == NULL) {
      fprintf (sxstderr, "%s: Cannot open (read) ", ME);
      sxperror (pathname);
      sxerrmngr.nbmess [2]++;
      return;
    }
    else {
      if (sxverbosep) {
	fprintf (sxtty, "%s:\n", pathname);
      }

      sxsrc_mngr (SXINIT, infile, pathname);
    }
    
    source_file_name = pathname;
  }

  source_file = infile;
    
  {
    SXSHORT c;

    /* si le dag_kind n'est pas spécifié par -dag ou -udag, on tente une détection automatique */
    if (dag_kind == 0) {
      c = sxlafirst_char();       // (*sxsrcmngr.infile)._IO_buf_base[0];
      sxlaback(1);                // on revient en arrière après avoir avancé d'un pas de lookahead pour récupérer c

      if ((c < '0' || c > '9') && c != '#' /*c'est probablement un dag normal*/)
	dag_kind = DAG_KIND;
      else
	dag_kind = UDAG_KIND;
    }
  }

  init ();

  dag_scanner (SXBEGIN, NULL /* dummy_tables_ptr */);
  dag_scanner (SXOPEN, NULL);
  dag_scanner (SXINIT, NULL);

  dag_scanner (SXACTION, NULL);

  dag_scanner (SXFINAL, NULL);
  sxsrc_mngr (SXFINAL);
  dag_scanner (SXCLOSE, NULL);
  dag_scanner (SXEND, NULL);

  finalize ();

  if (infile)
    fclose (infile);
}



/************************************************************************/
/* main function */
/************************************************************************/
int
main (int argc, char *argv[])
{
  int		argnum;
  SXBOOLEAN	in_options, is_source_file, is_stdin;

  sxinitialise (is_source_file); /* pour faire taire gcc -Wuninitialized */
  if (sxstdout == NULL) {
    sxstdout = stdout;
  }
  if (sxstderr == NULL) {
    sxstderr = stderr;
  }
  
#if BUG
  /* Suppress bufferisation, in order to have proper	 */
  /* messages when something goes wrong...		 */
  setbuf (stdout, NULL);
#endif
  
  sxopentty ();

  /* valeurs par defaut */
  sxverbosep = SXFALSE;
  is_help = SXFALSE;
  is_stdin = SXTRUE;

  dag_kind = 0;

  maximum_input_size = ~((SXUINT) 0); /* unbounded */

  if (argc > 1) {
    is_source_file = SXFALSE;

    /* Decodage des options */
    in_options = SXTRUE;
    argnum = 0;

    while (in_options && ++argnum < argc) {
      switch (option_get_kind (argv [argnum])) {
      case VERBOSE:
	sxverbosep = SXTRUE;
	break;

      case -VERBOSE:
	sxverbosep = SXFALSE;
	break;

      case HELP:
	is_help = SXTRUE;
	break;

      case TRAIN:
	is_train = SXTRUE;
	break;

      case STDIN:
	is_stdin = SXTRUE;
	break;

      case PATHS_NUMBER:
	if (++argnum >= argc) {
	  fprintf (sxstderr, "%s: an integer number must follow the \"%s\" option;\n", ME, option_get_text (PATHS_NUMBER));
	  SXEXIT (6);
	}

	paths_number = atoi (argv [argnum]);
	
	break;

      case DAG_INPUT:
	dag_kind = DAG_KIND;
	break;

      case UDAG_INPUT:
	dag_kind = UDAG_KIND;
	break;

      case SDAG_INPUT:
	dag_kind = SDAG_KIND;
	break;

      case STRING_INPUT:
	dag_kind = STRING_KIND;
	break;

      case SOURCE_FILE:
	if (is_stdin) {
	  is_stdin = SXFALSE;
	}

	is_source_file = SXTRUE;
	in_options = SXFALSE;
	break;

      case UNKNOWN_ARG:
	SXEXIT (3);
	/* arguments pris par la semantique */
	break;
      default:
	sxtrap (ME, "main");
      }
    }
  }

  if ((!is_stdin && !is_source_file) || is_help) {
    fprintf (sxstderr, Usage, argv [0]);
    SXEXIT (3);
  }

  if (is_stdin) {
    tagger_run (NULL);
  }
  else {
    do {
      tagger_run (argv [argnum++]);
    } while (argnum < argc);
  }

  {
    SXINT	severity;

    for (severity = SXSEVERITIES - 1; severity > 0 && sxerrmngr.nbmess [severity] == 0; severity--)
      ;

    SXEXIT (severity);
  }

  return EXIT_SUCCESS; /* Jamais atteint !! pour les compilo susceptibles ... */
}


