# AC_CHECK_SXPIPE
# Test for SxPipe presence and directories


AC_DEFUN([AC_CHECK_SXPIPE], [
	AC_MSG_CHECKING(for SxPipe)

	AC_ARG_WITH(
		sxpipe-prefix,
		AC_HELP_STRING(
			--with-sxpipe-prefix=PFX,
			SxPipe prefix
		),
		sxpipeprefix=$withval
		if test "x$sxpipeprefix" != "x" && !(test -d $sxpipeprefix); then
			AC_MSG_ERROR("SxPipe prefix is not a valid directory (\$sxpipeprefix=$sxpipeprefix)")
		fi
		,
		sxpipeprefix=""
	)

	AC_ARG_WITH(
		sxpipebin,
		AC_HELP_STRING(
			--with-sxpipebin=DIR,
			directory containing SxPipe binary executables
		),
		sxpipebin=$withval,
		if test -d $sxpipeprefix/usr/local/bin && test -x $sxpipeprefix/usr/local/bin/dag2udag; then
			sxpipebin=$sxpipeprefix/usr/local/bin
		elif test -d $sxpipeprefix/bin && test -x $sxpipeprefix/bin/dag2udag; then
			sxpipebin=$sxpipeprefix/bin
		else
			AC_MSG_ERROR("SxPipe binary executables not found")
		fi
	)

	AC_ARG_WITH(
		sxpipescripts,
		AC_HELP_STRING(
			--with-sxpipescripts=DIR,
			directory containing SxPipe scripts and script subdirectories
		),
		sxpipebin=$withval,
		if test -d $sxpipeprefix/usr/local/share/sxpipe && test -x $sxpipeprefix/usr/local/share/sxpipe/comments_cleaner.pl; then
			sxpipebin=$sxpipeprefix/usr/local/share/sxpipe
		elif test -d $sxpipeprefix/bin && test -x $sxpipeprefix/share/sxpipe/comments_cleaner.pl; then
			sxpipebin=$sxpipeprefix/share/sxpipe
		else
			AC_MSG_ERROR(SxPipe scripts not found")
		fi
	)

	AC_SUBST(sxpipebin)
	AC_SUBST(sxpipescripts)
	AC_MSG_RESULT(yes
		sxpipebin = $sxpipebin
		sxpipescripts = $sxpipescripts)
])
