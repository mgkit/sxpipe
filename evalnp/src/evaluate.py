#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
from xml.dom.minidom import *
import codecs
from optparse import *
from subprocess import *
import collections

class Enamex:
    def __init__(self, id, node, start, end, annotation_type):
        self.id = id
        self.node = node
        self.start = start
        self.end = end
        self.offsets = (start, end)
        self.annotation_type = annotation_type

class Para:
    para_instances = {}
    def __init__(self, item_id, rank, node):
        self.gold_enamex_instances = {}
        self.auto_enamex_instances = {}
        self.item_id = item_id
        self.rank = rank
        self.id = item_id+'#'+rank
        self.gold_node = node
        self.record()

    def record(self):
        Para.para_instances[(self.item_id, self.rank)] = self

    def addAutoPara(self, auto_node):
        self.auto_node = auto_node

    def addEnamex(self, enamex_instance):
        if enamex_instance.annotation_type == 'gold':
            self.gold_enamex_instances[enamex_instance.offsets] = enamex_instance
        elif enamex_instance.annotation_type == 'auto':
            self.auto_enamex_instances[enamex_instance.offsets] = enamex_instance

    def getOffsets(self, annotation_type):
        if annotation_type == 'auto':
            if self.auto_enamex_instances == {}:
                return []
            else:
                # offsets = map((lambda auto_enamex_instance: auto_enamex_instance.offsets), self.auto_enamex_instances)
                offsets = self.auto_enamex_instances.keys()
                return offsets
        elif annotation_type == 'gold':
            if self.gold_enamex_instances == {}:
                return []
            else:
                offsets = self.gold_enamex_instances.keys()
                return offsets

    def makeEval(self):
        gold_offsets_list = self.getOffsets('gold')
        auto_offsets_list = self.getOffsets('auto')

        seen_gold_offsets = set([])
        for auto_offsets in auto_offsets_list:
            auto_enamex_instance = self.auto_enamex_instances[auto_offsets]
            if auto_offsets in gold_offsets_list:
                seen_gold_offsets.add(auto_offsets)
                gold_enamex_instance = self.gold_enamex_instances[auto_offsets]
                if auto_enamex_instance.node.getAttribute('type') == gold_enamex_instance.node.getAttribute('type'):
                    resolution = 'null'
                    # correct span
                    eval_event = EvalEvent('exact', 'correct_span', 'correct_type', auto_enamex_instance, gold_enamex_instance, self.id)
                    # detection
                    if gold_enamex_instance.node.getAttribute('eid') != 'null' and auto_enamex_instance.node.getAttribute('eid') != 'null':
                        ref_detection = 'kk'
                        # resolution
                        if gold_enamex_instance.node.getAttribute('eid') == auto_enamex_instance.node.getAttribute('eid'): resolution = 'correct'
                        else: resolution = 'wrong'
                    elif gold_enamex_instance.node.getAttribute('eid') == 'null' and auto_enamex_instance.node.getAttribute('eid') == 'null':
                        ref_detection = 'uu'
                        # resolution
                        if gold_enamex_instance.node.getAttribute('name') == auto_enamex_instance.node.getAttribute('name'): resolution = 'correct'
                        else: resolution = 'wrong'
                    elif gold_enamex_instance.node.getAttribute('eid') != 'null' and auto_enamex_instance.node.getAttribute('eid') == 'null': ref_detection = 'ku'
                    elif gold_enamex_instance.node.getAttribute('eid') == 'null' and auto_enamex_instance.node.getAttribute('eid') != 'null': ref_detection = 'uk'
                    eval_event.addReferenceDetection(ref_detection)
                    eval_event.addResolution(resolution)
                else:
                    if strictness == 'precision':
                        eval_event = EvalEvent('false_match', 'correct_span', 'wrong_type', auto_enamex_instance, gold_enamex_instance, self.id)
                    else:
                        eval_event = EvalEvent('missed', 'correct_span', 'wrong_type', auto_enamex_instance, gold_enamex_instance, self.id)
            else:
                try: gold_enamex_instance = self.gold_enamex_instances[findPartial(auto_offsets, gold_offsets_list)]
                except: gold_enamex_instance = None
                if gold_enamex_instance != None: # get partially matching gold
                    seen_gold_offsets.add(auto_offsets)
                    if auto_enamex_instance.node.getAttribute('type') == gold_enamex_instance.node.getAttribute('type'):
                        if strictness == 'precision':
                            eval_event = EvalEvent('false_match', 'partial_span', 'correct_type', auto_enamex_instance, gold_enamex_instance, self.id)
                        else:
                            eval_event = EvalEvent('missed', 'partial_span', 'correct_type', auto_enamex_instance, gold_enamex_instance, self.id)
                    else:
                        if strictness == 'precision':
                            eval_event = EvalEvent('false_match', 'partial_span', 'wrong_type', auto_enamex_instance, gold_enamex_instance, self.id)
                        else:
                            eval_event = EvalEvent('missed', 'partial_span', 'wrong_type', auto_enamex_instance, gold_enamex_instance, self.id)
                else:
                    eval_event = EvalEvent('false_match', None, None, auto_enamex_instance, None, self.id)
        missed_gold_offsets = set(gold_offsets_list) - seen_gold_offsets
        for gold_offsets in missed_gold_offsets:
            gold_enamex_instance = self.gold_enamex_instances[gold_offsets]
            eval_event = EvalEvent('missed', None, None, None, gold_enamex_instance, self.id)
            
class EvalEvent:
    eval_events = {'detection':
                   {'exact':{'count':0.0, 'events':[]},
                   'false_match':{'count':0.0, 'events':[]},
                   'missed':{'count':0.0, 'events':[]},
                   'correct_span':{'count':0.0, 'events':[]},
                   'partial_span':{'count':0.0, 'events':[]},
                   'wrong_type':{'count':0.0, 'events':[]},
                   'correct_type':{'count':0.0, 'events':[]}},
                   'ref_detection':
                   {'exact':{'count':0.0, 'events':[]},
                   'false_match':{'count':0.0, 'events':[]},
                   'missed':{'count':0.0, 'events':[]}},
                   'resolution':
                   {'correct':{'count':0.0, 'events':[]},
                   'wrong':{'count':0.0, 'events':[]}}}
    
    eval_sets = {'detection':{'rr':0.0, 'relevant':0.0, 'retrieved':0.0},
                 'ref_detection':{'rr':0.0, 'relevant':0.0, 'retrieved':0.0},
                 'resolution':{'correct':0.0, 'total':0.0}}
    eval_events_instances = {}
    ids = 0
    def __init__(self, event, span_detection, type_detection, auto_enamex_instance, gold_enamex_instance, para_id):
        self.event = event
        self.span_detection = span_detection
        self.type_detection = type_detection
        self.gold_enamex_instance = gold_enamex_instance
        self.auto_enamex_instance = auto_enamex_instance
        try: self.goldEid = self.gold_enamex_instance.node.getAttribute('eid')
        except AttributeError: self.goldEid = 'null'
        try: self.autoEid = self.auto_enamex_instance.node.getAttribute('eid')
        except AttributeError: self.autoEid = 'null'
        self.para_id = para_id
        self.recordEval()
        self.ref_detection = 'null'
        self.resolution = 'null'

    def recordEval(self):
        EvalEvent.eval_events_instances[EvalEvent.ids] = self
        EvalEvent.ids += 1
        
        if self.event != None:
            EvalEvent.eval_events['detection'][self.event]['count'] += 1
            EvalEvent.eval_events['detection'][self.event]['events'].append(self)
        if self.span_detection != None:
            EvalEvent.eval_events['detection'][self.span_detection]['count'] += 1
            EvalEvent.eval_events['detection'][self.span_detection]['events'].append(self)
        if self.type_detection != None:
            EvalEvent.eval_events['detection'][self.type_detection]['count'] += 1
            EvalEvent.eval_events['detection'][self.type_detection]['events'].append(self)

        if self.event == 'exact':
            EvalEvent.eval_sets['detection']['rr'] += 1
            EvalEvent.eval_sets['detection']['retrieved'] += 1
            EvalEvent.eval_sets['detection']['relevant'] += 1
        elif self.event == 'false_match':
            EvalEvent.eval_sets['detection']['retrieved'] += 1
        elif self.event == 'missed':
            EvalEvent.eval_sets['detection']['relevant'] += 1

    def addReferenceDetection(self, ref_detection):
        # kk: rr, rel, retr; uu; ku: rel, missed; uk: false
        if ref_detection == 'kk':
            EvalEvent.eval_events['ref_detection']['exact']['count'] +=1
            #EvalEvent.eval_events['ref_detection']['exact']['events'].append(self)
            EvalEvent.eval_sets['ref_detection']['rr'] += 1
            EvalEvent.eval_sets['ref_detection']['relevant'] += 1
            EvalEvent.eval_sets['ref_detection']['retrieved'] += 1
            EvalEvent.eval_sets['resolution']['total'] += 1
            self.ref_detection = 'exact'
        elif ref_detection == 'uk':
            EvalEvent.eval_events['ref_detection']['false_match']['count'] +=1
            #EvalEvent.eval_events['ref_detection']['false_match']['events'].append(self)
            EvalEvent.eval_sets['ref_detection']['retrieved'] += 1
            self.ref_detection = 'false_match'
        elif ref_detection == 'ku':
            EvalEvent.eval_events['ref_detection']['missed']['count'] +=1
            #EvalEvent.eval_events['ref_detection']['missed']['events'].append(self)
            EvalEvent.eval_sets['ref_detection']['relevant'] += 1
            EvalEvent.eval_sets['ref_detection']['retrieved'] += 1
            self.ref_detection = 'missed'
        elif ref_detection == 'uu':
            EvalEvent.eval_events['ref_detection']['exact']['count'] +=1
            #EvalEvent.eval_events['ref_detection']['missed']['events'].append(self)
            EvalEvent.eval_sets['ref_detection']['rr'] += 1
            EvalEvent.eval_sets['ref_detection']['relevant'] += 1
            EvalEvent.eval_sets['ref_detection']['retrieved'] += 1
            EvalEvent.eval_sets['resolution']['total'] += 1
            self.ref_detection = 'exact'
            
    def addResolution(self, resolution):
        if resolution == 'correct':
            EvalEvent.eval_events['resolution']['correct']['count'] += 1
            # EvalEvent.eval_events['resolution'][resolution]['events'].append(self)
            EvalEvent.eval_sets['resolution']['correct'] += 1
            self.resolution = 'correct'
        elif resolution == 'wrong':
            EvalEvent.eval_events['resolution']['wrong']['count'] += 1
            # EvalEvent.eval_events['resolution']['wrong']['events'].append(self)
            self.resolution = 'wrong'

    def toXmlNode(self, dom):
        event_node = createElementNode(dom, 'eval_event', {'id':self.para_id})
        detection_node = createElementNode(dom, 'detection', {'detection':self.event, 'span':self.span_detection, 'type':self.type_detection})
        event_node.appendChild(detection_node)
        ref_detection_node = createElementNode(dom, 'reference_detection', {'ref_detection':self.ref_detection})
        event_node.appendChild(ref_detection_node)
        resolution_node = createElementNode(dom, 'resolution', {'resolution':self.resolution, 'goldEid':self.goldEid, 'autoEid':self.autoEid})
        event_node.appendChild(resolution_node)
        if self.gold_enamex_instance != None:
            gold_node = createElementNode(dom, 'GOLD', {'offsets':'_'.join(self.gold_enamex_instance.offsets)})
            gold_node.appendChild(self.gold_enamex_instance.node.cloneNode(True))
        else:
            gold_node = createElementNode(dom, 'GOLD')
        event_node.appendChild(gold_node)
        if self.auto_enamex_instance != None:
            auto_node = createElementNode(dom, 'AUTO', {'offsets':'_'.join(self.auto_enamex_instance.offsets)})
            auto_node.appendChild(self.auto_enamex_instance.node.cloneNode(True))
        else:
            auto_node = createElementNode(dom, 'AUTO')
        event_node.appendChild(auto_node)
        return event_node


    def display(self, id):
        print >> sys.stderr, "Eval - %s - #%d"%(self.para_id, id)
        print >> sys.stderr, "\t%s|%s|%s"%(self.event, self.span_detection, self.type_detection)
        if self.gold_enamex_instance != None: print >> sys.stderr, "Gold: %s (%s, %s)"%(self.gold_enamex_instance.node.toxml(), self.gold_enamex_instance.start, self.gold_enamex_instance.end)
        elif self.gold_enamex_instance == None: print >> sys.stderr, "Gold: None ()"
        if self.auto_enamex_instance != None: print >> sys.stderr, "Auto: %s (%s, %s)"%(self.auto_enamex_instance.node.toxml(), self.auto_enamex_instance.start, self.auto_enamex_instance.end)
        elif self.auto_enamex_instance == None: print >> sys.stderr, "Auto:None ()"
        
def main():
    options, args = getOptions()
    global types2eval
    if options.ent_type == None: types2eval = ['Person', 'Location', 'Organization', 'Company']
    else: types2eval = [options.ent_type]
    global eval_type
    eval_type = options.eval_type
    global strictness
    strictness = options.strict
    # print >> sys.stderr, "Parsing gold..."
    gold_dom = parse(options.gold)
    # print >> sys.stderr, "Initializing paras..."
    initParas(gold_dom)
    # print >> sys.stderr, "Getting gold data..."
    getDomData('gold', gold_dom, eval_type)
    # print >> sys.stderr, "Parsing auto..."
    auto_dom = parse(options.auto)
    # print >> sys.stderr, "Getting auto data..."
    getDomData('auto', auto_dom, eval_type)
    # print >> sys.stderr, "Making eval in paras..."
    for para_instance_id, para_instance in Para.para_instances.items():
        if not(para_instance.gold_enamex_instances == {} and para_instance.auto_enamex_instances == {}):
            para_instance.makeEval()
    # for i in range(EvalEvent.ids):
        # EvalEvent.eval_events_instances[i].display(i)
        # raw_input()

    # print >> sys.stderr, "Writing XML..."
    writeEval(options.out)

    # eval_events = {'exact':{'count':0.0, 'events':[]},
    #                'false_match':{'count':0.0, 'events':[]},
    #                'missed':{'count':0.0, 'events':[]},
    #                'correct_span':{'count':0.0, 'events':[]},
    #                'partial_span':{'count':0.0, 'events':[]},
    #                'wrong_type':{'count':0.0, 'events':[]},
    #                'correct_type':{'count':0.0, 'events':[]}}
    # eval_sets = {'detection':{'rr':0.0, 'relevant':0.0, 'retrieved':0.0}, 'ref_detection':{'rr':0.0, 'relevant':0.0, 'retrieved':0.0}, 'resolution':{'rr':0.0, 'relevant':0.0, 'retrieved':0.0}}

    delim = "------------"
    sys.stderr.write("|%s+%s+%s+%s+%s+%s+%s+%s+%s|\n"%("-----------------", delim, delim, delim, delim, delim, delim, delim, delim))
    sys.stderr.write("| %-15s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s |\n"%("Retr.-Rel. Type", "Relevant", "Missed", "False", "Retr.", "Retr.-Rel.", "Precision", "Recall", "Fscore"))
    sys.stderr.write("|%s+%s+%s+%s+%s+%s+%s+%s+%s|\n"%("-----------------", delim, delim, delim, delim, delim, delim, delim, delim))

    # COMPUTE P/R/F
    # DETECTION
    detection_precision = EvalEvent.eval_sets['detection']['rr'] / EvalEvent.eval_sets['detection']['retrieved']
    detection_recall = EvalEvent.eval_sets['detection']['rr'] / EvalEvent.eval_sets['detection']['relevant']
    detection_f = (2 * detection_precision * detection_recall) / (detection_precision + detection_recall)

    sys.stderr.write("| %-15s | %-10d | %-10d | %-10d | %-10d | %-10d | %-10f | %-10f | %-10f |\n"%("Detection", EvalEvent.eval_sets['detection']['relevant'], EvalEvent.eval_events['detection']['missed']['count'], EvalEvent.eval_events['detection']['false_match']['count'], EvalEvent.eval_sets['detection']['retrieved'], EvalEvent.eval_sets['detection']['rr'], detection_precision, detection_recall, detection_f))
    sys.stderr.write("|%s+%s+%s+%s+%s+%s+%s+%s+%s|\n"%("-----------------", delim, delim, delim, delim, delim, delim, delim, delim))

    # COMPUTE P/R/F
    # REFERENCE DETECTION
    ref_detection_precision = EvalEvent.eval_sets['ref_detection']['rr'] / EvalEvent.eval_sets['ref_detection']['retrieved']
    ref_detection_recall = EvalEvent.eval_sets['ref_detection']['rr'] / EvalEvent.eval_sets['ref_detection']['relevant']
    ref_detection_f = (2 * ref_detection_precision * ref_detection_recall) / (ref_detection_precision + ref_detection_recall)

    sys.stderr.write("| %-15s | %-10d | %-10d | %-10d | %-10d | %-10d | %-10f | %-10f | %-10f |\n"%("Ref.Detection", EvalEvent.eval_sets['ref_detection']['relevant'], EvalEvent.eval_events['ref_detection']['missed']['count'], EvalEvent.eval_events['ref_detection']['false_match']['count'], EvalEvent.eval_sets['ref_detection']['retrieved'], EvalEvent.eval_sets['ref_detection']['rr'], ref_detection_precision, ref_detection_recall, ref_detection_f))
    sys.stderr.write("|%s+%s+%s+%s+%s+%s+%s+%s+%s|\n"%("-----------------", delim, delim, delim, delim, delim, delim, delim, delim))

    # COMPUTE ACCURACY
    # RESOLUTION

    resolution_accuracy = EvalEvent.eval_sets['resolution']['correct'] / EvalEvent.eval_sets['resolution']['total']
    sys.stderr.write("| %-15s | %-10d | %-10s | %-10s | %-10s | %-10d | %-10f | %-10s | %-10s |\n"%("Resolution", EvalEvent.eval_sets['resolution']['total'], '-', '-', '-', EvalEvent.eval_sets['resolution']['correct'], resolution_accuracy, '-', '-'))
    sys.stderr.write("|%s+%s+%s+%s+%s+%s+%s+%s+%s|\n"%("-----------------", delim, delim, delim, delim, delim, delim, delim, delim))


def writeEval(file):
    impl = getDOMImplementation()
    xml = impl.createDocument(None, 'evaluation_logs', None)
    root = xml.documentElement
    # eval_events['detection'] = {'exact':0.0, 'false_match':0.0, 'missed':0.0, 'correct_span':0.0, 'partial_span':0.0, 'wrong_type':0.0, 'correct_type':0.0}
    for case in ['correct_span', 'correct_type', 'partial_span', 'wrong_type', 'false_match', 'missed']:
        case_node = createElementNode(xml, case.upper(), {'count':str(EvalEvent.eval_events['detection'][case]['count'])})
        for eval_event in EvalEvent.eval_events['detection'][case]['events']:
           case_node.appendChild(eval_event.toXmlNode(xml))
        root.appendChild(case_node)
    F = codecs.open(file, "w", "utf-8")
    F.write(xml.toprettyxml())
    F.close()

def findPartial(auto_offsets, gold_offsets_list):
    (astart, aend) = auto_offsets
    for (gstart, gend) in gold_offsets_list:
        if partialSpan(int(gstart), int(gend), int(astart), int(aend)): 
            return (gstart, gend)
    else:
        return None

def partialSpan(gstart, gend, astart, aend):
    # CASE 1: INSIDE
    if (astart > gstart) and (aend < gend): return "inside"
    # CASE 2: BIGGER
    elif (astart < gstart) and (aend > gend): return "bigger"
    # CASE 3: RIGHT-SHORT
    elif (astart == gstart) and (aend < gend): return "right-short"
    # CASE 4: RIGHT-LONG
    elif (astart == gstart) and (aend > gend): return "right-long"
    # CASE 5: LEFT-SHORT
    elif (astart > gstart) and (aend == gstart): return "left-short"
    # CASE 6: LEFT-LONG
    elif (astart < gstart) and (aend == gend): return "left-long"
    # CASE 7: RIGHT-OVERLAP
    elif (astart > gstart) and (astart < gend) and (aend > gend): return "right-overlap"                                                                                                    
    # CASE 8: LEFT-OVERLAP
    elif (astart < gstart) and (aend < gstart) and (aend > gstart): return "left-overlap"
    else: return False

def initParas(dom):
    items = selectNodes(dom, {'item':{'eval_type':[eval_type]}})
    for item in items:
        for para in item.getElementsByTagName('para'):
            para_instance = Para(item.getAttribute('ref'), para.getAttribute('rank'), para)
    
def getDomData(annotation_type, dom, eval_type):
    items = selectNodes(dom, {'item':{'eval_type':[eval_type]}})
    for item in items:
        for para in item.getElementsByTagName('para'):
            para_instance = Para.para_instances[(item.getAttribute('ref'), para.getAttribute('rank'))]
            if annotation_type == 'auto': para_instance.addAutoPara(para)
            # all enamexes in para:
            enamexes = para.getElementsByTagName('ENAMEX')
            # tuples list [(entity, start, end)+] of all para enamexes
            # para_data = getParaData(para.toxml())
            para_data = equalizeOffsets(para.toxml())
            # temp table {enamex:tuple} with all enamexes
            enamexes2para_data = dict(zip(enamexes, para_data))
            # select relevant enamexes for this eval (by type)
            relevant_enamexes = selectNodes(para, {'ENAMEX':{'type':types2eval}})
            # create instances of relevant enamexes with offsets as attributes
            counter = 0
            for relevant_enamex in relevant_enamexes:
                # print >> sys.stderr, enamexes2para_data[relevant_enamex]; raw_input()
                counter += 1
                (content, start, end) = enamexes2para_data[relevant_enamex]
                enamex_id = item.getAttribute('ref')+'#'+para.getAttribute('rank')+'#gold'+str(counter)
                enamex_instance = Enamex(enamex_id, relevant_enamex, start, end, annotation_type)
                para_instance.addEnamex(enamex_instance)

def selectNodes(dom, selectTable):
    nodes = []
    for node2select, attr_name2value in selectTable.items():
        for node in dom.getElementsByTagName(node2select):
            for attr_name, attr_values in attr_name2value.items():
                for attr_value in attr_values:
                    if node.getAttribute(attr_name) == attr_value:
                        nodes.append(node)
                        break
    return nodes

def equalizeOffsets(lines):
    temp = re.sub('\n+', '', lines)
    temp = re.sub('^[ \t]*</?para[^>]+>[ \t]*', '', temp)
    string = temp.rstrip()
    # sys.stderr.write(string.encode('utf-8'))
    has_tags = re.search('<[^>]+>', string)
    if has_tags == None:
        # re.sub(' +', '', string)
        return []
    else:
        offsets = []
        pattern = re.compile('(.*?)(<ENAMEX[^>]*>)([^<]+)(</ENAMEX>)(.*)$')
        start = 0
        while(pattern.search(string)):
            # sys.stdout.write(string.encode('utf-8'))
            match = pattern.search(string)
            if match.group(1) != None: before_tag = re.sub(' +', '', match.group(1))
            else: before_tag = ''
            start += len(before_tag) + 1
            otag = match.group(2)
            entity = re.sub(' +', '', match.group(3))
            ctag = match.group(4)
            after_tag = match.group(5)
            end = start + len(entity) - 1
            offsets.append(tuple([entity, str(start), str(end)]))
            if pattern.search(after_tag) != None:
                string = after_tag
                start = end
            else:
                break
        # sys.stderr.write('\n'.join(map((lambda t: '\t'.join(t)), offsets)).encode('utf-8')+'\n')
        return offsets


# DOM FUNCTIONS
def createElementNode(dom, tag_name, attributes_and_values=None, data=None):
    new_element = dom.createElement(tag_name)
    if attributes_and_values:
        for att_name in attributes_and_values.keys():
            if attributes_and_values[att_name] != None and attributes_and_values[att_name] != '':
                addAttribute2Element(dom, new_element, att_name, attributes_and_values[att_name])
    if data:
        data = re.sub('&amp;', '&', data)
        data = re.sub('&', '&amp;', data)
        addData2Element(dom, new_element, data)
    return new_element

def addAttribute2Element(dom, node, att_name, att_value):
    att = dom.createAttribute(att_name)
    att_node = node.setAttributeNode(att)
    node.setAttribute(att_name, att_value)

def addData2Element(dom, node, data):
    text = dom.createTextNode(data)
    new_node = node.appendChild(text)

def getOptions():
    parser = OptionParser()
    type_choices = ['Person', 'Location', 'Organization', 'Company']
    eval_choices = ['dev', 'test', 'any']
    strict_choices = ['precision', 'recall']
    parser.add_option("--auto", action="store", dest="auto", default=None, help="automatic annotation")
    parser.add_option("--gold", action="store", dest="gold", default=None, help="gold annotation")
    parser.add_option("--eval", action="store", dest="eval_type", default=None, type="choice", choices=eval_choices, help="evaluate on 'test' or 'dev' corpus")
    parser.add_option("--type", action="store", dest="ent_type", default=None, type="choice", choices=type_choices, help="evaluate only one entity type ('Person' or 'Location' or 'Organization' or 'Company')")
    parser.add_option("--strict", action="store", dest="strict", default='precision', type="choice", choices=strict_choices, help="be strict on precision|recall (default:precision)")
    parser.add_option("--out", "-o", action="store", dest="out", default=None, help="output file")
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    if options.auto == None or options.gold == None: sys.stderr.write("You must specify automatic and gold annotations\n"); parser.print_help(); sys.exit()
    if options.eval_type == None: sys.stderr.write('You must specify the evaluation type\n'); parser.print_help(); sys.exit()
    if options.out == None: sys.stderr.write("You must specify the output file name\n"); parser.print_help(); sys.exit()
    return options, args



def getParaData(para_content):
    # input_file = 'temp_offsets_input'
    # TEMPIN = codecs.open(input_file, 'w', 'utf-8')
    # TEMPIN.write(para_content)
    # cat_infile = Popen(["cat", input_file], stdout=PIPE)
    # equalizeOffsets_cmd = Popen(['./src/equalizeOffsets'], stdin=cat_infile.stdout, stdout=PIPE)
    # output = equalizeOffsets_cmd.communicate()[0]
    # TEMPIN.close()
    # os.unlink(input_file)
    offsets_tuples = equalizeOffsets(para_content)
    if offsets_tuples != None:
        return offsets_tuples
    else:
        return []
    # else:
    #     # READ OUTPUT AS "ENTITY-WITHOUT-SPACE \t START-OFFSET \t END-OFFSET"
    #     # TUPLE: (entity, start, end)
    #     para_data = []
    #     data = ()
    #     for line in output.split('\n'):
    #         data = tuple(line.split('\t'))
    #         if data != ('',): para_data.append(data)
    #     return para_data

if __name__ == "__main__":
    main()
