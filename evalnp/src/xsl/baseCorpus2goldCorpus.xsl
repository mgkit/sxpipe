<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jan 12, 2010</xd:p>
            <xd:p><xd:b>Author:</xd:b> stern</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    
    <!-- take the manually corrected corpus and extract paras that are manually corrected == gold -->
    <!-- divided in 2 sets of 50 items: 50 for development, 50 for test => @eval_type=dev|test -->
    
    <xsl:output indent="yes" method="xml" encoding="utf-8"/>
    <!--<xsl:strip-space elements="*"/>-->
    
    <!-- ENTITIES COUNTERS -->
    <xsl:param name="cpt_total_dev">
        <xsl:value-of select="$cpt_person_dev + $cpt_location_dev + $cpt_organization_dev + $cpt_company_dev"/> 
    </xsl:param>
<!--    <xsl:param name="cpt_total_dev">
        <xsl:value-of select="$cpt_person_dev + $cpt_location_dev + $cpt_organization_dev"/> 
    </xsl:param>-->
    <xsl:param name="cpt_total_test">
        <xsl:value-of select="$cpt_person_test + $cpt_location_test + $cpt_organization_test + $cpt_company_test"/> 
    </xsl:param>
<!--    <xsl:param name="cpt_total_test">
        <xsl:value-of select="$cpt_person_test + $cpt_location_test + $cpt_organization_test"/> 
        </xsl:param>-->
    
    
    <!-- DEV COUNTERS --><!--
    <xsl:param name="cpt_person_m_dev">
        <xsl:value-of select="count(//*[local-name()='news_item' and @corrected='true' and count(preceding-sibling::news_item[@corrected='true']) &lt; 51]/*/*[local-name()='para' and not(@double='true')]/Person_m)"/>
    </xsl:param>      -->
<!--    <xsl:param name="cpt_person_f_dev">
        <xsl:value-of select="count(//*[local-name()='news_item' and @corrected='true' and count(preceding-sibling::news_item[@corrected='true']) &lt; 51]/*/*[local-name()='para' and not(@double='true')]/Person_f)"/>
    </xsl:param>      -->
    <xsl:param name="cpt_person_dev">
        <xsl:value-of select="count(//*[local-name()='news_item' and @corrected='true' and count(preceding-sibling::news_item[@corrected='true']) &lt; 51]/*/*[local-name()='para' and not(@double='true')]/ENAMEX[@type='Person'])"/>
    </xsl:param>      
<!--    <xsl:param name="cpt_persons_dev">
        <xsl:value-of select="$cpt_person_dev + $cpt_person_f_dev + $cpt_person_m_dev"/>
    </xsl:param>   -->   
    <xsl:param name="cpt_location_dev">
        <xsl:value-of select="count(//*[local-name()='news_item' and @corrected='true' and count(preceding-sibling::news_item[@corrected='true']) &lt; 51]/*/*[local-name()='para' and  not(@double='true')]/ENAMEX[@type='Location'])"/>
    </xsl:param>
    <xsl:param name="cpt_organization_dev">
        <xsl:value-of select="count(//*[local-name()='news_item' and @corrected='true' and count(preceding-sibling::news_item[@corrected='true']) &lt; 51]/*/*[local-name()='para' and not(@double='true')]/ENAMEX[@type='Organization'])"/>
    </xsl:param>
    <xsl:param name="cpt_company_dev">
        <xsl:value-of select="count(//*[local-name()='news_item' and @corrected='true' and count(preceding-sibling::news_item[@corrected='true']) &lt; 51]/*/*[local-name()='para' and not(@double='true')]/Company)"/>
    </xsl:param>
    
    <!-- TEST COUNTERS -->
<!--    <xsl:param name="cpt_person_m_test">
        <xsl:value-of select="count(//*[local-name()='news_item' and @corrected='true' and count(preceding-sibling::news_item[@corrected='true']) &gt; 50]/*/*[local-name()='para' and not(@double='true')]/Person_m)"/>
    </xsl:param>      
    <xsl:param name="cpt_person_f_test">
        <xsl:value-of select="count(//*[local-name()='news_item' and @corrected='true' and count(preceding-sibling::news_item[@corrected='true']) &gt; 50]/*/*[local-name()='para' and not(@double='true')]/Person_f)"/>
    </xsl:param>      -->
    <xsl:param name="cpt_person_test">
        <xsl:value-of select="count(//*[local-name()='news_item' and @corrected='true' and count(preceding-sibling::news_item[@corrected='true']) &gt; 50]/*/*[local-name()='para' and not(@double='true')]/ENAMEX[@type='Person'])"/>
    </xsl:param>          
<!--    <xsl:param name="cpt_persons_test">
        <xsl:value-of select="$cpt_person_test + $cpt_person_f_test + $cpt_person_m_test"/>
    </xsl:param>     --> 
    <xsl:param name="cpt_location_test">
        <xsl:value-of select="count(//*[local-name()='news_item' and @corrected='true' and count(preceding-sibling::news_item[@corrected='true']) &gt; 50]/*/*[local-name()='para' and  not(@double='true')]/ENAMEX[@type='Location'])"/>
    </xsl:param>
    <xsl:param name="cpt_organization_test">
        <xsl:value-of select="count(//*[local-name()='news_item' and @corrected='true' and count(preceding-sibling::news_item[@corrected='true']) &gt; 50]/*/*[local-name()='para' and not(@double='true')]/ENAMEX[@type='Organization'])"/>
    </xsl:param>
    <xsl:param name="cpt_company_test">
        <xsl:value-of select="count(//*[local-name()='news_item' and @corrected='true' and count(preceding-sibling::news_item[@corrected='true']) &gt; 50]/*/*[local-name()='para' and not(@double='true')]/Company)"/>
    </xsl:param>
    
    <!-- CORPUS COUNTERS -->
    <xsl:param name="cpt_items">
        <xsl:value-of select="count(//*[local-name()='news_item' and @corrected='true'])"/>
    </xsl:param>
    <xsl:param name="cpt_paras">
        <xsl:value-of select="count(//*[local-name()='news_item' and @corrected='true']/*/*[local-name()='para' and not(@double='true')])"/>
    </xsl:param>
    
    
    <!-- TEMPLATES -->
    <xsl:template match="/child::*">
        <gold_corpus>
            <xsl:comment>CORPUS ANNOTE POUR L'EVALUATION DE SYSTEMES D'EXTRACTION D'ENTITES NOMMEES ET DE DETECTION DE REFERENCE</xsl:comment>
            <xsl:comment>REFERENCES : DB ALEDA (ALPAGE / BENOIT SAGOT)</xsl:comment>
            <xsl:comment>AUTEUR : R.STERN [ALPAGE (INRIA-PARIS7) / AFP]</xsl:comment>
            <xsl:comment>stern.rosa@gmail.com</xsl:comment>
<!--            <total_dev><xsl:value-of select="$cpt_total_dev"/></total_dev>
            <total_test><xsl:value-of select="$cpt_total_test"/></total_test>-->
            <xsl:apply-templates select="./news_item"/>    
        </gold_corpus>
    </xsl:template>

    <xsl:template match="news_item">
        <xsl:if test="./@corrected='true'">
            <item>
                <xsl:attribute name="rank">
                    <xsl:value-of select="1 + count(preceding-sibling::news_item[@corrected='true'])"/>
                </xsl:attribute>
                <xsl:attribute name="eval_type">
                    <xsl:choose>
                        <xsl:when test="count(preceding-sibling::news_item[@corrected='true']) mod 2">test</xsl:when>
                        <xsl:otherwise>dev</xsl:otherwise>
                    </xsl:choose>    
                </xsl:attribute>
                <xsl:attribute name="ref">
                    <xsl:value-of select="./@ref"/>
                </xsl:attribute>
                <!--<xsl:apply-templates select="./news_head"/>-->
                <xsl:apply-templates select="./news_text"/>
            </item>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="news_head">
        <head>
            <xsl:value-of select="."/>
        </head>
    </xsl:template>
    
    <xsl:template match="news_text">
        <!--<text><xsl:apply-templates select="./para"/></text>-->
        <xsl:apply-templates select="./para"/>
    </xsl:template>
    
    <xsl:template match="para">
        <xsl:if test="not(./@double)">               
            <xsl:choose>
                <!--<xsl:when test="position()=last()">-->
                <xsl:when test="./@signature='true'">
                    <para>
                        <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>
                        <xsl:attribute name="signature">
                            <xsl:value-of select="normalize-space(.)"/>
                        </xsl:attribute>
                    </para>
                </xsl:when>
                <xsl:otherwise>
                    <para>
                        <xsl:attribute name="rank"><xsl:value-of select="./@rank"/></xsl:attribute>
<!--                        <xsl:attribute name="entities">
                           <xsl:value-of select="count(./Person)+count(./Location)+count(Organization)+count(Company)"/>
                        </xsl:attribute>-->
                        <xsl:apply-templates/>
                    </para>        
                </xsl:otherwise>
            </xsl:choose>                
        </xsl:if>
    </xsl:template>
        
    <xsl:template match="NONE_ENAMEX"><!--Location|Organization|Person|Company"-->
        <ENAMEX>
            <xsl:attribute name="type"><xsl:value-of select="./@type"/></xsl:attribute>
            <xsl:if test="./@sub_type"><xsl:attribute name="sub_type"><xsl:value-of select="./@sub_type"/></xsl:attribute></xsl:if>
            <xsl:attribute name="name"><xsl:value-of select="./@name"/></xsl:attribute>
<!--            <xsl:choose>
                <xsl:when test="./@eid='null'">
                    <xsl:choose>
                        <xsl:when test="./@ref='unknown'"><xsl:attribute name="ref">unknown</xsl:attribute></xsl:when>
                        <xsl:otherwise><xsl:attribute name="ref">known</xsl:attribute></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="ref">known</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
-->            <xsl:value-of select="."/>
        </ENAMEX>
    </xsl:template>
    
    <xsl:template match="ENAMEX">
<!--        <xsl:choose>
            <xsl:when test=".='France'">
                <xsl:value-of select="."/>
            </xsl:when>
            <xsl:otherwise>
-->                <xsl:copy-of select="."/>
<!--            </xsl:otherwise>
        </xsl:choose>
-->    </xsl:template>
    
<!--    <xsl:template match="NUMEX">
        <NUMEX>
            <xsl:attribute name="type"><xsl:value-of select="./@type"/></xsl:attribute>
            <xsl:value-of select="."/>
        </NUMEX>
    </xsl:template>
    
    <xsl:template match="TIMEX">
        <TIMEX>
            <xsl:attribute name="type"><xsl:value-of select="./@type"/></xsl:attribute>
            <xsl:value-of select="."/>
        </TIMEX>
    </xsl:template>-->
    


</xsl:stylesheet>
<!--    
    <xsl:template match="Company">
        <Organization>
            <xsl:attribute name="name"><xsl:value-of select="./@name"/></xsl:attribute>
                                                <xsl:choose>
                <xsl:when test="./@ref">
                    <xsl:choose>
                        <xsl:when test="./@ref='unknown'"><xsl:attribute name="ref">unknown</xsl:attribute></xsl:when>
                        <xsl:otherwise><xsl:attribute name="ref">known</xsl:attribute></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="ref">known</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:value-of select="normalize-space(child::text())"/>
        </Organization>
    </xsl:template>
    
    <xsl:template match="Person_m">
        <Person>
            <xsl:attribute name="name"><xsl:value-of select="./@name"/></xsl:attribute>
            <xsl:attribute name="gender">masc</xsl:attribute>
                                    <xsl:choose>
                <xsl:when test="./@ref">
                    <xsl:choose>
                        <xsl:when test="./@ref='unknown'"><xsl:attribute name="ref">unknown</xsl:attribute></xsl:when>
                        <xsl:otherwise><xsl:attribute name="ref">known</xsl:attribute></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="ref">known</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:value-of select="normalize-space(child::text())"/>
        </Person>
    </xsl:template>
    <xsl:template match="Person_f">
        <Person>
            <xsl:attribute name="name"><xsl:value-of select="./@name"/></xsl:attribute>
            <xsl:attribute name="gender">fem</xsl:attribute>
                                    <xsl:choose>
                <xsl:when test="./@ref">
                    <xsl:choose>
                        <xsl:when test="./@ref='unknown'"><xsl:attribute name="ref">unknown</xsl:attribute></xsl:when>
                        <xsl:otherwise><xsl:attribute name="ref">known</xsl:attribute></xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="ref">known</xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:value-of select="normalize-space(child::text())"/>
        </Person>
    </xsl:template>
    
   

            <entities>
                <xsl:for-each select="./Person|./Location|./Organization|./Company">
                       <entity>
                            <xsl:attribute name="type">
                                <xsl:value-of select="local-name()"/>
                            </xsl:attribute>
                            <xsl:attribute name="pid">
                                <xsl:value-of select="./@pid"/>
                            </xsl:attribute>
                            <xsl:attribute name="name">
                                <xsl:value-of select="./@name"/>
                            </xsl:attribute>
                            <xsl:if test="./@gender">
                                <xsl:attribute name="gender">
                                    <xsl:value-of select="./@gender"/>
                                </xsl:attribute>
                            </xsl:if>                           
                           <xsl:value-of select="."/>
                       </entity>     
                </xsl:for-each>
                </entities>    -->
