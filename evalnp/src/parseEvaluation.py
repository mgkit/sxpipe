#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import re
from xml.dom.minidom import *
import codecs
from optparse import *
from subprocess import *
import collections

def main():
    options, args = getOptions()
    dom = parse(args[0])
    counts = {'detection':{'retrieved':0.0, 'relevant':0.0, 'rr':0.0, 'missed':0.0, 'false':0.0},
              'ref_detection': {'retrieved':0.0, 'relevant':0.0, 'rr':0.0, 'missed':0.0, 'false':0.0},
              'resolution':{'all':0.0, 'correct':0.0}}
    logs = {'spans':{'missed':[], 'false_match':[], 'false_or_partial':[], 'missed_or_partial':[], 'exact':[]},
                'types':{'wrong':[], 'correct':[]},
                'ref_detection':{'kk-uu':[], 'ku-uk':[]},
                'resolution':{'kk':[], 'uu':[], 'ku':[], 'uk':[]}}
    # <item eval_type="test" rank="1" ref="urn:newsml:afp.com:20090520T164214Z:TX-PAR-FVU98:1">
    # 	<para rank="2">
    # 		<auto_entities>
    # 			<ENAMEX eid="2000000001816670" local_id="auto1" name="Beijing" type="Location">Pékin</ENAMEX>
    # 		</auto_entities>
    # 		<gold_entities>
    # 			<ENAMEX eid="2000000002643743" gnccode="GB" gnfeature="PPLC" local_id="gold2" name="London (2)" sub_type="Capital" type="Location">Londres</ENAMEX>
    # 		</gold_entities>
    # 		<eval>
    # 			<entity autoEntity="auto1" entity_type="Location" goldEntity="gold1" known="correct_known" name="correct" resolution="correct" span="exact" type="correct"/>
    # 			<entity autoEntity="auto2" entity_type="Location" goldEntity="gold2" known="correct_known" name="wrong" resolution="correct" span="exact" type="correct"/>
    # 		</eval>
    # 	</para>
    # spans: exact, false_or_partial, missed, missed_or_partial, null
    for para in dom.documentElement.getElementsByTagName('para'):
        if len(para.getElementsByTagName('eval')) == 0: continue
        # print >> sys.stderr, "In news %s / para %s"%(para.parentNode.getAttribute('ref'), para.getAttribute('rank'))
        # INCREMENT RELEVANT (RESTRICT ON TYPE IF TYPE)
        # try: relevant = len(selectNodes(para.getElementsByTagName('gold_entities')[0].getElementsByTagName('ENAMEX'), {'type':options.type}))
        # except IndexError: relevant = 0.0
        try: relevant = len(para.getElementsByTagName('gold_entities')[0].getElementsByTagName('ENAMEX'))
        except IndexError: relevant = 0.0
        counts['detection']['relevant'] += relevant
        # INCREMENT RETRIEVED (RESTRICT ON TYPE IF TYPE)
        # try: retrieved = len(selectNodes(para.getElementsByTagName('auto_entities')[0].getElementsByTagName('ENAMEX'), {'type':options.type}))
        # except IndexError: retrieved = 0.0
        try: retrieved = len(para.getElementsByTagName('auto_entities')[0].getElementsByTagName('ENAMEX'))
        except IndexError: retrieved = 0.0
        counts['detection']['retrieved'] += retrieved
        # BROWSE ENTITIES
        # INCREMENT COUNTS (RESTRICT ON TYPE IF TYPE)
        for entity in para.getElementsByTagName('eval')[0].getElementsByTagName('entity'):
            try: gold_entity = selectNodes(para.getElementsByTagName('gold_entities')[0].getElementsByTagName('ENAMEX'), {'local_id':entity.getAttribute('goldEntity')})[0]
            except IndexError: gold_entity = None
            try: auto_entity = selectNodes(para.getElementsByTagName('auto_entities')[0].getElementsByTagName('ENAMEX'), {'local_id':entity.getAttribute('autoEntity')})[0]
            except IndexError: auto_entity = None
            span = entity.getAttribute('span'); type = entity.getAttribute('type'); ref_detection = entity.getAttribute('known'); resolution = entity.getAttribute('resolution')
            #if (options.type != None and entity.getAttribute('entity_type') == options.type) or options.type == None:
            # INCREMENT EXACT/MISSED/FALSE DETECTIONS
            # spans: exact, false_or_partial, missed, missed_or_partial, null
            # types: correct, wrong, null
            if span == 'exact' and type == 'correct':
                counts['detection']['rr'] += 1
            elif span == 'exact' and type != 'correct':
                counts['detection']['missed'] += 1
            elif span == 'missed' or span == 'missed_or_partial':
                counts['detection']['missed'] += 1
            elif span == 'false_match' or span == 'false_or_partial':
                counts['detection']['false'] += 1
            # INCREMENT REFERENCE DETECTIONS: retrieved = kk + uk; relevant = kk + ku; rr = kk
            # ref detection: correct_known, correct_unknown, false_known, false_unknown
            if ref_detection == 'correct_known':#kk
                counts['ref_detection']['retrieved'] += 1
                counts['ref_detection']['relevant'] += 1
                counts['ref_detection']['rr'] += 1
                counts['resolution']['all'] += 1
            elif ref_detection == 'correct_unknown':#uu
                pass
            elif ref_detection == 'false_known':#uk
                counts['ref_detection']['retrieved'] += 1
                counts['ref_detection']['false'] += 1
            elif ref_detection == 'false_unknown':#ku
                counts['ref_detection']['relevant'] += 1
                counts['ref_detection']['missed'] += 1
            # INCREMENT RESOLUTION: all = kk; good = 'correct'
            if resolution == 'correct':
                    counts['resolution']['correct'] += 1

            if options.logs != None:    
                # spans: exact, false_or_partial, missed, missed_or_partial, null
                try: logs['spans'][span].append(tuple([gold_entity, auto_entity]))
                except KeyError: pass
                # types: correct, wrong, null
                try: logs['types'][type].append(tuple([gold_entity, auto_entity]))
                except KeyError: pass
                # ref detection: correct_known, correct_unknown, false_known, false_unknown
                # resolution: correct, wrong, null
                if ref_detection == 'correct_known':
                    logs['ref_detection']['kk-uu'].append(tuple([gold_entity, auto_entity]))
                    logs['resolution']['kk'].append(tuple([gold_entity, auto_entity]))
                elif ref_detection == 'correct_unknown':
                    logs['ref_detection']['kk-uu'].append(tuple([gold_entity, auto_entity]))
                    logs['resolution']['uu'].append(tuple([gold_entity, auto_entity]))
                elif ref_detection == 'false_known':
                    logs['ref_detection']['ku-uk'].append(tuple([gold_entity, auto_entity]))
                    logs['resolution']['uk'].append(tuple([gold_entity, auto_entity]))
                elif ref_detection == 'false_unknown':
                    logs['ref_detection']['ku-uk'].append(tuple([gold_entity, auto_entity]))
                    logs['resolution']['ku'].append(tuple([gold_entity, auto_entity]))

    if options.logs != None:
        print >> sys.stderr, "Writing logs in %s..."%(options.logs)
        makeLists(logs, options.logs)
    print >> sys.stderr, "Computing scores..."
    delim = "------------"
    sys.stdout.write("|%s+%s+%s+%s+%s+%s+%s+%s+%s|\n"%("-----------------", delim, delim, delim, delim, delim, delim, delim, delim))
    sys.stdout.write("| %-15s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s |\n"%("Retr.-Rel. Type", "Relevant", "Missed", "False", "Retr.", "Retr.-Rel.", "Precision", "Recall", "Fscore"))
    sys.stdout.write("|%s+%s+%s+%s+%s+%s+%s+%s+%s|\n"%("-----------------", delim, delim, delim, delim, delim, delim, delim, delim))

    # COMPUTE P/R/F
    # DETECTION
    detection_precision = counts['detection']['rr'] / counts['detection']['retrieved']
    detection_recall = counts['detection']['rr'] / counts['detection']['relevant']
    detection_f = (2 * detection_precision * detection_recall) / (detection_precision + detection_recall)

    # DEBUG
    # print counts
    # print """detection_precision
    # = counts['detection']['rr'] / counts['detection']['retrieved']
    # = %f / %f
    # = %f
    # """%(counts['detection']['rr'], counts['detection']['retrieved'], counts['detection']['rr'] / counts['detection']['retrieved'])
    # print """detection_recall
    # = counts['detection']['rr'] / counts['detection']['relevant']
    # = %f / %f
    # = %f
    # """%(counts['detection']['rr'], counts['detection']['relevant'], counts['detection']['rr'] / counts['detection']['relevant'])
    # print """detection_f
    # = (2 * detection_precision * detection_recall) / (detection_precision + detection_recall)
    # = (2 * %f * %f) / (%f + %f)
    # = %f
    # """%(detection_precision, detection_recall, detection_precision, detection_recall, detection_f)
    sys.stdout.write("| %-15s | %-10d | %-10d | %-10d | %-10d | %-10d | %-10f | %-10f | %-10f |\n"%("Detection", counts['detection']['relevant'], counts['detection']['missed'], counts['detection']['false'], counts['detection']['retrieved'], counts['detection']['rr'], detection_precision, detection_recall, detection_f))
    sys.stdout.write("|%s+%s+%s+%s+%s+%s+%s+%s+%s|\n"%("-----------------", delim, delim, delim, delim, delim, delim, delim, delim))

    # REFERENCE DETECTION
    ref_detection_precision = counts['ref_detection']['rr'] / counts['ref_detection']['retrieved']
    ref_detection_recall = counts['ref_detection']['rr'] / counts['ref_detection']['relevant']
    ref_detection_f = (2 * ref_detection_precision * ref_detection_recall) / (ref_detection_precision + ref_detection_recall)

    sys.stdout.write("| %-15s | %-10d | %-10d | %-10d | %-10d | %-10d | %-10f | %-10f | %-10f |\n"%("Ref. Detection", counts['ref_detection']['relevant'], counts['ref_detection']['missed'], counts['ref_detection']['false'], counts['ref_detection']['retrieved'], counts['ref_detection']['rr'], ref_detection_precision, ref_detection_recall, ref_detection_f))
    sys.stdout.write("|%s+%s+%s+%s+%s+%s+%s+%s+%s|\n"%("-----------------", delim, delim, delim, delim, delim, delim, delim, delim))

    # RESOLUTION
    resolution_accuracy = counts['resolution']['correct'] / counts['resolution']['all']
    sys.stdout.write("| %-15s | %-10d | %-10s | %-10s | %-10s | %-10d | %-10f | %-10s | %-10s |\n"%("Resolution", counts['resolution']['all'], '-', '-', '-', counts['resolution']['correct'], resolution_accuracy, '-', '-'))
    sys.stdout.write("|%s+%s+%s+%s+%s+%s+%s+%s+%s|\n"%("-----------------", delim, delim, delim, delim, delim, delim, delim, delim))
    
def selectNodes(nodeList, attributes):
    nodes = []
    for node in nodeList:
        for (attribute, value) in attributes.items():
            if (value != None and node.getAttribute(attribute) == value) or value == None:
                nodes.append(node)
    return nodes

def makeLists(logs, file):
    impl = getDOMImplementation()
    xml = impl.createDocument(None, 'evaluation_logs', None)
    root = xml.documentElement

    for category in ['spans', 'types', 'ref_detection', 'resolution']:
        category_node = createElementNode(xml, category.upper())
        # print >> sys.stderr, "Making Category Node: %s"%(category.upper())
        for case in logs[category].keys():
            case_node = createElementNode(xml, case.upper())
            # print >> sys.stderr, "\tMaking Case Node: %s"%(case.upper())
            for pair in logs[category][case]:
                # print >> sys.stderr, "\t\tMaking Pair Nodes: Gold & Auto"
                gold_container_node = createElementNode(xml, 'GOLD')
                auto_container_node = createElementNode(xml, 'AUTO')
                (gold_node, auto_node) = pair
                container_node = createElementNode(xml, 'COUPLE')
                container_is_set = False
                if gold_node != None:
                    # print "Gold parent parent (para): ",; print gold_node.parentNode.parentNode
                    # print "Gold parent parent (item): ",; print gold_node.parentNode.parentNode.parentNode
                    para_rank = gold_node.parentNode.parentNode.getAttribute('rank')
                    item_id = gold_node.parentNode.parentNode.parentNode.getAttribute('ref')
                    # print "GOLD\tPara: %s Item: %s"%(para_rank, item_id)
                    addAttribute2Element(xml, container_node, 'item', item_id)
                    addAttribute2Element(xml, container_node, 'para', para_rank)
                    container_is_set = True
                    gold_container_node.appendChild(gold_node.cloneNode(1))
                    # print >> sys.stderr, "\t\t\tAppending Gold Node to Gold Container"
                if auto_node != None:
                    if container_is_set == False:
                        # print "Auto parent parent (para): ",; print auto_node.parentNode.parentNode
                        # print "Auto parent parent parent (item): ",; print auto_node.parentNode.parentNode.parentNode                        
                        para_rank = auto_node.parentNode.parentNode.getAttribute('rank')
                        item_id = auto_node.parentNode.parentNode.parentNode.getAttribute('ref')
                        # print "AUTO\tPara: %s Item: %s"%(para_rank, item_id)
                        addAttribute2Element(xml, container_node, 'item', item_id)
                        addAttribute2Element(xml, container_node, 'para', para_rank)
                    auto_container_node.appendChild(auto_node.cloneNode(1))
                    # print >> sys.stderr, "\t\t\tAppending Auto Node to Auto Container"; raw_input()
                container_node.appendChild(gold_container_node); # print >> sys.stderr, "\t\tAppending Gold Container to Case Node"
                container_node.appendChild(auto_container_node); # print >> sys.stderr, "\t\tAppending Auto Container to Case Node"
                case_node.appendChild(container_node)
                # print >> sys.stderr, case_node.toxml(); raw_input()
            category_node.appendChild(case_node); # print >> sys.stderr, "\tAppending Case Node to Category Node"
        root.appendChild(category_node); # print >> sys.stderr, "Appending Category Node to Root"; raw_input()
        # print >> sys.stderr, root.toxml(); raw_input()

    F = codecs.open(file, 'w', 'utf-8')
    F.write(xml.toprettyxml())
    F.close()
    print >> sys.stderr, "Done."

# DOM FUNCTIONS
def createElementNode(dom, tag_name, attributes_and_values=None, data=None):
    new_element = dom.createElement(tag_name)
    if attributes_and_values:
        for att_name in attributes_and_values.keys():
            if attributes_and_values[att_name] != None and attributes_and_values[att_name] != '':
                addAttribute2Element(dom, new_element, att_name, attributes_and_values[att_name])
    if data:
        data = re.sub('&amp;', '&', data)
        data = re.sub('&', '&amp;', data)
        addData2Element(dom, new_element, data)
    return new_element

def addAttribute2Element(dom, node, att_name, att_value):
    att = dom.createAttribute(att_name)
    att_node = node.setAttributeNode(att)
    node.setAttribute(att_name, att_value)

def addData2Element(dom, node, data):
    text = dom.createTextNode(data)
    new_node = node.appendChild(text)

def getOptions():
    usage = '\n\n\t%prog [options] <eval.xml>'
    parser = OptionParser(usage)
    type_choices = ['Person', 'Location', 'Organization', 'Company']
    # parser.add_option("--type", action="store", dest="type", type="choice", choices=type_choices, default=None, help="restrict to <type>")
    parser.add_option("--logs", action="store", dest="logs", default=None, help="output file for lists")
    try: (options, args) = parser.parse_args()
    except OptionError: sys.stderr.write('OptionError'); parser.print_help(); sys.exit()
    except: sys.stderr.write('An error occured during options parsing\n'); parser.print_help(); sys.exit()
    # if options.logs == None and options.type == None: sys.stderr.write('You must specify the output file for lists and/or the type to evaluate\n'); parser.print_help(); sys.exit()
    if len(args) != 1: parser.print_help(); sys.exit()
    return options, args

if __name__ == "__main__":
    main()
