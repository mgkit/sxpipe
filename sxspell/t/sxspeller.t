#!/usr/bin/perl
# $id$

use Test::More;
use strict;

# compute plan
my $srcdir = `echo -n \$srcdir`;

open(DATA, "$srcdir/t/sxspeller.data") or die "unable to open $srcdir/t/sxspeller.data: $!";
my @data = <DATA>;
close(DATA);
my $testsnum=0;
foreach my $line (@data) {
    if ($line!~/^\#/) {$testsnum++}
}
plan tests => $testsnum;

foreach my $line (@data) {
    next if ($line=~/^\#/);
    chomp $line;
    my ($input, $expected_word, $options) = split(/\t/, $line);
    my $prog="";
    if ($options eq "UW") {$options="-cl -pl -cq -ch -pp -ps -c -nsc -d"}
    elsif ($options eq "CORR") {$options="-cl -pl -cq -ch -pp -ps -c -lw 50 -t -sc -mw 300 -sww 100"}
    else {$options="";}
    my $output = `echo "$input" | src/sxspeller $options`;
    chomp $output;
    $output=~s/ $//;
    $output=~s/  +/ /g;
    is($output, $expected_word, "correction of \"$input\""); 
}
