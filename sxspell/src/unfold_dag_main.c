/* ********************************************************
   *							  *
   *							  *
   * Copyright (c) 2005 by Institut National de Recherche *
   *                    en Informatique et en Automatique *
   *							  *
   *							  *
   ******************************************************** */




/* ********************************************************
   *							  *
   *  Produit de l'equipe ATOLL.			  *
   *							  *
   ******************************************************** */





/* Main module for a dag unfolder (dag->udag) */


/************************************************************************/
/* Historique des modifications, en ordre chronologique inverse:	*/
/************************************************************************/
/* 8 nov 2006 (pb+bs):	On ne laisse que ce qui m�rite d'y �tre         */
/************************************************************************/
/* date myst�rieuse vers 2005 (bs): Vampirisation de earley_dag_main.c  */
/************************************************************************/


#define WHAT	"@(#)unfold_dag_main.c\t- SYNTAX [unix] - 23 mars 2004"
static struct what {
  struct what	*whatp;
  char		what [sizeof (WHAT)];
} what = {&what, WHAT};


#include "sxunix.h"
#include "earley.h"

/* Le source est un vrai dag */
/* Le source est stocke' ds les structures de... */
#include "dag_scanner.h"

/* On lit a priori sur stdin, et cetera */

FILE	*sxstdout = {NULL}, *sxstderr = {NULL};
FILE	*sxtty;
extern struct sxtables	dummy_tables;

BOOLEAN        (*main_parser)() = NULL;

/*---------------*/
/*    options    */
/*---------------*/

BOOLEAN		sxverbosep;
static BOOLEAN  is_help;
static char	ME [] = "unfold_dag_main";
static char	Usage [] = "\
Usage:\t%s [options] [files]\n\
options=\t-v, -verbose, -nv, -noverbose,\n\
";

#define OPTION(opt)	(1 << (opt - 1))
#define noOPTION(opt)	(~OPTION (opt))

#define OPT_NB ((sizeof(option_tbl)/sizeof(*option_tbl))-1)

#define UNKNOWN_ARG 	  0
#define VERBOSE 	  1
#define SOURCE_FILE	  2
#define LAST_OPTION	  SOURCE_FILE


static char	*option_tbl [] = {
    "",
    "v", "verbose", "nv", "noverbose",
};

static SXINT	option_kind [] = {
    UNKNOWN_ARG,
    VERBOSE, VERBOSE, -VERBOSE, -VERBOSE,
};

static SXINT	option_get_kind (arg)
    char	*arg;
{
  char	**opt;

  if (*arg++ != '-')
    return SOURCE_FILE;

  for (opt = &(option_tbl [OPT_NB]); opt > option_tbl; opt--) {
    if (strcmp (*opt, arg) == 0 /* egalite */ )
      break;
  }

  return option_kind [opt - option_tbl];
}



static char	*option_get_text (kind)
    SXINT	kind;
{
  SXINT	i;

  for (i = OPT_NB; i > 0; i--) {
    if (option_kind [i] == kind)
      break;
  }

  return option_tbl [i];
}


static	unfold_dag_run (pathname)
    char	*pathname;
{
  FILE	*infile;

  if (pathname == NULL) {
    SXINT	c;

    if (sxverbosep) {
      fputs ("\"stdin\":\n", sxtty);
    }

    if ((infile = sxtmpfile ()) == NULL) {
      fprintf (sxstderr, "%s: Unable to create ", ME);
      sxperror ("temp file");
      sxerrmngr.nbmess [2]++;
      return;
    }

    while ((c = getchar ()) != EOF) {
      putc (c, infile);
    }

    rewind (infile);
    sxsrc_mngr (INIT, infile, "");
  }
  else {
    if ((infile = sxfopen (pathname, "r")) == NULL) {
      fprintf (sxstderr, "%s: Cannot open (read) ", ME);
      sxperror (pathname);
      sxerrmngr.nbmess [2]++;
      return;
    }
    else {
      if (sxverbosep) {
	fprintf (sxtty, "%s:\n", pathname);
      }

      sxsrc_mngr (INIT, infile, pathname);
    }
  }

  sxerr_mngr (BEGIN);

  dag_kind = DAG;

  dag_scanner (OPEN, NULL);
  dag_scanner (INIT, NULL);

  {
    SXINT  severity;
    char mess [32];

    severity = dag_scanner (ACTION, NULL);

  }

  dag_scanner (CLOSE, NULL);
  dag_scanner (END, NULL);

  sxerr_mngr (END);
  fclose (infile);
  sxsrc_mngr (FINAL);

}

/************************************************************************/
/* main function
/************************************************************************/
int
main (argc, argv)
    SXINT		argc;
    char	*argv [];
{
  SXINT		argnum, i, j;
  BOOLEAN	in_options, is_source_file, is_stdin, failed;

  /* C'est l'utilisateur du scanneur qui decide du type de foreach qu'il veut realiser sur le dag */ 
  static SXINT foreach [6] = {1, 0, 1, 0, 0, 0};

  /*  dag_foreach = foreach; */
  dag_foreach [0] = foreach [0];
  dag_foreach [1] = foreach [1];
  dag_foreach [2] = foreach [2];
  dag_foreach [3] = foreach [3];
  dag_foreach [4] = foreach [4];
  dag_foreach [5] = foreach [5];

  if (sxstdout == NULL) {
    sxstdout = stdout;
  }
  if (sxstderr == NULL) {
    sxstderr = stderr;
  }
  
#ifdef BUG
  /* Suppress bufferisation, in order to have proper	 */
  /* messages when something goes wrong...		 */
  setbuf (stdout, NULL);
#endif
  
  sxopentty ();

  /* valeurs par defaut */
  sxverbosep = TRUE;

  is_stdin = TRUE;

  if (argc > 1) {
    is_source_file = FALSE;

    /* Decodage des options */
    in_options = TRUE;
    argnum = 0;

    while (in_options && ++argnum < argc) {
      switch (option_get_kind (argv [argnum])) {
      case VERBOSE:
	sxverbosep = TRUE;
	break;

      case -VERBOSE:
	sxverbosep = FALSE;
	break;

      case SOURCE_FILE:
	if (is_stdin) {
	  is_stdin = FALSE;
	}

	is_source_file = TRUE;
	in_options = FALSE;
	break;

      case UNKNOWN_ARG:
	fprintf (sxstderr, "%s: unknown option \"%s\".\n", ME, argv [argnum]);
	SXEXIT (3);
      }
    }
  }

  if (!is_stdin && !is_source_file || is_help) {
    fprintf (sxstderr, Usage, ME);
    SXEXIT (3);
  }

  sxstr_mngr (BEGIN);

  if (is_stdin) {
    unfold_dag_run (NULL);
  }
  else {
    do {
      SXINT	severity = sxerrmngr.nbmess [2];

      sxerrmngr.nbmess [2] = 0;
      sxstr_mngr (BEGIN) /* remise a vide de la string table */ ;
      unfold_dag_run (argv [argnum++]);
      sxerrmngr.nbmess [2] += severity;
    } while (argnum < argc);
  }

  sxstr_mngr (END);

  {
    SXINT	severity;

    for (severity = SEVERITIES - 1; severity > 0 && sxerrmngr.nbmess [severity] == 0; severity--)
      ;

    SXEXIT (severity);
  }
}

SXINT
sxivoid ()
/* procedure ne faisant rien */
{
  return 1;
}
