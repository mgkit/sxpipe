/* ********************************************************
   *							  *
   *							  *
   * Copyright (c) 2004 by Institut National de Recherche *
   *                    en Informatique et en Automatique *
   *							  *
   *							  *
   ******************************************************** */




/* ********************************************************
   *							  *
   *  Produit de l'equipe ATOLL.                 	  *
   *							  *
   ******************************************************** */

/* Programme test de sxspell */


/************************************************************************/
/* Historique des modifications, en ordre chronologique inverse:	*/
/************************************************************************/
/* mercredi 25 Aout 2004 (pb):	Ajout de cette rubrique "modifications"	*/
/************************************************************************/


#define WHAT	"@(#)sxspell_main.c\t- SYNTAX [unix] -  mercredi 25 Aout 2004"
static struct what {
  struct what	*whatp;
  char		what [sizeof (WHAT)];
} what = {&what, WHAT};


#include "sxunix.h"
#include "sxspell.h"

#define def_sxdfa_comb_inflected_form

#define sxdfa_comb_inflected_form             dico
#include dico_mots_h


/* On lit a priori sur stdin, et cetera */

FILE	          *sxstdout, *sxstderr;
FILE	          *sxtty;

static SXINT        run_kind, max_weight, sub_word_weight; 
static SXBOOLEAN    verbose, is_sub_word; 


/*---------------*/
/*    options    */
/*---------------*/

static char	ME [] = "sxspell_main";
static char	Usage [] = "\
Usage:\t%s [options] word\n\
options=\
\t--help,\n\
\t\t-v, -verbose, -nv (default), -noverbose,\n\
\t\t-k kind_val (0 => ALL, 1 => BEST (default), >1 => NBEST), -kind kind_val,\n\
\t\t-mw weight_val (default == UNBOUNDED), -max_weight weight_val,\n\
\t\t-sww weight_val (default == no sub_word seek), -sub_word_weight weight_val,\n";

#define OPTION(opt)	(1 << (opt - 1))
#define noOPTION(opt)	(~OPTION (opt))

#define OPT_NB ((sizeof(option_tbl)/sizeof(*option_tbl))-1)

#define UNKNOWN_ARG 		0
#define HELP 		        1
#define VERBOSE 		2
#define KIND		        3
#define WEIGHT	                4
#define SUB_WORD_WEIGHT	        5
#define LAST_OPTION		SUB_WORD_WEIGHT

static char	*option_tbl [] = {"",
				  "-help",
				  "v", "verbose", "nv", "noverbose",
				  "k", "kind",
				  "mw", "max_weight",
				  "sww", "sub_word_weight",
};

static SXINT	option_kind [] = {UNKNOWN_ARG,
				  HELP,
				  VERBOSE, VERBOSE, -VERBOSE, -VERBOSE,
				  KIND, KIND,
				  WEIGHT, WEIGHT,
				  SUB_WORD_WEIGHT, SUB_WORD_WEIGHT,
};

static SXINT	option_get_kind (arg)
    char	*arg;
{
    char	**opt;

    if (*arg++ != '-')
	return UNKNOWN_ARG;

    for (opt = &(option_tbl [OPT_NB]); opt > option_tbl; opt--) {
	if (strcmp (*opt, arg) == 0 /* egalite */ )
	    break;
    }

    return option_kind [opt - option_tbl];
}


static char	*option_get_text (kind)
    SXINT	kind;
{
    SXINT	i;

    for (i = OPT_NB; i > 0; i--) {
	if (option_kind [i] == kind)
	    break;
    }

    return option_tbl [i];
}

int
main (argc, argv)
    SXINT		argc;
    char	*argv [];
{
  SXINT	argnum, kind;

  if (sxstdout == NULL) {
    sxstdout = stdout;
  }

  if (sxstderr == NULL) {
    sxstderr = stderr;
  }

  /* valeurs par defaut */
  run_kind = 1; /* best */
  max_weight = UNBOUNDED; /* on regarde toutes les corrections */
  verbose = SXFALSE; /* sobre */
  is_sub_word = SXFALSE; /* On ne cherche pas a decouper en sous_mots */
  sub_word_weight = 0; /* Poids de la correction pour un decoupage */

  /* Decodage des options */
  kind = HELP;

  for (argnum = 1; argnum < argc; argnum++) {
    switch (kind = option_get_kind (argv [argnum])) {
    case HELP:
      fprintf (sxstderr, Usage, ME);
      SXEXIT (SXSEVERITIES);

    case VERBOSE:
      verbose = SXTRUE;
      break;

    case -VERBOSE:
      verbose = SXFALSE;
      break;

    case KIND:
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (KIND));
	SXEXIT (3);
      }
	     
      run_kind = atoi (argv [argnum]);

      break;

    case WEIGHT:
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (WEIGHT));
	SXEXIT (3);
      }
	     
      max_weight = atoi (argv [argnum]);

      break;

    case SUB_WORD_WEIGHT:
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (SUB_WORD_WEIGHT));
	SXEXIT (3);
      }
	     
      is_sub_word = SXTRUE;
      sub_word_weight   = atoi (argv [argnum]);

      break;

    case UNKNOWN_ARG:
      if (argnum < argc-1) {
	fprintf (sxstderr, "%s: unknown option \"%s\";\n", ME, argv [argnum]);
	fprintf (sxstderr, Usage, ME);
	SXEXIT (SXSEVERITIES);
      }

      break;
    }

    if (kind == UNKNOWN_ARG)
      break;
  }


  if (kind != UNKNOWN_ARG) {
    /* Il manque le mot a corriger ... */
    fprintf (sxstderr, Usage, ME);
    SXEXIT (SXSEVERITIES);
  }
  
  sxspell_model (run_kind, max_weight, is_sub_word, sub_word_weight, argv [argnum], &dico);
  SXEXIT (0);
}
