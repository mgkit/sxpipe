/* ********************************************************
   *							  *
   *							  *
   *      (c) 2002-2008 by Institut National de Recherche *
   *                    en Informatique et en Automatique *
   *							  *
   *							  *
   ******************************************************** */




/* ********************************************************
   *							  *
   *  Produit de l'equipe Alpage (anciennement Atoll)     *
   *							  *
   ******************************************************** */



#include "text_t.h" /* inclut sxunix.h */
#include "sxspell.h"

char WHAT_TEXT2DAG_MAIN [] = "@(#)TEXT2DAG - $Id$";

SXINT     max_weight, sub_word_weight, light_weight, super_light_weight, max_corr_nb, max_corr_weight_nb; 
SXBOOLEAN is_sub_word, print_comment, check_lower, is_tokenizer, is_process_lower, is_cut_on_quote; 
SXBOOLEAN process_prefix, process_suffix, is_cut_on_hyphen, is_light_correction, is_super_light_correction, is_spelling_correction, is_glue, is_output_uw, is_output_weights, is_change_only_case_and_diacritics, is_keep_original_tokens;
FILE *trace_file;

/* On est dans un cas "mono-langage": */

//extern struct sxtables	sxtables;

static char	ME [] = "text2dag";
static char	Usage [] = "\
Usage:\t%s [options] [files]\n\
options=\t-v, -verbose, -nv, -noverbose,\n\
\t\t-, -stdin,\n\
\t\t--help,\n\
\t\t-mw weight_val (default == UNBOUNDED), -max_weight weight_val,\n\
\t\t-sww weight_val (default == no sub_word seek), -sub_word_weight weight_val,\n\
\t\t-mcn corr_nb (default == 1), -max_corr_nb corr_nb,\n\
\t\t-mcwn corr_weight_nb (default == 1), -max_corr_weight_nb corr_nb,\n\
\t\t-cl, -check_lower,\n\
\t\t-pl, -process_lower,\n\
\t\t-cq, -cut_on_quote,\n\
\t\t-ch, -cut_on_hyphen,\n\
\t\t-pp, -process_prefix,\n\
\t\t-ps, -process_suffix,\n\
\t\t-lw w, -light_weight weight-value,\n\
\t\t-slw w, -super_light_weight weight-value,\n\
\t\t-sc -spelling_correction (default), -nsc -no_spelling_correction,\n\
\t\t-g, -glue,\n\
\t\t-uw,\n\
\t\t-ow,\n\
\t\t-t trace_file, -trace trace_file,\n\
\t\t-ccd,\n\
";

#define OPTION(opt)	(1 << (opt - 1))
#define noOPTION(opt)	(~OPTION (opt))

#define OPT_NB ((sizeof(option_tbl)/sizeof(*option_tbl))-1)

#define UNKNOWN_ARG 	  0
#define VERBOSE 	  1
#define HELP	          2
#define STDIN	  	  3
#define WEIGHT	                4
#define SUB_WORD_WEIGHT	        5
#define MAX_CORR_NB	        6
#define MAX_CORR_WEIGHT_NB	7
#define CHECK_LOWER	        8
#define PROCESS_LOWER	        9
#define CUT_ON_QUOTE	        10
#define CUT_ON_HYPHEN	        11
#define PROCESS_PREFIX	        12
#define PROCESS_SUFFIX	        13
#define LIGHT_WEIGHT	        14
#define SUPER_LIGHT_WEIGHT	15
#define SPELLING_CORRECTION     16
#define GLUE	                17
#define UW	                18
#define OUTPUT_WEIGHTS          19
#define TRACE	                20
#define CASE_DIACRITICS         21
#define KEEP_ORIG_TOKEN         22
#define SOURCE_FILE	  23
#define LAST_OPTION		SOURCE_FILE

static char	*option_tbl [] = {
  "",
  "v", "verbose", "nv", "noverbose",
  "-help",
  "stdin",
  "mw", "max_weight",
  "sww", "sub_word_weight",
  "mcn", "max_corr_nb",
  "mcwn", "max_corr_weight_nb",
  "cl", "check_lower",
  "pl", "process_lower",
  "cq", "cut_on_quote",
  "ch", "cut_on_hyphen",
  "pp", "process_prefix",
  "ps", "process_suffix",
  "lw", "light_weight",
  "slw", "super_light_weight",
  "sc", "spelling_correction", "nsc", "no_spelling_correction",
  "g", "glue",
  "uw",
  "ow", "output_weights",
  "t", "trace",
  "ccd", "limit_corrections_to_case_and_diacritic_modification",
  "kot", "keep_original_tokens",
};

static SXINT	option_kind [] = {
  UNKNOWN_ARG,
  VERBOSE, VERBOSE, -VERBOSE, -VERBOSE,
  HELP,
  STDIN,
  WEIGHT, WEIGHT,
  SUB_WORD_WEIGHT, SUB_WORD_WEIGHT,
  MAX_CORR_NB, MAX_CORR_NB,
  MAX_CORR_WEIGHT_NB, MAX_CORR_WEIGHT_NB,
  CHECK_LOWER, CHECK_LOWER,
  PROCESS_LOWER, PROCESS_LOWER,
  CUT_ON_QUOTE, CUT_ON_QUOTE,
  CUT_ON_HYPHEN, CUT_ON_HYPHEN,
  PROCESS_PREFIX, PROCESS_PREFIX,
  PROCESS_SUFFIX, PROCESS_SUFFIX,
  LIGHT_WEIGHT, LIGHT_WEIGHT,
  SUPER_LIGHT_WEIGHT, SUPER_LIGHT_WEIGHT,
  SPELLING_CORRECTION, SPELLING_CORRECTION, -SPELLING_CORRECTION, -SPELLING_CORRECTION,
  GLUE, GLUE,
  UW,
  OUTPUT_WEIGHTS, OUTPUT_WEIGHTS,
  TRACE, TRACE,
  CASE_DIACRITICS, CASE_DIACRITICS,
  KEEP_ORIG_TOKEN, KEEP_ORIG_TOKEN,
};

static SXINT	option_get_kind (arg)
    char	*arg;
{
  char	**opt;

  if (*arg++ != '-')
    return SOURCE_FILE;

  if (*arg == SXNUL)
    return STDIN;

  for (opt = &(option_tbl [OPT_NB]); opt > option_tbl; opt--) {
    if (strcmp (*opt, arg) == 0 /* egalite */ )
      break;
  }

  return option_kind [opt - option_tbl];
}


static char	*option_get_text (kind)
    SXINT	kind;
{
  SXINT	i;

  for (i = OPT_NB; i > 0; i--) {
    if (option_kind [i] == kind)
      break;
  }

  return option_tbl [i];
}

int
main (argc, argv)
    int		argc;
    char	*argv [];
{
  SXINT	argnum, option;
  FILE	*infile = NULL;
  SXBOOLEAN no_input_yet = SXTRUE;
  SXINT tmax = -sxtables.SXP_tables.P_tmax;

  if (sxstdout == NULL) {
    sxstdout = stdout;
  }
  if (sxstderr == NULL) {
    sxstderr = stderr;
  }

#ifdef BUG
  /* Suppress bufferisation, in order to have proper	 */
  /* messages when something goes wrong...		 */
  setbuf (stdout, NULL);
#endif /* BUG */

  /* valeurs par defaut */
  sxverbosep = SXFALSE;

  sxopentty ();
  sxstr_mngr (SXBEGIN) /* Creation de la table des chaines */ ;
  (*(sxtables.analyzers.parser)) (SXBEGIN) /* Allocation des variables globales du parser */ ;
  syntax (SXOPEN, &sxtables) /* Initialisation de SYNTAX (mono-langage) */ ;

  /* valeurs par defaut */
  max_weight = UNBOUNDED; /* on regarde toutes les corrections */
  is_sub_word = SXFALSE; /* On ne cherche pas a decouper en sous_mots */
  sub_word_weight = 0; /* Poids de la correction pour un decoupage */
  max_corr_nb = 1; /* Par défaut, la correction est déterministe */
  max_corr_weight_nb = 1; /* Par défaut, la correction ne rend que des réponses de poids optimal */
  check_lower = SXFALSE; /* On ne cherche pas le mot en minuscule */
  is_process_lower = SXFALSE;
  is_cut_on_quote = SXFALSE;
  is_cut_on_hyphen = SXFALSE;
  process_prefix = SXFALSE;
  process_suffix = SXFALSE;
  is_light_correction = SXFALSE; /* pas de correction "light" */
  light_weight = 0; 
  super_light_weight = 0; 
  is_spelling_correction = SXTRUE; /* Si tout a echoue', on tente une correction complete */
  is_glue = SXFALSE; /* SXTRUE => On essaie une correction en recollant les mots successifs */
  is_output_uw = SXFALSE;
  is_output_weights = SXFALSE;
  trace_file = NULL; /* si -t On sort les corrections dans le fichier donné en argument */
  is_change_only_case_and_diacritics = SXFALSE;

  /* valeur non-modifiables, héritées de sxspeller */
  is_tokenizer = SXTRUE;
  print_comment = SXTRUE;

  
  /* Tous les arguments sont des noms de fichiers, "-" signifiant stdin */
  for (argnum = 1; argnum < argc; argnum++) {
    switch (option = option_get_kind (argv [argnum])) {
    case VERBOSE:
      sxverbosep = SXTRUE;
      break;

    case -VERBOSE:
      sxverbosep = SXFALSE;
      break;

    case HELP:
      fprintf (sxstderr, Usage, ME);
      SXEXIT (3);

    case STDIN:
    case SOURCE_FILE:
      if (option == SOURCE_FILE && (infile = sxfopen (argv [argnum], "r")) == NULL) {
	fprintf (sxstderr, "%s: Cannot open (read) ", argv [0]);
	sxperror (argv [argnum]);
	sxerrmngr.nbmess [SXSEVERITIES - 1]++;
      }
      else {
	no_input_yet = SXFALSE;

	if (sxverbosep)
	  fprintf (sxtty, "%s:\n", option == SOURCE_FILE ? argv [argnum] : "stdin");

	if (option == SOURCE_FILE)
	  sxsrc_mngr (SXINIT, infile, argv [argnum]);
	else
	  sxsrc_mngr (SXINIT, stdin, "");

	sxerr_mngr (SXBEGIN);
	syntax (SXACTION, &sxtables);
	sxsrc_mngr (SXFINAL);
	sxerr_mngr (SXEND);
	if (option == SOURCE_FILE)
	  fclose (infile);
      }
      break;

    case WEIGHT:
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (WEIGHT));
	SXEXIT (3);
      }
	     
      max_weight = atoi (argv [argnum]);

      break;

    case SUB_WORD_WEIGHT:
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (SUB_WORD_WEIGHT));
	SXEXIT (3);
      }
	     
      is_sub_word = SXTRUE;
      sub_word_weight = atoi (argv [argnum]);

      break;

    case MAX_CORR_NB:
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (MAX_CORR_NB));
	SXEXIT (3);
      }
	     
      max_corr_nb = atoi (argv [argnum]);

      break;

    case MAX_CORR_WEIGHT_NB:
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (MAX_CORR_WEIGHT_NB));
	SXEXIT (3);
      }
	     
      max_corr_weight_nb = atoi (argv [argnum]);

      break;

    case CHECK_LOWER:
      check_lower = SXTRUE;
      break;

    case PROCESS_LOWER:
      is_process_lower = SXTRUE;
      break;

    case CUT_ON_QUOTE:
      is_cut_on_quote = SXTRUE;
      break;

    case CUT_ON_HYPHEN:
      is_cut_on_hyphen = SXTRUE;
      break;

    case PROCESS_PREFIX:
      process_prefix = SXTRUE;
      break;

    case PROCESS_SUFFIX:
      process_suffix = SXTRUE;
      break;

    case LIGHT_WEIGHT:
      is_light_correction = SXTRUE;
      
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (LIGHT_WEIGHT));
	SXEXIT (3);
      }
	     
      light_weight = atoi (argv [argnum]);
      break;

    case SUPER_LIGHT_WEIGHT:
      is_super_light_correction = SXTRUE;
      
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (SUPER_LIGHT_WEIGHT));
	SXEXIT (3);
      }
	     
      super_light_weight = atoi (argv [argnum]);
      break;

    case SPELLING_CORRECTION:
      is_spelling_correction = SXTRUE;
      break;

    case -SPELLING_CORRECTION:
      is_spelling_correction = SXFALSE;
      break;

    case GLUE:
      is_glue = SXTRUE;
      break;

    case OUTPUT_WEIGHTS:
      is_output_weights = SXTRUE;
      break;

    case UW:
      is_output_uw = SXTRUE;
      break;

    case TRACE:
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a filename must follow the \"%s\" option;\n", ME, option_get_text (TRACE));
	SXEXIT (3);
      }
	     
      if ((trace_file = sxfopen (argv [argnum], "w")) == NULL) {
	fprintf (sxstderr, "%s: could not open file \"%s\";\n", ME, argv [argnum]);
	SXEXIT (3);
      }
      break;

    case CASE_DIACRITICS:
      is_change_only_case_and_diacritics = SXTRUE;
      break;

    case KEEP_ORIG_TOKEN:
      is_keep_original_tokens = SXTRUE;
      break;

    case UNKNOWN_ARG:
      fprintf (sxstderr, "%s: unknown option \"%s\".\n", ME, argv [argnum]);
      SXEXIT (3);
    }
  }
  
  if (max_corr_weight_nb > max_corr_nb) {
    max_corr_weight_nb = max_corr_nb;
    fprintf (sxstderr, "Warning: arg of \"-mcwn\" shouldn't be greater than arg of \"-mcn\" (it leads to useless computations); we force option \"-mcwn %ld\"\n", max_corr_nb);
  }

  if (no_input_yet) {
    /* Pas de fichier donné, STDIN non explicite: on lit sur stdin */
    if (sxverbosep) {
      fputs ("\"stdin\":\n", sxtty);
    }
      
    sxsrc_mngr (SXINIT, stdin, "");
    sxerr_mngr (SXBEGIN);
    syntax (SXACTION, &sxtables);
    sxsrc_mngr (SXFINAL);
    sxerr_mngr (SXEND);
  }

  syntax (SXCLOSE, &sxtables);
  (*(sxtables.analyzers.parser)) (SXEND) /* Inutile puisque le process va etre termine */ ;
  sxstr_mngr (SXEND) /* Inutile puisque le process va etre termine */ ;

  if (infile)
    fclose (infile);

  if (trace_file)
    fclose (trace_file);

  {
    SXINT	severity;

    for (severity = SXSEVERITIES - 1; severity > 0 && sxerrmngr.nbmess [severity] == 0; severity--) {
      /* null; */
    }

    SXEXIT (severity);
  }
}

SXINT	sxvoidparser (SXINT what_to_do, struct sxtables *arg)
/* procedure ne faisant rien */
{
    return 1;
}

#if 0
VOID	sxvoid ()
/* procedure ne faisant rien */
{
}

SXINT	sxivoid ()
/* procedure ne faisant rien */
{
    return 1;
}
SXINT	sxvoiddesambig (SXINT what)
/* procedure ne faisant rien */
{
    return 1;
}
SXBOOLEAN	sxbvoid ()
/* procedure ne faisant rien */
{
    return 0;
}
#endif /* 0 */
