/* ********************************************************
   *							  *
   *							  *
   * Copyright (c) 2004 by Institut National de Recherche *
   *                    en Informatique et en Automatique *
   *							  *
   *							  *
   ******************************************************** */




/* ********************************************************
   *							  *
   *  Produit de l'equipe ATOLL.                 	  *
   *							  *
   ******************************************************** */

/* Programme qui tente de corriger les fautes d'orthographe (p.ex. apr�s OCR) */



#include "sxunix.h"
#include "varstr.h"
#include "sxstack.h"
#include "sxba.h"

char WHAT_SXSPELLER[] =	"@(#)SxPipe/sxspell - $Id$" WHAT_DEBUG;

/* INCLUSION DES REGLES DE CORRECTION */
#include "sxspell.h"
#include "sxspell_rules.h"

/*  S T A T I C     V A R I A B L E S   */
static SXINT     max_weight, sub_word_weight, light_weight; 
static SXBOOLEAN is_sub_word, print_error, print_input, print_output, print_comment, check_lower, is_tokenizer, is_process_lower, is_cut_on_quote; 
static SXBOOLEAN process_prefix, process_suffix, is_cut_on_hyphen, is_light_correction, is_spelling_correction, is_dry_run, is_glue;

/* On est dans un cas "mono-langage": */
extern struct sxtables	sxtables;

static SXBOOLEAN	is_error;

/*---------------*/
/*    options    */
/*---------------*/

static char	ME [] = "sxspeller";
static char	Usage [] = "\
Usage:\t%s [options] [source_file]\n\
options=\
\t--help,\n\
\t\t-mw weight_val (default == UNBOUNDED), -max_weight weight_val,\n\
\t\t-sww weight_val (default == no sub_word seek), -sub_word_weight weight_val,\n\
\t\t-i, -input,\n\
\t\t-o, -output,\n\
\t\t-e, -error,\n\
\t\t-c, -comment,\n\
\t\t-cl, -check_lower,\n\
\t\t-pl, -process_lower,\n\
\t\t-cq, -cut_on_quote,\n\
\t\t-ch, -cut_on_hyphen,\n\
\t\t-pp, -process_prefix,\n\
\t\t-ps, -process_suffix,\n\
\t\t-lw w, -light_weight weight-value,\n\
\t\t-sc -spelling_correction (default), -nsc -no_spelling_correction,\n\
\t\t-d, -dry_run,\n\
\t\t-g, -glue,\n\
\t\t-t, -tokenizer,\n";

#define OPTION(opt)	(1 << (opt - 1))
#define noOPTION(opt)	(~OPTION (opt))

#define OPT_NB ((sizeof(option_tbl)/sizeof(*option_tbl))-1)

#define UNKNOWN_ARG 		0
#define HELP 		        1
#define WEIGHT	                2
#define SUB_WORD_WEIGHT	        3
#define PRINT_INPUT	        4
#define PRINT_OUTPUT	        5
#define PRINT_ERROR	        6
#define CHECK_LOWER	        7
#define PROCESS_LOWER	        8
#define CUT_ON_QUOTE	        9
#define CUT_ON_HYPHEN	        10
#define PROCESS_PREFIX	        11
#define PROCESS_SUFFIX	        12
#define LIGHT_WEIGHT	        13
#define SPELLING_CORRECTION     14
#define PRINT_COMMENT	        15
#define DRY_RUN	                16
#define GLUE	                17
#define TOKENIZER	        18
#define LAST_OPTION		TOKENIZER

static char	*option_tbl [] = {"",
				  "-help",
				  "mw", "max_weight",
				  "sww", "sub_word_weight",
				  "i", "input",
				  "o", "output",
				  "e", "error",
				  "c", "comment",
				  "cl", "check_lower",
				  "pl", "process_lower",
				  "cq", "cut_on_quote",
				  "ch", "cut_on_hyphen",
				  "pp", "process_prefix",
				  "ps", "process_suffix",
				  "lw", "light_weight",
				  "sc", "spelling_correction", "nsc", "no_spelling_correction",
				  "d", "dry_run",
				  "g", "glue",
				  "t", "tokenizer",
};

static SXINT	option_kind [] = {UNKNOWN_ARG,
				  HELP,
				  WEIGHT, WEIGHT,
				  SUB_WORD_WEIGHT, SUB_WORD_WEIGHT,
				  PRINT_INPUT, PRINT_INPUT,
				  PRINT_OUTPUT, PRINT_OUTPUT,
				  PRINT_ERROR, PRINT_ERROR,
				  PRINT_COMMENT, PRINT_COMMENT,
				  CHECK_LOWER, CHECK_LOWER,
				  PROCESS_LOWER, PROCESS_LOWER,
				  CUT_ON_QUOTE, CUT_ON_QUOTE,
				  CUT_ON_HYPHEN, CUT_ON_HYPHEN,
				  PROCESS_PREFIX, PROCESS_PREFIX,
				  PROCESS_SUFFIX, PROCESS_SUFFIX,
				  LIGHT_WEIGHT, LIGHT_WEIGHT,
				  SPELLING_CORRECTION, SPELLING_CORRECTION, -SPELLING_CORRECTION, -SPELLING_CORRECTION,
				  DRY_RUN, DRY_RUN,
				  GLUE, GLUE,
				  TOKENIZER, TOKENIZER,
};

static SXINT	option_get_kind (arg)
    char	*arg;
{
    char	**opt;

    if (*arg++ != '-')
	return UNKNOWN_ARG;

    for (opt = &(option_tbl [OPT_NB]); opt > option_tbl; opt--) {
	if (strcmp (*opt, arg) == 0 /* egalite */ )
	    break;
    }

    return option_kind [opt - option_tbl];
}


static char	*option_get_text (kind)
    SXINT	kind;
{
    SXINT	i;

    for (i = OPT_NB; i > 0; i--) {
	if (option_kind [i] == kind)
	    break;
    }

    return option_tbl [i];
}

#define def_sxdfa_comb_inflected_form

/* le dico des formes flechies et des mots-composes */
extern struct sxdfa_comb cc_dico;
static struct sxdfa_comb *dico_des_mots = &(cc_dico);


#define sxdfa_comb_inflected_form             dico_prfx
#define sxdfa_comb_inflected_form_char2class  prfx_char2class
#define sxdfa_comb_inflected_form_comb_vector prfx_comb_vector

/* le dico des prefixes remarquables */
#include dico_prefix_h
static struct sxdfa_comb *dico_des_prefixes = &(dico_prfx);

#undef sxdfa_comb_inflected_form
#undef sxdfa_comb_inflected_form_char2class
#undef sxdfa_comb_inflected_form_comb_vector
#undef EOF_CODE
#undef MAKE_A_DICO_TIME
#define sxdfa_comb_inflected_form             dico_sffx
#define sxdfa_comb_inflected_form_char2class  sffx_char2class
#define sxdfa_comb_inflected_form_comb_vector sffx_comb_vector

/* le dico des suffixes remarquables */
#include dico_suffix_h
static struct sxdfa_comb *dico_des_suffixes = &(dico_sffx);

#undef sxdfa_comb_inflected_form
#undef sxdfa_comb_inflected_form_char2class
#undef sxdfa_comb_inflected_form_comb_vector
#undef EOF_CODE
#undef MAKE_A_DICO_TIME


static VARSTR        vstr, cvstr, wstr, bivstr, vstr_mot_en_min, vstr_ETR;
static char          *MOT_A_CORRIGER;

#define SXBA_fdcl(n,l)	SXBA_ELT n [SXNBLONGS (l) + 1] = {l,}
static SXBA_fdcl (MAJ_set, 256);
static SXBA_fdcl (vowell_set, 256);
static SXBA_fdcl (mpb_set, 256);

static SXBA          hyphen_set;


static char *action2prefix_chars [] = {"",
				       /* 1 */ "_",
				       /* 2 */ "-_",
				       /* 3 */ "e-_",
				       /* 4 */ "e-_",
				       /* 5 */ "_",
				       /* 6 */ "_",
};

static char *action2suffix_chars [] = {"",
				       /* 1 */ "_",
				       /* 2 */ "_",
				       /* 3 */ "_",
				       /* 4 */ "_",
				       /* 5 */ "_",
				       /* 6 */ "_",
};




static struct FSA          main_FSA, light_FSA, super_light_FSA;


static struct is_a_spelling_correction {
  char    *word;
  SXINT     lgth;
  SXBOOLEAN yes;
} is_a_spelling_correction;

static struct vstr_stack_hd {
  SXINT     top, size;
  VARSTR  *vstrs;
} *prefix_stack_ptr, *suffix_stack_ptr;

struct correction_struct {
  SXINT                             id, cost;
  VARSTR                          vstr, cvstr;
  struct is_a_spelling_correction is_a_spelling_correction;
  char                            *word;
  SXBOOLEAN                         is_first_line, is_first_word;
  struct vstr_stack_hd            prefix_stack, suffix_stack;
};

static struct correction_struct corrections [3], *prev_correction, *cur_correction, *bi_correction;
static SXINT which_correction, light_cost, heavy_cost;

#define RAZ_VSTR_STACK(t) (t).top=0

/* On stocke ds la pile des suffixes la sous-chaine */
static void
push_string (stack, lstr, rstr)
     struct vstr_stack_hd *stack;
     char                 *lstr, *rstr;
{
  SXINT    lgth;
  VARSTR vstr;

  lgth = rstr-lstr;

  if (++stack->top > stack->size) {
    stack->vstrs = (VARSTR *) sxrecalloc (stack->vstrs, stack->size+1, 2*stack->size+1, sizeof (VARSTR));
    stack->size *= 2;
  }

  if (stack->vstrs [stack->top] == NULL)
    vstr = stack->vstrs [stack->top] = varstr_alloc (32+lgth);
  else {
    /* y-a-t'il assez de place ? */
    vstr = varstr_raz (stack->vstrs [stack->top]);

    while (vstr->current+lgth+1 >= vstr->last)
      vstr = varstr_realloc (vstr);
  }

  varstr_lcatenate (vstr, lstr, lgth);
}

static void
alloc_vstr_stack_hd (stack, size)
     struct vstr_stack_hd *stack;
     SXINT                  size;
{
  stack->size = size;
  stack->vstrs = (VARSTR *) sxcalloc (stack->size+1, sizeof (VARSTR));
  stack->top = 0;
}

static void
free_vstr_stack_hd (stack)
     struct vstr_stack_hd *stack;
{
  VARSTR  vstr, *p = stack->vstrs, *top = stack->vstrs+stack->size;

  while (++p <= top) {
    if ((vstr = *p) == NULL)
      break;
      
    varstr_free (vstr);
  }

  sxfree (stack->vstrs), stack->vstrs = NULL;
}



static SXBOOLEAN
call_super_light_sxspell (char *mot, SXINT lgth)
{
  char    last_char;
  SXINT     x;
  SXBOOLEAN ret_val;

  last_char = mot [lgth];
  mot [lgth] = SXNUL;

  sxspell_init_fsa (mot, lgth, &super_light_FSA);

  sxspell_replace (&super_light_FSA, maj2diacritic, T_NB (maj2diacritic), maj2diacritic_weight);
  sxspell_replace (&super_light_FSA, add_diacritics, T_NB (add_diacritics), add_diacritics_weight);


  ret_val = sxspell_do_it (&super_light_FSA);

  mot [lgth] = last_char;

  return ret_val;
}


static SXBOOLEAN
call_light_sxspell (mot, lgth)
     char    *mot;
     SXINT     lgth;
{  char    last_char;
  SXINT     x;
  SXBOOLEAN ret_val, cur_char;

  last_char = mot [lgth];
  mot [lgth] = SXNUL;

  sxspell_init_fsa (mot, lgth, &light_FSA);

  /* changement apos/tiret */
  sxspell_replace (&light_FSA, apos_hyphen, T_NB (apos_hyphen), apos_hyphen_weight);
  /* changement de casse */
  sxspell_replace (&light_FSA, min_maj, T_NB (min_maj), min_maj_weight);
  sxspell_replace (&light_FSA, maj_min, T_NB (maj_min), maj_min_weight);
  sxspell_replace (&light_FSA, add_diacritics, T_NB (add_diacritics), add_diacritics_weight);
  sxspell_replace (&light_FSA, remove_diacritics, T_NB (remove_diacritics), remove_diacritics_weight);
  sxspell_replace (&light_FSA, change_diacritics, T_NB (change_diacritics), change_diacritics_weight);

  sxspell_replace_n_p (&light_FSA, twochars_to_diacritics, T_NB (twochars_to_diacritics), twochars_to_diacritics_weight);
  sxspell_replace_n_p (&light_FSA, mm_en_m, T_NB (mm_en_m), mm_en_m_weight);
  sxspell_replace_n_p (&light_FSA, m_en_mm, T_NB (m_en_mm), m_en_mm_weight);

  sxspell_replace_n_p (&light_FSA, final_dot, T_NB (final_dot), final_dot_weight);
#ifdef french
  sxspell_replace_n_p (&light_FSA, final_h, T_NB (final_h), final_h_weight);
  sxspell_replace_n_p (&light_FSA, final_e, T_NB (final_e), final_e_weight);
  sxspell_replace_n_p (&light_FSA, final_als, T_NB (final_als), final_als_weight);
  sxspell_replace_n_p (&light_FSA, iI_en_l, T_NB (iI_en_l), iI_en_l_weight);
  sxspell_replace_n_p (&light_FSA, dusse_je, T_NB (dusse_je), dusse_je_weight);
  sxspell_replace_n_p (&light_FSA, t_il, T_NB (t_il), t_il_weight);
#endif /* french */
  sxspell_replace_n_p (&light_FSA, blanc, T_NB (blanc), blanc_weight);
#ifdef latin1
  sxspell_replace_n_p (&light_FSA, eau_en_au, T_NB (eau_en_au), eau_en_au_weight);
  sxspell_replace_n_p (&light_FSA, gu_ge, T_NB (gu_ge), gu_ge_weight);
  sxspell_replace_n_p (&light_FSA, add_h, T_NB (add_h), add_h_weight);
#if 0
  /* qu'elle ne doit pas se corriger en quelle !! */
  sxspell_replace_n_p (&light_FSA, add_apos, T_NB (add_apos), add_apos_weight);
#endif /* 0 */
#endif /* latin1 */

  /* qq fautes d'orthographe courantes (d�pend de la langue) */
  sxspell_replace_n_p (&light_FSA, final_underscore, T_NB (final_underscore), final_underscore_weight);
  sxspell_replace_n_p (&light_FSA, ortho, T_NB (ortho), ortho_weight);
  sxspell_replace_n_p (&light_FSA, ortho2, T_NB (ortho2), ortho2_weight);

  /* nooooooooooonnnnnnnnnn => non */
  sxspell_no_repeat (&light_FSA, 3, no_repeat_weight);

  /* insertion de tirets */
  for (x = lgth; x >= 1; x--)
    sxspell_add (&light_FSA, x, (unsigned char*) "-", add_hyphen_weight);

  /* insertion de qq lettres */
  for (x = lgth; x >= 1; x--) {
    sxspell_add (&light_FSA, x, insert_string, insert_weight);
  }

  /* remplacements */
  for (x = lgth; x >= 1; x--) {
    cur_char = mot [x-1];

    if (cur_char != sxtolower (cur_char) || cur_char != sxtoupper (cur_char))
      /* on ne cherche � remplacer par une des lettres de change_string que les caract�res de l'entr�e qui sont de vraies lettres */
      sxspell_change (&light_FSA, x, change_string, change_weight);
  }
  
  ret_val = sxspell_do_it (&light_FSA);

  mot [lgth] = last_char;

  return ret_val;
}

static SXBOOLEAN
call_sxspell (mot, lgth)
     char    *mot;
     SXINT     lgth;
{
  char    last_char, cur_char;
  SXINT     x;
  SXBOOLEAN ret_val;

  last_char = mot [lgth];
  mot [lgth] = SXNUL;

  sxspell_init_fsa (mot, lgth, &main_FSA);

  /* changement apos/tiret */
  sxspell_replace (&main_FSA, apos_hyphen, T_NB (apos_hyphen), apos_hyphen_weight);
  /* changement de casse */
  sxspell_replace (&main_FSA, min_maj, T_NB (min_maj), min_maj_weight);
  sxspell_replace (&main_FSA, maj_min, T_NB (maj_min), maj_min_weight);
  sxspell_replace (&main_FSA, add_diacritics, T_NB (add_diacritics), add_diacritics_weight);
  sxspell_replace (&main_FSA, remove_diacritics, T_NB (remove_diacritics), remove_diacritics_weight);
  sxspell_replace (&main_FSA, change_diacritics, T_NB (change_diacritics), change_diacritics_weight);
  /* les lettres double'es sont fausses */
  sxspell_replace_n_p (&main_FSA, mm_en_m, T_NB (mm_en_m), mm_en_m_weight);
  /* il faut des consonnes double'es */
  sxspell_replace_n_p (&main_FSA, m_en_mm, T_NB (m_en_mm), m_en_mm_weight);

  sxspell_replace_n_p (&main_FSA, final_dot, T_NB (final_dot), final_dot_weight);
#ifdef french
  sxspell_replace_n_p (&main_FSA, abbrev, T_NB (abbrev), abbrev_weight);
  sxspell_replace_n_p (&main_FSA, final_h, T_NB (final_h), final_h_weight);
  sxspell_replace_n_p (&main_FSA, final_e, T_NB (final_e), final_e_weight);
  sxspell_replace_n_p (&main_FSA, final_als, T_NB (final_als), final_als_weight);
  sxspell_replace_n_p (&main_FSA, iI_en_l, T_NB (iI_en_l), iI_en_l_weight);
  sxspell_replace_n_p (&main_FSA, dusse_je, T_NB (dusse_je), dusse_je_weight);
  sxspell_replace_n_p (&main_FSA, t_il, T_NB (t_il), t_il_weight);
#endif /* french */
  sxspell_replace_n_p (&main_FSA, blanc, T_NB (blanc), blanc_weight);
#ifdef latin1
  sxspell_replace_n_p (&main_FSA, eau_en_au, T_NB (eau_en_au), eau_en_au_weight);
  sxspell_replace_n_p (&main_FSA, gu_ge, T_NB (gu_ge), gu_ge_weight);
  sxspell_replace_n_p (&main_FSA, add_h, T_NB (add_h), add_h_weight);
  sxspell_replace_n_p (&main_FSA, add_apos, T_NB (add_apos), add_apos_weight);
#endif /* latin1 */

  /* traitement des fautes de frappe */
  sxspell_replace (&main_FSA, qq_trucs, T_NB (qq_trucs), qq_trucs_weight);
  /* traitement des fautes de frappe */
  sxspell_replace (&main_FSA, typos_qwerty, T_NB (typos_qwerty), typos_weight);
  /* traitement du changement de claviers */
  sxspell_replace (&main_FSA, qwerty_azerty, T_NB (qwerty_azerty), qwerty_azerty_weight);
  /* On s'est trompe' de voyelle, meme accent */
  //  sxspell_replace (&main_FSA, mauvaise_voyelle, T_NB (mauvaise_voyelle), mauvaise_voyelle_weight);

#if 0
  /* Essai, attention au temps ... */
  sxspell_suppress_all (&main_FSA, suppress_weight);
#endif /* 0 */
  
  /* Le temps augmente tres fortement si i > 1 !! */
  /* traitement de la suppression de 1 caractere successif */
  sxspell_suppress_i (&main_FSA, suppress_weight, 1);

  /* traitement de l'intervertion de 2 caracteres */
  sxspell_swap (&main_FSA, swap_weight);
  /* qq fautes d'orthographe */
  sxspell_replace_n_p (&main_FSA, final_underscore, T_NB (final_underscore), final_underscore_weight);
  sxspell_replace_n_p (&main_FSA, ortho, T_NB (ortho), ortho_weight);
  sxspell_replace_n_p (&main_FSA, ortho2, T_NB (ortho2), ortho2_weight);
#ifdef french
  /* On peut ajouter qq lettres en fin */
  sxspell_add (&main_FSA, lgth+1, (unsigned char*) "cdefhpstxz", add_weight);
  /* et une hache muette au d�but... */
  sxspell_add (&main_FSA, 1, (unsigned char*) "hH", add_weight);
#endif /* french */

  /* nooooooooooonnnnnnnnnn => non */
  sxspell_no_repeat (&main_FSA, 3, no_repeat_weight);

  /* insertion de tirets */
  for (x = lgth; x >= 1; x--)
    sxspell_add (&main_FSA, x, (unsigned char*) "-_", add_hyphen_weight);

  /* insertion de qq lettres */
  for (x = lgth; x >= 1; x--) {
    sxspell_add (&main_FSA, x, insert_string, insert_weight);
  }

  /* remplacements */
  for (x = lgth; x >= 1; x--) {
    cur_char = mot [x-1];

    if (cur_char != sxtolower (cur_char) || cur_char != sxtoupper (cur_char))
      /* Le 24/05/05 */
      /* lettre */
      sxspell_change (&main_FSA, x, change_string, change_weight);
  }

  ret_val = sxspell_do_it (&main_FSA);

  mot [lgth] = last_char;

  return ret_val;
}

static void
print_spelling_correction (FSA_ptr)
     struct FSA *FSA_ptr;
{
  SXINT id, *bot_ptr, *top_ptr;
    
  /* On ne prend que le 1er resultat parmi les meilleurs */
  id = SXSPELL_x2id (*FSA_ptr, 1);

  if (!SXSPELL_has_subwords (id)) {
    if (print_comment && varstr_length (vstr))
      printf ("%s ", varstr_tostr (vstr));

    printf ("%s ", SXSPELL_id2str (*FSA_ptr, id));
  }
  else {
    /* cas sous-mots */
    SXSPELL_subword_foreach (*FSA_ptr, id, bot_ptr, top_ptr) {
      if (print_comment && varstr_length (vstr))
	printf ("%s ", varstr_tostr (vstr));

      id = SXSPELL_x2subword_id (bot_ptr);
      printf ("%s ", SXSPELL_id2str (*FSA_ptr, id));
#if 0
      if (!SXSPELL_last_subword (bot_ptr, top_ptr))
	putchar (' ');
#endif /* 0 */
    }
  }
}


static SXBOOLEAN
is_a_super_light_spelling_correction (str, lgth)
     char                 *str;
     SXINT                  lgth;
{
  /* On essaye de le corriger ... */
  /* ... avec un poids faible */
  SXINT     store_max_weight;
  SXBOOLEAN ret_val;

  store_max_weight = super_light_FSA.spell_result.max_weight; /* tres laid !! */
  super_light_FSA.spell_result.max_weight = light_weight; /* A FAIRE mettre un super_light_weight */

  ret_val = call_super_light_sxspell (str, lgth);

  super_light_FSA.spell_result.max_weight = store_max_weight; /* remise en etat */

  return ret_val;
}


static SXBOOLEAN
is_a_light_spelling_correction (str, lgth)
     char                 *str;
     SXINT                  lgth;
{
  /* On essaye de le corriger ... */
  /* ... avec un poids faible */
  SXINT     store_max_weight;
  SXBOOLEAN ret_val;

  store_max_weight = light_FSA.spell_result.max_weight; /* tres laid !! */
  light_FSA.spell_result.max_weight = light_weight;

  ret_val = call_light_sxspell (str, lgth);

  light_FSA.spell_result.max_weight = store_max_weight; /* remise en etat */

  return ret_val;
}


static void
store_a_super_light_spelling_correction (stack)
     struct vstr_stack_hd *stack;
{
  SXINT     id;
  SXINT     *bot_ptr, *top_ptr;
  char    *c_str;

  /* La correction a marche' */
  /* on la range ds stack */
  id = SXSPELL_x2id (super_light_FSA, 1);
  light_cost += SXSPELL_x2weight (super_light_FSA, 1);

  if (!SXSPELL_has_subwords (id)) {
    c_str = SXSPELL_id2str (super_light_FSA, id);
    push_string (stack, c_str, c_str+strlen (c_str));
  }
  else {
    /* cas sous-mots */
    SXSPELL_subword_foreach (super_light_FSA, id, bot_ptr, top_ptr) {
      id = SXSPELL_x2subword_id (bot_ptr);
      c_str = SXSPELL_id2str (super_light_FSA, id);
      push_string (stack, c_str, c_str+strlen (c_str));
    }
  }
}


static void
store_a_light_spelling_correction (stack)
     struct vstr_stack_hd *stack;
{
  SXINT     id;
  SXINT     *bot_ptr, *top_ptr;
  char    *c_str;

  /* La correction a marche' */
  /* on la range ds stack */
  id = SXSPELL_x2id (light_FSA, 1);
  light_cost += SXSPELL_x2weight (light_FSA, 1);

  if (!SXSPELL_has_subwords (id)) {
    c_str = SXSPELL_id2str (light_FSA, id);
    push_string (stack, c_str, c_str+strlen (c_str));
  }
  else {
    /* cas sous-mots */
    SXSPELL_subword_foreach (light_FSA, id, bot_ptr, top_ptr) {
      id = SXSPELL_x2subword_id (bot_ptr);
      c_str = SXSPELL_id2str (light_FSA, id);
      push_string (stack, c_str, c_str+strlen (c_str));
    }
  }
}


static SXBOOLEAN
call_a_super_light_spelling_correction (stack, str, lgth)
     struct vstr_stack_hd *stack;
     char                 *str;
     SXINT                  lgth;
{
  SXBOOLEAN ret_val;

  if ((ret_val = is_a_super_light_spelling_correction (str, lgth)))
    /* La correction a marche' */
    /* on la range ds stack */
    store_a_super_light_spelling_correction (stack);

  return ret_val;
}

static SXBOOLEAN
call_a_light_spelling_correction (stack, str, lgth)
     struct vstr_stack_hd *stack;
     char                 *str;
     SXINT                  lgth;
{
  SXBOOLEAN ret_val;

  if ((ret_val = is_a_light_spelling_correction (str, lgth)))
    /* La correction a marche' */
    /* on la range ds stack */
    store_a_light_spelling_correction (stack);

  return ret_val;
}

static SXBOOLEAN
has_upper (str, lgth)
     char *str;
     SXINT lgth;
{
  char    car;

  while (lgth--) {
    car = *str++;

    if (car != sxtolower (car))
      return SXTRUE;
  }

  return SXFALSE;
}


static SXBOOLEAN
string2lower (vstr, str, lgth)
     VARSTR vstr;
     char *str;
     SXINT lgth;
{
  char    car, *top_str;
  SXBOOLEAN has_changed;

  vstr = varstr_raz (vstr);

  while (vstr->current+lgth >= vstr->last)
    vstr = varstr_realloc (vstr);
  
  top_str = str + lgth;
  has_changed = SXFALSE;

  while (str < top_str) {
    car = sxtolower (*str);
    *(vstr->current++) = car;

    if (car != *str)
      has_changed = SXTRUE;

    str++;
  }

  *(vstr->current) = SXNUL;

  return has_changed;
}

/* On regarde si le mot corrige' est le meme que le mot a corriger a la casse pres */
static SXBOOLEAN
case_free_equality (FSA_ptr, str, lgth)
     struct FSA *FSA_ptr;
     char *str;
     SXINT  lgth;
{
  SXINT  id;
  char *lc_str;

  id = SXSPELL_x2id (*FSA_ptr, 1);

  if (SXSPELL_has_subwords (id))
    /* correction vers mots multiples */
    return SXFALSE;
  
  lc_str = SXSPELL_id2str (*FSA_ptr, id);

  if (lgth != strlen (lc_str))
    return SXFALSE;

  while (lgth-- > 0) {
    if (sxtolower (*str++) != sxtolower (*lc_str++))
      return SXFALSE;
  }

  return SXTRUE;
}
     

static void
print_a_super_light_spelling_correction (str, lgth)
     char *str;
     SXINT  lgth;
{
  char car, *p;

  /* on est ds le cas print_error */
  /* On n'imprime que si le mot corrige' n'est pas le meme que le mot a corriger a la casse pres */
  if (!case_free_equality (&super_light_FSA, str, lgth)) {
    fputs ("\n", stdout);

    if (print_input) {
      if (print_comment && varstr_length (vstr))
	printf ("%s ", varstr_tostr (vstr));

      car = *(p = str+lgth);
      *p = SXNUL;
      printf ("[%s] ", str);
      *p = car;
    }

    print_spelling_correction (&super_light_FSA);
  }
}

static void
print_a_light_spelling_correction (str, lgth)
     char *str;
     SXINT  lgth;
{
  char car, *p;

  /* on est ds le cas print_error */
  /* On n'imprime que si le mot corrige' n'est pas le meme que le mot a corriger a la casse pres */
  if (!case_free_equality (&light_FSA, str, lgth)) {
    fputs ("\n", stdout);

    if (print_input) {
      if (print_comment && varstr_length (vstr))
	printf ("%s ", varstr_tostr (vstr));

      car = *(p = str+lgth);
      *p = SXNUL;
      printf ("[%s] ", str);
      *p = car;
    }

    print_spelling_correction (&light_FSA);
  }
}

/* Essai ds lequel on donne priorite' au plus long et a egalite de longueur aux prefixes suffixes */
static SXBOOLEAN is_process_prefix_finished, is_process_suffix_finished;

/* Version light qui n'extrait que les vrais suffixes, le reste est fait par call_process_prefix */
static SXBOOLEAN
call_process_suffix (str_lptr, str_rptr)
     char    **str_lptr, **str_rptr;
{
  SXINT  action, cur_lgth;
  char *bot_str, *top_str, *lstr, *rstr, *cur_str, car;


  bot_str = *str_lptr;
  top_str = *str_rptr;

  while (!is_process_suffix_finished) {
    cur_str = bot_str;
    cur_lgth = top_str-bot_str;

    /* On regarde si le noyau restant est un mot connu, si oui on n'essaie pas d'en detacher d'autres suffixes */
    if (sxdfa_comb_get (dico_des_mots, bot_str, top_str-bot_str)) {
      /* ... oui */
      /* on le stocke */
      push_string (prefix_stack_ptr, bot_str, top_str);
      bot_str = top_str; /* l'appelant n'a plus rien a faire */
      is_process_suffix_finished = SXTRUE; /* fini */
    }
    
    if (!is_process_suffix_finished) {
      /* On regarde aussi si le noyau courant est la capitalisation d'un mot connu, si oui on
	 n'essaie pas d'en detacher d'autres suffixes */
      if (*bot_str == sxtoupper(*bot_str)) {
	*bot_str = sxtolower(*bot_str);
	if (sxdfa_comb_get (dico_des_mots, bot_str, cur_lgth)) {
	  /* ... oui */
	  /* on le stocke */
	  *bot_str = sxtoupper(*bot_str);
	  push_string (prefix_stack_ptr, bot_str, top_str);
	  bot_str = top_str; /* l'appelant n'a plus rien a faire */
	  is_process_suffix_finished = SXTRUE; /* fini */
	} else
	  *bot_str = sxtoupper(*bot_str);
      }
    }

    if (!is_process_suffix_finished) {
      action = sxdfa_comb_get_bounded (dico_des_suffixes, &cur_str, &cur_lgth);

      /* ajout BS: chosophile, machiniphage ; pas de v�rification que le premier morceau est corrigeable � peu de frais => TODO */
      if (action) {
	switch (action) {
	case 6:
	  /* d�tachement d'un suffixe pur ssi ce qui reste est connu du lexique ou commence par une minuscule */
          if (cur_lgth<=2 || !((sxtolower(*bot_str) == *bot_str) || sxdfa_comb_get (dico_des_mots, bot_str, cur_lgth-2))) {
	    is_process_suffix_finished = SXTRUE;
          }
	  break;
	case 5:
	  /* robi�em : ne marche qu'apr�s � (l barr� en latin2) */
          if (cur_lgth<=2 || *(bot_str+cur_lgth-1)!='�') {
	    is_process_suffix_finished = SXTRUE;
          }
	  break;
	case 4:
	  /* robilismy / robi�ysme : ne marche qu'apr�s li ou �y*/
          if (cur_lgth<=3 || !((*(bot_str+cur_lgth-2)=='l' && *(bot_str+cur_lgth-1)=='i') || (*(bot_str+cur_lgth-2)=='�' && *(bot_str+cur_lgth-1)=='y'))) {
	    is_process_suffix_finished = SXTRUE;
          }
	  break;
	case 3:
	  /* robi�am / robi�om : ne marche qu'apr�s �a ou �o*/
          if (cur_lgth<=3 || (!(*(bot_str+cur_lgth-2)=='�' && (*(bot_str+cur_lgth-1)=='o' || *(bot_str+cur_lgth-1)=='a')))) {
	    is_process_suffix_finished = SXTRUE;
          }
	  break;
	case 2:
	  /* chosophile -> chose _phile ; ne marche qu'avec [oi]->e � la fin du premier morceau*/
          if (cur_lgth<=0 || (*(bot_str+cur_lgth-1)!='o' && *(bot_str+cur_lgth-1)!='i')) {
	    is_process_suffix_finished = SXTRUE;
	    break;
          }
	  if (!is_dry_run)
	    *(bot_str+cur_lgth-1)='e';
	  break;
	case 1:
	  break;
	default:
	  is_process_suffix_finished = SXTRUE;
	  break;
	}

	if (!is_process_suffix_finished) {
	  /* ...y'a un suffixe et un noyau qui peut etre vide */
	  /* On stocke le separateur ... */
	  lstr = action2suffix_chars [action];
	  rstr = lstr + strlen (lstr);
	  push_string (suffix_stack_ptr, lstr, rstr);
	  light_cost += 1; /* A voir */
	  /* ... et le suffixe ... */
	  lstr = bot_str+cur_lgth;
	  varstr_lcatenate (suffix_stack_ptr->vstrs [suffix_stack_ptr->top], lstr, top_str-lstr);
	
	  top_str = lstr;
	}
      }
      else
	is_process_suffix_finished = SXTRUE;
    }
  }

  *str_lptr = bot_str;
  *str_rptr = top_str;

  return bot_str == top_str;
}


static SXBOOLEAN
call_process_prefix (str_lptr, str_rptr, has_initial_maj)
     char    **str_lptr, **str_rptr;
     SXBOOLEAN has_initial_maj;
{
  SXINT     id, action, action2, cur_lgth, cur_lgth2, store_weight_limit, left_pos, cur_pos, right_pos, pref_lgth, lgth;
  char    *cur_str, *cur_str2, *bot_str, *top_str, *cur_wstr, *lstr, *rstr;
  SXBOOLEAN done, pushed;
  char    *mot_courant;

  mot_courant = bot_str = *str_lptr;
  top_str = *str_rptr;

  /* Calcul de hyphen_set dynamique car on travaille sur un noyau du mot d'origine, les positions
     des tirets ont donc pu changer */
  sxba_empty (hyphen_set);

  if (top_str-bot_str >= (lgth = SXBASIZE (hyphen_set))) {
    lgth += top_str-bot_str;
    hyphen_set = sxba_resize (hyphen_set, lgth);
  }

  for (cur_str = bot_str, cur_pos = 0; cur_str < top_str; cur_str++, cur_pos++) {
    if (*cur_str == '-' || *cur_str == '�')
      SXBA_1_bit (hyphen_set, cur_pos);
  }

  /* a priori */
  is_process_prefix_finished = SXFALSE;
  is_process_suffix_finished = SXFALSE;

  if (bot_str < top_str /* tout n'est pas traite' */
      && process_suffix
      && !is_process_suffix_finished) {
    call_process_suffix (&bot_str, &top_str);
  }

  if (bot_str < top_str /* tout n'est pas traite' */
      && process_prefix) {
    /* is_process_prefix_finished == SXTRUE <=> La chaine a ete traitee en entier ou on n'a pas extrait de prefixe */
    /* super_boucle */
    while (!is_process_prefix_finished) {
      /* Traitement iteratif et non recursif */
      /* le noyau est-il une ff ? */
      id = sxdfa_comb_get (dico_des_mots, bot_str, top_str-bot_str);

      if (id != 0) {
	/* ... oui */
	/* on le stocke */
	push_string (prefix_stack_ptr, bot_str, top_str);
	light_cost += 1; /* A voir */
	bot_str = top_str;
	is_process_prefix_finished = SXTRUE;
      }
      else {
	cur_str = bot_str;
	cur_lgth = top_str-bot_str;

	action2 = 0; /* ce n'est pas une correction qui a donne le prefixe */
	
	if ((action = sxdfa_comb_get_bounded (dico_des_prefixes, &cur_str, &cur_lgth)/* && cur_lgth > 0 */))
	  pref_lgth = cur_str-bot_str;

	left_pos = ((action) ? cur_str : bot_str) - mot_courant;

	/* On recherche le tiret le + a droite  */
	/* hyphen_set contient les emplacements de tous les tirets */
	cur_pos = top_str-mot_courant;

	/* BOUCLE */
	while ((cur_pos = sxba_1_rlscan (hyphen_set, cur_pos)) > left_pos) {
	  /* y'a un tiret en cur_pos et le prefixe est de longueur au moins 1 */
	  cur_str2 = bot_str;
	  cur_lgth2 = cur_pos - (bot_str-mot_courant);

	  id = 0;

	  if (is_light_correction && id == 0 && action == 0) {
	    /* Ce prefixe ne peut pas se detacher directement */
	    /* On tente une correction light seulement s'il n'y a plus de tiret a gauche */
	    if (sxba_1_rlscan (hyphen_set, cur_pos) <= left_pos) {
	      /* Pour l'instant je ne mets pas le tiret ds le mot a corriger.  Il est possible de faire les
		 2 tentatives */
	      cur_str = mot_courant+cur_pos;

	      if (!has_initial_maj && !has_upper (bot_str, cur_str-bot_str) && call_a_light_spelling_correction (prefix_stack_ptr, bot_str, cur_str-bot_str)) {
		/* On saute le tiret car il ne peut pas faire partie integrante d'un suffixe ... */
		bot_str = mot_courant+cur_pos+1;
		/* ... et on recommence */
		id = 1;
		break;
	      }
	    }
	  }
	}
	/* Fin de BOUCLE */

	/* decollage */
	if (id == 0) {
	  /* on n'a rien pu detacher ... */
	  if (action) {
	    /* ... mais y'a un prefixe et un noyau qui peut etre vide */
	    done = SXFALSE;
	    pushed = SXFALSE;

	    switch (action) {
	    case 1:
	      /* Le prefixe est du type "anti-" */
	      /* Si le suffixe commence par une majuscule, on n'y touche pas : cas "anti-Seattle" */
	      if (SXBA_bit_is_set (MAJ_set, ((unsigned char) *cur_str))) {

		/* on stocke le prefixe ... */
		push_string (prefix_stack_ptr, bot_str, cur_str);

		/* ... avec son separateur (si ce n'est pas le 1er mot d'une phrase) */
		if (has_initial_maj) {
		  has_initial_maj = SXFALSE;
		}
		else {
		  varstr_catenate (prefix_stack_ptr->vstrs [prefix_stack_ptr->top], action2prefix_chars [action]);
		}

		/* ... et Seattle */
		push_string (prefix_stack_ptr, cur_str, top_str);
	  
		light_cost += 1; /* A voir */
		bot_str = top_str;
		done = SXTRUE;
	      }

	      break;

	    case 2:
	      /* Le prefixe est du type "anti" */
	      /* On regarde si en ajoutant un tiret le mot "anti-suffixe:" est une forme flechie.
		 Si oui, on va privilegier ce cas */
	      wstr = varstr_raz (wstr);

	      while (wstr->current+(pref_lgth+cur_lgth)+1 >= wstr->last)
		wstr = varstr_realloc (wstr);

	      varstr_lcatenate (wstr, bot_str, pref_lgth);

	      varstr_catchar (wstr, '-');
	      varstr_lcatenate (wstr, cur_str, cur_lgth);
	
	      cur_wstr = varstr_tostr (wstr);
	      cur_lgth2 = varstr_length (wstr);

	      id = sxdfa_comb_get (dico_des_mots, cur_wstr, cur_lgth2);

	      if (id) {
		/* oui */
		push_string (prefix_stack_ptr, cur_wstr, cur_wstr+cur_lgth2);
		light_cost += 1; /* A voir */
		bot_str = top_str;
		done = SXTRUE;
	      }

	      break;

	    case 3:
	      /*  Le prefixe est du type "entr'", on supprime l'apos que l'on remplace par un e */
	      push_string (prefix_stack_ptr, bot_str, cur_str-1);

	      light_cost += 1; /* A voir */
	      pushed = SXTRUE;
	      break;

	    case 4:
	      break;

	    case 5:
	      /* Le prefixe est du type "d�s" */
	      /* On ne le valide que s'il est suivi par une voyelle */
	      if (!SXBA_bit_is_set (vowell_set, ((unsigned char) *cur_str)))
		done = SXTRUE;

	      break;

	    case 6:
	      /* Le prefixe est du type "im" */
	      /* On ne le valide que s'il est suivi par "mpb" */
	      if (!SXBA_bit_is_set (mpb_set, ((unsigned char) *cur_str)))
		done = SXTRUE;

	      break;
	    }
	
	    if (!done) {
	      /* meme si le suffixe n'est pas bon, on stocke le prefixe ... */
	      if (!pushed) {
		push_string (prefix_stack_ptr, bot_str, cur_str);
	  
		light_cost += 1; /* A voir */
	      }

	      /* ... avec son separateur (si ce n'est pas le 1er mot d'une phrase ...) */
	      if (has_initial_maj) {
		has_initial_maj = SXFALSE;
	      }
	      else {
		varstr_catenate (prefix_stack_ptr->vstrs [prefix_stack_ptr->top], action2prefix_chars [action]);
	      }

	      bot_str = cur_str;
	    }
	    else {
	      is_process_prefix_finished = SXTRUE;
	    }
	  }
	  else
	    is_process_prefix_finished = SXTRUE;
	}
	/* fin decollage */
      }

    }
    /* Fin super_boucle */
  }

  /* Ici on a traite les suffixes et les prefixes au maximum */
  if (is_light_correction && bot_str < top_str && !has_initial_maj && !has_upper (bot_str, top_str-bot_str)) {
    /* Noyau non vide ... */
    /* ... on essaye de le corriger ... */
    /* ... avec un poids faible */
    if (call_a_light_spelling_correction (prefix_stack_ptr, bot_str, top_str-bot_str)) {
      if (print_error)
	print_a_light_spelling_correction (bot_str, top_str-bot_str);

      bot_str = top_str;
    }
  }

  *str_lptr = bot_str;
  *str_rptr = top_str;

  return bot_str == top_str;
}



/* La [sous-]chaine (str_lptr, str_rptr) n'est pas une forme flechie
   on la decoupe (recursivement) sur des tirets ou des apostrophes */
static SXBOOLEAN
call_cut_on_quote_hyphen (str_lptr, str_rptr, has_hyphen_in_prefix, is_pref_OK)
     char    **str_lptr, **str_rptr;
     SXBOOLEAN has_hyphen_in_prefix, is_pref_OK;
{
  SXINT  pref_id, suf_id, cur_lgth, action, cur_lgth2, cur_lgth3;
  char *cur_str, *cur_str2, *bot_str, *bot_str2, *bot_str3, car, *top_str, *work_str;
  SXBOOLEAN is_suf_OK;

  bot_str = cur_str = *str_lptr;
  top_str = *str_rptr;

  if (is_cut_on_hyphen && (*bot_str == '-' || *bot_str == '�')) {
    /* On saute le '-' du debut */
    has_hyphen_in_prefix = SXTRUE;
    bot_str = ++cur_str;

    if (sxdfa_comb_get (dico_des_mots, bot_str, top_str-bot_str)) {
      push_string (suffix_stack_ptr, bot_str, top_str);
      /* Attention le "-" final fait partie du noyau */
      *str_lptr = *str_rptr = bot_str;
      light_cost += 1; /* A voir */
      return SXTRUE;
    }
  }
    
  while (cur_str < top_str) {
    car = *cur_str++;
  
    if ((is_cut_on_quote && (car == '\'')) || (is_cut_on_hyphen && (car == '-' || car == '�'))) {
      is_suf_OK = SXTRUE;
      /* Si le prefixe et le suffixe sont ds le dico => OK */
      if (top_str-cur_str > 0) {
	/* suffixe non vide */
	bot_str2 = bot_str;
	cur_lgth2 = cur_str-bot_str;

	/* ajout� par Beno�t: on veut �tre s�r aussi que si on d�tache la partie gauche
	 en tant que pr�fixe on ne laisse pas en partie droite un "vrai" suffixe ;
	 ce n'est qu'ainsi qu'on peut avoir "programmeur-n�" -> "programmeur _-n�" et non "programmeur n�"*/ 
	bot_str3 = cur_str-1; /* "-1" car on r�cup�re le tiret */
	cur_lgth3 = top_str-cur_str+1;
	action = sxdfa_comb_get (dico_des_suffixes, bot_str3, cur_lgth3);
	if (action > 0) 
	  is_suf_OK = SXFALSE;

	action += sxdfa_comb_get_bounded (dico_des_prefixes, &bot_str2, &cur_lgth2);

	if (car == '-' || car == '�')
	  /* '-' fait partie du suffixe */
	  cur_str--;

	if (action == 0 || cur_lgth2 > 0) {
	  /* Ce n'est pas un vrai prefixe */
	  pref_id = sxdfa_comb_get (dico_des_mots, bot_str, cur_str-bot_str);
	  
	  if (pref_id && is_pref_OK) {
	    /* Ds cette version, on stocke le prefixe ds tous les cas !! */
	    push_string (prefix_stack_ptr, bot_str, cur_str);
	    bot_str = cur_str;
	    light_cost += 1; /* A voir */
	  }
	  else
	    is_pref_OK = SXFALSE;
	}
	else {
	  /* C'est un vrai prefixe ... */
	  /* on favorise ce cas qui sera traite' + tard => aucune recherche ds dico_des_mots */
	  pref_id = 0;
	  is_pref_OK = SXFALSE;
	}

	/* petite verrue ... */
	if ((car == '\'') && has_upper (cur_str, top_str-cur_str) /* (SXBA_bit_is_set (MAJ_set, *cur_str)) */) {
	  /* Mot a majuscule apres une apostrophe => on arre^te ... */
	  if (pref_id) {
	    /* ... si le prefixe etait correcte */
	    break;
	  }
	}

	if (is_suf_OK) {
	  /* ... et le suffixe */
	  suf_id = sxdfa_comb_get (dico_des_mots, cur_str, top_str-cur_str);
	  
	  if (suf_id) {
	    /* ... et le suffixe est une ff */
	    push_string (suffix_stack_ptr, cur_str, top_str);
	    top_str = cur_str;
	    light_cost += 1; /* A voir */
	    
	    if (pref_id && !is_pref_OK) {
	      /* le "bon" prefixe non contigu est un suffixe !! */
	      push_string (suffix_stack_ptr, bot_str, cur_str);
	      top_str = cur_str = bot_str;
	      light_cost += 1; /* A voir */
	    }
	  }
	  else {
	    /* ... sinon on regarde recursivement si c'en est une */
	    cur_str2 = cur_str;
	    suf_id = call_cut_on_quote_hyphen (&cur_str, &top_str, has_hyphen_in_prefix, is_pref_OK);
	    /* ds tous les cas top_str est correct !! */
	    
	    if (is_pref_OK)
	      bot_str = cur_str;
	    else {
	      /* Le 16/05/05 */
	      if (suf_id && pref_id) {
		/* le "bon" prefixe non contigu est un suffixe !! */
		push_string (suffix_stack_ptr, bot_str, cur_str2);
		top_str = cur_str = bot_str;
		light_cost += 1; /* A voir */
	      }
	    }
	  }
	}
	
	/* Ce sont les appels recursifs qui vont chercher les '-' et les '\'' sur la droite */
	break;
      }
    }
  }

  *str_lptr = bot_str;
  *str_rptr = top_str;

  return bot_str == top_str;
}


static SXINT
call_spell (mot, lgth, is_first_word/* , is_followed_by_a_nl */)
     char    *mot;
     SXINT     lgth;
     SXBOOLEAN is_first_word;
     /* SXBOOLEAN is_followed_by_a_nl; */
{
  SXINT     cur_lgth, x, id, y, *bot_ptr, *top_ptr, action, prev_action, pre_lgth, suf_lgth, i;
  SXINT     delta_vstr_mot_en_min, longueur_de_mot, prev_light_cost;
  char    car, cur_car, prev_car;
  char    *cur_str, *cur_str_mot_en_min, *top_str;
  SXBOOLEAN has_changed, has_initial_maj;
  char    *mot_courant;

  MOT_A_CORRIGER = mot_courant = mot;
  longueur_de_mot = lgth;

  id = sxdfa_comb_get (dico_des_mots, mot_courant, lgth);

  if (id)
    push_string (prefix_stack_ptr, mot_courant, mot_courant+lgth);

  if (id == 0) {
    if (is_light_correction) {
      has_changed = has_upper (mot_courant, lgth);

      if (has_changed) {
	/* Le mot contient des majuscules */
	/* => on regarde une correction super light */
	if (is_a_super_light_spelling_correction (mot_courant, lgth)) {
	  /* Cette correction ne devient effective que si elle contient des majuscules. Ex : Etats-Unis => �tats-Unis
	     sinon text2dag le recommencera en non-deterministe. Exemple RENE => Ren� | r�ne | ren� */
	  id = SXSPELL_x2id (super_light_FSA, 1);
  
	  if (!SXSPELL_has_subwords (id)) {
	    /* correction en un seul mot */
	    cur_str = SXSPELL_id2str (super_light_FSA, id);
	    cur_lgth = strlen (cur_str);

	    if (has_upper (cur_str, cur_lgth)) {
	      /* �tats-Unis ou Ren� */
	      push_string (prefix_stack_ptr, cur_str, cur_str+cur_lgth);

	      if (print_error)
		print_a_light_spelling_correction (cur_str, cur_lgth);
	    }
	    else
	      /* r�ne | ren� => on reste avec RENE, c'est la dageur qui corrigera */
	      id = 0;
	  }
	  else
	    id = 0;
	}
      }
      else {
	/* Le mot ne contient que des minuscules */
	/* => correction light */
	if (call_a_light_spelling_correction (prefix_stack_ptr, mot_courant, lgth)) {
	  id = 1;

	  if (print_error)
	    print_a_light_spelling_correction (mot_courant, lgth);
	}
      }
    }
  }

  if (id == 0) {
    if (is_first_word && is_process_lower)
      car = sxtolower (*mot_courant);
    else
      car = *mot_courant;

    /* (car != *mot_courant) <==> Le 1er mot commence par une majuscule */
  }

  if (id == 0 && (is_cut_on_quote || is_cut_on_hyphen)) {

    top_str = mot_courant + lgth;
    cur_str = mot_courant;

    if (car != *mot_courant) {
      prev_car = *mot_courant;
      *mot_courant = car;
      has_initial_maj = SXTRUE;
    }
    else
      has_initial_maj = SXFALSE;

    has_changed = call_cut_on_quote_hyphen (&cur_str, &top_str, SXFALSE /* has_hyphen_in_prefix */, SXTRUE /* is_first_call */);

    if (cur_str > mot_courant)
      /* Il y a un mot prefixe de'tache' */
      is_first_word = SXFALSE;

    if (has_initial_maj) {
      /* Prudence, on remet en etat */
      *mot_courant = prev_car;

      if (has_changed || cur_str > mot_courant) {
	char *first_char_ptr;

	/* On majusculise la 1ere lettre du 1er prefixe */
	first_char_ptr = prefix_stack_ptr->vstrs [1]->first;
	*first_char_ptr = sxtoupper (*first_char_ptr);
      }
    }

    if (has_changed) {
      id = 1;
    }
    else {
      /* On continue sur le noyau */
      mot_courant = cur_str;
      lgth = top_str-cur_str;
    }
  }

  /* On regarde si le mot a des prefixes et des suffixes remarquables */
  if (id == 0 && (process_prefix || process_suffix)) {
    SXINT prefix_top, suffix_top;

    top_str = mot_courant + lgth;
    cur_str = mot_courant;
    prev_light_cost = light_cost;
    prefix_top = prefix_stack_ptr->top;
    suffix_top = prefix_stack_ptr->top;
      
    has_initial_maj = SXFALSE;

    if (is_first_word && is_process_lower) {
      car = sxtolower (*mot_courant);

      if (car != *mot_courant) {
	prev_car = *mot_courant;
	*mot_courant = car;
	has_initial_maj = SXTRUE;
      }
    }

    has_changed = call_process_prefix (&cur_str, &top_str, has_initial_maj);

    if (has_initial_maj) {
      /* Prudence, on remet en etat */
      *mot_courant = prev_car;

      if (has_changed) {
	char *first_char_ptr;

	if (cur_str > mot_courant) {
	  /* On majusculise la 1ere lettre du 1er prefixe */
	  first_char_ptr = prefix_stack_ptr->vstrs [1]->first;
	  *first_char_ptr = sxtoupper (*first_char_ptr);
	}
	else {
	  /* On majusculise la 1ere lettre du 1er suffixe */
	  first_char_ptr = suffix_stack_ptr->vstrs [suffix_stack_ptr->top]->first;
	  *first_char_ptr = sxtoupper (*first_char_ptr);
	}
      }
      /* Else echec => On ignore les prefixes suffixes detaches et on va repartir sur mot */
    }

    if (has_changed) {
      id = 1;
    }
    else {
      /* On ignore les prefixes suffixes detaches et on repart sur mot */
      /* On conserve les extractions de cut_on_quote ... */
      light_cost = prev_light_cost;
      prefix_stack_ptr->top = prefix_top;
      prefix_stack_ptr->top = suffix_top;
    }
  }
  
  if (id == 0) {
    /* Toutes les bricoles ont echouees, on lance la vraie correction sur le noyau */
    /* ... en 2 temps */
    has_changed = has_upper (mot_courant, lgth);

    if (has_changed) {
      /* Y'a des majuscules, on abandonne */
      id = 1;
      push_string (prefix_stack_ptr, mot_courant, mot_courant+lgth);
      light_cost += 1; /* A voir */
    }
    else {
      /* tout en minuscule */
      /* On essaie d'abord une correction light */
      if (is_light_correction && call_a_light_spelling_correction (prefix_stack_ptr, mot_courant, lgth)) {
	if (print_error)
	  print_a_light_spelling_correction (mot_courant, lgth);
      }
      else {
	/* On fabrique l'automate initial du mot ... */
	is_a_spelling_correction.yes = SXTRUE;
	is_a_spelling_correction.word = mot_courant;
	is_a_spelling_correction.lgth = lgth;

	if (is_spelling_correction && call_sxspell (mot_courant, lgth))
	  id = 1;

	if (print_error) {
	  fputs ("\n", stdout);
	  
	  cur_car = mot_courant [lgth];
	  mot_courant [lgth] = SXNUL;

	  if (print_input) {
	    if (print_comment && varstr_length (vstr))
	      printf ("%s ", varstr_tostr (vstr));

	    printf ("[%s] ", mot_courant);
	  }
	
	  if (id == 0) {
	    if (print_comment && varstr_length (vstr))
	      printf ("%s ", varstr_tostr (vstr));

	    if (is_tokenizer) {
	      if (*mot_courant == sxtolower (*mot_courant))
		/* commence par une minuscule */
		fputs ("_uw", stdout);
	      else
		fputs ("_Uw", stdout);
	    }
	    else
	      printf ("<<<%s>>>", mot_courant);
	  }
	  else {
	    print_spelling_correction (&main_FSA);
	  }

	  mot_courant [lgth] = cur_car;
	}
	else {
	  if (print_output && !is_dry_run && id != 0) {
	    SXINT heavy_id, *bot_ptr, *top_ptr;

	    /* On extrait de main_FSA la meilleure correction */
	    /* On ne prend que le 1er resultat parmi les meilleurs */
	    heavy_id = SXSPELL_x2id (main_FSA, 1);
	    heavy_cost = SXSPELL_x2weight (main_FSA, 1);

	    varstr_raz (cvstr);
  
	    if (!SXSPELL_has_subwords (heavy_id)) {
	      if (print_comment && varstr_length (vstr))
		varstr_catenate (cvstr, varstr_tostr (cur_correction->vstr));

	      varstr_catenate (varstr_catenate (cvstr, SXSPELL_id2str (main_FSA, heavy_id)), " ");
	    }
	    else {
	      /* cas sous-mots */
	      SXSPELL_subword_foreach (main_FSA, heavy_id, bot_ptr, top_ptr) {
		if (print_comment && varstr_length (vstr))
		  varstr_catenate (cvstr, varstr_tostr (cur_correction->vstr));

		varstr_catenate (varstr_catenate (cvstr,
						  SXSPELL_id2str (main_FSA, SXSPELL_x2subword_id (bot_ptr))),
				 " ");
	      }
	    }
	  }
	}
      }
    }
  }

  return id;
}


/* str = "000<F 111>222</F>333" */

/*  On retourne "333" */ 
static char *
xml_rule (str)
     char * str;
{
  char *cur;

  cur = str;

  if (cur) cur = strchr (cur, (int)'<');
  if (cur) cur = strchr (cur, (int)'>');
  if (cur) cur = strchr (cur, (int)'<');
  if (cur) cur = strchr (cur, (int)'>');
  if (cur) cur++;

  return cur;
}

/*  On retourne "222</F>333" */ 
static char *
xml_body (str)
     char * str;
{
  char *cur;

  cur = str;

  if (cur) cur = strchr (cur, (int)'<');
  if (cur) cur = strchr (cur, (int)'>');
  if (cur) cur++;

  return cur;
}

static void
to_lower (str, length)
     char *str;
     SXINT length;
{
  while (length--) {
    *str = sxtolower (*str);
    str++;
  }
}

/* Cas special { <...>disp<...> <...>aru<...> } _ETR 
   mot         ^
   mot+lgth                                    ^
   On essaie de corriger "disp aru"
*/
static SXINT
call_spell_ETR (mot, lgth, is_first_word)
     char    *mot;
     SXINT     lgth;
     SXBOOLEAN is_first_word;
{
  SXINT  id, ste, length;
  char *pstr, *qstr, cur_char;
  
  /* On extrait "disp" "aru" ... */
  pstr = mot;
  id = 0;

  if (pstr = xml_body (mot)) {
    /* { <...>disp<...> <...>aru<...> } _ETR 
       pstr   ^
    */

    varstr_raz (vstr_ETR);

    if (qstr = strchr (pstr, (int)'<')) {
      /* { <...>disp<...> <...>aru<...> } _ETR 
	 qstr       ^
      */
      varstr_lcatenate (vstr_ETR, pstr, qstr-pstr);

      while (pstr = xml_body (qstr+1)) {
	/* { <...>disp<...> <...>aru<...> } _ETR 
	   pstr                  ^
	*/
	if (qstr = strchr (pstr, (int)'<')) {
	  /* { <...>disp<...> <...>aru<...> } _ETR 
	     qstr                     ^
	  */
	  /* Pour l'instant, je ne met pas de blanc entre "disp" et "aru" !! */
	  varstr_lcatenate (vstr_ETR, pstr, qstr-pstr);
	}
	else {
	  /* !! */
	  varstr_raz (vstr_ETR);
	  break;
	}
      }
    }

    if ((length = varstr_length (vstr_ETR))) {
      if (length < 17 /*  BS: pour ne pas perdre de temps � tenter de corriger ce qui est manifestement un _ETR */
	  && !has_upper (varstr_tostr (vstr_ETR), length)) { /* {<>The</> <>Star</>} _ETR */
	/* ste = sxstrsave (varstr_tostr (vstr_ETR)); */
	/*to_lower (varstr_tostr (vstr_ETR), length);  inutile avec le has_upper */
	id = call_spell (varstr_tostr (vstr_ETR), length, is_first_word);
      }
    }
  }
  else {
    /* On suppose qu'on est ds le cas {disp aru} */
    if (lgth-2 > 0) {
      if (lgth-2 < 17) { /*  BS: pour ne pas perdre de temps � tenter de corriger ce qui est manifestement un _ETR */
	vstr_ETR = varstr_lcatenate (varstr_raz (vstr_ETR), mot+1, lgth-2);
	if (!has_upper (varstr_tostr (vstr_ETR), lgth-2))/* {<>The</> <>Star</>} _ETR */
	  /*	to_lower (varstr_tostr (vstr_ETR), lgth-2);*/
	  id = call_spell (varstr_tostr (vstr_ETR), lgth-2, is_first_word);
      }
    }
  }

  if (id == 0)
    is_a_spelling_correction.yes = SXFALSE; /* On reinitialise */

  return id;
}



static void
string2set (string, set)
     unsigned char *string;
     SXBA          set;
{
  unsigned char uchar;

  while ((uchar = *string++))
    SXBA_1_bit (set, uchar);
}


static SXINT
output (correction)
     struct correction_struct *correction;
{
  SXINT i;
  char cur_car;

  /* On ecrit tout ... */
  if (print_input || print_output) {
    if (correction->is_first_word && !correction->is_first_line /* Le 22/03/05 */)
      fputs ("\n", stdout);
  }

  if (print_input) {
    if (print_comment && varstr_length (correction->vstr))
      printf ("%s ", varstr_tostr (correction->vstr));

    printf ("%s ", correction->word);
  }

  if (print_output) {
    if (is_dry_run) {
      if (print_comment && varstr_length (correction->vstr))
	printf ("%s ", varstr_tostr (correction->vstr));

      if (correction->id == 0)
	printf ("<<<%s>>> ", correction->word);
      else
	printf ("%s ", correction->word);
    }
    else {
      for (i = 1; i <= correction->prefix_stack.top; i++) {
	if (print_comment && varstr_length (correction->vstr))
	  printf ("%s ", varstr_tostr (correction->vstr));

	printf ("%s ", varstr_tostr (correction->prefix_stack.vstrs [i]));
      }
    
      if (correction->is_a_spelling_correction.yes) {
	if (correction->id == 0) {
	  if (print_comment && varstr_length (correction->vstr))
	    printf ("%s ", varstr_tostr (correction->vstr));

	  cur_car = correction->is_a_spelling_correction.word [correction->is_a_spelling_correction.lgth];
	  correction->is_a_spelling_correction.word [correction->is_a_spelling_correction.lgth] = SXNUL;

	  if (is_tokenizer) {
	    if (*correction->is_a_spelling_correction.word == sxtolower (*correction->is_a_spelling_correction.word))
	      /* commence par une minuscule */
	      fputs ("_uw ", stdout);
	    else
	      fputs ("_Uw ", stdout);
	  }
	  else
	    printf ("<<<%s>>> ", correction->is_a_spelling_correction.word);

	  correction->is_a_spelling_correction.word [correction->is_a_spelling_correction.lgth] = cur_car;
	}
	else {
	  printf ("%s", varstr_tostr (correction->cvstr));
	}
      }

      for (i = correction->suffix_stack.top; i > 0; i--) {
	if (print_comment && varstr_length (correction->vstr))
	  printf ("%s ", varstr_tostr (correction->vstr));

	printf ("%s ", varstr_tostr (correction->suffix_stack.vstrs [i]));
      }
    }
  }
}



SXINT
sxspeller_action (what, action_no)
    SXINT		what, action_no;
{
  struct sxtoken	*ptok;
  SXINT                   top, new_top, cur, id, i, word_lgth;
  char                  *pcom, *tcom, *word, cur_car, *final_comment;
  static SXBOOLEAN        is_first_word, is_first_line;
  SXBOOLEAN               is_first;

  switch (what) {
  case SXOPEN:
    break;

  case SXINIT:
    if (process_prefix) {
      /* On remplit qq ensembles ... */
      string2set (MAJ_string, MAJ_set);
      string2set (vowell_string, vowell_set);
      string2set (mpb_string, mpb_set);
    }

    for (which_correction = 0; which_correction <= 2; which_correction++) {
      cur_correction = corrections+which_correction;
      cur_correction->vstr = varstr_alloc (128);
      cur_correction->cvstr = varstr_alloc (128);
      alloc_vstr_stack_hd (&cur_correction->prefix_stack, 32);
      alloc_vstr_stack_hd (&cur_correction->suffix_stack, 32);
    }

    which_correction = 0;

    bivstr = varstr_alloc (128);
    wstr = varstr_alloc (128);
    vstr_ETR = varstr_alloc (128);
    vstr_mot_en_min = varstr_alloc (128);
    hyphen_set = sxba_calloc (128);

    if (is_light_correction) {
      sxspell_init (dico_des_mots,
		    &super_light_FSA,
		    (unsigned char)1 /* BEST */, 
		    max_weight, /* Pourquoi pas light_weight ?? */
		    light_compose_weight, /* A FAIRE : un super_light_compose_weight */
		    SXFALSE /* mots multiples : non */,
		    0/* ... de cout unitaire */);
    }

    if (is_light_correction) {
      sxspell_init (dico_des_mots,
		    &light_FSA,
		    (unsigned char)1 /* BEST */, 
		    max_weight, /* Pourquoi pas light_weight ?? */
		    light_compose_weight,
		    SXFALSE /* mots multiples : non */,
		    0/* ... de cout unitaire */);
    }

    /* Appel initial : on ne le fait qu'une fois */
    if (is_spelling_correction) {
      sxspell_init (dico_des_mots,
		    &main_FSA,
		    (unsigned char)1 /* BEST */, 
		    max_weight,
		    compose_weight,
		    is_sub_word /* mots multiples ? */,
		    sub_word_weight /* ... de cout unitaire */);
    }

    is_first_line = SXTRUE;
    prev_correction = NULL;

    break;

  case SXACTION:
    switch (action_no) {
    case 0:
      return 1;

    case 1:
      /* <word>			= %WORD ; 1 */
      cur_correction = corrections+which_correction;

      ptok = &(SXSTACKtoken (SXSTACKtop ()));
      cur_correction->word = word = sxstrget (ptok->string_table_entry);
      word_lgth = strlen (word);
	  
      vstr = cur_correction->vstr;
      cvstr = cur_correction->cvstr;

      varstr_raz (vstr);
      /* vstr doit etre positionne' ds le cas print_error */

      if ((pcom = ptok->comment)) {
	is_first_word = (strchr (pcom, (int)'\n') != NULL);
	pcom = ptok->comment;

	if ((pcom = strchr (pcom, (int)'{'))) {
	  tcom = pcom;

	  if ((tcom = strrchr (tcom, (int)'}'))) {
	    /* from_right */
	    if (print_comment)
	      varstr_lcatenate (vstr, pcom, tcom-pcom+1);
	  }
	  else
	    pcom = NULL;
	}
	else {
	  pcom = NULL;
	}
      }
      else {
	is_first_word = is_first_line ? SXTRUE : SXFALSE;
      }

      cur_correction->is_first_line = is_first_line;

      if (is_first_line) {
	is_first_line = SXFALSE;
      }

      RAZ_VSTR_STACK (cur_correction->prefix_stack);
      RAZ_VSTR_STACK (cur_correction->suffix_stack);
      is_a_spelling_correction.yes = SXFALSE;

      cur_correction->is_first_word = is_first_word;
      prefix_stack_ptr = &(cur_correction->prefix_stack);
      suffix_stack_ptr = &(cur_correction->suffix_stack);
      cur_correction->cost = 0;

      id = 0;
      light_cost = heavy_cost = 0;

      if (pcom && word_lgth >= 4 && strncmp (word, "_ETR", 4) == 0) {
	/* On est ds le cas special :
	   { <...>disp<...> <...>aru<...> } _ETR 
	   ou` on va essayer de corriger non pas _ETR mais "disp aru"
	   Si ca marche on retourne { <...>disp<...> <...>aru<...> } disparu
	   sinon { <...>disp<...> <...>aru<...> } _ETR
	   ds le cas dry_run, on retourne
	   { <...>disp<...> <...>aru<...> } _ETR ou
	   { <...>disp<...> <...>aru<...> } <<<_ETR>>>
	*/
	id = call_spell_ETR (pcom, tcom-pcom+1, is_first_word);
      }
      
      if (id == 0) {
	  id = call_spell (word, word_lgth, is_first_word);
      }

      cur_correction->id = id;
      cur_correction->cost = light_cost+heavy_cost; /* l'un des 2 au moins est nul */
      cur_correction->is_a_spelling_correction = is_a_spelling_correction;

      if (is_glue) {
	if (!print_error && id && *word == '_') {
	  /* Cas des meta ff (qui marchent forcement) */
	  if (prev_correction != NULL) {
	    prev_correction = NULL;
	    output (prev_correction);
	  }
	  
	  output (cur_correction);
	}
	else {
	  if (prev_correction == NULL) {
	    prev_correction = cur_correction;
	  }
	  else {
	    if (!print_error) {
	      SXINT prev_cur_cost;

	      prev_cur_cost = (prev_correction->cost + cur_correction->cost);
	      
	      if (prev_correction->id != 0 && cur_correction->id != 0 && prev_cur_cost == 0) {
		/* precedent et courant OK */
		output (prev_correction);
		prev_correction = cur_correction;
	      }
	      else {
		/* Au moins l'un des 2 a necessite' de la correction, on regarde ce que donne la concatenation des 2 */
		which_correction++;

		if (which_correction > 2)
		  which_correction = 0;

		bi_correction = corrections+which_correction;
		/* Attention : prev_correction->word peut pointer vers bivstr */
		/* tres laid */
		if (prev_correction->word == bivstr->first)
		  varstr_catenate (bivstr, cur_correction->word);
		else {
		  varstr_raz (bivstr);
		  varstr_catenate (varstr_catenate (bivstr, prev_correction->word), cur_correction->word);
		}

		bi_correction->word = word = varstr_tostr (bivstr);
		word_lgth = strlen (word);
	  
		vstr = bi_correction->vstr;
		cvstr = bi_correction->cvstr;

		varstr_raz (vstr);

		RAZ_VSTR_STACK (bi_correction->prefix_stack);
		RAZ_VSTR_STACK (bi_correction->suffix_stack);
		is_a_spelling_correction.yes = SXFALSE;

		bi_correction->is_first_word = SXFALSE;
		prefix_stack_ptr = &(bi_correction->prefix_stack);
		suffix_stack_ptr = &(bi_correction->suffix_stack);

		id = 0;
		light_cost = heavy_cost = 0;

		id = call_spell (word, word_lgth, prev_correction->is_first_word);
      
		bi_correction->id = id;
		bi_correction->cost = light_cost+heavy_cost; /* l'un des 2 au moins est nul */
		bi_correction->is_a_spelling_correction = is_a_spelling_correction;

		if (id == 0 || prev_cur_cost <= bi_correction->cost) {
		  output (prev_correction);
		  prev_correction = cur_correction;

		  if (which_correction == 0)
		    which_correction = 2;
		  else
		    which_correction--;
		}
		else {
		  /* On tente des concatenations multiples !! */
		  /* Attention a bivstr */
		  prev_correction = bi_correction;
		}
	      }
	    }
	  }
	}

	/* On incremente sur l'anneau des corrections */
	which_correction++;

	if (which_correction > 2)
	  which_correction = 0;
      }
      else {
	output (cur_correction);
      }
    
      fflush (NULL);

      return 1;

    case 2:
      /* <axiome>		= <sxspeller> ; 2 */
      if (!print_error) {
	if (prev_correction != NULL)
	  output (prev_correction);

	/* fputs ("\n", stdout); */
      }

      if ((final_comment = SXGET_TOKEN (sxplocals.Mtok_no).comment))
	printf ("%s", final_comment);

      return 1;
      
    case 3:
      fputs ("\n", stdout);
      return 1;

    default:
      fputs ("The function \"sxspeller_action\" is out of date with respect to its specification.\n", sxstderr);
      abort ();
    }

    break;

  case SXERROR:
    is_error = SXTRUE;
    break;

  case SXFINAL:
    for (which_correction = 0; which_correction <= 2; which_correction++) {
      cur_correction = corrections+which_correction;
      varstr_free (cur_correction->vstr), cur_correction->vstr = NULL;
      varstr_free (cur_correction->cvstr), cur_correction->cvstr = NULL;
      free_vstr_stack_hd (&cur_correction->prefix_stack);
      free_vstr_stack_hd (&cur_correction->suffix_stack);
    }

    varstr_free (bivstr), bivstr = NULL;
    varstr_free (wstr), wstr = NULL;
    varstr_free (vstr_ETR), vstr_ETR = NULL;
    varstr_free (vstr_mot_en_min), vstr_mot_en_min = NULL;

    sxfree (hyphen_set), hyphen_set = NULL;

    /* ... et on libere */
    if (is_spelling_correction) {
      sxspell_final_fsa (&main_FSA);
      sxspell_final (&main_FSA);
    }

    if (is_light_correction) {
      sxspell_final_fsa (&light_FSA);
      sxspell_final (&light_FSA);
    }

    if (is_light_correction) { /* A FAIRE : un is_super_light_correction */
      sxspell_final_fsa (&super_light_FSA);
      sxspell_final (&super_light_FSA);
    }

    break;

  case SXCLOSE:
  case SXSEMPASS:
    break;

  default:
    fputs ("The function \"sxspeller_action\" is out of date with respect to its specification.\n", sxstderr);
    abort ();
  }

  return 0;
}


int
main (argc, argv)
    int		argc;
    char	*argv [];
{
  SXINT	argnum, kind;

  if (sxstdout == NULL) {
    sxstdout = stdout;
  }

  if (sxstderr == NULL) {
    sxstderr = stderr;
  }

#ifdef BUG
  /* Suppress bufferisation, in order to have proper	 */
  /* messages when something goes wrong...		 */

  setbuf (stdout, NULL);
#endif /* BUG */

  /* valeurs par defaut */
  max_weight = UNBOUNDED; /* on regarde toutes les corrections */
  is_sub_word = SXFALSE; /* On ne cherche pas a decouper en sous_mots */
  sub_word_weight = 0; /* Poids de la correction pour un decoupage */

  /* Par defaut on suppose que l'on corrige un texte => */
  print_input = SXFALSE; /* On ne reecrit pas le source ... */
  print_output = SXTRUE; /* On ecrit le mot apres traitement ... */
  print_error = SXFALSE; /* ... ds tous les cas */
  print_comment = SXFALSE;
  check_lower = SXFALSE; /* On ne cherche pas le mot en minuscule */
  is_process_lower = SXFALSE;
  is_cut_on_quote = SXFALSE;
  is_cut_on_hyphen = SXFALSE;
  process_prefix = SXFALSE;
  process_suffix = SXFALSE;
  is_light_correction = SXFALSE; /* pas de correction "light" */
  light_weight = 0; 
  is_tokenizer = SXFALSE;
  is_spelling_correction = SXTRUE; /* Si tout a echoue', on tente une correction complete */
  is_dry_run = SXFALSE; /* SXTRUE => Un mot est ressorti tel quel s'il est possible de le corriger sinon il est sorti entre <<< et >>> */
  is_glue = SXFALSE; /* SXTRUE => On essaie une correction en recollant les mots successifs */

  /* Decodage des options */
  for (argnum = 1; argnum < argc; argnum++) {
    switch (kind = option_get_kind (argv [argnum])) {
    case HELP:
      fprintf (sxstderr, Usage, ME);
      return 0;

    case WEIGHT:
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (WEIGHT));
	SXEXIT (3);
      }
	     
      max_weight = atoi (argv [argnum]);

      break;

    case SUB_WORD_WEIGHT:
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (SUB_WORD_WEIGHT));
	SXEXIT (3);
      }
	     
      is_sub_word = SXTRUE;
      sub_word_weight = atoi (argv [argnum]);

      break;

    case PRINT_INPUT:
      print_input = SXTRUE;
      break;

    case PRINT_OUTPUT:
      print_output = SXTRUE;
      break;

    case PRINT_ERROR:
      print_error = SXTRUE;
      break;

    case PRINT_COMMENT:
      print_comment = SXTRUE;
      break;

    case CHECK_LOWER:
      check_lower = SXTRUE;
      break;

    case PROCESS_LOWER:
      is_process_lower = SXTRUE;
      break;

    case CUT_ON_QUOTE:
      is_cut_on_quote = SXTRUE;
      break;

    case CUT_ON_HYPHEN:
      is_cut_on_hyphen = SXTRUE;
      break;

    case PROCESS_PREFIX:
      process_prefix = SXTRUE;
      break;

    case PROCESS_SUFFIX:
      process_suffix = SXTRUE;
      break;

    case LIGHT_WEIGHT:
      is_light_correction = SXTRUE;
      
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (LIGHT_WEIGHT));
	SXEXIT (3);
      }
	     
      light_weight = atoi (argv [argnum]);
      break;

    case SPELLING_CORRECTION:
      is_spelling_correction = SXTRUE;
      break;

    case -SPELLING_CORRECTION:
      is_spelling_correction = SXFALSE;
      break;

    case DRY_RUN:
      is_dry_run = SXTRUE;
      break;

    case GLUE:
      is_glue = SXTRUE;
      break;

    case TOKENIZER:
      is_tokenizer = SXTRUE;
      print_input = SXFALSE;
      print_comment = SXTRUE;
      print_output = SXTRUE;
      print_error = SXFALSE;
      break;

    case UNKNOWN_ARG:
      if (argnum < argc-1) {
	fprintf (sxstderr, "%s: unknown option \"%s\";\n", ME, argv [argnum]);
	fprintf (sxstderr, Usage, ME);
	SXEXIT (3);
      }

      if (argv [argnum] [0] == '-' && strlen (argv [argnum]) > 1) {
	/* -blabla => erreur */
	fprintf (sxstderr, "%s: invalid source pathname \"%s\".\n", ME, argv [argnum]);
	SXEXIT (3);
      }

      break;
    }

    if (kind == UNKNOWN_ARG)
      break;
  }

  sxopentty ();
  sxstr_mngr (SXBEGIN) /* Creation de la table des chaines */ ;
  (*(sxtables.analyzers.parser)) (SXBEGIN) /* Allocation des variables globales du parser */ ;
  syntax (SXOPEN, &sxtables) /* Initialisation de SYNTAX (mono-langage) */ ;

  if (argnum == argc /* stdin (sans -) */ ||
      strcmp (argv [argnum], "-") == 0) {

    sxsrc_mngr (SXINIT, stdin, "");
    sxerr_mngr (SXBEGIN);
    syntax (SXACTION, &sxtables);
    sxsrc_mngr (SXFINAL);
    sxerr_mngr (SXEND);
    /* clearerr (stdin); si utilisation repetee de "stdin" */
  }
  else {
    /* argv [argnum] est le source */
    FILE	*infile;

    if ((infile = sxfopen (argv [argnum], "r")) == NULL) {
      fprintf (sxstderr, "%s: Cannot open (read) ", argv [argnum]);
      sxperror (argv [argnum]);
      sxerrmngr.nbmess [SXSEVERITIES - 1]++;
    }
    else {
      sxsrc_mngr (SXINIT, infile, argv [argnum]);
      sxerr_mngr (SXBEGIN);
      syntax (SXACTION, &sxtables);
      sxsrc_mngr (SXFINAL);
      sxerr_mngr (SXEND);
      fclose (infile);
    }
  }

  syntax (SXCLOSE, &sxtables);
  (*(sxtables.analyzers.parser)) (SXEND) /* Inutile puisque le process va etre termine */ ;
  sxstr_mngr (SXEND) /* Inutile puisque le process va etre termine */ ;

  {
    SXINT	severity;

    for (severity = SXSEVERITIES - 1; severity > 0 && sxerrmngr.nbmess [severity] == 0; severity--) {
      /* null; */
    }

    SXEXIT (severity);
  }
}

#if 0
VOID	sxvoid ()
/* procedure ne faisant rien */
{
}

SXBOOLEAN	sxbvoid ()
/* procedure ne faisant rien */
{
    return 0;
}
#endif /* 0 */
