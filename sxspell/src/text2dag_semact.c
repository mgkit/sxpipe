/* ********************************************************
   *                                                      *
   *                                                      *
   * Copyright (c) 2003 by Institut National de Recherche *
   *                    en Informatique et en Automatique *
   *                                                      *
   *                                                      *
   ******************************************************** */




/* ********************************************************
   *                                                      *
   *     Produit de l'equipe ATOLL.			  *
   *                                                      *
   ******************************************************** */
/* Action semantiques du processeur qui prend en entrees des phrases reelles
   (sequences de formes flechies) et qui produit en sortie un dag de symboles terminaux
   conformement a la specif contenue ds l'include text2dag_h
   text2dag_h a ete (pour le moment) fabriquer par l'execution de
   ./lfg_lex.out sur une specif lexicale a la lfg.
   C'est un dico qui a chaque forme flechie associe un ensemble de symboles terminaux
   De plus on peut avoir en entree une specif de mots composes et d'amalgames.
   Ds ce cas, ce module doit etre compile avec l'option -DCOMPOUND. */

/************************************************************************/
/* Historique des modifications, en ordre chronologique inverse:	*/
/************************************************************************/
/* Jeu 27 Nov 2003 16:50(PB)	Ajout de cette rubrique "modifications"	*/
/************************************************************************/

#define HAS_CMPD_OR_AMLGM


static char	WHAT_TEXT2DAG_SEMACT [] = "@(#)text2dag_semact.c\t- SYNTAX [unix] - Jeu 27 Nov 2003 16:50";

static char	ME [] = "text2dag_semact.c";

#include "sxunix.h"
#include <stdlib.h>

#include "varstr.h" /* avant les dicos pour bien tout inclure ce dont on a besoin dans fsa.h */

#ifndef def_sxdfa_comb_inflected_form
#define def_sxdfa_comb_inflected_form
#endif /* def_sxdfa_comb_inflected_form */

/* le dico des formes flechies et des mots-composes */
extern struct sxdfa_comb if_dico;
static struct sxdfa_comb *dico_des_ff = &(if_dico);

#ifdef has_co_dico
/* le dico des formes flechies et des mots-composes */
extern struct sxdfa_comb if_co_dico;
static struct sxdfa_comb *dico_des_ff_checkonly = &(if_co_dico);
#endif /* has_co_dico */

/* le dico des formes flechies et des mots-composes */
extern struct sxdfa_comb cc_dico;
static struct sxdfa_comb *dico_des_mots = &(cc_dico);


#define sxdfa_comb_inflected_form_base2stack  ac_base2stack
#define sxdfa_comb_inflected_form_char_stack_list  ac_char_stack_list
#include dico_ac_h
/* le dico des amalgams et compounds */
static struct sxdfa_comb *dico_des_ac = &(lhs_acc_name_comb);
#undef sxdfa_comb_inflected_form
#undef sxdfa_comb_inflected_form_char2class
#undef sxdfa_comb_inflected_form_comb_vector
#undef sxdfa_comb_inflected_form_base2stack
#undef sxdfa_comb_inflected_form_char_stack_list
#undef EOF_CODE
#undef MAKE_A_DICO_TIME

#define sxdfa_comb_inflected_form             dico_prfx
#define sxdfa_comb_inflected_form_char2class  prfx_char2class
#define sxdfa_comb_inflected_form_comb_vector prfx_comb_vector
/* le dico des prefixes remarquables */
#include dico_prefix_h
static struct sxdfa_comb *dico_des_prefixes = &(dico_prfx);
#undef sxdfa_comb_inflected_form
#undef sxdfa_comb_inflected_form_char2class
#undef sxdfa_comb_inflected_form_comb_vector
#undef EOF_CODE
#undef MAKE_A_DICO_TIME

#define sxdfa_comb_inflected_form             dico_sffx
#define sxdfa_comb_inflected_form_char2class  sffx_char2class
#define sxdfa_comb_inflected_form_comb_vector sffx_comb_vector
/* le dico des suffixes remarquables */
#include dico_suffix_h
static struct sxdfa_comb *dico_des_suffixes = &(dico_sffx);
#undef sxdfa_comb_inflected_form
#undef sxdfa_comb_inflected_form_char2class
#undef sxdfa_comb_inflected_form_comb_vector
#undef EOF_CODE
#undef MAKE_A_DICO_TIME


#include "XxYxZ.h"
#include "sxstack.h"

extern SXINT     max_weight, sub_word_weight, light_weight, super_light_weight, max_corr_nb, max_corr_weight_nb; 
extern SXBOOLEAN is_sub_word, print_comment, check_lower, is_tokenizer, is_process_lower, is_cut_on_quote; 
extern SXBOOLEAN process_prefix, process_suffix, is_cut_on_hyphen, is_light_correction, is_super_light_correction, is_spelling_correction, is_glue, is_output_uw, is_output_weights, is_change_only_case_and_diacritics, is_keep_original_tokens;
extern FILE      *trace_file;

static SXINT          real_tok_no, tok_no, init_tok_no, eol_tok_no;
static VARSTR       vstr, cc_vstr, cvstr, wstr, wvstr, bivstr, vstr_mot_en_min, vstr_ETR, mvstr, ffwc_vstr;
static char          *MOT_A_CORRIGER;

static SXINT bot_tok; 

#include "sxword.h"

#define MEMOIZE_MORE 0

#if MEMOIZE_MORE
#ifndef MEMO_THRSLD
#define MEMO_THRSLD 10
#endif /* ndef MEMO_THRSLD */
#if MEMO_THRSLD > 30
#error MEMO_THRSLD must be <= 30
#endif

#if MEMO_THRSLD > 0
static sxword_header sxspeller_input_strings;
static SXUINT *input_ste2memo_status;
static SXUINT cas2word_offset [] = {0, 0, 0, 0, 0, 0,
                               1, 1, 1, 1, 1, 1};
static SXUINT cas2data_mask [] = {0X1F, 0X1F << 5, 0X1F << 10, 0X1F << 15, 0X1F << 20, 0X1F << 25,
                             0X1F, 0X1F << 5, 0X1F << 10, 0X1F << 15, 0X1F << 20, 0X1F << 25};
static SXUINT cas2data_incr [] = {1, 1 << 5, 1 << 10, 1 << 15, 1 << 20, 1 << 25,
                             1, 1 << 5, 1 << 10, 1 << 15, 1 << 20, 1 << 25};
static SXUINT cas2data_max [] = {MEMO_THRSLD, MEMO_THRSLD << 5, MEMO_THRSLD << 10, MEMO_THRSLD << 15, MEMO_THRSLD << 20, MEMO_THRSLD << 25,
                             MEMO_THRSLD, MEMO_THRSLD << 5, MEMO_THRSLD << 10, MEMO_THRSLD << 15, MEMO_THRSLD << 20, MEMO_THRSLD << 25};
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
static sxword_header sxspeller_input_strings;
static SXBA *input_ste2memo_status;
#endif /* MEMOIZE_MORE */


#define SXBA_fdcl(n,l)	SXBA_ELT n [SXNBLONGS (l) + 1] = {l,}
static SXBA_fdcl (MAJ_set, 256);
static SXBA_fdcl (vowell_set, 256);
static SXBA_fdcl (mpb_set, 256);

static SXBA          hyphen_set;


static char *action2prefix_chars [] = {"",
				       /* 1 */ "_",
				       /* 2 */ "-_",
				       /* 3 */ "e-_",
				       /* 4 */ "e-_",
				       /* 5 */ "_",
				       /* 6 */ "_",
};

static char *action2suffix_chars [] = {"",
				       /* 1 */ "_",
				       /* 2 */ "_",
				       /* 3 */ "_",
				       /* 4 */ "_",
				       /* 5 */ "_",
				       /* 6 */ "_",
};


#ifdef HAS_CMPD_OR_AMLGM

static SXINT          meta_ste, from_tok_no, to_tok_no,Uw_ste;
static SXINT          max_acc_dag_state;
static char           *CUR_COMMENT;
static SXINT          CUR_WEIGHT;
static SXBOOLEAN      has_new_cc, HAS_AN_UPPER_CASE_LETTER;
static XxYxZ_header acc_dag, if_dag;
static XxY_header acc_trans_hd;
static SXINT        *acc_dag2comment_ste, *if_weight;
static char         **acc_comment, **if_comment;
#if 0
static XxYxZ_header amalgam_dag, compound_component_dag;
static char         **amalgam_comment, **compound_component_comment, **if_comment;
static SXINT          *amalgam_stack, *initial_compound_component_stack;
static SXINT          cc_stack [MAX_CC_LIST_LGTH+1];
static char         *cc_comment_stack [MAX_CC_LIST_LGTH+1];
#endif  /* 0 */

static SXBOOLEAN      is_first_call_to_output_if_id, correction_starts_with_a_capital, is_first_cc;
static SXINT          first_super_light_if_id;
static char         *CUR_STR;

static struct vstr_stack_hd {
  SXINT     top, size;
  VARSTR  *vstrs;
} *prefix_stack_ptr, *suffix_stack_ptr;

#define RAZ_VSTR_STACK(t) (t).top=0

static struct is_a_spelling_correction {
  char    *word;
  SXINT     lgth;
  SXBOOLEAN yes;
} is_a_spelling_correction;

struct correction_struct {
  SXINT                             id, cost;
  VARSTR                          vstr /* le commentaire associ� (sic) */, cvstr /* best correction */;
  struct is_a_spelling_correction is_a_spelling_correction;
  char                            *word;
  SXBOOLEAN                         is_first_word;
  struct vstr_stack_hd            prefix_stack, suffix_stack;
};

static struct correction_struct corrections [3], *prev_correction, *cur_correction, *bi_correction, *saved_cur_correction;
static SXINT light_cost, heavy_cost, global_cost;

static SXINT *if_stack, *comments_stack;

static VARSTR       SPELLED_VSTR;

/* FSA de l'�tape sxspeller */
static struct FSA   std_FSA, light_FSA, super_light_FSA;
/* FSA du niveau text2dag */
static struct FSA   main_FSA, cmpnd_FSA, if_FSA, light_cmpnd_FSA;







/* On stocke ds la pile des suffixes la sous-chaine */
static void
push_string (stack, lstr, rstr)
     struct vstr_stack_hd *stack;
     char                 *lstr, *rstr;
{
  SXINT    lgth;
  VARSTR vstr;

  lgth = rstr-lstr;

  if (++stack->top > stack->size) {
    stack->vstrs = (VARSTR *) sxrecalloc (stack->vstrs, stack->size+1, 2*stack->size+1, sizeof (VARSTR));
    stack->size *= 2;
  }

  if (stack->vstrs [stack->top] == NULL)
    vstr = stack->vstrs [stack->top] = varstr_alloc (32+lgth);
  else {
    /* y-a-t'il assez de place ? */
    vstr = varstr_raz (stack->vstrs [stack->top]);

    while (vstr->current+lgth+1 >= vstr->last)
      vstr = varstr_realloc (vstr);
  }

  varstr_lcatenate (vstr, lstr, lgth);
}

static void
alloc_vstr_stack_hd (stack, size)
     struct vstr_stack_hd *stack;
     SXINT                  size;
{
  stack->size = size;
  stack->vstrs = (VARSTR *) sxcalloc (stack->size+1, sizeof (VARSTR));
  stack->top = 0;
}

static void
free_vstr_stack_hd (stack)
     struct vstr_stack_hd *stack;
{
  VARSTR  vstr, *p = stack->vstrs, *top = stack->vstrs+stack->size;

  while (++p <= top) {
    if ((vstr = *p) == NULL)
      break;
      
    varstr_free (vstr);
  }

  sxfree (stack->vstrs), stack->vstrs = NULL;
}

/* Il faut echapper les caracteres "{}()[]%|\" par un \ et mettre un commentaire partout o� un blanc
   est trouv�, ainsi qu'au d�but */
static VARSTR
save_ff_with_comment (local_vstr, str)
     char *str;
     VARSTR local_vstr;
{ 
  SXINT  lgth, pref_lgth;
  char c;
  static char tmp_str[128];

  lgth = strlen (str);

  if (CUR_COMMENT != NULL) {
    local_vstr = varstr_catenate (local_vstr, CUR_COMMENT);
    local_vstr = varstr_catchar (local_vstr, ' ');
  }

  while (lgth) {
    pref_lgth = strcspn (str, "+*?{}()[]%|\\ ");

    if (pref_lgth == lgth) {
      local_vstr = varstr_catenate (local_vstr,  str);
      break;
    }

    if (pref_lgth == 0) {
      if (str[pref_lgth] == ' ') {
	if (CUR_COMMENT != NULL) {
	  local_vstr = varstr_catenate (local_vstr, CUR_COMMENT);
	  local_vstr = varstr_catchar (local_vstr, ' ');
	}
      } else {
	local_vstr = varstr_catchar (local_vstr,  '\\');
	local_vstr = varstr_catchar (local_vstr, *str);
      }
      str++;
      lgth--;
    }
    else {
      c = str [pref_lgth];
      str [pref_lgth] = SXNUL;
      local_vstr = varstr_catenate (local_vstr, str);
      if (c == ' ') {
	if (CUR_COMMENT != NULL)
	  local_vstr = varstr_catchar (local_vstr, ' ');
	  local_vstr = varstr_catenate (local_vstr, CUR_COMMENT);
	  local_vstr = varstr_catchar (local_vstr, ' ');
      } else {
	local_vstr = varstr_catchar (local_vstr, '\\');
	local_vstr = varstr_catchar (local_vstr, c);
      }
      str [pref_lgth] = c;
      str += pref_lgth+1;
      lgth -= pref_lgth+1;
    }
  }

  varstr_complete (local_vstr);

  if (is_output_weights) {
    sprintf (tmp_str, " [|%ld|]", CUR_WEIGHT);
    varstr_catenate (local_vstr, tmp_str);
  }

  return local_vstr;
}

static void
output_ff_with_comment (str)
     char *str;
{
  printf ("%s", varstr_tostr (save_ff_with_comment (varstr_raz (ffwc_vstr), str)));
}

static VARSTR
escape_catenate (vstr, str)
     VARSTR vstr;
     char   *str;
{
  SXINT  lgth, pref_lgth;

  lgth = strlen (str);

  while (lgth) {
    pref_lgth = strcspn (str, "+*?{}()[]%|\\");

    if (pref_lgth == lgth) {
      vstr = varstr_catenate (vstr, str);
      break;
    }

    if (pref_lgth) {
      vstr = varstr_lcatenate (vstr, str, pref_lgth);
      str += pref_lgth;
      lgth -= pref_lgth;
    }
      
    vstr = varstr_lcatenate (vstr, "\\", 1);
    vstr = varstr_catchar (vstr, *str);
    str++;
    lgth--;
  }

  return varstr_complete (vstr);
}



static void
acc_dag_oflw (old_size, new_size)
     SXINT old_size, new_size;
{
  acc_dag2comment_ste = (SXINT*) sxrealloc (acc_dag2comment_ste, new_size+1, sizeof (SXINT));
}

#if 0
static void
amalgam_dag_oflw (old_size, new_size)
     SXINT old_size, new_size;
{
  amalgam_stack = (SXINT*) sxrealloc (amalgam_stack, new_size+1, sizeof (SXINT));
  amalgam_comment = (char**) sxrealloc (amalgam_comment, new_size+1, sizeof (char*));
}

static void
compound_component_dag_oflw (old_size, new_size)
     SXINT old_size, new_size;
{
  initial_compound_component_stack = (SXINT*) sxrealloc (initial_compound_component_stack,
						       new_size+1,
						       sizeof (SXINT));
  compound_component_comment = (char**) sxrealloc (compound_component_comment, new_size+1, sizeof (char*));
}
#endif /* 0 */

static void
if_dag_oflw (old_size, new_size)
     SXINT old_size, new_size;
{
  if_comment = (char**) sxrealloc (if_comment, new_size+1, sizeof (char*));
  if (is_output_weights)
    if_weight = (SXINT*) sxrecalloc (if_weight, old_size+1, new_size+1, sizeof (SXINT));
}

#include "sxspell.h"
#include "sxspell_rules.h"


static SXBOOLEAN
has_upper (str, lgth)
     char *str;
     SXINT lgth;
{
  char    car;

  while (lgth--) {
    car = *str++;

#ifdef latin1
    if (car != sxtolower (car))
#else
    if ((strchr(MAJ_string, car)))
#endif /* koi8r */
      return SXTRUE;
  }

  return SXFALSE;
}

static SXBOOLEAN
is_upper (car)
     char car;
{
#ifdef latin1
  if (car != sxtolower (car))
#else
  if ((strchr(MAJ_string, car)))
#endif /* koi8r */
    return SXTRUE;

  return SXFALSE;
}

static SXBOOLEAN
is_lower (car)
     char car;
{
#ifdef latin1
  if (car != sxtoupper (car))
#else
  if ((strchr(MIN_string, car)))
#endif /* koi8r */
    return SXTRUE;

  return SXFALSE;
}

static char
ttdtolower (car)
     char car;
{
#ifdef latin1
  return sxtolower (car);
#else
  SXINT n;
  SXINT p;

  p = (SXINT)strchr(MAJ_string, car);
  if (p) {
    n = p-(SXINT)MAJ_string;
    return MIN_string[n];
  }
  return car;
#endif /* koi8r */
}

static char
ttdtoupper (car)
     char car;
{
#ifdef latin1
  return sxtoupper (car);
#else
  SXINT n;
  SXINT p;

  p = (SXINT)strchr(MIN_string, car);
  if (p) {
    n = p-(SXINT)MIN_string;
    return MAJ_string[n];
  }
  return car;
#endif /* koi8r */
}

/* id est l'identifiant d'une forme flechie qui est le source ou une correction super_light du source */
static SXBOOLEAN
store_if_str (char *str, SXINT weight)
{
  SXINT            triple;
  static SXBOOLEAN has_UCL = SXFALSE;

  if (str) {
    /* forme flechie */
    if (has_upper (str, strlen (str)))
      has_UCL = SXTRUE;

    XxYxZ_set (&if_dag, max_acc_dag_state, sxstrsave (str), max_acc_dag_state+1, &triple);
    if_comment [triple] = CUR_COMMENT;
    if (is_output_weights)
      if_weight [triple] = weight;
  }
  else {
    /* dernier appel */
    /* On regarde s'il faut ajouter Uw */
    if (HAS_AN_UPPER_CASE_LETTER /* Le mot, avant correction contenait au moins une majuscule */
	&& !has_UCL /* Aucune de ses corrections ne contient une majuscule */
	&& real_tok_no > 1 /* on �vite de surg�n�rer sur le 1er mot de la phrase */
	) {
      /* On ajoute donc la transition sur le mot inconnu */
      if (is_output_uw)
	XxYxZ_set (&if_dag, max_acc_dag_state, Uw_ste, max_acc_dag_state+1, &triple);
      else
	XxYxZ_set (&if_dag, max_acc_dag_state, sxstrsave (CUR_STR), max_acc_dag_state+1, &triple);
      if_comment [triple] = CUR_COMMENT;
      if (is_output_weights)
	if_weight [triple] = weight;
    }
    else
      /* Pour le coup d'apres */
      has_UCL = SXFALSE;
  }
}

static SXINT
store_if_id (x, FSA_ptr)
     SXINT x;
     struct FSA *FSA_ptr;
{
  /* ne g�re pas un r�sultat de correction en plusieurs mots (sub_word_weight) */
  SXINT id = 0, weight = 0, *bot_ptr, *top_ptr;
  
  if (x == 0)
    store_if_str (NULL, 0);
  else {
    id = SXSPELL_x2id (*FSA_ptr, x);
    weight = SXSPELL_x2weight (*FSA_ptr, x);
    weight = weight > 0 ? weight : -weight;

    if (SXSPELL_has_subwords (id)) { /* correction en plusieurs sous-mots (id < 0): on donne cc1
					{comment} cc2 ... {comment} ccMAX */
      if (mvstr)
	varstr_raz (mvstr);
      else
	mvstr = varstr_alloc (512);
      
      SXSPELL_subword_foreach (*FSA_ptr, id, bot_ptr, top_ptr) {
	mvstr = varstr_catenate (mvstr, SXSPELL_id2str (*FSA_ptr, SXSPELL_x2subword_id (bot_ptr)));
	if (!SXSPELL_last_subword(bot_ptr, top_ptr))
	  mvstr = varstr_catchar (mvstr, ' ');
      }
      
      varstr_complete (mvstr);
      store_if_str (varstr_tostr (mvstr), weight);
      
    } else /* correction mono-mot, on transmet ledit mot tel quel */
      store_if_str (SXSPELL_id2str (*FSA_ptr, id), weight);
  }
  return id;
}

static SXINT
make_a_new_trans (SXINT lhs_acc_id, char *string, SXINT length)
{
  SXINT            comment_id, string_id, triple, acc_trans_id; 

  if (string == NULL)
    string_id = 0;
  else {
    string_id = sxstr2save (string, length);
  }

  XxY_set (&acc_trans_hd, lhs_acc_id, string_id, &acc_trans_id);

  return acc_trans_id;
}

/* id est l'identifiant d'un amalgame ou un composant d'un mot compose' qui est le source ou une correction
   super_light du source */
static void
store_amlgm_or_cmpnd_id (id, str, lgth)
     SXINT id;
     char *str;
     SXINT lgth;
{
  SXINT triple;
  SXINT comment_id;

  if (id) {
    id = make_a_new_trans (id, str, lgth);

    if (CUR_COMMENT == NULL || CUR_COMMENT [0] == '\0')
      comment_id = 0;
    else {
      comment_id = sxstrsave (CUR_COMMENT);
    }

    XxYxZ_set (&acc_dag, max_acc_dag_state, id, max_acc_dag_state+1, &triple);
    acc_dag2comment_ste [triple] = comment_id;
  }
}

static SXINT
amlgm_or_cmpnd_id (x, FSA_ptr)
     SXINT x;
     struct FSA *FSA_ptr;
{
  SXINT id = 0, lgth;
  char *str;

  if (x)
    id = SXSPELL_x2id (*FSA_ptr, x);
  if (id) {
    str = SXSPELL_id2str (cmpnd_FSA, id);
    lgth = strlen (str);
    if (lgth)
      store_amlgm_or_cmpnd_id (sxdfa_comb_get (dico_des_ac, str, lgth), str, lgth);
    else
      id = 0;
  }
  return id;
}


/* Si str == NULL => fin de la sortie */
static void
output_if_str (char *str, SXINT weight)
{
  static SXINT   prev_ste, prev_weight;
  static SXBOOLEAN has_UCL = SXFALSE;

  if (str) {

    if (has_upper (str, strlen (str)))
      has_UCL = SXTRUE;

    if (is_first_call_to_output_if_id) {
      /* 1ere correction */
      is_first_call_to_output_if_id = SXFALSE;
      /* On ne sait pas si ca sera la seule => on la stocke */
      prev_ste = sxstrsave (str);
      if (is_output_weights)
	prev_weight = weight;
    }
    else {
      if (prev_ste) {
	/* Debut d'une sortie multiple */
	fputs ("(", stdout);
	CUR_WEIGHT = prev_weight;
	output_ff_with_comment (sxstrget (prev_ste));	
	prev_ste = 0;
      }

      fputs (" | ", stdout);
      CUR_WEIGHT = weight;
      output_ff_with_comment (str);
      fputs (" ", stdout);
    }
  }
  else {
    /* Fin de la correction */
    /* On regarde s'il faut ajouter Uw */
    if (HAS_AN_UPPER_CASE_LETTER /* Le mot, avant correction contenait au moins une majuscule */
	&& !has_UCL /* Aucune de ses corrections ne commence par une majuscule */
	&& real_tok_no > 1 /* on �vite de surg�n�rer sur le 1er mot de la phrase */
	) {
      if (prev_ste) {
	/* Debut d'une sortie multiple */
	fputs ("(", stdout);
	CUR_WEIGHT = prev_weight;
	output_ff_with_comment (sxstrget (prev_ste));
      }
      fputs (" | ", stdout);
      CUR_WEIGHT = 0;
      if (is_output_uw)
	output_ff_with_comment (Uw);
      else
	output_ff_with_comment (CUR_STR);

      if (!is_first_call_to_output_if_id)
	fputs (") ", stdout);
    }
    else {
      /* Pour le coup d'apres */
      has_UCL = SXFALSE;
      
      if (prev_ste) { /*  Correction unique */
	CUR_WEIGHT = prev_weight;
	output_ff_with_comment (sxstrget (prev_ste));
	fputs (" ", stdout);
	
	is_first_call_to_output_if_id = SXTRUE;
      }
      else {
	if (!is_first_call_to_output_if_id)
	  fputs (") ", stdout);
      }
    }
	
    prev_ste = 0;
  }
}


/* Le id vient d'une correction ds le dico des ff */
static SXINT
output_if_id (x, FSA_ptr)
     SXINT x;
     struct FSA *FSA_ptr;
{
  SXINT id = 0, weight = 0, *bot_ptr, *top_ptr;
  
  if (x == 0)
    output_if_str (NULL, 0); /* fermeture */
  else  {
    id = SXSPELL_x2id (*FSA_ptr, x);    
    weight = SXSPELL_x2weight (*FSA_ptr, x);
    weight = weight > 0 ? weight : -weight;

    if (SXSPELL_has_subwords (id)) { /* correction en plusieurs sous-mots (id < 0): on donne cc1
		   {comment} cc2 ... {comment} ccMAX */
      if (mvstr)
	varstr_raz (mvstr);
      else
	mvstr = varstr_alloc (512);

      SXSPELL_subword_foreach (*FSA_ptr, id, bot_ptr, top_ptr) {
	mvstr = varstr_catenate (mvstr, SXSPELL_id2str (*FSA_ptr, SXSPELL_x2subword_id (bot_ptr)));
	if (!SXSPELL_last_subword(bot_ptr, top_ptr))
	  mvstr = varstr_catchar (mvstr, ' ');
      }

      varstr_complete (mvstr);
      output_if_str (varstr_tostr (mvstr), weight);
      
    }
    else /* correction mono-mot, on transmet ledit mot tel quel */
      output_if_str (SXSPELL_id2str (*FSA_ptr, id), weight);
  }
  return id;
}


static SXINT
call_light_sxspell (dico, FSA_ptr, mot, lgth, f, is_deterministic)
     struct sxdfa_comb *dico;
     struct FSA        *FSA_ptr;
     char              *mot;
     SXINT             lgth;
     SXINT             (*f)();
     SXBOOLEAN           is_deterministic;
{
  SXINT     id, x, ret_val;
  char    last_char, cur_char;

#if MEMOIZE_MORE
#if MEMO_THRSLD > 0
  SXINT  ste, cas;
  SXUINT  data, data_mask, word_offset, data_max;
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
  SXINT  ste, cas;
  SXBA   memo_set;
#endif /* MEMOIZE_MORE */

  if (trace_file)
    fprintf (trace_file, "light_sxspell\t%s\t%s\t%s\t", dico->name, mot, is_deterministic ? "det" : "amb");

#if MEMOIZE_MORE
#if MEMO_THRSLD > 0
  ste = sxword_2save (&sxspeller_input_strings, mot, lgth);
  cas = dico->id + 0 /* 0 = light */;
  word_offset = ste + cas2word_offset [cas];
  data_mask = cas2data_mask [cas];
  data = input_ste2memo_status [word_offset] & data_mask;
  if (data == data_mask /* d�j� tent�, �chec */) {
    if (trace_file)
      fprintf (trace_file, "(no correction found -- cached)\n");
    return 0;
  }
  data_max = cas2data_max [cas];
  if (data == data_max) {
    ;/* r�cup�rer le r�sultat m�mo�z�, faire des trucs, et sortir*/
  }
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
  ste = sxword_2save (&sxspeller_input_strings, mot, lgth);
  cas = dico->id + 0 /* 0 = light */;
  memo_set = input_ste2memo_status [cas];
  if (SXBA_bit_is_set (memo_set, ste)) {
    if (trace_file)
      fprintf (trace_file, "(no correction found -- cached)\n");
    return 0;
  }
#endif /* MEMOIZE_MORE */
  
  /* Prudence */
  last_char = mot [lgth];
  mot [lgth] = SXNUL;

  sxspell_init (dico,
		FSA_ptr,
		(is_deterministic ? (unsigned char)1 /* BEST */ : (unsigned char)max_corr_weight_nb /* 0 = ALL, >0 donne le nb max de corr souhait�es */), 
		light_weight,
		light_compose_weight,
		SXFALSE /* mots multiples : non */,
		0/* ... de cout unitaire */);

  sxspell_init_fsa (mot, lgth, FSA_ptr);

  if (is_change_only_case_and_diacritics == SXFALSE) {
    /* changement apos/tiret */
    sxspell_replace (FSA_ptr, apos_hyphen, T_NB (apos_hyphen), apos_hyphen_weight);
  }
  /* changement de casse */
  sxspell_replace (FSA_ptr, min_maj, T_NB (min_maj), min_maj_weight /* change_case_weight */);
#ifndef german
  sxspell_replace (FSA_ptr, maj_min, T_NB (maj_min), is_first_cc ? 0 : maj_min_weight);
#endif /* !german */
  sxspell_replace (FSA_ptr, add_diacritics, T_NB (add_diacritics), is_first_cc ? 0 : add_diacritics_weight);
#ifdef german
  sxspell_replace_n_p (FSA_ptr, esszet, T_NB (esszet), add_diacritics_weight);
#endif /* german */

  if (is_change_only_case_and_diacritics == SXFALSE) {
    sxspell_replace (FSA_ptr, remove_diacritics, T_NB (remove_diacritics), remove_diacritics_weight);
    sxspell_replace (FSA_ptr, change_diacritics, T_NB (change_diacritics), change_diacritics_weight);
    
    sxspell_replace_n_p (FSA_ptr, twochars_to_diacritics, T_NB (twochars_to_diacritics), twochars_to_diacritics_weight);
    sxspell_replace_n_p (FSA_ptr, mm_en_m, T_NB (mm_en_m), mm_en_m_weight);
    sxspell_replace_n_p (FSA_ptr, m_en_mm, T_NB (m_en_mm), m_en_mm_weight);
    
    sxspell_replace_n_p (FSA_ptr, final_dot, T_NB (final_dot), final_dot_weight);
#ifdef french
    sxspell_replace_n_p (FSA_ptr, abbrev, T_NB (abbrev), abbrev_weight);
    sxspell_replace_n_p (FSA_ptr, final_h, T_NB (final_h), final_h_weight);
    //    sxspell_replace_n_p (FSA_ptr, final_e, T_NB (final_e), final_e_weight);
    sxspell_replace_n_p (FSA_ptr, final_als, T_NB (final_als), final_als_weight);
    sxspell_replace_n_p (FSA_ptr, iI_en_l, T_NB (iI_en_l), iI_en_l_weight);
    sxspell_replace_n_p (FSA_ptr, dusse_je, T_NB (dusse_je), dusse_je_weight);
    sxspell_replace_n_p (FSA_ptr, t_il, T_NB (t_il), t_il_weight);
#endif /* french */
    sxspell_replace_n_p (FSA_ptr, blanc, T_NB (blanc), blanc_weight);
#ifdef latin1
    sxspell_replace_n_p (FSA_ptr, eau_en_au, T_NB (eau_en_au), eau_en_au_weight);
    sxspell_replace_n_p (FSA_ptr, gu_ge, T_NB (gu_ge), gu_ge_weight);
    sxspell_replace_n_p (FSA_ptr, add_h, T_NB (add_h), add_h_weight);
#if 0
    /* qu'elle ne doit pas se corriger en quelle !! */
    sxspell_replace_n_p (FSA_ptr, add_apos, T_NB (add_apos), add_apos_weight);
#endif /* 0 */
#endif /* latin1 */
    
    /* qq fautes d'orthographe (d�pend de la langue) */
    sxspell_replace_n_p (FSA_ptr, final_underscore, T_NB (final_dot), final_underscore_weight);
    sxspell_replace_n_p (FSA_ptr, ortho, T_NB (ortho), ortho_weight);
    //  sxspell_replace_n_p (FSA_ptr, ortho2, T_NB (ortho2), ortho2_weight);
    
    /* nooooooooooonnnnnnnnnn => non */
#ifdef german
    sxspell_no_repeat (FSA_ptr, 4, no_repeat_weight);
#else
    sxspell_no_repeat (FSA_ptr, 3, no_repeat_weight);
#endif /* german */
    
    /* insertion de tirets */
    for (x = lgth; x >= 1; x--)
      sxspell_add (FSA_ptr, x, (unsigned char*)"-", add_hyphen_weight);
    
    /* insertion de qq lettres */
    for (x = lgth; x >= 1; x--) {
      sxspell_add (FSA_ptr, x, insert_string, insert_weight);
    }
    
    /* remplacements */
    for (x = lgth; x >= 1; x--) {
      cur_char = mot [x-1];
      
      if (is_upper (cur_char) || cur_char != sxtoupper (cur_char))
	/* on ne cherche � remplacer par une des lettres de change_string que les caract�res de l'entr�e qui sont de vraies lettres */
	sxspell_change (FSA_ptr, x, change_string, change_weight);
    }
  }
  
  ret_val = 0;

  if ((id = sxspell_do_it (FSA_ptr) /* toujours effectu� */)) {
    if (f) {
      for (x = 1; x <= SXSPELL_spelling_nb (*FSA_ptr) && (max_corr_nb == 0 || x <= max_corr_nb); x++) {
	/* Pas d'extraction de sous-mots */
	ret_val = (*f)(x, FSA_ptr) || ret_val;
	if (trace_file) {
	  SXINT id, *bot_ptr, *top_ptr;

	  fprintf (trace_file, "%s", x == 1 ? "" : "\t");

	  id = SXSPELL_x2id (*FSA_ptr, x);

	  if (SXSPELL_has_subwords (id)) { /* correction en plusieurs sous-mots (id < 0): on donne cc1
					      {comment} cc2 ... {comment} ccMAX */
	    if (mvstr)
	      varstr_raz (mvstr);
	    else
	      mvstr = varstr_alloc (512);
      
	    SXSPELL_subword_foreach (*FSA_ptr, id, bot_ptr, top_ptr) {
	      mvstr = varstr_catenate (mvstr, SXSPELL_id2str (*FSA_ptr, SXSPELL_x2subword_id (bot_ptr)));
	      if (!SXSPELL_last_subword(bot_ptr, top_ptr))
		mvstr = varstr_catchar (mvstr, ' ');
	    }
      
	    fprintf (trace_file, "%s", varstr_tostr (mvstr));
      
	  } else /* correction mono-mot, on transmet ledit mot tel quel */
	    fprintf (trace_file, "%s", SXSPELL_id2str (*FSA_ptr, id));

	  fprintf (trace_file, " (%ld)", SXSPELL_x2weight (*FSA_ptr, x));
	}
      }
      ret_val = (*f)(0, FSA_ptr) || ret_val; /* Fin des appels */  
    } else
      ret_val = 1;
  }

  if (trace_file)
    fprintf (trace_file, "\n");
      
  sxspell_final_fsa (FSA_ptr);
  sxspell_final (FSA_ptr);

  mot [lgth] = last_char;

#if MEMOIZE_MORE
#if MEMO_THRSLD > 0
  if (ret_val == 0)
    input_ste2memo_status [word_offset] |= data_mask;
  else {
    /* test temporaire */
    if (data < data_max) {
      if ((input_ste2memo_status [word_offset] += cas2data_incr [cas]) == data_max)
	;/* m�mo�ser */
    }
  }
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
  if (ret_val == 0)
    SXBA_1_bit (memo_set, ste);
#endif /* MEMOIZE_MORE */

  return ret_val; /* Y'a-t-il eu de la correction */
}


/* On corrige de fac,on non deterministe toutes les majuscules d'un mot */
static SXINT
call_maj2diacritic_sxspell (dico, FSA_ptr, mot, lgth, f)
     struct sxdfa_comb *dico;
     struct FSA        *FSA_ptr;
     char              *mot;
     SXINT             lgth;
     SXINT             (*f)();
{
  SXINT     id, x, ret_val;
  char    last_char;

#if MEMOIZE_MORE
#if MEMO_THRSLD > 0
  SXINT  ste, cas;
  SXUINT  data, data_mask, word_offset, data_max;
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
  SXINT  ste, cas;
  SXBA   memo_set;
#endif /* MEMOIZE_MORE */

  if (trace_file)
    fprintf (trace_file, "maj2diacritic_sxspell\t%s\t%s\t%s\t", dico->name, mot, "amb");

#if MEMOIZE_MORE
#if MEMO_THRSLD > 0
  ste = sxword_2save (&sxspeller_input_strings, mot, lgth);
  cas = dico->id + 1 /* 1 = maj2diacritic */;
  word_offset = ste + cas2word_offset [cas];
  data_mask = cas2data_mask [cas];
  data = input_ste2memo_status [word_offset] & data_mask;
  if (data == data_mask /* d�j� tent�, �chec */) {
    if (trace_file)
      fprintf (trace_file, "(no correction found -- cached)\n");
    return 0;
  }
  data_max = cas2data_max [cas];
  if (data == data_max) {
    ;/* r�cup�rer le r�sultat m�mo�z�, faire des trucs, et sortir*/
  }
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
  ste = sxword_2save (&sxspeller_input_strings, mot, lgth);
  cas = dico->id + 1 /* 1 = maj2diacritic */;
  memo_set = input_ste2memo_status [cas];
  if (SXBA_bit_is_set (memo_set, ste)) {
    if (trace_file)
      fprintf (trace_file, "(no correction found -- cached)\n");
    return 0;
  }
#endif /* MEMOIZE_MORE */
  
  /* Prudence */
  last_char = mot [lgth];
  mot [lgth] = SXNUL;

  sxspell_init (dico,
		FSA_ptr,
		(unsigned char)0 /* ALL */, 
		100,
		0,
		SXFALSE /* mots multiples : non */,
		0/* ... de cout unitaire */);

  sxspell_init_fsa (mot, lgth, FSA_ptr);
  
  sxspell_replace (FSA_ptr, maj2diacritic, T_NB (maj2diacritic), maj2diacritic_weight);
  
  ret_val = 0;

  if (/*id = */sxspell_do_it (FSA_ptr) /* toujours effectu� */
      && f) {
    for (x = 1; x <= SXSPELL_spelling_nb (*FSA_ptr) && (max_corr_nb == 0 || x <= max_corr_nb); x++) {
      /* Pas d'extraction de sous-mots */
      ret_val = (*f)(x, FSA_ptr) || ret_val;
      if (trace_file) {
	  SXINT id, *bot_ptr, *top_ptr;

	  fprintf (trace_file, "%s", x == 1 ? "" : "\t");

	  id = SXSPELL_x2id (*FSA_ptr, x);

	  if (SXSPELL_has_subwords (id)) { /* correction en plusieurs sous-mots (id < 0): on donne cc1
					      {comment} cc2 ... {comment} ccMAX */
	    if (mvstr)
	      varstr_raz (mvstr);
	    else
	      mvstr = varstr_alloc (512);
      
	    SXSPELL_subword_foreach (*FSA_ptr, id, bot_ptr, top_ptr) {
	      mvstr = varstr_catenate (mvstr, SXSPELL_id2str (*FSA_ptr, SXSPELL_x2subword_id (bot_ptr)));
	      if (!SXSPELL_last_subword(bot_ptr, top_ptr))
		mvstr = varstr_catchar (mvstr, ' ');
	    }
      
	    fprintf (trace_file, "%s", varstr_tostr (mvstr));
      
	  } else /* correction mono-mot, on transmet ledit mot tel quel */
	    fprintf (trace_file, "%s", SXSPELL_id2str (*FSA_ptr, id));

	  fprintf (trace_file, " (%ld)", SXSPELL_x2weight (*FSA_ptr, x));
	}
    }
    ret_val = (*f)(0, FSA_ptr) || ret_val; /* Fin des appels */  
  }
    
  if (trace_file)
    fprintf (trace_file, "\n");

  sxspell_final_fsa (FSA_ptr);
  sxspell_final (FSA_ptr);

  mot [lgth] = last_char;

#if MEMOIZE_MORE
#if MEMO_THRSLD > 0
  if (ret_val == 0)
    input_ste2memo_status [word_offset] |= data_mask;
  else {
    /* test temporaire */
    if (data < data_max) {
      if ((input_ste2memo_status [word_offset] += cas2data_incr [cas]) == data_max)
	;/* m�mo�ser */
    }
  }
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
  if (ret_val == 0)
    SXBA_1_bit (memo_set, ste);
#endif /* MEMOIZE_MORE */

  return ret_val; /* Y'a-t-il eu de la correction */
}



static SXINT
if_SPELLED_VSTR (x, FSA_ptr)
     SXINT x;
     struct FSA *FSA_ptr;
{
  SXINT id = 0;
  if (x)
    id = SXSPELL_x2id (*FSA_ptr, x);
  if (id)
    SPELLED_VSTR = varstr_catenate (varstr_raz (SPELLED_VSTR), SXSPELL_id2str (*FSA_ptr, id));
  return id;
}

static SXINT
amlgm_or_cmpnd_SPELLED_VSTR (x, FSA_ptr)
     SXINT x;
     struct FSA *FSA_ptr;
{
  SXINT id = 0;
  if (x)
    id = SXSPELL_x2id (*FSA_ptr, x);
  if (id)
    SPELLED_VSTR = varstr_catenate (varstr_raz (SPELLED_VSTR), SXSPELL_id2str (*FSA_ptr, id));
  return id;
}


/* Pour l'instant on n'essaie que de la correction maj_min et diacritics */
/* appelle f sur chaque correction */
static SXINT
call_super_light_sxspell (dico, FSA_ptr, mot, lgth, f)
     struct sxdfa_comb *dico;
     struct FSA        *FSA_ptr;
     char              *mot;
     SXINT             lgth;
     SXINT             (*f)();
{
  SXINT     id, x, ret_val;
  char    last_char;

#if MEMOIZE_MORE
#if MEMO_THRSLD > 0
  SXINT  ste, cas;
  SXUINT  data, data_mask, word_offset, data_max;
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
  SXINT  ste, cas;
  SXBA   memo_set;
#endif /* MEMOIZE_MORE */

  if (trace_file)
    fprintf (trace_file, "super_light_sxspell\t%s\t%s\t%s\t", dico->name, mot, "amb");

#if MEMOIZE_MORE
#if MEMO_THRSLD > 0
  ste = sxword_2save (&sxspeller_input_strings, mot, lgth);
  cas = dico->id + 2 /* 2 = superlight */;
  word_offset = ste + cas2word_offset [cas];
  data_mask = cas2data_mask [cas];
  data = input_ste2memo_status [word_offset] & data_mask;
  if (data == data_mask /* d�j� tent�, �chec */) {
    if (trace_file)
      fprintf (trace_file, "(no correction found -- cached)\n");
    return 0;
  }
  data_max = cas2data_max [cas];
  if (data == data_max) {
    ;/* r�cup�rer le r�sultat m�mo�z�, faire des trucs, et sortir*/
  }
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
  ste = sxword_2save (&sxspeller_input_strings, mot, lgth);
  cas = dico->id + 2 /* 2 = super_light */;
  memo_set = input_ste2memo_status [cas];
  if (SXBA_bit_is_set (memo_set, ste)) {
    if (trace_file)
      fprintf (trace_file, "(no correction found -- cached)\n");
    return 0;
  }
#endif /* MEMOIZE_MORE */

  /* Prudence */
  last_char = mot [lgth];
  mot [lgth] = SXNUL;
      
  sxspell_init (dico,
		FSA_ptr,
		(unsigned char)0 /* ALL (modif du 19/05/05) */, 
		super_light_weight,
		0,
		SXFALSE /* mots multiples, non */,
		0 /* ... de cout unitaire */);
  sxspell_init_fsa (mot, lgth, FSA_ptr);

#ifndef german
  sxspell_replace (FSA_ptr, maj_min, T_NB (maj_min), 1 /* maj_min_weight */); // et non -1
#endif /* !german */
  sxspell_replace (FSA_ptr, add_diacritics, T_NB (add_diacritics), add_diacritics_weight);
#ifdef german
  sxspell_replace_n_p (FSA_ptr, esszet, T_NB (esszet), add_diacritics_weight);
#endif /* german */

  ret_val = 0;

  if ((id = sxspell_do_it (FSA_ptr) /* toujours effectu� */)) {
    if (f) {
      for (x = 1; x <= SXSPELL_spelling_nb (*FSA_ptr) && (max_corr_nb == 0 || x <= max_corr_nb); x++) {
	/* Pas d'extraction de sous-mots */
	ret_val = (*f)(x, FSA_ptr) || ret_val;
	if (trace_file) {
	  SXINT id, *bot_ptr, *top_ptr;

	  fprintf (trace_file, "%s", x == 1 ? "" : "\t");

	  id = SXSPELL_x2id (*FSA_ptr, x);

	  if (SXSPELL_has_subwords (id)) { /* correction en plusieurs sous-mots (id < 0): on donne cc1
					      {comment} cc2 ... {comment} ccMAX */
	    if (mvstr)
	      varstr_raz (mvstr);
	    else
	      mvstr = varstr_alloc (512);
      
	    SXSPELL_subword_foreach (*FSA_ptr, id, bot_ptr, top_ptr) {
	      mvstr = varstr_catenate (mvstr, SXSPELL_id2str (*FSA_ptr, SXSPELL_x2subword_id (bot_ptr)));
	      if (!SXSPELL_last_subword(bot_ptr, top_ptr))
		mvstr = varstr_catchar (mvstr, ' ');
	    }
      
	    fprintf (trace_file, "%s", varstr_tostr (mvstr));
      
	  } else /* correction mono-mot, on transmet ledit mot tel quel */
	    fprintf (trace_file, "%s", SXSPELL_id2str (*FSA_ptr, id));

	  fprintf (trace_file, " (%ld)", SXSPELL_x2weight (*FSA_ptr, x));
	}
      }
      ret_val = (*f)(0, FSA_ptr) || ret_val; /* Fin des appels */  
    } else
      ret_val = 1;
  }
    
  if (trace_file)
    fprintf (trace_file, "\n");
      
  sxspell_final_fsa (FSA_ptr);
  sxspell_final (FSA_ptr);

  mot [lgth] = last_char;

#if MEMOIZE_MORE
#if MEMO_THRSLD > 0
  if (ret_val == 0)
    input_ste2memo_status [word_offset] |= data_mask;
  else {
    /* test temporaire */
    if (data < data_max) {
      if ((input_ste2memo_status [word_offset] += cas2data_incr [cas]) == data_max)
	;/* m�mo�ser */
    }
  }
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
  if (ret_val == 0)
    SXBA_1_bit (memo_set, ste);
#endif /* MEMOIZE_MORE */

  return ret_val; /* Y'a-t-il eu de la correction */
}

static SXBOOLEAN
call_sxspell (dico, FSA_ptr, mot, lgth, f, is_deterministic)
     struct sxdfa_comb *dico;
     struct FSA        *FSA_ptr;
     char              *mot;
     SXINT             lgth;
     SXINT             (*f)();
     SXBOOLEAN           is_deterministic;
{
  char    last_char, cur_char;
  SXINT     x, id;
  SXINT     ret_val;

#if MEMOIZE_MORE
#if MEMO_THRSLD > 0
  SXINT  ste, cas;
  SXUINT  data, data_mask, word_offset, data_max;
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
  SXINT  ste, cas;
  SXBA   memo_set;
#endif /* MEMOIZE_MORE */

  if (trace_file)
    fprintf (trace_file, "sxspell\t%s\t%s\t%s\t", dico->name, mot, is_deterministic ? "det" : "amb");

#if MEMOIZE_MORE
#if MEMO_THRSLD > 0
  ste = sxword_2save (&sxspeller_input_strings, mot, lgth);
  cas = dico->id + 3 /* 3 = std */;
  word_offset = ste + cas2word_offset [cas];
  data_mask = cas2data_mask [cas];
  data = input_ste2memo_status [word_offset] & data_mask;
  if (data == data_mask /* d�j� tent�, �chec */) {
    if (trace_file)
      fprintf (trace_file, "(no correction found -- cached)\n");
    return 0;
  }
  data_max = cas2data_max [cas];
  if (data == data_max) {
    ;/* r�cup�rer le r�sultat m�mo�z�, faire des trucs, et sortir*/
  }
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
  ste = sxword_2save (&sxspeller_input_strings, mot, lgth);
  cas = dico->id + 3 /* 3 = std */;
  memo_set = input_ste2memo_status [cas];
  if (SXBA_bit_is_set (memo_set, ste)) {
    if (trace_file)
      fprintf (trace_file, "(no correction found -- cached)\n");
    return 0;
  }
#endif /* MEMOIZE_MORE */
  
  /* Prudence */
  last_char = mot [lgth];
  mot [lgth] = SXNUL;

  sxspell_init (dico,
		FSA_ptr,
		(is_deterministic ? (unsigned char)1 /* BEST */ : (unsigned char)max_corr_weight_nb /* 0 = ALL, >0 donne le nb max de corr souhait�es */), 
		max_weight,
		compose_weight,
		is_sub_word /* mots multiples ? */,
		sub_word_weight /* ... de cout unitaire */);
  
  sxspell_init_fsa (mot, lgth, FSA_ptr);

  if (is_change_only_case_and_diacritics == SXFALSE) {
  /* changement apos/tiret */
    sxspell_replace (FSA_ptr, apos_hyphen, T_NB (apos_hyphen), apos_hyphen_weight);
  }
  /* changement de casse */
  sxspell_replace (FSA_ptr, min_maj, T_NB (min_maj), min_maj_weight);
  sxspell_replace (FSA_ptr, maj_min, T_NB (maj_min), is_first_cc ? 0 : maj_min_weight);
  sxspell_replace (FSA_ptr, add_diacritics, T_NB (add_diacritics), add_diacritics_weight);
#ifdef german
  sxspell_replace_n_p (FSA_ptr, esszet, T_NB (esszet), add_diacritics_weight);
#endif /* german */

  if (is_change_only_case_and_diacritics == SXFALSE) {
    sxspell_replace (FSA_ptr, remove_diacritics, T_NB (remove_diacritics), remove_diacritics_weight);
    sxspell_replace (FSA_ptr, change_diacritics, T_NB (change_diacritics), change_diacritics_weight);
    
    sxspell_replace_n_p (FSA_ptr, twochars_to_diacritics, T_NB (twochars_to_diacritics), twochars_to_diacritics_weight);
    /* les lettres double'es sont fausses */
    sxspell_replace_n_p (FSA_ptr, mm_en_m, T_NB (mm_en_m), mm_en_m_weight);
    /* il faut des consonnes double'es */
    sxspell_replace_n_p (FSA_ptr, m_en_mm, T_NB (m_en_mm), m_en_mm_weight);
    
    sxspell_replace_n_p (FSA_ptr, final_dot, T_NB (final_dot), final_dot_weight);
#ifdef french
    sxspell_replace_n_p (FSA_ptr, abbrev, T_NB (abbrev), abbrev_weight);
    sxspell_replace_n_p (FSA_ptr, final_h, T_NB (final_h), final_h_weight);
    sxspell_replace_n_p (FSA_ptr, final_e, T_NB (final_e), final_e_weight);
    sxspell_replace_n_p (FSA_ptr, final_als, T_NB (final_als), final_als_weight);
    sxspell_replace_n_p (FSA_ptr, iI_en_l, T_NB (iI_en_l), iI_en_l_weight);
    sxspell_replace_n_p (FSA_ptr, dusse_je, T_NB (dusse_je), dusse_je_weight);
    sxspell_replace_n_p (FSA_ptr, t_il, T_NB (t_il), t_il_weight);
#endif /* french */
    sxspell_replace_n_p (FSA_ptr, blanc, T_NB (blanc), blanc_weight);
#ifdef latin1
    sxspell_replace_n_p (FSA_ptr, eau_en_au, T_NB (eau_en_au), eau_en_au_weight);
    sxspell_replace_n_p (FSA_ptr, gu_ge, T_NB (gu_ge), gu_ge_weight);
    sxspell_replace_n_p (FSA_ptr, add_h, T_NB (add_h), add_h_weight);
    sxspell_replace_n_p (FSA_ptr, add_apos, T_NB (add_apos), add_apos_weight);
#endif /* latin1 */
    
    sxspell_replace (FSA_ptr, qq_trucs, T_NB (qq_trucs), qq_trucs_weight);
    /* traitement des fautes de frappe */
    //  sxspell_replace (FSA_ptr, typos_qwerty, T_NB (typos_qwerty), typos_weight);
    /* traitement du changement de claviers */
    sxspell_replace (FSA_ptr, qwerty_azerty, T_NB (qwerty_azerty), qwerty_azerty_weight);
    /* On s'est trompe' de voyelle, meme accent */
    //  sxspell_replace (FSA_ptr, mauvaise_voyelle, T_NB (mauvaise_voyelle), mauvaise_voyelle_weight);
    
#if 0
    /* Essai, attention au temps ... */
    sxspell_suppress_all (FSA_ptr, suppress_weight);
#endif /* 0 */
    
    /* Le temps augmente tres fortement si i > 1 !! */
    /* traitement de la suppression de 1 caractere successif */
    sxspell_suppress_i (FSA_ptr, suppress_weight, 1);
    
    /* traitement de l'intervertion de 2 caracteres */
    sxspell_swap (FSA_ptr, swap_weight);
    /* qq fautes d'orthographe */
    sxspell_replace_n_p (FSA_ptr, final_underscore, T_NB (final_underscore), final_underscore_weight);
    sxspell_replace_n_p (FSA_ptr, ortho, T_NB (ortho), ortho_weight);
    sxspell_replace_n_p (FSA_ptr, ortho2, T_NB (ortho2), ortho2_weight);
#ifdef french
    /* On peut ajouter qq lettres en fin */
    sxspell_add (FSA_ptr, lgth+1, (unsigned char*)"cdefhpstxz", add_weight);
    /* et une hache muette au d�but... */
    sxspell_add (FSA_ptr, 1, (unsigned char*)"hH", add_weight);
#endif /* french */
    
    /* nooooooooooonnnnnnnnnn => non */
#ifdef german
    sxspell_no_repeat (FSA_ptr, 4, no_repeat_weight);
#else
    sxspell_no_repeat (FSA_ptr, 3, no_repeat_weight);
#endif /* german */
    
    /* insertion de tirets */
    for (x = lgth; x >= 2; x--)
      sxspell_add (FSA_ptr, x, (unsigned char*)"-_", add_hyphen_weight);
    
    /* insertion de qq lettres */
    for (x = lgth; x >= 1; x--)
      sxspell_add (FSA_ptr, x, insert_string, insert_weight);
    
    /* remplacements */
    for (x = lgth; x >= 1; x--) {
      cur_char = mot [x-1];
      
      if (is_upper (cur_char) || cur_char != sxtoupper (cur_char))
	/* on ne cherche � remplacer par une des lettres de change_string que les caract�res de l'entr�e qui sont de vraies lettres */
	sxspell_change (FSA_ptr, x, change_string, change_weight);
    }
  }
  
  ret_val = 0;

  if ((id = sxspell_do_it (FSA_ptr) /* toujours effectu� */)) {
    if (f) {
      for (x = 1; x <= SXSPELL_spelling_nb (*FSA_ptr) && (max_corr_nb == 0 || x <= max_corr_nb); x++) {
	/* Pas d'extraction de sous-mots */
	ret_val = (*f)(x, FSA_ptr) || ret_val;
	if (trace_file) {
	  SXINT id, *bot_ptr, *top_ptr;

	  fprintf (trace_file, "%s", x == 1 ? "" : "\t");

	  id = SXSPELL_x2id (*FSA_ptr, x);

	  if (SXSPELL_has_subwords (id)) { /* correction en plusieurs sous-mots (id < 0): on donne cc1
					      {comment} cc2 ... {comment} ccMAX */
	    if (mvstr)
	      varstr_raz (mvstr);
	    else
	      mvstr = varstr_alloc (512);
      
	    SXSPELL_subword_foreach (*FSA_ptr, id, bot_ptr, top_ptr) {
	      mvstr = varstr_catenate (mvstr, SXSPELL_id2str (*FSA_ptr, SXSPELL_x2subword_id (bot_ptr)));
	      if (!SXSPELL_last_subword(bot_ptr, top_ptr))
		/* we are on the i<n-th component of a correction
		 * involving n output words - we should add some special
		 * weight if is_output_weights (e.g., -2) [same
		 * everywhere else when !SXSPELL_last_subword(bot_ptr,
		 * top_ptr) is tested*/
		mvstr = varstr_catchar (mvstr, ' ');
	    }
      
	    fprintf (trace_file, "%s", varstr_tostr (mvstr));
      
	  } else /* correction mono-mot, on transmet ledit mot tel quel */
	    fprintf (trace_file, "%s", SXSPELL_id2str (*FSA_ptr, id));

	  fprintf (trace_file, " (%ld)", SXSPELL_x2weight (*FSA_ptr, x));
	}
      }
      ret_val = (*f)(0, FSA_ptr) || ret_val; /* Fin des appels */  
    } else
      ret_val = 1;
  }
  
  if (trace_file)
    fprintf (trace_file, "\n");
      
  sxspell_final_fsa (FSA_ptr);
  sxspell_final (FSA_ptr);

  mot [lgth] = last_char;

#if MEMOIZE_MORE
#if MEMO_THRSLD > 0
  if (ret_val == 0)
    input_ste2memo_status [word_offset] |= data_mask;
  else {
    /* test temporaire */
    if (data < data_max) {
      if ((input_ste2memo_status [word_offset] += cas2data_incr [cas]) == data_max)
	;/* m�mo�ser */
    }
  }
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
  if (ret_val == 0)
    SXBA_1_bit (memo_set, ste);
#endif /* MEMOIZE_MORE */

  return ret_val;
}

static void
print_spelling_correction (FSA_ptr)
     struct FSA *FSA_ptr;
{
  SXINT id, *bot_ptr, *top_ptr;
    
  /* On ne prend que le 1er resultat parmi les meilleurs */
  id = SXSPELL_x2id (*FSA_ptr, 1);

  if (!SXSPELL_has_subwords (id)) {
    output_ff_with_comment (SXSPELL_id2str (*FSA_ptr, id));
    fputs (" ", stdout);
  }
  else {
    /* cas sous-mots */
    SXSPELL_subword_foreach (*FSA_ptr, id, bot_ptr, top_ptr) {
      id = SXSPELL_x2subword_id (bot_ptr);
      output_ff_with_comment (SXSPELL_id2str (*FSA_ptr, id));
      fputs (" ", stdout);
    }
  }
}


static SXINT
edges (i, j, p)
    SXINT		i, j;
    char	**p;
{
  SXINT  nb = 0, ste, triple, prev_triple;

  XxYxZ_XZforeach (if_dag, i, j, triple) {
    prev_triple = triple;
    ste = XxYxZ_Y (if_dag, triple);
    nb++;
  }

  if (nb == 1) {
    CUR_COMMENT = if_comment [prev_triple];
    if (is_output_weights)
      CUR_WEIGHT = if_weight [prev_triple];
    
    if (CUR_COMMENT || is_output_weights)
      ste = sxstrsave (varstr_tostr (save_ff_with_comment (varstr_raz (wvstr), sxstrget (ste))));
      
    *p = sxstrget (ste);
  }
  else {
    varstr_raz (wvstr);
    wvstr = varstr_catenate (wvstr, "(");

    XxYxZ_XZforeach (if_dag, i, j, triple) {
      ste = XxYxZ_Y (if_dag, triple);
      CUR_COMMENT = if_comment [triple];
      if (is_output_weights)
	CUR_WEIGHT = if_weight [prev_triple];

      wvstr = save_ff_with_comment (wvstr, sxstrget (ste));

      if (--nb > 0)
	wvstr = varstr_catenate (wvstr, " | ");
    }

    wvstr = varstr_catenate (wvstr, ")");

    ste = sxstrsave (varstr_tostr (wvstr));
    *p = sxstrget (ste);
  }
  return ste;
}

#if 0
/* On fabrique un commentaire en concatenant les commentaires de cc_comment_stack [1..n] */
static char*
get_cc_comment (i)
     SXINT i;
{
  return cc_comment_stack [i];
}

static char*
get_tok_comment (tok_no)
     SXINT tok_no;
{
  return SXGET_TOKEN (tok_no).comment;
}
#endif /* 0 */

static char*
make_comment (bot, top, f)
     SXINT bot, top;
     char * (*f)();
{
  SXINT  i, nb, ste;
  char *comment, *cur_comment;

  nb = 0;

  for (i = bot; i < top; i++) {
    if ((cur_comment = (*f) (i))) {
      if (nb == 0) {
	comment = cur_comment;
	nb = 1;
      }
      else {
	if (strcmp (comment, cur_comment) != 0) {
	  comment = cur_comment;
	  nb++;
	}
      }
    }
  }

  if (nb <= 1)
    return nb == 0 ? NULL : comment;

  /* Vraie concat ... */
  varstr_raz (wvstr);
  wvstr = varstr_catenate (wvstr, "{");

  for (i = bot; i < top; i++) {
    if ((cur_comment = (*f) (i))) {
      if (nb == 0) {
	comment = cur_comment;
      }
      else {
	if (strcmp (comment, cur_comment) != 0) {
	  comment = cur_comment;
	}
	else
	  continue;
      }

      cur_comment++; /* On saute le "{" ... */
      wvstr = varstr_lcatenate (wvstr, cur_comment, strlen (cur_comment));
      /* ... et le "}" de fin  est change' en " " */
      *(wvstr->current-1) = ' ';
    }
  }
  
  /* ... et le " " de fin  est rechange' en "}" */
  *(wvstr->current-1) = '}';

  ste = sxstrsave (varstr_tostr (wvstr));
  
  return sxstrget (ste);
}

static SXINT *lhs_id_stack, *lhs_p_stack, *lhs_comment_ste_stack;

static char*
get_comment_in_stack (i)
     SXINT i;
{
  return sxstrget (lhs_comment_ste_stack [i]);
}

static SXINT
make_rhs_comment_ste (offset, lgth)
     SXINT offset, lgth;
{
  return sxstrsave (make_comment (offset, offset + lgth, get_comment_in_stack));
}

#if 0
static SXBOOLEAN
cc_path (i, j)
     SXINT i, j;
{
  /* On regarde si le contenu de cc_stack est un compound */
  SXINT     x, cur, id, triple, k, cc_nb, last_id, ste; 
  char    *comment;
  SXBOOLEAN has_new;

  has_new = SXFALSE;
  cc_nb = cc_stack [0];
  last_id = cc_stack [cc_nb];

  if (SXBA_bit_is_set (final_compound_component_id_set, last_id)) {
    /* c'est un candidat compound */
    for (x = 1; x <= cc_nb; x++) {
      XH_push (XH_cc_list, cc_stack [x]);
    }

    if (x = XH_is_set (&XH_cc_list)) {
      /* C'est un compound */
      comment = make_comment (1, cc_nb+1, get_cc_comment);

      /* On recupere la liste des compound qui se contracte avec les composants de cc_stack */
      for (cur = cc2cmpnd_list [x]; cur != 0; cur = cmpnd_list [cur].next) {
	id = cmpnd_list [cur].hd;

	if (SXBA_bit_is_set (amalgam_id_set, id)) {
	  /* amalgam */
	  if (!XxYxZ_set (&amalgam_dag, i, id, j, &triple)) {
	    PUSH (amalgam_stack, triple);
	    amalgam_comment [triple] = comment;
	  }
	}

	if (SXBA_bit_is_set (compound_component_id_set, id)) {
	  /* compound component */
	  if (!XxYxZ_set (&compound_component_dag, i, id, j, &triple)) {
	    /* Nouveau */
	    compound_component_comment [triple] = comment;
	    /* On remplit compound_component_dag jusqu'a stabilite */
	    has_new = SXTRUE;

	    if (SXBA_bit_is_set (initial_compound_component_id_set, id))
	      PUSH (initial_compound_component_stack, triple);
	  }
	}

	//	if (SXBA_bit_is_set (if_id_set, id)) {
	  /* forme flechie */
	  ste = sxstrsave (ac_id2string [id]);
	  XxYxZ_set (&if_dag, i, ste, j, &triple);
	  if_comment [triple] = comment;
	  //	}
      }
    }
  }

  if (cc_nb < MAX_CC_LIST_LGTH) {
    XxYxZ_Xforeach (compound_component_dag, j, triple) {
      id = XxYxZ_Y (compound_component_dag, triple);
      k = XxYxZ_Z (compound_component_dag, triple);

      PUSH (cc_stack, id);
      cc_comment_stack [TOP (cc_stack)] = compound_component_comment [triple];

      if (cc_path (i, k))
	has_new = SXTRUE;

      POP (cc_stack);
    }
  }

  return has_new;
}
#endif /* 0 */

static SXBOOLEAN has_new_acc_trans;

static void lhs_walker (SXINT acc_dag_transition);
static XxY_header XxY_duplicate_hd;

static void
dag_walker (SXINT p)
{
  SXINT trans, id, triple;

  XxYxZ_Xforeach (acc_dag, p, triple) {
    trans = XxYxZ_Y (acc_dag, triple);
    id = XxY_X (acc_trans_hd, trans);

    if (SXBA_bit_is_set (acc_entrance_set, id)) {
      /* id commence une lhs, ie.: est-ce le premier mot d'un compound */
#if 0
      DRAZ (lhs_id_stack);
      DRAZ (lhs_p_stack);
      DRAZ (lhs_comment_ste_stack);
#endif /* 0 */
#if EBUG
      id = DTOP (lhs_p_stack);
#endif /* EBUG */
      /* Attention lhs_walker peut ajouter des transitions a acc_dag partant de p */
      /* Ds l'implantation actuelle des foreach, ces nelles transitions ne sont pas prises en compte
	 par le XxYxZ_Xforeach englobant (elles le seront par des appels ulterieurs de dag_walker (p)) */
      lhs_walker (triple);
#if EBUG
      if (id != DTOP (lhs_p_stack))
	sxtrap (ME, "dag_walker");
#endif /* EBUG */
    }
    else
      /* Si id == 0, ou si id est un acc qui n'est pas un point d'entree, il faut quand meme suivre les chemins (on peut trouver un point d'entree + loin) */
      dag_walker (XxYxZ_Z (acc_dag, triple));
  }
}


static void
lhs_walker (SXINT acc_dag_transition)
{
  SXINT save_val, rhs_list_id, x_top, x, rhs_id, bot, top, prev_p, next_q, trans, triple, cur_id, cur_p, dup_id;
  SXINT *rhs_list_stack, *top_lhs_id_stack, *cur_lhs_id_stack, *cur_lhs_p_stack, *top_lhs_p_stack;

  SXINT p = XxYxZ_X (acc_dag, acc_dag_transition);
  SXINT q = XxYxZ_Z (acc_dag, acc_dag_transition);
  SXINT id = XxY_X (acc_trans_hd, XxYxZ_Y (acc_dag, acc_dag_transition));
  SXINT comment_ste = acc_dag2comment_ste [acc_dag_transition];
  SXINT rhs_comment_ste;

  DPUSH (lhs_p_stack, p);
  DPUSH (lhs_id_stack, id);
  DPUSH (lhs_comment_ste_stack, comment_ste);

  if (SXBA_bit_is_set (acc_exit_set, id)) {
    /* id termine une lhs */
    /* ATTENTION les transitions sont des identifiants d'acc, la structure utilisee
       ne peut pas etre ni packee (sauf en 64 bits) ni un sxdfa_comb (ds lequel les trans sont des chars).
       J'ai donc pris une sxdfa_struct */

    /* Cette fin de lhs peut correspondre a plusieurs points d'entree de lhs_p_stack/lhs_id_stack que l'on
       recherche depuis le sommet car ces sequences peuvent etre interrompues par des "trous" */
    cur_lhs_p_stack = lhs_p_stack+DTOP (lhs_p_stack);
    top_lhs_id_stack = cur_lhs_id_stack = lhs_id_stack+DTOP (lhs_id_stack);

    while (lhs_id_stack < cur_lhs_id_stack) {
      cur_id = *cur_lhs_id_stack--;
      cur_p = *cur_lhs_p_stack--;

      if (cur_id == 0)
	/* trou, on abandonne donc la recherche de points d'entree */
	break;

      if (SXBA_bit_is_set (acc_entrance_set, cur_id)) {
	/* detection d'un point d'entree ... */
	/* On a donc une lhs potentielle entre cur et top */
	/* truc, on transforme cette sequence en "vraie" pile pour sxdfa_seek_a_word () */
	save_val = *cur_lhs_id_stack;
	*cur_lhs_id_stack = top_lhs_id_stack-cur_lhs_id_stack;

	/* On regarde si c'est une lhs de regle */
	rhs_list_id = sxdfa_seek_a_word (&sxdfa_acc_lhs, cur_lhs_id_stack);

	if (*cur_lhs_id_stack != 0)
	  /* pour etre une lhs, la longueur doit etre nulle (sequence reconnue entierement */
	  rhs_list_id = 0;

	/* Fin de truc */
	*cur_lhs_id_stack = save_val;

	if (rhs_list_id) {
	  /* Oui, c'est la lhs de regle[s] */
	  /* On recherche ses parties droites */
	  /* On vient de reconnaitre une partie gauche de regle */
	  /* Les regles de reecriture des ac specifient un langage de type 1.
	     Le sous-ensemble des chaines (du langage) genere a partir d'un texte initial doit etre represente
	     par un DAG (donc obligatoirement fini).  Une seule regle de la forme a --> a b qui a partir du source
	     "a" definit le langage (infini) ab* ne peut etre (entierement) appliquee.
	     Pour resoudre ce pb, je choisis de n'appliquer sur un etat du DAG qu'au plus une fois une regle donnee.
	     Il faut noter que cette restriction peut facilement se generaliser en permettant une application "au plus k fois".
	  */

	  /* Si cette reconnaissance a deja ete faite depuis l'etat cur_p, on ne recommence pas */
	  if (!XxY_set (&XxY_duplicate_hd, cur_p, rhs_list_id, &dup_id)) {
	    /* Non, c'est la 1ere fois */

	    /* on construit le commentaire complexe, commun � toutes les formes de partie droite des r�gles concern�es */
	    SXINT lhs_length = top_lhs_id_stack - cur_lhs_id_stack;
	    
	    if (lhs_length == 1)
	      rhs_comment_ste = comment_ste;
	    else
	      rhs_comment_ste = make_rhs_comment_ste (DTOP (lhs_comment_ste_stack) - lhs_length + 1, lhs_length);
	    
	    rhs_list_stack = rhs_list+rhs_list_id;

	    x_top = TOP (rhs_list_stack);
	    /* Il y a x_top regles de reecriture qui ont la lhs precedente */

	    for (x = 1; x <= x_top; x++) {
	      rhs_id = rhs_list_stack [x];

	      bot = rhs_str_list_display [rhs_id];
	      top = rhs_str_list_display [rhs_id+1];

	      prev_p = cur_p;

	      while (bot < top) {
		trans = make_a_new_trans (rhs_id_list [bot], rhs_str_list [bot], strlen (rhs_str_list [bot]));

		/* On met une transition sur les chaines */
		if (bot+1 == top)
		  next_q = q;
		else
		  next_q = ++max_acc_dag_state;
	
		if (!XxYxZ_set (&acc_dag, prev_p, trans, next_q, &triple)) {
		  /* On vient d'ajouter une transition au dag, il n'est donc pas stabilise', il faudra recommencer */
		  acc_dag2comment_ste [triple] = rhs_comment_ste;
		  has_new_acc_trans = SXTRUE;
		}

		bot++;
		prev_p = next_q;
	      }
	    }
	  }
	}
      }
    }
  }

  /* Il peut y avoir d'autres exit + loin, il faut continuer */
  XxYxZ_Xforeach (acc_dag, q, triple) {
    trans = XxYxZ_Y (acc_dag, triple);
    id = XxY_X (acc_trans_hd, trans);

    if (id)
      /* acc */
      lhs_walker (triple);
    else {
      /* Ce n'est pas un composant d' amalgam/compound ... */
      /* ... on met un trou ... */
      DPUSH (lhs_p_stack, 0);
      DPUSH (lhs_id_stack, 0);
      DPUSH (lhs_comment_ste_stack, 0);
      /* ... et on recherche un nouveau point d'entree sur ce chemin */
      dag_walker (XxYxZ_Z (acc_dag, triple));
      /* ... on pop le trou */
      DPOP (lhs_p_stack);
      DPOP (lhs_id_stack);
      DPOP (lhs_comment_ste_stack);
    }
  }

  DPOP (lhs_p_stack);
  DPOP (lhs_id_stack);
  DPOP (lhs_comment_ste_stack);
}


static SXBOOLEAN
close_local_dags (final_dag_state)
     SXINT final_dag_state;
{
  SXINT triple, triple2, acc_trans_id, ste, last_triple_before_dag_walker;

  has_new_acc_trans = SXTRUE;

  last_triple_before_dag_walker = XxYxZ_top (acc_dag);

  while (has_new_acc_trans) {
    has_new_acc_trans = SXFALSE;
    
    dag_walker (1);
  }

  for (triple = XxYxZ_top (acc_dag); triple > last_triple_before_dag_walker; triple--) {
    if (!XxYxZ_is_erased (acc_dag, triple) && (acc_trans_id = XxYxZ_Y (acc_dag, triple))) {
      ste = XxY_Y (acc_trans_hd, acc_trans_id);
      if (sxdfa_comb_get (dico_des_ff, sxstrget (ste), sxstrlen (ste))
#if defined(dico_ff_checkonly_h) || defined(has_co_dico)
	  || sxdfa_comb_get (dico_des_ff_checkonly, sxstrget (ste), sxstrlen (ste))
#endif /* defined(dico_ff_checkonly_h) || defined(has_co_dico) */
	  ) {
	XxYxZ_set (&if_dag, XxYxZ_X (acc_dag, triple), ste, XxYxZ_Z (acc_dag, triple), &triple2);
	if_comment [triple2] = sxstrget (acc_dag2comment_ste [triple]);
      }
    }
  }

  /* Ici if_dag est complet */
  /* On cherche tous les chemins entre 1 et final_dag_state */

  /* ... ou en utilisant fsa2re */
  {
    extern SXVOID       fermer (SXBA *R, SXBA_INDEX taille);
    SXINT                 nb, id, tok_no, ste, lgth, top, cur;
    char                *comment, *bot_str, *top_str, *str, *saved_CUR_COMMENT, *saved_CUR_STR;
    SXBA                *R, *R_plus;
    SXINT               i, j;

    nb = max_acc_dag_state+1;
    R = sxbm_calloc (nb+1, nb+1);
    R_plus = sxbm_calloc (nb+1, nb+1);

    for (triple = 1; triple <= XxYxZ_top (if_dag); triple++) {
      i = XxYxZ_X (if_dag, triple);
      /* ste = XxYxZ_Y (if_dag, triple); */
      j = XxYxZ_Z (if_dag, triple);

      SXBA_1_bit (R [i], j);
      SXBA_1_bit (R_plus [i], j);
    }

    fermer (R_plus, nb+1);

#ifdef HAS_CMPD_OR_AMLGM
    if (!SXBA_bit_is_set (R_plus [1], final_dag_state)
	&& (top = final_dag_state)) {
      SXINT offset = 0;

      // on a un cc (ou plusieurs) qui ne forment pas un chemin!!! (type "afin" non-suivi de "de")
      // on va tenter de corriger en forme flechie chacun des cc, � condition qu'ils commencent par une minuscule
      for (cur = 1; cur < top; cur++) {
	if (!XxYxZ_is_erased (acc_dag, cur) && (acc_trans_id = XxYxZ_Y (acc_dag, cur))) { /* si == 0, c'est une transition bidon (rechercher '== 2' dans ce source) */
	  i = max_acc_dag_state /* car store_if_id va mette dans if_dag une transition de max_acc_dag_state � max_acc_dag_state + 1, il doit donc �tre positionn� �: */ = XxYxZ_X (acc_dag, cur);
	  j = XxYxZ_Z (acc_dag, cur); // qui vaut, esp�rons-le, i+1 ;-)
	  ste = XxY_Y (acc_trans_hd, acc_trans_id);
	  saved_CUR_COMMENT = CUR_COMMENT;
	  saved_CUR_STR = CUR_STR;
	  CUR_COMMENT = sxstrget (acc_dag2comment_ste [cur]);
	  CUR_STR = sxstrget (ste);
	  if (is_lower(*CUR_STR) && call_sxspell (dico_des_ff, &light_FSA, sxstrget (ste), sxstrlen (ste), store_if_id, SXTRUE /* is_deterministic */)) {
	  } else {
	    store_if_str (sxstrget (ste), 0);
	    store_if_str (NULL, 0);
	  }
	  SXBA_1_bit (R [i], j);
	  SXBA_1_bit (R_plus [i], j);
	  CUR_COMMENT = saved_CUR_COMMENT;
	  CUR_STR = saved_CUR_STR;
	}
      }
      
      fermer (R_plus, nb+1);
    }
#endif /* HAS_CMPD_OR_AMLGM */

    if (SXBA_bit_is_set (R_plus [1], final_dag_state)) {
      /* Il y a (au moins) un chemin */
      char cur_car;
      char* op[6] = {"(", ")*", "(", ")+", "(", ")?"};

      varstr_raz (vstr);
      saved_CUR_COMMENT = CUR_COMMENT;
      vstr = fsa2re (vstr, R, R_plus, nb, 1, final_dag_state, edges, op);
      CUR_COMMENT = saved_CUR_COMMENT;

      /* S'il y a un "|" non parenthese', on parenthese */
      top_str = vstr->current;
      bot_str = vstr->first;

      nb = 0;

      while (bot_str < top_str) {
	cur_car = *bot_str;

	if (cur_car == '(')
	  nb++;
	else {
	  if (cur_car == ')')
	    nb--;
	}
	
	if (nb == 0 && cur_car == '|' &&
	    (bot_str == vstr->first || *(bot_str - 1) != '[' )
	    && *(bot_str + 1) != ']'
	    )
	  break;

	bot_str++;
      }

      /* Les echappements ont ete faits ds edges */
      if (bot_str < top_str)
	printf ("(%s) ", varstr_tostr (vstr));
      else
	printf ("%s ", varstr_tostr (vstr));
    }
    else {
      /* Echec, on sort ... */
      saved_CUR_COMMENT = CUR_COMMENT;
      
      for (tok_no = from_tok_no; tok_no < to_tok_no; tok_no++) {
	ste = SXGET_TOKEN (tok_no).string_table_entry;

	if (ste > SXEMPTY_STE) {
	  str = sxstrget (ste);
	  lgth = sxstrlen (ste);
	  CUR_COMMENT = SXGET_TOKEN (tok_no).comment;
	    
	  if (from_tok_no == init_tok_no) {
	    /* ... "({com} _Uw | {com} _uw) " */
	    if (is_output_uw) {
	      fputs ("(", stdout);
	      output_ff_with_comment (Uw);
	      fputs (" | ", stdout);
	      output_ff_with_comment (uw);
	      fputs (")", stdout);
	    } else
	      output_ff_with_comment (str);
	    fputs (" ", stdout);
	  }
	  else {
	    /* ... "{com} _uw" ou "{com} _Uw" */
	    if (is_output_uw) {
	      if (!is_upper(*str))
		/* minuscule initiale */
		output_ff_with_comment (uw);
	      else
		output_ff_with_comment (Uw);
	    } else
	      output_ff_with_comment (str);
	    fputs (" ", stdout);
	  }
	}
      }
      CUR_COMMENT = saved_CUR_COMMENT;	  
    }

    sxbm_free (R);
    sxbm_free (R_plus);
  }

  XxYxZ_clear (&acc_dag);
  XxY_clear (&acc_trans_hd);
  DTOP (lhs_id_stack) = 0;
  DTOP (lhs_p_stack) = 0;
  DTOP (lhs_comment_ste_stack) = 0;
  XxY_clear (&XxY_duplicate_hd);
#if 0
  amalgam_stack [0] = 0;
  XxYxZ_clear (&amalgam_dag);
  XxYxZ_clear (&compound_component_dag);
  initial_compound_component_stack [0] = 0;
#endif /* 0 */
  XxYxZ_clear (&if_dag);
}

#if 0
static SXBOOLEAN
close_local_dags (final_dag_state)
     SXINT final_dag_state;
{
  SXINT  triple, i, j, k, l, id, x = 1, hd, ste;
  char *comment, *orig_comment;

  while (!IS_EMPTY (amalgam_stack) || has_new_cc) {
    while (!IS_EMPTY (amalgam_stack)) {
      triple = POP (amalgam_stack);
      i = XxYxZ_X (amalgam_dag, triple);
      id = XxYxZ_Y (amalgam_dag, triple);
      j = XxYxZ_Z (amalgam_dag, triple);
      orig_comment = amalgam_comment [triple];

      /* On etend id */
      for (x = ac_id2amlgm_list [id]; x != 0; x = amlgm_list [x].next) {
	hd = amlgm_list [x].hd;
	k = i;
	comment = orig_comment;

	while ((id = amlgm_component_stack [hd++]) > 0) {
	  if (amlgm_component_stack [hd])
	    /* pas le dernier */
	    l = ++max_acc_dag_state;
	  else
	    l = j;

	  if (SXBA_bit_is_set (amalgam_id_set, id)) {
	    /* amalgam */
	    if (!XxYxZ_set (&amalgam_dag, k, id, l, &triple)) {
	      PUSH (amalgam_stack, triple);
	      amalgam_comment [triple] = comment;
	    }
	  }

	  if (SXBA_bit_is_set (compound_component_id_set, id)) {
	    /* compound component */
	    if (!XxYxZ_set (&compound_component_dag, k, id, l, &triple)) {
	      /* Nouveau */
	      compound_component_comment [triple] = comment;
	      /* On remplit compound_component_dag jusqu'a stabilite */
	      has_new_cc = SXTRUE;

	      if (SXBA_bit_is_set (initial_compound_component_id_set, id))
		PUSH (initial_compound_component_stack, triple);
	    }
	  }

	  //	  if (SXBA_bit_is_set (if_id_set, id)) {
	    /* forme flechie */
	    ste = sxstrsave (ac_id2string [id]);
	    XxYxZ_set (&if_dag, k, ste, l, &triple);
	    if_comment [triple] = comment;
	    //	  }

	  k = l;
	  /* On associe le commentaire d'origine a tous les mots de l'expansion de l'amalgamme */
	  comment = orig_comment;
	}
      }
    }

    if (has_new_cc) {
      has_new_cc = SXFALSE;

      for (x = 1; x <= initial_compound_component_stack [0]; x++) {
	triple = initial_compound_component_stack [x];
	i = XxYxZ_X (compound_component_dag, triple);
	id = XxYxZ_Y (compound_component_dag, triple);
	j = XxYxZ_Z (compound_component_dag, triple);

	PUSH (cc_stack, id);
	cc_comment_stack [TOP (cc_stack)] = compound_component_comment [triple];

	if (cc_path (i, j))
	  has_new_cc = SXTRUE;

	POP (cc_stack);
      }
    }
  }

  /* Ici if_dag est complet */
  /* On cherche tous les chemins entre 1 et final_dag_state */

  /* ... ou en utilisant fsa2re */
  {
    extern SXVOID       fermer (SXBA *R, SXBA_INDEX taille);
    SXINT                 nb, id, tok_no, ste, lgth, top, cur;
    char                *comment, *bot_str, *top_str, *str, *saved_CUR_COMMENT;
    SXBA                *R, *R_plus;

    nb = max_acc_dag_state+1;
    R = sxbm_calloc (nb+1, nb+1);
    R_plus = sxbm_calloc (nb+1, nb+1);

    for (triple = 1; triple <= XxYxZ_top (if_dag); triple++) {
      i = XxYxZ_X (if_dag, triple);
      /* ste = XxYxZ_Y (if_dag, triple); */
      j = XxYxZ_Z (if_dag, triple);

      SXBA_1_bit (R [i], j);
      SXBA_1_bit (R_plus [i], j);
    }

    fermer (R_plus, nb+1);

#ifdef HAS_CMPD_OR_AMLGM
    if (!SXBA_bit_is_set (R_plus [1], final_dag_state)
	&& (top = XxYxZ_top (compound_component_dag))) {
      SXINT offset = 0;

      // on a un cc (ou plusieurs) qui ne forment pas un chemin!!! (type "afin" non-suivi de "de")
      // on va tenter de corriger en forme flechie chacun des cc
      for (cur = 1; cur <= top; cur++) {
	i = max_acc_dag_state = XxYxZ_X (compound_component_dag, cur);
	j = XxYxZ_Z (compound_component_dag, cur); // qui vaut, esp�rons-le, i+1
	str = ac_id2string [XxYxZ_Y (compound_component_dag, cur)];
	vstr = varstr_catenate (varstr_raz (vstr), str);
	saved_CUR_COMMENT = CUR_COMMENT;
	CUR_COMMENT = compound_component_comment [cur];
	if (call_sxspell (dico_des_ff, &light_FSA, varstr_tostr (vstr), varstr_length (vstr), store_if_id, SXTRUE /* is_deterministic */)) {
	  SXBA_1_bit (R [i], j);
	  SXBA_1_bit (R_plus [i], j);
	}
	CUR_COMMENT = saved_CUR_COMMENT;
      }

      fermer (R_plus, nb+1);

    }
#endif /* HAS_CMPD_OR_AMLGM */

    if (SXBA_bit_is_set (R_plus [1], final_dag_state)) {
      /* Il y a (au moins) un chemin */
      char cur_car;
      char* op[6] = {"(", ")*", "(", ")+", "(", ")?"};

      varstr_raz (vstr);
      saved_CUR_COMMENT = CUR_COMMENT;
      vstr = fsa2re (vstr, R, R_plus, nb, 1, final_dag_state, edges, op);
      CUR_COMMENT = saved_CUR_COMMENT;

      /* S'il y a un "|" non parenthese', on parenthese */
      top_str = vstr->current;
      bot_str = vstr->first;

      nb = 0;

      while (bot_str < top_str) {
	cur_car = *bot_str;

	if (cur_car == '(')
	  nb++;
	else {
	  if (cur_car == ')')
	    nb--;
	}
	
	if (nb == 0 && cur_car == '|' &&
	    (bot_str == vstr->first || *(bot_str - 1) != '[' )
	    && *(bot_str + 1) != ']'
	    )
	  break;

	bot_str++;
      }

      /* Les echappements ont ete faits ds edges */
      if (bot_str < top_str)
	printf ("(%s) ", varstr_tostr (vstr));
      else
	printf ("%s ", varstr_tostr (vstr));
    }
    else {
      /* Echec, on sort ... */
      id = 0;
      for (tok_no = from_tok_no; tok_no < to_tok_no; tok_no++) {
	ste = SXGET_TOKEN (tok_no).string_table_entry;
	
	if (ste > SXEMPTY_STE) {
	  str = sxstrget (ste);
	  lgth = sxstrlen (ste);
	  CUR_COMMENT = SXGET_TOKEN (tok_no).comment;
	  
	  if (is_output_uw) {
	    if (from_tok_no == init_tok_no) {
	      /* ... "({com} _Uw | {com} _uw) " */
	      fputs ("(", stdout);
	      output_ff_with_comment (Uw);
	      fputs (" | ", stdout);
	      output_ff_with_comment (uw);
	      fputs (") ", stdout);
	    } else
	      output_ff_with_comment (str);
	  }
	  else {
	    if (is_output_uw) {
	      /* ... "{com} _uw" ou "{com} _Uw" */
	      if (!is_upper(*str))
		/* minuscule initiale */
		output_ff_with_comment (uw);
	      else
		output_ff_with_comment (Uw);
	    } else
	      output_ff_with_comment (str);
	    fputs (" ", stdout);
	  }
	}
      }
    }

    sxbm_free (R);
    sxbm_free (R_plus);
  }

  XxYxZ_clear (&acc_dag);
  XxY_clear (&acc_trans_hd);
  DTOP (lhs_id_stack) = 0;
  DTOP (lhs_p_stack) = 0;
  DTOP (lhs_comment_ste_stack) = 0;
  XxY_clear (&XxY_duplicate_hd);
#if 0
  amalgam_stack [0] = 0;
  XxYxZ_clear (&amalgam_dag);
  XxYxZ_clear (&compound_component_dag);
  initial_compound_component_stack [0] = 0;
  XxYxZ_clear (&if_dag);
#endif /* 0 */
}
#endif /* 0 */
#endif /* HAS_CMPD_OR_AMLGM */

static SXINT
store_if_has_capital_letter (x, FSA_ptr)
     SXINT x;
     struct FSA *FSA_ptr;
{
  SXINT id = SXSPELL_x2id (*FSA_ptr, x);
  char *cur_str;
  SXINT cur_lgth;

  /* Cette correction ne devient effective que si elle contient des majuscules. Ex : Etats-Unis => �tats-Unis
     sinon text2dag le recommencera en non-deterministe. Exemple RENE => Ren� | r�ne | ren� */
  if (id) {
    if (!SXSPELL_has_subwords (id)) {
      /* correction en un seul mot */
      cur_str = SXSPELL_id2str (*FSA_ptr, id);
      cur_lgth = strlen (cur_str);

      if (cur_lgth && has_upper (cur_str, cur_lgth))
	/* �tats-Unis ou Ren� */
	push_string (prefix_stack_ptr, cur_str, cur_str+cur_lgth);

      else
	/* r�ne | ren� => on reste avec RENE, c'est la dageur qui corrigera */
	id = 0;
    }
    else
      id = 0;
  }

  return id;
}

static SXINT
store_correction_cost (x, FSA_ptr)
     SXINT x;
     struct FSA *FSA_ptr;
{
  SXINT     *bot_ptr, *top_ptr, id;
  char    *c_str;
  
  if (x>1)
#if EBUG
    sxtrap (ME, "Bug and implementation restriction: as for now, we expect only one correction from sxspeller");
#else /* ?EBUG */
    return 0;
#endif /* EBUG */

  if (x == 0)
    return 0;

  id = SXSPELL_x2id (*FSA_ptr, x); /* LA correction */

  global_cost += SXSPELL_x2weight (*FSA_ptr, x); /* execut� une seule fois, en x=1 */

  return id;
}


static SXINT
store_in_prefix_stack (x, FSA_ptr)
     SXINT x;
     struct FSA *FSA_ptr;
{
  SXINT     *bot_ptr, *top_ptr, id;
  char    *c_str;
  
  if (x>1)
#if EBUG
    sxtrap (ME, "Bug and implementation restriction: as for now, we expect only one correction from sxspeller");
#else /* ?EBUG */
    return 0;
#endif /* EBUG */

  if (x == 0)
    return 0;

  id = SXSPELL_x2id (*FSA_ptr, x); /* LA correction */

  light_cost += SXSPELL_x2weight (*FSA_ptr, x); /* execut� une seule fois, en x=1 */
    
  if (!SXSPELL_has_subwords (id)) {
    c_str = SXSPELL_id2str (*FSA_ptr, id);
    if (strlen (c_str))
      push_string (prefix_stack_ptr, c_str, c_str+strlen (c_str));
    else
      id = 0;
  }
  else {
    /* cas sous-mots */
    SXSPELL_subword_foreach (*FSA_ptr, id, bot_ptr, top_ptr) {
      id = SXSPELL_x2subword_id (bot_ptr);
      c_str = SXSPELL_id2str (*FSA_ptr, id);
      if (strlen (c_str))
	push_string (prefix_stack_ptr, c_str, c_str+strlen (c_str));
      else 
	id = 0;
    }
  }

  return id;
}

#if 0
static SXINT
store_best_correction_in_cvstr (x, FSA_ptr)
     SXINT x;
     struct FSA *FSA_ptr;
{
  SXINT     *bot_ptr, *top_ptr, id;
  char    *c_str;
  
  id = 0;
  
  if (x == 1 /* on ne prend que la meilleure */
      && (id = SXSPELL_x2id (*FSA_ptr, x))) { /* LA correction */
    /* On ne prend que le 1er resultat parmi les meilleurs */
    heavy_cost = SXSPELL_x2weight (*FSA_ptr, x); /* x = 1 pour le moment */
    
    varstr_raz (cvstr);
    
    if (!SXSPELL_has_subwords (id)) {
      if (varstr_length (cc_vstr)) // �tait vstr
	cvstr = varstr_catenate (cvstr, varstr_tostr (cc_vstr)); //�tait cur_correction->vstr
      
      cvstr = varstr_catenate (varstr_catenate (cvstr, SXSPELL_id2str (*FSA_ptr, id)), " ");
    }
    else {
      /* cas sous-mots */
      SXSPELL_subword_foreach (*FSA_ptr, id, bot_ptr, top_ptr) {
	if (varstr_length (cc_vstr)) // �tait vstr
	  cvstr = varstr_catenate (cvstr, varstr_tostr (cc_vstr)); //�tait cur_correction->vstr
	
	cvstr = varstr_catenate (varstr_catenate (cvstr,
					  SXSPELL_id2str (*FSA_ptr, SXSPELL_x2subword_id (bot_ptr))),
			 " ");
      }
    }
  }

  return id;
}
#endif /* 0 */

/* Essai ds lequel on donne priorite' au plus long et a egalite de longueur aux prefixes suffixes */
static SXBOOLEAN is_process_prefix_finished, is_process_suffix_finished;

/* Version light qui n'extrait que les vrais suffixes, le reste est fait par call_process_prefix */
static SXBOOLEAN
call_process_suffix (str_lptr, str_rptr)
     char    **str_lptr, **str_rptr;
{
  SXINT  action, cur_lgth;
  char *bot_str, *top_str, *lstr, *rstr, *cur_str, car;


  bot_str = *str_lptr;
  top_str = *str_rptr;

  while (!is_process_suffix_finished) {
    cur_str = bot_str;
    cur_lgth = top_str-bot_str;

    /* On regarde si le noyau restant est un mot connu, si oui on n'essaie pas d'en detacher d'autres suffixes */
    if (sxdfa_comb_get (dico_des_mots, bot_str, top_str-bot_str)) {
      /* ... oui */
      /* on le stocke */
      push_string (prefix_stack_ptr, bot_str, top_str);
      bot_str = top_str; /* l'appelant n'a plus rien a faire */
      is_process_suffix_finished = SXTRUE; /* fini */
    }
    
    if (!is_process_suffix_finished) {
      /* On regarde aussi si le noyau courant est la capitalisation d'un mot connu, si oui on
	 n'essaie pas d'en detacher d'autres suffixes */
      if (*bot_str == sxtoupper(*bot_str)) {
	*bot_str = ttdtolower(*bot_str); //changer
	if (sxdfa_comb_get (dico_des_mots, bot_str, cur_lgth)) {
	  /* ... oui */
	  /* on le stocke */
	  *bot_str = ttdtoupper(*bot_str);
	  push_string (prefix_stack_ptr, bot_str, top_str);
	  bot_str = top_str; /* l'appelant n'a plus rien a faire */
	  is_process_suffix_finished = SXTRUE; /* fini */
	} else
	  *bot_str = ttdtoupper(*bot_str);
      }
    }

    if (!is_process_suffix_finished) {
      action = sxdfa_comb_get_bounded (dico_des_suffixes, &cur_str, &cur_lgth);

      /* ajout BS: chosophile, machiniphage ; pas de v�rification que le premier morceau est corrigeable � peu de frais => TODO */
      if (action > 0 && (process_suffix || action > 1000)) {
	if (action > 1000)
	  action -= 1000;
	switch (action) {
	case 8:
	  /* pour le kurmanji : ne marche qu'apr�s a i � e � u � o */
          if (cur_lgth<=2
	      || ( *(bot_str+cur_lgth-1)!='u' && *(bot_str+cur_lgth-1)!='�'
		   && *(bot_str+cur_lgth-1)!='e' && *(bot_str+cur_lgth-1)!='�'
		   && *(bot_str+cur_lgth-1)!='i' && *(bot_str+cur_lgth-1)!='�'
		   && *(bot_str+cur_lgth-1)!='a' && *(bot_str+cur_lgth-1)!='o'
		   )
	      ) {
	    is_process_suffix_finished = SXTRUE;
          }
	  break;
	case 7:
	  /* pour le kurmanji : ne marche qu'apr�s autre chose que a i � e � u � o */
          if (cur_lgth<=2
	      || *(bot_str+cur_lgth-1)=='u' || *(bot_str+cur_lgth-1)=='�'
	      || *(bot_str+cur_lgth-1)=='e' || *(bot_str+cur_lgth-1)=='�'
	      || *(bot_str+cur_lgth-1)=='i' || *(bot_str+cur_lgth-1)=='�'
	      || *(bot_str+cur_lgth-1)=='a' || *(bot_str+cur_lgth-1)=='o'
	      ) {
	    is_process_suffix_finished = SXTRUE;
          }
	  break;
	case 6:
	  /* d�tachement d'un suffixe pur ssi ce qui reste est connu du lexique ou commence par une minuscule */
          if (cur_lgth<=2 || !((!is_upper(*bot_str)) || sxdfa_comb_get (dico_des_mots, bot_str, cur_lgth-2))) {
	    is_process_suffix_finished = SXTRUE;
          }
	  break;
	case 5:
	  /* robi�em : ne marche qu'apr�s � (l barr� en latin2) */
          if (cur_lgth<=2 || *(bot_str+cur_lgth-1)!='�') {
	    is_process_suffix_finished = SXTRUE;
          }
	  break;
	case 4:
	  /* robilismy / robi�ysme : ne marche qu'apr�s li ou �y*/
          if (cur_lgth<=3 || !((*(bot_str+cur_lgth-2)=='l' && *(bot_str+cur_lgth-1)=='i') || (*(bot_str+cur_lgth-2)=='�' && *(bot_str+cur_lgth-1)=='y'))) {
	    is_process_suffix_finished = SXTRUE;
          }
	  break;
	case 3:
	  /* robi�am / robi�om : ne marche qu'apr�s �a ou �o*/
          if (cur_lgth<=3 || (!(*(bot_str+cur_lgth-2)=='�' && (*(bot_str+cur_lgth-1)=='o' || *(bot_str+cur_lgth-1)=='a')))) {
	    is_process_suffix_finished = SXTRUE;
          }
	  break;
	case 2:
	  /* chosophile -> chose _phile ; ne marche qu'avec [oi] � la fin du premier morceau, qui a le droit de devenir 'e' si besoin; ne marche pas si le mot a une majuscule initiale */
          if (cur_lgth<=0 || (*(bot_str+cur_lgth-1)!='o' && *(bot_str+cur_lgth-1)!='i') || is_upper(*bot_str)) {
	    is_process_suffix_finished = SXTRUE;
	    break;
          }
	  if (!sxdfa_comb_get (dico_des_mots, bot_str, cur_lgth))
	    *(bot_str+cur_lgth-1)='e';
	  break;
	case 1:
	  break;
	default:
	  is_process_suffix_finished = SXTRUE;
	  break;
	}

	if (!is_process_suffix_finished) {
	  /* ...y'a un suffixe et un noyau qui peut etre vide */
	  /* On stocke le separateur ... */
	  lstr = action2suffix_chars [action];
	  rstr = lstr + strlen (lstr);
	  push_string (suffix_stack_ptr, lstr, rstr);
	  light_cost += detach_weight; /* A voir */
	  /* ... et le suffixe ... */
	  lstr = bot_str+cur_lgth;
	  varstr_lcatenate (suffix_stack_ptr->vstrs [suffix_stack_ptr->top], lstr, top_str-lstr);
	
	  top_str = lstr;
	}
      }
      else
	is_process_suffix_finished = SXTRUE;
    }
  }

  *str_lptr = bot_str;
  *str_rptr = top_str;

  return bot_str == top_str;
}

static SXINT call_process_prefix_depth;

static SXBOOLEAN
call_process_prefix (str_lptr, str_rptr, has_initial_maj)
     char    **str_lptr, **str_rptr;
     SXBOOLEAN has_initial_maj;
{
  SXINT     id, action, action2, cur_lgth, cur_lgth2, store_weight_limit, left_pos, cur_pos, right_pos, pref_lgth, lgth;
  char    *cur_str, *cur_str2, *bot_str, *top_str, *cur_wstr, *lstr, *rstr;
  SXBOOLEAN done, pushed;
  char    *mot_courant;

  call_process_prefix_depth++;

  mot_courant = bot_str = *str_lptr;
  top_str = *str_rptr;

  /* Calcul de hyphen_set dynamique car on travaille sur un noyau du mot d'origine, les positions
     des tirets ont donc pu changer */
  sxba_empty (hyphen_set);

  if (top_str-bot_str >= (lgth = SXBASIZE (hyphen_set))) {
    lgth += top_str-bot_str;
    hyphen_set = sxba_resize (hyphen_set, lgth);
  }

  for (cur_str = bot_str, cur_pos = 0; cur_str < top_str; cur_str++, cur_pos++) {
    if (*cur_str == '-' || *cur_str == '�')
      SXBA_1_bit (hyphen_set, cur_pos);
  }

  /* a priori */
  is_process_prefix_finished = SXFALSE;
  is_process_suffix_finished = SXFALSE;

  if (bot_str < top_str /* tout n'est pas traite' */
      //      && process_suffix
      && !is_process_suffix_finished) {
    call_process_suffix (&bot_str, &top_str);
  }

  // TODO: pourquoi?  fprintf (sxstderr, "%s - %s\n", bot_str, top_str);

  if (bot_str < top_str /* tout n'est pas trait� */
      && process_prefix) {
    /* is_process_prefix_finished == SXTRUE <=> La chaine a ete traitee en entier ou on n'a pas extrait de prefixe */
    /* super_boucle */
    while (!is_process_prefix_finished) {
      if (bot_str == top_str /* tout est trait� */) {
	is_process_prefix_finished = SXTRUE;
	break;
      }

      /* Traitement iteratif et non recursif */
      /* le noyau est-il une ff ou une light-correction de ff ? */
      id = sxdfa_comb_get (dico_des_mots, bot_str, top_str-bot_str);

      if (id != 0) {
	/* ... oui */
	/* on le stocke */
	push_string (prefix_stack_ptr, bot_str, top_str);
	light_cost += detach_weight;
	bot_str = top_str;
	is_process_prefix_finished = SXTRUE;
      }
      else if (is_light_correction) {
	if (call_process_prefix_depth > 1 /* pas besoin d'essayer si on est sur l'appel le plus externe, car �a a �t� fait � l'ext�rieur */
	    //	    && (id = call_light_sxspell (dico_des_mots, &light_FSA, bot_str, top_str-bot_str, store_in_prefix_stack, SXTRUE))) {
	    && (id = call_super_light_sxspell (dico_des_mots, &light_FSA, bot_str, top_str-bot_str, store_in_prefix_stack))) {
	  bot_str = top_str;
	  is_process_prefix_finished = SXTRUE;
	}
      } 
      if (id == 0) {
	cur_str = bot_str;
	cur_lgth = top_str-bot_str;

	action2 = 0; /* ce n'est pas une correction qui a donne le prefixe */
	
	if ((action = sxdfa_comb_get_bounded (dico_des_prefixes, &cur_str, &cur_lgth)/* && cur_lgth > 0 */))
	  pref_lgth = cur_str-bot_str;

	left_pos = ((action) ? cur_str : bot_str) - mot_courant;

	/* On recherche le tiret le + a droite  */
	/* hyphen_set contient les emplacements de tous les tirets */
	cur_pos = top_str-mot_courant;

	/* BOUCLE */
	while ((cur_pos = sxba_1_rlscan (hyphen_set, cur_pos)) > left_pos) {
	  /* y'a un tiret en cur_pos et le prefixe est de longueur au moins 1 */
	  cur_str2 = bot_str;
	  cur_lgth2 = cur_pos - (bot_str-mot_courant);

	  id = 0;

	  if (is_light_correction && id == 0 && action == 0) {
	    /* Ce prefixe ne peut pas se detacher directement */
	    /* On tente une correction light seulement s'il n'y a plus de tiret a gauche */
	    if (sxba_1_rlscan (hyphen_set, cur_pos) <= left_pos) {
	      /* Pour l'instant je ne mets pas le tiret ds le mot a corriger.  Il est possible de faire les
		 2 tentatives */
	      cur_str = mot_courant+cur_pos;

	      if (is_light_correction && !has_initial_maj && !has_upper (bot_str, cur_str-bot_str)
		  && (id = call_light_sxspell (dico_des_mots, &light_FSA, bot_str, cur_str-bot_str, store_in_prefix_stack, SXTRUE))) {
		/* On saute le tiret car il ne peut pas faire partie integrante d'un suffixe ... */
		bot_str = mot_courant+cur_pos+1;
		/* ... et on recommence */
		id = 1;
		break;
	      }
	    }
	  }
	}
	/* Fin de BOUCLE */

	/* decollage */
	if (id == 0) {
	  /* on n'a rien pu detacher ... */
	  if (action) {
	    /* ... mais y'a un prefixe et un noyau qui peut etre vide */
	    done = SXFALSE;
	    pushed = SXFALSE;

	    switch (action) {
	    case 1:
	      /* Le prefixe est du type "anti-" */
	      /* Si le suffixe commence par une majuscule, on n'y touche pas : cas "anti-Seattle" */
	      if (SXBA_bit_is_set (MAJ_set, ((unsigned char) *cur_str))) {

		/* on stocke le prefixe ... */
		push_string (prefix_stack_ptr, bot_str, cur_str);

		/* ... avec son separateur (si ce n'est pas le 1er mot d'une phrase) */
		if (has_initial_maj) {
		  has_initial_maj = SXFALSE;
		}
		else {
		  varstr_catenate (prefix_stack_ptr->vstrs [prefix_stack_ptr->top], action2prefix_chars [action]);
		}

		/* ... et Seattle */
		if (top_str > cur_str) /* BS: rajout� sans comprendre */
		  push_string (prefix_stack_ptr, cur_str, top_str);
	  
		light_cost += detach_weight;
		bot_str = top_str;
		done = SXTRUE;
	      }

	      break;

	    case 2:
	      /* Le prefixe est du type "anti" */
	      /* On regarde si en ajoutant un tiret le mot "anti-suffixe:" est une forme flechie.
		 Si oui, on va privilegier ce cas */
	      wstr = varstr_raz (wstr);

	      while (wstr->current+(pref_lgth+cur_lgth)+1 >= wstr->last)
		wstr = varstr_realloc (wstr);

	      varstr_lcatenate (wstr, bot_str, pref_lgth);

	      varstr_catchar (wstr, '-');
	      varstr_lcatenate (wstr, cur_str, cur_lgth);
	
	      cur_wstr = varstr_tostr (wstr);
	      cur_lgth2 = varstr_length (wstr);

	      id = sxdfa_comb_get (dico_des_mots, cur_wstr, cur_lgth2);

	      if (id) {
		/* oui */
		push_string (prefix_stack_ptr, cur_wstr, cur_wstr+cur_lgth2);
		light_cost += detach_weight;
		bot_str = top_str;
		done = SXTRUE;
	      }

	      break;

	    case 3:
	      /*  Le prefixe est du type "entr'", on supprime l'apos que l'on remplace par un e */
	      push_string (prefix_stack_ptr, bot_str, cur_str-1);

	      light_cost += detach_weight;
	      pushed = SXTRUE;
	      break;

	    case 4:
	      break;

	    case 5:
	      /* Le prefixe est du type "d�s" */
	      /* On ne le valide que s'il est suivi par une voyelle */
	      if (!SXBA_bit_is_set (vowell_set, ((unsigned char) *cur_str)))
		done = SXTRUE;

	      break;

	    case 6:
	      /* Le prefixe est du type "im" */
	      /* On ne le valide que s'il est suivi par "mpb" */
	      if (!SXBA_bit_is_set (mpb_set, ((unsigned char) *cur_str)))
		done = SXTRUE;

	      break;
	    }
	
	    if (!done) {
	      /* meme si le suffixe n'est pas bon, on stocke le prefixe ... */
	      if (!pushed) {
		push_string (prefix_stack_ptr, bot_str, cur_str);
	  
		light_cost += detach_weight;
	      }

	      /* ... avec son separateur (si ce n'est pas le 1er mot d'une phrase ...) */
	      if (has_initial_maj) {
		has_initial_maj = SXFALSE;
	      }
	      else {
		varstr_catenate (prefix_stack_ptr->vstrs [prefix_stack_ptr->top], action2prefix_chars [action]);
	      }

	      bot_str = cur_str;
	    }
	    else {
	      is_process_prefix_finished = SXTRUE;
	    }
	  }
	  else
	    is_process_prefix_finished = SXTRUE;
	}
	/* fin decollage */
      }

    }
    /* Fin super_boucle */
  }

  /* Ici on a traite les suffixes et les prefixes au maximum */
  if (is_light_correction && bot_str < top_str && !has_initial_maj && !has_upper (bot_str, top_str-bot_str)) {
    /* Noyau non vide ... */
    /* ... on essaye de le corriger ... */
    /* ... avec un poids faible */

    if (call_light_sxspell (dico_des_mots, &light_FSA, bot_str, top_str-bot_str, store_in_prefix_stack, SXTRUE))
      bot_str = top_str;

  }

  *str_lptr = bot_str;
  *str_rptr = top_str;

  call_process_prefix_depth--;
  return bot_str == top_str;
}



/* La [sous-]chaine (str_lptr, str_rptr) n'est pas une forme flechie
   on la decoupe (recursivement) sur des tirets ou des apostrophes */
static SXBOOLEAN
call_cut_on_quote_hyphen (str_lptr, str_rptr, has_hyphen_in_prefix, is_pref_OK)
     char    **str_lptr, **str_rptr;
     SXBOOLEAN has_hyphen_in_prefix, is_pref_OK;
{
  SXINT  pref_id, suf_id, cur_lgth, action, cur_lgth2, cur_lgth3;
  char *cur_str, *cur_str2, *bot_str, *bot_str2, *bot_str3, car, *top_str, *work_str;
  SXBOOLEAN is_suf_OK;

  bot_str = cur_str = *str_lptr;
  top_str = *str_rptr;

  if (is_cut_on_hyphen && (*bot_str == '-' || *bot_str == '�')) {
    /* On saute le '-' du debut */
    has_hyphen_in_prefix = SXTRUE;
    bot_str = ++cur_str;

    if (sxdfa_comb_get (dico_des_mots, bot_str, top_str-bot_str)) {
      push_string (suffix_stack_ptr, bot_str, top_str);
      /* Attention le "-" final fait partie du noyau */
      *str_lptr = *str_rptr = bot_str;
      light_cost += 1; /* A voir */
      return SXTRUE;
    }
  }
    
  while (cur_str < top_str) {
    car = *cur_str++;
  
    if ((is_cut_on_quote && (car == '\'')) || (is_cut_on_hyphen && (car == '-' || car == '�'))) {
      is_suf_OK = SXTRUE;
      /* Si le prefixe et le suffixe sont ds le dico => OK */
      if (top_str-cur_str > 0) {
	/* suffixe non vide */
	bot_str2 = bot_str;
	cur_lgth2 = cur_str-bot_str;

	/* ajout� par Beno�t: on veut �tre s�r aussi que si on d�tache la partie gauche
	 en tant que pr�fixe on ne laisse pas en partie droite un "vrai" suffixe ;
	 ce n'est qu'ainsi qu'on peut avoir "programmeur-n�" -> "programmeur _-n�" et non "programmeur n�"*/ 
	bot_str3 = cur_str-1; /* "-1" car on r�cup�re le tiret */
	cur_lgth3 = top_str-cur_str+1;
	action = sxdfa_comb_get (dico_des_suffixes, bot_str3, cur_lgth3);
	if (action > 0) 
	  is_suf_OK = SXFALSE;

	action += sxdfa_comb_get_bounded (dico_des_prefixes, &bot_str2, &cur_lgth2);

	if (car == '-' || car == '�')
	  /* '-' fait partie du suffixe */
	  cur_str--;

	if (action == 0 /* il y a un mot pr�fixe et/ou un mot suffixe (d�tachable */ || cur_lgth2 > 0) {
	  /* Ce n'est pas un vrai prefixe */
	  pref_id = sxdfa_comb_get (dico_des_mots, bot_str, cur_str-bot_str);
	  
	  if (pref_id && is_pref_OK) {
	    /* Ds cette version, on stocke le prefixe ds tous les cas !! */
	    push_string (prefix_stack_ptr, bot_str, cur_str);
	    bot_str = cur_str;
	    light_cost += 1; /* A voir */
	  }
	  else
	    is_pref_OK = SXFALSE;
	}
	else {
	  /* C'est un vrai prefixe ... */
	  /* on favorise ce cas qui sera traite' + tard => aucune recherche ds dico_des_mots */
	  pref_id = 0;
	  is_pref_OK = SXFALSE;
	}

	/* petite verrue ... */
	if ((car == '\'') && has_upper (cur_str, top_str-cur_str) /* (SXBA_bit_is_set (MAJ_set, *cur_str)) */) {
	  /* Mot a majuscule apres une apostrophe => on arre^te ... */
	  if (pref_id) {
	    /* ... si le prefixe etait correct */
	    break;
	  }
	}

	if (is_suf_OK) {
	  /* ... et le suffixe */
	  suf_id = sxdfa_comb_get (dico_des_mots, cur_str, top_str-cur_str);
	 
	  if (is_light_correction && suf_id == 0)
	    /* on tente quand m�me une light */
	    suf_id = call_light_sxspell (dico_des_mots, &light_FSA, cur_str, top_str-cur_str, NULL, SXTRUE);
	  
	  if (suf_id) {
	    /* ... et le suffixe est une ff */
	    push_string (suffix_stack_ptr, cur_str, top_str);
	    top_str = cur_str;
	    light_cost += 1; /* A voir */
	    
	    if (pref_id && !is_pref_OK) {
	      /* le "bon" prefixe non contigu est un suffixe !! */
	      push_string (suffix_stack_ptr, bot_str, cur_str);
	      top_str = cur_str = bot_str;
	      light_cost += 1; /* A voir */
	    }
	  }
	  else {
	    /* ... sinon on regarde recursivement si c'en est une */
	    cur_str2 = cur_str;
	    suf_id = call_cut_on_quote_hyphen (&cur_str, &top_str, has_hyphen_in_prefix, is_pref_OK);
	    /* ds tous les cas top_str est correct !! */
	    
	    if (is_pref_OK)
	      bot_str = cur_str;
	    else {
	      /* Le 16/05/05 */
	      if (suf_id && pref_id) {
		/* le "bon" prefixe non contigu est un suffixe !! */
		push_string (suffix_stack_ptr, bot_str, cur_str2);
		top_str = cur_str = bot_str;
		light_cost += 1; /* A voir */
	      }
	    }
	  }
	}
	
	/* Ce sont les appels recursifs qui vont chercher les '-' et les '\'' sur la droite */
	break;
      }
    }
  }

  *str_lptr = bot_str;
  *str_rptr = top_str;

  return bot_str == top_str;
}


/* partie principale de feu sxspeller (ne pas confondre avec call_sxspell) */
static SXINT
call_sxspeller (mot, lgth, is_first_word/* , is_followed_by_a_nl */, local_is_spelling_correction)
     char    *mot;
     SXINT     lgth;
     SXBOOLEAN is_first_word;
     /* SXBOOLEAN is_followed_by_a_nl; */
     SXBOOLEAN local_is_spelling_correction;
{
  SXINT     cur_lgth, x, id, y, *bot_ptr, *top_ptr, action, prev_action, pre_lgth, suf_lgth, i;
  SXINT     delta_vstr_mot_en_min, longueur_de_mot, prev_global_cost;
  char    car, cur_car, prev_car;
  char    *cur_str, *cur_str_mot_en_min, *top_str;
  SXBOOLEAN has_changed, has_initial_maj;
  char    *mot_courant;

  MOT_A_CORRIGER = mot_courant = mot;
  longueur_de_mot = lgth;

  if (*mot_courant == '_' && (*(mot_courant + 1) >= 'A' && *(mot_courant + 1) <= 'Z'))
    id = 1;
  else {
    id = sxdfa_comb_get (dico_des_mots, mot_courant, lgth);
  }

  if (id)
    push_string (prefix_stack_ptr, mot_courant, mot_courant+lgth);

  if (id == 0) {
    if (is_first_word && is_process_lower)
      car = ttdtolower (*mot_courant); // changer
    else
      car = *mot_courant;

    /* (car != *mot_courant) <==> Le 1er mot commence par une majuscule */
  }

  if (is_light_correction && id == 0) {
  /* On tente d'abord une (super)light (super ou non selon majuscule ou pas) sur le mot entier pour voir si on doit essayer de d�tacher des trucs */
#ifndef german
    if (has_upper (mot_courant, lgth)) {
      /* Le mot contient des majuscules */
      /* => on regarde une correction super light */
      id = call_super_light_sxspell (dico_des_mots, &super_light_FSA, mot_courant, lgth, NULL /* store_if_has_capital_letter */);
      if (id == 0 && car != *mot_courant) {
	/* si �chec, on essaye en minusculant l'initiale... */
	prev_car = *mot_courant;
	*mot_courant = car;
	id = call_super_light_sxspell (dico_des_mots, &super_light_FSA, mot_courant, lgth, NULL /* store_if_has_capital_letter */);
	*mot_courant = prev_car;
      }
    }
    else
#endif /* !german */
      /* Le mot ne contient que des minuscules ou on est en allemand */
      /* => correction light */
      id = call_light_sxspell (dico_des_mots, &light_FSA, mot_courant, lgth, NULL /* store_in_prefix_stack */, SXTRUE);
    
    if (id) /* le mot est (super)light-corrigible: pas besoin de d�tacher des trucs ; on stocke donc tout le mot d'origine � destination de la zone ex-text2dag */
      push_string (prefix_stack_ptr, mot_courant, mot_courant+lgth);    
  }

  if (id == 0 && (is_cut_on_quote || is_cut_on_hyphen)) {
    top_str = mot_courant + lgth;
    cur_str = mot_courant;

    if (car != *mot_courant /* le premier caract�re du mot �tait une majuscule ET on est sur le premier mot de la phrase et is_process_lower */) {
      prev_car = *mot_courant;
      *mot_courant = car;
      has_initial_maj = SXTRUE;
    }
    else
      has_initial_maj = SXFALSE;

    has_changed = call_cut_on_quote_hyphen (&cur_str, &top_str, SXFALSE /* has_hyphen_in_prefix */, SXTRUE /* is_first_call */);

    if (cur_str > mot_courant)
      /* Il y a un mot prefixe de'tache' */
      is_first_word = SXFALSE;

    if (has_initial_maj) {
      /* Prudence, on remet en etat */
      *mot_courant = prev_car;

      if (has_changed || cur_str > mot_courant) {
	char *first_char_ptr;

	/* On majusculise la 1ere lettre du 1er prefixe */
	first_char_ptr = prefix_stack_ptr->vstrs [1]->first;
	*first_char_ptr = sxtoupper (*first_char_ptr);
      }
    }

    if (has_changed)
      id = 1;
    else {
      /* On continue sur le noyau */
      mot_courant = cur_str;
      lgth = top_str-cur_str;
    }
  }

  /* On regarde si le mot a des prefixes et des suffixes remarquables */
  if (id == 0 /*&& (process_prefix || process_suffix)*/) {
    SXINT prefix_top, suffix_top;

    top_str = mot_courant + lgth;
    cur_str = mot_courant;
    prev_global_cost = global_cost;
    prefix_top = prefix_stack_ptr->top;
    suffix_top = prefix_stack_ptr->top;
      
    has_initial_maj = SXFALSE;

    if (is_first_word && is_process_lower) {
      car = ttdtolower (*mot_courant); //  changer

      if (car != *mot_courant) {
	prev_car = *mot_courant;
	*mot_courant = car;
	has_initial_maj = SXTRUE;
      }
    }

    has_changed = call_process_prefix (&cur_str, &top_str, has_initial_maj);

    if (has_initial_maj) {
      /* Prudence, on remet en etat */
      *mot_courant = prev_car;

      if (has_changed) {
	char *first_char_ptr;

	if (cur_str > mot_courant) {
	  /* On majusculise la 1ere lettre du 1er prefixe */
	  first_char_ptr = prefix_stack_ptr->vstrs [1]->first;
	  *first_char_ptr = sxtoupper (*first_char_ptr);
	}
	else {
	  /* On majusculise la 1ere lettre du 1er suffixe */
	  first_char_ptr = suffix_stack_ptr->vstrs [suffix_stack_ptr->top]->first;
	  *first_char_ptr = sxtoupper (*first_char_ptr);
	}
      }
      /* Else echec => On ignore les prefixes suffixes detaches et on va repartir sur mot */
    }

    if (has_changed)
      id = 1;
    else {
      /* On ignore les prefixes suffixes detaches et on repart sur mot, en conservant les extractions de cut_on_quote */
      global_cost = prev_global_cost;
      prefix_stack_ptr->top = prefix_top;
      prefix_stack_ptr->top = suffix_top;
    }
  }
  
  if (id == 0) {
    /* On abandonne, c'est ex-text2dag qui corrigera */
    //    id = 1;
    push_string (prefix_stack_ptr, mot_courant, mot_courant+lgth);
    global_cost += 1; /* A voir */
    if (is_glue) {
      /*  c'est text2dag qui corrigera plus tard, mais dans le cas is_glue on doit estimer le co�t
	  de cette (�ventuelle) correction */
      /* ... en 2 temps */
      /* On essaie d'abord une correction light */
      global_cost = 0;
      is_a_spelling_correction.yes = SXFALSE; /* car la correction n'a pas �t� faite, on a juste
						 r�cup�r� son poids (la partie ex-text2dag la fera
						 �ventuellement plus tard) */
      is_a_spelling_correction.word = mot_courant;
      is_a_spelling_correction.lgth = lgth;
      
      if (is_spelling_correction) {
#ifndef german
	if (has_upper (mot_courant, lgth))
	  /* dans ce cas on n'a tent� que superlight et d�tachements ; on ne tente que light, car il y a des majuscules */
	  id = call_light_sxspell (dico_des_mots, &std_FSA, mot_courant, lgth, store_correction_cost, SXTRUE /* is_deterministic */);
	else
#endif /* !german */
	  /* dans ce cas l� on sait d�j�, pour l'avoir tent�, que light �choue */
	  id = call_sxspell (dico_des_mots, &std_FSA, mot_courant, lgth, store_correction_cost, SXTRUE /* is_deterministic */);
      }
    }
  }

  cur_correction->id = id;
  cur_correction->cost = global_cost;
  cur_correction->is_a_spelling_correction = is_a_spelling_correction;
  
  return id;
}


/*  On retourne "222</F>333" */ 
static char *
xml_body (str)
     char * str;
{
  char *cur;

  cur = str;

  if (cur) cur = strchr (cur, (int)'<');
  if (cur) cur = strchr (cur, (int)'>');
  if (cur) cur++;

  return cur;
}


/* Cas special { <...>disp<...> <...>aru<...> } _ETR 
   mot         ^
   mot+lgth                                    ^
   On essaie de corriger "disp aru"
*/
static SXINT
call_spell_ETR (mot, lgth, is_first_word, local_is_spelling_correction)
     char    *mot;
     SXINT     lgth;
     SXBOOLEAN is_first_word, local_is_spelling_correction;
{
  SXINT  id, ste, length;
  char *pstr, *qstr, cur_char;
  
  /* On extrait "disp" "aru" ... */
  pstr = mot;
  id = 0;

  if ((pstr = xml_body (mot))) {
    /* { <...>disp<...> <...>aru<...> } _ETR 
       pstr   ^
    */

    varstr_raz (vstr_ETR);

    if ((qstr = strchr (pstr, (int)'<'))) {
      /* { <...>disp<...> <...>aru<...> } _ETR 
	 qstr       ^
      */
      varstr_lcatenate (vstr_ETR, pstr, qstr-pstr);

      while ((pstr = xml_body (qstr+1))) {
	/* { <...>disp<...> <...>aru<...> } _ETR 
	   pstr                  ^
	*/
	if ((qstr = strchr (pstr, (int)'<'))) {
	  /* { <...>disp<...> <...>aru<...> } _ETR 
	     qstr                     ^
	  */
	  /* Pour l'instant, je ne met pas de blanc entre "disp" et "aru" !! */
	  varstr_lcatenate (vstr_ETR, pstr, qstr-pstr);
	}
	else {
	  /* !! */
	  varstr_raz (vstr_ETR);
	  break;
	}
      }
    }

    if ((length = varstr_length (vstr_ETR))) {
      if (length < 17 /*  BS: pour ne pas perdre de temps � tenter de corriger ce qui est manifestement un _ETR */
	  && !has_upper (varstr_tostr (vstr_ETR), length)) { /* {<>The</> <>Star</>} _ETR */
	/* ste = sxstrsave (varstr_tostr (vstr_ETR)); */
	/*to_lower (varstr_tostr (vstr_ETR), length);  inutile avec le has_upper */
	id = call_sxspeller (varstr_tostr (vstr_ETR), length, is_first_word, local_is_spelling_correction);
      }
    }
  }
  else {
    /* On suppose qu'on est ds le cas {disp aru} */
    if (lgth-2 > 0) {
      if (lgth-2 < 17) { /*  BS: pour ne pas perdre de temps � tenter de corriger ce qui est manifestement un _ETR */
	vstr_ETR = varstr_lcatenate (varstr_raz (vstr_ETR), mot+1, lgth-2);
	if (!has_upper (varstr_tostr (vstr_ETR), lgth-2))/* {<>The</> <>Star</>} _ETR */
	  /*	to_lower (varstr_tostr (vstr_ETR), lgth-2);*/
	  id = call_sxspeller (varstr_tostr (vstr_ETR), lgth-2, is_first_word, local_is_spelling_correction);
      }
    }
  }

  if (id == 0)
    is_a_spelling_correction.yes = SXFALSE; /* On reinitialise */

  cur_correction->id = id;
  cur_correction->cost = light_cost + heavy_cost; /* l'un des 2 au moins est nul */
  cur_correction->is_a_spelling_correction = is_a_spelling_correction;
  
 return id;
}


static SXINT
push_sxspeller_results (correction)
     struct correction_struct *correction;
{
  SXINT i;
  char cur_car;

  for (i = 1; i <= correction->prefix_stack.top; i++) {
    DPUSH (comments_stack, sxstrsave (varstr_tostr (correction->vstr)));
    DPUSH (if_stack, sxstrsave (varstr_tostr (correction->prefix_stack.vstrs [i])));
  }
    
  if (correction->is_a_spelling_correction.yes) {
    DPUSH (comments_stack, sxstrsave (varstr_tostr (correction->vstr)));

    if (correction->id == 0) {
      cur_car = correction->is_a_spelling_correction.word [correction->is_a_spelling_correction.lgth];
      correction->is_a_spelling_correction.word [correction->is_a_spelling_correction.lgth] = SXNUL;

      DPUSH (if_stack, sxstrsave (correction->is_a_spelling_correction.word));      

      correction->is_a_spelling_correction.word [correction->is_a_spelling_correction.lgth] = cur_car;
    }
    else {
      DPUSH (if_stack, sxstrsave (varstr_tostr (correction->cvstr)));
    }
  }

  for (i = correction->suffix_stack.top; i > 0; i--) {
    DPUSH (comments_stack, sxstrsave (varstr_tostr (correction->vstr)));
    DPUSH (if_stack, sxstrsave (varstr_tostr (correction->suffix_stack.vstrs [i])));
  }
}

static void
swap_corrections (void)
{
  if (prev_correction == corrections) {
    prev_correction = corrections + 1;
    cur_correction = corrections;
  } else {
    prev_correction = corrections;
    cur_correction = corrections + 1;
  }
}

static char*
get_corrections_comment (corr_num)
     SXINT corr_num;
{
  if (corr_num == 1)
    return varstr_tostr (prev_correction->vstr);
  else if (corr_num == 2)
    return varstr_tostr (cur_correction->vstr);
  else
    sxtrap (ME, "get_corrections_comment can be called only with argument 1 or 2");
  return NULL;
}


static void
sxspeller (word, word_lgth, pcom, is_first_word, is_last_word, local_is_spelling_correction)
     char    *word, *pcom;
     SXINT     word_lgth;
     SXBOOLEAN is_first_word, is_last_word, local_is_spelling_correction;
{
  SXINT id;
  char *tcom;
  static SXBOOLEAN must_push_prev_correction;

  if (is_first_word)
    must_push_prev_correction = SXFALSE;

  cc_vstr = cur_correction->vstr;
  cvstr = cur_correction->cvstr;
  cur_correction->word = word;	  

  varstr_raz (cc_vstr);

  if (pcom) {
    if ((pcom = strchr (pcom, (int)'{'))) {
      tcom = pcom;

      if ((tcom = strrchr (tcom, (int)'}'))) {
	/* from_right */
	varstr_lcatenate (cc_vstr, pcom, tcom-pcom+1);
      }
      else
	pcom = NULL;
    }
    else {
      pcom = NULL;
    }
  }

  RAZ_VSTR_STACK (cur_correction->prefix_stack);
  RAZ_VSTR_STACK (cur_correction->suffix_stack);
  is_a_spelling_correction.yes = SXFALSE;

  cur_correction->is_first_word = is_first_word;
  prefix_stack_ptr = &(cur_correction->prefix_stack);
  suffix_stack_ptr = &(cur_correction->suffix_stack);
  cur_correction->cost = 0;

  id = 0;
  light_cost = heavy_cost = global_cost = 0;

/* ne marche plus dans la nouvelle architecture, A FAIRE */
#if 0
  if (pcom && word_lgth >= 4 && strncmp (word, "_ETR", 4) == 0) {
    /* On est ds le cas special :
       { <...>disp<...> <...>aru<...> } _ETR 
       ou` on va essayer de corriger non pas _ETR mais "disp aru"
       Si ca marche on retourne { <...>disp<...> <...>aru<...> } disparu
       sinon { <...>disp<...> <...>aru<...> } _ETR
       ds le cas dry_run, on retourne
       { <...>disp<...> <...>aru<...> } _ETR ou
       { <...>disp<...> <...>aru<...> } <<<_ETR>>>
    */
    id = call_spell_ETR (pcom, tcom-pcom+1, is_first_word, local_is_spelling_correction);
  }
#endif /* 0 */
      
  if (id == 0) {
    id = call_sxspeller (word, word_lgth, is_first_word, local_is_spelling_correction);
  }

  if (is_glue) {
    if (id && *word == '_' && (*(word + 1) >= 'A' && *(word + 1) <= 'Z')) {
      /* Cas des meta ff (qui marchent forcement) */
      if (must_push_prev_correction) {
	push_sxspeller_results (prev_correction);
	must_push_prev_correction = SXFALSE;
      }
      push_sxspeller_results (cur_correction);
    } else if (!must_push_prev_correction) {
      must_push_prev_correction = SXTRUE;
      swap_corrections ();
    } else {
      SXINT prev_cur_cost;

      prev_cur_cost = (prev_correction->cost + cur_correction->cost);
	      
      if (prev_correction->id != 0 && cur_correction->id != 0 && prev_cur_cost == 0) {
	/* precedent et courant OK */
	push_sxspeller_results (prev_correction);
	/* must_push_prev_correction vaut d�j� SXTRUE */
	swap_corrections ();
      }
      else {
	/* Au moins l'un des 2 a necessit� de la correction, on regarde ce que donne la concatenation des 2 */
	bivstr = varstr_catenate (varstr_raz (bivstr), prev_correction->word);
	bivstr = varstr_catchar (bivstr, ' ');
	bivstr = varstr_catenate (bivstr, cur_correction->word);

	bi_correction->word = word = varstr_tostr (bivstr);
	word_lgth = varstr_length (bivstr);
	  
	cc_vstr = bi_correction->vstr;
	cvstr = bi_correction->cvstr;

	RAZ_VSTR_STACK (bi_correction->prefix_stack);
	RAZ_VSTR_STACK (bi_correction->suffix_stack);
	is_a_spelling_correction.yes = SXFALSE;

	bi_correction->is_first_word = SXFALSE;
	prefix_stack_ptr = &(bi_correction->prefix_stack);
	suffix_stack_ptr = &(bi_correction->suffix_stack);

	id = 0;
	light_cost = heavy_cost = 0;

	saved_cur_correction = cur_correction; /* car call_sxspeller travaille dans cur_correction */
	cur_correction = bi_correction;
	id = call_sxspeller (word, word_lgth, prev_correction->is_first_word, SXTRUE /* local_is_spelling_correction */);
	cur_correction = saved_cur_correction;
      
	if (id == 0 || prev_cur_cost <= bi_correction->cost) {
	  push_sxspeller_results (prev_correction);
	  /* must_push_prev_correction vaut d�j� SXTRUE */
	  swap_corrections ();
	}
	else {
	  cc_vstr = varstr_catenate (varstr_raz (cc_vstr), make_comment (1, 3, get_corrections_comment));
      
	  push_sxspeller_results (bi_correction);
	  must_push_prev_correction = SXFALSE;
#if 0
	  /* On tente des concatenations multiples !! */
	  *prev_correction = *bi_correction; // dans ce cas-l� on recopie
#endif /* 0 */
	}
      }
    }
    if (is_last_word && must_push_prev_correction) {
      push_sxspeller_results (prev_correction);
      must_push_prev_correction = SXFALSE; /* prudence */
    }
  }
  else {
    push_sxspeller_results (cur_correction);
  }
}

static void
string2set (string, set)
     unsigned char *string;
     SXBA          set;
{
  unsigned char uchar;

  while ((uchar = *string++))
    SXBA_1_bit (set, uchar);
}

static void sxspeller_input_strings_oflw (SXINT old_size, SXINT new_size)
{
#if MEMOIZE_MORE
  input_ste2memo_status = (SXUINT*) sxrecalloc (input_ste2memo_status, 2 * old_size + 1, 2 * new_size + 1, sizeof (SXUINT));
#else /* MEMOIZE_MORE */
  input_ste2memo_status = (SXBA*) sxbm_resize (input_ste2memo_status, 12, 12, new_size + 1);
#endif /* MEMOIZE_MORE */
}

static void
text2dag_init (void)
{
  SXINT which_correction;
  static SXINT acc_dag_foreach [6] = {1, 0, 0, 0, 0, 0};
  static SXINT if_dag_assoc_list [6] = {0, 0, 0, 0, 1, 0}; /* X, Y, Z, XY, XZ, YZ */;

  Uw_ste = sxstrsave (Uw);

  string2set (MAJ_string, MAJ_set);
  string2set (vowell_string, vowell_set);
  string2set (mpb_string, mpb_set);
  
  for (which_correction = 0; which_correction <= 2; which_correction++) {
    cur_correction = corrections+which_correction;
    cur_correction->vstr = varstr_alloc (128);
    cur_correction->cvstr = varstr_alloc (128);
    alloc_vstr_stack_hd (&cur_correction->prefix_stack, 32);
    alloc_vstr_stack_hd (&cur_correction->suffix_stack, 32);
  }
  
  bivstr = varstr_alloc (128);
  wstr = varstr_alloc (128);
  vstr_ETR = varstr_alloc (128);
  vstr_mot_en_min = varstr_alloc (128);
  ffwc_vstr = varstr_alloc (128);
  hyphen_set = sxba_calloc (128);

  DALLOC_STACK (if_stack, 16);
  DALLOC_STACK (comments_stack, 16);

  prev_correction = corrections;
  cur_correction = corrections + 1;
  bi_correction = corrections + 2;
  
#ifdef HAS_CMPD_OR_AMLGM
  /* il y a eu des specifs d'amalgames ou de mots composes */
  XxYxZ_alloc (&acc_dag, "acc_dag", 2*128+1, 1, acc_dag_foreach, acc_dag_oflw, NULL);
  acc_dag2comment_ste = (SXINT*) sxalloc (XxYxZ_size (acc_dag) + 1, sizeof (SXINT));
    
  XxY_alloc (&acc_trans_hd, "acc_trans_hd", 2*128+1, 1, 0, 0, NULL, NULL);
    
  DALLOC_STACK (lhs_id_stack, 2*128+1);
  DALLOC_STACK (lhs_p_stack, 2*128+1);
  DALLOC_STACK (lhs_comment_ste_stack, 2*128+1);
    
  XxY_alloc (&XxY_duplicate_hd, "XxY_duplicate_hd", 2*128+1, 1, 0, 0, NULL, NULL);
  XxYxZ_alloc (&if_dag, "if_dag", 2*128+1, 1, if_dag_assoc_list, if_dag_oflw, NULL);
  if_comment = (char**) sxalloc (XxYxZ_size (if_dag)+1, sizeof (char*));
  if (is_output_weights)
    if_weight = (SXINT*) sxcalloc (XxYxZ_size (if_dag)+1, sizeof (SXINT));
  vstr = varstr_alloc (128);
  wvstr = varstr_alloc (128);
#endif /* HAS_CMPD_OR_AMLGM */

  SPELLED_VSTR = varstr_alloc (32);

  dico_des_ac->name = "dico_des_ac";
  dico_des_ff->name = "dico_des_ff";
  dico_des_mots->name = "dico_des_mots";
  dico_des_ac->id = 0;
  dico_des_ff->id = 4;
  dico_des_mots->id = 8;

#if MEMOIZE_MORE
#if MEMO_THRSLD > 0
  sxword_alloc (&sxspeller_input_strings, "sxspeller_input_strings", 1024, 1, 12, 
		sxcont_malloc, sxcont_alloc, sxcont_realloc, NULL /* resize */, sxcont_free, sxspeller_input_strings_oflw, NULL);
  input_ste2memo_status = (SXUINT*) sxcalloc (1 + 2 * SXWORD_size (sxspeller_input_strings), sizeof (SXUINT));
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
  sxword_alloc (&sxspeller_input_strings, "sxspeller_input_strings", 1024, 1, 12, 
		sxcont_malloc, sxcont_alloc, sxcont_realloc, NULL /* resize */, sxcont_free, sxspeller_input_strings_oflw, NULL);
  input_ste2memo_status = sxbm_calloc (12, SXWORD_size (sxspeller_input_strings) + 1);
#endif /* MEMOIZE_MORE */
}

static void
text2dag_raz (void)
{
  SXINT which_correction;

  for (which_correction = 0; which_correction <= 2; which_correction++) {
    cur_correction = corrections+which_correction;
    varstr_raz (cur_correction->vstr);
    varstr_raz (cur_correction->cvstr);
    cur_correction->prefix_stack.top = 0;
    cur_correction->suffix_stack.top = 0;
  }
  
  varstr_raz (bivstr);
  varstr_raz (wstr);
  varstr_raz (vstr_ETR);
  varstr_raz (vstr_mot_en_min);
  varstr_raz (ffwc_vstr);
  sxba_empty (hyphen_set);

  DRAZ (if_stack);
  DRAZ (comments_stack);

  prev_correction = corrections;
  cur_correction = corrections + 1;
  bi_correction = corrections + 2;
  
#ifdef HAS_CMPD_OR_AMLGM
  /* il y a eu des specifs d'amalgames ou de mots composes */
  XxYxZ_clear (&acc_dag);
    
  XxY_clear (&acc_trans_hd);
    
  DRAZ (lhs_id_stack);
  DRAZ (lhs_p_stack);
  DRAZ (lhs_comment_ste_stack);
    
  XxY_clear (&XxY_duplicate_hd);
  XxYxZ_clear (&if_dag);
  varstr_raz (vstr);
  varstr_raz (wvstr);
#endif /* HAS_CMPD_OR_AMLGM */

  varstr_raz (SPELLED_VSTR);

}

static void
text2dag_finalize (void)
{
  SXINT which_correction;

  for (which_correction = 0; which_correction <= 2; which_correction++) {
    cur_correction = corrections + which_correction;
    if (cur_correction->vstr)
      varstr_free (cur_correction->vstr), cur_correction->vstr = NULL;
    if (cur_correction->cvstr)
      varstr_free (cur_correction->cvstr), cur_correction->cvstr = NULL;
    free_vstr_stack_hd (&cur_correction->prefix_stack/*, 32*/);
    free_vstr_stack_hd (&cur_correction->suffix_stack/*, 32*/);
  }
  
  varstr_free (bivstr), bivstr = NULL;
  varstr_free (ffwc_vstr), ffwc_vstr = NULL;
  varstr_free (wstr), wstr = NULL;
  if (mvstr)
    varstr_free (mvstr), mvstr = NULL;
  varstr_free (vstr_ETR), vstr_ETR = NULL;
  varstr_free (vstr_mot_en_min), vstr_mot_en_min = NULL;
  
  sxfree (hyphen_set), hyphen_set = NULL;

  DFREE_STACK (if_stack);
  DFREE_STACK (comments_stack);

#ifdef HAS_CMPD_OR_AMLGM
  XxYxZ_free (&acc_dag);
  sxfree (acc_dag2comment_ste), acc_dag2comment_ste = NULL;
  XxY_free (&acc_trans_hd);
  DFREE_STACK (lhs_id_stack);
  DFREE_STACK (lhs_p_stack);
  DFREE_STACK (lhs_comment_ste_stack);
  XxY_free (&XxY_duplicate_hd);
  sxfree (if_comment), if_comment = NULL;
  if (is_output_weights)
    sxfree (if_weight), if_weight = NULL;
  varstr_free (wvstr), wvstr = NULL;
  varstr_free (vstr), vstr = NULL;
#endif /* HAS_CMPD_OR_AMLGM */

  varstr_free (SPELLED_VSTR), SPELLED_VSTR = NULL;

#if MEMOIZE_MORE
  fprintf (sxstderr, "SXWORD_top (sxspeller_input_strings) = %ld\n", (long) SXWORD_top (sxspeller_input_strings));

#if MEMO_THRSLD > 0
  sxword_free (&sxspeller_input_strings);
  sxfree (input_ste2memo_status);
#endif /* MEMO_THRSLD > 0 */
#else /* MEMOIZE_MORE */
  sxword_free (&sxspeller_input_strings);
  sxbm_free (input_ste2memo_status), input_ste2memo_status = NULL;
#endif /* MEMOIZE_MORE */
}

/* laid il faudrait passer par un tdef */
#define eol_code (SXINT)1
#define tmax (SXINT)3

SXINT
text2dag_semact (code, numact)
    SXINT		code;
    SXINT		numact;
{
  SXINT        tok_no, lgth, id, id2, Id, ste = 0, triple, x, lahead;
  char         *str, *comment, first_up, first_low, *final_comment;
  SXBOOLEAN    is_Ci, prev_ste_is_sent_bound_ste, prev_ste_is_dblquote_ste, prev_ste_is_guillemo_ste;
  struct sxtoken *ptoken;
  SXINT        sent_bound_ste = sxstr2save ("_SENT_BOUND", 11);
  SXINT        dblquote_ste = sxstrsave ("\"");
  SXINT        guillemo_ste = sxstrsave ("�");

  switch (code) {
  case SXCLOSE:
    fflush (stdout);
  case SXOPEN:
  case SXSEMPASS:
    return 0;

  case SXINIT:
#ifdef HAS_CMPD_OR_AMLGM
    /* On met les meta-char des E.R. */
    sxstrsave ("(");
    sxstrsave (")");
    sxstrsave ("[");
    sxstrsave ("]");
    meta_ste = sxstrsave ("|");
#endif /* HAS_CMPD_OR_AMLGM */

    text2dag_init();

    do {
      fflush (NULL);

      tok_no = 0;
    
      /* Les appels du scanner se font manuellement */
      sxerr_mngr (SXINIT);

      do {
	sxscan_it ();
	
	if (tok_no++ == 0)
	  init_tok_no = sxplocals.Mtok_no;
	
	ptoken = &SXGET_TOKEN (sxplocals.Mtok_no);
	lahead = ptoken->lahead;
      } while (lahead != tmax && lahead != eol_code);

      /* n = sxplocals.Mtok_no - 1;*/

      if (sxplocals.Mtok_no > 1) { /* on n'a pas trouv� une ligne vide, i.e. dont le eol est en position 1 */
	sxput_token (*ptoken); /* on prend un nouveau token */

	if (lahead == tmax) {
	  /* On a trouve' la fin de fichier sans eol_code devant, on le simule */
	  ptoken->lahead = eol_code; /* on change le precedent */
	}
	else {
	  /* lahead == eol_code */
	  /* On force EOF derriere eol */
	  SXGET_TOKEN (sxplocals.Mtok_no).lahead = tmax /* c'est-�-dire EOF */;
	}
	

	eol_tok_no = sxplocals.Mtok_no - 1;
#ifdef HAS_CMPD_OR_AMLGM
	max_acc_dag_state = 1; /* origine du dag des compound/amalgam locaux */
	has_new_cc = SXFALSE;
#endif /* HAS_CMPD_OR_AMLGM */
	real_tok_no = 0; /* il y a d�calage entre le tok_no (d'entr�e) et le real_tok_no (de sortie)
			    si sxspeller retarde la mise dans if_stack pour cause de glue */
	prev_ste_is_sent_bound_ste = SXFALSE;
	prev_ste_is_dblquote_ste = SXFALSE;
	prev_ste_is_guillemo_ste = SXFALSE;

	for (tok_no = init_tok_no; tok_no < eol_tok_no; tok_no++) {
	  ste = SXGET_TOKEN (tok_no).string_table_entry;

	  if (ste > SXEMPTY_STE) {
	    str = sxstrget (ste);
	    lgth = sxstrlen (ste);
	    comment = SXGET_TOKEN (tok_no).comment;

	    /* DEBUT DE FEU SXSPELLER */
	    /* on l'appelle ici en mode local_is_spelling_correction = SXFALSE: les full corrections (ni light ni super_light) 
	       sont maintenant non-d�terministes et faites plus tard, au sein de feu text2dag */
	    DRAZ (if_stack);
	    DRAZ (comments_stack);
	    sxspeller (str, lgth, comment,
		       (tok_no == init_tok_no),// || prev_ste_is_sent_bound_ste /* approximatif dans le cas DAG ; �a casse tout!!! pkoi? je ne sais pas, mais je l'enl�ve d'ici*/, 
		       tok_no + 1 == eol_tok_no, SXFALSE /* local_is_spelling_correction */);	
	    /* FIN DE FEU SXSPELLER */

	    for (x = 1; x <= DTOP (if_stack); x++)  {
	      is_first_cc = SXFALSE;
	      if (x == 1) {
		if ((++real_tok_no == init_tok_no) || prev_ste_is_sent_bound_ste || prev_ste_is_dblquote_ste || prev_ste_is_guillemo_ste) {
		  is_first_cc = SXTRUE;
		}
	      }

	      str = sxstrget (if_stack [x]);
	      lgth = strlen (str);
	      CUR_STR = str;
	      CUR_COMMENT = comment = sxstrget (comments_stack [x]);

	      if (*str == '_' && (*(str + 1) >= 'A' && *(str + 1) <= 'Z') /* on ne corrige pas les identifiants d'entit�s nomm�es... */) {
#ifdef HAS_CMPD_OR_AMLGM
		if (max_acc_dag_state > 1) {
		  /* file d'attente non vide et etat d'evaluation */
		  to_tok_no = real_tok_no;
		  close_local_dags (max_acc_dag_state);
		  max_acc_dag_state = 1;
		}
#endif /* HAS_CMPD_OR_AMLGM */
		is_first_call_to_output_if_id = SXTRUE; /* Pour output_if_id () */
		output_if_str (str, 0);
		output_if_str (NULL, 0);
	      }
	      else {
#ifdef german
		HAS_AN_UPPER_CASE_LETTER = SXFALSE;
#else
		HAS_AN_UPPER_CASE_LETTER = has_upper (str, lgth);
#endif /* german */


#ifdef HAS_CMPD_OR_AMLGM
		if (max_acc_dag_state <= 24) { /* BS: s�curit� ; sinon on peut partir en vrille si on
						  a une s�quence super longue de mots qui sont TOUS
						  des composants de compos�s et/ou des amalgames (un
						  exemple, "on nous bassine avec des paroles, des
						  paroles, des paroles, [r�p�t� 50 fois]", trouv� pour
						  de vrai dans la vraie vie...) */
		  
		  if (!HAS_AN_UPPER_CASE_LETTER) { /* Si le mot n'a aucune majuscule */
		    /* On regarde si le mot courant est un amalgame ou un (composant d'un) compound */
		    Id = id = sxdfa_comb_get (dico_des_ac, str, lgth);

		    if (id) { /* si oui on le met dans le bon dag */
		      store_amlgm_or_cmpnd_id (id, str, lgth);
		      id = 1;
		    } 

		    else if (is_light_correction) {
		      if (is_spelling_correction) { /* si non, on essaye de voir si une light correction peut en faire un composant de compound... */
			/* ... mais on ne le fait pas si le mot est une ponctuation */
			if (lgth > 2 || !ispunct ((int)(unsigned char)*str)) {
			  if ((id = call_light_sxspell (dico_des_ac, &cmpnd_FSA, str, lgth, amlgm_or_cmpnd_id, SXFALSE)))
			    id = 2; /* indique qu'il a fallu de la correction pour trouver qqch */
			}
		      }
		    }
		  }
		  else if (is_light_correction) { /* le mot a au moins une majuscule */
		    /* Suivant la valeur de l'option de compilation IS_LIGHT_CORRECTION on regarde si une correction
		       du mot courant est un amalgame ou un (composant d'un) compound */
		    id = 0;

		    /* Si le mot comporte une majuscule, on tente une vraie light correction */
		    /* ... et que le source soit :
		       PESE, Pes�, Peus� ou PEUSE */
		    /* Correction non deterministe des lettres majuscules */
		    //		  id = call_maj2diacritic_sxspell (dico_des_ac, &cmpnd_FSA, str, lgth, amlgm_or_cmpnd_id);
		    /* On obtient : PESE = {pes�, p�se}, Pes� = {pes�}, Peus� = {} ou PEUSE = {} */
		  
		    if (id == 0) {
		      /* ... qui a echouee */
		      /* cas de Peus� = {} ou PEUSE = {} */
		      /* On essaie alors une correction light (non-detetrministe) */
		      id = call_light_sxspell (dico_des_ac, &cmpnd_FSA, str, lgth, amlgm_or_cmpnd_id, SXFALSE);
		    }
		  } else {
		    /* pas de tentative de correction */
		    if ((id = sxdfa_comb_get (dico_des_ac, str, lgth)))
		      store_amlgm_or_cmpnd_id (id, str, lgth);
		  }

#else /* HAS_CMPD_OR_AMLGM */
		  /* Il se peut qu'on utilise text2dag_semact alors que le dictionnaire des mots composes
		     est vide.  Ds ce cas la sortie est egale a l'entree ... */
		  id = 0;
#endif /* HAS_CMPD_OR_AMLGM */
		}
		else
		  id = 0;

		if (id == 0) {
		  /* ce n'est ni un amalgam ni un composant d'un mot compose' ... */
		  /* On suppose que c'est une forme flechie : On termine les ER en cours eventuelles, on sort la ff et on reinitialise une nelle lecture */
#ifdef HAS_CMPD_OR_AMLGM
		  if (max_acc_dag_state > 1) {
		    /* file d'attente non vide et etat d'evaluation */
		    to_tok_no = real_tok_no;
		    close_local_dags (max_acc_dag_state);
		    max_acc_dag_state = 1;
		  }
#endif /* HAS_CMPD_OR_AMLGM */

		  is_first_call_to_output_if_id = SXTRUE; /* Pour output_if_id () */

		  if (!HAS_AN_UPPER_CASE_LETTER) { /* Si le mot n'a aucune majuscule */
		    id = sxdfa_comb_get (dico_des_ff, str, lgth); /* on regarde si le mot courant est une forme flechie */
#if defined(dico_ff_checkonly_h) || defined(has_co_dico)
		    if (id == 0)
		      id = sxdfa_comb_get (dico_des_ff_checkonly, str, lgth);
#endif /* defined(dico_ff_checkonly_h) || defined(has_co_dico) */

		    if (id) {
		      output_if_str (str, -1);
		      output_if_str (NULL, 0);
		      /* dernier appel */
		    } else {
		      /* sinon, on tente une full correction non d�terministe (NEW!!!) */
		      if (is_keep_original_tokens) {
			output_if_str (str, -1);
		      }
		      if (is_spelling_correction)
			id = call_sxspell (dico_des_ff, &if_FSA, str, lgth, output_if_id, SXFALSE /* is_deterministic */);
		      if (id == 0) {
			if (is_output_uw) 
			  output_if_str (uw, 0); /* non: on sort _uw */
			else if (!is_keep_original_tokens) /* sinon �a a d�j� �t� output_if_stris� */
			  output_if_str (str, -1);
			output_if_str (NULL, 0);
		      }
		    }

		  }
		  else if (is_light_correction) { /* Il y a des majuscules ds le mot */
		    /* Voir les explications ci-dessus � propos des recherches dans dico_cmpnd, tout est pareil sauf que l'on cherche maintenant dans dico_des_ff,
		       avec if_FSA, que l'on �crit directement les trucs trouv�s (output_if_id) et que l'on utilise if_SPELLED_VSTR comme truc temporaire */
		    if (id == 0 && is_keep_original_tokens) {
		      output_if_str (str, -1);
		    }
		    if (id == 0)
		      id = call_light_sxspell (dico_des_ff, &if_FSA, str, lgth, output_if_id, SXFALSE);

		    if (id == 0) {
		      if (is_output_uw) {
			if (real_tok_no == init_tok_no)
			  /* On va sortir (_uw | _Uw) */
			  output_if_str (uw, 0);
		  
			output_if_str (Uw, 0);
		      }
		      else if (!is_keep_original_tokens) /* sinon �a a d�j� �t� output_if_stris� */
			output_if_str (str, -1);
		      output_if_str (NULL, 0);
		    }
		    /* else tout a ete sorti */
		  }
		}
#ifdef HAS_CMPD_OR_AMLGM
		else {
		  /* Le mot courant est un amalgame ou un composant d'un mot compose' */
		  /* Est-ce aussi une ff (ou une correction d'une ff) ? */
		  if (max_acc_dag_state == 1)
		    from_tok_no = real_tok_no;

		  if (!HAS_AN_UPPER_CASE_LETTER) {
		    /* Tout en minuscule, ou allemand */
		    Id = id;
		    id = sxdfa_comb_get (dico_des_ff, str, lgth);
#if defined(dico_ff_checkonly_h) || defined(has_co_dico)
		    if (id == 0)
		      id = sxdfa_comb_get (dico_des_ff_checkonly, str, lgth);
#endif /* defined(dico_ff_checkonly_h) || defined(has_co_dico) */
		    if (id == 0 && is_keep_original_tokens) {
		      store_if_str (str, -1);
		    }

		    id2 = 0;
#ifdef german
		    if (is_first_cc && *str == sxtoupper(*str)) {
		      *str = ttdtolower(*str); //changer
		      id2 = sxdfa_comb_get (dico_des_ff, str, lgth);
#if defined(dico_ff_checkonly_h) || defined(has_co_dico)
		      if (id2 == 0)
			id2 = sxdfa_comb_get (dico_des_ff_checkonly, str, lgth);
#endif /* defined(dico_ff_checkonly_h) || defined(has_co_dico) */
		      *str = ttdtoupper(*str);
		    }
#endif /* german */

		    if (id || id2) {
		      if (Id == 2) { // c'est une vraie ff mais un cc seulement par correction -> on veut juste la ff
			SXINT triple, triple2;

			XxYxZ_Xforeach (acc_dag, max_acc_dag_state, triple) {
			  XxYxZ_erase (acc_dag, triple); // on supprime ladite correction du dag des amalgmes et composants de compos�s...
			}
			XxYxZ_gc (&acc_dag);
			XxYxZ_set (&acc_dag, max_acc_dag_state, 0, max_acc_dag_state + 1, &triple2); // ...et on remplace la transition dans ce dag par une transition bidon pour garantir sa connexit�
		      }
		      if (id)
			store_if_str (str, 0);
		      if (id2) {
			*str = ttdtolower(*str); //changer
			store_if_str (str, 0);
			*str = ttdtoupper(*str);
		      }
		      store_if_str (NULL, 0);
		      /* dernier appel */
		    }
		    else if (Id == 2 && is_spelling_correction) /* pour �tre amlgm ou cc, il a fallu corriger ; on tente de
								   corriger pour en faire une ff*/
		      /* sinon, on tente une full correction non d�terministe (NEW!!!) */
		      id = call_sxspell (dico_des_ff, &if_FSA, str, lgth, store_if_id /* appel � NULL inclus */, SXFALSE /* !is_deterministic */);
		  }
		  else {
		    /* Il y a des majuscules ds le mot */
		    /* Voir les explications ci-dessus � propos des recherches dans dico_cmpnd, tout est pareil sauf que l'on cherche maintenant dans dico_des_ff,
		       avec if_FSA, que l'on �crit directement les trucs trouv�s (output_if_id) et que l'on utilise if_SPELLED_VSTR comme truc temporaire */
		    id = 0;

		    id = sxdfa_comb_get (dico_des_ff, str, lgth);
#if defined(dico_ff_checkonly_h) || defined(has_co_dico)
		    if (id == 0)
		      id = sxdfa_comb_get (dico_des_ff_checkonly, str, lgth);
#endif /* defined(dico_ff_checkonly_h) || defined(has_co_dico) */
		    if (id == 0 && is_keep_original_tokens) {
		      store_if_str (str, -1);
		    }
		    if (id) {
		      store_if_str (str, -1);
		      //		    if (real_tok_no == init_tok_no) {
		      char str_0 = *str;
		      *str = ttdtolower (*str); // changer
		      id = sxdfa_comb_get (dico_des_ff, str, lgth);
#if defined(dico_ff_checkonly_h) || defined(has_co_dico)
		      if (id == 0)
			id = sxdfa_comb_get (dico_des_ff_checkonly, str, lgth);
#endif /* defined(dico_ff_checkonly_h) || defined(has_co_dico) */
		      if (id)
			store_if_str (str, -1);
		      else
			id = call_light_sxspell (dico_des_ff, &light_FSA, str, lgth, store_if_id, SXFALSE);
		      *str = str_0;
		    }
		    //		  }

		    if (is_light_correction) {
		      if (id == 0)
			id = call_light_sxspell (dico_des_ff, &if_FSA, str, lgth, store_if_id, SXFALSE);
		    } else {		    
		      id = sxdfa_comb_get (dico_des_ff, str, lgth);
#if defined(dico_ff_checkonly_h) || defined(has_co_dico)
		      if (id == 0)
			id = sxdfa_comb_get (dico_des_ff_checkonly, str, lgth);
#endif /* defined(dico_ff_checkonly_h) || defined(has_co_dico) */
		      
		      if (id) {
			store_if_str (str, -1);
			store_if_str (NULL, 0);
		      }
		    }

		    if (id == 0) {
		      if (is_output_uw) {
			if (real_tok_no == init_tok_no)
			  /* On va sortir (_uw | _Uw) */
			  store_if_str (uw, 0);
		  
			store_if_str (Uw, 0);
		      } else if (!is_keep_original_tokens) /* sinon �a a d�j� �t� output_if_stris� */
			store_if_str (str, 0);
		      store_if_str (NULL, 0);
		    }
		    /* else tout a ete sorti */
		  }

		  max_acc_dag_state++;
		}
#endif /* HAS_CMPD_OR_AMLGM */
	      }
	      if (x == 1) {
		prev_ste_is_sent_bound_ste = (if_stack [x] == sent_bound_ste);
		prev_ste_is_dblquote_ste = (if_stack [x] == dblquote_ste);
		prev_ste_is_guillemo_ste = (if_stack [x] == guillemo_ste);
	      }
	    }
	  }
	}

#ifdef HAS_CMPD_OR_AMLGM
	if (max_acc_dag_state > 1) {
	  to_tok_no = eol_tok_no;
	  close_local_dags (max_acc_dag_state);
	}
#endif /* HAS_CMPD_OR_AMLGM */

	final_comment = SXGET_TOKEN (eol_tok_no).comment;
	
	if (final_comment)
	  printf ("%s", final_comment);

	fputs ("\n", stdout);
	  
	text2dag_raz();
      }
      else if (sxplocals.Mtok_no == 1) {
	final_comment = SXGET_TOKEN (eol_tok_no).comment;
	
	if (final_comment)
	  printf ("%s\n", final_comment);
      }
      
      sxcomment_clear (&(sxplocals.sxcomment));
      sxstr_mngr (SXBEGIN); // vidange du sxstr_mngr
      
      sxplocals.Mtok_no = sxplocals.atok_no = sxplocals.ptok_no = 0;
    } while (lahead != tmax);
    
    
    return 0;

  case SXFINAL:
    text2dag_finalize();

    return 0;

  case SXERROR:
  case SXACTION:
  default:
    fputs ("\nThe function \"text2dag_semact\" is out of date with respect to its specification.\n", sxstderr);
    abort ();
  }
}

