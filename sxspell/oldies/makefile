# To avoid problems with other shells:
SHELL=/bin/sh

LEX=/home/bourbon/sagot/easy

SX=$(sx)
LNorCP=${SHELL} $(SX)/etc/bin/lien-relatif.sh
SXSRC=$(SX)/src
SXLIB=$(SX)/lib
SXINCL=$(SX)/incl
ETC=$(SX)/etc
ETCLIB=$(ETC)/lib
ETCINCL=$(ETC)/incl

SPELL=.
SRC=$(SPELL)/src
LIB=$(SPELL)/lib
INCL=$(SPELL)/incl


BIBS= $(ETCLIB)/libsxetc$(sxV).a $(SXLIB)/libsx$(sxV).a

#CC=cd $(LIB); umask 7; exec cc -I$(SXINCL) -I$(ETCINCL) -I$(INCL) $(CFLAGS) -c
CC=umask 7; exec cc -I$(SXINCL) -I$(ETCINCL) -I$(INCL) $(CFLAGS) -c

$(SPELL)/ALL .PRECIOUS:	$(DICO_ORTHO)		\
			$(SPELL)/../sxspell.out		\
			$(SPELL)/../sxspeller.out		\
			$(SPELL)/../named_entity.out	\
			$(SPELL)/../ieme.out

$(SPELL)/../sxspell.out:	$(SPELL)/sxspell.out
	@rm -f $@; mv $? $@ && $(LNorCP) $@ $?

$(SPELL)/../sxspeller.out:	$(SPELL)/sxspeller.out
	@rm -f $@; mv $? $@ && $(LNorCP) $@ $?

$(SPELL)/../named_entity.out:	$(SPELL)/named_entity.out
	@rm -f $@; mv $? $@ && $(LNorCP) $@ $?

$(SPELL)/../ieme.out:	$(SPELL)/ieme.out
	@rm -f $@; mv $? $@ && $(LNorCP) $@ $?

#####################################################################################################################
# Construction des .h du lexique orthographique

DICOS_ORTHO=\
	$(INCL)/dico_des_mots.h		\
	$(INCL)/dico_des_prefixes.h	\
	$(INCL)/dico_des_suffixes.h	\

$(INCL)/dico_des_mots.h:
	dico $(LEX)/dico.sxspell > $(INCL)/dico_des_mots.h

$(INCL)/dico_des_prefixes.h:
	dico -prefix_bound $(LEX)/dico.sxspell.pref > $(INCL)/dico_des_prefixes.h

$(INCL)/dico_des_suffixes.h:
	dico -suffix_bound $(LEX)/dico.sxspell.suff > $(INCL)/dico_des_suffixes.h


#####################################################################################################################
# Test de sxspell_mngr.c, on utilise le dico francais de la campagne easy

SXSPELL=\
	$(LIB)/sxspell_main.o		\
	$(LIB)/sxspell_mngr.o

$(SPELL)/sxspell.out:	$(SXSPELL)	$(BIBS)
	@echo '	cc -o $@'; rm -f $@; ${SHELL} -c 'cc -o $@ $(SXSPELL) $(CFLAGS) $(LDFLAGS) $(BIBS) || (rm $@;exit 1)'

$(LIB)/sxspell_main.o:		$(SXINCL)/sxunix.h 	\
				$(SXINCL)/sxba.h	\
				$(SXINCL)/sxword.h	\
				$(SXINCL)/XxY.h		\
				$(SXINCL)/sxdico.h	\
				$(INCL)/dico_des_mots.h \
				$(SXINCL)/sxspell.h	\
			$(SRC)/sxspell_main.c
	$(CC) $(SRC)/sxspell_main.c -o $(LIB)/sxspell_main.o	\
			-DSXSPELL_RULES				\
			-DEMO					\
			-Ddico_mots_h=\"dico_des_mots.h\"


#                       -DEMO active l'entree sxspell_model de sxspell_mngr


# On recompile sxspell_mngr avec l'option -DEMO
$(LIB)/sxspell_mngr.o:		$(SXINCL)/sxunix.h 	\
				$(SXINCL)/sxstack.h	\
				$(SXINCL)/sxba.h	\
				$(SXINCL)/sxword.h	\
				$(SXINCL)/XxY.h		\
				$(SXINCL)/sxdico.h	\
				$(SXINCL)/sxspell.h	\
			$(SXSRC)/sxspell_mngr.c
	$(CC) $(SXSRC)/sxspell_mngr.c -o $(LIB)/sxspell_mngr.o -DSXSPELL_RULES -DEMO -DEBUG

# -DSXSPELL_RULES => on utilise les regles "standard" de sxspell.h
# -DEMO           => active l'entree sxspell_model de sxspell_mngr.c

######################################################################################################################
# Wrappeur de sxspell_mngr qui prend en entree une sequence de mots et retourne 
# un certain nb de trucs en fonction des parametres d'appel
# la phrase corrigee, la phrase segmentee, les mots inconnus, ...
# dico_des_mots.h est construit par :
# dico $sx/spell/spec/dico_des_mots.txt > $sx/spell/incl/dico_des_mots.h
# dico_des_prefixes.h est construit par :
# dico -prefix_bound $sx/spell/spec/dico_des_prefixes.txt > $sx/spell/incl/dico_des_prefixes.h
# dico_des_suffixes.h est construit par :
# dico -suffix_bound $sx/spell/spec/dico_des_suffixes.txt > $sx/spell/incl/dico_des_suffixes.h
# Les sources dico_des_*.txt sont crees par Benoit
# dico_des_mots.txt est une fusion des formes flechies, des amalgames et des composants des mots-composes

SPELLER=$(LIB)/sxspeller.o	\
	$(LIB)/sxspeller_t.o

$(SPELL)/sxspeller.out:	$(SPELLER)	$(BIBS)
	@echo '	cc -o $@'; rm -f $@; ${SHELL} -c 'cc -o $@ $(SPELLER) $(CFLAGS) $(LDFLAGS) $(BIBS) || (rm $@;exit 1)'

$(LIB)/sxspeller.o:		$(SXINCL)/sxunix.h 	\
				$(SXINCL)/sxdico.h	\
				$(SXINCL)/sxspell.h	\
				$(INCL)/dico_des_mots.h 	\
				$(INCL)/dico_des_prefixes.h 	\
				$(INCL)/dico_des_suffixes.h 	\
			$(SRC)/sxspeller.c
	$(CC) $(SRC)/sxspeller.c -o $(LIB)/sxspeller.o	       	\
			-Dsxtables=sxspeller_tables		\
			-DSXSPELL_RULES	       			\
			-Ddico_mots_h=\"dico_des_mots.h\"	\
			-Ddico_prefix_h=\"dico_des_prefixes.h\"	\
			-Ddico_suffix_h=\"dico_des_suffixes.h\"

#                       -DSXSPELL_RULES => on utilise les regles "standard" de sxspell.h

$(LIB)/sxspeller_t.o: 	$(SXINCL)/sxunix.h	\
			$(SRC)/sxspeller_t.c
	$(CC) $(SRC)/sxspeller_t.c -o $(LIB)/sxspeller_t.o	\
			-Dsxtables=sxspeller_tables	\
			-DSEMACT=sxspeller_action

######################################################################################################################
# Reconnaissance des entitees nommees
# Pour l'instant les nombres

NAMED=	$(LIB)/named_entity_main.o	\
	$(LIB)/named_entity_action.o	\
	$(LIB)/named_entity_t.o

$(SPELL)/named_entity.out:	$(NAMED)	$(BIBS)
	@echo '	cc -o $@'; rm -f $@; ${SHELL} -c 'cc -o $@ $(NAMED) $(CFLAGS) $(LDFLAGS) $(BIBS) || (rm $@;exit 1)'

$(LIB)/named_entity_main.o:	$(SXINCL)/sxunix.h	\
			$(ETCINCL)/release.h		\
			$(SRC)/named_entity_main.c
	$(CC) $(SRC)/named_entity_main.c -o $(LIB)/named_entity_main.o		\
			-Dsxtables=named_entity_tables

$(LIB)/named_entity_action.o:	$(SXINCL)/sxunix.h	\
			$(INCL)/named_entity_td.h	\
			$(SRC)/named_entity_action.c
	$(CC) $(SRC)/named_entity_action.c -o $(LIB)/named_entity_action.o

$(LIB)/named_entity_t.o: 	$(SXINCL)/sxunix.h	\
			$(SRC)/named_entity_t.c
	$(CC) $(SRC)/named_entity_t.c -o $(LIB)/named_entity_t.o	\
			-Dsxtables=named_entity_tables	\
			-DSCANACT=named_entity_scan_act\
			-DSEMACT=named_entity_action	\
			-DPARSACT=named_entity_parsact

######################################################################################################################
# Reconnaissance des entitees nommees
# Pour l'instant les nombres

IEME=	$(LIB)/ieme_main.o	\
	$(LIB)/ieme_action.o	\
	$(LIB)/ieme_t.o

$(SPELL)/ieme.out:	$(IEME)	$(BIBS)
	@echo '	cc -o $@'; rm -f $@; ${SHELL} -c 'cc -o $@ $(IEME) $(CFLAGS) $(LDFLAGS) $(BIBS) || (rm $@;exit 1)'

$(LIB)/ieme_main.o:	$(SXINCL)/sxunix.h	\
			$(ETCINCL)/release.h	\
			$(SRC)/ieme_main.c
	$(CC) $(SRC)/ieme_main.c	-o $(LIB)/ieme_main.o	\
			-Dsxtables=ieme_tables

$(LIB)/ieme_action.o:	$(SXINCL)/sxunix.h	\
			$(INCL)/ieme_td.h	\
			$(SRC)/ieme_action.c
	$(CC) $(SRC)/ieme_action.c -o $(LIB)/ieme_action.o

$(LIB)/ieme_t.o: 	$(SXINCL)/sxunix.h	\
			$(SRC)/ieme_t.c
	$(CC) $(SRC)/ieme_t.c -o $(LIB)/ieme_t.o	\
			-Dsxtables=ieme_tables	\
			-DSEMACT=ieme_action	\
			-DPARSACT=ieme_parsact
