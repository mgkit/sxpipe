/* ********************************************************
   *							  *
   *							  *
   * Copyright (c) 2004 by Institut National de Recherche *
   *                    en Informatique et en Automatique *
   *							  *
   *							  *
   ******************************************************** */




/* ********************************************************
   *							  *
   *  Produit de l'equipe ATOLL.                 	  *
   *							  *
   ******************************************************** */

/* Programme qui tente de corriger les fautes introduites par un traitement OCR */


/************************************************************************/
/* Historique des modifications, en ordre chronologique inverse:	*/
/************************************************************************/
/* jeudi 2 Sep 2004 (pb):	Ajout de cette rubrique "modifications"	*/
/************************************************************************/


#define WHAT	"@(#)sxspeller.c\t- SYNTAX [unix] -  jeudi 2 Sep 2004"
static struct what {
  struct what	*whatp;
  char		what [sizeof (WHAT)];
} what = {&what, WHAT};


#include "sxunix.h"
#include "sxspell.h"
#include "varstr.h"
#include "sxstack.h"
#include "sxba.h"

/* On lit a priori sur stdin, et cetera */
FILE	          *sxstdout, *sxstderr;
FILE	          *sxtty;

/*  S T A T I C     V A R I A B L E S   */
static int     max_weight, sub_word_weight, light_weight; 
static BOOLEAN is_sub_word, print_error, print_input, print_output, print_comment, check_lower, is_tokenizer, is_process_lower,
               is_cut_on_quote; 
static BOOLEAN process_prefix, process_suffix, is_cut_on_hyphen, is_light_correction, is_spelling_correction, is_dry_run;

/* On est dans un cas "mono-langage": */
extern struct sxtables	sxtables;

static BOOLEAN	is_error;

/*---------------*/
/*    options    */
/*---------------*/

static char	ME [] = "sxspeller";
static char	Usage [] = "\
Usage:\t%s [options] [source_file]\n\
options=\
\t--help,\n\
\t\t-mw weight_val (default == UNBOUNDED), -max_weight weight_val,\n\
\t\t-sww weight_val (default == no sub_word seek), -sub_word_weight weight_val,\n\
\t\t-i, -input,\n\
\t\t-o, -output,\n\
\t\t-e, -error,\n\
\t\t-c, -comment,\n\
\t\t-cl, -check_lower,\n\
\t\t-pl, -process_lower,\n\
\t\t-cq, -cut_on_quote,\n\
\t\t-ch, -cut_on_hyphen,\n\
\t\t-pp, -process_prefix,\n\
\t\t-ps, -process_suffix,\n\
\t\t-lw w, -light_weight weight-value,\n\
\t\t-sc -spelling_correction (default), -nsc -no_spelling_correction,\n\
\t\t-d, -dry_run,\n\
\t\t-t, -tokenizer,\n";

#define OPTION(opt)	(1 << (opt - 1))
#define noOPTION(opt)	(~OPTION (opt))

#define OPT_NB ((sizeof(option_tbl)/sizeof(*option_tbl))-1)

#define UNKNOWN_ARG 		0
#define HELP 		        1
#define WEIGHT	                2
#define SUB_WORD_WEIGHT	        3
#define PRINT_INPUT	        4
#define PRINT_OUTPUT	        5
#define PRINT_ERROR	        6
#define CHECK_LOWER	        7
#define PROCESS_LOWER	        8
#define CUT_ON_QUOTE	        9
#define CUT_ON_HYPHEN	        10
#define PROCESS_PREFIX	        11
#define PROCESS_SUFFIX	        12
#define LIGHT_WEIGHT	        13
#define SPELLING_CORRECTION     14
#define PRINT_COMMENT	        15
#define DRY_RUN	                16
#define TOKENIZER	        17
#define LAST_OPTION		TOKENIZER

static char	*option_tbl [] = {"",
				  "-help",
				  "mw", "max_weight",
				  "sww", "sub_word_weight",
				  "i", "input",
				  "o", "output",
				  "e", "error",
				  "c", "comment",
				  "cl", "check_lower",
				  "pl", "process_lower",
				  "cq", "cut_on_quote",
				  "ch", "cut_on_hyphen",
				  "pp", "process_prefix",
				  "ps", "process_suffix",
				  "lw", "light_weight",
				  "sc", "spelling_correction", "nsc", "no_spelling_correction",
				  "d", "dry_run",
				  "t", "tokenizer",
};

static int	option_kind [] = {UNKNOWN_ARG,
				  HELP,
				  WEIGHT, WEIGHT,
				  SUB_WORD_WEIGHT, SUB_WORD_WEIGHT,
				  PRINT_INPUT, PRINT_INPUT,
				  PRINT_OUTPUT, PRINT_OUTPUT,
				  PRINT_ERROR, PRINT_ERROR,
				  PRINT_COMMENT, PRINT_COMMENT,
				  CHECK_LOWER, CHECK_LOWER,
				  PROCESS_LOWER, PROCESS_LOWER,
				  CUT_ON_QUOTE, CUT_ON_QUOTE,
				  CUT_ON_HYPHEN, CUT_ON_HYPHEN,
				  PROCESS_PREFIX, PROCESS_PREFIX,
				  PROCESS_SUFFIX, PROCESS_SUFFIX,
				  LIGHT_WEIGHT, LIGHT_WEIGHT,
				  SPELLING_CORRECTION, SPELLING_CORRECTION, -SPELLING_CORRECTION, -SPELLING_CORRECTION,
				  DRY_RUN, DRY_RUN,
				  TOKENIZER, TOKENIZER,
};

static int	option_get_kind (arg)
    register char	*arg;
{
    register char	**opt;

    if (*arg++ != '-')
	return UNKNOWN_ARG;

    for (opt = &(option_tbl [OPT_NB]); opt > option_tbl; opt--) {
	if (strcmp (*opt, arg) == 0 /* egalite */ )
	    break;
    }

    return option_kind [opt - option_tbl];
}


static char	*option_get_text (kind)
    register int	kind;
{
    register int	i;

    for (i = OPT_NB; i > 0; i--) {
	if (option_kind [i] == kind)
	    break;
    }

    return option_tbl [i];
}

#if 0
/* Le 20/10/04 on utilise un dico specifique fusion des ff et des cmpnd */
/* Ca permet de faire de la correction d'erreurs dans les 2 !! */
/* Utilisation simultanee des 2 dico du meme source */
#define ICO_INFLECTED_FORMS
#define ICO_COMPOUND

#include dico_h
/* le dico des formes flechies */
static struct dico *dico_des_ff = &(dico_if);

/* le dico des mots composes */
static struct dico *dico_des_mc = &(dico_cmpnd);
#endif /* 0 */

#define dico_hd     dico_mots
#define char2class  mots_char2class
#define comb_vector mots_comb_vector

#include dico_mots_h
/* le dico des formes flechies et des mots-composes */
static struct dico *dico_des_mots = &(dico_mots);


#undef dico_hd
#undef char2class
#undef comb_vector
#define dico_hd     dico_prfx
#define char2class  prfx_char2class
#define comb_vector prfx_comb_vector

/* le dico des prefixes remarquables */
#include dico_prefix_h
static struct dico *dico_des_prefixes = &(dico_prfx);

#undef dico_hd
#undef char2class
#undef comb_vector
#define dico_hd     dico_sffx
#define char2class  sffx_char2class
#define comb_vector sffx_comb_vector

/* le dico des suffixes remarquables */
#include dico_suffix_h
static struct dico *dico_des_suffixes = &(dico_sffx);

static VARSTR        vstr, cvstr, wstr, bivstr, vstr_mot_en_min;
static char          *MOT_A_CORRIGER;

#define SXBA_fdcl(n,l)	SXBA_ELT n [NBLONGS (l) + 1] = {l,}
static SXBA_fdcl (MAJ_set, 256);
static SXBA_fdcl (vowell_set, 256);
static SXBA_fdcl (mpb_set, 256);

static SXBA          hyphen_set;

static char *MAJ_string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ�������������������������������";
static char *vowell_string = "AaEeIiOoUuYy����������������������������������������������������";
static char *mpb_string = "mpb";


/* Les regles pour le tokenizeur de la campagne EASY */
#define EASY_maj_min_weight	           (-1)
#define EASY_diacritics_weight	           (-5)
#define EASY_2chars_to_diacritics_weight   (-5)
#define EASY_no_repeat_weight              (-5)
#define EASY_change_case_weight            (-8)
#define EASY_final_als_weight              (-10)
#define EASY_iI_en_l_weight                (-15)
#define EASY_final_h_weight                (-20)
#define EASY_eau_en_au_weight              (-20)
#define EASY_final_dot_weight              5
#define EASY_t_il_weight                   15
#define EASY_blanc_weight                  15
#define EASY_mm_en_m_weight                20
#define EASY_m_en_mm_weight                20
#define EASY_add_h_weight                  24
#define EASY_final_e_weight                25
#define EASY_gu_ge_weight                  30
#define EASY_dusse_je_weight               40
#define EASY_add_apos_weight               45


#define light_compose_weight               100


/* ajout d'une apostrophe dans certains contextes - poids assez �lev� : EASY_add_apos_weight */
static char *EASY_add_apos  [] = {
  "a", "a'",
  "�", "�'",
  "e", "e'",
  "�", "�'",
  "�", "�'",
  "�", "�'",
  "i", "i'",
  "�", "�'",
  "o", "o'",
  "�", "�'",
  "u", "u'",
  "�", "�'",
  "a'", "a",
  "�'", "�",
  "e'", "e",
  "�'", "�",
  "�'", "�",
  "�'", "�",
  "i'", "i",
  "�'", "�",
  "o'", "o",
  "�'", "�",
  "u'", "u",
  "�'", "�",
  ".'", "_",
};

/* abr�viations sans leur point - poids tr�s faible EASY_final_dot_weight */ 
static char *EASY_final_dot  [] = {
  "$", ".",
};

/* ajout d'un h initial ou final - poids moyen EASY_final_h_weight */
static char *EASY_final_h  [] = {
  "$", "h",
  "^", "h",
  "^z", "j",
};

/* ajout final d'un e - poids assez �lev� EASY_final_e_weight */
static char *EASY_final_e  [] = {
  "$", "e",
  "ts$", "s",
  "e$", "",
  "s$", "",
  "^z", "",
  "euh$", "e",
  "^x", "ex",
  "'$", "",
};

/* flexions mal fix�es - poids faible  EASY_final_als_weight */
static char *EASY_final_als [] = {
  "als$", "aux",
  "aux$", "als",
  "�re$", "euse",
  "euse$", "�re",
  "�res$", "euses",
  "euses$", "�res",
  "oye$", "oie",
  "oyes$", "oies",
  "oyent$", "oient",
  "uye$", "uie",
  "uyes$", "uies",
  "uyent$", "uient",
};

/* i' ou I'- poids faible EASY_iI_en_l_weight */
static char *EASY_iI_en_l [] = {
  "^i'$", "l'",
  "^I'$", "l'",
};

/* i' ou I'- poids faible EASY_dusse_je_weight */
static char *EASY_dusse_je [] = {
  "ss�$", "s",
};

/* -t-il et similaires incomplets - poids faible EASY_t_il_weight */
static char *EASY_t_il [] = {
  "^-t", "-t-",
  "t$", "",
};

/* Suppression de l'espace ou remplacement par un tiret - poids faible EASY_blanc_weight */
static char *EASY_blanc [] = {
  " ", "",
  " ", "-",
  "-", "",
};

/* eau <-> au - poids assez faible EASY_eau_en_au_weight */
static char *EASY_eau_en_au [] = {
  "eau", "au",
  "au", "eau",
};

/* manque un u ou un e apr�s un g pour en changer le son - poids moyen EASY_gu_ge_weight */
static char *EASY_gu_ge [] = {
  "ge", "gue",
  "g�", "gu�",
  "g�", "gu�",
  "g�", "gu�",
  "gi", "gui",
  "g�", "gu�",
  "ga", "gea",
  "g�", "ge�",
  "go", "geo",
  "g�", "ge�",
  "gu", "geu",
  "g�", "ge�",
};

/* ajout d'un h dans certains contextes - poids moyen EASY_add_h_weight */
static char *EASY_add_h [] = {
  "a", "ah",
  "�", "�h",
  "e", "eh",
  "�", "�h",
  "�", "�h",
  "�", "�h",
  "i", "ih",
  "�", "�h",
  "o", "oh",
  "�", "�h",
  "u", "uh",
  "�", "�h",
  "k", "kh",
  "t", "th",
  "p", "ph",
  "d", "dh",
  "g", "gh",
  "r", "rh",
  "b", "bh",
  "z", "zh",
  "s", "sh",
  "c", "ch",
  "j", "dj",
  "dj", "j",
};


/* Changement de casse */
static char *EASY_maj_min [] = {
  "A", "a",
  "B", "b",
  "C", "c",
  "D", "d",
  "E", "e",
  "F", "f",
  "G", "g",
  "H", "h",
  "I", "i",
  "J", "j",
  "K", "k",
  "L", "l",
  "M", "m",
  "N", "n",
  "O", "o",
  "P", "p",
  "Q", "q",
  "R", "r",
  "S", "s",
  "T", "t",
  "U", "u",
  "V", "v",
  "W", "w",
  "X", "x",
  "Y", "y",
  "Z", "z",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
  "�", "�",
};

/* diacritics ecrits comme 2 caracteres successifs ... */
static char *EASY_2chars_to_diacritics [] = {
  "A^", "�",
  "A'", "�",
  "A`", "�",
  "a^", "�",
  "a'", "�",
  "a`", "�",

  "E^", "�",
  "E'", "�",
  "E`", "�",
  "E\"", "�",
  "e^", "�",
  "e'", "�",
  "e`", "�",
  "e\"", "�",

  "I^", "�",
  "I\"", "�",
  "i^", "�",
  "i\"", "�",

  "O^", "�",
  "o^", "�",

  "U^", "�",
  "U`", "�",
  "U\"", "�",
  "u^", "�",
  "u`", "�",
  "u\"", "�",

  "y\"", "�",

  "C,", "�",
  "c,", "�",
};


/* remplacement des diacritics */
static char *EASY_diacritics [] = {
  "A", "a���",
  "�", "a���",
  "�", "a���",
  "�", "a���",
  "a", "���",
  "�", "a��",
  "�", "a��",
  "�", "a��",

  "E", "e����",
  "�", "e����",
  "�", "e����",
  "�", "e����",
  "�", "e����",
  "e", "����",
  "�", "e���",
  "�", "e���",
  "�", "e���",
  "�", "e���",

  "I", "i��",
  "�", "i��",
  "�", "i��",
  "i", "��",
  "�", "i�",
  "�", "i�",

  "O", "o�",
  "�", "o�",
  "o", "�",
  "�", "o",

  "U", "u���",
  "�", "u���",
  "�", "u���",
  "�", "u���",
  "u", "���",
  "�", "u��",
  "�", "u��",
  "�", "u��",

  "Y", "y�",
  "y", "�",
  "�", "y",

  "C", "c�",
  "�", "c�",
  "c", "�",
  "�", "c",
};

/* Doublement de lettres ... */
static char *EASY_m_en_mm [] = {
  "b", "bb",
  "c", "cc",
  "d", "dd",
  "�", "��",
  "f", "ff",
  "g", "gg",
  "l", "ll",
  "m", "mm",
  "n", "nn",
  "o", "oo",
  "p", "pp",
  "r", "rr",
  "s", "ss",
  "t", "tt",
  "z", "zz",
};

/* ... et pas de doublement */
static char *EASY_mm_en_m [] = {
  "aa", "a",
  "��", "�",
  "��", "�",
  "bb", "b",
  "cc", "c",
  "dd", "d",
  "ee", "e",
  "��", "�",
  "��", "�",
  "��", "�",
  "��", "�",
  "ff", "f",
  "gg", "g",
  "hh", "h",
  "ii", "i",
  "��", "�",
  "��", "�",
  "jj", "j",
  "kk", "k",
  "ll", "l",
  "mm", "m",
  "nn", "n",
  "oo", "o",
  "��", "�",
  "pp", "p",
  "qq", "q",
  "rr", "r",
  "ss", "s",
  "tt", "t",
  "uu", "u",
  "��", "�",
  "��", "�",
  "��", "�",
  "vv", "v",
  "ww", "w",
  "xx", "x",
  "yy", "y",
  "��", "�",
  "zz", "z",
};


#define qq_trucs_weight    50
/* ... et qq_trucs */
static char *qq_trucs [] = {
  "@", "a",
  "@", "",
  "K", "ca",
  "k", "ca",
};

static char *action2prefix_chars [] = {"",
				       /* 1 */ "_",
				       /* 2 */ "-_",
				       /* 3 */ "e-_",
				       /* 4 */ "e-_",
				       /* 5 */ "_",
				       /* 6 */ "_",
};

static char *action2suffix_chars [] = {"",
				       /* 1 */ "_",
};



static struct FSA          main_FSA, light_FSA, Xlight_FSA;

static int
dico_seek (dico, word, lgth)
     struct dico *dico;
     int word, lgth;
{
  return sxdico_get (dico, &word, &lgth);
}

static struct is_a_spelling_correction {
  char    *word;
  int     lgth;
  BOOLEAN yes;
} is_a_spelling_correction;

static struct vstr_stack_hd {
  int     top, size;
  VARSTR  *vstrs;
} *prefix_stack_ptr, *suffix_stack_ptr;

struct correction_struct {
  int                             id, cost;
  VARSTR                          vstr, cvstr;
  struct is_a_spelling_correction is_a_spelling_correction;
  char                            *word;
  BOOLEAN                         is_first_word;
  struct vstr_stack_hd            prefix_stack, suffix_stack;
};

static struct correction_struct corrections [3], *prev_correction, *cur_correction, *bi_correction;
static int which_correction, light_cost, heavy_cost;

#define RAZ_VSTR_STACK(t) (t).top=0

/* On stocke ds la pile des suffixes la sous-chaine */
static void
push_string (stack, lstr, rstr)
     struct vstr_stack_hd *stack;
     char                 *lstr, *rstr;
{
  int    lgth;
  VARSTR vstr;

  lgth = rstr-lstr;

  if (++stack->top > stack->size) {
    stack->vstrs = (VARSTR *) sxrecalloc (stack->vstrs, stack->size+1, 2*stack->size+1, sizeof (VARSTR));
    stack->size *= 2;
  }

  if (stack->vstrs [stack->top] == NULL)
    vstr = stack->vstrs [stack->top] = varstr_alloc (32+lgth);
  else {
    /* y-a-t'il assez de place ? */
    vstr = varstr_raz (stack->vstrs [stack->top]);

    while (vstr->current+lgth+1 >= vstr->last)
      vstr = varstr_realloc (vstr);
  }

  varstr_lcatenate (vstr, lstr, lgth);
}

static void
alloc_vstr_stack_hd (stack, size)
     struct vstr_stack_hd *stack;
     int                  size;
{
  stack->size = size;
  stack->vstrs = (VARSTR *) sxcalloc (stack->size+1, sizeof (VARSTR));
  stack->top = 0;
}

static void
free_vstr_stack_hd (stack)
     struct vstr_stack_hd *stack;
{
  VARSTR  vstr, *p = stack->vstrs, *top = stack->vstrs+stack->size;

  while (++p <= top) {
    if ((vstr = *p) == NULL)
      break;
      
    varstr_free (vstr);
  }

  sxfree (stack->vstrs), stack->vstrs = NULL;
}

static BOOLEAN
call_Xlight_sxspell (mot, lgth)
     char    *mot;
     int     lgth;
{  char    last_char;
  int     x;
  BOOLEAN ret_val;

  last_char = mot [lgth];
  mot [lgth] = NUL;

  sxspell_init_fsa (mot, lgth, &Xlight_FSA);

  /* changement apos/tiret */
  sxspell_replace (&Xlight_FSA, apos_hyphen, T_NB (apos_hyphen), apos_hyphen_weight);
  /* changement de casse */
  sxspell_replace (&Xlight_FSA, change_case, T_NB (change_case), EASY_change_case_weight /* change_case_weight */);
  sxspell_replace (&Xlight_FSA, EASY_maj_min, T_NB (EASY_maj_min), EASY_maj_min_weight);
  sxspell_replace (&Xlight_FSA, EASY_diacritics, T_NB (EASY_diacritics), EASY_diacritics_weight);

  sxspell_replace_n_p (&Xlight_FSA, EASY_2chars_to_diacritics, T_NB (EASY_2chars_to_diacritics), EASY_2chars_to_diacritics_weight);
  sxspell_replace_n_p (&Xlight_FSA, EASY_mm_en_m, T_NB (EASY_mm_en_m), EASY_mm_en_m_weight);
  sxspell_replace_n_p (&Xlight_FSA, EASY_m_en_mm, T_NB (EASY_m_en_mm), EASY_m_en_mm_weight);

  sxspell_replace_n_p (&Xlight_FSA, EASY_final_dot, T_NB (EASY_final_dot), EASY_final_dot_weight);
  sxspell_replace_n_p (&Xlight_FSA, EASY_final_h, T_NB (EASY_final_h), EASY_final_h_weight);
  sxspell_replace_n_p (&Xlight_FSA, EASY_final_e, T_NB (EASY_final_e), EASY_final_e_weight);
  sxspell_replace_n_p (&Xlight_FSA, EASY_final_als, T_NB (EASY_final_als), EASY_final_als_weight);
  sxspell_replace_n_p (&Xlight_FSA, EASY_iI_en_l, T_NB (EASY_iI_en_l), EASY_iI_en_l_weight);
  sxspell_replace_n_p (&Xlight_FSA, EASY_dusse_je, T_NB (EASY_dusse_je), EASY_dusse_je_weight);
  sxspell_replace_n_p (&Xlight_FSA, EASY_t_il, T_NB (EASY_t_il), EASY_t_il_weight);
  sxspell_replace_n_p (&Xlight_FSA, EASY_blanc, T_NB (EASY_blanc), EASY_blanc_weight);
  sxspell_replace_n_p (&Xlight_FSA, EASY_eau_en_au, T_NB (EASY_eau_en_au), EASY_eau_en_au_weight);
  sxspell_replace_n_p (&Xlight_FSA, EASY_gu_ge, T_NB (EASY_gu_ge), EASY_gu_ge_weight);
  sxspell_replace_n_p (&Xlight_FSA, EASY_add_h, T_NB (EASY_add_h), EASY_add_h_weight);
  sxspell_replace_n_p (&Xlight_FSA, EASY_add_apos, T_NB (EASY_add_apos), EASY_add_apos_weight);

  /* qq fautes d'orthographe */
  sxspell_replace_n_p (&Xlight_FSA, ortho, T_NB (ortho), ortho_weight);
  sxspell_replace_n_p (&Xlight_FSA, ortho2, T_NB (ortho2), ortho2_weight);

  /* nooooooooooonnnnnnnnnn => non */
  sxspell_no_repeat (&Xlight_FSA, 3, EASY_no_repeat_weight);

  /* insertion de tirets */
  for (x = lgth; x >= 1; x--)
    sxspell_add (&Xlight_FSA, x, "-", add_hyphen_weight);

  /* insertion de qq lettres */
  for (x = lgth; x >= 1; x--) {
    sxspell_add (&Xlight_FSA, x, insert_string, insert_weight);
  }

  /* remplacements */
  for (x = lgth; x >= 1; x--)
    sxspell_change (&Xlight_FSA, x, change_string, change_weight);
  
  ret_val = sxspell_do_it (&Xlight_FSA);

  mot [lgth] = last_char;

  return ret_val;
}


static BOOLEAN
call_light_sxspell (mot, lgth)
     char    *mot;
     int     lgth;
{  char    last_char;
  int     x;
  BOOLEAN ret_val;

  last_char = mot [lgth];
  mot [lgth] = NUL;

  sxspell_init_fsa (mot, lgth, &light_FSA);

  /* changement apos/tiret */
  sxspell_replace (&light_FSA, apos_hyphen, T_NB (apos_hyphen), apos_hyphen_weight);
  /* changement de casse */
  sxspell_replace (&light_FSA, change_case, T_NB (change_case), EASY_change_case_weight /* change_case_weight */);
  sxspell_replace (&light_FSA, EASY_maj_min, T_NB (EASY_maj_min), EASY_maj_min_weight);
  sxspell_replace (&light_FSA, EASY_diacritics, T_NB (EASY_diacritics), EASY_diacritics_weight);

  sxspell_replace_n_p (&light_FSA, EASY_2chars_to_diacritics, T_NB (EASY_2chars_to_diacritics), EASY_2chars_to_diacritics_weight);
  sxspell_replace_n_p (&light_FSA, EASY_mm_en_m, T_NB (EASY_mm_en_m), EASY_mm_en_m_weight);
  sxspell_replace_n_p (&light_FSA, EASY_m_en_mm, T_NB (EASY_m_en_mm), EASY_m_en_mm_weight);

  sxspell_replace_n_p (&light_FSA, EASY_final_dot, T_NB (EASY_final_dot), EASY_final_dot_weight);
  sxspell_replace_n_p (&light_FSA, EASY_final_h, T_NB (EASY_final_h), EASY_final_h_weight);
  sxspell_replace_n_p (&light_FSA, EASY_final_e, T_NB (EASY_final_e), EASY_final_e_weight);
  sxspell_replace_n_p (&light_FSA, EASY_final_als, T_NB (EASY_final_als), EASY_final_als_weight);
  sxspell_replace_n_p (&light_FSA, EASY_iI_en_l, T_NB (EASY_iI_en_l), EASY_iI_en_l_weight);
  sxspell_replace_n_p (&light_FSA, EASY_dusse_je, T_NB (EASY_dusse_je), EASY_dusse_je_weight);
  sxspell_replace_n_p (&light_FSA, EASY_t_il, T_NB (EASY_t_il), EASY_t_il_weight);
  sxspell_replace_n_p (&light_FSA, EASY_blanc, T_NB (EASY_blanc), EASY_blanc_weight);
  sxspell_replace_n_p (&light_FSA, EASY_eau_en_au, T_NB (EASY_eau_en_au), EASY_eau_en_au_weight);
  sxspell_replace_n_p (&light_FSA, EASY_gu_ge, T_NB (EASY_gu_ge), EASY_gu_ge_weight);
  sxspell_replace_n_p (&light_FSA, EASY_add_h, T_NB (EASY_add_h), EASY_add_h_weight);
  sxspell_replace_n_p (&light_FSA, EASY_add_apos, T_NB (EASY_add_apos), EASY_add_apos_weight);

  /* qq fautes d'orthographe */
  sxspell_replace_n_p (&light_FSA, ortho, T_NB (ortho), ortho_weight);
  sxspell_replace_n_p (&light_FSA, ortho2, T_NB (ortho2), ortho2_weight);

  /* nooooooooooonnnnnnnnnn => non */
  sxspell_no_repeat (&light_FSA, 3, EASY_no_repeat_weight);

  /* insertion de tirets */
  for (x = lgth; x >= 1; x--)
    sxspell_add (&light_FSA, x, "-", add_hyphen_weight);

  /* insertion de qq lettres */
  for (x = lgth; x >= 1; x--) {
    sxspell_add (&light_FSA, x, insert_string, insert_weight);
  }

  /* remplacements */
  for (x = lgth; x >= 1; x--)
    sxspell_change (&light_FSA, x, change_string, change_weight);
  
  ret_val = sxspell_do_it (&light_FSA);

  mot [lgth] = last_char;

  return ret_val;
}

static BOOLEAN
call_sxspell (mot, lgth)
     char    *mot;
     int     lgth;
{
  char    last_char;
  int     x;
  BOOLEAN ret_val;

  last_char = mot [lgth];
  mot [lgth] = NUL;

  sxspell_init_fsa (mot, lgth, &main_FSA);

  /* changement apos/tiret */
  sxspell_replace (&main_FSA, apos_hyphen, T_NB (apos_hyphen), apos_hyphen_weight);
  /* changement de casse */
  sxspell_replace (&main_FSA, change_case, T_NB (change_case), change_case_weight);
  sxspell_replace (&main_FSA, EASY_maj_min, T_NB (EASY_maj_min), EASY_maj_min_weight);
  sxspell_replace (&main_FSA, EASY_diacritics, T_NB (EASY_diacritics), EASY_diacritics_weight);
  /* les lettres double'es sont fausses */
  sxspell_replace_n_p (&main_FSA, EASY_mm_en_m, T_NB (EASY_mm_en_m), EASY_mm_en_m_weight);
  /* il faut des consonnes double'es */
  sxspell_replace_n_p (&main_FSA, EASY_m_en_mm, T_NB (EASY_m_en_mm), EASY_m_en_mm_weight);

  sxspell_replace_n_p (&main_FSA, EASY_final_dot, T_NB (EASY_final_dot), EASY_final_dot_weight);
  sxspell_replace_n_p (&main_FSA, EASY_final_h, T_NB (EASY_final_h), EASY_final_h_weight);
  sxspell_replace_n_p (&main_FSA, EASY_final_e, T_NB (EASY_final_e), EASY_final_e_weight);
  sxspell_replace_n_p (&main_FSA, EASY_final_als, T_NB (EASY_final_als), EASY_final_als_weight);
  sxspell_replace_n_p (&main_FSA, EASY_iI_en_l, T_NB (EASY_iI_en_l), EASY_iI_en_l_weight);
  sxspell_replace_n_p (&main_FSA, EASY_dusse_je, T_NB (EASY_dusse_je), EASY_dusse_je_weight);
  sxspell_replace_n_p (&main_FSA, EASY_t_il, T_NB (EASY_t_il), EASY_t_il_weight);
  sxspell_replace_n_p (&main_FSA, EASY_eau_en_au, T_NB (EASY_eau_en_au), EASY_eau_en_au_weight);
  sxspell_replace_n_p (&main_FSA, EASY_gu_ge, T_NB (EASY_gu_ge), EASY_gu_ge_weight);
  sxspell_replace_n_p (&main_FSA, EASY_add_h, T_NB (EASY_add_h), EASY_add_h_weight);
  sxspell_replace_n_p (&main_FSA, EASY_add_apos, T_NB (EASY_add_apos), EASY_add_apos_weight);

  /* traitement des fautes de frappe */
  sxspell_replace (&main_FSA, qq_trucs, T_NB (qq_trucs), qq_trucs_weight);
  /* traitement des fautes de frappe */
  sxspell_replace (&main_FSA, typos_qwerty, T_NB (typos_qwerty), typos_weight);
  /* traitement du changement de claviers */
  sxspell_replace (&main_FSA, qwerty_azerty, T_NB (qwerty_azerty), qwerty_azerty_weight);
  /* On s'est trompe' de voyelle, meme accent */
  sxspell_replace (&main_FSA, mauvaise_voyelle, T_NB (mauvaise_voyelle), mauvaise_voyelle_weight);

#if 0
  /* Essai, attention au temps ... */
  sxspell_suppress_all (&main_FSA, suppress_weight);
#endif /* 0 */
  
  /* Le temps augmente tres fortement si i > 1 !! */
  /* traitement de la suppression de 1 caractere successif */
  sxspell_suppress_i (&main_FSA, suppress_weight, 1);

  /* traitement de l'intervertion de 2 caracteres */
  sxspell_swap (&main_FSA, swap_weight);
  /* qq fautes d'orthographe */
  sxspell_replace_n_p (&main_FSA, ortho, T_NB (ortho), ortho_weight);
  sxspell_replace_n_p (&main_FSA, ortho2, T_NB (ortho2), ortho2_weight);
  /* On peut ajouter qq lettres en fin */
  sxspell_add (&main_FSA, lgth+1, "cdefhpstxz", add_weight);
  /* et une hache muette au d�but... */
  sxspell_add (&main_FSA, 1, "hH", add_weight);

  /* insertion de tirets */
  for (x = lgth; x >= 1; x--)
    sxspell_add (&main_FSA, x, "-", add_hyphen_weight);

  /* insertion de qq lettres */
  for (x = lgth; x >= 1; x--) {
    sxspell_add (&main_FSA, x, insert_string, insert_weight);
  }

  /* remplacements */
  for (x = lgth; x >= 1; x--)
    sxspell_change (&main_FSA, x, change_string, change_weight);

  ret_val = sxspell_do_it (&main_FSA);

  mot [lgth] = last_char;

  return ret_val;
}

static void
print_spelling_correction (FSA_ptr)
     struct FSA *FSA_ptr;
{
  int id, *bot_ptr, *top_ptr;
    
  /* On ne prend que le 1er resultat parmi les meilleurs */
  id = SXSPELL_x2id (*FSA_ptr, 1);

  if (!SXSPELL_has_subwords (id)) {
    if (print_comment && varstr_length (vstr))
      printf ("%s ", varstr_tostr (vstr));

    printf ("%s ", SXSPELL_id2str (*FSA_ptr, id));
  }
  else {
    /* cas sous-mots */
    SXSPELL_subword_foreach (*FSA_ptr, id, bot_ptr, top_ptr) {
      if (print_comment && varstr_length (vstr))
	printf ("%s ", varstr_tostr (vstr));

      id = SXSPELL_x2subword_id (bot_ptr);
      printf ("%s ", SXSPELL_id2str (*FSA_ptr, id));
#if 0
      if (!SXSPELL_last_subword (bot_ptr, top_ptr))
	putchar (' ');
#endif /* 0 */
    }
  }
}


static BOOLEAN
is_a_Xlight_spelling_correction (str, lgth)
     char                 *str;
     int                  lgth;
{
  /* On essaye de le corriger ... */
  /* ... avec un poids faible */
  int     store_max_weight;
  BOOLEAN ret_val;

  store_max_weight = Xlight_FSA.spell_result.max_weight; /* tres laid !! */
  Xlight_FSA.spell_result.max_weight = light_weight; /* A FAIRE mettre un Xlight_weight */

  ret_val = call_Xlight_sxspell (str, lgth);

  Xlight_FSA.spell_result.max_weight = store_max_weight; /* remise en etat */

  return ret_val;
}


static BOOLEAN
is_a_light_spelling_correction (str, lgth)
     char                 *str;
     int                  lgth;
{
  /* On essaye de le corriger ... */
  /* ... avec un poids faible */
  int     store_max_weight;
  BOOLEAN ret_val;

  store_max_weight = light_FSA.spell_result.max_weight; /* tres laid !! */
  light_FSA.spell_result.max_weight = light_weight;

  ret_val = call_light_sxspell (str, lgth);

  light_FSA.spell_result.max_weight = store_max_weight; /* remise en etat */

  return ret_val;
}


static void
store_a_Xlight_spelling_correction (stack)
     struct vstr_stack_hd *stack;
{
  int     id;
  int     *bot_ptr, *top_ptr;
  char    *c_str;

  /* La correction a marche' */
  /* on la range ds stack */
  id = SXSPELL_x2id (Xlight_FSA, 1);
  light_cost += SXSPELL_x2weight (Xlight_FSA, 1);

  if (!SXSPELL_has_subwords (id)) {
    c_str = SXSPELL_id2str (Xlight_FSA, id);
    push_string (stack, c_str, c_str+strlen (c_str));
  }
  else {
    /* cas sous-mots */
    SXSPELL_subword_foreach (Xlight_FSA, id, bot_ptr, top_ptr) {
      id = SXSPELL_x2subword_id (bot_ptr);
      c_str = SXSPELL_id2str (Xlight_FSA, id);
      push_string (stack, c_str, c_str+strlen (c_str));
    }
  }
}


static void
store_a_light_spelling_correction (stack)
     struct vstr_stack_hd *stack;
{
  int     id;
  int     *bot_ptr, *top_ptr;
  char    *c_str;

  /* La correction a marche' */
  /* on la range ds stack */
  id = SXSPELL_x2id (light_FSA, 1);
  light_cost += SXSPELL_x2weight (light_FSA, 1);

  if (!SXSPELL_has_subwords (id)) {
    c_str = SXSPELL_id2str (light_FSA, id);
    push_string (stack, c_str, c_str+strlen (c_str));
  }
  else {
    /* cas sous-mots */
    SXSPELL_subword_foreach (light_FSA, id, bot_ptr, top_ptr) {
      id = SXSPELL_x2subword_id (bot_ptr);
      c_str = SXSPELL_id2str (light_FSA, id);
      push_string (stack, c_str, c_str+strlen (c_str));
    }
  }
}


static BOOLEAN
call_a_Xlight_spelling_correction (stack, str, lgth)
     struct vstr_stack_hd *stack;
     char                 *str;
     int                  lgth;
{
  BOOLEAN ret_val;

  if (ret_val = is_a_Xlight_spelling_correction (str, lgth))
    /* La correction a marche' */
    /* on la range ds stack */
    store_a_Xlight_spelling_correction (stack);

  return ret_val;
}

static BOOLEAN
call_a_light_spelling_correction (stack, str, lgth)
     struct vstr_stack_hd *stack;
     char                 *str;
     int                  lgth;
{
  BOOLEAN ret_val;

  if (ret_val = is_a_light_spelling_correction (str, lgth))
    /* La correction a marche' */
    /* on la range ds stack */
    store_a_light_spelling_correction (stack);

  return ret_val;
}

static BOOLEAN
string2lower (vstr, str, lgth)
     VARSTR vstr;
     char *str;
     int lgth;
{
  char    car, *top_str;
  BOOLEAN has_changed;

  vstr = varstr_raz (vstr);

  while (vstr->current+lgth >= vstr->last)
    vstr = varstr_realloc (vstr);
  
  top_str = str + lgth;
  has_changed = FALSE;

  while (str < top_str) {
    car = sxtolower (*str);
    *(vstr->current++) = car;

    if (car != *str)
      has_changed = TRUE;

    str++;
  }

  *(vstr->current) = NUL;

  return has_changed;
}

/* On regarde si le mot corrige' est le meme que le mot a corriger a la casse pres */
static BOOLEAN
case_free_equality (FSA_ptr, str, lgth)
     struct FSA *FSA_ptr;
     char *str;
     int  lgth;
{
  int  id;
  char *lc_str;

  id = SXSPELL_x2id (*FSA_ptr, 1);

  if (SXSPELL_has_subwords (id))
    /* correction vers mots multiples */
    return FALSE;
  
  lc_str = SXSPELL_id2str (*FSA_ptr, id);

  if (lgth != strlen (lc_str))
    return FALSE;

  while (lgth-- > 0) {
    if (sxtolower (*str++) != sxtolower (*lc_str++))
      return FALSE;
  }

  return TRUE;
}
     

static void
print_a_Xlight_spelling_correction (str, lgth)
     char *str;
     int  lgth;
{
  char car, *p;

  /* on est ds le cas print_error */
  /* On n'imprime que si le mot corrige' n'est pas le meme que le mot a corriger a la casse pres */
  if (!case_free_equality (&Xlight_FSA, str, lgth)) {
    fputs ("\n", stdout);

    if (print_input) {
      if (print_comment && varstr_length (vstr))
	printf ("%s ", varstr_tostr (vstr));

      car = *(p = str+lgth);
      *p = NUL;
      printf ("[%s] ", str);
      *p = car;
    }

    print_spelling_correction (&Xlight_FSA);
  }
}

static void
print_a_light_spelling_correction (str, lgth)
     char *str;
     int  lgth;
{
  char car, *p;

  /* on est ds le cas print_error */
  /* On n'imprime que si le mot corrige' n'est pas le meme que le mot a corriger a la casse pres */
  if (!case_free_equality (&light_FSA, str, lgth)) {
    fputs ("\n", stdout);

    if (print_input) {
      if (print_comment && varstr_length (vstr))
	printf ("%s ", varstr_tostr (vstr));

      car = *(p = str+lgth);
      *p = NUL;
      printf ("[%s] ", str);
      *p = car;
    }

    print_spelling_correction (&light_FSA);
  }
}

/* Essai ds lequel on donne priorite' au plus long et a egalite de longueur aux prefixes suffixes */
static BOOLEAN is_process_prefix_finished, is_process_suffix_finished;

/* Version light qui n'extrait que les vrais suffixes, le reste est fait par call_process_prefix */
static BOOLEAN
call_process_suffix (str_lptr, str_rptr)
     char    **str_lptr, **str_rptr;
{
  int  action, cur_lgth;
  char *bot_str, *top_str, *lstr, *rstr, *cur_str;


  bot_str = *str_lptr;
  top_str = *str_rptr;

  while (!is_process_suffix_finished) {
    cur_str = bot_str;
    cur_lgth = top_str-bot_str;

    action = sxdico_get_bounded (dico_des_suffixes, &cur_str, &cur_lgth, SUFFIX_BOUND);

    if (action) {
      /* ...y'a un suffixe et un noyau qui peut etre vide */
      /* On stocke le separateur ... */
      lstr = action2suffix_chars [action];
      rstr = lstr + strlen (lstr);
      push_string (suffix_stack_ptr, lstr, rstr);
      light_cost += 1; /* A voir */
      /* ... et le suffixe ... */
      lstr = bot_str+cur_lgth;
      varstr_lcatenate (suffix_stack_ptr->vstrs [suffix_stack_ptr->top], lstr, top_str-lstr);

      top_str = lstr;
    }
    else
      is_process_suffix_finished = TRUE;
  }

  *str_lptr = bot_str;
  *str_rptr = top_str;

  return bot_str == top_str;
}


static BOOLEAN
call_process_prefix (str_lptr, str_rptr)
     char    **str_lptr, **str_rptr;
{
  int     id, action, action2, cur_lgth, cur_lgth2, store_weight_limit, left_pos, cur_pos, right_pos, pref_lgth, lgth;
  char    *cur_str, *cur_str2, *bot_str, *top_str, *cur_wstr, *lstr, *rstr;
  BOOLEAN done, pushed;
  char    *mot_courant;

  mot_courant = bot_str = *str_lptr;
  top_str = *str_rptr;

  /* Calcul de hyphen_set dynamique car on travaille sur un noyau du mot d'origine, les positions
     des tirets ont donc pu changer */
  sxba_empty (hyphen_set);

  if (top_str-bot_str >= (lgth = BASIZE (hyphen_set))) {
    lgth += top_str-bot_str;
    hyphen_set = sxba_resize (hyphen_set, lgth);
  }

  for (cur_str = bot_str, cur_pos = 0; cur_str < top_str; cur_str++, cur_pos++) {
    if (*cur_str == '-')
      SXBA_1_bit (hyphen_set, cur_pos);
  }

  /* a priori */
  is_process_prefix_finished = FALSE;
  is_process_suffix_finished = FALSE;

  if (bot_str < top_str /* tout n'est pas traite' */
      && process_suffix
      && !is_process_suffix_finished) {
    call_process_suffix (&bot_str, &top_str);
  }

  if (bot_str < top_str /* tout n'est pas traite' */
      && process_prefix) {
    /* is_process_prefix_finished == TRUE <=> La chaine a ete traitee en entier ou on n'a pas extrait de prefixe */
    while (!is_process_prefix_finished) {
      /* Traitement iteratif et non recursif */
      /* le noyau est-il une ff ? */
      id = dico_seek (dico_des_mots, bot_str, top_str-bot_str);

      if (id != 0) {
	/* ... oui */
	/* on le stocke */
	push_string (prefix_stack_ptr, bot_str, top_str);
	light_cost += 1; /* A voir */
	bot_str = top_str;
	is_process_prefix_finished = TRUE;
      }
      else {
	cur_str = bot_str;
	cur_lgth = top_str-bot_str;

	action2 = 0; /* ce n'est pas une correction qui a donne le prefixe */
	
	if (action = sxdico_get_bounded (dico_des_prefixes, &cur_str, &cur_lgth, PREFIX_BOUND))
	  pref_lgth = cur_str-bot_str;

	left_pos = ((action) ? cur_str : bot_str) - mot_courant;

	/* On recherche le tiret le + a droite  */
	/* hyphen_set contient les emplacements de tous les tirets */
	cur_pos = top_str-mot_courant;

	while ((cur_pos = sxba_1_rlscan (hyphen_set, cur_pos)) > left_pos) {
	  /* y'a un tiret en cur_pos et le prefixe est de longueur au moins 1 */
	  cur_str2 = bot_str;
	  cur_lgth2 = cur_pos - (bot_str-mot_courant);
	  id = dico_seek (dico_des_mots, cur_str2, cur_lgth2);
	  
	  if (id) {
	    /* On detache car ce ... */
	    /* ... n'est pas un vrai prefixe */
	    push_string (prefix_stack_ptr, bot_str, mot_courant+cur_pos);
	    /* On saute le tiret car il ne peut pas faire partie integrante d'un suffixe ... */
	    light_cost += 1; /* A voir */
	    bot_str = mot_courant+cur_pos+1;
	    /* ... et on recommence */
	    break;
	  }

	  if (is_light_correction && id == 0 && action == 0) {
	    /* Ce prefixe ne peut pas se detacher directement */
	    /* On tente une correction light seulement s'il n'y a plus de tiret a gauche */
	    if (sxba_1_rlscan (hyphen_set, cur_pos) <= left_pos) {
	      /* Pour l'instant je ne mets pas le tiret ds le mot a corriger.  Il est possible de faire les
		 2 tentatives */
	      cur_str = mot_courant+cur_pos;

#if 0
	      /* Le 26/01/05 je ne vois plus pourquoi in fait c,a ... */
	      if (is_a_light_spelling_correction (bot_str, cur_str-bot_str)) {
		id = SXSPELL_x2id (light_FSA, 1);

		if (!SXSPELL_has_subwords (id)) {
		  /* La meilleure correction est une correction simple */
		  cur_str2 = SXSPELL_id2str (light_FSA, id);
		  cur_lgth2 = strlen (cur_str2);
		  action2 = sxdico_get_bounded (dico_des_prefixes, &cur_str2, &cur_lgth2, PREFIX_BOUND);
		}
		else
		  action2 = 0;

		if (action2 == 0) {
		  /* Le mot corrige n'est pas un prefixe => on le detache */
		  store_a_light_spelling_correction (prefix_stack_ptr);

		  if (print_error)
		    print_a_light_spelling_correction (bot_str, cur_str-bot_str);

		  /* On saute le tiret car il ne peut pas faire partie integrante d'un suffixe ... */
		  bot_str = mot_courant+cur_pos+1;
		  /* ... et on recommence */
		  id = 1;
		  break;
		}

		/* Ici une correction legere a donne' un prefixe, on se remet ds les conditions usuelles ... */
		cur_str2 = SXSPELL_id2str (light_FSA, id);
		pref_lgth = strlen (cur_str2);
		cur_lgth = top_str-cur_str;
		id = 0;
		action = action2;
	      }
#endif /* 0 */

	      if (call_a_light_spelling_correction (prefix_stack_ptr, bot_str, cur_str-bot_str)) {
		/* On saute le tiret car il ne peut pas faire partie integrante d'un suffixe ... */
		bot_str = mot_courant+cur_pos+1;
		/* ... et on recommence */
		id = 1;
		break;
	      }
	    }
	  }
	}

	if (id == 0) {
	  /* on n'a rien pu detacher ... */
	  if (action) {
	    /* ... mais y'a un prefixe et un noyau qui peut etre vide */
	    done = FALSE;
	    pushed = FALSE;

	    switch (action) {
	    case 1:
	      /* Le prefixe est du type "anti-" */
	      /* Si le suffixe commence par une majuscule, on n'y touche pas : cas "anti-Seattle" */
	      if (SXBA_bit_is_set (MAJ_set, ((unsigned char) *cur_str))) {

		/* on stocke le prefixe ... */
		if (action2 == 0)
		  push_string (prefix_stack_ptr, bot_str, cur_str);
		else
		  push_string (prefix_stack_ptr, cur_str2, cur_str2+pref_lgth);

		/* ... avec son separateur */
		varstr_catenate (prefix_stack_ptr->vstrs [prefix_stack_ptr->top], action2prefix_chars [action]);
		/* ... et Seattle */
		push_string (prefix_stack_ptr, bot_str, top_str);
	  
		light_cost += 1; /* A voir */
		bot_str = top_str;
		done = TRUE;
	      }

	      break;

	    case 2:
	      /* Le prefixe est du type "anti" */
	      /* On regarde si en ajoutant un tiret le mot "anti-suffixe:" est une forme flechie.
		 Si oui, on va privilegier ce cas */
	      wstr = varstr_raz (wstr);

	      while (wstr->current+(pref_lgth+cur_lgth)+1 >= wstr->last)
		wstr = varstr_realloc (wstr);

	      if (action2 == 0)
		varstr_lcatenate (wstr, bot_str, pref_lgth);
	      else
		varstr_lcatenate (wstr, cur_str2, pref_lgth);

	      varstr_catchar (wstr, '-');
	      varstr_lcatenate (wstr, cur_str, cur_lgth);
	
	      cur_wstr = varstr_tostr (wstr);
	      cur_lgth2 = varstr_length (wstr);

	      id = dico_seek (dico_des_mots, cur_wstr, cur_lgth2);

	      if (id) {
		/* oui */
		push_string (prefix_stack_ptr, cur_wstr, cur_wstr+cur_lgth2);
		light_cost += 1; /* A voir */
		bot_str = top_str;
		done = TRUE;
	      }

	      break;

	    case 3:
	      /*  Le prefixe est du type "entr'", on supprime l'apos que l'on remplace par un e */
	      if (action2 == 0)
		push_string (prefix_stack_ptr, bot_str, cur_str-1);
	      else
		push_string (prefix_stack_ptr, cur_str2, cur_str2+pref_lgth-1);

	      light_cost += 1; /* A voir */
	      pushed = TRUE;
	      break;

	    case 4:
	      break;

	    case 5:
	      /* Le prefixe est du type "d�s" */
	      /* On ne le valide que s'il est suivi par une voyelle */
	      if (!SXBA_bit_is_set (vowell_set, ((unsigned char) *cur_str)))
		done = TRUE;

	      break;

	    case 6:
	      /* Le prefixe est du type "im" */
	      /* On ne le valide que s'il est suivi par "mpb" */
	      if (!SXBA_bit_is_set (mpb_set, ((unsigned char) *cur_str)))
		done = TRUE;

	      break;
	    }
	
	    if (!done ) {
	      /* meme si le suffixe n'est pas bon, on stocke le prefixe ... */
	      if (!pushed) {
		if (action2 == 0)
		  push_string (prefix_stack_ptr, bot_str, cur_str);
		else
		  push_string (prefix_stack_ptr, cur_str2, cur_str2+pref_lgth);
	  
		light_cost += 1; /* A voir */
	      }
	      
	      /* ... avec son separateur */
	      varstr_catenate (prefix_stack_ptr->vstrs [prefix_stack_ptr->top], action2prefix_chars [action]);

	      bot_str = cur_str;
	    }
	    else {
	      is_process_prefix_finished = TRUE;
	    }
	  }
	  else
	    is_process_prefix_finished = TRUE;
	}
      }
    }
  }

  /* Ici on a traite les suffixes et les prefixes au maximum */
  if (is_light_correction && bot_str < top_str) {
    /* Noyau non vide ... */
    /* ... on essaye de le corriger ... */
    /* ... avec un poids faible */
    if (call_a_light_spelling_correction (prefix_stack_ptr, bot_str, top_str-bot_str)) {
      if (print_error)
	print_a_light_spelling_correction (bot_str, top_str-bot_str);

      bot_str = top_str;
    }
  }

  *str_lptr = bot_str;
  *str_rptr = top_str;

  return bot_str == top_str;
}


#if 0
/* Essai d'une nelle version le 25/01/05 */
/* La [sous-]chaine (str_lptr, str_rptr) n'est pas une forme flechie
   on la decoupe (recursivement) sur des tirets ou des apostrophes */
static BOOLEAN
call_cut_on_quote_hyphen (str_lptr, str_rptr, has_hyphen_in_prefix, is_first_call)
     char    **str_lptr, **str_rptr;
     BOOLEAN has_hyphen_in_prefix, is_first_call;
{
  int  pref_id, suf_id, cur_lgth, action;
  char *cur_str, *bot_str, *store_cur_str, car, *top_str, *work_str;

  bot_str = cur_str = *str_lptr;
  top_str = *str_rptr;

  if (is_cut_on_hyphen && (*bot_str == '-')) {
    /* On saute le '-' du debut */
    has_hyphen_in_prefix = TRUE;
    cur_str++;
  }
    
  while (cur_str < top_str) {
    car = *cur_str++;

    if ((is_cut_on_quote && (car == '\'')) || (is_cut_on_hyphen && (car == '-'))) {
      /* Si le prefixe et le suffixe sont ds le dico => OK */
      if (top_str-cur_str > 0) {
	/* suffixe non vide */
	if (car == '-')
	  /* '-' fait partie du suffixe */
	  cur_str--;
	  
	pref_id = dico_seek (dico_des_mots, bot_str, cur_str-bot_str);

#if 0
	/* Le passage en minuscule de la lettre capitale commencant une phrase a deja ete faite ... */
	/* ... on reactivera ce passage si on en veut + !! */
	if (is_process_lower && pref_id == 0) {
	  /* On regarde si le passage en minuscule de ce prefixe est ds le dico */
	  if (string2lower (wstr, bot_str, cur_str-bot_str)) {
	    /* Le prefixe a des majuscules */
	    pref_id = dico_seek (dico_des_mots, varstr_tostr (wstr), varstr_length (wstr));
	  }
	}
#endif /* 0 */

	/* petite verrue ... */
	if (is_process_lower && (car == '\'') && (SXBA_bit_is_set (MAJ_set, *cur_str))) {
	  /* Majuscule apres une apostrophe => on arre^te si is_process_lower */
	  if (pref_id) {
	    /* J'y comprends + rien !! */
	    push_string (suffix_stack_ptr, bot_str, top_str);
	    top_str = bot_str;

	    break;
	  }
	}

	/* ... et le suffixe */
	store_cur_str = cur_str;

	suf_id = dico_seek (dico_des_mots, cur_str, top_str-cur_str);

	if (suf_id) {
	  /* ... et le suffixe est une ff */
	  push_string (suffix_stack_ptr, cur_str, top_str);
	}
	else {
	  /* ... sinon on regarde recursivement si c'en est une */
	  suf_id = call_cut_on_quote_hyphen (&cur_str, &top_str, has_hyphen_in_prefix, FALSE);

#if 0
	  /* De'place' ds le traitement de call_process_prefix */
	  if (suf_id == 0) {
	    /* Le detachement complet du suffixe a echoue', mais il peut quand meme y avoir un detachement partiel */
	    if (store_cur_str != cur_str)
	      sxtrap (ME, "call_cut_on_quote_hyphen");

	    if (car == '-') {
	      /* On regarde si "pref-" est un prefixe */
	      work_str = bot_str;
	      cur_lgth = cur_str-bot_str+1;
	      action = sxdico_get_bounded (dico_des_prefixes, &work_str, &cur_lgth, PREFIX_BOUND);

	      if (action == 0) {
		/* le prefixe avec "-" n'est pas dans le dico des prefixes */
		/* On regarde si le suffixe (non detache'), prive' de "-", est une ff */
		/* On essaie en sautant le "-" commencant la sous-chaine */
		/* traitement de sc�nario-catastrophe */
		/* "-catastrophe" n'est pas ds le dico */
		/* On essaie "catastrophe" */
		suf_id = dico_seek (dico_des_mots, cur_str+1, top_str-cur_str-1);

		if (suf_id)
		  /* Doit-on verifier que "sc�nario" est ds le dico ?? */
		  push_string (suffix_stack_ptr, cur_str+1, top_str);
	      }
	    }
	  }
#endif /* 0 */
	}

	if (suf_id) {
	  /* Le suffixe a marche' et a ete stocke' */
	  top_str = store_cur_str;

	  if (pref_id != 0) {
	    push_string (suffix_stack_ptr, bot_str, top_str);
	    top_str = bot_str;
	  }
	}
	else {
	  /* Si le suffixe n'a pas marche' ... */
	  /* ... On ne stocke le prefixe que s'il a marche' ... */
	  if (pref_id != 0) {
	    /* ... et , s'il n'y a pas eu de tirets et si on est sur une apos */
	    if (!has_hyphen_in_prefix && (car == '\'')) {
	      push_string (prefix_stack_ptr, bot_str, store_cur_str);
	      bot_str = store_cur_str;
	    }
	    else {
	      /* ... ou si on peut faire une light correction sur le suffixe */
	      if (is_light_correction) {
		if (call_a_light_spelling_correction (suffix_stack_ptr, cur_str, top_str-cur_str)) {
		  if (print_error)
		    print_a_light_spelling_correction (cur_str, top_str-cur_str);

		  top_str = cur_str;
	      
		  push_string (suffix_stack_ptr, bot_str, cur_str);
		  top_str = bot_str;
		}
	      }
	    }
	  }
	}
      }

      /* Ce sont les appels recursifs qui vont chercher les '-' et les '\'' sur la droite */
      break;
    }
  }

  *str_lptr = bot_str;
  *str_rptr = top_str;

  return bot_str == top_str;
}
#endif /* 0 */

/* La [sous-]chaine (str_lptr, str_rptr) n'est pas une forme flechie
   on la decoupe (recursivement) sur des tirets ou des apostrophes */
static BOOLEAN
call_cut_on_quote_hyphen (str_lptr, str_rptr, has_hyphen_in_prefix, is_pref_OK)
     char    **str_lptr, **str_rptr;
     BOOLEAN has_hyphen_in_prefix, is_pref_OK;
{
  int  pref_id, suf_id, cur_lgth, action;
  char *cur_str, *bot_str, car, *top_str, *work_str;

  bot_str = cur_str = *str_lptr;
  top_str = *str_rptr;

  if (is_cut_on_hyphen && (*bot_str == '-')) {
    /* On saute le '-' du debut */
    has_hyphen_in_prefix = TRUE;
    bot_str = ++cur_str;

    if (dico_seek (dico_des_mots, bot_str, top_str-bot_str)) {
      push_string (suffix_stack_ptr, bot_str, top_str);
      *str_lptr = *str_rptr = bot_str;
      light_cost += 1; /* A voir */
      return TRUE;
    }
  }
    
  while (cur_str < top_str) {
    car = *cur_str++;

    if ((is_cut_on_quote && (car == '\'')) || (is_cut_on_hyphen && (car == '-'))) {
      /* Si le prefixe et le suffixe sont ds le dico => OK */
      if (top_str-cur_str > 0) {
	/* suffixe non vide */
	if (car == '-')
	  /* '-' fait partie du suffixe */
	  cur_str--;
	  
	pref_id = dico_seek (dico_des_mots, bot_str, cur_str-bot_str);
	  
	if (pref_id && is_pref_OK) {
	  /* Ds cette version, on stocke le prefixe ds tous les cas !! */
	  push_string (prefix_stack_ptr, bot_str, cur_str);
	  bot_str = cur_str;
	  light_cost += 1; /* A voir */
	}
	else
	  is_pref_OK = FALSE;

	/* petite verrue ... */
	if (is_process_lower && (car == '\'') && (SXBA_bit_is_set (MAJ_set, *cur_str))) {
	  /* Majuscule apres une apostrophe => on arre^te si is_process_lower */
	  if (pref_id) {
	    break;
	  }
	}

	/* ... et le suffixe */
	suf_id = dico_seek (dico_des_mots, cur_str, top_str-cur_str);

	if (suf_id) {
	  /* ... et le suffixe est une ff */
	  push_string (suffix_stack_ptr, cur_str, top_str);
	  top_str = cur_str;
	  light_cost += 1; /* A voir */
	}
	else {
	  /* ... sinon on regarde recursivement si c'en est une */
	  suf_id = call_cut_on_quote_hyphen (&cur_str, &top_str, has_hyphen_in_prefix, is_pref_OK);
	  /* ds tous les cas top_str est correct !! */

	  if (is_pref_OK)
	    bot_str = cur_str;
	}
      }

      /* Ce sont les appels recursifs qui vont chercher les '-' et les '\'' sur la droite */
      break;
    }
  }

  *str_lptr = bot_str;
  *str_rptr = top_str;

  return bot_str == top_str;
}


static int
call_spell (mot, lgth, is_first_word/* , is_followed_by_a_nl */)
     char    *mot;
     int     lgth;
     BOOLEAN is_first_word;
     /* BOOLEAN is_followed_by_a_nl; */
{
  int     cur_lgth, x, id, y, *bot_ptr, *top_ptr, action, prev_action, pre_lgth, suf_lgth, i;
  int     delta_vstr_mot_en_min, longueur_de_mot;
  char    *cur_str, car, cur_car, *cur_str_mot_en_min, *top_str;
  BOOLEAN has_changed;
  char    *mot_courant;

  MOT_A_CORRIGER = mot_courant = mot;
  longueur_de_mot = lgth;

  id = dico_seek (dico_des_mots, mot_courant, lgth);

  if (id)
    push_string (prefix_stack_ptr, mot_courant, mot_courant+lgth);

  if (id == 0 && is_light_correction && call_a_Xlight_spelling_correction (prefix_stack_ptr, mot_courant, lgth)) {
    id = 1;

    if (print_error)
      print_a_Xlight_spelling_correction (mot_courant, lgth);
  }

#if 0
  if (id == 0 && (check_lower || is_process_lower))
    /* On passe le mot_courant en minuscules */
    has_changed = string2lower (vstr_mot_en_min, mot_courant, lgth);

  if (id == 0 && check_lower) {
    /* On regarde si le mot_courant en minuscule existe */
    if (has_changed) {
      /* le mot_courant a des majuscules */
      /* On commence par regarder si le mot_courant est une forme flechie */
      cur_str_mot_en_min = varstr_tostr (vstr_mot_en_min);

      id = dico_seek (dico_des_mots, cur_str_mot_en_min, lgth);

      if (id) {
	if (*mot != sxtolower (*mot)) {
	  *cur_str_mot_en_min = *mot;
	  push_string (prefix_stack_ptr, cur_str_mot_en_min, cur_str_mot_en_min+lgth);
	  light_cost += 1; /* A voir */
	}
	else
	  id = 0;
      }
    }
  }
#endif /* 0 */

  if (id == 0 && (is_cut_on_quote || is_cut_on_hyphen)) {
    top_str = mot_courant + lgth;
    cur_str = mot_courant;

    if (call_cut_on_quote_hyphen (&cur_str, &top_str, FALSE /* has_hyphen_in_prefix */, TRUE /* is_first_call */)) {
      id = 1;
    }
    else {
      /* On continue sur le noyau */
      delta_vstr_mot_en_min = cur_str-mot_courant;
      mot_courant = cur_str;
      lgth = top_str-cur_str;
    }
  }

#if 0
  if (id == 0 && is_process_lower) {
    car = sxtolower (*mot_courant);

    if (car != *mot_courant) {
      /* On ne corrige que les mots qui commencent par des minuscules ... */
      /* ... ou les mots qui 
	 - commencent par une majuscule
	 - sont en debut de phrase (is_first_word)
	 - ne contiennent pas d'autres majuscules (sauf apres les tirets et apostrophes)
	 - et contiennent des tirets ou des apostrophes
      */
      if (is_first_word) {
	/* debut de phrase et a une majuscule */
	cur_str = mot_courant+1;
	top_str = mot_courant + lgth;
	has_changed = FALSE;

	while (cur_str < top_str) {
	  car = *cur_str++;

	  if (car != sxtolower (car)) {
	    /* 2 majuscules */
	    break;
	  }

	  if (car == '\'' || car == '-') {
	    has_changed = TRUE;

	    if (cur_str < top_str) {
	      car = *cur_str;

	      if (car != sxtolower (car))
		/* majuscule */
		cur_str++;
	    }
	  }
	}

	if (has_changed && (cur_str == top_str)) {
	  /* On va tenter une correction ... */
	  /* sur le mot en minuscules */
	  mot_courant = varstr_tostr (vstr_mot_en_min)+delta_vstr_mot_en_min;
	}
	else
	  id = 1;
      }
      else
	id = 1;

      if (id) {
	push_string (prefix_stack_ptr, mot_courant, mot_courant+lgth);
	light_cost += 1; /* A voir */
      }
    }
  }
#endif /* 0 */
  
#if 0
  /* remonte' le 25/01/05 */
  if (id == 0 && (is_cut_on_quote || is_cut_on_hyphen)) {
    top_str = mot_courant + lgth;
    cur_str = mot_courant;

    if (call_cut_on_quote_hyphen (&cur_str, &top_str, FALSE /* has_hyphen_in_prefix */, TRUE /* is_first_call */)) {
      id = 1;
    }
    else {
      /* On continue sur le noyau */
      mot_courant = cur_str;
      lgth = top_str-cur_str;
    }
  }
#endif /* 0 */

  if (id == 0 && is_light_correction) {
    /* ... une petite correction light ... */
    if (call_a_light_spelling_correction (prefix_stack_ptr, mot_courant, lgth)) {
      id = 1;

      if (print_error)
	print_a_light_spelling_correction ( mot_courant, lgth);
    }
  }

  /* On regarde si le mot a des prefixes et des suffixes remarquables */
  if (id == 0 && process_prefix) {
    top_str = mot_courant + lgth;
    cur_str = mot_courant;

    if (call_process_prefix (&cur_str, &top_str))
      id = 1;
    else {
      /* On ignore les prefixes suffixes detaches et on repart sur mot */
      RAZ_VSTR_STACK (*prefix_stack_ptr);
      RAZ_VSTR_STACK (*suffix_stack_ptr);
      mot_courant = mot;
      lgth = longueur_de_mot;
      light_cost = 0;
    }
  }
  
  if (id == 0) {
    /* Toutes les bricoles ont echouees, on lance la vraie correction sur le noyau */
    /* ... en 2 temps */
    /* On essaie d'abord une correction light */
    if (is_light_correction && call_a_light_spelling_correction (prefix_stack_ptr, mot_courant, lgth)) {
      if (print_error)
	print_a_light_spelling_correction (mot_courant, lgth);
    }
    else {
      /* On fabrique l'automate initial du mot ... */
      is_a_spelling_correction.yes = TRUE;
      is_a_spelling_correction.word = mot_courant;
      is_a_spelling_correction.lgth = lgth;

      if (is_spelling_correction && call_sxspell (mot_courant, lgth))
	id = 1;

      if (print_error) {
	fputs ("\n", stdout);
	  
	cur_car = mot_courant [lgth];
	mot_courant [lgth] = NUL;

	if (print_input) {
	  if (print_comment && varstr_length (vstr))
	    printf ("%s ", varstr_tostr (vstr));

	  printf ("[%s] ", mot_courant);
	}
	
	if (id == 0) {
	  if (print_comment && varstr_length (vstr))
	    printf ("%s ", varstr_tostr (vstr));

	  if (is_tokenizer) {
	    if (*mot_courant == sxtolower (*mot_courant))
	      /* commence par une minuscule */
	      fputs ("_uw", stdout);
	    else
	      fputs ("_Uw", stdout);
	  }
	  else
	    printf ("<<<%s>>>", mot_courant);
	}
	else {
	  print_spelling_correction (&main_FSA);
	}

	mot_courant [lgth] = cur_car;
      }
      else {
	if (print_output && !is_dry_run && id != 0) {
	  int heavy_id, *bot_ptr, *top_ptr;

	  /* On extrait de main_FSA la meilleure correction */
	  /* On ne prend que le 1er resultat parmi les meilleurs */
	  heavy_id = SXSPELL_x2id (main_FSA, 1);
	  heavy_cost = SXSPELL_x2weight (main_FSA, 1);

	  varstr_raz (cvstr);
  
	  if (!SXSPELL_has_subwords (heavy_id)) {
	    if (print_comment && varstr_length (vstr))
	      varstr_catenate (cvstr, varstr_tostr (cur_correction->vstr));

	    varstr_catenate (varstr_catenate (cvstr, SXSPELL_id2str (main_FSA, heavy_id)), " ");
	  }
	  else {
	    /* cas sous-mots */
	    SXSPELL_subword_foreach (main_FSA, heavy_id, bot_ptr, top_ptr) {
	      if (print_comment && varstr_length (vstr))
		varstr_catenate (cvstr, varstr_tostr (cur_correction->vstr));

	      varstr_catenate (varstr_catenate (cvstr,
						SXSPELL_id2str (main_FSA, SXSPELL_x2subword_id (bot_ptr))),
			       " ");
	    }
	  }
	}
      }
    }
  }

  return id;
}


/* str = "000<F 111>222</F>333" */

/*  On retourne "333" */ 
static char *
xml_rule (str)
     char * str;
{
  char *cur;

  cur = str;

  if (cur) cur = strchr (cur, (int)'<');
  if (cur) cur = strchr (cur, (int)'>');
  if (cur) cur = strchr (cur, (int)'<');
  if (cur) cur = strchr (cur, (int)'>');
  if (cur) cur++;

  return cur;
}

/*  On retourne "222</F>333" */ 
static char *
xml_body (str)
     char * str;
{
  char *cur;

  cur = str;

  if (cur) cur = strchr (cur, (int)'<');
  if (cur) cur = strchr (cur, (int)'>');
  if (cur) cur++;

  return cur;
}

/* Cas special { <...>disp<...> <...>aru<...> } _ETR 
   mot         ^
   mot+lgth                                    ^
   On essaie de corriger "disp aru"
*/
static int
call_spell_ETR (mot, lgth, is_first_word)
     char    *mot;
     int     lgth;
     BOOLEAN is_first_word;
{
  int  id, ste, length;
  char *pstr, *qstr, cur_char;
  
  /* On extrait "disp" "aru" ... */
  pstr = mot;
  id = 0;

  if (pstr = xml_body (mot)) {
    /* { <...>disp<...> <...>aru<...> } _ETR 
       pstr   ^
    */
    
    cur_char = sxtolower (*pstr);

    if (cur_char != *pstr)
      /* Si "Disp", on ne fait rien ... */
      return 0;

    varstr_raz (wstr);

    if (qstr = strchr (pstr, (int)'<')) {
      /* { <...>disp<...> <...>aru<...> } _ETR 
	 qstr       ^
      */
      varstr_lcatenate (wstr, pstr, qstr-pstr);

      while (pstr = xml_body (qstr+1)) {
	/* { <...>disp<...> <...>aru<...> } _ETR 
	   pstr                  ^
	*/
	if (qstr = strchr (pstr, (int)'<')) {
	  /* { <...>disp<...> <...>aru<...> } _ETR 
	     qstr                     ^
	  */
	  /* Pour l'instant, je ne met pas de blanc entre "disp" et "aru" !! */
	  varstr_lcatenate (wstr, pstr, qstr-pstr);
	}
	else {
	  /* !! */
	  varstr_raz (wstr);
	  break;
	}
      }
    }

    if (length = varstr_length (wstr)) {
      ste = sxstrsave (varstr_tostr (wstr));
      id = call_spell (sxstrget (ste), length, is_first_word);
    }
  }
  else {
    /* On suppose qu'on est ds le cas {disp aru} */
    if (lgth-2 > 0)
      id = call_spell (mot+1, lgth-2, is_first_word);
  }

  if (id == 0)
    is_a_spelling_correction.yes = FALSE; /* On reinitialise */

  return id;
}



static void
string2set (string, set)
     char *string;
     SXBA          set;
{
  unsigned char uchar;

  while (uchar = *string++)
    SXBA_1_bit (set, uchar);
}


static int
output (correction)
     struct correction_struct *correction;
{
  int i;
  char cur_car;

  /* On ecrit tout ... */
  if (print_input || print_output) {
    if (correction->is_first_word)
      fputs ("\n", stdout);
  }

  if (print_input) {
    if (print_comment && varstr_length (correction->vstr))
      printf ("%s ", varstr_tostr (correction->vstr));

    printf ("%s ", correction->word);
  }

  if (print_output) {
    if (is_dry_run) {
      if (print_comment && varstr_length (correction->vstr))
	printf ("%s ", varstr_tostr (correction->vstr));

      if (correction->id == 0)
	printf ("<<<%s>>> ", correction->word);
      else
	printf ("%s ", correction->word);
    }
    else {
      for (i = 1; i <= correction->prefix_stack.top; i++) {
	if (print_comment && varstr_length (correction->vstr))
	  printf ("%s ", varstr_tostr (correction->vstr));

	printf ("%s ", varstr_tostr (correction->prefix_stack.vstrs [i]));
      }
    
      if (correction->is_a_spelling_correction.yes) {
	if (correction->id == 0) {
	  if (print_comment && varstr_length (correction->vstr))
	    printf ("%s ", varstr_tostr (correction->vstr));

	  cur_car = correction->is_a_spelling_correction.word [correction->is_a_spelling_correction.lgth];
	  correction->is_a_spelling_correction.word [correction->is_a_spelling_correction.lgth] = NUL;

	  if (is_tokenizer) {
	    if (*correction->is_a_spelling_correction.word == sxtolower (*correction->is_a_spelling_correction.word))
	      /* commence par une minuscule */
	      fputs ("_uw", stdout);
	    else
	      fputs ("_Uw", stdout);
	  }
	  else
	    printf ("<<<%s>>> ", correction->is_a_spelling_correction.word);

	  correction->is_a_spelling_correction.word [correction->is_a_spelling_correction.lgth] = cur_car;
	}
	else {
	  printf ("%s", varstr_tostr (correction->cvstr));
	}
      }

      for (i = correction->suffix_stack.top; i > 0; i--) {
	if (print_comment && varstr_length (correction->vstr))
	  printf ("%s ", varstr_tostr (correction->vstr));

	printf ("%s ", varstr_tostr (correction->suffix_stack.vstrs [i]));
      }
    }
  }
}



#if 0
#define has_capital(c) (string2lower (wstr, c->word, strlen (c->word)))

/* On regarde si une des corrections commence par une majuscule */
static BOOLEAN
is_capital_initial (correction)
     struct correction_struct *correction;
{
  int i;
  char car1;

  string2lower (wstr, correction->word, strlen (correction->word));

  for (i = 1; i <= correction->prefix_stack.top; i++) {
    car1 = varstr_tostr (correction->prefix_stack.vstrs [i]) [0];

    if (car1 != sxtolower (car1))
      return TRUE;
  }

  if (correction->is_a_spelling_correction.yes) {
    /* Correction globale */
    if (correction->id == 0)
      car1 = correction->is_a_spelling_correction.word [0];
    else
      car1 = varstr_tostr (correction->cvstr) [0];

    if (car1 != sxtolower (car1))
      return TRUE;
  }

  for (i = correction->suffix_stack.top; i > 0; i--) {
    car1 = varstr_tostr (correction->suffix_stack.vstrs [i]) [0];

    if (car1 != sxtolower (car1))
      return TRUE;
  }

  return FALSE;
}
#endif /* 0 */


int
sxspeller_action (what, action_no)
    int		what, action_no;
{
  struct sxtoken	*ptok;
  int                   top, new_top, cur, id, i, word_lgth;
  char                  *pcom, *tcom, *word, cur_car;
  static BOOLEAN        is_first_word, is_first_line;
  BOOLEAN               is_first;
  static char           Agrave [] = "�";

  switch (what) {
  case OPEN:
    break;

  case INIT:
    if (process_prefix) {
      /* On remplit qq ensembles ... */
      string2set (MAJ_string, MAJ_set);
      string2set (vowell_string, vowell_set);
      string2set (mpb_string, mpb_set);
    }

    for (which_correction = 0; which_correction <= 2; which_correction++) {
      cur_correction = corrections+which_correction;
      cur_correction->vstr = varstr_alloc (128);
      cur_correction->cvstr = varstr_alloc (128);
      alloc_vstr_stack_hd (&cur_correction->prefix_stack, 32);
      alloc_vstr_stack_hd (&cur_correction->suffix_stack, 32);
    }

    which_correction = 0;

    bivstr = varstr_alloc (128);
    wstr = varstr_alloc (128);
    vstr_mot_en_min = varstr_alloc (128);
    hyphen_set = sxba_calloc (128);

    if (is_light_correction) {
      sxspell_init (dico_des_mots,
		    &Xlight_FSA,
		    (unsigned char)1 /* BEST */, 
		    max_weight,
		    light_compose_weight, /* A FAIRE : un Xlight_compose_weight */
		    FALSE /* mots multiples : non */,
		    0/* ... de cout unitaire */);
    }

    if (is_light_correction) {
      sxspell_init (dico_des_mots,
		    &light_FSA,
		    (unsigned char)1 /* BEST */, 
		    max_weight,
		    light_compose_weight,
		    FALSE /* mots multiples : non */,
		    0/* ... de cout unitaire */);
    }

    /* Appel initial : on ne le fait qu'une fois */
    if (is_spelling_correction) {
      sxspell_init (dico_des_mots,
		    &main_FSA,
		    (unsigned char)1 /* BEST */, 
		    max_weight,
		    compose_weight,
		    is_sub_word /* mots multiples ? */,
		    sub_word_weight /* ... de cout unitaire */);
    }

    is_first_line = TRUE;
    prev_correction = NULL;

    break;

  case ACTION:
    switch (action_no) {
    case 0:
      return;

    case 1:
      /* <word>			= %WORD ; 1 */
      cur_correction = corrections+which_correction;

      ptok = &(SXSTACKtoken (SXSTACKtop ()));
      cur_correction->word = word = sxstrget (ptok->string_table_entry);
      word_lgth = strlen (word);
	  
      vstr = cur_correction->vstr;
      cvstr = cur_correction->cvstr;

      varstr_raz (vstr);
      /* vstr doit etre positionne' ds le cas print_error */

      if (pcom = ptok->comment) {
	is_first_word = (strchr (pcom, (int)'\n') != NULL);
	pcom = ptok->comment;

	if (pcom = strchr (pcom, (int)'{')) {
	  tcom = pcom;

	  if (tcom = strrchr (tcom, (int)'}')) {
	    /* from_right */
	    if (print_comment)
	      varstr_lcatenate (vstr, pcom, tcom-pcom+1);
	  }
	  else
	    pcom == NULL;
	}
	else {
	  pcom = NULL;
	}
      }
      else
	is_first_word = FALSE;

      if (is_first_line) {
	is_first_line = FALSE;
	is_first_word = TRUE;
      }

      RAZ_VSTR_STACK (cur_correction->prefix_stack);
      RAZ_VSTR_STACK (cur_correction->suffix_stack);
      is_a_spelling_correction.yes = FALSE;

      cur_correction->is_first_word = is_first_word;
      prefix_stack_ptr = &(cur_correction->prefix_stack);
      suffix_stack_ptr = &(cur_correction->suffix_stack);

      id = 0;
      light_cost = heavy_cost = 0;

      if (pcom && word_lgth >= 4 && strncmp (word, "_ETR", 4) == 0) {
	/* On est ds le cas special :
	   { <...>disp<...> <...>aru<...> } _ETR 
	   ou` on va essayer de corriger non pas _ETR mais "disp aru"
	   Si ca marche on retourne { <...>disp<...> <...>aru<...> } disparu
	   sinon { <...>disp<...> <...>aru<...> } _ETR
	   ds le cas dry_run, on retourne
	   { <...>disp<...> <...>aru<...> } _ETR ou
	   { <...>disp<...> <...>aru<...> } <<<_ETR>>>
	*/
	id = call_spell_ETR (pcom, tcom-pcom+1, is_first_word);
      }
      
      if (id == 0) {
#if 0
	/* Non, on fait ca ds le dagueur */
	/* petite verrue pour changer A en � ... */
	if (is_first_word && word_lgth == 1 && *word == 'A' &&
	    *sxstrget (sxget_token (sxplocals.atok_no)->string_table_entry) != '-') {
	  /* ... si le mot suivant commence par un tiret (Ex : A -t-il), on ne fait rien */
	    push_string (prefix_stack_ptr, &(Agrave [0]), &(Agrave [1]));
	}
	else
#endif /* 0 */
	  id = call_spell (word, word_lgth, is_first_word);
      }

      cur_correction->id = id;
      cur_correction->cost = light_cost+heavy_cost; /* l'un des 2 est au moins est nul */
      cur_correction->is_a_spelling_correction = is_a_spelling_correction;

      if (!print_error && id && *word == '_') {
	/* Cas des meta ff (qui marchent forcement) */
	if (prev_correction != NULL)
	  output (prev_correction);
	  
	output (cur_correction);

	prev_correction = NULL;
      }
      else {
	if (prev_correction == NULL) {
	  prev_correction = cur_correction;
	}
	else {
	  if (!print_error) {
	    int prev_cur_cost;

	    if (prev_correction->id != 0 && cur_correction->id != 0 &&
		(prev_cur_cost = (prev_correction->cost + cur_correction->cost)) == 0) {
	      /* precedent et courant OK */
	      output (prev_correction);
	      prev_correction = cur_correction;
	    }
	    else {
	      /* Au moins l'un des 2 a necessite' de la correction, on regarde ce que donne la concatenation des 2 */
#if 0
	      /* ... seulement si les 2 ne contiennent pas de majuscules */
	      if (has_capital (prev_correction) || has_capital (cur_correction)) {
		/* On sort le precedent, meme s'il ne commence pas par une maj */
		output (prev_correction);
		prev_correction = cur_correction;
	      }
	      /* else { */
#endif /* 0 */
	      which_correction++;

	      if (which_correction > 2)
		which_correction = 0;

	      bi_correction = corrections+which_correction;
	      /* Attention : prev_correction->word peut pointer vers bivstr */
	      /* tres laid */
	      if (prev_correction->word == bivstr->first)
		varstr_catenate (bivstr, cur_correction->word);
	      else {
		varstr_raz (bivstr);
		varstr_catenate (varstr_catenate (bivstr, prev_correction->word), cur_correction->word);
	      }

	      bi_correction->word = word = varstr_tostr (bivstr);
	      word_lgth = strlen (word);
	  
	      vstr = bi_correction->vstr;
	      cvstr = bi_correction->cvstr;

	      varstr_raz (vstr);

	      RAZ_VSTR_STACK (bi_correction->prefix_stack);
	      RAZ_VSTR_STACK (bi_correction->suffix_stack);
	      is_a_spelling_correction.yes = FALSE;

	      bi_correction->is_first_word = FALSE;
	      prefix_stack_ptr = &(bi_correction->prefix_stack);
	      suffix_stack_ptr = &(bi_correction->suffix_stack);

	      id = 0;
	      light_cost = heavy_cost = 0;

	      id = call_spell (word, word_lgth, prev_correction->is_first_word);
      
	      bi_correction->id = id;
	      bi_correction->cost = light_cost+heavy_cost; /* l'un des 2 est au moins est nul */
	      bi_correction->is_a_spelling_correction = is_a_spelling_correction;

	      if (id == 0 || prev_cur_cost <= bi_correction->cost) {
		output (prev_correction);
		prev_correction = cur_correction;

		if (which_correction == 0)
		  which_correction = 2;
		else
		  which_correction--;
	      }
	      else {
		/* On tente des concatenations multiples !! */
		/* Attention a bivstr */
		prev_correction = bi_correction;
	      }
	    }
	  }
	}
      }

      /* On incremente sur l'anneau des corrections */
      which_correction++;

      if (which_correction > 2)
	which_correction = 0;

      return;

    case 2:
      /* <axiome>		= <sxspeller> ; 2 */
      if (!print_error) {
	if (prev_correction != NULL)
	  output (prev_correction);

	fputs ("\n", stdout);
      }
      return;

    default:
      fputs ("The function \"sxspeller_action\" is out of date with respect to its specification.\n", sxstderr);
      abort ();
    }

    break;

  case ERROR:
    is_error = TRUE;
    break;

  case FINAL:
    for (which_correction = 0; which_correction <= 2; which_correction++) {
      cur_correction = corrections+which_correction;
      varstr_free (cur_correction->vstr), cur_correction->vstr = NULL;
      varstr_free (cur_correction->cvstr), cur_correction->cvstr = NULL;
      free_vstr_stack_hd (&cur_correction->prefix_stack, 32);
      free_vstr_stack_hd (&cur_correction->suffix_stack, 32);
    }

    varstr_free (bivstr), bivstr = NULL;
    varstr_free (wstr), wstr = NULL;
    varstr_free (vstr_mot_en_min), vstr_mot_en_min = NULL;

    sxfree (hyphen_set), hyphen_set = NULL;

    /* ... et on libere */
    if (is_spelling_correction) {
      sxspell_final_fsa (&main_FSA);
      sxspell_final (&main_FSA);
    }

    if (is_light_correction) {
      sxspell_final_fsa (&light_FSA);
      sxspell_final (&light_FSA);
    }

    if (is_light_correction) { /* A FAIRE : un is_Xlight_correction */
      sxspell_final_fsa (&Xlight_FSA);
      sxspell_final (&Xlight_FSA);
    }
    break;

  case CLOSE:
  case SEMPASS:
    break;

  default:
    fputs ("The function \"sxspeller_action\" is out of date with respect to its specification.\n", sxstderr);
    abort ();
  }

  return 0;
}


int
main (argc, argv)
    int		argc;
    char	*argv [];
{
  int	argnum, kind;

  if (sxstdout == NULL) {
    sxstdout = stdout;
  }

  if (sxstderr == NULL) {
    sxstderr = stderr;
  }

#ifdef BUG
  /* Suppress bufferisation, in order to have proper	 */
  /* messages when something goes wrong...		 */

  setbuf (stdout, NULL);
#endif /* BUG */

  /* valeurs par defaut */
  max_weight = UNBOUNDED; /* on regarde toutes les corrections */
  is_sub_word = FALSE; /* On ne cherche pas a decouper en sous_mots */
  sub_word_weight = 0; /* Poids de la correction pour un decoupage */

  /* Par defaut on suppose que l'on corrige un texte => */
  print_input = FALSE; /* On ne reecrit pas le source ... */
  print_output = TRUE; /* On ecrit le mot apres traitement ... */
  print_error = FALSE; /* ... ds tous les cas */
  print_comment = FALSE;
  check_lower = FALSE; /* On ne cherche pas le mot en minuscule */
  is_process_lower = FALSE;
  is_cut_on_quote = FALSE;
  is_cut_on_hyphen = FALSE;
  process_prefix = FALSE;
  process_suffix = TRUE;
  is_light_correction = FALSE; /* pas de correction "light" */
  light_weight = 0; 
  is_tokenizer = FALSE;
  is_spelling_correction = TRUE; /* Si tout a echoue', on tente une correction complete */
  is_dry_run = FALSE; /* TRUE => Un mot est ressorti tel quel s'il est possible de le corriger sinon il est sorti entre <<< et >>> */

  /* Decodage des options */
  for (argnum = 1; argnum < argc; argnum++) {
    switch (kind = option_get_kind (argv [argnum])) {
    case HELP:
      fprintf (sxstderr, Usage, ME);
      return;

    case WEIGHT:
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (WEIGHT));
	SXEXIT (3);
      }
	     
      max_weight = atoi (argv [argnum]);

      break;

    case SUB_WORD_WEIGHT:
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (SUB_WORD_WEIGHT));
	SXEXIT (3);
      }
	     
      is_sub_word = TRUE;
      sub_word_weight = atoi (argv [argnum]);

      break;

    case PRINT_INPUT:
      print_input = TRUE;
      break;

    case PRINT_OUTPUT:
      print_output = TRUE;
      break;

    case PRINT_ERROR:
      print_error = TRUE;
      break;

    case PRINT_COMMENT:
      print_comment = TRUE;
      break;

    case CHECK_LOWER:
      check_lower = TRUE;
      break;

    case PROCESS_LOWER:
      is_process_lower = TRUE;
      break;

    case CUT_ON_QUOTE:
      is_cut_on_quote = TRUE;
      break;

    case CUT_ON_HYPHEN:
      is_cut_on_hyphen = TRUE;
      break;

    case PROCESS_PREFIX:
      process_prefix = TRUE;
      break;

    case PROCESS_SUFFIX:
      process_suffix = TRUE;
      break;

    case LIGHT_WEIGHT:
      is_light_correction = TRUE;
      
      if (++argnum >= argc) {
	fprintf (sxstderr, "%s: a non negative integer value must follow the \"%s\" option;\n", ME, option_get_text (LIGHT_WEIGHT));
	SXEXIT (3);
      }
	     
      light_weight = atoi (argv [argnum]);
      break;
      break;

    case SPELLING_CORRECTION:
      is_spelling_correction = TRUE;
      break;

    case -SPELLING_CORRECTION:
      is_spelling_correction = FALSE;
      break;

    case DRY_RUN:
      is_dry_run = TRUE;
      break;

    case TOKENIZER:
      is_tokenizer = TRUE;
      print_input = FALSE;
      print_comment = TRUE;
      print_output = TRUE;
      print_error = FALSE;
      break;

    case UNKNOWN_ARG:
      if (argnum < argc-1) {
	fprintf (sxstderr, "%s: unknown option \"%s\";\n", ME, argv [argnum]);
	fprintf (sxstderr, Usage, ME);
	SXEXIT (3);
      }

      if (argv [argnum] [0] == '-' && strlen (argv [argnum]) > 1) {
	/* -blabla => erreur */
	fprintf (sxstderr, "%s: invalid source pathname \"%s\".\n", ME, argv [argnum]);
	SXEXIT (3);
      }

      break;
    }

    if (kind == UNKNOWN_ARG)
      break;
  }

  sxopentty ();
  sxstr_mngr (BEGIN) /* Creation de la table des chaines */ ;
  (*(sxtables.analyzers.parser)) (BEGIN) /* Allocation des variables globales du parser */ ;
  syntax (OPEN, &sxtables) /* Initialisation de SYNTAX (mono-langage) */ ;

  if (argnum == argc /* stdin (sans -) */ ||
      strcmp (argv [argnum], "-") == 0) {

    sxsrc_mngr (INIT, stdin, "");
    sxerr_mngr (BEGIN);
    syntax (ACTION, &sxtables);
    sxsrc_mngr (FINAL);
    sxerr_mngr (END);
    /* clearerr (stdin); si utilisation repetee de "stdin" */
  }
  else {
    /* argv [argnum] est le source */
    FILE	*infile;

    if ((infile = sxfopen (argv [argnum], "r")) == NULL) {
      fprintf (sxstderr, "%s: Cannot open (read) ", argv [argnum]);
      sxperror (argv [argnum]);
      sxerrmngr.nbmess [SEVERITIES - 1]++;
    }
    else {
      sxsrc_mngr (INIT, infile, argv [argnum]);
      sxerr_mngr (BEGIN);
      syntax (ACTION, &sxtables);
      sxsrc_mngr (FINAL);
      sxerr_mngr (END);
      fclose (infile);
    }
  }

  syntax (CLOSE, &sxtables);
  (*(sxtables.analyzers.parser)) (END) /* Inutile puisque le process va etre termine */ ;
  sxstr_mngr (END) /* Inutile puisque le process va etre termine */ ;

  {
    register int	severity;

    for (severity = SEVERITIES - 1; severity > 0 && sxerrmngr.nbmess [severity] == 0; severity--) {
      /* null; */
    }

    SXEXIT (severity);
  }
}


VOID	sxvoid ()
/* procedure ne faisant rien */
{
}

BOOLEAN	sxbvoid ()
/* procedure ne faisant rien */
{
    return 0;
}
