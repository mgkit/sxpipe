#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;
use Encode;
#------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------
# CHARGEMENT de la base de données et des fichiers qu'on veut utiliser et stockage des données
#------------------------------------------------------------------------------------------------------
$|=1; 
my $lang = 'fr';
my $dir_Lefff;
while (1) {
$_=shift;
if (!$_ || /^$/) {last;}
elsif (/^-d$/) {$dir_Lefff=shift;}
elsif (/^-l$/) {$lang=shift;}
	elsif (/^-(-)?h(elp)?$/) {
		print "Le script corr_morphoFlex a pour but corriger les fautes flexionnelle d'un texte et de les annoter comme tel. Pour bien fonctionner il nécessite les options suivantes:
\t-l\tpermet de selectionner la langue voulu (optionnel: par défaut la langue selectionnée sera le français)
\t-ad\tcorrespond au répertoire dans lequel est stocké le dictionnaire de référence(Lefff)\n";
}
}

die "### ERROR: corr_morphoFlex.pl requires using option -d followed by the folder that contains $lang.mdb" if ($dir_Lefff eq "");


# On ouvre dico Lefff
my $dir="/home/marion/Bureau/";
my $lefff_dbh = DBI->connect("dbi:SQLite:$dir_Lefff/$lang.mdb", "", "", {RaiseError => 1, AutoCommit => 1});
$lefff_dbh->{sqlite_unicode}=1;
my $lefff_getall= $lefff_dbh->prepare('SELECT DISTINCT wordform FROM data WHERE lemma=? AND tag=?;');

#------------------------------------------------------------------------------------------------------

while(<>){
# formattage
chomp;
if (/ ?(_XML|_MS_ANNOTATION|_PAR_BOUND)/) {
		print "$_\n";
		next;
}   
	s/^\s*/ /o;
	s/\s$/ /o;
	#print "$_\n\n";

	s/ ?([^\\])\(([^<])/$1 \( $2/go;
	s/ ?([^\\])\)([^<])/$1 \)$2/go;
	s/([^\\])\)/$1 \)/g;
	s/Ana \(/Ana\(/go;
	s/ ,Ana/,Ana/go;	
	s/ ,fr/,fr/go;

my $tmp = $_;
#tant qu'il y a des néo
#while ($tmp =~ s/(\( *({[^\}]*\}) (_INC_[^ ]+?|_NEO|[^ ]+?)( *\| *[^\}]*\} (_INC_[^ ]+?|_NEO|[^ ]+?) +\[\|[^\{\}]*?\|\])*( *\| *[^\}]*\} _NEO +\[\|[^\{]*Ana[^\{]*?\|\])+( *\| *[^\}]*\} (_INC_[^ ]+?|_NEO|[^ ]+?) +\[\|[^\{\}]*?\|\])* *[\)\|])//){
while ($tmp =~ s/([\(|\|] *({[^\}]*\}) _INC_[^ ]+( *\| *[^\}]*\} (_INC_[^ ]+|\_[A-Z]+|_NEO) +(\[\|[^\{\}]*?\|\])?)* *[\)|\|]?)//){
	my $tmpcombinaisonAmb = $1;
	my $combinaisonAmb = quotemeta($tmpcombinaisonAmb);
	my $motSxPipe = $2;
	next if ($tmpcombinaisonAmb !~ m/Ana/);
	#print "\n→→→→1 $motSxPipe → $tmp\n";
		 
 	#on voit si on veut garder la règle
 	my $typeBestRegle="D";
 	my $scoreBestRegle=0;
 	my ($bestpos, $bestlemme)="";
 	#print "2 $tmpcombinaisonAmb \n";
 	while ($tmpcombinaisonAmb =~ s/_NEO (\[[^\]]*?Ana\( *([FD])\/([0-9]+)\/#([^#]+)#\/[^:]*:::([^\|;,]+).*?\))/_NEO_ $1 /){ 
 		my $type = $2;
 		my $poids = $3;
 		my $lemme = $4;
		my $posRegle = $5;
		#print "\n→→→→3 $type - $poids - $lemme - $posRegle\n";
		if (int($poids)>int($scoreBestRegle)){
			$scoreBestRegle = $poids;
			$typeBestRegle = $type;
			$bestpos=$posRegle;
			$bestlemme = $lemme;
		}
	}
	$tmpcombinaisonAmb=~s/_NEO_/_NEO/go;
 	
 	#si pas règle flexionnelle ou score trop petit on zappe.
 	next if ($typeBestRegle ne "F" or $scoreBestRegle<1000);
 	
  	#print "→→ $scoreBestRegle - $typeBestRegle - $bestpos - $bestlemme\n";
  	my @allResPossible;
  	$lefff_getall->execute("$bestlemme","$bestpos");
  	while ($mot =  $lefff_getall->fetchrow){
 	 	push (@allResPossible,$mot);
  	}
  		next if ($#allResPossible==-1);
		my $info = "$bestlemme\_$bestpos";
 		$motSxPipe =~ s/<\/F>/◀NEOFLEX_$info<\/F>/;
 		my $motSxPipeP = " \| $motSxPipe "; 
		my $corr = join("$motSxPipeP",@allResPossible);
		$corr = "$motSxPipe $corr";
		$corr = " ( $corr )" if ($#allResPossible>0);
		
 		#ATTENTION PARENTHESES 
 		#print "--> $tmpcombinaisonAmb - $corr\n ";
  		if ($tmpcombinaisonAmb =~ m/^ *\(.*\) *$/){
 			$_ =~ s/$combinaisonAmb */$corr /g;
 		}elsif ($tmpcombinaisonAmb =~ m/^ *\(.*\| *$/){
 			$_ =~ s/$combinaisonAmb */\( $corr \| /g;
 		}elsif ($tmpcombinaisonAmb =~ m/^ *\|.*\| *$/){
 			$_ =~ s/ *$combinaisonAmb */ \| $corr \| /g;
 		}elsif ($tmpcombinaisonAmb =~ m/^ *\|.*\) *$/){
 			$_ =~ s/ *$combinaisonAmb */ \| $corr \) /g;
 		}else{
 			$_ =~ s/$combinaisonAmb/$corr/g;
 		}
	}

	# sortie 
	s/ +/ /go;
	s/( ?[^\\])\(([^<] ?)/$1 \($2/go;
	s/( ?[^\\])\)($|[^<:] ?)/$1\) $2/go; 
	s/Ana *\( *([^\)]+?) *\) */Ana\($1\)/g;
	s/ +/ /go;
	s/^ //;
	s/ $//;
	print "$_\n";
}

##########################################################################################################
# MÉTHODES
##########################################################################################################
sub	encode_UTF8{	
	my ($mot)= @_;
	$mot = Encode::decode("utf-8",$mot);
	return $mot;
}
