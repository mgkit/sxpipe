#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use utf8;
use DBI;
use Encode;

$| = 1;

my $external_lexicons_directory = "/usr/local/share/alexina";
while (1) {
	$_=shift;
	if (/^$/) {last;}
	elsif (/^-d$/) {$external_lexicons_directory=shift;}
	elsif (/^-el$/) {$external_lexicons=shift;}
	elsif (/^-l$/) {$lang=shift;}
}

die "### ERROR: correction_inc_analogie.pl requires using option -d " if ($external_lexicons_directory eq "");
die "Options -l and -el are incompatible. Please provide exactly one of them" if ($lang ne "" && $external_lexicons ne "");
$lang = "fr" if ($lang eq "" && $external_lexicons eq "");

if ($external_lexicons ne "") {
	for (split ',', $external_lexicons) {
		$dbh{$_} = DBI->connect("dbi:SQLite:$external_lexicons_directory/$_.mdb", "", "", {RaiseError => 1, AutoCommit => 1}) || die "External lexicon database $_.dat not found";
		$sth{$_} = $dbh{$_}->prepare('select wordform from data where wordform=?;') || die "Could not prepare request on $_.dat";
		$sthnd{$_} = $dbh{$_}->prepare('select wordform from data where wordform_nodia=?;') || die "Could not prepare request on $_.dat";
	}
} else {
	$dbh{$lang} = DBI->connect("dbi:SQLite:$external_lexicons_directory/$lang.mdb", "", "", {RaiseError => 1, AutoCommit => 1}) || die "External lexicon database $lang.dat not found";
	$sth{$lang} = $dbh{$lang}->prepare('select wordform from data where wordform=?;') || die "Could not prepare request on $lang.dat";
	$sthnd{$lang} = $dbh{$lang}->prepare('select wordform from data where wordform_nodia=?;') || die "Could not prepare request on $lang.dat";
}


while (<>) {
	chomp;
	my $s="";
	while (s/^(.*?\s*)({[^{}]+})\s*_INC_([^ ]+)//) {
		
		$s .= $1;
		my $comment = $2;
		my $token = $3;
		my $parenthese_debut = 1 if ($s=~/\| *$/);
		my $pipe_debut = 1 if ($s =~/\( *$/);
		#print $token." -- $comment\n";
		$token_back = $token;
		# Suppression de quelques accents courants
		$token =~ tr/ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïñòóôõöùúûüýÿ/AAAAAACEEEEIIIINOOOOOUUUUYaaaaaaceeeeiiiinooooouuuuyy/;
		# Mise en minuscule du mot
		$token = lc($token);
		my %tmp = ();
		$analysis_found = 0;
		for (keys %sth) {
			$sth{$_}->execute($token);
			if (@row = $sth{$_}->fetchrow_array) {
				$analysis_found = 1;
				$tmp{decode('utf8', $row[0])} = 1;
			}
			$sth{$_}->finish;
		}
		if (!$analysis_found) { # on pourrait virer ce test, et avoir encore plus d'ambiguïté pour le plaisir :-)
			for (keys %sthnd) {
				$sthnd{$_}->execute($token);
				while (@row = $sthnd{$_}->fetchrow_array) {
					$analysis_found = 1;
					$tmp{decode('utf8', $row[0])} = 1;
				}
				$sthnd{$_}->finish;
			}
		}
		if (scalar keys %tmp == 0) {
			$s .= $comment." _INC_".$token_back;
		} elsif (scalar keys %tmp == 1) {
			for (keys %tmp) { # y'en a qu'un
				$comment=~ s/<\/F>\}/◀MAJDIA<\/F>\}/;
				$s .= $comment." ".$_;
			}
		} else {
			$comment=~ s/<\/F>\}/◀MAJDIA<\/F>\}/;
			my $all_corr .=  join " | ", map {$comment." ".$_} sort {$a cmp $b} keys %tmp;
			if($parenthese_debut or $pipe_debut){
				$s .= $all_corr ;
			}else{
				$s .= "(".$all_corr.")";
			}
		}
	}
	print $s.$_."\n";
}



