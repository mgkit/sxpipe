#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;
use Encode;



$|=1; 

my $share_dir = ""; # folder (typically /usr/local/share/sxpipe) that contains composants de composé data
my $moredic_dir = "";  #Dictionnaires ajoutés
my $morelex=0;
my $seuil = 0.75;
while (1) {
	$_=shift;
	if (!$_ || /^$/) {last;}
	elsif (/^-(-)?h(elp)?$/) {
		print "Le script corr_homophonie a pour but détecter les erreurs dues à la suppression d'un espace et de les corrigers, il prend l'option -d représentant le répertoire sxpipe en option.\n";
	}elsif (/^-d$/) {$share_dir=shift;}
	elsif (/^-seuil$/) {$seuil=shift;}
}
die "### ERROR: corr_homophonie.pl requires using option -d " if ($share_dir eq "");


## hash de cache
my %is_in_dicoPhon_cache;

#on ouvre la base de donnée du dico (phon,tok,lemme,pos,freq)
my $dicoPhon_dbh;
$dicoPhon_dbh = DBI->connect("dbi:SQLite:$share_dir/db_dico_phon.dat", "", "", {RaiseError => 1, AutoCommit => 1});
my $dicoPhon_sth =  $dicoPhon_dbh->prepare('select distinct phon,freq from data where tok=?;');
my $dicoPhon2_sth =  $dicoPhon_dbh->prepare('select distinct tok,freq from data where phon=?;');



while (<>) {
	# formattage
	chomp;
	if (/ ?(_XML|_MS_ANNOTATION|_PAR_BOUND)/) {
		print "$_\n";
		next;
	}
	s/^\s*/ /o;
	s/\s$/ /o;

	#Reconnaissance des mots étrangers
	s/ ?([^\\])\(([^<])/$1 \( $2/go;
	s/ ?([^\\])\)([^<])/$1 \)$2/go;
	s/([^\\])\)/$1 \)/g;

	my $tmp=$_." ";
	$_ = "";
	while ($tmp =~ s/^( *([\(\| ]?)( *([^\}]*\} )([^ ]*?) +(?:\[\|.*?\|\] *)?)(\)*) *)//) {
		my $motsxpipe = $1;
		my $debut = $2;
		my $motsxpipe_sans_ponct = $3;
		my $mot = $5;
		my $fin = $6;
					#print "ceci est un mot : $mot --- $motsxpipe_sans_ponct\n";
		
		if ($mot!~m/_INC_/ and $motsxpipe_sans_ponct!~m/\[\|[^ ]/ ) {
			#print "ceci est un mot connu: $mot\n";
			my ($bool_homophone,$allhomophones_ref) = haveHomophone($mot,$seuil);#reconnaissance des homophones
 			$motsxpipe = makeAnnotationHomophonie($mot,$debut,$fin, $motsxpipe_sans_ponct,$allhomophones_ref) if ($bool_homophone);
		}
		$_ .=$motsxpipe;
	}
	# sortie 
	s/ +/ /g;
	s/([^\\])\(([^<])/$1 \($2/go;
	s/([^\\])\)($|[^<])/$1\) $2/go;
	s/^ //;
	s/ +/ /g;
	s/ $//;
	print "$_\n";
}


#on ferme la base de donnée du lefff
$dicoPhon_sth->finish;
$dicoPhon2_sth->finish;
$dicoPhon_dbh ->disconnect; # pour se déconnecter de la base de données en fin de programme.


###--------------------------------------------------------------
#                      MÉTHODES
###--------------------------------------------------------------


sub haveHomophone{
	my $mot = shift;
	my $seuil = shift;
	my %allhomophones = ();
	my $bool_homophones=0;
	
	$dicoPhon_sth->execute($mot);
	while ( ( $phon, $freq ) = $dicoPhon_sth->fetchrow ) {
		$phon = Encode::decode( "utf-8", $phon );
		$freq = Encode::decode( "utf-8", $freq );
		#print "---- $phon - $freq -----\n";
		
		if ( $freq >= $seuil ) {
			#Pour le mot de base
			$dicoPhon2_sth->execute($phon);
			while ( ( $tok, $freq2 ) = $dicoPhon2_sth->fetchrow ) {
				$tok   = Encode::decode( "utf-8", $tok );
				$freq2 = Encode::decode( "utf-8", $freq2 );
				$allhomophones{$tok}=1 if ( $freq2 >= $seuil and $mot ne $tok);
				$bool_homophones = 1 if ( $freq2 >= $seuil and $mot ne $tok );
			}#si e
			if ($phon =~ m/e/){
				my $phon_tmp = $phon;
				$phon_tmp =~ s/e/E/go;
				$phon_tmp2 =~ s/e([^e]?[^e]?)$/E$1/;
				$dicoPhon2_sth->execute($phon_tmp);
				while ( ( $tok, $freq2 ) = $dicoPhon2_sth->fetchrow ) {
					#print "\t\t → $tok, $freq2\n";
					$tok   = Encode::decode( "utf-8", $tok );
					$freq2 = Encode::decode( "utf-8", $freq2 );
					$allhomophones{$tok}=1 if ( $freq2 >= $seuil and $mot ne $tok);
					$bool_homophones = 1 if ( $freq2 >= $seuil and $mot ne $tok );
				}
				if ($phon_tmp ne $phon_tmp2){
					$dicoPhon2_sth->execute($phon_tmp2);				
					while ( ( $tok, $freq2 ) = $dicoPhon2_sth->fetchrow ) {
						#print "\t\t → $tok, $freq2\n";
						$tok   = Encode::decode( "utf-8", $tok );
						$freq2 = Encode::decode( "utf-8", $freq2 );
						$allhomophones{$tok}=1 if ( $freq2 >= $seuil and $mot ne $tok);
						$bool_homophones = 1 if ( $freq2 >= $seuil and $mot ne $tok );
					}
				}
			}#si E
			if ($phon =~ m/E/){
				my $phon_tmp = $phon;
				$phon_tmp =~ s/E/e/go;
				$phon_tmp2 =~ s/E([^E]?[^E]?)$/e$1/;
				#print  "\t → $phon_tmp \n";
				#print "\t\t → $tok, $freq2\n";
				$dicoPhon2_sth->execute($phon_tmp);
				while ( ( $tok, $freq2 ) = $dicoPhon2_sth->fetchrow ) {
					$tok   = Encode::decode( "utf-8", $tok );
					$freq2 = Encode::decode( "utf-8", $freq2 );
					$allhomophones{$tok}=1 if ( $freq2 >= $seuil and $mot ne $tok);
					$bool_homophones = 1 if ( $freq2 >= $seuil and $mot ne $tok );
				}
				if ($phon_tmp ne $phon_tmp2){
					$dicoPhon2_sth->execute($phon_tmp2);				
					while ( ( $tok, $freq2 ) = $dicoPhon2_sth->fetchrow ) {
						#print "\t\t → $tok, $freq2\n";
						$tok   = Encode::decode( "utf-8", $tok );
						$freq2 = Encode::decode( "utf-8", $freq2 );
						$allhomophones{$tok}=1 if ( $freq2 >= $seuil and $mot ne $tok);
						$bool_homophones = 1 if ( $freq2 >= $seuil and $mot ne $tok );
					}
				}
			}#si o
			if ($phon =~ m/o/){
				my $phon_tmp = $phon;
				$phon_tmp =~ s/o/O/go;
				$dicoPhon2_sth->execute($phon_tmp);
				while ( ( $tok, $freq2 ) = $dicoPhon2_sth->fetchrow ) {
					$tok   = Encode::decode( "utf-8", $tok );
					$freq2 = Encode::decode( "utf-8", $freq2 );
					$allhomophones{$tok}=1 if ( $freq2 >= $seuil and $mot ne $tok);
					$bool_homophones = 1 if ( $freq2 >= $seuil and $mot ne $tok );
				}
			}#si O
			if ($phon =~ m/O$/){
				my $phon_tmp = $phon;
				$phon_tmp =~ s/O/o/go;
				$dicoPhon2_sth->execute($phon_tmp);
				while ( ( $tok, $freq2 ) = $dicoPhon2_sth->fetchrow ) {
					$tok   = Encode::decode( "utf-8", $tok );
					$freq2 = Encode::decode( "utf-8", $freq2 );
					$allhomophones{$tok}=1 if ( $freq2 >= $seuil and $mot ne $tok);
					$bool_homophones = 1 if ( $freq2 >= $seuil and $mot ne $tok );
				}
			}
		}
	}
	return ($bool_homophones,\%allhomophones);
}


sub makeAnnotationHomophonie{
	my ($mot,$debut,$fin, $motsxpipe,$allhomophones_ref)=@_;
	%allhomophones=%$allhomophones_ref;
	my $motsxpipe_tmp=" ";
	for $homophone (keys %allhomophones){
		my $newhomophone= $motsxpipe;
		$newhomophone=~s/ $mot / $homophone /; 
		$motsxpipe_tmp.=" \| ".$newhomophone;
	} 
	$motsxpipe_tmp=~ s/<\/F>/◀Homophone<\/F>/g;
	$motsxpipe_tmp=~ s/([^\[]*)\|([^\]]*)/$1 \| $2/g;
	$motsxpipe_tmp=$motsxpipe.$motsxpipe_tmp;
	
	$motsxpipe_tmp=" ".$debut." ( ".$motsxpipe_tmp." ) ".$fin." ";
	return $motsxpipe_tmp;
}
