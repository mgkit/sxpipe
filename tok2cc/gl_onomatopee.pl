#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;

$|=1; 

my @moreOnomatopee_files = ();#Listes d'onomatopées ajoutées
while (1) {
$_=shift;
if (/^$/) {last;}
	elsif (/^-(-)?h(elp)?$/) {
		print "Le script gl_onomatopee a pour but détecter les onomatopées et les interjections et de les annoter comme tel. Pour bien fonctionner il nécessite les options suivantes:
\t-addOnom\tcorrespond au fichier dans lequel sont stockées les onomatopées/interj (l'option doit être rappelé pour chaque fichier concerné)\n";
	}
	elsif (/^-onom_file$/) {push (@moreOnomatopee_files,shift);}
}

die "### ERROR: gl_onomatopee.pl requires using option -addOnom followed by onomatopee data" if (@moreOnomatopee_files < 1);

#on stocke les onomatopées données en options
my %onomatopees;
my $file;
for $file (@moreOnomatopee_files){
	open (Fic, "<$file") || die "Problème à l\'ouverture $file : $!";;
	binmode (Fic, ":encoding(utf8)");
	while (<Fic>) {
 		chomp;
		m/^(.*)\t(.*)$/;
		$onomatopees{$2} = $1;#on stoke l'onomatopée normalisée et sa regex
	}	
	close (Fic);
}

while(<>){ #on parcourt le doc
# formattage
chomp;
if (/ ?(_XML|_MS_ANNOTATION|_PAR_BOUND) *$/) {
	print "$_\n";
	next;
}
s/^\s*/ /o;
s/\s$/ /o;

s/ ?([^\\])\(([^<])/$1 \( $2/go;
s/ ?([^\\])\)([^<])/$1 \)$2/go;
s/$/ /go;

#Pré-correction
my $tmp = "$_ ";
$_="";


chomp $tmp;
while ($tmp =~ s/(^ *([\(\| ]*) ? ?(\{[^\}]*\}) (.*?) +(\[\|[^\|]*\|\])? *([\) ]*) *)//) { #pour chaque mot sxpipe
	my $motsxpipe = $1; #forme sxpipe entière
	my $poncdebut = $2; #ponct avant
	my $original = $3; #première partie de la forme sxpipe (dans les {} )
	my $mot_original = $4; #seconde partie de la forme sxpipe (après les {} )
	my $mot = quotemeta($mot_original);
	my $refEN = $5; #référent d'une entité nommée
	my $poncfin = $6; #ponct après
	#print "$2 - $3 - $4 - $5 - $mot\n";
	#print "debug : \n tmp rest : ".$tmp."\n motsxpipe: $motsxpipe \n";

	#####Pour tout les mots étiquetés inconnu
	if ($mot =~ m/_INC_/){
		my $motbis= $mot ;
		$motbis=~ s/_INC_//g;
		my $normalisation = isOnomatopee($motbis,\%onomatopees);#reconnaissance des onomatopées
 		$motsxpipe = makeOnomatopee($mot, $motsxpipe,$normalisation) if ($normalisation ne "");
	}
	$_ .=$motsxpipe;			 
}

# sortie 
s/ +/ /go;
s/( ?[^\\])\(([^<] ?)/$1 \($2/go;
s/( ?[^\\])\)($|[^<] ?)/$1\) $2/go; 
s/^ //;
s/ $//;
print "$_\n";
}


#print STDERR "Correcteur: Détection pré-correction: done\n";

################################################################################
#  MÉTHODES
################################################################################

sub isOnomatopee{
	my ($mot,$onomatopees)=@_;
	my %onomatopees=%$onomatopees;
	my $onomatopee2;
	foreach $onomatopee2 (keys %onomatopees){
		my $regex = qr/$onomatopee2/i;
		return $onomatopees{$onomatopee2} if ($mot=~ m/^$onomatopee2$/);
	}
	return "";
}

sub makeOnomatopee {
	my ($mot,$motsxpipe,$normalisation)=@_;
	$motsxpipe =~ s/ $mot / $normalisation /g;
	$motsxpipe =~ s/<\/F>/◀ONOMATOPEE<\/F>/g;
	return $motsxpipe;
}
