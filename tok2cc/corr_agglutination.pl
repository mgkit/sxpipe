#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;



$|=1; 

my $lang = 'fr';
my $share_dir = ""; # folder (typically /usr/local/share/sxpipe) that contains composants de composé data
my $moredic_dir = "";  #Dictionnaires ajoutés
my $morelex=0;

while (1) {
	$_=shift;
	if (!$_ || /^$/) {last;}
	elsif (/^-(-)?h(elp)?$/) {
		print "Le script corr_agglutination a pour but détecter les erreurs dues à la suppression d'un espace et de les corrigers, il prend l'option -d représentant le répertoire sxpipe en option.\n";
	}elsif (/^-d$/) {$share_dir=shift;}
	elsif (/^-l$/) {$lang=shift;}
}
die "### ERROR: corr_agglutination.pl requires using option -d " if ($share_dir eq "");


## hash de cache
my %is_in_lefff_cache;

#on ouvre la base de donnée du lefff
my $lefff_dbh;
$lefff_dbh = DBI->connect("dbi:SQLite:$share_dir/$lang.mdb", "", "", {RaiseError => 1, AutoCommit => 1});
my $lefff_sth =  $lefff_dbh->prepare('select EXISTS (select * from data where wordform=?);');


while (<>) {
	# formattage
	chomp;
	if (/ ?(_XML|_MS_ANNOTATION|_PAR_BOUND)/) {
		print "$_\n";
		next;
	}
	s/^\s*/ /o;
	s/\s$/ /o;

	#Reconnaissance des mots agglutinés
	s/ ?([^\\])\(([^<])/$1 \( $2/go;
	s/ ?([^\\])\)([^<])/$1 \)$2/go;
	s/([^\\])\)/$1 \)/g;

	my $tmp=$_." ";
	$_ = "";
	while ($tmp =~ s/^( *([\(\| ]?)( *([^\}]*\} )([^ ]*?) +(?:\[\|.*?\|\] *)?)([\) ]*) *)//) {
		my $motsxpipe = $1;
		my $debut = $2;
		my $motsxpipe_sans_ponct = $3;
		my $mot = $5;
		my $fin = $6;
		
		if ($mot =~ m/_INC_/) {
			my $motbis= $mot ;
			$motbis=~ s/_INC_//g;
			my ($bool_agglu,$allAgglu_ref) = isAgglutination($motbis);
 			$motsxpipe = makeAnnotationAgglutination($mot,$debut,$fin, $motsxpipe_sans_ponct,$allAgglu_ref) if ($bool_agglu);
		}
		$_ .=$motsxpipe;
	}

	# sortie 
	s/ +/ /go;
	s/( ?[^\\])\(([^<] ?)/$1 \($2/go;
	s/( ?[^\\])\)($|[^<] ?)/$1\) $2/go;
	s/^ //;
	s/ $//;
	print "$_\n";
}


#on ferme la base de donnée du lefff
$lefff_sth->finish;
$lefff_dbh ->disconnect; # pour se déconnecter de la base de données en fin de programme.


###--------------------------------------------------------------
#                      MÉTHODES
###--------------------------------------------------------------
sub is_in_lefff {
	my $word = shift;
	return $is_in_lefff_cache{$word} if (defined($is_in_lefff_cache{$word})) ;
	return 1 if ($word =~ m/^(au|du)/) ; #Add Amalgame!!!!
	$lefff_sth->execute($word);
	my $reponse = $lefff_sth->fetchrow;
	$is_in_lefff_cache{$word} = $reponse;
	return $reponse if ($reponse);
	$lefff_sth->execute(ucfirst($word));
	my $reponse2 = $lefff_sth->fetchrow;
	return $reponse2;
}

sub isAgglutination{
	my $mot = shift;
	my @allAgglu=();
	my $bool_agglu=0;
	if ($mot=~m/^([jlJL]|qu)(.*)$/){
		if (is_in_lefff(lc $2)){
			push (@allAgglu,$1."'_".$2) ;
			$bool_agglu=1;
		}
	}

	$mot=~s/^(.)//;
	my $pref=$1;
	if(!$bool_agglu){
	while ($mot=~s/(.)(.*)$/$2/){
		my $suff=$1.$2;
		my $lettre=$1;
		if (is_in_lefff(lc $pref) and is_in_lefff(lc $suff)){
			push (@allAgglu,$pref."_".$suff) ;
			$bool_agglu=1;
		}
		$pref=$pref.$lettre;
	}
	}
	return ($bool_agglu,\@allAgglu);
}


sub makeAnnotationAgglutination{
	my ($mot,$debut,$fin, $motsxpipe,$allAgglu_ref)=@_;
	@allAgglu=@$allAgglu_ref;
	my $motsxpipe_tmp=" ";#$motsxpipe;
	for $agglu (@allAgglu){
		my @decompAgglu = split(/_/,$agglu);
		
		my $newagglu;
		for $elemt (@decompAgglu){
			$newagglu = $motsxpipe;
			$newagglu=~s/$mot/$elemt/;
			$motsxpipe_tmp.=$newagglu." ";
		}
		$motsxpipe_tmp.="\|";
	}
	$motsxpipe_tmp=~s/\|$//;
	$motsxpipe_tmp=~ s/<\/F>/◀Agglu<\/F>/g;
	$motsxpipe_tmp=$motsxpipe." | ".$motsxpipe_tmp;
	$motsxpipe_tmp=$debut.$motsxpipe_tmp if($debut ne "");
	$motsxpipe_tmp=$motsxpipe_tmp.$fin if($fin ne "");
	$motsxpipe_tmp=" ( ".$motsxpipe_tmp." ) " if($debut eq "" and $fin eq "");
	
	return $motsxpipe_tmp;
}
 			



