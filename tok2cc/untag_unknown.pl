#!/usr/bin/env perl
# $Id$

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$| = 1;

while (<>) {
    s/<<<//g;
    s/>>>//g;
    print $_;
}
