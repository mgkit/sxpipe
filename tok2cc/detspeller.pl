#!/usr/bin/env perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

my $correction_file = "";
my $spec_word = "";
my $output_correction_count = 0;
my $correction_count_file = "";
my $correction_count = 0;
my $token_count = 0;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-sw$/ || /^-spec_word$/i) {$spec_word="_SPECWORD_";}
    elsif (/^-ccf$/) {$output_correction_count = 1; $correction_count_file = shift || "ccf"}
    else {$correction_file = $_}
}

my (%corr,%tobeapplied,$in,$out,$first,$in_majinit,$out_majinit,$first_majinit);

if ($correction_file ne "") {
    if (open (FILE, "<$correction_file")) {
      binmode FILE, ":utf8";
      while (<FILE>) {
	chomp;
	s/(^|[^\\])#.*$/\1/;
	s/\t\s+/\t/g;
	s/\s+\t/\t/g;
	s/\s+$//;
	s/^\s+//;
	/^(([^ \t]+?)(?: [^\t]*?)?)\t([^\t]*?)(\t|$)/ || next;
	$in = $1;
	$first = $2;
	$out = $3;
	$out =~ s/ /_/g;
	$in_majinit = $in;
	$in_majinit =~ s/^(.)/uc($1)/e;
	$in = quotemeta($in);
	$in_majinit = quotemeta($in_majinit);
	$corr{$first}{$in}{in} = $in;
	$corr{$first}{$in}{re_in} = qr/$in/;
	$corr{$first}{$in}{out} = $out;
	if ($in ne $in_majinit) {
	  $out_majinit = $out;
	  $out_majinit =~ s/^(.)/uc($1)/e;
	  $first_majinit = $first;
	  $first_majinit =~ s/^(.)/uc($1)/e;
	  $corr{$first_majinit}{$in_majinit}{in} = $in_majinit;
	  $corr{$first_majinit}{$in_majinit}{re_in} = qr/$in_majinit/;
	  $corr{$first_majinit}{$in_majinit}{out} = $out_majinit;
	}
      }
      close FILE;
    } else {
      print STDERR "###### WARNING: could not open $correction_file: $!\n";
      $correction_file = "";
    }
}

my ($w,$c,$c1);
while (<>) {
  if ($correction_file ne "") {
    chomp;
    %tobeapplied = ();
    for $w (split(/ /,$_)) {
      if (defined($corr{$w})) {
	for $c (sort keys %{$corr{$w}}) {
	  $tobeapplied{$corr{$w}{$c}{re_in}} = $corr{$w}{$c}{out};
	}
      }
    }
    s/^/  /;
    s/$/  /;
    for $in (sort {length($b) <=> length($a)} keys %tobeapplied) {
      $correction_count += s/(?<=[^}] )($in)(?= )/{$1} $spec_word$tobeapplied{$in}/g;
      $correction_count += s/(?<=} )($in)(?= )/$spec_word$tobeapplied{$in}/g;
    }
    s/^ +//;
    s/  +$//;
    print "$_\n";
  } else {
    print "$_";
  }
  $token_count += (scalar s/((?:{.*?} *)?[^ ]+)/|\1/g);
}

if ($output_correction_count) {
  open CCF, ">$correction_count_file" || print STDERR "\n###### WARNING: $correction_count_file could not be opened ($!)\n";
  print CCF $correction_count."/".$token_count."\n";
  close CCF;
}
