#!/usr/bin/env perl
# $Id$

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$| = 1;

$lang="fr";

$cl=qr/(?:[A-ZÀÉÈÊËÂÄÔÖÛÜÇ])/o;

$link=qr/(?:<<<)?(?:for|and|resources|on|in|an|de|à|la|et|aux|l\'|les?|\-|pour|d\'|des)(?:>>>)?/o;

while (<>) {
    chomp;
    if (/ (_XML|_MS_ANNOTATION|_PAR_BOUND) *$/) {
	print "$_\n";
	next;
    }

    @words=();
    $ne="";
    $level=0; #sécurité
    if (/^$cl/) {
	s/([^ ])\{/$1 {/g;
	s/\}([^ ])/\} $1/g;
	for (split) {
	    if (/^[^\{]/ && $level==0) {
		push(@words,$_);
	    } else {
		if ($ne ne "") {$ne.=" "}
		$ne.=$_;
		if (/^\{/) {
		    $level++;
		}
		if ($ne=~/\} *[^ ]+$/) {
		    $level--;
		    if ($level==0) {
			push(@words,$ne);
			$ne="";
		    }
		}
	    }
	}
	$tags="";
	$guessed_initials="";
	$guessed_expanded="";
# Sigle Avec des Link ( SAL )
	@oldwords=@words;
	@words=();
	$state=0; #hors entnom
	for (@oldwords) {
	    if (/^(?:<<<)?($cl)/o && ($state==0 || $state==1)) {
		$state=1;
		$guessed_initials.=$1;
		s/^<<<(.*)>>>$/$1/;
		$guessed_expanded.=$_." ";
	    } elsif (/^(?:<<<)?$link(?:>>>)?$/o && $state==1) {
		s/^<<<(.*)>>>$/$1/;
		$guessed_expanded.=$_." ";
	    } elsif ((/^\($/ || /^\[$/) && $state==1) {
		$state=2;
		$op=$_;
	    } elsif ($_=~/^(?:<<<)?$guessed_initials(?:>>>)?$/ && $state==2) {
		$state=3;
	    } elsif ((/^\)$/ || /^\]$/) && $state==3) {
		$end=$_;
		push (@words,"{ $guessed_expanded $op $guessed_initials $end }_NP_WITH_INITIALS");
		$guessed_expanded="";
		$guessed_initials="";
		$op="";
		$state=0;
	    } elsif ($state>0) {
		push (@words,split(/ /,$guessed_expanded));
		if ($op ne "") {push (@words,$op);}
		if ($state==3) {push (@words,$guessed_initials);}
		push (@words,$_);
		$guessed_expanded="";
		$guessed_initials="";
		$op="";
		$state=0;
	    } else {
		push (@words,$_);
		$state=0;
	    }
	}
	if ($state>0) {
	    push (@words,split(/ /,$guessed_expanded));
	    if ($op ne "") {push (@words,$op);}
	    if ($state==3) {push (@words,$guessed_initials);}
	    $guessed_expanded="";
	    $guessed_initials="";
	    $op="";
	    $state=0;
	}
# SAL ( Sigle Avec des Link )
	@oldwords=@words;
	@words=();
	$state=0; #hors entnom
	for (@oldwords) {
#	    print $expanded."\t".$initials."\t$state\t".($#initials + 3)."\n";
	    if (/^(?:<<<)?((?:$cl){2,})(?:>>>)?$/o && $state<=1) {
		if ($state==1) {push(@words,$initials)}
		$state=1;
		$initials=$1;
	    } elsif ((/^\($/ || /^\[$/) && $state==1) {
		$state=2;
		@initials=split(//,$initials);
		$op=$_;
	    } elsif ($state>=2 && /^(?:<<<)?($cl)/o && $initials[$state-2] eq $1) {
		s/^<<<(.*)>>>$/$1/;
		$expanded.=" ".$_;
		$state++;
	    } elsif ($state>=2 && /^(?:<<<)?$link(?:>>>)?$/o) {
		s/^<<<(.*)>>>$/$1/;
		$expanded.=" ".$_;
	    } elsif ($state>3 && (/^\)$/ || /^\]$/) && $state==($#initials+3)) {
		$end=$_;
		push (@words,"{ $initials $op$expanded $end }_NP_WITH_INITIALS");
		$expanded="";
		$initials="";
		$op="";
		$state=0;
	    } elsif ($state>0) {
		push (@words,$initials);
		if ($op ne "") {push (@words,$op);}
		if ($state>=2) {push (@words,split(/ /,$expanded));}
		push (@words,$_);
		$expanded="";
		$initials="";
		$op="";
		$state=0;
	    } else {
		push (@words,$_);
		$state=0;
	    }
	}
	if ($state>0) {
	    push (@words,$initials);
	    if ($op ne "") {push (@words,$op);}
	    if ($state>3) {push (@words,split(/ /,$expanded));}
	    $expanded="";
	    $initials="";
	    $op="";
	    $state=0;
	}
# SAL [-,] Sigle Avec des Link
	@oldwords=@words;
	@words=();
	$state=0; #hors entnom
	for (@oldwords) {
#	    print $expanded."\t".$initials."\t$state\t".($#initials + 3)."\n";
	    if (/^(?:<<<)?((?:$cl){2,})(?:>>>)?$/o && $state<=1) {
		if ($state==1) {push(@words,$initials)}
		$state=1;
		$initials=$1;
	    } elsif (/^[-,]$/ && $state==1) {
		$state=2;
		@initials=split(//,$initials);
		$op=$_;
	    } elsif ($state>=2 && /^(?:<<<)?($cl)/o && $initials[$state-2] eq $1) {
		s/^<<<(.*)>>>$/$1/;
		$expanded.=" ".$_;
		$state++;
	    } elsif ($state>=2 && /^(?:<<<)?$link$/o) {
		s/^<<<(.*)>>>$/$1/;
		$expanded.=" ".$_;
	    } elsif ($state>3 && $state==($#initials+3)) {
		$following=$_;
		push (@words,"{ $initials $op$expanded }_NP_WITH_INITIALS $following");
		$expanded="";
		$initials="";
		$op="";
		$state=0;
	    } elsif ($state>0) {
		push (@words,$initials);
		if ($op ne "") {push (@words,$op);}
		if ($state>=2) {
		    for (split(/ /,$expanded)) {
			push (@words,$_);
		    }
		}
		push (@words,$_);
		$expanded="";
		$initials="";
		$op="";
		$state=0;
	    } else {
		push (@words,$_);
		$state=0;
	    }
	}
	if ($state>0 && $state < $#initials+3) {
	    push (@words,$initials);
	    if ($op ne "") {push (@words,$op);}
	    if ($state>=2) {
		for (split(/ /,$expanded)) {
		    push (@words,$_);
		}
	    }
	    $expanded="";
	    $initials="";
	    $op="";
	    $state=0;
	} elsif ($state==$#initials+3) {
	    push (@words,"{ $initials $op$expanded } _NP_WITH_INITIALS");
	    $expanded="";
	    $initials="";
	    $op="";
	    $state=0;
	}
	print join(' ',@words)."\n";
    } else {
	print "$_\n";
    }
}
