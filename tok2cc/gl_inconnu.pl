#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
use strict;
use warnings;

$|=1; 

my $lang = 'fr';
my $share_dir = ""; # folder (typically /usr/local/share/sxpipe) that contains composants de composé data
my $moredic_dir = "";  #Dictionnaires ajoutés
my $morelex=0;

while (1) {
$_=shift;
if (!$_ || /^$/) {last;}
	elsif (/^-(-)?h(elp)?$/) {
		print "Le script gl_inconnu a pour but détecter les mots inconnus d'un texte et de les annoter comme tel. Pour bien fonctionner il nécessite les options suivantes:
\t-l\tpermet de selectionner la langue voulu (optionnel: par défaut la langue selectionnée sera le français)
\t-ad\tcorrespond au répertoire dans lequel est stocké le dictionnaire de référence(Lefff)
\t-addLex\tpermet d'ajouter un lexique (un mot par ligne)
\t-d\tcorrespond au répertoire dans lequel sont stockés les composants de composés\n";
}
elsif (/^-d$/) {$share_dir=shift;}
elsif (/^-l$/) {$lang=shift;}
elsif (/^-addLex$/) {
	$moredic_dir=shift;
	$morelex = 1; 
}   
}
die "### ERROR: gl_inconnu.pl requires using option -d " if ($share_dir eq "");

my $lex_dir=$share_dir;

## hash de cache
my %is_in_lefff_cache;
my %is_in_morelex_cache;

#on ouvre la base de donnée du lefff
my $lefff_dbh;
$lefff_dbh = DBI->connect("dbi:SQLite:$lex_dir/$lang.mdb", "", "", {RaiseError => 1, AutoCommit => 1});
my $lefff_sth =  $lefff_dbh->prepare('select EXISTS (select * from data where wordform=?);');

#on ouvre la base de donnée des autres lexiques
my $morelex_dbh;
my $morelex_sth;
if ($morelex){
	$morelex_dbh = DBI->connect("dbi:SQLite:$moredic_dir/dic_viavoo.dat", "", "", {RaiseError => 1, AutoCommit => 1});
	$morelex_sth = $morelex_dbh->prepare('select EXISTS (select * from data where wordform=?);');
}

#on stocke les composants de composé
my @lexComp;
open FILE, "<$share_dir/fr-light-normalisation.cc" || die "Could not open $share_dir/fr-light-normalisation.cc: $!";
binmode FILE, ":encoding(iso-8859-1)";
while (<FILE>) {
	chomp;
	s/^\"//;
	s/\"$//;
	push(@lexComp, $_);
}
close FILE;

while(<>){ #on parcourt le doc
	# formattage
	chomp;
	if (/ ?(_XML|_MS_ANNOTATION|_PAR_BOUND) *$/) {
	print "$_\n";	
		next;
	}
	s/^\s*/ /o;
	s/\s$/ /o;

	s/ ?([^\\])\(([^<])/$1 \( $2/go;
	s/ ?([^\\])\)([^<])/$1 \)$2/go;
	s/$/ /go;

	my $tmp = "$_";
	$_="";

	chomp $tmp;
	while ($tmp =~ s/(^ *([\(\| ]*) ? ?(\{[^\}]*\}) +(.*?) +(\[\|[^\|]*\|\])? *(\))* *)//) { #pour chaque mot sxpipe
		my $motsxpipe = $1; #forme sxpipe entière
		#$poncdebut = $2; #ponct avant
		#$original = $3; #première partie de la forme sxpipe (dans les {} )
		my $mot_original = $4; #seconde partie de la forme sxpipe (après les {} )
		my $mot = quotemeta($mot_original);
		#$refEN = $5; #référent d'une entité nommée
		#$poncfin = $6; #ponct après
		#print "$motsxpipe - $mot\n";
		##print "debug : \n tmp rest : ".$tmp."\n motsxpipe: $motsxpipe \n";

		#####Pour tout les mots qui sont : non traité par sxpipe et pas dans le lefff
		if (isInconnu($mot_original,$morelex)){ #si mot est inconnu on l'annote
			#print "inconnu\n";
			$motsxpipe = makeInconnu($mot,$mot_original, $motsxpipe) ;
		}
		$_ .=$motsxpipe;			 
	}

	# sortie 
	s/ *([^\\])\(([^<]) */$1 \($2/go;
	s/ *([^\\])\)([^<]) */$1\) $2/go;
	s/^ //;
	s/ $//;
	print "$_\n";
}

#on ferme la base de donnée du lefff
$lefff_sth->finish;
$lefff_dbh ->disconnect; # pour se déconnecter de la base de données en fin de programme.

#print STDERR "Correcteur: Détection pré-correction: done\n";

################################################################################
#  MÉTHODES
################################################################################

sub makeInconnu {
	my ($mot,$mot_original,$motsxpipe)=@_;
	$motsxpipe =~ s/ $mot / _INC_$mot_original /g;
	return $motsxpipe;
}

sub is_in_lefff {
	my $word = shift;
	return $is_in_lefff_cache{$word} if (defined($is_in_lefff_cache{$word})) ;
	$lefff_sth->execute($word);
	my $reponse = $lefff_sth->fetchrow;
	$is_in_lefff_cache{$word} = $reponse;
	return $reponse;
}

sub is_in_morelex {
	my $word = shift;
	return $is_in_morelex_cache{$word} if (defined($is_in_morelex_cache{$word})) ;
	#print $word."\n";
	$morelex_sth->execute(lc $word);
	my $reponse = $morelex_sth->fetchrow;
	$is_in_morelex_cache{$word} = $reponse;
	return $reponse;
}
	
sub isInconnu {
	my ($mot,$morelex)=@_;
	if($mot =~ m/^(\\[\(\)\*\+\?\|\%])+$/ or $mot =~ m/^\.+$/) {
		return 0 ;#pas inconnu car symbole regex
	}elsif ($mot =~ m/^_/) {
		return 0 ;#pas inconnu car déjà étiqueté
	}elsif ($mot =~ m/^\s*$/) {
		return 0 ;#pas inconnu car vide
	}elsif ($mot =~ m/^.+__(prep|csu)$/) {
		return 0 ;#pas inconnu considéré comme prep ou csu
	}elsif(is_in_lefff($mot) || is_in_lefff(lcfirst $mot) || is_in_lefff(lc $mot)) {
		return 0 ;#pas inconnu car in lefff
	}elsif($morelex){
		return 0 if(is_in_morelex($mot) || is_in_morelex(lc $mot)) ; #pas inconnu car in autres lexiques
	}elsif($mot ~~ @lexComp) {
		return 0 ;#pas inconnu car in composant compose
	}
	$mot =~ m/(.*)__.*/;
	return 0 if(is_in_lefff($1)) ;#pas inconnu car in lefff

	my $mot_mod = lcfirst($mot);
	$mot_mod =~ s/_/ /g;
	if(is_in_lefff($mot_mod)) {
		return 0 ;#pas inconnu car in lefff
	}elsif($morelex){
		return 0 if(is_in_morelex($mot)) ; #pas inconnu car in autres lexique
	}
	return 1;
}
