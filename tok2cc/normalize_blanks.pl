#!/usr/bin/env perl
# $Id$
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$| = 1;

while (<>) {
    chomp;
    if (/ (_XML|_MS_ANNOTATION|_PAR_BOUND) *$/) {
	print "$_\n";
	next;
    }
    s/  */ /g;
    s/{ /{/g;
    s/ }/}/g;
    s/\\/\\\\/g;
    s/%/\\%/g;
    print $_."\n";
}
