#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use utf8;
use DBI;
use Encode;

$| = 1;

my $external_lexicons_directory = "./";
while (1) {
	$_=shift;
	if (/^$/) {last;}
	elsif (/^-d$/) {$external_lexicons_directory=shift;}
	elsif (/^-el$/) {$external_lexicons=shift;}
	elsif (/^-l$/) {$lang=shift;}
}

die "Options -l and -el are incompatible. Please provide exactly one of them" if ($lang ne "" && $external_lexicons ne "");
$lang = "fr" if ($lang eq "" && $external_lexicons eq "");

$select_restrictions = " and tag not like 'AUX%' and tag not like 'CF%' and tag not like 'V_Y%'";
if ($external_lexicons ne "") {
	for (split ',', $external_lexicons) {
		$dbh{$_} = DBI->connect("dbi:SQLite:$external_lexicons_directory/$_.mdb", "", "", {RaiseError => 1, AutoCommit => 1}) || die "External lexicon database $_.dat not found";
		$dbh{$_}->do('PRAGMA case_sensitive_like = true');
		$sth{$_} = $dbh{$_}->prepare('select distinct lemma,tag,inflclass from data where wordform=?'.$select_restrictions.';') || die "Could not prepare request on $_.dat";
		$sthl{$_} = $dbh{$_}->prepare('select distinct lemma,tag,inflclass from data where wordform like ?'.$select_restrictions.';') || die "Could not prepare request on $_.dat";
	}
} else {
	$dbh{$lang} = DBI->connect("dbi:SQLite:$external_lexicons_directory/$lang.mdb", "", "", {RaiseError => 1, AutoCommit => 1}) || die "External lexicon database $lang.dat not found";
	$dbh{$lang}->do('PRAGMA case_sensitive_like = true');
	$sth{$lang} = $dbh{$lang}->prepare('select distinct lemma,tag,inflclass from data where wordform=?'.$select_restrictions.';') || die "Could not prepare request on $lang.dat";
	$sthl{$lang} = $dbh{$lang}->prepare('select distinct lemma,tag,inflclass from data where wordform like ?'.$select_restrictions.';') || die "Could not prepare request on $lang.dat";
}
my $tmp = "";
while (<>) {
	chomp;
	while (s/^(.*?)_INC_([^ ]+)//) {
		$tmp.= $1;
		my $analysis_found = 0;
		my $token = $2;
		my @comps = split('-', $token);
		if (@comps > 1) {
			# Tente de supprimer les tirets
			if (not $analysis_found) {
				my @comp_hyps = ();
				my $comp = join('', @comps);
				for (keys %sth) {
					$sth{$_}->execute($comp);
	 				while (@row = $sth{$_}->fetchrow_array) {
						$row[2] =~ s/:/!/g;
						$lemma = decode('utf8',$row[0]);
						while (1) {
							$pos = index($token, '-', $pos + 1);
							last if($pos < 0);
							if ($pos < length($lemma)) {
								substr($lemma, $pos, 0, '-');
							}
						}
						push(@comp_hyps, {lemma => $lemma, tag => decode('utf8',$row[1]), flex => decode('utf8',$row[2])});
					}
				}
				if (@comp_hyps) {
					$analysis_found = 1;
					my @hyps = ();
					push(@hyps, [@comp_hyps]);
					@hyps = reverse(@hyps);
					my @hyps_all = @{createHypothesis(\@hyps)};
					$tmp.=  '_NEO'.' [|Assoc(join):'.join(';Assoc(join):', @hyps_all).'|]';
				}
			}
			
			# Tente de remplacer les tirets par des espaces
			if (not $analysis_found) {
				my @comp_hyps = ();
				my $comp = join(' ', @comps);
				for (keys %sth) {
					$sth{$_}->execute($comp);
					while (@row = $sth{$_}->fetchrow_array) {
						$row[2] =~ s/:/!/g;
						$lemma = join('-', split(' ', decode('utf8',$row[0])));
						push(@comp_hyps, {lemma => $lemma, tag => decode('utf8',$row[1]), flex => decode('utf8',$row[2])});
					}
				}
				if (@comp_hyps) {
					$analysis_found = 1;
					my @hyps = ();
					push(@hyps, [@comp_hyps]);
					@hyps = reverse(@hyps);
					my @hyps_all = @{createHypothesis(\@hyps)};
					$tmp.=  '_NEO'.' [|Assoc(spa):'.join(';Assoc(spa):', @hyps_all).'|]';
				}
			}

			# Formes en X-Y-Z [...]
			if (not $analysis_found) {
				my @hyps = ();
				foreach my $comp (@comps) {
					my @comp_hyps = ();
					for (keys %sth) {
						$sth{$_}->execute($comp);
						while (@row = $sth{$_}->fetchrow_array) {
							$row[2] =~ s/:/!/g;
							my $tag = decode('utf8',$row[1]);
							if ($tag !~ /^(AUX|CF|V_Y)/) {
								$lemma = decode('utf8',$row[0]);
								$lemma =~ s/-*$//;
								push(@comp_hyps, {lemma => $lemma, tag => $tag, flex => decode('utf8',$row[2])});
							}
						}
					}
					if (not @comp_hyps) {
						last;
					} else {
						push(@hyps, [@comp_hyps]);
					}
				}
				if ($#hyps == $#comps) {
					$analysis_found = 1;
					@hyps = reverse(@hyps);
					my @hyps_all = @{createHypothesis(\@hyps)};
					$tmp.=  '_NEO'.' [|Assoc(sep):'.join(';Assoc(sep):', @hyps_all).'|]';
				}
			}
			
			# Formes en Xo-Y
			if (not $analysis_found) {
				if (@comps == 2) {
					# Vérifie que X se termine en o et a une taille minimale
					my $comp_x = $comps[0];
					if ($comp_x =~ /o$/ and length($comp_x) > 3) {
						# Vérifie que Y est dans la base
						my $comp_y = $comps[1];
						my @comp_y_hyps = ();
						for (keys %sth) {
							$sth{$_}->execute($comp_y);
							while (@row = $sth{$_}->fetchrow_array) {
								$row[2] =~ s/:/!/g;
								$lemma = decode('utf8',$row[0]);
								push(@comp_y_hyps, {lemma => $lemma, tag => decode('utf8',$row[1]), flex => decode('utf8',$row[2])});
							}
						}
						if (@comp_y_hyps) {
							# Regarde les préfixes de X (jusque 50%) qui sont préfixe de formes (de préférence ADJ) de taille minimale
							my $comp_x_prefix = $comp_x;
							my $comp_x_prefix_min = length($comp_x_prefix)/2;
							$comp_x_prefix = $comp_x_prefix.'-_';
							my @comp_x_hyps = ();
							my $comp_x_hyps_adj = 0;
							my $comp_x_hyps_len = 0;
							while (not @comp_x_hyps and length($comp_x_prefix) > $comp_x_prefix_min) {
								for (keys %sth) {
									$sthl{$_}->execute($comp_x_prefix.'%');
									while (@row = $sthl{$_}->fetchrow_array) {
										$row[2] =~ s/:/!/g;
										my $tag = decode('utf8',$row[1]);
										my $tag_adj = $tag =~ /^ADJ/;
										my $lemma = decode('utf8',$row[0]);
										$lemma =~ s/-*$//;
										my $lemma_length = length($lemma);
										my $flex = decode('utf8',$row[2]);
										if (not $comp_x_hyps_len or not $comp_x_hyps_adj and $tag_adj or $comp_x_hyps_adj == $tag_adj and $comp_x_hyps_len > $lemma_length) {
											$comp_x_hyps_adj = $tag_adj;
											$comp_x_hyps_len = $lemma_length;
											@comp_x_hyps = ();
										}
										if ($comp_x_hyps_adj == $tag_adj and $comp_x_hyps_len == $lemma_length) {
											push(@comp_x_hyps, {lemma => $comp_x, tag => $tag, flex => $flex});
										}
									}
								}
								$comp_x_prefix = substr($comp_x_prefix, 0, length($comp_x_prefix) - 1);
							}
							if (@comp_x_hyps) {
								$analysis_found = 1;
								my @hyps = ();
								push(@hyps, [@comp_x_hyps]);
								push(@hyps, [@comp_y_hyps]);
								@hyps = reverse(@hyps);
								my @hyps_all = @{createHypothesis(\@hyps)};
								$tmp.=  '_NEO'.' [|Assoc(pref):'.join(';Assoc(pref):', @hyps_all).'|]';
							}
						}
					}
				}
			}
		}
		$tmp.=  _INC_.$token if not $analysis_found
	}
	$tmp.=  "$_\n";
}
$tmp=~s/\|\] *\[\|/;/g;
print $tmp;


sub createHypothesis {
	@hyps = @{$_[0]};
	my @hyps_current = @{pop(@hyps)};
	my @hyps_suffix = $#hyps >= 0 ? @{createHypothesis(\@hyps)} : ('');
	my @hyps_current_suffix = ();
	foreach my $hyp_suffix (@hyps_suffix) {
		foreach my $hyp_current (@hyps_current) {
			$hyp_current_suffix = $hyp_current->{lemma};
			$hyp_current_suffix .= '-' if length($hyp_suffix);
			$hyp_current_suffix .= $hyp_suffix;
			$hyp_current_suffix .= ':'.$hyp_current->{flex}.':'.$hyp_current->{tag} if not length($hyp_suffix);
			push(@hyps_current_suffix, $hyp_current_suffix);
		}
	}
	return \@hyps_current_suffix;
}

