#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;
use Encode;
#------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------
# CHARGEMENT de la base de données et des fichiers qu'on veut utiliser et stockage des données
#------------------------------------------------------------------------------------------------------
$|=1; 

my $lang = 'fr';
my $dir_Lefff_mdb;
my $dir_Lefff;
while (1) {
	$_=shift;
	if (!$_ || /^$/) {last;}
	elsif (/^-d$/) {$dir_Lefff_mdb=shift;} 
	elsif (/^-dirLefff$/) {$dir_Lefff=shift;}
	elsif (/^-l/) {$lang = shift;}
}

die "### ERROR: lemmatize_neo.pl requires using option -d" if ($dir_Lefff_mdb eq "");


# On ouvre dico Lefff
my $lefff_dbh = DBI->connect("dbi:SQLite:$dir_Lefff_mdb/$lang.mdb", "", "", {RaiseError => 1, AutoCommit => 1});
#my $lefff_sth= $lefff_dbh->prepare('SELECT DISTINCT wordform FROM data WHERE lemma=? AND tag=?;');
my $lefff_exist =  $lefff_dbh->prepare('select EXISTS (select * FROM data WHERE wordform LIKE ?);');
my $lefff_count= $lefff_dbh->prepare('SELECT COUNT (DISTINCT wordform) FROM data WHERE wordform LIKE ?;');
my %lefff_exist_cache;
my %lefff_count_cache;

#------------------------------------------------------------------------------------------------------
my %bestLemme;

while (<>) {
	# formattage
	chomp;
	if (/ ?(_XML|_MS_ANNOTATION|_PAR_BOUND)/) {
		print "$_\n";
		next;
	}
	s/^\s*/ /o;
	s/\s$/ /o;
	s/ ?([^\\])\(([^<])/$1 \( $2/go;
	s/ ?([^\\])\)([^<])/$1 \)$2/go;
	s/([^\\])\)/$1 \)/g; 
	s/Ana *\( *([^\)]+?) *\) */Ana\($1\)/g;

	my $tmp = $_;
	#pour chaques néologismes étudié dont on ne connait pas le lemme:
	my ($token,$flexion, $cat);
	while ($tmp =~ s/(\{<F id="[EF0-9]+">([^\}]*)<\/F>\} _INC_([^ ]+)_([^ ]+) *\[\|[^\|]+:::([^\|]+)\|\])//) {
		$allSequence=$1;
		$token=$2;
		$cat=$3;
		$flexion=$4;
		$token = $2;
		$catconv = convertCat($cat);
		#on cherche le lemme correspondant
		#on récup le résultat:
		my $resultat;
		if (exists $bestLemme{$token."_".$cat."_".$flexion}) {
			$resultat = $bestLemme{$token};
		} else {
			$resultat =`echo '$token\t\t$flexion\t$catconv' | $dir_Lefff/morpho.fr.rev|cut -f2,4|uniq`;
			$resultat =  encode_UTF8($resultat);
			chomp($resultat);
			#si on trouve plusieurs résultats on réduit le champs à un resultat.
			if ($resultat =~ m/\n/) {
				$resultat = getBestLemme($resultat,$cat, $flexion);
				$bestLemme{$token."_".$cat."_".$flexion}=$resultat;
			}
		}
		#si résultat vide ou si résultat contient ŋ on ignore sinon: 
		if ($resultat ne "" and $resultat !~ m/ŋ/) { 
			$seq = quotemeta($allSequence);
			my $allSequenceAcLemma = $allSequence;
			if ($resultat=~ m/^([^\t]+)\t(.+)$/) {
				my $lemma=$1;
				my $pos=$2;		
				$pos =~ s/:/!/;
				$allSequenceAcLemma =~ s/:::/\:$lemma\:$pos\:/;
				$_ =~ s/$seq/$allSequenceAcLemma/ ;
			}
		}
	}
	# sortie 
	s/ +/ /go;
	s/( ?[^\\])\(([^<] ?)/$1 \($2/go;
	s/( ?[^\\])\)($|[^<:] ?)/$1\) $2/go; 
	s/Ana *\( *([^\)]+?) *\) */Ana\($1\)/g;
	s/ +/ /go;
	s/^ //;
	s/ $//;
	print "$_\n";
}

##########################################################################################################
# MÉTHODES
##########################################################################################################
sub	encode_UTF8{	
	my ($mot)= @_;
	$mot = Encode::decode("utf-8",$mot);
	return $mot;
}

sub	existe_suffixe_inLefff{	
	my ($mot)= @_;
	return $lefff_exist_cache{$mot} if (exists($lefff_exist_cache{$mot}));
	$lefff_exist->execute("$mot");
	$lefff_exist_cache{$mot}=$lefff_exist->fetchrow;
	return $lefff_exist_cache{$mot};
}

sub	compte_occ_suffixe_inLefff{	
	my ($mot)= @_;
	return $lefff_count_cache{$mot} if (exists($lefff_count_cache{$mot}));
	$lefff_count_cache{$mot}=$lefff_count->fetchrow;
	return $lefff_count_cache{$mot};
}

sub	convertCat{	
	my ($cat)= @_;
	$cat =~ s/^ADJ/adj/;
	$cat =~ s/^ADV/adv/;
	$cat =~ s/^V/v/;
	$cat =~ s/^NC/nc/;
	$cat =~ s/^DET/det/;
	$cat =~ s/^NP/np/;
	return $cat;
}

sub	getBestLemme{	
	my ($resultat, $cat, $flexion)= @_;
	my @allres = split('\n',$resultat);
	#si nc avec juste deux propositions 
	my $lemme1=$allres[0];
	my $lemme2=$allres[1];	
	$lemme1 =~ s/\t.+$//;
	$lemme2 =~ s/\t.+$//;
	if ($cat eq "NC" and $flexion =~ m/p$/ and scalar(@allres)==2 and ($allres[0] eq $allres[1]."s" or $allres[1] eq $allres[0]."s")){
		my $candi = shift(@allres);
		$candi=~ s/s$//o;
		return $candi;
	}
	
	my ($candi1,$candi2);
	$candi1 = shift(@allres);
	#on gère cas du ŋ
	while($candi1 =~ m/ŋ/ and @allres>0){
		$candi1 = shift(@allres);
	}	
	return "" if($candi1 =~ m/ŋ/);
	
	my $lemmeCandi1=$candi1;
	$lemmeCandi1 =~ s/\t.+$//;
	
	my $tailleMin=1;
	#parcourS tous les candis possibles
	while (@allres>0){
		$candi2 = shift(@allres);#on recup candi2
		#si candi2 n'a pas de ŋ
		next if($candi2 =~ m/ŋ/);
		my $lemmeCandi2=$candi2;
		$lemmeCandi2 =~ s/\t.+$//;
		my $tailleMax=length($candi1);
		
		$tailleMax=length($lemmeCandi2) if (length($lemmeCandi2)<length($lemmeCandi1));
		#on recup suffixe commun
		my $idx=$tailleMin;
		while ($idx<$tailleMax and substr($lemmeCandi1,length($lemmeCandi1)-$idx) eq substr($lemmeCandi2,length($lemmeCandi2)-$idx)){
			$idx+=1;
		}		
		#On récupère ensuite le candidat qui a le plus long suffixe existant:
		($candi1, $tailleMin) = getbestCandidatLemme($lemmeCandi1, $candi1, $lemmeCandi2,$candi2, $idx, $tailleMax);
	}
	return $candi1; 
}

sub getbestCandidatLemme{
	#compare deux candidats lemmes dans le but de trouver le meilleur 
	#qd on a trouvé:
	#on renomme le candidat qui a le plus long suffixe $candi1 
	#on boucle (en conservant le suffixe max pour pas recalculer les suff plus petits)
	my ($candi1, $candiTotal1, $candi2,$candiTotal2, $idx, $tailleMax)=@_;
	my $exist_Candi1 = existe_suffixe_inLefff("%".substr($candi1,length($candi1)-$idx));
	my $exist_Candi2 = existe_suffixe_inLefff("%".substr($candi2,length($candi2)-$idx));	
	while ($idx<$tailleMax){
		if ($exist_Candi1 and $exist_Candi2){ #si les 2 existe on continue à chercher
			$idx+=1;
			$exist_Candi1 = existe_suffixe_inLefff("%".substr($candi1,length($candi1)-$idx));
			$exist_Candi2 = existe_suffixe_inLefff("%".substr($candi2,length($candi2)-$idx));
		}elsif($exist_Candi1){				#si un seul des deux existe on garde le plus long
			return ($candiTotal1,$idx);			
		}elsif($exist_Candi2){	
			return ($candiTotal2,$idx);			
		}else{ #si les 2 existent jusqu'au même niveau on prend le plus frequent
			$idx-=1;
			my $occ_suff1 = compte_occ_suffixe_inLefff("%".substr($candi1,length($candi1)-$idx));
			my $occ_suff2 = compte_occ_suffixe_inLefff("%".substr($candi2,length($candi2)-$idx));
			return ($candiTotal1,$idx) if ($occ_suff1>=$occ_suff2);
			return ($candiTotal2,$idx);
		}
	}
	$idx-=1;
	my $occ_suff1 = compte_occ_suffixe_inLefff("%".substr($candi1,length($candi1)-$idx));
	my $occ_suff2 = compte_occ_suffixe_inLefff("%".substr($candi2,length($candi2)-$idx));
	return ($candiTotal1,$idx) if ($occ_suff1>$occ_suff2);
	return ($candiTotal2,$idx); 
}
