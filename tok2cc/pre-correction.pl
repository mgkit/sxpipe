#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;

$|=1; 
my $lang = 'fr';
my $lib_dir = ""; # folder that contains morphological .mdb databases (e.g., fr.mdb)
my @precosimple_files = (); #Lexique de précorrection ajouté
my $moredic_dir = "";  #Dictionnaires ajoutés
my $morelex=0; 

while (1) {
	$_=shift;
	if (!$_ || /^$/) {last;}
	elsif (/^-(-)?h(elp)?$/) {
		print "Le script pre-correction a pour but de corriger les étirements, des décompositions, les fautes simples (annotés) et de détecter les sigles. Pour bien fonctionner il nécessite les options suivantes:
\t-l\tpermet de selectionner la langue voulu (optionnel: par défaut la langue selectionnée sera le français
\t-ad\tcorrespond au répertoire dans lequel est stocké le dictionnaire de référence(Lefff)
\t-addLex\tpermet d'ajouter un lexique (un mot par ligne)
\t-addPrecoMotUniq$\tcorrespond au fichier dans lequel sont stockées les corrections mot1 → mot2 ou regex1 → regex2 (l'option doit être rappelé pour chaque fichier concerné)\n";
	}
	elsif (/^-l$/) {$lang=shift;}
	elsif (/^-d$/) {$lib_dir=shift;}
	elsif (/^-corr_uniq_file/) { #correction mot→mot ou regex→regex
		my $a=shift; 
		push(@precosimple_files,$a)}
	elsif (/^-addLex$/) {
		$moredic_dir=shift;
		$morelex = 1; 
	}
}

if ($lib_dir eq "") {
  die "### ERROR: pre-correction.pl requires using option -d followed by the folder that contains morphological databases";
}
## hash de cache
my %is_in_lefff_cache;
my %is_in_morelex_cache;

#on ouvre la base de donnée du lefff
my $lefff_dbh;
$lefff_dbh = DBI->connect("dbi:SQLite:$lib_dir/$lang.mdb", "", "", {RaiseError => 1, AutoCommit => 1});
my $lefff_sth =  $lefff_dbh->prepare('select EXISTS (select * from data where wordform=?);');

#on ouvre la base de donnée des autres lexiques
#my $morelex_dbh;
#my $morelex_sth;
#if ($morelex){
#	$morelex_dbh = DBI->connect("dbi:SQLite:$moredic_dir/dic_viavoo.dat", "", "", {RaiseError => 1, AutoCommit => 1});
#	$morelex_sth = $morelex_dbh->prepare('select EXISTS (select * from data where wordform=?);');
#}

#on stocke les corrections pour les mots simples donnés par l'user
my %autocorrsimplenoregex;
my %autocorrsimpleregex;
if (@precosimple_files > 0){
	my $file;
	for $file (@precosimple_files){
		open (Fic, "<$file") || die "Problème à l\'ouverture $file : $!";;
		binmode (Fic, ":encoding(utf-8)");
		#binmode (Fic, ":utf8");
		while (<Fic>) {
  			chomp;
  			if(m/[\*\+\?\(\{\[]/){
  				m/^(.*)\t(.*)$/;
  				my $allpairespossibles = get2firstLetters($1);
  				my @allpairespossibles= @$allpairespossibles;
  				for $paire(@allpairespossibles){
  					$autocorrsimpleregex{$paire}{$1} = $2;
  				}
  			}else{
 				m/^(.*)\t(.*)$/;
				$autocorrsimplenoregex{$1} = $2;
			}
		}	
		close (Fic);
	}
}


while(<>){ #on parcourt le doc
# formattage
chomp;
if (/ ?(_XML|_MS_ANNOTATION|_PAR_BOUND) *$/) {
		print "$_\n";
		next;
}   
s/^\s*/ /o;
s/\s$/ /o;

s/( *[^\\])\(([^<])/ $1 \( $2/go;
s/( *[^\\])\)($|[^<])/ $1 \)$2/go;
s/$/ /go;


#Pré-correction
my $tmp = "$_ ";
$_="";

#for $bigramme (%auto_correction_double){
	#	@allmot =split (" ",$bigramme);
	#	for $motgram in $allmot {
	#	$regexmot1 = qr/$allmot[0]/i;
	#	$regexmot2 = qr/$allmot[1]/i;
	#
	#	if $tmp =~ m/({[^\}.]*\} $regexmot1) ({[^\}.]*\} $regexmot2)/{
	#		$tmp =~ s/$regexmot1 {[^\}.]*\} $regexmot2//;
	#
	#	}
	#	}
	#} 
	#faire algo récursif
	#on a un tableau des mots à trouver à la suite
	#while on trouve le premier mot :
	#on appelle une méthode qui prend la suite de la phrase et le tableau des mots restants à trouver
	#
	#méthode regarde par quoi commence la suite de phrase envoyée :
	# si espace alors mot l'un à la suite de l'autre 
	#si fin de parenthèse à peu près idem
	#si pipe on va à la fin de la parenthèse
	#si si parenthèse ouvrante on parcourt toutes les possibilités (mots précédés soit de ( soit de 
	#avant la parenthèse fermante
	#si mot trouvé on envoie la suite de la phrase + mots restants des mots à trouver
	#sinon false
	
chomp $tmp;
while ($tmp =~ s/(^ *([\(\| ]*) ? ?({[^\}]*\}) (.*?) +(\[\|[^\|]*\|\])? *([ \)]*) *)//) { #pour chaque mot sxpipe
	my $motsxpipe = $1; #forme sxpipe entière
	my $poncdebut = $2; #ponct avant
	my $original = $3; #première partie de la forme sxpipe (dans les {} )
	my $mot_original = $4; #seconde partie de la forme sxpipe (après les {} ){<F id="E9F13">auteur-compositeur-narrateur◀SEGEMPH</F>}
	my $mot = quotemeta($mot_original);
	$mot=~s/\\é/é/go;
	$mot=~s/\\è/è/go;
	my $refEN = $5; #référent d'une entité nommée
	my $poncfin = $6; #ponct après
	#print "$2 - $3 - $4 - $5 -\n";
	#print "debug : \n tmp rest : ".$tmp."\n motsxpipe: $motsxpipe \n";

	#si a été annoté précédemment comme mot décomposé
	if ($mot =~ m/_TMP_SEGEMPH/){
		#on prend le mot contenu entre accolades et on en récupère les deux corrections possibles
		$original =~ m/.*(\{[^>]+>([^<◀]+)[<◀][^\}]+\}).*/ ;#TODO
		my $motEntreAccolades = $1;
		my $motDecompInitial = $2 ;
		my $motDecompAcEspace ;#= $motDecompInitial;
		
		@allPartMot = split("-",$motDecompInitial);
		for $partMot (@allPartMot){
		 	if(is_in_lexique($partMot,$morelex)){
		 		$motDecompAcEspace.= $motEntreAccolades." ".$partMot." ";
		 	}else{
		 		$motDecompAcEspace.= $motEntreAccolades." _INC_".$partMot." ";	
		 	}
		} 
		#on modifie la forme sxpipe l'etiquetant et en ajoutant l'ambiguïté
		my $sxpipeDecompInit =  $motsxpipe ;
		$sxpipeDecompInit =~ s/_TMP_SEGEMPH/_INC_$motDecompInitial/;
		$motsxpipe = "\( $sxpipeDecompInit | $motDecompAcEspace \)";
		$motsxpipe =~ s/<\/F>/◀SEGEMPH<\/F>/go;

	####Pour tout les mots étiquetés Apostroph 
	}elsif ($mot =~ m/_GlueApos/){ #\{$2$3_$4$5__$2$3'$5\} _GlueApos
		$original=~m/.*\{[^>]+>([^<◀]+)__([^<◀]+)[<◀][^\}]+\}.*/;
		my $aposInit=$1;
		my $aposCorr=$2;
		$motsxpipe=~s/_GlueApos/$aposCorr/;
		$motsxpipe=~s/($aposInit)__$aposCorr/$aposInit◀GlueApos/;		
	}elsif ($mot =~ m/_AddApos/){ #\{$2__$2'\} _AddApos
		$original=~m/.*\{[^>]+>([^<◀]+)__([^<◀]+)[<◀][^\}]+\}.*/;
		my $aposInit=$1;
		my $aposCorr=$2;
		$motsxpipe=~s/_AddApos/$aposCorr/;
		$motsxpipe=~s/($aposInit)__$aposCorr/$aposInit◀AddApos/;
		
	}elsif ($mot =~ m/_SuppApos/){ #\{$2__$2'\} _AddApos
		$original=~m/.*\{[^>]+>([^<◀]+)__([^<◀]+)[<◀][^\}]+\}.*/;
		my $aposInit=$1;
		my $aposCorr=$2;
		$motsxpipe=~s/_SuppApos/$aposCorr/;
		$motsxpipe=~s/($aposInit)__$aposCorr/$aposInit◀SuppApos/;
		
	####Pour tout les mots étiquetés inconnu
	}elsif ($mot =~ m/_INC_/){
		my $motbis= $mot ;
		$motbis=~ s/_INC_//g;#mot sans étiquete _INC
		my $mot_originalbis= $mot_original ;
		$mot_originalbis=~ s/_INC_//g;#mot original sans étiquete _INC
		my $keypreco = havePreco($motbis,\%autocorrsimplenoregex,\%autocorrsimpleregex);
		if ($keypreco ne ""){ ##si mot a une précorrection unigramme possible 
			$motsxpipe = makeAutoCorrSimple($keypreco,$mot,$motsxpipe, \%autocorrsimplenoregex, \%autocorrsimpleregex,$poncdebut, $poncfin,$mot_original,$mot_originalbis) ;
		}elsif (uc($mot) ne $mot){  ##si mot pas en majuscule: On teste les étirements
			my $motsxpipetmp =$motsxpipe;
			$motsxpipetmp =~ s/^[\(\|]//g;
			$motsxpipetmp =~ s/[\)\|] *$//g;
			$motsxpipetmp =~ s/_INC_//g;
			my @allCorrections = ($motsxpipetmp);
			my $motsxpipeacEtirement = makeEtirement ($mot,$motbis, $motsxpipe,$poncdebut, $poncfin, \@allCorrections,$mot_original) ;
			$motsxpipe = $motsxpipeacEtirement if ($motsxpipeacEtirement =~ m/◀ETIR/);
		}elsif ($motbis =~ m/^[AEIOUY]{4,}$/ |$motbis =~ m/^[BCDFGHJKLMNPQRSTVWXZ]{3,}$/){   ##si mot de plus de 3 consonnes ou de plus de 4 voyelles en majuscules : SIGLE
			$motsxpipe = makeSigle($mot, $motsxpipe) ;		   
		}
	}
	$_ .=$motsxpipe;
}


# sortie 
s/ +/ /go;
s/( ?[^\\])\(([^<] ?)/$1 \($2/g;
s/( ?[^\\])\)($|[^<] ?)/$1\) $2/g; 
s/ +/ /go;
s/^ //o;
s/ $//o;
print "$_\n";
}


#on ferme la base de donnée du lefff
$lefff_sth->finish;
$lefff_dbh ->disconnect; # pour se déconnecter de la base de données en fin de programme.

#print STDERR "Correcteur: Détection pré-correction: done\n";

################################################################################
#  MÉTHODES
################################################################################

sub makeSigle {
	my ($mot,$motsxpipe)=@_;
	$motsxpipe =~ s/ $mot / _SIGLEM /g;
	return $motsxpipe;
}

sub havePreco{
	my ($motbis,$autocorrsimplenoregex,$autocorrsimpleregex)=@_;

	%autocorrsimplenoregex =%$autocorrsimplenoregex;
	%autocorrsimpleregex =%$autocorrsimpleregex;
	return $motbis if (exists ($autocorrsimplenoregex{$motbis}));
	$motbis =~ m/^(..)/;
	my $paireLettreMot = lc $1;
	if (exists($autocorrsimpleregex{$paireLettreMot}) ){
		for $corr (keys %{$autocorrsimpleregex{$paireLettreMot}}){
			return $corr if ($motbis =~ m/^$corr$/);
		}
	}	
	return "";
} 

sub makeAutoCorrSimple {
	my ($keypreco,$mot,$motsxpipe,$autocorrsimplenoregex,$autocorrsimpleregex,$ponctdebut, $ponctfin,$mot_original,$mot_originalbis) =@_;
	%autocorrsimplenoregex =%$autocorrsimplenoregex;
	%autocorrsimpleregex =%$autocorrsimpleregex;
	if(exists ($autocorrsimplenoregex{$keypreco})){
		if($autocorrsimplenoregex{$keypreco}=~m/_/){
			my $corrAutoCorr= $autocorrsimplenoregex{$keypreco};
			my @allcorrAutoCorr = split(/_/,$corrAutoCorr);
			my $sortieNorm="";
			for $elmtcorrAutoCorr (@allcorrAutoCorr){
				my $motsxpipe_tmp=$motsxpipe;
				$motsxpipe_tmp =~ s/ $mot / $elmtcorrAutoCorr /g ;
				$sortieNorm.=$motsxpipe_tmp;
			}
			$motsxpipe = $sortieNorm;
		}else{
			$motsxpipe =~ s/ $mot / $autocorrsimplenoregex{$keypreco} /g ;
		}
	}else{
		$mot_originalbis =~ m/^(..)/;
		my $paireLettreMot = lc $1;
		my $formevoulue =  $autocorrsimpleregex{$paireLettreMot}{$keypreco};
		my $regexsortie = get_res_regex($keypreco,$mot,$formevoulue);
		$motsxpipe =~ s/ $mot / $regexsortie /g ;
	}
	$motsxpipe =~ s/<\/F>\}/◀AUTOCORRSIMPLE<\/F>\}/go;
	return $motsxpipe;
}

sub get_res_regex{ #bug nessecairrement◀AUTOCORRSIMPLE</F>} nécessaire
	my ($regexentree,$mot,$motCorr)=@_;
	my $regexsortie=$motCorr;
	$mot=~ m/$regexentree/;
	my @var;
	push(@var,$1);
	push(@var,$2);
	push(@var,$3);
	push(@var,$4);
	push(@var,$5);
	push(@var,$6);
	push(@var,$7);
	push(@var,$8);
	push(@var,$9);
	push(@var,$10);
	for (my $i=0;$i<10; $i++){
		my $valdollard = $i+1;
		$regexsortie=~ s/\$$valdollard/$var[$i]/;
	}
	return $regexsortie;
}

sub makeEtirement {
	my ($mot,$motbis, $motsxpipe, $poncdebut, $poncfin, $allCorrections,$mot_original)=@_;
	my @allCorrections =@$allCorrections;
	my @lettres=split(//,$motbis);
	my $i=0;
	my $mot_corr = $motbis;
	my $lettrebefore="";
	my $isEtir=0;
	
	while ($i+2<=$#lettres){#on parcourt toutes les lettres du mots afin de détecter les étirements
		my $lettre1=$lettres[$i];
		my $lettre2=$lettres[$i+1];
		my $lettre3=$lettres[$i+2];
		#Si étirement détecté :
		if ($lettre1 eq $lettre2 && $lettre2 eq $lettre3 && $lettre1 ne $lettrebefore && $lettre1 !~ m/\\/){
			$isEtir=1;
			my $lettre_etir = $lettre1;#on stocke la lettre
			my @allCorrectionstmp = @allCorrections;
			my $correction;
			for $correction (@allCorrectionstmp){
				$correction=~ m/\} ([^ ]*)  ?.*$/ ;
				my $mottmp = $1; 
				
				#si lettre étirée ne peut pas être dédoublée dans un mot on la réduit directement en une lettre
				if ($lettre_etir =~ m/[âäàèêëïîöôüûùÿjxw]/ ) {
					$mot_corr =~ s/$lettre_etir{3,}/$lettre_etir/g ; 
					$correction =~ s/ $mottmp / $mot_corr /g;
					push (@allCorrections ,$correction) until ($correction ~~ @allCorrections);
				}else{ #if ($etir !~ m/$lettre_etir/){
				#on crée le mot avec la lettre étirée dédoublée ou non.
					my $mot_corr_uniq = $mottmp ;
					my $mot_corr_dedoub =$mottmp ; 
					my $sxpipe_corr_uniq = $correction ;
					my $sxpipe_corr_dedoub =$correction ;
					$mot_corr_uniq =~ s/$lettre_etir{3,}/$lettre_etir/ ;
					$mot_corr_dedoub =~ s/$lettre_etir{3,}/$lettre_etir$lettre_etir/ ; 
					$sxpipe_corr_uniq =~ s/ $mottmp / $mot_corr_uniq /g;
					$sxpipe_corr_dedoub =~ s/ $mottmp / $mot_corr_dedoub /g ; 

					#si les deux mots existent ou si aucun n'existe, on stocke l'ambiguïté
					if ((!is_in_lexique($mot_corr_uniq,$morelex) && (!is_in_lexique($mot_corr_dedoub,$morelex))) | (is_in_lexique($mot_corr_uniq,$morelex) && is_in_lexique($mot_corr_dedoub,$morelex))){
						$correction=$sxpipe_corr_uniq;
						push (@allCorrections ,$correction) until ($correction ~~ @allCorrections);		
						push (@allCorrections ,$sxpipe_corr_dedoub) until ($sxpipe_corr_dedoub ~~ @allCorrections);					
					#si seul un des deux mots existe on ne conserve que celui là
					}elsif (is_in_lexique($mot_corr_uniq,$morelex)){
						$correction =~ s/ $mottmp / $mot_corr_uniq /g;
						push (@allCorrections ,$correction) until ($correction ~~ @allCorrections);
					}else{
						$correction =~ s/ $mottmp / $mot_corr_dedoub /g;
						push (@allCorrections ,$correction) until ($correction ~~ @allCorrections);	
					}
				}
			}
		}
		$lettrebefore=$lettre1;
		$i++;
	}

	#on choisi quelles corrections on veut noter
	my @motsexistants =();
	my $corr;
	$poncdebutq = quotemeta($poncdebut);
	my $poncfinq = quotemeta($ponctfin);

	for $corr (@allCorrections){
		$corr=~ s/$poncdebutq/ / if $poncdebut;
		$corr=~ s/$poncfinq/ / if $ponctfin;
		$corr =~ m/\} ([^ ]*) .*$/;
		my $motcorrige = $1;
		my $corrNoINC = $corr;
		$corr =~ s/\} /\} _INC_/;
		#Si qu'une seule correction on la conserve
		if ($#allCorrections==0){  
		push(@motsexistants,$corr)  
		#Si plusieurs corrections possibles
		}elsif(is_in_lexique($motcorrige,$morelex)){
		push(@motsexistants,$corrNoINC);
		}
}

	@motsexistants = @allCorrections if ($#motsexistants<0);
	my $corrUniq = 0;
	$corrUniq = 1 if ($#motsexistants==0);
	$motsxpipe=$poncdebut;
	$motsxpipe.="( " if (!$corrUniq and !$poncdebut and !$poncfin );
	$motsxpipe.=join("\| ",@motsexistants);
	$motsxpipe.=") " if (!$corrUniq and !$poncdebut and !$poncfin );
	$motsxpipe.=$poncfin;
	$motsxpipe =~ s/<\/F>\}/◀ETIR<\/F>\}/g if $isEtir;

	return $motsxpipe;
}

sub is_in_lexique {
	my ($word,$morelex)=@_;
	#print $word." is in lexique?\n";
	if($morelex){
		return 1 if(is_in_morelex($word)) ; #pas inconnu car in autres lexiques
	}
	return $is_in_lefff_cache{$word} if (defined($is_in_lefff_cache{$word})) ;

	$lefff_sth->execute(lc $word);
	my $reponse = $lefff_sth->fetchrow;

	if ($reponse == 0){
		$is_in_lefff_cache{$word} = 0;
		return 0;
	}
	$is_in_lefff_cache{$word} = 1;
	return 1;
}

sub is_in_morelex {
	my $word = shift;
	return $is_in_morelex_cache{$word} if (defined($is_in_morelex_cache{$word}));
	$morelex_sth->execute(lc $word);
	my $reponse = $morelex_sth->fetchrow;
	$is_in_morelex_cache{$word} = $reponse;
	return $reponse;
}

sub getnextLetter{
	#retourne @firstlettres contenant la ou les premières lettres possibles dans une regex
	my ($mot) = @_;
	my $motlower = lc $mot; 
	my @firstlettres; 
	my %sortie;
	#on récupère la ou les premières lettres possibles	
	if ($motlower =~ m/^\(([^\)]+)\)/){
		$match=$1;
		print $match
		$aSupp = quotemeta($match);
		$motlower =~ s/^\($aSupp\)//;
		@alllettres =  split(/\|/, $match);
		for $lettre (@alllettres){			
			if ($lettre =~ /^.$/){
				$sortie{$motlower}{$lettre}=1;
			}else{
				if ($lettre =~ m/^\[([^\]]+)\](.+)?$/){					
					for $lettre2 (split("", $1) ){
						$sortie{"$2$motlower"}{$lettre2}=1;
					}
				}else{
					$lettre =~ m/^(.)(.+)$/;
					$sortie{"$2$motlower"}{$1}=1;					
				}				
			}
		}
		
 	}elsif ($motlower =~ m/^\(?\[([^\]]+)\]\)?/){
 		$match=$1;
		$aSupp = quotemeta($match);
		$motlower =~ s/^\(?\[$aSupp\]\)?//; 
		for $lettre (split("", $match) ){
			$sortie{$motlower}{$lettre}=1;
		}
 	}else{ 		
 		$motlower =~ m/^(.)/;
 		$match=$1;
 		$motlower =~ s/^$match//; 		
		$sortie{$motlower}{$match}=1;
 	}
 	return (\%sortie)
}

sub get2firstLetters{
	my ($mot) = @_;
	my @allKeys;
	#on recup motraccourci et @1ères lettres avec getnextLetter($mot)
	my $allresultats1 = getnextLetter($mot);
	my %allresultats1 = %$allresultats1;
	for $motdec (keys(%allresultats1)){
		my $oldmodec=$motdec;
		#on avise en fonction des quantifieurs (+*?{1,2}{2})
		if ($motdec =~ m/^(\+)/){
			for $lettre (keys %{$allresultats1{$motdec}}){
				push(@allKeys, "$lettre$lettre");
			}
			$motdec =~ s/^\+//;
		}elsif ($motdec =~ m/^(\*)/){
			for $lettre (keys %{$allresultats1{$motdec}}){
				push(@allKeys, "$lettre$lettre");
			}
			$motdec =~ s/^\*//;
			my $morekeys = get2firstLetters($motdec);
			push(@allKeys, @$morekeys);
		}elsif ($motdec =~ m/^\?/){
			$motdec =~ s/^\?//;
			my $morekeys = get2firstLetters($motdec);
			push(@allKeys, @$morekeys);
		}elsif ($motdec =~ m/^(\{[^\}]+\})/){
			my $aSupp = quotemeta($1);
			$motdec =~ s/^$aSupp//;
			my $morekeys = get2firstLetters($motdec);
			push(@allKeys, @$morekeys);
		}
		#on achève la clef
		my $boollettre2=0;
		while (!$boollettre2){
			my $allresultats2 = getnextLetter($motdec);
			my %allresultats2 = %$allresultats2;
			for $motdec2 (keys(%allresultats2)){
				$boollettre2=1;
				for $lettre1 ((keys %{$allresultats1{$oldmodec}})){
					for $lettre2 ((keys %{$allresultats2{$motdec2}})){
						push(@allKeys, "$lettre1$lettre2");
					}
				}
				#on avise en fonction des quantifieurs (*?)
				if ($motdec2 =~ m/^(\*|\?)/){
					$motdec = $motdec2;
					$motdec =~ s/^\?//;			
					$motdec =~ s/^\*//;
					$boollettre2=0;
				}
			}
		}
	}
	return \@allKeys;
}



