#!/bin/sh

#need uniquement un lexique format .lex sxpipe en utf8
dico=$1
path=$(pwd)
echo "On construit les documents necessaires"
cat $dico |cut -f1,3 |sort -u > lexique_tok_pos
cat $dico |cut -f5 |perl -pe "s/___.*$//" |sort -u > lexique_lemmes
lexique=$path/lexique_tok_pos
lexiqueLemme=$path/lexique_lemmes

#On extrait les affixes et on récupère les docs utiles pour la suite des calculs 
echo "_______________________________________________________________________________ "
echo " on extrait les affixes de la langue"
cd extractionAffixes
echo " on déplace/renomme les documents utiles pour la suite"
sh extractAffixes.sh  $lexique db.dat
cp 3.regleSuffixe ../
cp 3.reglePrefixe ../
cp db.dat ../
cd ..
mv 3.regleSuffixe regleSuffixe 
mv 3.reglePrefixe reglePrefixe

#On crée nos relations entre les mots d'une même famille et les POS qui les relies
echo "_______________________________________________________________________________ "
date
echo " 1. on extrait les relations entre les mots de la langue"
perl extractionReglesMorpho/2.MakePairesCandidats_lemmes.pl -pref $path/reglePrefixe -suff $path/regleSuffixe -sortie $path/1.db_regles_lemmes.dat -db $path/db.dat -lemmes $lexiqueLemme
date
echo " 2. on extrait les relations les POS des mots d'une même famille"
perl extractionReglesMorpho/4.keepPOS_Ac_typageRegle.pl -sortie $path/2.db_regles-POS-typageRegle -oldregles $path/1.db_regles_lemmes.dat -dico $dico
date
echo " 3. on relie chaque mot du dictionnaire avec les mots prédits comme de la même famille de lui."
perl extractionReglesMorpho/relieMotsMemeFamille.pl -dico $dico -regles $path/2.db_regles-POS-typageRegle -sortie $path/3.db_motsMFamille.dat
date
