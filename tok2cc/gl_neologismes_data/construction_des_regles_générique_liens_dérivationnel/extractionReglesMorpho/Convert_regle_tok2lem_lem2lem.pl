#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use locale;
use DBI;
use Encode;
 
$|=1; 

while (1) {
    $_=shift;
    if (!$_ || /^$/) {last;}
    elsif (/^-dico/) {$dico=shift;}   #prend dico format .lex encodé en UTF8
    elsif (/^-regles/) {$dbRegles=shift;}   #prend base de données de POS
    elsif (/^-sortie/) {$sortie=shift;}   #prend base de données qu'on veut sortir
}

#------------------------------------------------------------------------------------------------------
# Initialisation de la base de données et des fichiers qu'on veut utiliser et stockage des données
#------------------------------------------------------------------------------------------------------

# On ouvre la base de règles (mot1,mot2,pos1, pos2, pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle)
my $regles_dbh = DBI->connect("dbi:SQLite:$dbRegles", "", "", {RaiseError => 1, AutoCommit => 1});
my $regles_getnb= $regles_dbh->prepare('SELECT count(*) FROM data;');
my $regles_getRegles= $regles_dbh->prepare('SELECT * FROM data;');


# On ouvre la base de règles dans laquelle on veut écrire
if (-r $sortie) {
  print STDERR "  Erasing previous database $sortie\n";
  `rm $sortie`;
}
my $sortie_dbh = DBI->connect("dbi:SQLite:$sortie", "", "", {RaiseError => 1, AutoCommit => 1});
$sortie_dbh->do("CREATE TABLE data(mot1,mot2,pos1, pos2, pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle);");
my $sortie_sth = $sortie_dbh->prepare('INSERT INTO data(mot1,mot2,pos1, pos2, pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)');


#on conserve les mots du dico
my %lefff;
my @mots;
open (Fic, "<$dico") || die "Problème à l\'ouverture fichier contenant les mots : $!";;
binmode (Fic, ":encoding(utf8)");
while (<Fic>) {
 	chomp;
	m/^([^\t]*)\t[^\t]*\t([^\t]*)\t[^\t]*\t([^_]*)___+[^\t]*\t[^\t]*\t([^\t]*).*$/;	#à titre indicatif: $mot = $1; $cat = $2; $lemme=$3;$catflex = $4;
	
	my $cat = $2;
	if ($cat eq "advm" or $cat eq "advp"){
		$cat="adv";
	} 
	if ($cat ne "cf"){
		$lefff{"#".$1."#"}{$cat."_".$4}="#".$3."#"; #on stocke le mot et sa pos_classeflex avec lemme
		push(@mots, $1);
		#print "$1 - $2 - $4\n";
	}
}	
close (Fic);
print STDERR "dico End \n";

#---------------------------------------------------------------------------------------------------------------------
# Parcours des paires mots pos uniques et pour chacune d'entre elles, on récupère les préfixes suffixes les + longs
#---------------------------------------------------------------------------------------------------------------------
$regles_getnb->execute();
my $nbtotal = $regles_getnb->fetchrow;
my $i=0;
$regles_getRegles->execute();
while (($mot1,$mot2,$pos1, $pos2, $pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle) = $regles_getRegles->fetchrow){
	$i++;	
    	print STDERR "\r  Loading data...$i /$nbtotal";
	($mot1,$mot2,$pos1, $pos2, $pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle)=encode_UTF8($mot1,$mot2,$pos1, $pos2, $pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle);	
	if (exists $lefff{$mot1}{$pos1}){
		if($lefff{$mot1}{$pos1} eq $mot1){
			$sortie_sth->execute($mot1,$mot2,$pos1, $pos2, $pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle);
		}else{ 
			$sortie_sth->execute($lefff{$mot1}{$pos1},$mot2,$pos1, $pos2, $pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle);
		}
	}else{
		print "$mot1,$mot2,$pos1, $pos2, $pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle \n";
	}
} 

#------------------------------------------------------------------------------------------------------
# Fin du programme on ferme nos requêtes et on crée les indexs pour la base de données.
#------------------------------------------------------------------------------------------------------

$regles_getnb->finish;
$regles_getRegles->finish;
$regles_dbh->disconnect;

$sortie_sth->finish;

print STDERR "  Creating index...";
$sortie_dbh->do("CREATE INDEX idtypeRegle ON data(typeRegle);");
$sortie_dbh->do("CREATE INDEX idmot1 ON data(mot1);");
$sortie_dbh->do("CREATE INDEX idmot2 ON data(mot2);");
$sortie_dbh->do("CREATE INDEX idpos1 ON data(pos1);");
$sortie_dbh->do("CREATE INDEX idpos2 ON data(pos2);");
print STDERR "done\n";
$sortie_dbh->disconnect;
print STDERR "\r  Loading data...\n";

################################################################################
#  MÉTHODES 
################################################################################


sub	encode_UTF8{	
	my ($mot1,$mot2,$pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle)= @_;
	$mot1 = Encode::decode("utf-8",$mot1);
	$mot2 = Encode::decode("utf-8",$mot2);
	$pos1 = Encode::decode("utf-8",$pos1);
	$pos2 = Encode::decode("utf-8",$pos2);
	$pref1 = Encode::decode("utf-8",$pref1);
	$suff1 = Encode::decode("utf-8",$suff1);
	$pref2 = Encode::decode("utf-8",$pref2);
	$suff2 = Encode::decode("utf-8",$suff2);
	$lettrePref = Encode::decode("utf-8",$lettrePref);
	$lettreSuff = Encode::decode("utf-8",$lettreSuff);
	$nbOcc = Encode::decode("utf-8",$nbOcc);
	$typeRegle = Encode::decode("utf-8",$typeRegle);
	return ($mot1,$mot2,$pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle);
}
