#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;
use Encode;
 
$|=1; 

#------------------------------------------------------------------------------------------------------
# Initialisation de la base de données et des fichiers qu'on veut utiliser et stockage des données
#------------------------------------------------------------------------------------------------------


$|=1; 
while (1) {
    $_=shift;
    if (!$_ || /^$/) {last;}
    elsif (/^-suff$/) {$fileSuffixes=shift;}   
    elsif (/^-pref$/) {$filePrefixes=shift;} 
    elsif (/^-db$/) {$dbpath=shift;}
    elsif (/^-sortie$/) {$sortie=shift;}   #"/home/marion/Analyse_Morpho/3.db_regles_lemmes.dat"  
    elsif (/^-lemmes$/) {$lemmes=shift;}  

}

if ($fileSuffixes eq "") {
  die "### ERROR: 2.MakeCandidats_Lemmes.pl requires using option -suff followed by the file that contains suffix";
}elsif($filePrefixes eq "") {
  die "### ERROR: 2.MakeCandidats_Lemmes.pl requires using option -pref followed by the file that contains prefix";
}elsif($sortie eq "") {
  die "### ERROR: 2.MakeCandidats_Lemmes.pl requires using option -sortie followed by the output file";
}

#On donne chemin des fichiers contenant les affixes

#On prépare la base de données
my $db = shift || $sortie;

if (-r $db) {
  print STDERR "  Erasing previous database $db\n";
  `rm $db`;
}
my $dbh = DBI->connect("dbi:SQLite:$db", "", "", {RaiseError => 1, AutoCommit => 0});
$dbh->do("CREATE TABLE data(mot1,mot2,pref1,suff1,pref2,suff2,lettrePref,lettreSuff);");
my $sth=$dbh->prepare('INSERT INTO data(mot1,mot2,pref1,suff1,pref2,suff2,lettrePref,lettreSuff) VALUES (?,?,?,?,?,?,?,?)');
my $l = 0;
print STDERR "  Loading data...\n";

#on stocke les suffixes donnés
my %suffixes;
my $i = 0;
open (Fic, "<$fileSuffixes") || die "Problème à l\'ouverture $fileSuffixes : $!";;
binmode (Fic, ":encoding(utf8)");
while (<Fic>) {
 	chomp; 
	m/^ *([0-9]+)\t(.*)\t(.*)\t\[(.*)\]$/;#######recup $4
	if ($1>40){
		$i+=1;
		$suffixes{$2}{$4}{$3}=1;
	}	
}
close (Fic);

print STDERR "Suffixes End \n";

#on stocke les préfixes donnés
my %prefixes;
my $j = 0;
open (Fic, "<$filePrefixes") || die "Problème à l\'ouverture $filePrefixes : $!";;
binmode (Fic, ":encoding(utf8)");
while (<Fic>) {
 	chomp; 
	m/^ *([0-9]+)\t(.*)\t(.*)\t\[(.*)\]$/;#######recup $4
	if ($1>40){
		$j+=1;
		$prefixes{$2}{$4}{$3}=1;
		#$prefixes{quotemeta($2)}{quotemeta($3)}=1;
		#print $j." ".$1." ".$2." ".$3."\n"; 
	}	
}
close (Fic);
print STDERR "Préfixes End \n";

#on ouvre la base de donnée du lefff
my $lefff_dbh;
$lefff_dbh = DBI->connect("dbi:SQLite:$dbpath", "", "", {RaiseError => 1, AutoCommit => 1});
my $lefff_getkey = $lefff_dbh->prepare('SELECT key FROM data WHERE (value="nc" or value="adj" or value="v" or value="adv" or value="advm" or value="advp" or value="V" or value="N" or value="A" or value="ADV" or value="ADJ");');

#On Stocke le lefff
my $mot;
my %lefff;
if (1) {
    $lefff_getkey->execute(); 
    while ($mot =  $lefff_getkey->fetchrow){
		$mot = Encode::decode("utf-8", $mot);	
		$lefff{$mot} = 1;
    }
    print STDERR "Lefff End \n";
}

#on conserve les lemmes du lefff
my %lemmes;
open (Fic, "<$lemmes") || die "Problème à l\'ouverture fichier contenant les lemmes : $!";;
binmode (Fic, ":encoding(utf8)");
while (<Fic>) {
 	chomp;
	m/^(.*)$/;
	$lemmes{$1} = 1; #on stocke le lemme
}	
close (Fic);
    print STDERR "Lemmes End \n";
#------------------------------------------------------------------------------------------------------
# Parcours des mots du lefff pour créer des paires de candidats avec leurs regex correspondantes.
#------------------------------------------------------------------------------------------------------

#On parcourt les mots pour trouver les candidats
#my %pairesCandidats;
my $nbmots = keys(%lefff);
my $numMot=0;
for $mot (keys(%lefff)){
	$numMot+=1;
	#if ($numMot<2001){
		my $motNett =$mot;	
		#On reconstruit le mot
		$mot = "#".$mot."#";#quotemeta("#".$mot."#");

		#on recupère les affixes possibles pour notre mot
		my ($havePrefixe,$prefixesref) = getPrefixe($mot);
		my ($haveSuffixe,$suffixesref) = getSuffixe($mot);
		my @prefixes = @$prefixesref;
		my @suffixes = @$suffixesref;
		#print "$mot  $havePrefixe $haveSuffixe  \n";
	
		#On teste si mot peut avoir des préfixes et des suffixes. Et si oui on les teste puis on les stocke:
		my %candidats;
		#On teste si mot peut avoir des préfixes seuls :
		if ($havePrefixe){
			for $tabpref (@prefixes){
				@tabprefbis = @$tabpref;
				my $prefixe = $tabprefbis[0];
				my $prefixe2 = $tabprefbis[1];
				my $lettreContexte = $tabprefbis[2];
				my $candi = $mot;
				$candi =~ s/^($prefixe)/$prefixe2/;				
				$candi =~ s/^#//;	
				$candi =~ s/#$//;	
				#print "$prefixe\t $prefixe2\t $mot \t$candi\n";
								
				if (exists($lemmes{$candi})){
					addPaire($motNett, $candi,$prefixe,"",$prefixe2,"",$lettreContexte,"" );
					#print "\t".$motNett." ".$candi."\n";
					$l+=1;
					if ($l % 10000 == 0) {
    					print STDERR "\r  Loading data...$l \t $numMot/$nbmots";
   					$dbh->commit;
					}
				}
			}
		}
	
		#On teste si mot peut avoir des suffixes seuls:
		if($haveSuffixe) {
			for $tabsuf (@suffixes){
				@tabsufffbis = @$tabsuf;
				my $suffixe = $tabsufffbis[0];
				my $suffixe2 = $tabsufffbis[1];
				my $lettreContexteSuff = $tabsufffbis[2];
				my $candi = $mot;
				$candi =~ s/$suffixe$/$suffixe2/;
				my $candilemmes=$candi;
				$candilemmes =~ s/^#//;	
				$candilemmes =~ s/#$//; 
#				print "$suffixe\t $suffixe2\t $mot \t$candi\n";
				if (exists($lemmes{$candilemmes})){
					addPaire($motNett, $candilemmes,"", $suffixe,"",$suffixe2,"",$lettreContexteSuff );
					#print "\t".$motNett." ".$candilemmes."\n";
					$l+=1;
					if ($l % 10000 == 0) {
  						print STDERR "\r  Loading data...$l \t $numMot/$nbmots";
  						$dbh->commit;
					}
				}
		
				#On teste si mot avec le suffixe peut être accompagné de préfixes:
				if ($havePrefixe){
					for $tabpref (@prefixes){
						@tabprefifbis = @$tabpref;
						my $prefixe = $tabprefifbis[0];
						my $prefixe2 = $tabprefifbis[1];
						my $lettreContextePref = $tabprefifbis[2];
						my $candibis = $candi;
						$candibis =~ s/^$prefixe/$prefixe2/;				
						$candibis =~ s/^#//;	
						$candibis =~ s/#$//;					
						if (exists($lemmes{$candibis})){
							addPaire($motNett, $candibis,$prefixe, $suffixe, $prefixe2,$suffixe2,$lettreContextePref,$lettreContexteSuff);
							#print "\t".$motNett." ".$candibis."\n";
							$l+=1;
							if ($l % 10000 == 0) {
  								print STDERR "\r  Loading data...$l \t $numMot/$nbmots";
  								$dbh->commit;	
							}
						}
					}
				}
			}
		}
	#}
}
	
#------------------------------------------------------------------------------------------------------
# Fin du programme on ferme nos requêtes et on crée les index pour la base de données.
#------------------------------------------------------------------------------------------------------

print STDERR "\r  Loading data...$l\n";
$dbh->commit;
$sth->finish;
print STDERR "  Creating index...";
$dbh->do("CREATE INDEX indMot ON data(mot1);");
$dbh->do("CREATE INDEX indPref ON data(pref1);");
$dbh->do("CREATE INDEX indSuff ON data(suff1);");
$dbh->do("CREATE INDEX pref1_suff1 ON data(pref1,suff1);");

$dbh->commit;
print STDERR "done\n";
$dbh->disconnect;
################################################################################
#  MÉTHODES
################################################################################

sub getPrefixe{
	my ($mot) = @_;
	my @prefixes=();
	my $havePrefixe=0;
	my $prefixe;
	for $prefixe (keys(%prefixes)){
		if ($mot =~ m/^$prefixe/){
			for $lettresComp (keys %{$prefixes{$prefixe}}){
				my $pref = qr/$prefixe ?[$lettresComp]/;
				##print "Préfixe: $prefixe $lettresComp .......$pref \n";
				my $lettreContexte=$lettresComp;
				if (length($lettresComp)>=9){
					$pref = qr/$prefixe/;
					$lettreContexte="A";
				}elsif (length($lettresComp)>=4 and $lettresComp =~/^[aàâäeéèêëiîïoôöuùûüy]+$/){
					$pref = qr/^${prefixe}[aàâäeéèêëiîïoôöuùûüy]/; 
					$lettreContexte="V";
				}elsif (length($lettresComp)>=4 and $lettresComp =~/^[bcçdfghjklmnpkrstvwxz]+$/){
					$pref = qr/^${prefixe}[bcçdfghjklmnpkrstvwxz]/; 
					$lettreContexte="C";
				}
				if ($mot =~ m/^$pref/){
					my $prefixe2;
					for $prefixe2 (keys %{$prefixes{$prefixe}{$lettresComp}}){
						push(@prefixes, [$prefixe,$prefixe2,$lettreContexte]);
						#print "!!!!! $prefixe\t$prefixe2\t$lettresComp\t $lettreContexte\n";
					}
					$havePrefixe = 1;
				}
			}
		}
	}
	return ($havePrefixe,\@prefixes);
}

sub getSuffixe{
	my ($mot) = @_;
	my @suffixes=();
	my $havesuffixe = 0;
	my $suffixe;

	for $suffixe (keys (%suffixes)){
		#print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! $suffixe\n";

		if ($mot =~ m/$suffixe$/){
			
			for $lettresComp (keys %{$suffixes{$suffixe}}){
				my $suff = qr/[$lettresComp]$suffixe$/;

				my $lettreContexte = $lettresComp;
				
				if (length($lettresComp)>=9){
					$suff = qr/$suffixe/;
					$lettreContexte="A";
				}elsif (length($lettresComp)>=4 and $lettresComp =~/^[aàâäeéèêëiîïoôöuùûüy]+$/){
					$suff = qr/[aàâäeéèêëiîïoôöuùûüy]$suffixe$/; 
					$lettreContexte = "V";
				}elsif (length($lettresComp)>=4 and $lettresComp =~/^[bcçdfghjklmnpkrstvwxz]+$/){
					$suff = qr/[bcçdfghjklmnpkrstvwxz]$suffixe$/; 
					$lettreContexte= "C"
				}
				if ($mot =~ m/$suff/){
					my $suffixe2;
					for $suffixe2 (keys %{$suffixes{$suffixe}{$lettresComp}}){
						push(@suffixes, [$suffixe,$suffixe2,$lettreContexte]);
#						print ">>> $suffixe, $suffixe2\n";
					}
					$havesuffixe = 1; 
				}
			}
		}
	}
	return ($havesuffixe,\@suffixes);
}

 sub addPaireBefore {
	my ($mot1, $mot2, $pref1, $suff1, $pref2, $suff2, $lettreContextepref,$lettreContextesuff)= @_;
	#print "---> $mot1, $mot2, $pref1, $suff1, $pref2, $suff2,$lettreContextepref,$lettreContextesuff \n";
	if ($lettreContextepref eq "") {$lettreContextepref="A"}
	if ($lettreContextesuff eq "") {$lettreContextesuff="A"}
	my @contextesP = split("", $lettreContextepref);
	my @contextesS = split("", $lettreContextesuff);
	if (($lettreContextepref eq "A") and ($lettreContextesuff eq "A")){
		$sth->execute($mot1, $mot2, $pref1, $suff1, $pref2, $suff2, "", "");
		#print "1 $mot1, $mot2, $pref1, $suff1, $pref2, $suff2,  ,  \n";
	}elsif ($lettreContextepref eq "A"){
		my $contexteS;
		for $contexteS (@contextesS){
			$sth->execute($mot1, $mot2, $pref1, $suff1, $pref2, $suff2, "", $contexteS);
			#print "2 $mot1, $mot2, $pref1, $suff1, $pref2, $suff2,  , $contexteS \n";
		}
	}elsif ($lettreContextesuff eq "A"){
		my $contexteP;
		for $contexteP (@contextesP){
			$sth->execute($mot1, $mot2, $pref1, $suff1, $pref2, $suff2, $contexteP, "");
			#print "3 $mot1, $mot2, $pref1, $suff1, $pref2, $suff2, $contexteP ,  \n";
		}
	}else{
		my $contexteP;
		for $contexteP (@contextesP){
			my $contexteS;
			for $contexteS (@contextesS){
				$sth->execute($mot1, $mot2, $pref1, $suff1, $pref2, $suff2, $contexteP, $contexteS);
				#print "4 $mot1, $mot2, $pref1, $suff1, $pref2, $suff2, $contexteP, $contexteS \n";				
			}
		}
	}
} 


sub addPaire {
	
	my ($mot1, $mot2, $pref1, $suff1, $pref2, $suff2, $lettreContextepref,$lettreContextesuff) = @_;
	if ($lettreContextepref eq "") {$lettreContextepref="A"}
	if ($lettreContextesuff eq "") {$lettreContextesuff="A"}
	
	if (not(($lettreContextepref eq "C") or ($lettreContextepref eq "V") or ($lettreContextepref eq "A"))){
	    my $mot1bis = "#".$mot1;
	    $mot1bis =~ m/^$pref1(.).*$/;
	    $lettreContextepref=$1;
	    if ($lettreContextepref eq "#"){
		$lettreContextepref="";
	    }
	}
	if (not(($lettreContextesuff eq "C") or ($lettreContextesuff eq "V") or ($lettreContextesuff eq "A"))){
	    my $mot1bis = $mot1."#";
	    $mot1bis =~ m/^.*(.)$suff1$/;	   
	    $lettreContextesuff=$1;
	    if ($lettreContextesuff eq "#"){
		$lettreContextesuff="";
	    }
	}
	$sth->execute($mot1, $mot2, $pref1, $suff1, $pref2, $suff2, $lettreContextepref, $lettreContextesuff);
#       print "$mot1, $mot2, $pref1, $suff1, $pref2, $suff2, $lettreContextepref, $lettreContextesuff \n";				
} 
