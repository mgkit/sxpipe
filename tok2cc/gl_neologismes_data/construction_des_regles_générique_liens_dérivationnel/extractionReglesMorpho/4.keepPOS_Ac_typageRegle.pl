#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;
use Encode;

$|=1; 

while (1) {
    $_=shift;
    if (!$_ || /^$/) {last;}
    elsif (/^-oldregles$/) {$oldregles=shift;}
    elsif (/^-sortie$/) {$sortie=shift;}   #"/home/marion/Analyse_Morpho/3.db_regles_lemmes.dat"  
    elsif (/^-dico$/) {$dico=shift;}  
}

if($sortie eq "") {
  die "### ERROR: 2.MakeCandidats_Lemmes.pl requires using option -sortie followed by the output file";
}

#on conserve les lemmes du lefff
my %lefff;
my %lefffLemme;
my %dicomotLemme;
open (Fic, "<$dico") || die "Problème à l\'ouverture fichier contenant les lemmes : $!";;
binmode (Fic, ":encoding(utf8)");
while (<Fic>) {
 	chomp;
	m/^([^\t]*)\t[^\t]*\t([^\t]*)\t[^\t]*\t([^\t]*?)__+[0-9]+\t[^\t]*\t([^\t]*).*$/;	#à titre indicatif: $mot = $1; $cat = $2; $lemme=$3 $catflex = $4;
	$lefff{$1}{$2."_".$4} = 1; #on stocke le lemme et sa pos_classeflex
	$dicomotLemme{$1}{$3} = 1;
	my $cat=$2;
	if ($1 eq $3){
		if ($cat ne "cf"){
			if ($cat eq "advm" or $cat eq "advp"){
				$cat="adv";
			}
			$lefffLemme{$1}{$cat."_".$4} = 1; #on stocke le lemme et sa pos_classeflex
		}
		#print "$1 - $cat - $3-$4\n";
	#}else{
		#print "$1 - $cat - $3-$4\n";
	}
}	
close (Fic);

#on ouvre la base de donnée des candidats
my $candidats_dbh;
$candidats_dbh = DBI->connect("dbi:SQLite:$oldregles", "", "", {RaiseError => 1, AutoCommit => 1});
my $candidats_getall= $candidats_dbh->prepare('SELECT * FROM data ;');


#On prépare la base de données
my $db = shift || "$sortie";

if (-r $db) {
  print STDERR "  Erasing previous database $db\n";
  `rm $db`;
}

my $dbh = DBI->connect("dbi:SQLite:$db", "", "", {RaiseError => 1, AutoCommit => 0});
$dbh->do("CREATE TABLE data (pos1,pos2,pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle);");
my $sth=$dbh->prepare('INSERT INTO data	(pos1,pos2,pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle) VALUES (?,?,?,?,?,?,?,?,?,?)');
my $l = 0;
print STDERR "  Loading data...\n";

#_______________________________________________


my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print sprintf("%04d-%02d-%02d %02d:%02d:%02d",1900+$year,$mon+1,$mday,$hour,$min,$sec);



#On parcourt toutes les entrées et on les stocke avec leur nombre d'occurence.
my %newEntreeDB;
$candidats_getall->execute();
my ($mot,$lemme,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff);
#pour chaque ligne de la base
while (($mot,$lemme,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff) =  $candidats_getall->fetchrow){	
	($mot,$lemme,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff)=encode_UTF8($mot,$lemme,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff);
	#print STDERR "$mot,$lemme,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff\n";
	#On remplis les contextes vides par A
	if (!$lettrePref or $lettrePref eq "" ) {
		$lettrePref = "A";
	}
	if (!$lettreSuff or $lettreSuff eq "") {
		$lettreSuff = "A";
	}
	#print	"($mot,$lemme,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff)" ;
	#if (bonContext($mot,$pref1,$suff1,$lettrePref,$lettreSuff)){
		#print "yes : $mot {$lemme}{$pref1}{$suff1}{$pref2}{$suff2}{$lettrePref}{$lettreSuff}\n";
	#Si le lemme est bien un lemme
	if (exists $lefffLemme{$lemme}){	
		my $motLefff;
		my $motLemme;
		#print "-> $mot $lemme\n";
		for $motLefff (keys %{$lefff{$mot}}){
			#print "-> $motLefff\n";
			for $motLemme (keys %{$lefffLemme{$lemme}}){
				#print "-> $motLemme\n";
				#print "---$mot-----$lemme---$motLefff-------$motLemme----------\n";
				my $typeRegle="D";
				my $catgramMot=$motLefff;
				my $catgramLemme=$motLemme;
				$catgramMot=~ s/_.*$//;
				$catgramLemme=~ s/_.*$//;
				if (exists $dicomotLemme{$mot}{$lemme} && $catgramMot eq $catgramLemme){
					$typeRegle="F";
					#print " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!F \n"
				}
				if (exists $newEntreeDB{$motLefff}{$motLemme}{$typeRegle}{$pref1}{$suff1}{$pref2}{$suff2}{$lettrePref}{$lettreSuff}){
					$newEntreeDB{$motLefff}{$motLemme}{$typeRegle}{$pref1}{$suff1}{$pref2}{$suff2}{$lettrePref}{$lettreSuff}+=1; 
					#print " += 1 \n"	;
				}else {
					$newEntreeDB{$motLefff}{$motLemme}{$typeRegle}{$pref1}{$suff1}{$pref2}{$suff2}{$lettrePref}{$lettreSuff}=1; 
					#print " = 1 \n";
				}
			}
		}
	}
}


($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print sprintf("%04d-%02d-%02d %02d:%02d:%02d",1900+$year,$mon+1,$mday,$hour,$min,$sec);

my $i=-1;

#On réécrit notre base de données
my ($pos1,$pos2,$type,$prefixe1,$prefixe2, $suffixe1, $suffixe2, $lettre1, $lettre2);

for $pos1 (keys (%newEntreeDB)){
	for $pos2 (keys %{$newEntreeDB{$pos1}}){ 
		for $type (keys %{$newEntreeDB{$pos1}{$pos2}}){ 				
			for $prefixe1 (keys %{$newEntreeDB{$pos1}{$pos2}{$type}}){
				for $suffixe1 (keys %{$newEntreeDB{$pos1}{$pos2}{$type}{$prefixe1}}){
					for $prefixe2 (keys %{$newEntreeDB{$pos1}{$pos2}{$type}{$prefixe1}{$suffixe1}}){
						for $suffixe2 (keys %{$newEntreeDB{$pos1}{$pos2}{$type}{$prefixe1}{$suffixe1}{$prefixe2}}){

							for $lettre1 (keys %{$newEntreeDB{$pos1}{$pos2}{$type}{$prefixe1}{$suffixe1}{$prefixe2}{$suffixe2}}){
								#print "blalblalbal\n"; #rentre pas ici!!!!!!!!!
								for $lettre2 (keys %{$newEntreeDB{$pos1}{$pos2}{$type}{$prefixe1}{$suffixe1}{$prefixe2}{$suffixe2}{$lettre1}}){
									my $nbocc= $newEntreeDB{$pos1}{$pos2}{$type}{$prefixe1}{$suffixe1}{$prefixe2}{$suffixe2}{$lettre1}{$lettre2};
									#print "$nbocc\n{$pos1}{$pos2}{$type}{$prefixe1}{$suffixe1}{$prefixe2}{$suffixe2}{$lettre1}{$lettre2}\n"; 
									if ($nbocc>1){
										$sth->execute($pos1, $pos2, $prefixe1, $suffixe1, $prefixe2, $suffixe2,$lettre1, $lettre2,$nbocc,$type);	
									}
								
									$i++;
									if ($i % 1000 == 0) {
	    									$dbh ->commit;
									}
  								}
							}
						}
					}
				}
			}
		}
	}	
}

($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print sprintf("%04d-%02d-%02d %02d:%02d:%02d",1900+$year,$mon+1,$mday,$hour,$min,$sec);

#_______________________________________________

#on ferme la base de donnée du lefff
#$lefff_getkey ->finish;
#$lefff_dbh ->disconnect; # pour se déconnecter de la base de données en fin de programme.
$candidats_getall->finish;
$candidats_dbh ->disconnect; # pour se déconnecter de la base de données en fin de programme.


print STDERR "\r  Loading data...$l\n";
$sth->finish;
$dbh->commit;
print STDERR "  Creating index...\n";
$dbh->do("CREATE INDEX indpos1 ON data(pos1);");
print STDERR "  Creating index...mot\n";
$dbh->do("CREATE INDEX indpos2 ON data(pos2);");
print STDERR "  Creating index...lemme\n";
$dbh->do("CREATE INDEX indPref ON data(pref1);");
print STDERR "  Creating index...pref\n";
$dbh->do("CREATE INDEX indSuff ON data(suff1);");
print STDERR "  Creating index...suff\n";
$dbh->do("CREATE INDEX pref1_suff1 ON data(pref1,suff1);");
print STDERR "  Creating index...pref - suff\n";

$dbh->commit;
print STDERR "done\n";
$dbh->disconnect;


################################################################################
#  MÉTHODES
################################################################################


sub	encode_UTF8{	
	my ($mot1,$mot2,$pref1,$suff1,$pref2,$suff2,$lettrePref, $lettreSuff)= @_;
	$mot1 = Encode::decode("utf-8",$mot1);
	$mot2 = Encode::decode("utf-8",$mot2);
	$pref1 = Encode::decode("utf-8",$pref1);
	$suff1 = Encode::decode("utf-8",$suff1);
	$pref2 = Encode::decode("utf-8",$pref2);
	$suff2 = Encode::decode("utf-8",$suff2);
	$lettrePref = Encode::decode("utf-8",$lettrePref);
	$lettreSuff = Encode::decode("utf-8",$lettreSuff);
	return ($mot1,$mot2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff);
}

sub bonContext{
	my ($mot,$pref1,$suff1,$lettrePref,$lettreSuff)=@_;
	$mot = "#$mot#";
	if ($lettrePref eq "A"){
		$lettrePref="";
	}
	if ($lettreSuff eq "A"){
		$lettreSuff="";
	}
	if ($mot=~ m/^$pref1$lettrePref.*$lettreSuff$suff1/){
		return 1;
	}
	return 0



}
