#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use locale;
use DBI;
use Encode;
 
$|=1; 

while (1) {
    $_=shift;
    if (!$_ || /^$/) {last;}
    elsif (/^-regles/) {$dbRegles=shift;}   #prend base de données de POS
    elsif (/^-sortie/) {$sortie=shift;}   #prend base de données qu'on veut sortir
}

#------------------------------------------------------------------------------------------------------
# Initialisation de la base de données et des fichiers qu'on veut utiliser et stockage des données
#------------------------------------------------------------------------------------------------------

# On ouvre la base de règles (mot1,mot2,pos1, pos2, pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle)
my $regles_dbh = DBI->connect("dbi:SQLite:$dbRegles", "", "", {RaiseError => 1, AutoCommit => 1});
my $regles_getuniq= $regles_dbh->prepare('SELECT DISTINCT mot1,mot2,pos1,pos2 FROM data;');
my $regles_getReglesSpec= $regles_dbh->prepare('SELECT * FROM data WHERE mot1=? AND mot2=? AND pos1=? AND pos2=?;');
#my $regles_getLigne= $regles_dbh->prepare('SELECT * FROM data WHERE mot1=? AND mot2=? AND pos1=? AND pos2=? AND pref1=? AND suff1=?;');


# On ouvre la base de règles dans laquelle on veut écrire
if (-r $sortie) {
  print STDERR "  Erasing previous database $sortie\n";
  `rm $sortie`;
}
my $sortie_dbh = DBI->connect("dbi:SQLite:$sortie", "", "", {RaiseError => 1, AutoCommit => 1});
$sortie_dbh->do("CREATE TABLE data(mot1,mot2,pos1, pos2, pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle);");
my $sortie_sth = $sortie_dbh->prepare('INSERT INTO data(mot1,mot2,pos1, pos2, pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)');

#---------------------------------------------------------------------------------------------------------------------
# Parcours des paires mots pos uniques et pour chacune d'entre elles, on récupère les préfixes suffixes les + longs
#---------------------------------------------------------------------------------------------------------------------
$regles_getuniq->execute();
my $i=0;
my $nbtotal;
while (($mot1,$mot2,$pos1,$pos2) = $regles_getuniq->fetchrow){
	$nbtotal++;
}
print "filtrage \n";
$regles_getuniq->execute();
while (($mot1,$mot2,$pos1,$pos2) = $regles_getuniq->fetchrow){
	$i++;	
    print STDERR "\r  Loading data...$i/$nbtotal";
	($mot1,$mot2,$pos1,$pos2)=encode_UTF8($mot1,$mot2,$pos1,$pos2);	
	
	
	#on cherche les préfixes suffixes les + longs
	my $maxSuff = "";
	my $maxPref ="";
	@bestligne;
	$regles_getReglesSpec->execute($mot1,$mot2,$pos1,$pos2);
	while (($mot1,$mot2,$pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle) =  $regles_getReglesSpec->fetchrow){
		@ligne=encode_UTF82($mot1,$mot2,$pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle);
		#print "@ligne"."\n";
		if (length($pref1.$suff1)>length($maxPref.$maxSuff)){
			$maxPref=$pref1;
			$maxSuff=$suff1;
			@bestligne = @ligne;
		}
	}
	$sortie_sth->execute(@ligne);
	#print " ($bestligne[0],$bestligne[1],$bestligne[2],$bestligne[3],$bestligne[4],$bestligne[5],$bestligne[6],$bestligne[7],$bestligne[8],$bestligne[9],$bestligne[10],$bestligne[11])\n";

} 

	
#------------------------------------------------------------------------------------------------------
# Fin du programme on ferme nos requêtes et on crée les indexs pour la base de données.
#------------------------------------------------------------------------------------------------------

$regles_getuniq->finish;
$regles_getReglesSpec->finish;
$regles_dbh->disconnect;

$sortie_sth->finish;
%$sortie_dbh->commit;
print STDERR "  Creating index...";
$sortie_dbh->do("CREATE INDEX idtypeRegle ON data(typeRegle);");
$sortie_dbh->do("CREATE INDEX idmot1 ON data(mot1);");
$sortie_dbh->do("CREATE INDEX idmot2 ON data(mot2);");
$sortie_dbh->do("CREATE INDEX idpos1 ON data(pos1);");
$sortie_dbh->do("CREATE INDEX idpos2 ON data(pos2);");
print STDERR "done\n";
$sortie_dbh->disconnect;
print STDERR "\r  Loading data...$l\n";

################################################################################
#  MÉTHODES 
################################################################################


sub	encode_UTF8{	
	my ($mot1,$mot2,$pos1,$pos2)= @_;
	$pos1 = Encode::decode("utf-8",$pos1);
	$pos2 = Encode::decode("utf-8",$pos2);
	$mot1 = Encode::decode("utf-8",$mot1);
	$mot2 = Encode::decode("utf-8",$mot2);
	return ($mot1,$mot2,$pos1,$pos2);
}

sub	encode_UTF82{	
	my ($mot1,$mot2,$pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle)= @_;
	$mot1 = Encode::decode("utf-8",$mot1);
	$mot2 = Encode::decode("utf-8",$mot2);
	$pos1 = Encode::decode("utf-8",$pos1);
	$pos2 = Encode::decode("utf-8",$pos2);
	$pref1 = Encode::decode("utf-8",$pref1);
	$suff1 = Encode::decode("utf-8",$suff1);
	$pref2 = Encode::decode("utf-8",$pref2);
	$suff2 = Encode::decode("utf-8",$suff2);
	$lettrePref = Encode::decode("utf-8",$lettrePref);
	$lettreSuff = Encode::decode("utf-8",$lettreSuff);
	$nbOcc = Encode::decode("utf-8",$nbOcc);
	$typeRegle = Encode::decode("utf-8",$typeRegle);
	return ($mot1,$mot2,$pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle);
}
