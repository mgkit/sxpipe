#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;
use Encode;
 
$|=1; 

#------------------------------------------------------------------------------------------------------
# Initialisation de la base de données et des fichiers qu'on veut utiliser et stockage des données
#------------------------------------------------------------------------------------------------------


while (1) {
    $_=shift;
    if (!$_ || /^$/) {last;}
    elsif (/^-oldregles$/) {$oldregles=shift;}
    elsif (/^-sortie$/) {$sortie=shift;}   
    elsif (/^-dico$/) {$dico=shift;}  
}

if($sortie eq "") {
  die "### ERROR: 2.MakeCandidats_Lemmes.pl requires using option -sortie followed by the output file";
}


# On ouvre la base de règles
my $regles_dbh = DBI->connect("dbi:SQLite:$oldregles", "", "", {RaiseError => 1, AutoCommit => 1});
my $regles_getall= $regles_dbh->prepare('SELECT pos1,pos2,pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle FROM data WHERE pos1 = ? AND pref1=? AND suff1=?;');
my $regles_getall2= $regles_dbh->prepare('SELECT pos1,pos2,pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle FROM data WHERE pref1=? AND suff1=?;');

#on conserve les mots du lefff
my %lefff;
open (Fic, "<$dico") || die "Problème à l\'ouverture fichier contenant le dico : $!";;
binmode (Fic, ":encoding(utf8)");
while (<Fic>) {
 	chomp;
	m/^([^\t]*)\t[^\t]*\t([^\t]*)\t[^\t]*\t[^\t]*\t[^\t]*\t([^\t]*).*$/;	#à titre indicatif: $mot = $1; $cat = $2; $catflex = $3;
	my $cat=$2;
	if ($cat ne "cf"){
		if ($cat eq "advm" or $cat eq "advp"){
			$cat="adv";
		}
		$lefff{$1}{$cat."_".$3}=1; #on stocke le mot et sa pos_classeflex
	}

	#print "$1 - $2 - $3\n";
}	
close (Fic);
print STDERR "Lefff End \n";


# On créé new base de données
my $db = shift || "$sortie";

if (-r $db) {
  print STDERR "  Erasing previous database $db\n";
  `rm $db`;
}

my $dbh = DBI->connect("dbi:SQLite:$db", "", "", {RaiseError => 1, AutoCommit => 0});
$dbh->do("CREATE TABLE data(pos1,pos2,pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbocc,typeRegle);");
my $sth=$dbh->prepare('INSERT INTO data(pos1,pos2,pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbocc,typeRegle) VALUES (?,?,?,?,?,?,?,?,?,?)');

my $l = 0;

#------------------------------------------------------------------------------------------------------
# Parcours des mots du lefff pour créer des paires de candidats avec leurs regex correspondantes.
#------------------------------------------------------------------------------------------------------
my $nbmots = keys(%lefff);
my $numMot=0;

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print sprintf("%04d-%02d-%02d %02d:%02d:%02d",1900+$year,$mon+1,$mday,$hour,$min,$sec);
print "\n";
my %allMotForAnalogie; 

my $mot;#="#rescotchage#";#="réexpédiez";
for $mot (keys %lefff){
	$motDiese= "#$mot#";
	$numMot+=1;
	print STDERR "\r $numMot/$nbmots";
	#On extrait les préfixes et suffixes du mot inc
	my ($prefref,$suffref)= getAffixes($motDiese);
	my @prefInc = @$prefref;
	my @suffInc = @$suffref;
	push(@prefInc,"");
	push(@suffInc,"");
	#print $mot."\n";
	my %allPropositionForMot; 


	#pour chaque part of speech que peut avoir le mot :
	my $posMot;##="inc";
	for $posMot (keys %{$lefff{$mot}}){
		#print $mot." ".$posMot."\n";
		
		#On extrait toutes les données correspondants à ces suffixes ou préfixes dans notre base de données 
		my ($pref, $suff);
		
		for $pref (@prefInc){
			for $suff (@suffInc){
#				print $pref."\t".$suff."\t".$posMot."\t.....\n";
				$regles_getall->execute($posMot,"$pref","$suff");
#				$regles_getall2->execute("$pref","$suff");
				while (($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle) =  $regles_getall->fetchrow){
					($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle)=encode_UTF8($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle);	
					my $newMot = $motDiese;
					$newMot =~ s/$pref1(.*)$suff1/$pref2$1$suff2/;
					#on stocke le mot généré par la regex s'il est connu du dico et si sa pos concorde.
					$newMot =~ m/^#(.*)#$/;
					#print $newMot."\n";
					if (exists($lefff{$1}) and exists($lefff{$1}{$pos2}) and concordContexte($motDiese,$pref1,$lettrePref,$lettreSuff,$suff1)){ 
						my $catgramMot=$pos1;
						my $catgramLemme=$pos2;
						$catgramMot=~ s/_.*$//;
						$catgramLemme=~ s/_.*$//;	
						#print "\t\t-> $nbOcc\t$mot\t$newMot\t$pos1\t$pos2\t-$type-\n";
											
						if (($catgramMot eq $catgramLemme) and ($typeRegle eq "F")){#(exists $dicomotLemme{$mot}{$newmot}) and -> si on veut lemmatiser
							#print "\t\t-> $nbOcc\t$mot\t$newMot\t$pos1\t$pos2\t$pref1 - $suff1\t$pref2 - $suff2 -$typeRegle-\n";
							$allPropositionForMot{$motDiese}{$newMot}{$typeRegle}{$pos1}{$pos2}{$pref1}{$suff1}{$pref2}{$suff2}{$lettrePref}{$lettreSuff}=$nbOcc;
						}elsif ($typeRegle eq "D"){
							$allPropositionForMot{$motDiese}{$newMot}{$typeRegle}{$pos1}{$pos2}{$pref1}{$suff1}{$pref2}{$suff2}{$lettrePref}{$lettreSuff}=$nbOcc;
							#print "\t\t-> $nbOcc\t$mot\t$newMot\t$pos1\t$pos2\t$pref1 - $suff1\t$pref2 - $suff2 -$typeRegle-\n";
						}

			 			#print "-> $nbOcc $typeRegle \t$mot\t$newMot\t$pos1\t$pos2\t$pref1 - $suff1\t$pref2 - $suff2\n";
					}
				}
			}
		}
	}	
	#on filtre les règles qu'on veut conserver et on les ajoute à %allMotForAnalogie
	my $allregles=gestBestRegle(\%allPropositionForMot);
	@allRegles = @$allregles;
	for $regle (@allRegles){
		@regle1 = @$regle;
		#print "---- $regle1[0]}{$regle1[1]}{$regle1[2]}{$regle1[3]}{$regle1[4]}{$regle1[5]}{$regle1[6]}{$regle1[7]}{$regle1[8]}\n";
		$allMotForAnalogie{$regle1[0]}{$regle1[1]}{$regle1[2]}{$regle1[3]}{$regle1[4]}{$regle1[5]}{$regle1[6]}{$regle1[7]}{$regle1[8]}+=1;   
	}  
}

#On imprime le contenu de %allregex, soit, toutes les règles filtrées.
for $pos1 (keys %allMotForAnalogie){
	for $pos2 (keys %{$allMotForAnalogie{$pos1}}){ 
		for $type (keys %{$allMotForAnalogie{$pos1}{$pos2}}){ 				
			for $prefixe1 (keys %{$allMotForAnalogie{$pos1}{$pos2}{$type}}){
				for $suffixe1 (keys %{$allMotForAnalogie{$pos1}{$pos2}{$type}{$prefixe1}}){
					for $prefixe2 (keys %{$allMotForAnalogie{$pos1}{$pos2}{$type}{$prefixe1}{$suffixe1}}){
						for $suffixe2 (keys %{$allMotForAnalogie{$pos1}{$pos2}{$type}{$prefixe1}{$suffixe1}{$prefixe2}}){
							for $lettre1 (keys %{$allMotForAnalogie{$pos1}{$pos2}{$type}{$prefixe1}{$suffixe1}{$prefixe2}{$suffixe2}}){
								for $lettre2 (keys %{$allMotForAnalogie{$pos1}{$pos2}{$type}{$prefixe1}{$suffixe1}{$prefixe2}{$suffixe2}{$lettre1}}){
									my $nbocc= $allMotForAnalogie{$pos1}{$pos2}{$type}{$prefixe1}{$suffixe1}{$prefixe2}{$suffixe2}{$lettre1}{$lettre2};
									#if ($nbocc>=1){
										$sth->execute($pos1, $pos2, $prefixe1, $suffixe1, $prefixe2, $suffixe2,$lettre1, $lettre2,$nbocc,$type);	
										#print "$pos1, $pos2, $prefixe1, $suffixe1, $prefixe2, $suffixe2,$lettre1, $lettre2,$nbocc,$type\n";
									#}
					
									$i++;
									if ($i % 1000 == 0) {
										$dbh ->commit;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print sprintf("%04d-%02d-%02d %02d:%02d:%02d",1900+$year,$mon+1,$mday,$hour,$min,$sec);


	
#------------------------------------------------------------------------------------------------------
# Fin du programme on ferme nos requêtes et on crée les indexs pour la base de données.
#------------------------------------------------------------------------------------------------------
$regles_getall->finish;
$regles_getall2->finish;
$regles_dbh->disconnect;

print STDERR "\r  Loading data...$l\n";
$sth->finish;
$dbh->commit;

print STDERR "  Creating index...";
$dbh->do("CREATE INDEX indSuff ON data(suff1);");
$dbh->do("CREATE INDEX pref1_suff1 ON data(pref1,suff1);");
$dbh->commit;
print STDERR "done\n";

$dbh->disconnect;
################################################################################
#  MÉTHODES
################################################################################

sub getAffixes{
	my ($motInc) = @_;
	my $id = 1;
	my (@pref, @suff);
	while ($id!=length($motInc)){
		$motInc =~ m/^(.{$id})(.*)$/;
		push(@pref,$1);
		push(@suff,$2);
		$id+=1; 
	}
	return (\@pref, \@suff);
}

sub	encode_UTF8{	
	my ($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle)= @_;
	$pos1 = Encode::decode("utf-8",$pos1);
	$pos2 = Encode::decode("utf-8",$pos2);
	$pref1 = Encode::decode("utf-8",$pref1);
	$suff1 = Encode::decode("utf-8",$suff1);
	$pref2 = Encode::decode("utf-8",$pref2);
	$suff2 = Encode::decode("utf-8",$suff2);
	$lettrePref = Encode::decode("utf-8",$lettrePref);
	$lettreSuff = Encode::decode("utf-8",$lettreSuff);
	$nbOcc = Encode::decode("utf-8",$nbOcc);
	$typeRegle = Encode::decode("utf-8",$typeRegle);
	return ($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$typeRegle);
}

sub gestBestRegle{
	my ($allPropositionForMot)=@_;
	%allPropositionForMot = %$allPropositionForMot;
	my @listeRegle;
	my $bestScore = 1;
	while ( $bestScore>0){#($#listeRegle<2)and
		my @bestRegle = ("","","","","","","","","");
		$bestScore = 0;
		my ($bestMot1,$bestMot2);
		#my @best=("$pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff);
		for $mot (keys (%allPropositionForMot)){
			for $newMot (keys %{$allPropositionForMot{$mot}}){
				for $type (keys %{$allPropositionForMot{$mot}{$newMot}}){
					for $pos1 (keys %{$allPropositionForMot{$mot}{$newMot}{$type}}){
						for $pos2 (keys %{$allPropositionForMot{$mot}{$newMot}{$type}{$pos1}}){ 
							for $prefixe1 (keys %{$allPropositionForMot{$mot}{$newMot}{$type}{$pos1}{$pos2}}){
								for $suffixe1 (keys %{$allPropositionForMot{$mot}{$newMot}{$type}{$pos1}{$pos2}{$prefixe1}}){
									for $prefixe2 (keys %{$allPropositionForMot{$mot}{$newMot}{$type}{$pos1}{$pos2}{$prefixe1}{$suffixe1}}){
										for $suffixe2 (keys %{$allPropositionForMot{$mot}{$newMot}{$type}{$pos1}{$pos2}{$prefixe1}{$suffixe1}{$prefixe2}}){
											for $lettre1 (keys %{$allPropositionForMot{$mot}{$newMot}{$type}{$pos1}{$pos2}{$prefixe1}{$suffixe1}{$prefixe2}{$suffixe2}}){
												for $lettre2 (keys %{$allPropositionForMot{$mot}{$newMot}{$type}{$pos1}{$pos2}{$prefixe1}{$suffixe1}{$prefixe2}{$suffixe2}{$lettre1}}){
													my $nbocc= $allPropositionForMot{$mot}{$newMot}{$pos1}{$type}{$pos2}{$prefixe1}{$suffixe1}{$prefixe2}{$suffixe2}{$lettre1}{$lettre2};
													my $newScore= getScore($nbocc,$prefixe1,$prefixe2,$suffixe1,$suffixe2,$mot,$newMot);
													#print "$#listeRegle $taille\t$mot, $newMot, $pos1, $pos2, $prefixe1, $suffixe1, $prefixe2, $suffixe2,$lettre1, $lettre2,$nbocc\n";
													
													if ($newScore>$bestScore){
														$bestScore= $newScore;
														@bestRegle=($pos1, $pos2,$type, $prefixe1, $suffixe1, $prefixe2, $suffixe2,$lettre1, $lettre2);
														$bestMot1=$mot;
														$bestMot2=$newMot;
														#print "$#listeRegle \t$mot, $newMot, $pos1, $pos2, $prefixe1, $suffixe1, $prefixe2, $suffixe2,$lettre1, $lettre2,$nbocc\n";
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if ($bestScore>0){
			push(@listeRegle,\@bestRegle); 
			delete($allPropositionForMot{$bestMot1}{$bestMot2}{$type}{$bestRegle[0]}{$bestRegle[1]}{$bestRegle[2]}{$bestRegle[3]}{$bestRegle[4]}{$bestRegle[5]}{$bestRegle[6]}{$bestRegle[7]});
			#print "$bestScore !!!!!!!!!!!!!!!!!!!!!!!{$bestMot1}{$bestMot2}{$bestRegle[0]}{$bestRegle[1]}{$bestRegle[2]}{$bestRegle[3]}{$bestRegle[4]}{$bestRegle[5]}{$bestRegle[6]}{$bestRegle[7]}\n";
		}
	}
	return \@listeRegle;
}

sub getScore{
	my ($nbocc,$prefixe1,$prefixe2,$suffixe1,$suffixe2,$mot,$newMot)=@_;
	$mot =~ m/^$prefixe1(.*)$suffixe1$/;
	if ($nbocc > 10 and length($1)>3){
		return 1;
	}
	return 0;
}

sub concordContexte{
	my ($motDiese,$pref1,$lettrePref,$lettreSuff,$suff1)=@_;
	if($lettrePref eq "A"){
		$lettrePref="";
	}elsif($lettrePref eq "c"){
		$lettrePref= qr/^$prefixe ?[bcçdfghjklmnpkrstvwxz]/; 
	}elsif($lettrePref eq "v"){
		$lettrePref=qr/^$prefixe ?[aàâäeéèêëiîïoôöuùûüy]/; 
	}
	if($lettreSuff eq "A"){
		$lettreSuff="";
	}elsif($lettreSuff eq "C"){
		$lettreSuff=  qr/^$prefixe ?[bcçdfghjklmnpkrstvwxz]/; 
	}elsif($lettreSuff eq "V"){
		$lettreSuff=qr/^$prefixe ?[aàâäeéèêëiîïoôöuùûüy]/; 
	}
	return $motDiese=~ m/^$pref1$lettrePref.*$lettreSuff$suff1$/;
}
