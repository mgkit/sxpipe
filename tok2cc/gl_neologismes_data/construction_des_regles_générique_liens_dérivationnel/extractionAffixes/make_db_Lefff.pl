#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;
use Encode;


my $db = shift || "db.dat";


if (-r $db) {
  print STDERR "  Erasing previous database $db\n";
  `rm $db`;
}

my $dbh = DBI->connect("dbi:SQLite:$db", "", "", {RaiseError => 1, AutoCommit => 0});
$dbh->do("CREATE TABLE data(key, value);");
my $sth=$dbh->prepare('INSERT INTO data(key,value) VALUES (?,?)');
my $l = 0;
print STDERR "  Loading data...";
while (<>) {
  	$l++;
  	if ($l % 10000 == 0) {
		print STDERR "\r  Loading data...$l";
	$dbh->commit;
  	}
  	chomp;
  	/^([^\t]*)\t([^\t]+)\t?$/ || next;
	my $key = $1;
	my $value = $2;	
	$sth->execute($key,$value);
}
print STDERR "\r  Loading data...$l\n";
$sth->finish;
$dbh->commit;
print STDERR "  Creating index...";
$dbh->do("CREATE INDEX mot ON data(key);");
$dbh->do("CREATE INDEX lemme ON data(value);");
$dbh->commit;
print STDERR "done\n";
$dbh->disconnect;
