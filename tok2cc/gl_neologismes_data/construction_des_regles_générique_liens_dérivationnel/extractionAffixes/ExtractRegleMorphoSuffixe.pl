#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;
use Encode;
 
$|=1; 
while (1) {
$_=shift;
if (!$_ || /^$/) {last;}
elsif (/^-d$/) {$db=shift;}   
}

if ($db eq "") {
  die "### ERROR: extractRegleMorphoeSuffixe.pl requires using option -db followed by the file that contains affix";
}
#on ouvre la base de donnée du lefff
my $lefff_dbh;
$lefff_dbh = DBI->connect("dbi:SQLite:".$db, "", "", {RaiseError => 1, AutoCommit => 1});
my $lefff_getkey = $lefff_dbh->prepare('SELECT key FROM data WHERE (value="C" or value="nc" or value="adj" or value="v" or value="adv" or value="advm" or value="advp" or value="N" or value="A" or value="V" or value="ADV" or value="ADJ" or value="ADVP");');


#On parcourt les mots du lexique
$lefff_getkey->execute(); 
my $mot;
my %lefff;
my %lefffinit;
my %lefffend;

while ($mot =  $lefff_getkey->fetchrow){
	$mot = Encode::decode("utf-8", $mot);
	#if (substr($mot,0,1) =~ m/[dD]/){
	$lefff{$mot} = 1;
	#}
	$lefffinit{substr($mot,0,3)}{$mot}=1;

}

my %suffixe;
my %prefixe;
my $i=-1;

#my $s="";
my $nbLefff = keys(%lefff);
for $mot (keys(%lefff)){
	
$i++;
if ($i % 100 == 0) {
	print STDERR "\r  Loading data...$i / $nbLefff";
}

if (length($mot)>4){
	my $mot2;
	#on cherche les suffixes		
	for $mot2 (keys %{$lefffinit{substr($mot,0,3)}}){ 		
	my $suff1 = $mot;
	my $suff2 = $mot2;
	if ($suff1 ne $suff2){
		$suff2 =~ m/^..(.)/;
		my $lettrePrecedente=$1;
		$suff1 =~ s/^...(.+)$/$1#/;
		$suff2 =~ s/^...(.+)$/$1#/;
		#$suffixe{$suff1}{$suff2}+=1;
		print "$suff1\t$suff2\t-$lettrePrecedente-\n";
		#print "$mot $mot2 $suff1 $suff2 $suffixe{$suff1}{$suff2}\n";
		
		while (not($suff1 eq "#" and $suff2 eq "#") and (substr($suff1,0,1))eq(substr($suff2,0,1))){
		$suff1=~ s/^.(.*)#$/$1#/;
		$suff2=~ s/^(.)(.*)#$/$2#/;   			
		#$suffixe{$suff1}{$suff2}+=1;		  
		$lettrePrecedente=$1;
		print "$suff1\t$suff2\t-$lettrePrecedente-\n";
		#print "$mot $mot2 $suff1 $suff2 $suffixe{$suff1}{$suff2}\n";   					
		}
	}
	}
	
	#on cherche les prefixes		
#	for $mot2 (keys %{$lefffend{substr($mot,length($mot)-6,6)}}){ 
#	my $pref1 = $mot;
#	my $pref2 = $mot2;
#	if ($pref1 ne $pref2){
#		my $pref1 = "#".$mot;
#		my $pref2 = "#".$mot2;
#		$pref2 =~ m/(.).....$/;
#		my $lettreSuivante=$1;
#		$pref1 =~ s/......$//;
#		$pref2 =~ s/......$//;
#		#$prefixe{$pref1}{$pref2}+=1;
#		print "$pref1\t$pref2\t-$lettreSuivante-\n";
#		#print "$mot $mot2 $pref1\t$pref2\n";	
#		
#		while (not($pref1 eq "#" and $pref2 eq "#") and (substr($pref1,length($pref1)-1,1))eq(substr($pref2,length($pref2)-1,1))){
#		$pref1=~ s/^#(.*).$/#$1/; 
#		$pref2=~ s/^#(.*)(.)$/#$1/; 	   					
#		#$prefixe{$pref1}{$pref2}+=1;
#		$lettreSuivante=$2;
#		print "$pref1\t$pref2\t-$lettreSuivante-\n";	
#		#print "$mot $mot2 $pref1\t$pref2\n";	
#		}
#	}
#	}
}
}


#on ferme la base de donnée du lefff
$lefff_getkey ->finish;
$lefff_dbh ->disconnect; # pour se déconnecter de la base de données en fin de programme.

#printLexique(\%suffixe);


################################################################################
#  MÉTHODES
################################################################################

sub printLexique{
	my ($hash)=@_;
	my %regles = %$hash;
	my $affixe1;
	my $affixe2;
	for $affixe1 (keys(%regles)){
		for $affixe2 (keys %{$regles{$affixe1}}){
			print "$regles{$affixe1}{affixe2}\t$affixe1\t$affixe2\n";
		}
	}
}
