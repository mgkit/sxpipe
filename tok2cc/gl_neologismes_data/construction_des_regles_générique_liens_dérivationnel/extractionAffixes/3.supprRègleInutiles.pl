#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
#use strict;
#use warnings;
use Encode;
 
$|=1;

# Script qui prend des règles et supprime celle qui sont redondante :
# ex : a -> ait [t]
#  ta -> tait [n]
#  La seconde n'est pas nécessaire on la supprime.
#Avant de supprimer la règle 2 on vérifie néanmoins que son nombre d'occurrence est bien inférieur à la règle 1 et que les lettre complémentaires concordent.

while (1) {
$_=shift;
if (!$_ || /^$/) {last;}
elsif (/^-f$/) {$fileAffixes=shift;}   
}

if ($fileAffixes eq "") {
  die "### ERROR: 3.supprRègleInutiles.pl requires using option -f followed by the file that contains affix";
}

#On donne chemin des fichiers contenant les affixes
#$fileAffixes ="/home/marion/analyse_morpho_val/1.extraction_regles/2.pref_context_merge" ;

#on stocke les affixes donnés en options
my %affixes;
open (Fic, "<$fileAffixes") || die "Problème à l\'ouverture $fileAffixes : $!";;
binmode (Fic, ":encoding(utf8)");
while (<Fic>) {
 	chomp; 
	m/^ *([0-9]+)\t(.*)\t(.*)\t\[(.*)\]$/;
	$affixes{$2}{$3}=[$1,$4];		
}
close (Fic);


my $affixe1;
for $affixe1 (keys(%affixes)){
	my $affixe2;
	for $affixe2 (keys %{$affixes{$affixe1}}){
		my $factorisable=0;
		#S'il est possible de factoriser la règle car elle commence par une même lettre (suffixe) ou se termine par une même lettre (préfixe)
		###if (substr($affixe1,0,1) eq substr($affixe2,0,1)){ #si première lettre identique
		if (substr($affixe1,length($affixe1)-1,1) eq substr($affixe2,length($affixe2)-1,1)){ #si dernière lettre identique
			my $newaffixe1 = $affixe1;
			my $newaffixe2 = $affixe2;
			$newaffixe1 =~ s/(.)$//;
			###$newaffixe1 =~ s/^(.)//;
			my $lettreComplementaire = $1;
			$newaffixe2 =~ s/.$//;
			###$newaffixe2 =~ s/^(.)//;
			#Si règle existe avec mots factorisés dont le nb de d'occurence est >= à la règle et qui ont la lettre complémentaire
			if ((exists($affixes{$newaffixe1}{$newaffixe2}))and($affixes{$affixe1}{$affixe2}[0]<=$affixes{$newaffixe1}{$newaffixe2}[0])and($affixes{$newaffixe1}{$newaffixe2}[1]=~ m/$lettreComplementaire/)){ 
						$factorisable=1	;				
						#print " ".$affixes{$affixe1}{$affixe2}[0]."\t$affixe1\t$affixe2\t[".$affixes{$affixe1}{$affixe2}[1]."]\n";
						#print " ".$affixes{$newaffixe1}{$newaffixe2}[0]."\t$newaffixe1\t$newaffixe2\t[".$affixes{$newaffixe1}{$newaffixe2}[1]."]\t\t\n";
						#print "\t$affixes{$newaffixe1}{$newaffixe2}[1]- $lettreComplementaire\n";
			}
		}
		
		if (not $factorisable){
			#on réécrit la règle
			print " ".$affixes{$affixe1}{$affixe2}[0]."\t$affixe1\t$affixe2\t[".$affixes{$affixe1}{$affixe2}[1]."]\n";
		}
	}
}
