#!/bin/sh

myfic=$1
path=$(pwd)
mydb=$2
pathDB=$path/$mydb

cat $myfic | perl make_db_Lefff.pl
date
echo " -------- Extraction des suffixes -------- "
mkdir suffixeAlphabet
mkdir suffixeAlphabetSorted
sh 1.getAlphabetSuffixes.sh $pathDB
date
echo " -------- Extraction des préfixes -------- "
mkdir prefixeAlphabet
mkdir prefixeAlphabetSorted
sh 1.getAlphabetPrefixes.sh $pathDB
date
echo " -------- On regroupe les contextes des affixes -------- "
perl 2.mergeLettreSuppInRegles.pl -f prefixes_10_occs_min > 2.pref_context_merge
perl 2.mergeLettreSuppInRegles.pl -f suffixes_10_occs_min > 2.suff_context_merge
date
echo " -------- Suppression des règles inutiles -------- "
perl 3.supprRègleInutiles.pl -f 2.suff_context_merge > 3.regleSuffixe
perl 3.supprRègleInutiles.pl -f 2.pref_context_merge > 3.reglePrefixe
