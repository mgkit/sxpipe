#!/bin/sh 
mydb=$1
date 
perl ExtractRegleMorphoSuffixe.pl -d $mydb > suff
echo -n "Extraction de règles ok - "
date
cat suff|grep -iE "^#	" > suffixeAlphabet/suffixeDiese
echo -n "diese ok - "
date
cat suff|grep -iE "^[aàäâ]" > suffixeAlphabet/suffixeA
echo -n "A ok - "
date
cat suff|grep -iE "^[b]" > suffixeAlphabet/suffixeB
echo -n "B ok - "
date
cat suff|grep -iE "^[cç]" > suffixeAlphabet/suffixeC
echo -n "C ok - "
date
cat suff|grep -iE "^[d]" > suffixeAlphabet/suffixeD
echo -n "D ok - "
date
cat suff|grep -iE "^[eéèêë]" > suffixeAlphabet/suffixeE
echo -n "E ok - "
date
cat suff|grep -iE "^[f]" > suffixeAlphabet/suffixeF
echo -n "F ok - "
date
cat suff|grep -iE "^[g]" > suffixeAlphabet/suffixeG
echo -n "G ok - "
date
cat suff|grep -iE "^[h]" > suffixeAlphabet/suffixeH
echo -n "H ok - "
date
cat suff|grep -iE "^[iîï]" > suffixeAlphabet/suffixeI
echo -n "i ok - "
date
cat suff|grep -iE "^[j]" > suffixeAlphabet/suffixeJ
echo -n "j ok - "
date
cat suff|grep -iE "^[k]" > suffixeAlphabet/suffixeK
echo -n "k ok - "
date
cat suff|grep -iE "^[l]" > suffixeAlphabet/suffixeL
echo -n "l ok - "
date
cat suff|grep -iE "^[m]" > suffixeAlphabet/suffixeM
echo -n "m ok - "
date
cat suff|grep -iE "^[n]" > suffixeAlphabet/suffixeN
echo -n "n ok - "
date
cat suff|grep -iE "^[oôö]" > suffixeAlphabet/suffixeO
echo -n "o ok - "
date
cat suff|grep -iE "^[p]" > suffixeAlphabet/suffixeP
echo -n "p ok - "
date
cat suff|grep -iE "^[q]" > suffixeAlphabet/suffixeQ
echo -n "q ok - "
date
cat suff|grep -iE "^[r]" > suffixeAlphabet/suffixeR
echo -n "r ok - "
date
cat suff|grep -iE "^[s]" > suffixeAlphabet/suffixeS
echo -n "s ok - "
date
cat suff|grep -iE "^[t]" > suffixeAlphabet/suffixeT
echo -n "t ok - "
date
cat suff|grep -iE "^[uûü]" > suffixeAlphabet/suffixeU
echo -n "u ok - "
date
cat suff|grep -iE "^[v]" > suffixeAlphabet/suffixeV
echo -n "v ok - "
date
cat suff|grep -iE "^[w]" > suffixeAlphabet/suffixeW
echo -n "w ok - "
date
cat suff|grep -iE "^[x]" > suffixeAlphabet/suffixeX
echo -n "x ok - "
date
cat suff|grep -iE "^[y]" > suffixeAlphabet/suffixeY
echo -n "y ok - "
date
cat suff|grep -iE "^[z]" > suffixeAlphabet/suffixeZ
echo -n "z ok - "
date
cat suffixeAlphabet/suffixeDiese|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedDiese
echo -n "trie diese ok - "
date
cat suffixeAlphabet/suffixeA|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedA
echo -n "trie a ok - "
date
cat suffixeAlphabet/suffixeB|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedB
echo -n "trie b ok - "
date
cat suffixeAlphabet/suffixeC|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedC
echo -n "trie c ok - "
date
cat suffixeAlphabet/suffixeD|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedD
echo -n "trie d ok - "
date
cat suffixeAlphabet/suffixeE|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedE
echo -n "trie e ok - "
date
cat suffixeAlphabet/suffixeF|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedF
echo -n "trie f ok - "
date
cat suffixeAlphabet/suffixeG|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedG
echo -n "trie g ok - "
date
cat suffixeAlphabet/suffixeH|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedH
echo -n "trie h ok - "
date
cat suffixeAlphabet/suffixeI|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedI
echo -n "trie i ok - "
date
cat suffixeAlphabet/suffixeJ|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedJ
echo -n "trie j ok - "
date
cat suffixeAlphabet/suffixeK|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedK
echo -n "trie k ok - "
date
cat suffixeAlphabet/suffixeL|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedL
echo -n "trie l ok - "
date
cat suffixeAlphabet/suffixeM|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedM
echo -n "trie m ok - "
date
cat suffixeAlphabet/suffixeN|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedN
echo -n "trie n ok - "
date
cat suffixeAlphabet/suffixeO|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedO
echo -n "trie o ok - "
date
cat suffixeAlphabet/suffixeP|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedP
echo -n "trie p ok - "
date
cat suffixeAlphabet/suffixeQ|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedQ
echo -n "trie q ok - "
date
cat suffixeAlphabet/suffixeR|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedR
echo -n "trie r ok - "
date
cat suffixeAlphabet/suffixeS|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedS
echo -n "trie s ok - "
date
cat suffixeAlphabet/suffixeT|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedT
echo -n "trie t ok - "
date
cat suffixeAlphabet/suffixeU|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedU
echo -n "trie u ok - "
date
cat suffixeAlphabet/suffixeV|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedV
echo -n "trie v ok - "
date
cat suffixeAlphabet/suffixeW|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedW
echo -n "trie w ok - "
date
cat suffixeAlphabet/suffixeX|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedX
echo -n " triex ok - "
date
cat suffixeAlphabet/suffixeY|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedY
echo -n "trie y ok - "
date
cat suffixeAlphabet/suffixeZ|sort|uniq -c > suffixeAlphabetSorted/suffixeSortedZ
echo -n "trie z ok - "
date
cat suffixeAlphabetSorted/* |grep -iE " +[0-9][0-9]">suffixes_10_occs_min
date
