#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;
use Encode;

$|=1; 
my $keepMotInc = 0 ;

my $lang = 'fr';
my $sxp_dir="";
while (1) {
$_=shift;
if (!$_ || /^$/) {last;}
	elsif (/^-(-)?h(elp)?$/) {
		print "Le script gl_néologisme a pour but détecter les néologismes dérivationnels d'un texte et de les annoter comme tel. Pour bien fonctionner il nécessite les options suivantes:
\t-l\tpermet de selectionner la langue voulu (optionnel: par défaut la langue selectionnée sera le français)
\t-keepInc\tsi on veut conserver le mot étiqueté comme inconnu
\t-d$\tcorrespond au chemin vers les dossiers sxpipe contenant le lexique de référence et la base de données contenant les règles de transformation\n";
}
elsif (/^-d$/) {$sxp_dir=shift;}
elsif (/^-keepInc/) {$keepMotInc = 1;}
elsif (/^-l/) {$lang = shift;}
}
if ($sxp_dir eq "") {
  die "### ERROR: gl_neologisme.pl requires using option -d followed by the folder that contains rules database and lexicon";
}
#------------------------------------------------------------------------------------------------------
# Initialisation de la base de données et des fichiers qu'on veut utiliser et stockage des données
#------------------------------------------------------------------------------------------------------
my $dir_Lefff = $sxp_dir; #chemin pour accéder au lexique
my $dir_gl_Neo = $sxp_dir; #chemin pour accéder aux bases de règles

# On ouvre la base de règles
my $regles_dbh = DBI->connect("dbi:SQLite:$dir_gl_Neo/db_regles_morpho.dat", "", "", {RaiseError => 1, AutoCommit => 1});
$regles_dbh->{sqlite_unicode}=1;
my $regles_getall= $regles_dbh->prepare('SELECT pos1,pos2,pref1,suff1,pref2,suff2,lettrePref,lettreSuff,nbOcc,typeRegle FROM data WHERE pref1=? AND suff1=?;');

## hash de cache
my %is_in_lefff_cache;

#on ouvre la base de donnée du lefff
my $lefff_dbh;
$lefff_dbh = DBI->connect("dbi:SQLite:$dir_Lefff/$lang.mdb", "", "", {RaiseError => 1, AutoCommit => 1});
my $lefff_sth =  $lefff_dbh->prepare('select EXISTS (select * from data where lemma=? and tag=?);');


my $l = 0;

#------------------------------------------------------------------------------------------------------


while(<>){
# formattage
chomp;
if (/ ?(_XML|_MS_ANNOTATION|_PAR_BOUND)/) {
	print "$_\n";
	next;
}
s/^\s*/ /o;
s/\s$/ /o;

#Reconnaissance des mots étrangers
s/ ?([^\\])\(([^<])/$1 \( $2/go;
s/ ?([^\\])\)([^<])/$1 \)$2/go;
s/([^\\])\)/$1 \)/g;

my $tmp=$_." ";
$_ = "";
#print "$tmp \n";
while ($tmp =~ s/^( *([^\}]*\} )([^ ]*?) +(?:\[\|([^\]]*?)\|\] *)?[\)\| ]* *)//) { 
		my $motsxpipe = $1;
		my $original = $2;
		my $mot = $3;
		my $infoLex = $4;
		#print $motsxpipe."\n";
		#Si le mot est inconnu:
		if ($mot =~ m/_INC_/){
		my $motbis= $mot ;
		$mot=~ s/_INC_//g;
			$mot = "#$mot#";
			#print $mot."\n";
			#On extrait les préfixes et suffixes du mot inc
			my ($prefref,$suffref)= getAffixes($mot);
			my @prefInc = @$prefref;
			my @suffInc = @$suffref;
			push(@prefInc,"");
			push(@suffInc,"");
	
			my %allPropositionForMot; 


			#pour chaque part of speech que peut avoir le mot :
			my $posMot="inc";
		
			#On extrait toutes les données correspondants à ces suffixes ou préfixes dans notre base de données 
			my ($pref, $suff);
		
			for $pref (@prefInc){
				for $suff (@suffInc){

					$regles_getall->execute("$pref","$suff");
					while (($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$type) =  $regles_getall->fetchrow){
						#On limite le nb d'occ à > 80 et on bloque les règles contenant cf
						if ($nbOcc>80 and $pos1!~m/^cf/ and $pos2!~m/^cf/){
							#($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$type)=encode_UTF8($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$type);	
							#print "($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$type) ok!!! \n";
							my $newMot = $mot;
							$newMot =~ s/$pref1(.*)$suff1/$pref2$1$suff2/;
	
							#on stocke le mot généré par la regex s'il est connu du dico et si sa pos concorde:
							$newMot =~ m/^#(.*)#$/;
							#print "($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$type)";
							#print "\t $1 \n";
					
							if (exist_combi_mot_pos($1,$pos2)) {
								#print "\t \t combi possible entre: $mot $1\n";
								#On vérifie le contexte et la taille de la base
								if (bonContext($mot,$pref1,$suff1,$lettrePref,$lettreSuff)){ 
									my $catgramMot=$pos1;
									my $catgramLemme=$pos2;
									$catgramMot=~ s/_.*$//;
									$catgramLemme=~ s/_.*$//;	
									if (($catgramMot eq $catgramLemme) and ($type eq "F")){#(exists $dicomotLemme{$mot}{$newmot}) and -> si on veut lemmatiser
				 						$allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}{$type}{"$pref1/$pref2/$suff1/$suff2"}=$nbOcc; 
				 						#print "test {$mot}{$newMot}{$pos1}{$pos2}{$type}=$nbOcc \n"
									}elsif ($type eq "D"){
				 						$allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}{$type}{"$pref1/$pref2/$suff1/$suff2"}=$nbOcc; 
				 						#print "test {$mot}{$newMot}{$pos1}{$pos2}{$type}=$nbOcc \n"
				 										 						
									}
								}
							}
						}
					}
				}
			}

			#réécriture de mot sxpipe				
			$motsxpipe =~ m/(({[^\}]*\}) _INC_[^ ]*)/;
			my $motSxpipesansponct = $1;
			my $idSxpipe = $2;
			my $motsxpipeNeo = "";
			
			#on filtre les règles qu'on veut conserver et on les ajoute à %allMotForAnalogie
			my $allregles=gestBestRegle(\%allPropositionForMot);
			
			#On ajoute les règles conservées
			%allRegles = %$allregles;	
			
			if (keys %allRegles > 0){ #analogie trouvée
				if ($infoLex=~ m/fr/){
					@allAnalyses = split(/;/,$infoLex);
					for $analyse (@allAnalyses){
						@allInfosAnalyse = split(/:/,$analyse);
						$POSLex = $allInfosAnalyse[3];
						$POSLex =~ s/^NOM/NC/;   
						
						#print "$allInfosAnalyse[0] - $allInfosAnalyse[1] - $allInfosAnalyse[2] - $allInfosAnalyse[3] - $allInfosAnalyse[4]-\n";
						$allRegles{$POSLex}{":".$POSLex}{$allInfosAnalyse[1]."\:".$allInfosAnalyse[2]}{$allInfosAnalyse[0]}=1;
					}
				}
				
				my $lemme;
				for $pos (keys %allRegles){ #pour chaque POS
					for $infoNeo (keys %{$allRegles{$pos}}){ #pour chaque :POS
						@lemmes = keys %{$allRegles{$pos}{$infoNeo}};
						$allLemmes = join (";",@lemmes);
					
						if ($allLemmes eq "0"){
							$regles = join (",",keys %{$allRegles{$pos}{$infoNeo}{"0"}});	
							$regles.="::".$infoNeo;  
						
						}else{
							@analysesLemme=();
							for $lemme (keys %{$allRegles{$pos}{$infoNeo}}){
								if ($lemme ne "0"){
									#on concatène les types de règles
									@t1= (keys %{$allRegles{$pos}{$infoNeo}{"0"}});
									@t2 = (keys %{$allRegles{$pos}{$infoNeo}{$lemme}});
									@allregles = (@t1,@t2);
									$regles = join(",",@allregles);	
									push(@analysesLemme,$regles.":".$lemme.$infoNeo);
								}
							}
							$regles = join (";",@analysesLemme);
						}
					}
					#$motsxpipeNeo .= " | ".$idSxpipe." _INC_$pos [|$regles|]";	
					$motsxpipeNeo .= " | ".$idSxpipe." _NEO [|$regles|]";
				}
			
				#on met le nouveau mot sxpipe au bon format
				if ($motsxpipeNeo ne ""){
					my $newmotsxpipe;
					if ($keepMotInc){
						$newmotsxpipe = $motSxpipesansponct.$motsxpipeNeo;
					}else{
						$motsxpipeNeo =~ s/^ \| / /;
						$newmotsxpipe = $motsxpipeNeo;
					}
					#my $newmotsxpipe = $motSxpipesansponct.$motsxpipeNeo;
					my $debut= $motsxpipe =~ m/^ *(\(|\|)/;
					my $fin = $motsxpipe =~ m/(\)|\|) *$/;
					
					if (not $debut and not $fin){
						$newmotsxpipe =~ s/^ */ ( /;
					}elsif ($debut) {
						$motsxpipe =~ m/^ *(\(|\|)/;
						my $ponct_tmp = $1;
						$newmotsxpipe =~ s/^ */ $ponct_tmp /;
					}
					#--debut ponct--
					#if ($motsxpipe !~ m/^ *\|/){
					#	$newmotsxpipe =~ s/^ */ ( /;
					#}else{
					#	$newmotsxpipe =~ s/^ */|/;
					#}
					#my $debut= $motsxpipe =~ m/^ *(\(|\|)/;
					
					#--fin ponct--
					if ($motsxpipe =~ m/ *\) *$/){
						$newmotsxpipe =~ s/ *$/ ) /;
					}elsif($motsxpipe =~ m/ *\| *$/){
						$newmotsxpipe =~ s/ *$/\|/;
					}elsif (not $debut){			
						$newmotsxpipe =~ s/ *$/ \) /;
					}		
					$motsxpipe=$newmotsxpipe;
				}
			}
		}
		$_.=$motsxpipe;
	}

	# sortie 
	s/ +/ /go;
	s/( ?[^\\])\(([^<] ?)/$1 \($2/go;
	s/( ?[^\\])\)($|[^<:] ?)/$1\) $2/go; 
	s/ +/ /go;
	s/Ana *\( *([^\)]+?) *\) */Ana\($1\)/g;
	s/^ //;
	s/ $//;
	print "$_\n";
}

#------------------------------------------------------------------------------------------------------
# Fin du programme on ferme nos requêtes et on crée les indexs pour la base de données.
#------------------------------------------------------------------------------------------------------
$regles_getall->finish;
$regles_dbh->disconnect;
$lefff_sth->finish;
$lefff_dbh ->disconnect; # pour se déconnecter de la base de données en fin de programme.

##########################################################################################################
# MÉTHODES
##########################################################################################################

sub getAffixes{
	my ($motInc) = @_;
	my $id = 1;
	my (@pref, @suff);
	while ($id!=length($motInc)){
		$motInc =~ m/^(.{$id})(.*)$/;
		push(@pref,$1);
		push(@suff,$2);
		$id+=1; 
	}
	return (\@pref, \@suff);
}

sub	encode_UTF8{	
	my ($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$type)= @_;
	my $s = "$pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$type";
	$s = Encode::decode("utf-8",$s);
	($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$type) = split(",",$s);
	return ($pos1,$pos2,$pref1,$suff1,$pref2,$suff2,$lettrePref,$lettreSuff,$nbOcc,$type);
}

sub gestBestRegle{
	my ($allPropositionForMot)= @_;
	%allPropositionForMot = %$allPropositionForMot;
	my %listeRegle;
	my $bestScore = 1;
	my $nbRegleKeep=0;
	while ($bestScore!=0 and $nbRegleKeep<3){
		my @bestRegle = ("","","","");
		$bestScore = 0;
		my ($bestMot1,$bestMot2,$bestaffixes);
		for $mot (keys (%allPropositionForMot)){
			for $newMot (keys %{$allPropositionForMot{$mot}}){
				for $pos1 (keys %{$allPropositionForMot{$mot}{$newMot}}){
					for $pos2 (keys %{$allPropositionForMot{$mot}{$newMot}{$pos1}}){ 
						for $type (keys %{$allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}}){	
							for $affixes (keys %{$allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}{$type}}){
								my $nbocc= $allPropositionForMot{$mot}{$newMot}{$pos1}{$pos2}{$type}{$affixes};				
								if ($nbocc>$bestScore){
									$bestScore=$nbocc;
									@bestRegle=($pos1, $pos2,$type,$nbocc);
									$bestMot1=$mot;
									$bestMot2=$newMot;
									$bestaffixes = $affixes;
								}
							}
						}
					}
				}
			}
		}
		if ($bestScore>0){
			my $pos1_maj = get_pos_maj($bestRegle[0]);
			my $pos2_maj = get_pos_maj($bestRegle[1]);			
			if ($nbRegleKeep<1 and $bestScore<100){
				$listeRegle{$pos1_maj}{":$pos1_maj"}{"0"}{"Ana($bestRegle[2]/$bestRegle[3]/$bestMot2/$pos2_maj/$bestaffixes)"}=1;
				#print "{$pos1_maj }{ :: $pos1_maj }{ 0 }{ Ana($bestRegle[2]/$bestRegle[3]/$bestMot2/$pos2_maj)}\n"; 
				return \%listeRegle;	
			}
			$listeRegle{$pos1_maj}{":$pos1_maj"}{"0"}{"Ana($bestRegle[2]/$bestRegle[3]/$bestMot2/$pos2_maj/$bestaffixes)"}=1;
			#print "{$pos1_maj}{: $pos1_maj}{0}{Ana($bestRegle[2]/$bestRegle[3]/$bestMot2/$pos2_maj)}\n";
			
			delete($allPropositionForMot{$bestMot1}{$bestMot2}{$bestRegle[0]}{$bestRegle[1]}{$bestRegle[2]});
			$nbRegleKeep+=1;
		}
	}
	return \%listeRegle; #tab qui prend {POS}{::POS}{"0"->no lemme}{Etiquette Ana(info ana)}
}

sub bonContext{
	my ($mot,$pref1,$suff1,$lettrePref,$lettreSuff)=@_; 
#		print "$pref1$lettrePref.*$lettreSuff$suff1  #$mot#\n";
	
	if ($lettrePref eq "A"){
		$lettrePref="";
	} elsif ($lettrePref eq "C"){
		$lettrePref=qr/[bcçdfghjklmnpkrstvwxz]/;
	} elsif ($lettrePref eq "V"){
		$lettrePref=qr/[aàâäeéèêëiîïoôöuùûüy]/; 
	}
	if ($lettreSuff eq "A"){
		$lettreSuff="";
	} elsif ($lettreSuff eq "C"){
		$lettreSuff=qr/[bcçdfghjklmnpkrstvwxz]/;
	} elsif ($lettreSuff eq "V"){
		$lettreSuff=qr/[aàâäeéèêëiîïoôöuùûüy]/; 
	}
	#print "$pref1$lettrePref.*$lettreSuff$suff1   $mot\n";
	if ($mot=~ m/^$pref1($lettrePref.*$lettreSuff)$suff1/){
		return 1 if (length($1)>3);
	}
	return 0
}

sub exist_combi_mot_pos{ 
	my ($mot, $pos) = @_;
	return $is_in_lefff_cache{$mot}{$pos} if (defined($is_in_lefff_cache{$mot}{$pos}));
	$lefff_sth->execute($mot,get_pos_maj($pos));
	$existeCombinaisonMotPOS = $lefff_sth->fetchrow;
	$is_in_lefff_cache{$mot}{$pos} = $existeCombinaisonMotPOS;
	return $existeCombinaisonMotPOS;
}

sub get_pos_maj{ 
	my ($pos) = @_;
	$pos =~ s/^adj/ADJ/;
	$pos =~ s/^adv_/ADV_/;
	$pos =~ s/^advneg/ADVNEG/;
	$pos =~ s/^ce/CE/;
	$pos =~ s/^cfi/CFI/;
	$pos =~ s/^cln/CLN/;
	$pos =~ s/^coo/COO/;  
	$pos =~ s/^csu/CSU/;
	$pos =~ s/^det/DET/;
	$pos =~ s/^nc/NC/;
	$pos =~ s/^prep/PREP/;
	$pos =~ s/^pres/PRES/;  
	$pos =~ s/^pro/PRO/;
	$pos =~ s/^v/V/;
	$pos =~ s/ADJ_Kms/ADJ_ms/;
	return $pos
}
