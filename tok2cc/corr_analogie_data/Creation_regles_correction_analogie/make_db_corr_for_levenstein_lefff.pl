#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;
use Encode;



## hash de cache
my %is_in_lefff_cache;

#on ouvre la base de donnée du lefff
my $lefff_dbh = DBI->connect("dbi:SQLite:/usr/local/share/sxpipe/fr.mdb", "", "", {RaiseError => 1, AutoCommit => 1});
my $lefff_sth =  $lefff_dbh->prepare('select wordform from data;');

my $lefff_corr_dbh = DBI->connect("dbi:SQLite:/home/marion/Bureau/lefff_corr.db", "", "", {RaiseError => 1, AutoCommit => 0});
$lefff_corr_dbh->do("CREATE TABLE data(suff, pref, letter);");
my $lefff_corr_sth=$lefff_corr_dbh->prepare('INSERT INTO data(pref,suff , letter) VALUES (?,?,?)');


my $l = 0;
$lefff_sth->execute();
while (($wordform) =  $lefff_sth->fetchrow){
	$wordform= Encode::decode("utf-8",$wordform);
	
	$pref="";
	while ($wordform=~s/^(.)//){
		$pref="_" if ($pref eq "");
		$wordform="_" if ($wordform eq "");
		$lefff_corr_sth->execute($pref,$wordform, $1);
		$pref="" if ($pref eq "_");
		$wordform="" if ($wordform eq "_");
		$pref.=$1;
		$l++;
		if ($l % 1000 == 0) {
			print STDERR "\r  Loading data...$l";
	$lefff_corr_dbh->commit;
  	}
		
	}
}

print STDERR "\r  Loading data...$l\n";
$lefff_sth->finish;
$lefff_corr_sth->finish;
$lefff_corr_dbh->commit;
print STDERR "  Creating index...";
$lefff_corr_dbh->do("CREATE INDEX id_suff ON data(suff);");
$lefff_corr_dbh->do("CREATE INDEX id_pref ON data(pref);");
$lefff_corr_dbh->do("CREATE INDEX id_letter ON data(letter);");
$lefff_corr_dbh->do("CREATE INDEX id_suff_pref ON data(suff,pref);");

$lefff_corr_dbh->commit;
print STDERR "done\n";
$lefff_corr_dbh->disconnect;
