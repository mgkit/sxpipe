#!/usr/bin/env perl
# $Id$

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$| = 1;

$lang="fr";

$detect_language=1;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-no_gl$/ || /^-no_guess_language$/i) {$detect_language=0;}
    elsif (/^-l(?:ang)?=(.*)$/) {$lang=$1;} elsif (/^-l(?:ang)?$/) {$lang=shift;}
}

if ($lang eq "sk") {
    $known_words=qr/(?:[^ ]*[šďťňľŕĺáéíúýä][^ ]*)/io;
    $usually_foreign_words=qr/(?:for|to|made|and|devices?|Company|[sS]peech|University|Knowledge|Center|Recognition|L[Aa]nguage|Spoken|European|[^ ]{3,}logy|[^ ]{3,}tics|Distributed|Coordinating|Committee|Databases|Systems?|Assessment|Advisory|Engineering|Activities|[tT]he|Registry|Helpdesk|Foundation|Multi[mM]odality|Laboratory|Enhanced|Multilingual|Access|Libraries|Institute|Corpor[aA]|[sS]peech\-to\-[sS]peech|Cross-Language|Affairs|US|[^ ]{3,}tric|(?:meta\-)?analysis|[Aa]ging|[qQ]uality|English|[Dd]iagnosis|[Dd]isease|a?typical|(?:anti)?psychotic|utility|Drawing|Biography|Lambs|frequency|[aA]nnals|under|Department|Rights|Watch|[Pp]ublishers?|[tT]oward|new|[Cc]ommunities|ich|und|für|zwischen|dem|zum|[Ss]outh|corporate|this|think|way|currency|people|happy|watch|work|got|poor|others?|being|Annals|[Nn]eurology|[Pp]sychatry|of|is|man|men|into|out|l[ae\']|les|du|de|et|sauf|notamment|comme|avec|édition|une)/o;
} elsif ($lang eq "pl") {
    $known_words=qr/(?:[^ ]*[šďťňřáéíúýěů][^ ]*)/io;
    $usually_foreign_words=qr/(?:for|to|made|and|devices?|Company|[sS]peech|University|Knowledge|Center|Recognition|L[Aa]nguage|Spoken|European|[^ ]{3,}logy|[^ ]{3,}tics|Distributed|Coordinating|Committee|Databases|Systems?|Assessment|Advisory|Engineering|Activities|[tT]he|Registry|Helpdesk|Foundation|Multi[mM]odality|Laboratory|Enhanced|Multilingual|Access|Libraries|Institute|Corpor[aA]|[sS]peech\-to\-[sS]peech|Cross-Language|Affairs|US|[^ ]{3,}tric|(?:meta\-)?analysis|[Aa]ging|[qQ]uality|English|[Dd]iagnosis|[Dd]isease|a?typical|(?:anti)?psychotic|utility|Drawing|Biography|Lambs|frequency|[aA]nnals|under|Department|Rights|Watch|[Pp]ublishers?|[tT]oward|new|[Cc]ommunities|ich|und|für|zwischen|dem|zum|[Ss]outh|corporate|this|think|way|currency|people|happy|watch|work|got|poor|others?|being|Annals|[Nn]eurology|[Pp]sychatry|of|is|man|men|into|out|l[ae\']|les|du|de|et|sauf|notamment|comme|avec|édition|une)/o;
} elsif ($lang eq "ru") {
    $known_words=qr/(?:лингвистика)/io;
    $usually_foreign_words=qr/(?:[A-Za-zÉé]{3,})/o;
} elsif ($lang eq "fr") {
    $known_words=qr/(?:l[ae\']|les|du|de|et|\-|sauf|notamment|comme|avec|édition|une?|aux)/io;
    $usually_foreign_words=qr/(?:for|to|made|and|devices?|Company|[sS]peech|University|Knowledge|Center|Recognition|L[Aa]nguage|Spoken|European|[^ ]{3,}logy|[^ ]{3,}tics|Distributed|Coordinating|Committee|Databases|Systems?|Assessment|Advisory|Engineering|Activities|[tT]he|Registry|Helpdesk|Foundation|Multi[mM]odality|Laboratory|Enhanced|Multilingual|Access|Libraries|Institute|Corpor[aA]|[sS]peech\-to\-[sS]peech|Cross-Language|Affairs|US|[^ ]{3,}tric|(?:meta\-)?analysis|[Aa]ging|[qQ]uality|English|[Dd]iagnosis|[Dd]isease|a?typical|(?:anti)?psychotic|utility|Drawing|Biography|Lambs|frequency|[aA]nnals|under|Department|Rights|Watch|[Pp]ublishers?|[tT]oward|new|[Cc]ommunities|ich|und|für|zwischen|dem|zum|[Ss]outh|corporate|this|think|way|currency|people|happy|watch|work|got|poor|others?|being|Annals|[Nn]eurology|[Pp]sychatry|of|is|man|men|into|out)/o;
} else {
    $known_words=qr/(?:zzxxzzxx)/io;
    $usually_foreign_words=qr/(?:zzxxzzxxzzxxzzxx)/io;
}

    
while (<>) {
    chomp;
    if (/ (_XML|_MS_ANNOTATION|_PAR_BOUND) *$/ || $lang =~ /^(fa|ckb)$/) { # fa et ckb n'ont pas encore de lexique assez grand pour que ce détecteur fonctionne
	print "$_\n";
	next;
    }

    @words=();
    @comments=();
    $ne="";
    $level=0; #sécurité
    if (/>>> <<</ || /(?:^| )(?:<<<)?$usually_foreign_words(?:>>>)?(?: |$)/) {
	s/(?<=[^ ])\{/ \{/g;
	s/\}(?=[^ ])/\} /g;
	for (split) {
	    if (/^\{/) {
		$level++;
	    }
	    if ($level>0) {
		if ($currc ne "") {$currc.=" "}
		s/^ *\{ *//; 
		$currc.=$_;
		$currc=~s/ *\} *//;
	    } else {
		push(@words,$_);
		push(@comments,$currc);
		$currc="";
	    }
	    if (/\}$/) {
		$level--;
	    }
	}
	$tags="";
	for (@words) {
	    if (/^(?:<<<)?$usually_foreign_words(?:>>>)?$/o) {
		$tags=$tags."U";
	    } elsif (/^(?:<<<)?[A-ZÀÉÈÊËÂÄÔÖÛÜÇ].*(?:>>>)?$/o) {
		$tags=$tags."c";
	    } elsif (/^(?:<<<)?$known_words(?:>>>)?$/o) {
		$tags=$tags."N";
	    } elsif (/^(?:<<<)?[\"\'](?:>>>)?$/o) {
		$tags=$tags."g";
	    } elsif (/^s$/o) {
		$tags=$tags."1";
	    } elsif (/^[,;\:\.]$/o) {
		$tags=$tags."p";
	    } elsif (/^(?:<<<)?[\(«](?:>>>)?$/o || /^\[$/o || /^<<<\[>>>$/o) {
		$tags=$tags."o";
	    } elsif (/^(?:<<<)?[\)»](?:>>>)?$/o || /^\]$/o || /^<<<\]>>>$/o) {
		$tags=$tags."f";
	    } elsif (/^\?$/o || /^\!$/o) {
		$tags=$tags."b";
	    } elsif (/^_SENT_BOUND$/o) {
		$tags=$tags."s";
	    } elsif (/^<<<.*>>>$/o) {
		$tags=$tags."u";
	    } else {
		$tags=$tags."n";
	    }
	}
	$prevtags="";
#	print STDERR "$_\n";
#	print STDERR "  $tags\n";
	while ($prevtags ne $tags) {
	    $prevtags=$tags;
	    $tags=~s/([Uu])1/$1u/go;
	    $tags=~s/1([Uu])/u$1/go;
	    $tags=~s/cuc/uuu/go;
	    $tags=~s/cU/uU/go;
	    $tags=~s/Uc/Uu/go;
	    $tags=~s/([Uu])n([Uu])/$1u$2/go;
	    $tags=~s/oU.f/ouuf/go;
	    $tags=~s/o.Uf/ouuf/go;
	    $tags=~s/ouU.f/ouuuf/go;
	    $tags=~s/o.Uuf/ouuuf/go;
	    $tags=~s/o.uUf/ouuuf/go;
	    $tags=~s/oUu.f/ouuuf/go;
	    $tags=~s/ou.Uf/ouuuf/go;
	    $tags=~s/oU.uf/ouuuf/go;
	    $tags=~s/oUU..f/ouuuuf/go;
	    $tags=~s/oU.U.f/ouuuuf/go;
	    $tags=~s/oU..Uf/ouuuuf/go;
	    $tags=~s/o.U.Uf/ouuuuf/go;
	    $tags=~s/o..UUf/ouuuuf/go;
	    $tags=~s/oU..Uf/ouuuuf/go;
	    while ($tags=~s/([Uu])c([Uu])/$1u$2/goi) {}
	    while ($tags=~s/c([Uu][Uu])/u$1/goi) {}
	    while ($tags=~s/([Uu][Uu])c/$1u/goi) {}
	    $tags=~s/uu([pgof])u[uc]/uu$1uu/goi;
	    $tags=~s/uu([pgof])cu/uu$1uu/goi;
	    $tags=~s/[uc]u([pgof])uu/uu$1uu/goi;
	    $tags=~s/uc([pgof])uu/uu$1uu/goi;
	    while ($tags=~s/[uU][uU]nc/uuuu/go) {}
	    while ($tags=~s/cn[uU][uU]/uuuu/go) {}
	    $tags=~s/g[^pfos]uu/guuu/goi;
	    $tags=~s/uu[^pfos]g/uuug/goi;
	    $tags=~s/uu[^gfops]u/uuuu/goi;
	    $tags=~s/uu[^gofps][^gofps]uu/uuuuuu/goi;
	    $tags=~s/uup[^gofps][^gofps]uu/uupuuuu/goi;
	    $tags=~s/uu[^gofps]p[^gofps]uu/uuupuuu/goi;
	    $tags=~s/uu[^gofps][^gofps]puu/uuuupuu/goi;
	    $tags=~s/[uU][uU][uU][nc]p/uuuup/go;###
	    $tags=~s/[uU][uU][uU][nc][nc]p/uuuuup/go;
	    $tags=~s/p[nc][uU][uU][uU]/puuuu/go;
	    $tags=~s/p[nc][nc][uU][uU][uU]/puuuuu/go;
	    $tags=~s/([go])[nc][^gofs][uU]([uU]+[gf])/$1uuu$2/go;
	    $tags=~s/([go])[nc]p[uU]([uU]+[gf])/$1upu$2/go;
	    $tags=~s/([go])[nc][^gofps][^gofps][uU][uU]([uU]+[gf])/$1uuuuu$2/go;
	    $tags=~s/([go])[nc][^gofps]p[uU][uU]([uU]+[gf])/$1uupuu$2/go;
	    $tags=~s/([go])[nc]p[^gofps][uU][uU]([uU]+[gf])/$1upuuu$2/go;
	    $tags=~s/([go])[nc][^gofps][^gofps][^gofp][uU][uU][uU]([uU]+[gf])/$1uuuuuuu$2/go;
	    $tags=~s/([go])[nc]p[^gofps][^gofps][uU][uU][uU]([uU]+[gf])/$1upuuuuu$2/go;
	    $tags=~s/([go])[nc][^gofps]p[^gofps][uU][uU][uU]([uU]+[gf])/$1uupuuuu$2/go;
	    $tags=~s/([go])[nc][^gofps][^gofps]p[uU][uU][uU]([uU]+[gf])/$1uuupuuu$2/go;
	    $tags=~s/([go])[nc]p[^gofps]p[uU][uU][uU]([uU]+[gf])/$1upupuuu$2/go;
	    while ($tags=~s/uo(u+)f/uu$1u/goi) {}
	    $tags=~s/uuub(.)/uuuu$1/goi;###
	    $tags=~s/([go])u(u+)pu(u+[gf])/$1u$2uu$3/goi;
	    $tags=~s/([go])u(u+)pu(u+)pu(u+[gf])/$1u$2uu$3uu$4/goi;
	    $tags=~s/([go])u(u+)pu(u+)pu(u+)pu(u+[gf])/$1u$2uu$3uu$4uu$5/goi;
	}
	$tags=~s/^[cn][uU][uU]/uuu/go;
	$tags=~s/^[cn][cn][uU][uU]/uuuu/go;
	$tags=~s/[uU][uU][uU][cn][cn]p$/uuuuup/go;
	$tags=~s/[uU][uU][uU][cn]$/uuuu/go;
	$tags=~s/Uu/ee/go;
	$tags=~s/[Uu]U/ee/go;
	while ($tags=~s/[euU][euU][uU]/eee/go
	      || $tags=~s/[uU][euU][euU]/eee/go
	      || $tags=~s/[euU][uU][euU]/eee/go) {}
#	print STDERR "> $tags\n";
	$i=0;
	$state=0;
	$e="";
	$output = "";
	for (split(//,$tags)) {
	    $w=$words[$i];
	    $c=$comments[$i];
	    $origw=$w;
	    $w=~s/^<<<(.*)>>>$/$1/o;
#	    if ($c eq "" && $w!~/^[\(\|\)]/) {$c=$w} POURQUOI CETTE EXCLUSION ICI???
	    if ($c eq "") {$c=$w}
	    if (/^e$/ && $state==0) {
		if ($i>0) {$output .= " ";}
		$output .= "{";
		$output .= $c;
		$e.=$c." ";
		$state=1;
	    } elsif (/^[^e]$/ && $state==1) {
		$e=~s/ $//;
		$lang="";
		if ($detect_language) {
		    $lang=guess_language($e);
		}
		if ($c ne "") {
		    $output .= "} _ETR$lang \{$c\} $origw";
		} else {
		    $output .= "} _ETR$lang $origw";
		}
		$state=0;
		$e="";
	    } else {
		if ($i>0) {$output .= " ";}
		if ($state==1) {
		    $output .= $c;
		    $e.=$c." ";
		} else {
		    if ($c ne "") {
			$output .= "{$c} $origw";
		    } else {
			$output .= "$origw";
		    }
		}
	    }
	    if ($i==$#words) {
		if ($state==1){$output .= "} _ETR";}
		$output;
	    }
	    $i++;
	}
    } else {
#	s/<<<//g;
#	s/>>>//g;
      $output = $_;
    }
    while ($output =~ /".*"/ && $output =~ s/^(.*?)(?=")//) {
      print $1;
      if ($output =~ s/^((\") <<<([^>]+)>>> \2)//) {
	$tmp = $1;
	$quote = $2;
	$candidate = $3;
	if ($candidate =~ /(istes?|phobies?|eurs?|ismes?|ités?|isée?s?|ifiée?s?|iser?|ifier?|ment|apies?|tions?|[ai]bles?|iques?|tudes?|ances?|euses?|toires?|ages?)$/) {
	  print $tmp;
	} else {
	  print "$quote \{$candidate\} _ETR $quote";
	}
      } else {
	$output =~ s/(".*?")//;
	print $1;
      }
      last if $output =~ /^\s*$/;
    }
    print "$output\n";
}

sub guess_language {
    $_=shift;
    $lang="";
    $en=qr/(?:the|of|and|how|back|made in|water|from|south|corporate|this|think|way|currency|people|happy|new|watch|work|or|got|poor|others?|being)/io;
    $de=qr/(?:eine?|eine[snrm]|ich|und|für|zwischen|dem|zum)/io;
    $es=qr/(?:los|el|nuev[ao]s?)/io;
    $it=qr/(?:come|di|dell[a\'])/io;
    if (/qing?/io || /iang\b/io || /huan\b/io || /ao.ong\b/io || /eng\b/io || /xue\b/io || /anh\b/io) {
	$lang="_ZH";
    } elsif (/iy[ae]h?\b/io || /yy/io || /aâ/io || ($_!~/\bIraq\b/ && /q\b/io) || /qs/io || /qab\b/ || /\bal[\-\']/io || /\bakh?bar\b/io || /\bj[ai]h[ai]d\b/io || /llah\b/io || /\-e\b/io || /qh/io || /\bwa\b/io) {
	$lang="_AR";
    } elsif (/\b$en\b/ || /ee/io || /ity\b/io || /o[rm]y\b/io || /ely\b/io || /ss\b/io || /ism\b/io || /ial\b/io) {
	$lang="_EN";
    } elsif ($_!~/w\b$/io && ((/w/io && /[öü]/io) || /\b(?:$de|Die)\b/ || /ß/ || /\bD[Aa][Ss]\b/ || /\bdeutsch/io || /[^ ][^ aeio]ung\b/io || /..(?:tik|heit|heiten|keit|keiten|ungen|beit|beiten|ismus|schaft|schäfte)\b/io)) {
	$lang="_DE";
    } elsif (/\bvoor/io || /ijt/io) {
	$lang="_NL";
    } elsif ($_!~/oa/io &&  (/ñ/io || /[aeo]s\b/io || /ismo\b/io || /\b$es\b/ || /ci[oó]n/io || /cional\b/ || /[td]ad/io)) {
	$lang="_ES";
    } elsif (/\b$it\b/io || /zion(?:al)?e\b/io) {
	$lang="_IT";
    } elsif ($_!~/sh/io && $_!~/ck/io && (/æ/ || /o\b/io || /[^ aeio]u[sm]\b/io || /[^aeou]a[me]\b/io || /\bin\b/io)) {
	$lang="_LATIN";
    } elsif (/tion\b/io || /eux\b/io || /ique\b/io || /\b(?:l[ae\']|les|du|de|et|\-|sauf|notamment|comme|avec|édition|une?)\b/) {
	$lang="_FR";
    } elsif (/[tc]ki\b/io || /\bvoda\b/io || /\b[wvkz]\b/io) {
	$lang="_SL";
    } elsif (/\bngu/io) {
	$lang="_VE";
    } elsif (/j[aeiou]\b/io || /sai\b/io || /koi\b/io || /chi\b/io){
	$lang="_JP";
    } elsif (/ing\b/io || /[ao]w/io || /ea[^ ]/io) {
	$lang="_EN";
    }
    return $lang;
}
