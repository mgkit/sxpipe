#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
#use locale;
use DBI;

$|=1; 

my $share_dir = ""; # folder (typically /usr/local/share/sxpipe) that contains composants de composés data
my $moredic_dir = "";  #Dictionnaires ajoutés
my $morelex=0;
my $lang = "fr"; #langue du lexique utilisé par défaut fr

while (1) {
	$_=shift;
	if (/^$/) {last;}
		elsif (/^-(-)?h(elp)?$/) {
		print "Le script gl_decomposition a pour but de traiter les mots dits \"décomposés\" soit, les mots qui ont été segmentés par des tirets. Pour bien fonctionner il nécessite les options suivantes:
	\t-l\tpermet de selectionner la langue voulu (optionnel: par défaut la langue selectionnée sera le français
	\t-addLex\tpermet d'ajouter un lexique (un mot par ligne)
	\t-d\tcorrespond au répertoire dans lequel est stocké le t-test (permet de savoir si le mot décomposé apparait dans le corpus WaCKy)\n";
	}
	elsif (/^-l$/) {$lang = shift;}
	elsif (/^-d$/) {$share_dir=shift;}
	elsif (/^-addLex$/) {
		$moredic_dir=shift;
		$morelex = 1; 
	}
}

my $lex_dir=$share_dir; # folder (typically /usr/local/share/sxpipe) that contains composants de composés data

die "### ERROR: gl_décomposition.pl requires using option -d " if ($share_dir eq "");

## hash de cache
my %is_in_lefff_cache;
my %is_in_morelex_cache;
my %getTraitTtest_cache;


#on stocke les composants de composés
my @lexComp;
open FILE, "$share_dir/fr.cc";
binmode FILE, ":encoding(iso-8859-1)";
while (<FILE>) {
chomp;
s/^\"//;
s/\"$//;
push(@lexComp, $_);
}
close FILE;

#on ouvre la base de donnée du ttest
my $ttest_dbh;
$ttest_dbh = DBI->connect("dbi:SQLite:$share_dir/traitsTtest.dat", "", "", {RaiseError => 1, AutoCommit => 1});
$ttest_dbh->{sqlite_unicode}=1;
my $ttest_sth = $ttest_dbh->prepare('select * from data where key=?;');

#on ouvre la base de donnée du lefff
my $lefff_dbh;
$lefff_dbh = DBI->connect("dbi:SQLite:$lex_dir/$lang.mdb", "", "", {RaiseError => 1, AutoCommit => 1});
my $lefff_sth =  $lefff_dbh->prepare('select EXISTS (select * from data where wordform=?);');

#on ouvre la base de donnée des autres lexiques
my $morelex_dbh;
my $morelex_sth;
if ($morelex){
	$morelex_dbh = DBI->connect("dbi:SQLite:$moredic_dir/dic_viavoo.dat", "", "", {RaiseError => 1, AutoCommit => 1});
	$morelex_sth = $morelex_dbh->prepare('select EXISTS (select * from data where wordform=?);');
}


#on stocke les corrections pour les mots simples
while(<>){ #on parcourt le doc
# formattage
chomp;
if (/ ?(_XML|_MS_ANNOTATION|_PAR_BOUND) *$/) {
	print "$_\n";
	next;
}   
s/^\s*/ /o;
s/\s$/ /o;

#Pré-correction
$tmp = "$_ \n";
$_="";
while ($tmp =~ s/^(\s*(?:{[^{}]+})?\s*)([^ ]+)//) { #pour chaque mot sxpipe
		$comment = $1;
		$mot = $2;
		$_ .= $comment;
	#	print STDERR "-$comment----$mot-\n";
		$mot_mod = lc($mot);
		$mot_mod =~ s/_/ /g;

		#si mot non étiqueté et pas ds dico : 
		if (isInconnu($mot,$morelex) && (!is_in_lefff(lc $1) ||!is_in_lefff(lc $2))){ #si mot est inconnu
			$motq = quotemeta ($mot);

			#si composant de composé 
			if ($mot ~~ @lexComp){
				$mot=$mot;
				#next;
			##Si mot décomposé
			}elsif($mot =~ m/ *([cdjlmnqut]*[\.,;\:!\?\'\"\\])?(([^ \.,;:!\?\'\"\\-]+\-)+[^ \.,;:!\?\'\"\\-]+)([\.,;:!\?\'\"\\]?)/i){
				$motbis = $2;
				$motq = quotemeta ($2);
				$mot_corr = $motq;
				$mot_corr =~s/\\\-//g ;
				$mot_espace = $motq;
				$mot_espace =~s/\\\-/ /g ;
				$ponctdebut = $1;
				my $ponctend = $4;
				@motdec = split(/\\-/,$motq);
				$ecartType = getEcartType(@motdec);

				#si le mot ne comporte qu'un tiret, est dans le lefff et que chacune de ses parties fait plus d'une lettre, on le corrige et on l'annote :
				#print "$mot → $motdec[0] - $motdec[1]\n";
				if ($#motdec<2 && !isInconnu(lc $mot_corr,$morelex) && length($motdec[0])>1  && length($motdec[1])>1 ){
					$mot =~ s/$motq/$ponctdebut {$motbis◀SEGEMPH} $mot_corr $ponctfin/g if($mot !~ m/\-(elles?|ils?|on)$/);
					$mot=~ s/^ *//;
					$mot=~ s/  / /;
					$mot=~ s/ *$//;
				#si le mot contient plusieurs tirets, qu'il est dans le lefff et qu'il a un écart type < 2 :
				}elsif ($#motdec>=2 && !isInconnu(lc $mot_corr,$morelex) && $ecartType<2 ){
					$mot =~ s/$motq/$ponctdebut {$motbis◀SEGEMPH} $mot_corr $ponctfin/g if($mot !~ m/\-(elles?|ils?|on)$/);
					$mot=~ s/^ *//;
					$mot=~ s/  / /;
					$mot=~ s/ *$//;
				#Si le mot n'apparait pas dans le corpus WaCKy(ttest) on l'étiiquete comme ambiguë (sinon on ne change rien)
				}elsif (!is_in_ttest($mot) && length($motdec[0])>1  && length($motdec[1])>1 ) {
					$mot =~ s/$motq/$ponctdebut {$motbis} _TMP_SEGEMPH $ponctfin/g if($mot !~ m/\-(elles?|ils?|on)$/);
					$mot=~ s/^ *//;
					$mot=~ s/  / /;
					$mot=~ s/ *$//;
				}
			}
		}
		$_ .="$mot";			 
	}

	# sortie 
	s/^ //;
	s/ $//;
	print "$_";
}
#print STDERR "Correcteur: Décomposition: done\n";

################################################################################
#  MÉTHODES
################################################################################


sub is_in_lefff {
	my $word = shift;
	return $is_in_lefff_cache{$word} if (defined($is_in_lefff_cache{$word}));
	$lefff_sth->execute(lc $word);
	$reponse = $lefff_sth->fetchrow;
	$is_in_lefff_cache{$word} = $reponse;
	return $reponse;
}

sub is_in_morelex {
	my $word = shift;
	return $is_in_morelex_cache{$word} if (defined($is_in_morelex_cache{$word})) ;
	$morelex_sth->execute(lc $word);
	my $reponse = $morelex_sth->fetchrow;
	$is_in_morelex_cache{$word} = $reponse;
	return $reponse;
}

sub getEcartType {
	my (@motdec)=@_;
	
	#calcul de la moyenne
	my $somme_totale = 0;#représente la somme des caractères composants chaque élément
	for $elmt (@motdec){
		$somme_totale+=length($elmt);
	}
	my $moyenne = $somme_totale/($#motdec+1);

	#calcul de l'écart-type
	$somme_for_EcartType = 0;
	for $elmt (@motdec){
		$somme_for_EcartType_tmp = length($elmt)-$moyenne;
		$somme_for_EcartType+=$somme_for_EcartType_tmp*$somme_for_EcartType_tmp;
	}
	return $somme_for_EcartType/($#motdec+1);#on renvoit l'écart-type
}

sub is_in_ttest {
	my $word = $_;
	return $is_in_ttest_cache{$word} if (defined($is_in_ttest_cache{$word}));
	$ttest_sth->execute(lc $word);
	$reponse = $ttest_sth->fetchrow;
	if ($reponse !~ m\.\){
		$is_in_ttest_cache{$word} = 0;
		return 0;
	}
	$is_in_ttest_cache{$word} = 1;
	return 1;
}

sub isInconnu {
	my ($mot,$morelex)=@_;
	if ($mot =~ m/^_/) {
		return 0 ;#pas inconnu car déjà étiqueté
	}elsif ($mot =~ m/^\s*$/) {
		return 0 ;#pas inconnu car vide
	}elsif(is_in_lefff(lc $mot) || is_in_lefff($mot)) {
		return 0 ;#pas inconnu car in lefff
	}elsif($morelex){
		if(is_in_morelex($mot)) {
			#print " autre lex\n";
			return 0 ; #pas inconnu car in autres lexiques
		}
	}
	$mot_mod = lc($mot);
	$mot_mod =~ s/_/ /g;
	if(is_in_lefff(lc $mot_mod)) {
		return 0 ;#pas inconnu car in lefff
}elsif($morelex){
		if(is_in_morelex($mot)) {
			#print " autre lex\n";
			return 0 ; #pas inconnu car in autres lexique
		}
	}
	return 1; 
}
