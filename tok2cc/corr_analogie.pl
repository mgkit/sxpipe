#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;

##
#/usr/local/share/sxpipe/tok2cc/corr_analogie.pl -l $lang -d /usr/local/share/sxpipe -lr 0.00005 -ll 0.0001
##

$|=1; 
my $keepMotInc = 0 ;

my $lang = 'fr';
my ($dir_sxpipe);
my $lambdaR=0.5;
my $lambdaL=0.5;
my $wantScore = 0;
my $nbCorrKeeps = "all";
while (1) {
	$_=shift;
	if (!$_ || /^$/) {last;}
	elsif (/^-d/) {$dir_sxpipe=shift;}
	elsif (/^-lr/) {$lambdaR=shift;}
	elsif (/^-ll/) {$lambdaL=shift;}
	elsif (/^-l/) {$lang = shift;}
	elsif (/^-score/) {$wantScore = 1;}
	elsif (/^-nbcorr/) {$nbCorrKeeps = shift;}
}

die "### ERROR: correction_inc_analogie.pl requires using option -d " if ($dir_sxpipe eq "");
die "### ERROR: correction_inc_analogie.pl requires using option -lr " if ($lambdaR eq "");
die "### ERROR: correction_inc_analogie.pl requires using option -ll " if ($lambdaL eq "");


my $dir_Lefff_dic=$dir_sxpipe;
my $dir_Lefff_corr=$dir_sxpipe;
my $dir_dbregle=$dir_sxpipe;
my $dir_freq_token=$dir_sxpipe;

my $lambdaFreq=1-($lambdaR+$lambdaL);

my %typelettre2regex;
$typelettre2regex{"C"} = "[bBßcCçÇdDfFgGhHjJkKlLmMnNñÑpPqQrRsStTvVwWxXzZ¸]";
$typelettre2regex{"V"} = "[aAåÅâÂäÄāÃáÁàÀæÆeEêÊëÊéÉèÈiIîÎíÍìÌïÏoOôÔõÕóÓòÒöÖuUûÛüÜúÚùÙyYŷŶÿŸýÝœŒˆ¨˜´`]";
$typelettre2regex{"#"} = "#";

#print "bla\n";
#------------------------------------------------------------------------------------------------------
# Initialisation de la base de données et des fichiers qu'on veut utiliser et stockage des données
#------------------------------------------------------------------------------------------------------

# On ouvre la base de règles restreinte
my $regles_dbh = DBI->connect("dbi:SQLite:$dir_sxpipe/regle_corr_analogie_restreint.dat", "", "", {RaiseError => 1, AutoCommit => 1});
$regles_dbh->{sqlite_unicode}=1;
my $regles_getall= $regles_dbh->prepare('SELECT infixe_Corr,lettrePref,lettreSuff,score FROM data WHERE infixe_Err=?;');
my $err_getall= $regles_dbh->prepare('SELECT distinct infixe_Err From data;');
#On récup toutes les erreurs restreintes possible:
$err_getall->execute();
my @allErrPossible=();
while (($err) =  $err_getall->fetchrow){
	push(@allErrPossible, $err);
}
#print  "regle_corr_analogie_restreint - ".localtime."\n";

# On ouvre la base de règles large
my $reglesL_dbh = DBI->connect("dbi:SQLite:$dir_sxpipe/regle_corr_analogie_large.dat", "", "", {RaiseError => 1, AutoCommit => 1});
$reglesL_dbh->{sqlite_unicode}=1;
my $reglesL_getall= $reglesL_dbh->prepare('SELECT infixe_Corr,lettrePref,lettreSuff,score FROM data WHERE infixe_Err=?;');
my $errL_getall= $reglesL_dbh->prepare('SELECT distinct infixe_Err From data;');
#On récup toutes les erreurs large possible:
$errL_getall->execute();
my @allErrPossible_L=();
while (($err_L) =  $errL_getall->fetchrow){
	push(@allErrPossible_L, $err_L);
}
#print  "regle_corr_analogie_large - ".localtime."\n";

## hash de cache
my %is_in_lefff_cache;
my %is_in_lefff_nodia_cache;

## hash représentant les db
my %freq_mots;
my $is_in_dico;
my %is_in_nodia_dico;

#on ouvre la base de données du lefff
my $lefff_dbh = DBI->connect("dbi:SQLite:$dir_Lefff_dic/$lang.mdb", "", "", {RaiseError => 1, AutoCommit => 1});
$lefff_dbh->{sqlite_unicode}=1;
my $lefff_sth =  $lefff_dbh->prepare('select wordform, wordform_nodia from data;');
$lefff_sth->execute();
while (($mot,$mot_no_dia) =  $lefff_sth->fetchrow){ # fetchrow_array TODO
	$is_in_dico{$mot}=1;
	$is_in_nodia_dico{$mot_no_dia}{$mot}=1;
}
#print  "lefff - ".localtime."\n";


#on ouvre la base de donnée contenant les mots segmentés du lefff pour la correction générique
my $lefff_corr_dbh = DBI->connect("dbi:SQLite:$dir_Lefff_corr/lefff_corr_generique.dat", "", "", {RaiseError => 1, AutoCommit => 1});
$lefff_corr_dbh->{sqlite_unicode}=1;
my $lefff_corr_sth =  $lefff_corr_dbh->prepare('select letter from data where suff=? and pref=?;');

#on ouvre la base de donnée contenant les fréquences pour les mots de la langue donnée
my $freq_tok_dbh = DBI->connect("dbi:SQLite:"."$dir_freq_token"."/freq_wiki_fr.dat", "", "", {RaiseError => 1, AutoCommit => 1});
$freq_tok_dbh->{sqlite_unicode}=1;
my $get_freq_tok_sth =  $freq_tok_dbh->prepare('select tok,freq from data;');
$get_freq_tok_sth->execute();
while (($tok,$freq) =  $get_freq_tok_sth->fetchrow){
	$freq_mots{$tok}=$freq;
}
#print "db chargée - ".localtime."\n";

#------------------------------------------------------------------------------------------------------

my %Corr_possible = ();
my %Corr_possibleL = (); 
my %Corr_possibleR = (); 
my %score2corr = ();
my @newMots = ();

while(<>){
	# formattage
	chomp;
	next if (/ ?(_XML|_MS_ANNOTATION|_PAR_BOUND)/) ;
	s/^\s*/ /o;
	s/\s*$/ /o;
	
	#Reconnaissance des mots étrangers
	s/ ?([^\\])\(([^<])/$1 \( $2/go;
	s/ ?([^\\])\)([^<])/$1 \)$2/go;
	s/([^\\])\)/$1 \)/g;
	s/Affixe *\( *([^\)]+?) *\) */Affixe\($1\)/g;
	s/Ana *\( *([^\)]+?) *\) */Ana\($1\)/g;
	s/Assoc *\( *([^\)]+?) *\) */Assoc\($1\)/g;

	
	my $tmp=$_." ";
	$_ = "";
	while ($tmp =~ s/(^ *([\(\| ]*) *({[^\}]*\}) (.*?) (\[\|[^\|]*\|\])? *([ \)]*) *)//) { #pour chaque mot sxpipe
		my $motsxpipe = $1; #forme sxpipe entière
		my $poncdebut = $2; #ponct avant
		my $original = $3; #première partie de la forme sxpipe (dans les {} )
		my $mot = $4; #seconde partie de la forme sxpipe (après les {} )
		#my $mot = quotemeta($mot_original);
		#my $refEN = $5; #référent d'une entité nommée
		my $poncfin = $6; #ponct après
		$poncfin = "" if ($poncfin=~m/^ +$/);
		my $corr_analogique= "";
			
		# Si NEO on passe au mot suivant
		if ($mot =~ m/_INC_(NC|ADJ|V|ADV|ADVM)_/i){
			$_.=$motsxpipe;
			next;
		}
		# Si Inc_[ on passe au mot suivant
		if ($mot =~ m/_INC_\[/){
			$_.=$motsxpipe;
			next;
		}
			
		#Si le mot reste INC et non considéré comme un néo
		elsif ($mot =~ m/_?INC_/){	
			$mot=~ s/_?INC_//g;	
			###print STDERR "$mot:\n";
			$mot = "##".decompose_accents($mot)."##"; #On dédupplique les accents du mot
			%Corr_possible = ();
			%Corr_possibleR = ();
			%Corr_possibleL = ();
			%score2corr = ();
			
			#cherche Corr possible avec DB
			getcorrPossible($err,$mot,$regles_getall,0);
			getcorrPossible($err,$mot,$reglesL_getall,1);
			
			$mot =~ s/##//go;
			
			#si on a pas trouvé une corr avec la correction large on cherche avec la correction générique
			if (scalar keys %Corr_possibleR == 0 and scalar keys %Corr_possibleL == 0 ){ #Si pas de corr → corr générique
				getcorrGenerique(recompose_accents($mot));
			}else{ #sinon merge score des corrections trouvées.
				getScoreRegles();
			}
						
			#On récupère les résultats trouvés et on les met au format sxpipe si on veut garder toutes les corr
			if ($nbCorrKeeps eq "all"){
				for $corr (keys %Corr_possible){
					$oldCorr=$corr;
					
					#On réécrit la correction trouvée
					$corr =~ s/##//go;
					$original=~ m/^[^>]+>([^<]+)<\/F>\}/;
					$motoriginal = $1;
					$corr = majusculise_accents(uc $corr) if ($motoriginal=~ m/[A-ZÂÅÄÃÁÀËÊÉÈÎÏÍÌÔÖÕÓÒÛÜÚÙŶŸÝÇÑ]{3}/ or $motoriginal=~ m/^[A-ZÂÅÄÃÁÀËÊÉÈÎÏÍÌÔÖÕÓÒÛÜÚÙŶŸÝÇÑ]+$/);
					
					my $original_annote = $original;
					my $type =  $Corr_possible{$oldCorr}{type};
					my $scoreFinal= $Corr_possible{$oldCorr}{score};
					$original_annote =~ s/<\/F>/◀Corr_ANA_$scoreFinal-$type<\/F>/;
					$corr_analogique.= "$original_annote $corr | ";
				}
				$corr_analogique =~ s/\| $// if (scalar keys %Corr_possible > 0);
			}else{ #On prend uniquement le nombre de correction souhaité
				$corr_analogique = get_best_corr($original);
			}
		}
		#on adapte la ponctuation en fonction de si on a trouvé des corrections
		if ($corr_analogique ne ""){
			 #→ si on veut garder l'inconnu _INC
			#print "$poncfin - $poncdebut - $motsxpipe - $corr_analogique\n";
			#$poncfinq = quotemeta($poncfin);
			#$motsxpipe =~ s\$poncfinq *$\\;
			#$motsxpipe .= " | ".$corr_analogique;
			#sinon
			$motsxpipe = $corr_analogique;
			if (!$poncfin and !$poncdebut and $motsxpipe=~/\|/){
				$motsxpipe = "( $motsxpipe )";
			}elsif ((!$poncdebut or !$poncfin )and $motsxpipe=~/\|/){
				$motsxpipe=$poncdebut." ( $motsxpipe ) ".$poncfin  ;
			}else{
				$motsxpipe=$poncdebut.$motsxpipe.$poncfin  ;
			}
		}
		$_.=$motsxpipe;
	}
	
	# sortie 
	s/ +/ /go;
	s/( ?[^\\])\(([^<] ?)/$1 \($2/go;
	s/( ?[^\\])\)($|[^<:] ?)/$1\) $2/go; 
	s/Affixe *\( *([^\)]+?) *\) */Affixe\($1\)/g;
	s/Ana *\( *([^\)]+?) *\) */Ana\($1\)/g;
	s/Assoc *\( *([^\)]+?) *\) */Assoc\($1\)/g;
	s/ +/ /go;
	s/^ //;
	s/ $//;
	print "$_\n";
}

#------------------------------------------------------------------------------------------------------
# Fin du programme on ferme nos requêtes et on crée les indexs pour la base de données.
#------------------------------------------------------------------------------------------------------
$regles_getall->finish;
$regles_dbh->disconnect;
$reglesL_getall->finish;
$reglesL_dbh->disconnect;
$lefff_sth->finish;
$lefff_dbh ->disconnect; # pour se déconnecter de la base de données en fin de programme.

##########################################################################################################
# MÉTHODES
##########################################################################################################
sub get_best_corr{
	my ($original)=@_;
	my $corr_analogique="";
	my @bestCorr=();
	for $correctionPossible (keys %Corr_possible){
		$score2corr{$Corr_possible{$correctionPossible}{score}}{$correctionPossible}=1;
	}
	my @allScores= keys (%score2corr);
	my @allScores_sorted= sort({$b <=> $a} @allScores);
	
	#On récupère les X meilleures corrections:
	my $tab_size=@allScores_sorted;
	my $nb_corr_recup=0;
	while ($nb_corr_recup < $nbCorrKeeps and $tab_size>0){ #Tant qu'on a pas le nb de corr voulu 
		my $bestScore = shift(@allScores_sorted);
		$tab_size-=1;
		
		if ($wantScore){ #On décompte en conservant ce qui a le même score
			$nb_corr_recup+=1;
			for $correction (keys %{$score2corr{$bestScore}}){
				push(@bestCorr,$correction);
			}
		}else{ #on décompte en ne prenant que le nombre exat de corr voulu
			@allNewCorrPossible = keys %{$score2corr{$bestScore}};
			my $nbNewCorrPossible=@allNewCorrPossible;
			
			if(($nb_corr_recup+$nbNewCorrPossible)<= $nbCorrKeeps){
				for $correction (@allNewCorrPossible){
					$nb_corr_recup+=1;
					push(@bestCorr,$correction);
				}
			}else{#on choisi des corrections au pif
				while($nb_corr_recup < $nbCorrKeeps and scalar @allNewCorrPossible>0){
					my $id_elemt_select = int(rand(scalar @allNewCorrPossible));
					push(@bestCorr,$allNewCorrPossible[$id_elemt_select]);
					$nb_corr_recup+=1;
					#On supprime la corr add
					my @tmp=@allNewCorrPossible;
					if($id_elemt_select==0){
						@allNewCorrPossible=(@tmp[1..$nbNewCorrPossible-1]);
					}elsif($id_elemt_select==$nbNewCorrPossible-1){
						@allNewCorrPossible=(@tmp[0..$nbNewCorrPossible-2]);
					}else{
						@allNewCorrPossible=(@tmp[0..$id_elemt_select-1],@tmp[$id_elemt_select+1..$nbNewCorrPossible-1]);
					}
					$nbNewCorrPossible-=1;
				}
			}
		}
	}
	for $corr ( @bestCorr){
		$oldCorr=$corr;
		$corr =~ s/##//go;
		my $original_annote = $original;
		my $type =  $Corr_possible{$oldCorr}{type};
		my $scoreFinal= $Corr_possible{$oldCorr}{score};
		$original_annote =~ s/<\/F>/◀Corr_ANA_$scoreFinal-$type<\/F>/;
		$corr_analogique.= "$original_annote $corr | ";
	}
	$corr_analogique =~ s/\| $// if (scalar keys %Corr_possible > 0);
	return $corr_analogique
}

sub getScoreRegles{
	for $correctionLargePossible (keys %Corr_possibleL){
		my $newScore;
		my $scoreLarge = $Corr_possibleL{$correctionLargePossible}{score};
		my $scoreFreq=get_freq($correctionLargePossible);
		my $scoreRestreint="0";
		$scoreRestreint = $Corr_possibleR{$correctionLargePossible}{score} if (exists $Corr_possibleR{$correctionLargePossible});
		$newScore=($lambdaR*$scoreRestreint)+($lambdaL*$scoreLarge)+($lambdaFreq*$scoreFreq);
		$Corr_possible{$correctionLargePossible}{score}=$newScore;
		$Corr_possible{$correctionLargePossible}{type}="-RLFreq";
	}
}

sub is_in_lefff {
	my $word = shift;
	return $is_in_lefff_cache{$word} if (defined($is_in_lefff_cache{$word}));
	$lefff_sth->execute($word);
	my $reponse = $lefff_sth->fetchrow;
	$is_in_lefff_cache{$word} = $reponse;
	return $reponse;
}

sub is_in_lefff_no_dia {
	my $word = shift;
	return @{$is_in_lefff_nodia_cache{lc $word}} if (defined($is_in_lefff_nodia_cache{lc $word})) ;
	
	my @all_Corr_possible=();
	my $corr_trouvee=0;
	$lefff_nodia_sth->execute(lc $word);
	while (($mot) =  $lefff_nodia_sth->fetchrow){
		$corr_trouvee=1;
		push(@all_Corr_possible,$mot);
	}
	if ($corr_trouvee){
		@{$is_in_lefff_nodia_cache{lc $word}} = @all_Corr_possible;
		return @all_Corr_possible;
	}
	return ();
}

sub get_freq {
	my $worddiese = shift;
	$worddiese =~ m/^##(.*)##$/;
	return 0 if (not exists $freq_mots{lc $1});
	return $freq_mots{lc $1};
}

sub decompose_accents{
	my $all_err = shift;
	if ($all_err =~ /[âêîôûŷÂÊÎÔÛŶ]/) {
		$all_err =~ s/â/ˆa/go;
		$all_err =~ s/ê/ˆe/go;
		$all_err =~ s/î/ˆi/go;
		$all_err =~ s/ô/ˆo/go;
		$all_err =~ s/û/ˆu/go;
		$all_err =~ s/ŷ/ˆy/go;
		$all_err =~ s/Â/ˆA/go;
		$all_err =~ s/Ê/ˆE/go;
		$all_err =~ s/Î/ˆI/go;
		$all_err =~ s/Ô/ˆO/go;
		$all_err =~ s/Û/ˆU/go;
		$all_err =~ s/Ŷ/ˆY/go;
	}
	if ($all_err =~ /[äëïöüÿÄËÏÖÜŸ]/) {
		$all_err =~ s/ä/¨a/go;
		$all_err =~ s/ë/¨e/go;
		$all_err =~ s/ï/¨i/go;
		$all_err =~ s/ö/¨o/go;
		$all_err =~ s/ü/¨u/go;
		$all_err =~ s/ÿ/¨y/go;
		$all_err =~ s/Ä/¨A/go;
		$all_err =~ s/Ë/¨E/go;
		$all_err =~ s/Ï/¨I/go;
		$all_err =~ s/Ö/¨O/go;
		$all_err =~ s/Ü/¨U/go;
		$all_err =~ s/Ÿ/¨Y/go;
	}
	if ($all_err =~ /[āõÃÕñÑ]/) {
		$all_err =~ s/ā/˜a/go;
		$all_err =~ s/õ/˜o/go;
		$all_err =~ s/ñ/˜n/go;
		$all_err =~ s/Ã/˜A/go;
		$all_err =~ s/Õ/˜O/go;
		$all_err =~ s/Ñ/˜N/go;
	}
	if ($all_err =~ /[áéíóúÁÉÍÓÚÝ]/) {
		$all_err =~ s/á/´a/go;
		$all_err =~ s/é/´e/go;
		$all_err =~ s/í/´i/go;
		$all_err =~ s/ó/´o/go;
		$all_err =~ s/ú/´u/go;
		$all_err =~ s/Á/´A/go;
		$all_err =~ s/É/´E/go;
		$all_err =~ s/Í/´I/go;
		$all_err =~ s/Ó/´O/go;
		$all_err =~ s/Ú/´U/go;
		$all_err =~ s/Ý/´Y/go;
	}
	if ($all_err =~ /[àèìòùýÀÈÌÒÙ]/) {
		$all_err =~ s/à/`a/go;
		$all_err =~ s/è/`e/go;
		$all_err =~ s/ì/`i/go;
		$all_err =~ s/ò/`o/go;
		$all_err =~ s/ù/`u/go;
		$all_err =~ s/ý/´y/go;
		$all_err =~ s/À/`A/go;
		$all_err =~ s/È/`E/go;
		$all_err =~ s/Ì/`I/go;
		$all_err =~ s/Ò/`O/go;
		$all_err =~ s/Ù/`U/go;
	}
	if ($all_err =~ /[çÇ]/) {
		$all_err =~ s/ç/¸c/go;
		$all_err =~ s/Ç/¸C/go;
	}
	return $all_err;
}

sub recompose_accents {
	my $all_err = shift;
#	$all_err =~ s/([ˆ¨˜])([aeiou])/$rediac{$1}{$2}/g;
	return $all_err unless $all_err =~ /[ˆ¨˜´`¸]/;
	if ($all_err =~ /¨/) {
		$all_err =~ s/¨a/ä/go;
		$all_err =~ s/¨e/ë/go;
		$all_err =~ s/¨i/ï/go;
		$all_err =~ s/¨o/ö/go;
		$all_err =~ s/¨u/ü/go;
		$all_err =~ s/¨y/ÿ/go;
		$all_err =~ s/¨A/Ä/go;
		$all_err =~ s/¨E/Ë/go;
		$all_err =~ s/¨I/Ï/go;
		$all_err =~ s/¨O/Ö/go;
		$all_err =~ s/¨U/Ü/go;
		$all_err =~ s/¨Y/Ÿ/go;
	}
	if ($all_err =~ /˜/) {
		$all_err =~ s/˜a/ā/go;
		$all_err =~ s/˜o/õ/go;
		$all_err =~ s/˜n/ñ/go;
		$all_err =~ s/˜A/Ã/go;
		$all_err =~ s/˜O/Õ/go;
		$all_err =~ s/˜N/Ñ/go;
	}
	if ($all_err =~ /´/) {
		$all_err =~ s/´a/á/go;
		$all_err =~ s/´e/é/go;
		$all_err =~ s/´i/í/go;
		$all_err =~ s/´o/ó/go;
		$all_err =~ s/´u/ú/go;
		$all_err =~ s/´y/ý/go;
		$all_err =~ s/´A/Á/go;
		$all_err =~ s/´E/É/go;
		$all_err =~ s/´I/Í/go;
		$all_err =~ s/´O/Ó/go;
		$all_err =~ s/´U/Ú/go;
		$all_err =~ s/´Y/Ý/go;
	}
	if ($all_err =~ /`/) {
		$all_err =~ s/`a/à/go;
		$all_err =~ s/`e/è/go;
		$all_err =~ s/`i/ì/go;
		$all_err =~ s/`o/ò/go;
		$all_err =~ s/`u/ù/go;
		$all_err =~ s/`A/À/go;
		$all_err =~ s/`E/È/go;
		$all_err =~ s/`I/Ì/go;
		$all_err =~ s/`O/Ò/go;
		$all_err =~ s/`U/Ù/go;
	
	}
	if ($all_err =~ /ˆ/) {
		$all_err =~ s/ˆa/â/go;	
		$all_err =~ s/ˆe/ê/go;
		$all_err =~ s/ˆi/î/go;
		$all_err =~ s/ˆo/ô/go;
		$all_err =~ s/ˆu/û/go;
		$all_err =~ s/ˆy/ŷ/go;
		$all_err =~ s/ˆA/Â/go;
		$all_err =~ s/ˆE/Ê/go;
		$all_err =~ s/ˆI/Î/go;
		$all_err =~ s/ˆO/Ô/go;
		$all_err =~ s/ˆU/Û/go;
		$all_err =~ s/ˆY/Ŷ/go;
	}
	if ($all_err =~ /¸/) {
		$all_err =~ s/¸c/ç/go;
		$all_err =~ s/¸C/Ç/go;
	}
	return $all_err;
}

sub minusculise_accents{
	my $s = shift;
	$s =~ tr/ÂÅÄÃÁÀËÊÉÈÎÏÍÌÔÖÕÓÒÛÜÚÙŶŸÝÇÑ/âåäāáàëêéèîïíìôöõóòûüúùŷÿýçñ/;
	return $s;
}	

sub majusculise_accents{
	my $s = shift;
	$s =~ tr/âåäāáàëêéèîïíìôöõóòûüúùŷÿýçñ/ÂÅÄÃÁÀËÊÉÈÎÏÍÌÔÖÕÓÒÛÜÚÙŶŸÝÇÑ/;
	return $s;
}

sub normalisationAffixes{
	my ($err,$corr,$pref_normalize,$suff_normalize) = @_;

	my $dupliq_cd=0;
	my $dupliq_cg=0;
	my $suppr_cd=0;
	my $suppr_cg=0;

	#Gestion des dupplications de lettres
	if ($err =~ m/\+/){
		if ($err =~ s/^\+_?//){ #dupplication de la première lettre du prefixe dans l'erreur
			$pref_normalize=~ m/^.*\[([^\]]*)\]$/;
			$err="[$1]".$err;
			$suppr_cg=1;
		}	
		if ($err =~ s/_?\+$//){ #dupplication de la première lettre du suff dans l'erreur
			$suff_normalize=~m/^\[([^\]]*)\].*$/;
			$err.="[$1]";
			$suppr_cd=1;
		}
	}
	
	$err =~ s/\[\]//go;
	if ($corr =~ m/\+/){
		if ($corr =~ s/^\+_?//){ #dupplication de la première lettre du prefixe dans la corr
			$pref_normalize=~ m/^.\[([^\t]*)\]$/;
			my $tmp = $1;
			$dupliq_cg=1;
		}
		if ($corr =~ s/_?\+$//){ #dupplication de la première lettre du suff dans la corr
			$suff_normalize=~m/^<\[([^\t]*)\].$/;
			$dupliq_cd=1;
		}
	}

	$pref_normalize =~ s/^(.)//;
	if ($1 ne "["){
		$pref_normalize = $typelettre2regex{$1}.$pref_normalize;		
	}else{
		$pref_normalize = $1.$pref_normalize;		
	}
	$suff_normalize =~ s/(.)$//;
	if ($1 ne "]"){
		$suff_normalize = $suff_normalize.$typelettre2regex{$1};	
	}else{
		$suff_normalize = $suff_normalize.$1;	
	}
	return ($err,$corr,$pref_normalize,$suff_normalize,$dupliq_cd,$dupliq_cg,$suppr_cd,$suppr_cg);
}

sub createRegexCorr{
	my ($islarge,$txtmatch,$dupliq_cg,$dupliq_cd,$corr) = @_;
	return $corr if (not $dupliq_cd and not $dupliq_cg); #si aucune dupplication on renvoie la corr
	if ($islarge){ 
		$txtmatch=~ m/^(.).*(.)$/i;
		$corr=$1.$corr if ($dupliq_cg);
		$corr=$corr.$2 if ($dupliq_cd);
	}else{
		$txtmatch=~ m/^.(.).*(.).$/i;
		$corr=$1.$corr if ($dupliq_cg);
		$corr=$corr.$2 if ($dupliq_cd);
	}	
	return $corr;
}

sub genereNewMots{
	my ($errcorr,$mot,$regex_faute,$dupliq_cg,$dupliq_cd,$suppr_cd,$suppr_cg,$corr,$islarge)=@_;
	my $MotAnalys = $mot;	
	my $debutMot  = "";		
	#print $regex_faute."\n";
	while ($MotAnalys =~ m/^(.*?)($regex_faute)/i ){ # ou lc($MotAnalys) ?
		my $debutNewMot = $1;
		my $match = $2;
		#On crée la regex adaptée à la faute détectée et on l'applique afin d'avoir la forme corrigée.
		my $corr_obtained =  createRegexCorr($islarge,$match,$dupliq_cg,$dupliq_cd,$corr);
		my $MotAnalysCorr=$MotAnalys;
		# le pb de memoire est entre ici et la fin du while
		$MotAnalysCorr =~ s/$regex_faute/$1$corr_obtained$3/i;
		#On stocke la forme obtenue
		push(@newMots, $debutMot.$MotAnalysCorr);

		my $debutNewMotq= quotemeta($debutNewMot);
		#On conserve ce qui avait été lu auparavant.
		if($islarge){
			if($debutNewMot eq ""){
				$MotAnalys =~ s/^(.)//i;
				$debutNewMot=$1;
			}else{
				$MotAnalys=~ s/^$debutNewMotq//i;
			}
			$debutMot.=$debutNewMot;			
		}else{
			$MotAnalys=~ s/^$debutNewMotq//i;
			$debutNewMot =~ s/(.)$//;
			$MotAnalys=$1.$MotAnalys;
			if($debutNewMot =~ /^.?$/i){
				$MotAnalys =~ s/^(.)//i;
				$debutNewMot.=$1;
			}
			$debutMot.=$debutNewMot;
		}
##		$MotAnalys =~ s/^(.)//i;		#todo?!!!
##		$debutMot.=$1;	
	}
}

sub getcorrPossible{
	my ($err,$mot,$regleAappliquer,$islarge)=@_;
	#On parcourt chaque erreur possible 
	for $err (($islarge == 1) ? @allErrPossible_L : @allErrPossible){
		my $errq =  quotemeta($err);		
		#Si le mot contient l'erreur:
		if ($mot =~ m/$errq/ or $err =~ m/_/ or $err =~ m/^\+/ or $err =~ m/\+$/){	
			#Pour chaque erreur possible on vérifie les contextes. 
			$regleAappliquer->execute($err);
			while (($corr,$pref,$suff, $score) =  $regleAappliquer->fetchrow){
				#S'ils correspondent, on tente de corriger le mot et on vérifie l'existance de la correction.
				#On stocke la correction lorsqu'elle est possible.
				$errcorr=$err."-".$corr;				
				my ($err,$corr,$pref_normalize,$suff_normalize,$dupliq_cd,$dupliq_cg,$suppr_cd,$suppr_cg) = 
					normalisationAffixes($err,$corr,$pref,$suff);
				#On créé notre expression reg
				my $regex_faute;
				if($err eq "_"){
					$regex_faute= "($pref_normalize)()($suff_normalize)";
				} else {
					$regex_faute= "($pref_normalize)($err)($suff_normalize)";
				}
				$corr="" if ($corr eq "_");
				
				#on génère la création d'une correction possible
##				print "$errcorr : $regex_faute $dupliq_cg,$dupliq_cd,$suppr_cd,$suppr_cg\n";
				@newMots = ();
				genereNewMots($errcorr,$mot,$regex_faute,$dupliq_cg,$dupliq_cd,$suppr_cd,$suppr_cg,$corr,$islarge);
				for $newMot (@newMots){
					if ($newMot ne $mot){
						$newMot = recompose_accents($newMot);
						#on vérifie l'existence dans le lefff
						$newMot =~ m/^##(.*)##$/;
						###print "$errcorr : $newMot - $1  \n"; 
						if ($1){
							if ($is_in_dico{$1} or $is_in_dico{ucfirst($1)} or $is_in_dico{minusculise_accents(lc($1))}){
								#print "→→→→→→→ $errcorr : $newMot - $1 ".$is_in_dico{$1}."-".$is_in_dico{ucfirst($1)}."-".$is_in_dico{minusculise_accents(lc($1))}." \n"; 
								#Si ok on ajoute la correction possible
								if ($islarge){
									$Corr_possibleL{$newMot}{score}=$score unless defined($Corr_possibleL{$newMot}{score}) && $Corr_possibleL{$newMot}{score} >= $score;
									$Corr_possibleL{$newMot}{type}='L';
								}else{
									$Corr_possibleR{$newMot}{score}=$score unless defined($Corr_possibleR{$newMot}{score}) && $Corr_possibleR{$newMot}{score} >= $score;
									$Corr_possibleR{$newMot}{type}='R';
								}
							}else{
								#Si err diacritique on ajoute la correction possible
								my @corr = ();
								@corr = keys $is_in_nodia_dico{$1} if (exists $is_in_nodia_dico{$1});
								for $corr_no_dia (@corr){
									$corr_no_dia="##$corr_no_dia##";
									if ($islarge){
										$Corr_possibleL{$corr_no_dia}{score}=$score unless defined($Corr_possibleL{$corr_no_dia}{score}) && $Corr_possibleL{$corr_no_dia}{score} >= $score;
										$Corr_possibleL{$corr_no_dia}{type}='LA';
									}else{
										$Corr_possibleR{$corr_no_dia}{score}=$score unless defined($Corr_possibleR{$corr_no_dia}{score}) && $Corr_possibleR{$corr_no_dia}{score} >= $score;  
										$Corr_possibleR{$corr_no_dia}{type}='RA';
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

sub getcorrGenerique{
	my $mot = shift;
	my $mot_tmp = $mot;
	my $pref="";
	while ($mot_tmp =~ s/^(.)//){
		my $lettre = $1;
		#suppression d'une lettre
		if ($is_in_dico{ucfirst($pref.$mot_tmp)} or $is_in_dico{minusculise_accents(lc($pref.$mot_tmp))}) {
			$Corr_possible{$pref.$mot_tmp}{score}=sprintf( "%0.12f",get_freq($pref.$mot_tmp));
			$Corr_possible{$pref.$mot_tmp}{type}="Gd";
		}

		#subsitution d'une lettre
		$pref="_" if ($pref eq "");
		$lefff_corr_sth->execute(minusculise_accents(lc($mot_tmp)),minusculise_accents(lc($pref)));
		$pref="" if ($pref eq "_");
		while(($letter2) = $lefff_corr_sth->fetchrow){
			$Corr_possible{$pref.$letter2.$mot_tmp}{score}=sprintf( "%0.12f",get_freq($pref.$letter2.$mot_tmp));
			$Corr_possible{$pref.$letter2.$mot_tmp}{type}="Gs";
		}
 		$lefff_corr_sth->execute($mot_tmp,ucfirst($pref));
		while(($letter2) = $lefff_corr_sth->fetchrow){
			$Corr_possible{$pref.$letter2.$mot_tmp}{score}=sprintf( "%0.12f",get_freq($pref.$letter2.$mot_tmp));
			$Corr_possible{$pref.$letter2.$mot_tmp}{type}="Gs";
		}

		#inversement de deux lettres
		if ($mot_tmp ne ""){
			$mot_tmp =~ m/^(.)(.*)$/;
			if ($is_in_dico{ucfirst($pref.$1.$lettre.$2)} or $is_in_dico{minusculise_accents(lc($pref.$1.$lettre.$2))}) {
				$Corr_possible{$pref.$1.$lettre.$2}{score}=sprintf( "%0.12f",get_freq($pref.$1.$lettre.$2));
				$Corr_possible{$pref.$1.$lettre.$2}{type}="Gi";
			}
		}
		
		#ajout d'une lettre
		$lefff_corr_sth->execute(minusculise_accents(lc($lettre.$mot_tmp)),minusculise_accents(lc($pref)));
		while(($letter2) = $lefff_corr_sth->fetchrow){
			$Corr_possible{$pref.$letter2.$lettre.$mot_tmp}{score}=sprintf( "%0.12f",get_freq($pref.$letter2.$lettre.$mot_tmp));
			$Corr_possible{$pref.$letter2.$lettre.$mot_tmp}{type}="Ga";
		}
 		$lefff_corr_sth->execute($lettre.$mot_tmp,ucfirst($pref));
		while(($letter2) = $lefff_corr_sth->fetchrow){
			$Corr_possible{$pref.$letter2.$lettre.$mot_tmp}{score}=sprintf( "%0.12f",get_freq($pref.$letter2.$lettre.$mot_tmp));
			$Corr_possible{$pref.$letter2.$lettre.$mot_tmp}{type}="Ga";
		}
		$pref.=$lettre;
	}
	
	#ajout d'une lettre
	$lefff_corr_sth->execute("",minusculise_accents(lc($mot)));
	while(($letter2) = $lefff_corr_sth->fetchrow){
	  $Corr_possible{$mot.$letter2}{score}=sprintf( "%0.12f",get_freq($mot.$letter2));
		$Corr_possible{$mot.$letter2}{type}="Ga";
	}
 	$lefff_corr_sth->execute("",ucfirst($mot));
	while(($letter2) = $lefff_corr_sth->fetchrow){
	  $Corr_possible{$mot.$letter2}{score}=sprintf( "%0.12f",get_freq($mot.$letter2));
	  $Corr_possible{$mot.$letter2}{type}="Ga";
	}
}
