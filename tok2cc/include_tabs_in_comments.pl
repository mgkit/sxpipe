#!/usr/bin/env perl

$| = 1;

while (<>) {
  chomp;
  if (/ (_XML|_MS_ANNOTATION|_PAR_BOUND) *$/) {
    print "$_\n";
    next;
  }
  
  s/ +\t/\t/g;
  s/\t +/\t/g;
  s/\t\t+/\t/g;
  s/  +/ /g;
  s/\s+\}/\}/g;
  s/^\s*([^\{\s][^\s]*)/\{$1\} $1/g;
  while(s/(\}[^\{\}]+\s)([^\{\s][^\s\}]*)/$1\{$2\} $2/g) {}

  s/(\{[^\}]+)(\}\s*[^\s]+)(\s)/$1$3$2 /g;

  print "$_\n";
}
