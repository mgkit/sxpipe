#!/usr/bin/perl

use strict;

my $die_on_input_error=0;
while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-die$/) {$die_on_input_error=1;}
}

my $comment = qr/\{(?:\\}|[^}])*?\}/o;
my $form = qr/(?:\\[()|\\]|[^ \t\n{}()|])+/o;
my $semlex = qr/(?:\[\|(?:\|[^\]]|[^\|])*?\|\])/o;
my $cform = qr/$comment\s*$form(?:\s*$semlex)?/o;
my $cform_star = qr/(?:$cform\s*)*?/o;
my $parento = qr/(?<!\\)\(/o;
my $parentf = qr/(?<!\\)\)/o;
my $questionmark = qr/(?<!\\)\?/o;
my $pipe = qr/(?<![\\\[])\|(?!\])/o;
my $parents = qr/$parento\s*(?:$pipe\s*|$cform\s*)*$parentf/o;
my $cform_or_parents_star = qr/(?:$cform\s*|$parents\s*)*?/o;

my $s;
my ($output, $suboutput, $copy);
my $n;
my $m;
my ($last_j, $j, $k, $i);
my $depth;
my (@alts,@depth2curalts);
my $sentid = 0;

while (<>) {
  chomp;
#  while (s/({(?:\\}|[^}])*)\|/\1_#_PIPE/g) {}
  # echo "5 millions" | sxpipe | ./dag2pdag 
  s/\{((?:\\}|[^}])*)(<F [^<>]*>(?:\\}|[^}])*?<\/F>)\s*\}\s*($form(?:\s*$semlex)?)\s*\{\s*\2\s*\}\s*($form(?:\s*$semlex)?)\s*$questionmark/({$1$2} $3 | {$1} $3 {$2} $4)/g;
  # echo "{<F id="E1F2">millions</F>} millions? {<F id="E1F2">millions</F><F id="E1F1">5</F>} _NUMBER" | ./dag2pdag
  s/\{\s*((?:\\}|[^}])*?)\s*\}\s*($form(?:\s*$semlex)?)\s*$questionmark\s*\{\1((?:\\}|[^}])*)\s*\}\s*($form(?:\s*$semlex)?)/({$1} $2 {$3} $4 | {$1$3} $4)/g;
  # cas restants, on introduit une alternative de longueur nulle
  s/($cform)$questionmark/(|$1)/g;
  $copy = $_;
  while ($copy =~ s/$parento\s*(?:$pipe\s*|(?:$comment\s*)?$form(?:\s*$semlex)?\s*)*\s*$parentf/X/) {
  }
  if ($copy =~ /$pipe/ && $copy !~ /$parento/) {
    $_ = "( $_ )";
  }
  #print STDERR "$_\n";

  s/^\s+//;
  s/\s+$//;

  $depth = 0;
  $output = "";
  @depth2curalts = ();
  @alts = ();

  if (/id=\"E(\d+)F/) {
    $sentid = $1;
  }

  while (s/((?:$comment\s*)?$form(?:\s*$semlex)?|$parento|$pipe|$parentf)\s*//) {
    $s = $1;
    if ($s eq "(") {
      $depth++;
      @{$depth2curalts[$depth]} = ();
      $depth2curalts[$depth][0] = 1;
      $alts[$depth][0] = "";
    } elsif ($s eq ")") {
      if ($depth == 0) {
	die if $die_on_input_error;
	print STDERR "### WARNING (dag2pdag: sentence E$sentid): extra closing round bracket found and ignored! Some alternatives have been dropped\n";
	$output =~ s/^(.*$parentf).*$/\1 /; # si on veut qu'un ) en trop fasse plus comme un | en trop, commenter cette ligne
	next;
      }
      $m = $#{$alts[$depth]};
      if ($depth == 1) {
	$output .= "( " unless $m == 0;
	$output .= join ("| ", @{$alts[$depth]});
	$output .= ") " unless $m == 0;
      } else {
	$n = $#{$alts[$depth-1]};
	$m = $#{$alts[$depth]};
	for $j (0..$m) { # niveau courant $depth
	  for $i (0..$n) { # niveau parent $depth-1
	    next unless $depth2curalts[$depth-1][$i] == 1;
	    if ($j < $m) {
	      $k = $#{$alts[$depth-1]}+1;
	      $alts[$depth-1][$k] = $alts[$depth-1][$i].$alts[$depth][$j];
	      $depth2curalts[$depth-1][$k] = 1;
	    } else {
	      $alts[$depth-1][$i] .= $alts[$depth][$j];
	    }
	  }
	}
      }
      @{$alts[$depth]} = ();
      @{$depth2curalts[$depth]} = ();
      $depth--;
    } elsif ($s eq "|") {
      if ($depth == 0) {
	die if $die_on_input_error;
	print STDERR "### WARNING (dag2pad: sentence E$sentid): extra pipe found and ignored!\n";
	next;
      }
      $m = $#{$alts[$depth]};
      for $j (0..$m) { # niveau courant $depth
	$depth2curalts[$depth][$j] = 0;
      }
      $j = $#{$alts[$depth]} + 1;
      $depth2curalts[$depth][$j] = 1;
      $alts[$depth][$j] = "";
    } else {
      if ($depth == 0) {
	$output .= $s." ";
      } else {
	$m = $#{$alts[$depth]};
	for $j (0..$m) { # niveau courant $depth
	  next unless $depth2curalts[$depth][$j] == 1;
	  next unless $depth2curalts[$depth][$j] == 1;
	  $alts[$depth][$j] .= $s." ";
	}
      }
    }
  }

  if ($depth == 1) {
    die if $die_on_input_error;
    print STDERR "### WARNING (dag2pad: sentence E$sentid): missing closing round bracket! Acting as if a round bracket was added at the end of the sentence.\n";
    $output .= "( " unless $m == 0;
    $output .= join ("| ", @{$alts[$depth]});
    $output .= ") " unless $m == 0;
  } elsif ($depth > 1) {
    die if $die_on_input_error;
    print STDERR "### WARNING (dag2pad: sentence E$sentid): missing more than one closing round bracket! Content was dropped.\n";
  }

#  s/_#_PIPE/\|/g;

  $output =~ s/ $//;
  
  print "$output\n";
}
