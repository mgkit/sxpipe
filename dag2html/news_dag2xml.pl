#!/usr/bin/env perl

use warnings;

use strict;
use File::Find;
use File::Copy;
use Encode;

#prend la sortie de sxpipe passé sur un xml de dépêches
#convertit sortie dag en xml
#une balise xml par ligne
#une ligne de dag par ligne

$| = 1;

my $balise = "";
my $output = "";
my $lang = "fr";
my %entities = ();

while(<>)
{
	$output = $_;
	#si ligne = balise xml => print
	$output =~ s/&thinsp;//g;
	if($output =~ /^\{(<[^>]+>)\}\s*_XML$/)
	{
		$balise = $1;
		$balise =~ s/<\\+/</g;
		$balise =~ s/\\+(\?*>)/$1/g;
		print $balise."\n";
	}
	
	#si ligne = dag => convertir
	else
	{
	  ###ENTITIES
		$output =~ s/  +/ /g;
		while($output =~ /(\{[^\}]+\})\s*_([A-Z0-9]+(?:_[a-z])?)\s\[\|([^\|]+?)(?:__+(<a_href[^\|]*))?\|\]/)
		{
			$output = $`."#ENT#".$';
			my $type = $2;	my $entity = $1;	my $info = $3; my $href = ""; if ($4) {$href = $4};
			$href =~ s/_/ /g;
			$href =~ s/\&/\&amp;/g;
			$info =~ s/__+/\t/g;
			$info =~ s/\&/\&amp;/g;
			$info =~ s/\'/\&apos;/g;
			$info =~ s/</\&lt;/g;
			$info =~ s/>/\&gt;/g;
			$info =~ s/"/\&quot;/g;
			#my($entity, $infos) = split("\t", $info);
			#if($output =~ s/(\{[^\}]+\})\s*_([A-Z]+)\s\[\|([^\|]+)\|\]/<$2 info="$3">$1<\/$2>/){print "s\n";}
			#$entity =~ s/_/ /g;
			#$entity =~ s/\([0-9]+\)//;
			$entity =~ s/<[^>]+>//g;
			$entity =~ s/[\{\}]//g;	
			$output =~ s/#ENT#/<$type info="$info">$entity$href<\/$type>/;
			if($href){$entities{$type}{$entity}{$info."###".$href}++;}
			else{$entities{$type}{$entity}{$info}++;}
			#print STDERR "$type\t$entity\t$info";<STDIN>;
		}	

		####MARKUPS
		$output =~ s/_MARKUP__:([^\s\(\)}]+)/<\/$1>/g;
		#print STDERR $output;<STDIN>;
		$output =~ s/_MARKUP__([^:]+):/<$1>/g;
		#print STDERR $output;<STDIN>;
		$output =~ s/\(\s*\{[^\}]+\}\s*"\s*\|\s*\{[^\}]+\}\s*_EPSILON\s*\)/{"} "/g;
		#print STDERR $output;<STDIN>;
		#MARKUPS : soit les traiter tous un par un, soit faire une regex plus robuste que celle-là (il y aura peut-etre autre chose que des ) apres)
		$output =~ s/(\{[^\}]+\})\s*_(\w+)/<$2>$1<\/$2>/g;
		#print STDERR $output;<STDIN>;		
		####MARKUPS - END		
		
		
		###TEXTE
		#ligne qui enleve les doublons apparemment - rosa
		#while($output =~ s/(<F id=\")([^\"]+)(\"[^>]*>[^<]*<\/F>.*)<F id=\"\2\"[^>]*>[^<]*<\/F>/$1$2$3/g){}		
		#while($output =~ /(<F id=\")([^\"]+)(\"[^>]*>[^<]*<\/F>.*)<F id=\"\2\"[^>]*>[^<]*<\/F>/){print STDERR $&;<STDIN>;}		
		$output =~ s/\{([^\}]*)\}\s*(<(?:[DS]?[TH]?Q|AUT_CL|AUT|PRED)>) */$2/g;
		$output =~ s/ *\{([^\}]*)\}\s*(<\/(?:[DS]?[TH]?Q|AUT_CL|AUT|PRED)>)/$2 /g;
		while($output =~ s/(<F id=\")([^\"]+)(\"[^>]*>[^<]*<\/F>.*)<F id=\"\2\"[^>]*>[^<]*<\/F>/$1$2$3/g){}
#		print STDERR $output;<STDIN>;
		$output =~ s/<\/?F[^>]*>//g;
#		print STDERR $output;<STDIN>;		
		$output =~ s/{(\S+-(?:t-)?)(il|elle)} \S+ <AUT_CL>{} -(?:t-)?(?:il|elle)<\/AUT_CL>/$1<AUT_CL>$2<\/AUT_CL>/g;
#		$output =~ s/(^|[^>])(\{[^\}]+\})([^<])/$1$3/g;
		$output =~ s/\{([(|)])\} *\\+[(|)]/\\$1/g;
#		print STDERR $output."\n";
		$output =~ s/\{([^\}]*)\} *([^<{(|) ]+)/$1/g;
		$output =~ s/(^| )[\(\|\)]/$1/g;
		$output =~ s/\\//g;
		$output =~ s/  +/ /g;
		$output =~ s/(<[^>]+>\"|\() /$1/g;
		$output =~ s/ ("<\/[^>]+>|\))/$1/g;
#		print STDERR $output."\n";
		$output =~ s/¦¦.¦¦(.)¦¦/$1/g;
		$output =~ s/[\{\}]//g;	
		
		$output =~ s/e__prep le__det/u/g;
		$output =~ s/e__prep les__det/es/g;
		$output =~ s/e__prep lequel/uquel/g;
		$output =~ s/e__prep ledit/udit/g;
		$output =~ s/e__prep lesdits/esdits/g;
		$output =~ s/e__prep lesquels/esquels/g;
		$output =~ s/e__prep lesquelles/esquelles/g;
		$output =~ s/usqu'à le__det/usqu'au/g;
		$output =~ s/usqu'à les__det/usqu'aux/g;
		$output =~ s/i__csu il/'il/g;
		
		$output =~ s/à__prep le__det/au/g;
		$output =~ s/à__prep les__det/aux/g;
		$output =~ s/à__prep lequel/auquel/g;
		$output =~ s/à__prep lesquels/auxquels/g;
		$output =~ s/à__prep lesquelles/auxquelles/g;
		$output =~ s/À__prep le__det/Au/g;
		$output =~ s/À__prep les__det/Aux/g;
		$output =~ s/À__prep lequel/Auquel/g;
		$output =~ s/À__prep lesquels/Auxquels/g;
		$output =~ s/À__prep lesquelles/Auxquelles/g;
		$output =~ s/en__prep les__det/ès/g;
		$output =~ s/En__prep les__det/Ès/g;
		$output =~ s/¦¦à¦¦À¦¦__prep le__det/¦¦a¦¦A¦¦u/g;
		$output =~ s/¦¦à¦¦À¦¦__prep les__det/¦¦a¦¦A¦¦ux/g;
		$output =~ s/¦¦à¦¦À¦¦__prep lequel/¦¦a¦¦A¦¦uquel/g;
		$output =~ s/¦¦à¦¦À¦¦__prep lesquels/¦¦a¦¦A¦¦uxquels/g;
		$output =~ s/¦¦à¦¦À¦¦__prep lesquelles/¦¦a¦¦A¦¦uxquelles/g;
		$output =~ s/¦¦e¦¦E¦¦n__prep les__det/¦¦è¦¦È¦¦s/g;
		
		
		
		$output =~ s/\\\(\s*/(/g;
		$output =~ s/\s*\\\)/)/g;
		$output =~ s/([\w]')\s+/$1/g;
		$output =~ s/ ,/,/g;
		$output =~ s/\\%/%/g;		

		###BALISES XML
		while($output =~ /info="[^_"]+_+[^"]+"/)
		{	
			my $match = $&;
			$output = $`."#BLANK#".$';
			#$match =~ s/geoname_[0-9]+//gi;
			$match =~ s/(geoname_[0-9]+)/[$1]/gi;
			$match =~ s/__+/\t/g;
			$match =~ s/_/ /g;
			$output =~ s/#BLANK#/$match/g;
		}
		#print STDERR $output;<STDIN>;
# 		while($output =~ /info="[^\t]+\t\(UNKNOWN[^\)]+\)"/)
# 		{
# 			my $match = $&;
# 			$output = $`."#BLANK#".$';
# 			$match =~ s/\(UNKNOWN[^\)]+\)/#/g;
# 			$output =~ s/#BLANK#/$match/g;
# 		}
		$output =~ s/\\\(/(/g;
		$output =~ s/\\\(/)/g;		
		$output =~ s/  +/ /g;
		#encore des pbs de doublons
		#print STDERR $output;<STDIN>;		
		print $output." ";
	}
}

my $date = `date '+%m%d%y%H%M%S'`;	chomp($date);
#my $old_log = "entities_log_".$date.".xml";
#File::Copy::move("entities_log.xml", $old_log);
unlink("entities_log.xml");
$date = `date '+%m/%d/%y // %H:%M:%S'`;
chomp($date);
my $log = "entities_log.xml";
open(EN, ">$log") or die "Can't open $log: $!\n";
print EN "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<ENTITIES>\n";
print EN "<!-- ENTITIES BY SXPIPE-TMP CREATED $date -->\n\n";
foreach my $type(sort(keys(%entities)))
{
	print EN "<$type>\n";
	foreach my $entity(sort(keys(%{$entities{$type}})))
	{
		my $href = 0;
		print EN "<ENTITY print=\"$entity\" ";
		foreach my $info(sort(keys(%{$entities{$type}{$entity}})))
		{
			my $occ = $entities{$type}{$entity}{$info};
			if($info =~ /^(.+?)###(.+)$/){$info = $1; $href = $2;}
			$href =~ s/^<a\s+href="//;
			$href =~ s/">[^<]+<\/a>//;
			#$info =~ s/__+/\t/g;
			my($name, $infos) = split("[\t]", $info);
			$name =~ s/_/ /g;
			if(!$infos){$infos = "null";}
			$infos =~ s/_/ /g;
			if($href){print EN "name=\"$name\" occ=\"$occ\" infos=\"$infos\" href=\"$href\"/>\n";}
			else{print EN "name=\"$name\" occ=\"$occ\" infos=\"$infos\"/>\n";}
		}	
	}
	print EN "</$type>\n";
}
print EN "</ENTITIES>";
close(EN);


__END__
while($output =~ /(\{[^\}]+\})\s*_(ORG2)/)
{
	$output = $`."#ENT#".$';
	my $type = $2;	my $entity = $1;
	$entity =~ s/<[^>]+>//g;
	$entity =~ s/[\{\}]//g;	
	my $info = "null";
	$entities{$type}{$entity}{$info}++;
	$output =~ s/#ENT#/<$type>$entity<\/$type>/;
}
