#!/bin/sh

DIR=$1
XML_IN=$2
XML_OUT=$XML_IN
XML_OUT=${XML_OUT/.xml/_out.xml}
DAG=$XML_IN
DAG=${DAG/.xml/.dag}
SXPIN=$XML_IN
SXPIN=${SXPIN/.xml/_sxpinput}
SXPOUT=$XML_IN
SXPOUT=${SXPOUT/.xml/_sxpoutput}
HTML1=$XML_IN
HTML1=${HTML1/.xml/.html}
HTML2=$XML_IN
HTML2=${HTML2/.xml/_entities.html}

echo "$DIR"
echo "$XML_IN"
echo "$XML_OUT"

echo "NEWS > XML"
news2xml.pl $DIR $XML_IN
echo "XML > TXT > SXP > DAG > XML"
#news_xml2txt.pl $XML_IN | sxpipe-tmp -g > $DAG
news_xml2txt.pl $XML_IN | tee $SXPIN | sxpipe-tmp | tee $SXPOUT | news_dag2xml.pl > $XML_OUT
#news_xml2txt.pl $XML_IN | sxpipe-tmp -g | news_dag2xml.pl > $XML_OUT
echo "XML > HTML"
#xsltproc `pwd`/sxp2html.xsl $XML_OUT | recode html..l1 > $HTML1
xsltproc /Users/stern/bin/ALPC/sxpipe/dag2html/sxp2html.xsl $XML_OUT > $HTML1
#xsltproc `pwd`/entities2html.xsl $XML_OUT | recode html..l1 > $HTML2
#xsltproc /Users/stern/bin/ALPC/sxpipe/dag2html/entities2html.xsl $XML_OUT > $HTML2
#ou chemin de la feuille de style
